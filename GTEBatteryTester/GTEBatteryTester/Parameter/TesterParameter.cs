﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEBatteryTester.Parameter
{
    public class TesterParameter
    {
        public const int SCANPORTTIME = 5;
        public const string APPSETTINGFILEFILE = @"AppSetting.ap";

        public const int DEFAULTRETRYCONNTECTEDTIME = 3;
    }
}
