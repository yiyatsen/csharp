﻿#pragma checksum "..\..\MainWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2C9371C081C087B2A608A56A3AF99CAE6CBE9915"
//------------------------------------------------------------------------------
// <auto-generated>
//     這段程式碼是由工具產生的。
//     執行階段版本:4.0.30319.42000
//
//     對這個檔案所做的變更可能會造成錯誤的行為，而且如果重新產生程式碼，
//     變更將會遺失。
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace GTEBatteryTester {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.StatusBarItem statusValue;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.StatusBarItem portStatusValue;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.StatusBarItem baudRateStatusValue;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.StatusBarItem versionValue;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl cmdTabControl;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel cmdPanel;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox setDateTimeValue;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox setMCUEEPAddress;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox setMCUEEPValue;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox getMCUEEPAddress;
        
        #line default
        #line hidden
        
        
        #line 170 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox BarcodeText;
        
        #line default
        #line hidden
        
        
        #line 199 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox setPBEEPAddress;
        
        #line default
        #line hidden
        
        
        #line 214 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox setPBEEPValue;
        
        #line default
        #line hidden
        
        
        #line 240 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox getPBEEPAddress;
        
        #line default
        #line hidden
        
        
        #line 275 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox setCANParaStatus;
        
        #line default
        #line hidden
        
        
        #line 289 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox setCANPara;
        
        #line default
        #line hidden
        
        
        #line 302 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox setCANData;
        
        #line default
        #line hidden
        
        
        #line 320 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox setCANEngineMins;
        
        #line default
        #line hidden
        
        
        #line 353 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox hexDataCheckBox;
        
        #line default
        #line hidden
        
        
        #line 355 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cryptoDataCheckBox;
        
        #line default
        #line hidden
        
        
        #line 360 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button switchBatteryMode;
        
        #line default
        #line hidden
        
        
        #line 373 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox sendDataText;
        
        #line default
        #line hidden
        
        
        #line 390 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox receivedDataText;
        
        #line default
        #line hidden
        
        
        #line 407 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox outputDataText;
        
        #line default
        #line hidden
        
        
        #line 413 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button clearbtn;
        
        #line default
        #line hidden
        
        
        #line 426 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox messageDataText;
        
        #line default
        #line hidden
        
        
        #line 445 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox customSendCMDText;
        
        #line default
        #line hidden
        
        
        #line 450 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox customParserCMDText;
        
        #line default
        #line hidden
        
        
        #line 452 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button sendCMD;
        
        #line default
        #line hidden
        
        
        #line 454 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button parserCMD;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GTEBatteryTester;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.statusValue = ((System.Windows.Controls.Primitives.StatusBarItem)(target));
            return;
            case 2:
            this.portStatusValue = ((System.Windows.Controls.Primitives.StatusBarItem)(target));
            return;
            case 3:
            this.baudRateStatusValue = ((System.Windows.Controls.Primitives.StatusBarItem)(target));
            return;
            case 4:
            this.versionValue = ((System.Windows.Controls.Primitives.StatusBarItem)(target));
            return;
            case 5:
            this.cmdTabControl = ((System.Windows.Controls.TabControl)(target));
            return;
            case 6:
            this.cmdPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 7:
            
            #line 68 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.setCMD_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            
            #line 70 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.setCMD_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            
            #line 72 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.setCMD_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            
            #line 74 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.setCMD_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            
            #line 76 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.setCMD_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            
            #line 78 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.setCMD_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            
            #line 81 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCMD_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            
            #line 83 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCMD_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            
            #line 85 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCMD_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            
            #line 87 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCMD_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            
            #line 89 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCMD_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            
            #line 91 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCMD_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.setDateTimeValue = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.setMCUEEPAddress = ((System.Windows.Controls.TextBox)(target));
            return;
            case 21:
            this.setMCUEEPValue = ((System.Windows.Controls.TextBox)(target));
            return;
            case 22:
            
            #line 135 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.setCMD_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.getMCUEEPAddress = ((System.Windows.Controls.TextBox)(target));
            return;
            case 24:
            
            #line 154 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCMD_Click);
            
            #line default
            #line hidden
            return;
            case 25:
            this.BarcodeText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 26:
            
            #line 172 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.setBarcode_Click);
            
            #line default
            #line hidden
            return;
            case 27:
            
            #line 184 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCMD_Click);
            
            #line default
            #line hidden
            return;
            case 28:
            
            #line 185 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCMD_Click);
            
            #line default
            #line hidden
            return;
            case 29:
            this.setPBEEPAddress = ((System.Windows.Controls.TextBox)(target));
            return;
            case 30:
            this.setPBEEPValue = ((System.Windows.Controls.TextBox)(target));
            return;
            case 31:
            
            #line 216 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.setCMD_Click);
            
            #line default
            #line hidden
            return;
            case 32:
            
            #line 218 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.setCMD_Click);
            
            #line default
            #line hidden
            return;
            case 33:
            
            #line 220 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.setCMD_Click);
            
            #line default
            #line hidden
            return;
            case 34:
            
            #line 222 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.setCMD_Click);
            
            #line default
            #line hidden
            return;
            case 35:
            this.getPBEEPAddress = ((System.Windows.Controls.TextBox)(target));
            return;
            case 36:
            
            #line 242 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCMD_Click);
            
            #line default
            #line hidden
            return;
            case 37:
            
            #line 244 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCMD_Click);
            
            #line default
            #line hidden
            return;
            case 38:
            
            #line 246 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCMD_Click);
            
            #line default
            #line hidden
            return;
            case 39:
            
            #line 248 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCMD_Click);
            
            #line default
            #line hidden
            return;
            case 40:
            this.setCANParaStatus = ((System.Windows.Controls.TextBox)(target));
            return;
            case 41:
            
            #line 277 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCANCMD_Click);
            
            #line default
            #line hidden
            return;
            case 42:
            this.setCANPara = ((System.Windows.Controls.TextBox)(target));
            return;
            case 43:
            this.setCANData = ((System.Windows.Controls.TextBox)(target));
            return;
            case 44:
            
            #line 304 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCANCMD_Click);
            
            #line default
            #line hidden
            return;
            case 45:
            
            #line 308 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCANCMD_Click);
            
            #line default
            #line hidden
            return;
            case 46:
            
            #line 309 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCANCMD_Click);
            
            #line default
            #line hidden
            return;
            case 47:
            this.setCANEngineMins = ((System.Windows.Controls.TextBox)(target));
            return;
            case 48:
            
            #line 321 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCANCMD_Click);
            
            #line default
            #line hidden
            return;
            case 49:
            
            #line 327 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCANCMD_Click);
            
            #line default
            #line hidden
            return;
            case 50:
            
            #line 328 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCANCMD_Click);
            
            #line default
            #line hidden
            return;
            case 51:
            
            #line 329 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.getCANCMD_Click);
            
            #line default
            #line hidden
            return;
            case 52:
            
            #line 350 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Primitives.ToggleButton)(target)).Click += new System.Windows.RoutedEventHandler(this.ConnectButton_Click);
            
            #line default
            #line hidden
            return;
            case 53:
            
            #line 352 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.SettingButton_Click);
            
            #line default
            #line hidden
            return;
            case 54:
            this.hexDataCheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 354 "..\..\MainWindow.xaml"
            this.hexDataCheckBox.Checked += new System.Windows.RoutedEventHandler(this.hexDataCheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 354 "..\..\MainWindow.xaml"
            this.hexDataCheckBox.Unchecked += new System.Windows.RoutedEventHandler(this.hexDataCheckBox_Checked);
            
            #line default
            #line hidden
            return;
            case 55:
            this.cryptoDataCheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 356 "..\..\MainWindow.xaml"
            this.cryptoDataCheckBox.Checked += new System.Windows.RoutedEventHandler(this.cryptoDataCheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 356 "..\..\MainWindow.xaml"
            this.cryptoDataCheckBox.Unchecked += new System.Windows.RoutedEventHandler(this.cryptoDataCheckBox_Checked);
            
            #line default
            #line hidden
            return;
            case 56:
            this.switchBatteryMode = ((System.Windows.Controls.Button)(target));
            
            #line 360 "..\..\MainWindow.xaml"
            this.switchBatteryMode.Click += new System.Windows.RoutedEventHandler(this.switchBatteryMode_Click);
            
            #line default
            #line hidden
            return;
            case 57:
            this.sendDataText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 58:
            this.receivedDataText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 59:
            this.outputDataText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 60:
            this.clearbtn = ((System.Windows.Controls.Button)(target));
            
            #line 413 "..\..\MainWindow.xaml"
            this.clearbtn.Click += new System.Windows.RoutedEventHandler(this.clearbtn_Click);
            
            #line default
            #line hidden
            return;
            case 61:
            this.messageDataText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 62:
            this.customSendCMDText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 63:
            this.customParserCMDText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 64:
            this.sendCMD = ((System.Windows.Controls.Button)(target));
            
            #line 453 "..\..\MainWindow.xaml"
            this.sendCMD.Click += new System.Windows.RoutedEventHandler(this.sendCMD_Click);
            
            #line default
            #line hidden
            return;
            case 65:
            this.parserCMD = ((System.Windows.Controls.Button)(target));
            
            #line 455 "..\..\MainWindow.xaml"
            this.parserCMD.Click += new System.Windows.RoutedEventHandler(this.parserCMD_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

