﻿using GTE.Command;
using GTE.Encapsulation;
using GTE.InfoModel;
using GTEBatteryLib;
using GTEBatteryTester.Model;
using GTEBatteryTester.Parameter;
using GTEBatteryTesterConfig;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEBatteryTester
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Property
        /// <summary>
        /// 應用程式之設定組態
        /// </summary>
        private static AppSetting mAppSetting { get; set; }
        public static AppSetting AppSetting { get { return mAppSetting; } }
        private static BatteryAPI mBatteryAPI = new BatteryAPI();
        public static BatteryAPI BatteryAPI { get { if (mBatteryAPI == null) { mBatteryAPI = new BatteryAPI(); } return mBatteryAPI; } }

        private bool mIsDisplayHexData;
        #endregion

        public MainWindow()
        {
            InitializeComponent();

            InitializeVersion();
            InitializeAppSetting();
            InitializeBattery();
        }

        #region APP Setting
        private void InitializeVersion()
        {
            Version CurrentVersion = Assembly.GetExecutingAssembly().GetName().Version;
            string ver = CurrentVersion.ToString();
            versionValue.Content = ver;
        }
        /// <summary>
        /// 初始化視窗程式需要之設定檔
        /// </summary>
        private void InitializeAppSetting()
        {
            string appPath = System.AppDomain.CurrentDomain.BaseDirectory;
            string appSettingFilePath = appPath + TesterParameter.APPSETTINGFILEFILE;
            try
            {
                if (File.Exists(appSettingFilePath))
                {
                    using (StreamReader file = File.OpenText(appSettingFilePath))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        mAppSetting = (AppSetting)serializer.Deserialize(file, typeof(AppSetting));
                    }
                }
            }
            catch { }
            if (mAppSetting == null)
            {
                mAppSetting = new AppSetting();

                string[] portList = System.IO.Ports.SerialPort.GetPortNames();
                if (portList != null && portList.Length > 0)
                {
                    mAppSetting.Port = portList[0];
                }

                mAppSetting.BaudRate = mAppSetting.BaudRateList[0];
                mAppSetting.IsAutoBaudRate = true;

                mAppSetting.RetryTime = TesterParameter.DEFAULTRETRYCONNTECTEDTIME;

                SaveAppSetting();
            }
        }
        /// <summary>
        /// 儲存AppSetting資訊至檔案(AppSetting.ap)內
        /// </summary>
        /// <returns></returns>
        public static bool SaveAppSetting()
        {
            bool isSuccess = false;

            string appSettingFilePath = System.AppDomain.CurrentDomain.BaseDirectory + TesterParameter.APPSETTINGFILEFILE;

            if (AppSetting != null)
            {
                try
                {
                    using (FileStream fs = File.Open(appSettingFilePath, FileMode.Create))
                    using (StreamWriter sw = new StreamWriter(fs))
                    using (JsonWriter jw = new JsonTextWriter(sw))
                    {
                        jw.Formatting = Formatting.Indented;

                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(jw, AppSetting);
                        isSuccess = true;
                    }
                }
                catch { }
            }

            return isSuccess;
        }

        /// <summary>
        /// 啟動設定視窗按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingButton_Click(object sender, RoutedEventArgs e)
        {
            SettingWindow settingWindow = new SettingWindow();
            settingWindow.Owner = this;
            settingWindow.ShowDialog();
        }
        #endregion

        #region Battery Setting
        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            ToggleButton bt = (ToggleButton)sender;
            if (bt.IsChecked.Value)
            {
                bt.IsEnabled = false;
                Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                SetConnectStatusToStatusBar("連線中...");
                ThreadPool.QueueUserWorkItem(o => ConnectBattery(sender));
            }
            else
            {
                DisconnectBattery();
                bt.Content = "連線";
            }
        }
        /// <summary>
        /// 連線電池
        /// </summary>
        /// <param name="sender"></param>
        private void ConnectBattery(object sender)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();//引用stopwatch物件
            sw.Reset();//碼表歸零
            sw.Start();//碼表開始計時

            bool isSuccess = false;

            if (AppSetting != null && AppSetting.Port != null)
            {
                for (int i = 0; i < mAppSetting.RetryTime; i++) // 重複嘗試次數
                {
                    if (AppSetting.IsAutoBaudRate)
                    {
                        foreach (int baudRate in AppSetting.BaudRateList)
                        {
                            isSuccess = mBatteryAPI.Connect(AppSetting.Port, baudRate, new EncapsulationType[] { EncapsulationType.MPVersion, EncapsulationType.VVersion });
                            if (isSuccess)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        isSuccess = mBatteryAPI.Connect(AppSetting.Port, AppSetting.BaudRate);
                    }

                    if (isSuccess)
                    {
                        break;
                    }
                }
            }

            sw.Stop();//碼表停止

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    ToggleButton bt = (ToggleButton)sender;

                    if (isSuccess)
                    {
                        SetConnectStatusToStatusBar("已連線");
                        bt.Content = "中斷";

                        RefreshGetCmdPanel();

                        BatteryConnectSetting();

                        SetPortToStatusBar(mBatteryAPI.GetPort());
                        SetBaudRateToStatusBar(mBatteryAPI.GetBaudRate().ToString());


                        switchBatteryMode.Content = "APP Mode";
                        switchBatteryMode.Tag = 0;
                    }
                    else
                    {
                        DisconnectBattery();
                        bt.Content = "連線";
                        MessageBox.Show("Check Port and Baud Rate", "Warning");
                    }

                    switchBatteryMode.IsEnabled = isSuccess;

                    string value = string.Format("嘗試連線時間:{0:0.00}sec", sw.Elapsed.TotalSeconds);
                    SetMessageText(messageDataText, value);

                    Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                    bt.IsEnabled = true;
                }));
        }
        /// <summary>
        /// 中斷電池連線
        /// </summary>
        private void DisconnectBattery()
        {
            mBatteryAPI.Disconnect();

            SetConnectStatusToStatusBar("未連線");

            SetPortToStatusBar("");
            SetBaudRateToStatusBar("");
        }

        /// <summary>
        /// 初始化電池物件
        /// </summary>
        private void InitializeBattery()
        {
            mBatteryAPI.ConnectingDeviceEvent += mBattery_ConnectingDeviceEvent;
            mBatteryAPI.SendDataEvent += mBattery_SendDataEvent;
            mBatteryAPI.ReceivedDataEvent += mBattery_ReceivedDataEvent;
            mBatteryAPI.ReceivedMessageEvent += mBattery_ReceivedMessageEvent;
        }
        #region Message Information
        private void mBattery_ConnectingDeviceEvent(EncapsulationType type, string port, int baudrate, bool isSuccess, string version)
        {
            string value = "";
            if (isSuccess)
            {
                value = string.Format("嘗試連線(成功):Port({0}) BaudRate({1})", port, baudrate);
            }
            else
            {
                value = string.Format("嘗試連線(失敗):Port({0}) BaudRate({1})", port, baudrate);
            }

            switch (type)
            {
                case EncapsulationType.MPVersion:
                    {
                        value = value + " 版本(加密)(" + version + ")";
                        break;
                    }
                case EncapsulationType.VVersion:
                    {
                        value = value + " 版本(非加密)(" + version + ")";
                        break;
                    }
            }

            SetMessageText(messageDataText, value);
        }

        private void hexDataCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;

            if (cb.IsChecked.Value)
            {
                mIsDisplayHexData = true;
            }
            else
            {
                mIsDisplayHexData = false;
            }
        }

        private void cryptoDataCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;

            mBatteryAPI.EnableEncryption(!cb.IsChecked.Value);
        }

        private void BatteryConnectSetting()
        {
            mBatteryAPI.EnableEncryption(!cryptoDataCheckBox.IsChecked.Value);
        }

        private void mBattery_SendDataEvent(string port, BatteryCmd cmdType, object data)
        {
            byte[] item = (byte[])data;

            ParserData(sendDataText, port, cmdType, item);
        }

        private void mBattery_ReceivedDataEvent(string port, BatteryCmd cmdType, object data)
        {
            byte[] item = (byte[])data;

            ParserData(receivedDataText, port, cmdType, item);
        }

        private void ParserData(TextBox textBox, string port, BatteryCmd cmdType, byte[] item)
        {
            if (item != null)
            {
                string value = cmdType.ToString() + "(" + item.Length + ")" + Environment.NewLine;

                if (mIsDisplayHexData)
                {
                    value = value + ">>";
                    foreach (byte d in item)
                    {
                        value = value + string.Format("{0:X02} ", d);
                    }
                }
                else
                {
                    value = value + ">>" + ASCIIEncoding.ASCII.GetString(item);
                }

                SetMessageText(textBox, value);
            }
        }

        private void mBattery_ReceivedMessageEvent(string port, BatteryCmd cmdType, object data)
        {
            //string value = "CMD:" + cmdType.ToString() + Environment.NewLine;
            //value = value + "Data:" + data.ToString();
            //SetMessageText(outputDataText, value);
        }

        private void SetMessageText(TextBox textBox, string value)
        {
            Application.Current.Dispatcher.Invoke(new Action(
                () =>
                {
                    textBox.Text = textBox.Text + value + Environment.NewLine;
                    textBox.ScrollToEnd();
                }
            ));
        }

        private void clearbtn_Click(object sender, RoutedEventArgs e)
        {
            sendDataText.Text = "";
            receivedDataText.Text = "";
            outputDataText.Text = "";
        }
        #endregion


        private void SetConnectStatusToStatusBar(string status)
        {
            statusValue.Content = status;
        }
        #endregion

        #region Cmd Item Setting
        private void ClearGetCmdPaenl()
        {
            foreach (UIElement cmdButton in cmdPanel.Children)
            {
                Button bt = (cmdButton as Button);
                if (bt != null)
                {
                    bt.Click -= GetCmdButton_Click;
                }
            }
            cmdPanel.Children.Clear();
        }

        private void RefreshGetCmdPanel()
        {
            ClearGetCmdPaenl();

            List<BatteryCmdInfo> allCmdInfo = mBatteryAPI.GetAllBatteryCmdInfo();
            if (allCmdInfo != null)
            {
                foreach (BatteryCmdInfo cmdInfo in allCmdInfo)
                {
                    if (cmdInfo.IsParaReadOnly)
                    {
                        Button bt = new Button();
                        bt.Margin = new Thickness(2, 2, 2, 2);
                        bt.Content = string.Format("({0:X02}){1}", cmdInfo.CmdCode, cmdInfo.Cmd.ToString().Replace("CMD", ""));
                        bt.Tag = cmdInfo;
                        bt.Click += GetCmdButton_Click;
                        cmdPanel.Children.Add(bt);
                    }
                }
            }
        }

        private void GetCmdButton_Click(object sender, RoutedEventArgs e)
        {
            BatteryCmdInfo cmdInfo = (BatteryCmdInfo)((Button)sender).Tag;
            if (mBatteryAPI.IsConnect() && cmdInfo != null)
            {
                SendCMD(cmdInfo.Cmd);
            }
        }

        private void setCMD_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null && btn.Tag != null && mBatteryAPI != null && mBatteryAPI.IsConnect())
            {
                try
                {
                    int cmd = Convert.ToInt32(btn.Tag.ToString(), 16);

                    if (btn.Tag.ToString().Equals("91"))
                    {
                        byte param = Convert.ToByte(Convert.ToInt32(setMCUEEPAddress.Text, 16));
                        byte value = Convert.ToByte(Convert.ToInt32(setMCUEEPValue.Text, 16));

                        SendCMD((BatteryCmd)cmd, param, value);
                    }
                    else if (btn.Tag.ToString().Equals("9C") || btn.Tag.ToString().Equals("9D") || btn.Tag.ToString().Equals("9E") || btn.Tag.ToString().Equals("9F"))
                    {
                        byte param = Convert.ToByte(Convert.ToInt32(setPBEEPAddress.Text, 16));
                        byte value = Convert.ToByte(Convert.ToInt32(setPBEEPValue.Text, 16));

                        SendCMD((BatteryCmd)cmd, param, value);
                    }
                    else
                    {
                        byte param = Convert.ToByte(Convert.ToInt32(setDateTimeValue.Text));

                        SendCMD((BatteryCmd)cmd, param);
                    }
                }
                catch
                {
                    MessageBox.Show("Error Value");
                }
            }
        }

        private void getCANCMD_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null && btn.Tag != null && mBatteryAPI != null && mBatteryAPI.IsConnect())
            {
                try
                {
                    if (btn.Tag.ToString().Equals("02"))
                    {
                        SendCMD(BatteryCmd.CMD_GET_CAN_MODE_STATUS, (byte)0x02);
                    }
                    else if (btn.Tag.ToString().Equals("0200"))
                    {
                        SendCMD(BatteryCmd.CMD_SET_CAN_MODE_STATUS, (byte)0x02, (byte)0x00);
                    }
                    else if (btn.Tag.ToString().Equals("0201"))
                    {
                        SendCMD(BatteryCmd.CMD_SET_CAN_MODE_STATUS, (byte)0x02, (byte)0x01);
                    }
                    else if (btn.Tag.ToString().Equals("01"))
                    {
                        SendCMD(BatteryCmd.CMD_GET_CAN_MODE_STATUS, (byte)0x01);
                    }
                    else if (btn.Tag.ToString().Equals("0100"))
                    {
                        SendCMD(BatteryCmd.CMD_SET_CAN_MODE_STATUS, (byte)0x01, (byte)0x00);
                    }
                    else if (btn.Tag.ToString().Equals("01XX"))
                    {
                        byte param = Convert.ToByte(Convert.ToInt32(setCANEngineMins.Text, 16));
                        SendCMD(BatteryCmd.CMD_SET_CAN_MODE_STATUS, (byte)0x01, param);
                    }
                    else if (btn.Tag.ToString().Equals("GET"))
                    {
                        byte param = Convert.ToByte(Convert.ToInt32(setCANParaStatus.Text, 16));
                        SendCMD(BatteryCmd.CMD_GET_CAN_MODE_STATUS, param);
                    }
                    else if (btn.Tag.ToString().Equals("SET"))
                    {
                        byte param = Convert.ToByte(Convert.ToInt32(setCANPara.Text, 16));
                        byte data = Convert.ToByte(Convert.ToInt32(setCANData.Text, 16));
                        SendCMD(BatteryCmd.CMD_SET_CAN_MODE_STATUS, param, data);
                    }
                }
                catch
                {
                    MessageBox.Show("Error Value");
                }
            }
        }

        private void getCMD_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null && btn.Tag != null && mBatteryAPI != null && mBatteryAPI.IsConnect())
            {
                try
                {
                    int cmd = Convert.ToInt32(btn.Tag.ToString(), 16);

                    if (btn.Tag.ToString().Equals("93"))
                    {
                        byte param = Convert.ToByte(Convert.ToInt32(getMCUEEPAddress.Text, 16));

                        SendCMD((BatteryCmd)cmd, param);
                    }
                    else if (btn.Tag.ToString().Equals("97") || btn.Tag.ToString().Equals("98") || btn.Tag.ToString().Equals("99") || btn.Tag.ToString().Equals("9A"))
                    {
                        byte param = Convert.ToByte(Convert.ToInt32(getPBEEPAddress.Text, 16));
                        SendCMD((BatteryCmd)cmd, param);
                    }
                    else
                    {
                        SendCMD((BatteryCmd)cmd);
                    }
                }
                catch
                {
                    MessageBox.Show("Error Value");
                }
            }
        }

        private void setBarcode_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null && mBatteryAPI != null && mBatteryAPI.IsConnect())
            {
                string barcodeText = BarcodeText.Text;
                try
                {
                    Regex regex = new Regex(@"^\d{10}$");
                    bool isInt = regex.IsMatch(barcodeText);

                    if (isInt) // 皆為數字且長度符合Barcode長度
                    {
                        int[] barCodeItem = new int[5];
                        barCodeItem[0] = int.Parse(barcodeText.Substring(0, 1)); // 生產商代碼
                        barCodeItem[1] = int.Parse(barcodeText.Substring(1, 1)); // 產品商代碼
                        barCodeItem[2] = int.Parse(barcodeText.Substring(2, 2)); // 製造年份
                        barCodeItem[3] = int.Parse(barcodeText.Substring(4, 2)); // 製造年週別
                        barCodeItem[4] = int.Parse(barcodeText.Substring(6, 4)); // 序號

                        SendEEPCMD(BatteryEEPAttrCmd.ManufactureInfo, Convert.ToByte((barCodeItem[0] << 4) | barCodeItem[1]));// Set Manufacturer & ProductClass
                        int year = 2000 + barCodeItem[2]; //barcode只有兩千年後兩碼
                        SendEEPCMD(BatteryEEPAttrCmd.ManufactureDate, ISO8601WeekNumToThursday(year, barCodeItem[3]));// Set Manufacture Date

                        SendEEPCMD(BatteryEEPAttrCmd.ManufactureWeekOfYear, Convert.ToByte((barCodeItem[3])));// Set Manufacture Week Of Year

                        SendEEPCMD(BatteryEEPAttrCmd.SerialNumber, barCodeItem[4]);// Set Serial Number       
                    }
                    else
                    {
                        MessageBox.Show("Barcode:Error Format");
                    }
                }
                catch
                {
                    MessageBox.Show("Error Value");
                }
            }
        }

        private void switchBatteryMode_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null && mBatteryAPI != null && mBatteryAPI.IsConnect())
            {
                if (btn.Tag == null)
                {
                    btn.Tag = 0;
                }
                int tag = 0;

                Int32.TryParse(btn.Tag.ToString(), out tag);

                if (tag == 0)
                {
                    System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();//引用stopwatch物件
                    sw.Reset();//碼表歸零
                    sw.Start();//碼表開始計時

                    object value = mBatteryAPI.SendBatteryCmd(BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET, Convert.ToByte(0x83));
                    sw.Stop();

                    OutputResult(value, (byte)BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET, sw.Elapsed.TotalSeconds);

                    if(value != null){
                        bool isSuccess = Convert.ToBoolean(value);
                        if (isSuccess)
                        {
                            btn.Tag = 1;
                            btn.Content = "ENG Mode";
                        }
                    }
                }
                else
                {
                    System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();//引用stopwatch物件
                    sw.Reset();//碼表歸零
                    sw.Start();//碼表開始計時

                    object value = mBatteryAPI.SendBatteryCmd(BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET, Convert.ToByte(0x03));
                    sw.Stop();

                    OutputResult(value, (byte)BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET, sw.Elapsed.TotalSeconds);

                    if (value != null)
                    {
                        bool isSuccess = Convert.ToBoolean(value);
                        if (isSuccess)
                        {
                            btn.Tag = 0;
                            btn.Content = "APP Mode";
                        }
                    }
                }
            }
        }

        private void sendCMD_Click(object sender, RoutedEventArgs e)
        {
            if (mBatteryAPI != null && mBatteryAPI.IsConnect())
            {
                try
                {
                    List<byte> data = ConvertStringToByte(customSendCMDText.Text);
                    if (data.Count > 2)
                    {
                        BatteryCmdInfo cmdInfo = new BatteryCmdInfo(data[2], data[0], false, 500);
                        if (data[2] == 0xf1)
                        {
                            cmdInfo = new BatteryCmdInfo((BatteryCmd)data[2], false, 1500);
                        }

                        cmdInfo.Para = data[1];

                        if (data.Count > 3)
                        {
                            cmdInfo.Info = data.GetRange(3, data.Count - 3).ToArray();
                        }

                        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();//引用stopwatch物件
                        sw.Reset();//碼表歸零
                        sw.Start();//碼表開始計時

                        object value = mBatteryAPI.SendCmd(cmdInfo);

                        sw.Stop();

                        OutputResult(value, cmdInfo.CmdCode, sw.Elapsed.TotalSeconds);
                    }
                    else
                    {
                        MessageBox.Show("Error Cmd");
                    }
                }
                catch
                {
                    MessageBox.Show("Error String");
                }
            }                
        }

        private void parserCMD_Click(object sender, RoutedEventArgs e)
        {
            if (mBatteryAPI != null && mBatteryAPI.IsConnect())
            {
                try
                {
                    List<byte> data = ConvertStringToByte(customParserCMDText.Text);
                    if (data.Count > 1)
                    {
                        OutputResult(mBatteryAPI.ParserData(data.ToArray()), 0, 0);
                    }
                }
                catch
                {
                    MessageBox.Show("Error String");
                }
            }
        }

        private void SendCMD(BatteryCmd cmd)
        {
            if (mBatteryAPI.IsConnect())
            {
                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();//引用stopwatch物件
                sw.Reset();//碼表歸零
                sw.Start();//碼表開始計時

                object value = mBatteryAPI.SendBatteryCmd(cmd);
                sw.Stop();

                OutputResult(value, (byte)cmd, sw.Elapsed.TotalSeconds);
            }
        }

        private void SendCMD(BatteryCmd cmd, byte param)
        {
            if (mBatteryAPI.IsConnect())
            {
                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();//引用stopwatch物件
                sw.Reset();//碼表歸零
                sw.Start();//碼表開始計時

                object value = mBatteryAPI.SendBatteryCmd(cmd, param);
                sw.Stop();

                OutputResult(value, (byte)cmd, sw.Elapsed.TotalSeconds);
            }
        }

        private void SendCMD(BatteryCmd cmd, byte param, object value)
        {
            if (mBatteryAPI.IsConnect())
            {
                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();//引用stopwatch物件
                sw.Reset();//碼表歸零
                sw.Start();//碼表開始計時

                object result = mBatteryAPI.SendBatteryCmd(cmd, param, value, 1);
                sw.Stop();

                OutputResult(result, (byte)cmd, sw.Elapsed.TotalSeconds);
            }
        }

        private void SendEEPCMD(BatteryEEPAttrCmd cmd, object param)
        {
            if (mBatteryAPI.IsConnect())
            {
                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();//引用stopwatch物件
                sw.Reset();//碼表歸零
                sw.Start();//碼表開始計時

                object value = mBatteryAPI.SetMCUEEPCmd(cmd, param);
                sw.Stop();

                OutputResult(value, (byte)cmd, sw.Elapsed.TotalSeconds);
            }
        }

        private void OutputResult(object value, byte cmdCode, double time)
        {
            string result = string.Format("(Code:{0:X02})(Time:{1:0.00}sec)", cmdCode, time)
                        + Environment.NewLine;
            if (value != null)
            {
                Type type = value.GetType();
                result = string.Format("(Code:{0:X02})(Time:{1:0.00}sec)(Type:{2})", cmdCode, time, type.Name)
                    + Environment.NewLine;

                if (cmdCode == 0xF1)
                {
                    byte[] debugData = value as byte[];
                    if (debugData != null)
                    {
                        result = result + System.Text.Encoding.ASCII.GetString(debugData);
                    }
                }
                else
                {
                    switch (type.Name)
                    {
                        case "Boolean":
                        case "String":
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "UInt16":
                        case "UInt32":
                        case "UInt64":
                            {
                                result = result + ">>" + value.ToString();
                                break;
                            }
                        case "Byte":
                            {
                                result = result + ">>" + string.Format("{0:X02} ", value);
                                break;
                            }
                        case "Byte[]":
                            {
                                result = result + ">>";
                                byte[] dataItem = (byte[])value;
                                foreach (byte item in dataItem)
                                {
                                    result = result + string.Format("{0:X02} ", item);
                                }
                                break;
                            }
                        case "Int32[]":
                            {
                                result = result + ">>";
                                Int32[] dataItem = (Int32[])value;
                                foreach (Int32 item in dataItem)
                                {
                                    result = result + string.Format("{0} ", item);
                                }
                                break;
                            }
                        case "Double[]":
                            {
                                result = result + ">>";
                                Double[] dataItem = (Double[])value;
                                foreach (Double item in dataItem)
                                {
                                    result = result + string.Format("{0:0.00} ", item);
                                }
                                break;
                            }
                        case "Int32[][]":
                            {
                                result = result + ">>";
                                Int32[][] dataItem = (Int32[][])value;
                                for (int i = 0; i < dataItem.Length; i++)
                                {
                                    result = result + string.Format("{0} ", i);

                                    for (int j = 0; j < dataItem[i].Length; j++)
                                    {
                                        result = result + string.Format("{0} ", dataItem[i][j]);
                                    }

                                    result = result + Environment.NewLine + "      ";
                                }
                                result.Trim();
                                break;
                            }
                    }
                }
            }

            SetMessageText(outputDataText, result);
        }

        private List<byte> ConvertStringToByte(string text)
        {
            List<byte> data = new List<byte>();

            if (text != null)
            {
                string[] stringData = text.Split(' ');
                foreach (string byteString in stringData)
                {
                    if (!byteString.Equals(""))
                    {
                        data.Add(Convert.ToByte(byteString, 16));
                    }
                }
            }

            return data;
        }
        #endregion

        #region Refresh UI Method
        /// <summary>
        /// 設定Port至StatusBar
        /// </summary>
        /// <param name="Port"></param>
        private void SetPortToStatusBar(string Port)
        {
            portStatusValue.Content = Port;
        }
        /// <summary>
        /// 設定BaudRate至StatusBar
        /// </summary>
        /// <param name="baudRate"></param>
        private void SetBaudRateToStatusBar(string baudRate)
        {
            baudRateStatusValue.Content = baudRate;
        }
        #endregion



        /// <summary>
        /// 轉換至ISO8601標準的週別日期時間
        /// </summary>
        /// <param name="year"></param>
        /// <param name="weekOfYear"></param>
        /// <returns></returns>
        public int[] ISO8601WeekNumToThursday(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1); //當年分第一天
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;//移動至星期三

            DateTime firstThursday = jan1.AddDays(daysOffset);//當周星期四時間
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            DateTime result = firstThursday.AddDays(weekNum * 7);
            int[] date = new int[3];
            date[0] = result.Year;
            date[1] = result.Month;
            date[2] = result.Day;
            return date;
        }


    }
}
