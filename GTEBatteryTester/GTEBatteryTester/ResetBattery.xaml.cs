﻿using GTE.Command;
using GTE.Encapsulation;
using GTEBatteryLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEBatteryTester
{
    /// <summary>
    /// ResetBattery.xaml 的互動邏輯
    /// </summary>
    public partial class ResetBattery : Window
    {

        /// <summary>
        /// 監聽Port列表之定時器
        /// </summary>
        private DispatcherTimer mPortScanTimer;
        private static BatteryAPI mBatteryAPI = new BatteryAPI();
        private string mCurrentPort = "";
        private int[] BaudRateList = new int[] { 115200, 38400 };

        public ResetBattery()
        {
            InitializeComponent();

            InitializeScanPort();
        }

        private void InitializeVersion()
        {
            Version CurrentVersion = Assembly.GetExecutingAssembly().GetName().Version;
            string ver = CurrentVersion.ToString();
            versionValue.Content = ver;
        }

        #region Scan Port
        /// <summary>
        /// 初始化ComPort監聽時間器
        /// </summary>
        private void InitializeScanPort()
        {
            SetSerialPortToComboBox(true);
            mPortScanTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(3) };
            mPortScanTimer.Tick += PortScanTimer_Tick;
            mPortScanTimer.Start();
        }
        /// <summary>
        /// 啟動監聽ComPort狀態之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PortScanTimer_Tick(object sender, EventArgs e)
        {
            SetSerialPortToComboBox(false);
        }
        /// <summary>
        /// 設定Port列表至ComboBox
        /// </summary>
        private void SetSerialPortToComboBox(bool isFirstTime)
        {
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();

            if (ports.Length > 0)
            {
                bool isChanged = false;
                bool existPort = false;
                foreach (object data in portList.Items)
                {
                    if (!ports.Contains(data.ToString()))
                    {
                        isChanged = true;
                    }
                    if (mCurrentPort.Equals(data.ToString()))
                    {
                        existPort = true;
                    }
                }

                if (!existPort || isChanged)
                {
                    portList.Items.Clear();
                    int index = 0;
                    for (int i = 0; i < ports.Length; i++)
                    {
                        if (ports[i].Equals(mCurrentPort))
                        {
                            index = i;
                        }

                        portList.Items.Add(ports[i]);
                    }
                    mCurrentPort = ports[index];
                    portList.SelectedIndex = index;
                }
            } else {
                mCurrentPort = "";
                portList.Items.Clear();
            }
        }
        #endregion

        private void ConfrimButton_Click(object sender, RoutedEventArgs e)
        {
            Button bt = (Button)sender;
            if (mCurrentPort != "")
            {
                bt.IsEnabled = false;
                Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                statusValue.Content = "Waiting";
                ThreadPool.QueueUserWorkItem(o => ConnectBattery(sender));
            }
            else
            {
                MessageBox.Show("Please Select ComPort", "Warning");
            }
        }

        private object SendCMD(BatteryCmd cmd)
        {
            if (mBatteryAPI.IsConnect())
            {
                return mBatteryAPI.SendBatteryCmd(cmd);
            }

            return null;
        }

        /// <summary>
        /// 連線電池
        /// </summary>
        /// <param name="sender"></param>
        private void ConnectBattery(object sender)
        {
            bool isSuccess = false;
            string msg = "Cannot detect Battery";

            for (int i = 0; i < 3; i++) // 重複嘗試次數
            {
                foreach (int baudRate in BaudRateList)
                {
                    isSuccess = mBatteryAPI.Connect(mCurrentPort, baudRate, new EncapsulationType[] { EncapsulationType.MPVersion, EncapsulationType.VVersion });
                    if (isSuccess)
                    {
                        break;
                    }
                }
                if (isSuccess)
                {
                    break;
                }
            }

            if (isSuccess)
            {
                object tmp = SendCMD(BatteryCmd.CMD_ALL_OZ_RESET_SET);
                if (tmp != null)
                {
                    isSuccess = true;
                    msg = "Cannot reset Battery";
                }
                isSuccess = (tmp != null);
                tmp = SendCMD(BatteryCmd.CMD_RESET_BATTERY_MCU);
                if (tmp != null)
                {
                    isSuccess = isSuccess && (tmp != null);
                    msg = "Cannot reset MCU";
                }
            }

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    Button bt = (Button)sender;

                    if (isSuccess)
                    {
                        MessageBox.Show("Success", "Complete");
                    }
                    else
                    {
                        MessageBox.Show(msg, "Warning");
                    }
                    
                    statusValue.Content = "Ready";
                    Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                    bt.IsEnabled = true;
                }));
        }
    }
}
