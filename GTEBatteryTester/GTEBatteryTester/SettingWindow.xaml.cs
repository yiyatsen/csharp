﻿using GTEBatteryTester.Model;
using GTEBatteryTester.Parameter;
using GTEBatteryTesterConfig;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEBatteryTester
{
    /// <summary>
    /// SettingWindow.xaml 的互動邏輯
    /// </summary>
    public partial class SettingWindow : Window
    {
        #region WIN 32
        [DllImport("user32.dll")]
        private extern static int SetWindowLong(IntPtr hwnd, int index, int value);
        [DllImport("user32.dll")]
        private extern static int GetWindowLong(IntPtr hwnd, int index);

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x00080000;

        void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            WindowInteropHelper wih = new WindowInteropHelper(this);
            int style = GetWindowLong(wih.Handle, GWL_STYLE);
            SetWindowLong(wih.Handle, GWL_STYLE, style & ~WS_SYSMENU);
        }
        #endregion

        #region Property
        /// <summary>
        /// 應用程式之設定組態
        /// </summary>
        private AppSetting mAppSetting = MainWindow.AppSetting;
        /// <summary>
        /// 監聽Port列表之定時器
        /// </summary>
        private DispatcherTimer mPortScanTimer;
        #endregion

        public SettingWindow()
        {
            SourceInitialized += MainWindow_SourceInitialized;

            InitializeComponent();

            InitializeAppSetting();
            InitializeScanPort();

            InitializeUI();
        }

        /// <summary>
        /// 初始化視窗程式需要之設定檔
        /// </summary>
        private void InitializeAppSetting()
        {
            if (mAppSetting != null)
            {

            }
        }

        #region UI Method
        /// <summary>
        /// 初始化UI數值
        /// </summary>
        private void InitializeUI()
        {
            SetBaudRateToComboBox();

            if (mAppSetting.IsAutoBaudRate)
            {
                autoBaudRate.IsChecked = true;
                baudRateList.IsEnabled = false;
            }

            retryConnectedTimeTextBox.Text = mAppSetting.RetryTime.ToString();
        }
        #endregion

        #region Scan Port
        /// <summary>
        /// 初始化ComPort監聽時間器
        /// </summary>
        private void InitializeScanPort()
        {
            SetSerialPortToComboBox(true);
            mPortScanTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(TesterParameter.SCANPORTTIME) };
            mPortScanTimer.Tick += PortScanTimer_Tick;
            mPortScanTimer.Start();
        }
        /// <summary>
        /// 啟動監聽ComPort狀態之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PortScanTimer_Tick(object sender, EventArgs e)
        {
            SetSerialPortToComboBox(false);
        }
        /// <summary>
        /// 設定Port列表至ComboBox
        /// </summary>
        private void SetSerialPortToComboBox(bool isFirstTime)
        {
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();
            if (ports != null && ports.Length > 0)
            {
                bool isChanged = false;
                foreach (object data in portList.Items)
                {
                    if (!ports.Contains(data.ToString()))
                    {
                        isChanged = true;
                    }
                }

                if (isChanged || isFirstTime)
                {
                    string port = null;
                    int index = 0;
                    if (portList.SelectedItem != null)
                    {
                        port = portList.SelectedItem.ToString();
                    }

                    portList.Items.Clear();
                    for (int i = 0; i < ports.Length; i++)
                    {
                        if (isFirstTime)
                        {
                            if (ports[i].Equals(mAppSetting.Port))
                            {
                                index = i;
                            }
                        }
                        else
                        {
                            if (ports[i].Equals(port))
                            {
                                index = i;
                            }
                        }

                        portList.Items.Add(ports[i]);
                    }
                    portList.SelectedIndex = index;
                }
            }
        }
        #endregion

        #region Baud Rate
        /// <summary>
        /// 設定BaudRate列表至ComboBox
        /// </summary>
        private void SetBaudRateToComboBox()
        {
            for (int i = 0; i < mAppSetting.BaudRateList.Length; i++)
            {
                baudRateList.Items.Add(mAppSetting.BaudRateList[i]);
                if (mAppSetting.BaudRate == mAppSetting.BaudRateList[i])
                {
                    baudRateList.SelectedIndex = i;
                }
            }
        }

        private void autoBaudRate_Checked(object sender, RoutedEventArgs e)
        {
            if (autoBaudRate.IsChecked.Value)
            {
                baudRateList.IsEnabled = false;
            }
            else
            {
                baudRateList.IsEnabled = true;
            }
        }
        #endregion
        #region RetryConnectedTime Method
        private void retryConnectedTimeTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !onlyNumeric(e.Text);
        }
        private bool onlyNumeric(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that allows numeric input only
            return !regex.IsMatch(text);
        }
        #endregion
        #region App Setting Method
        /// <summary>
        /// 取消設定並且關閉視窗之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 確認設定並且關閉視窗之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfrimButton_Click(object sender, RoutedEventArgs e)
        {
            SaveSettingInfo();
            this.Close();
        }
        /// <summary>
        /// 儲存修改的設定值
        /// </summary>
        private void SaveSettingInfo()
        {
            if (portList.SelectedItem != null)
            {
                mAppSetting.Port = portList.SelectedItem.ToString();
            }

            if (baudRateList.SelectedItem != null)
            {
                mAppSetting.BaudRate = int.Parse(baudRateList.SelectedItem.ToString());
            }

            mAppSetting.IsAutoBaudRate = autoBaudRate.IsChecked.Value;

            mAppSetting.RetryTime = int.Parse(retryConnectedTimeTextBox.Text.ToString());

            MainWindow.SaveAppSetting();
        }
        #endregion

    }
}
