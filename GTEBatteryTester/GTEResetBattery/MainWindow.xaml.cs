﻿using GTE.Command;
using GTE.Encapsulation;
using GTEBatteryLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEResetBattery
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    { 
        /// <summary>
        /// 監聽Port列表之定時器
        /// </summary>
        private DispatcherTimer mPortScanTimer;
        private static BatteryAPI mBatteryAPI = new BatteryAPI();
        private int[] BaudRateList = new int[] { 115200, 38400 };
        private string mCurrentPort = "";

        public MainWindow()
        {
            InitializeComponent();
            InitializeScanPort();
        }

        #region Scan Port
        /// <summary>
        /// 初始化ComPort監聽時間器
        /// </summary>
        private void InitializeScanPort()
        {
            SetSerialPortToComboBox();
            mPortScanTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(3) };
            mPortScanTimer.Tick += PortScanTimer_Tick;
            mPortScanTimer.Start();
        }
        /// <summary>
        /// 啟動監聽ComPort狀態之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PortScanTimer_Tick(object sender, EventArgs e)
        {
            SetSerialPortToComboBox();
        }
        /// <summary>
        /// 設定Port列表至ComboBox
        /// </summary>
        private void SetSerialPortToComboBox()
        {
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();

            if (ports.Length > 0)
            {
                bool isChanged = false;
                if (portList.Items.Count != ports.Length)
                {
                    isChanged = true;
                }
                else
                {
                    foreach (object data in portList.Items)
                    {
                        if (!ports.Contains(data.ToString()))
                        {
                            isChanged = true;
                        }
                    }
                }

                if (isChanged)
                {
                    int index = 0;
                    string tmpPort = "";
                    if (portList.SelectedItem != null)
                    {
                        tmpPort = portList.SelectedItem.ToString();
                    }

                    portList.Items.Clear();
                    for (int i = 0; i < ports.Length; i++)
                    {
                        if (ports[i].Equals(tmpPort))
                        {
                            index = i;
                        }

                        portList.Items.Add(ports[i]);
                    }
                    portList.SelectedIndex = index;
                }
            }
            else
            {
                portList.Items.Clear();
            }
        }
        #endregion

        private void ConfrimButton_Click(object sender, RoutedEventArgs e)
        {
            Button bt = (Button)sender;
            if (portList.SelectedItem != null && portList.SelectedItem.ToString() != "")
            {
                mCurrentPort = portList.SelectedItem.ToString();
                bt.IsEnabled = false;
                Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                statusValue.Content = "Waiting";
                ThreadPool.QueueUserWorkItem(o => ConnectBattery(sender));
            }
            else
            {
                MessageBox.Show("Please Select ComPort", "Warning");
            }
        }

        private object SendCMD(BatteryCmd cmd, byte param, object value)
        {
            if (mBatteryAPI.IsConnect())
            {
                return mBatteryAPI.SendBatteryCmd(cmd, param, value, 1);
            }

            return null;
        }

        private object SendCMD(BatteryCmd cmd, byte param)
        {
            if (mBatteryAPI.IsConnect())
            {
                return mBatteryAPI.SendBatteryCmd(cmd, param);
            }

            return null;
        }

        private object SendCMD(BatteryCmd cmd)
        {
            if (mBatteryAPI.IsConnect())
            {
                return mBatteryAPI.SendBatteryCmd(cmd);
            }

            return null;
        }

        /// <summary>
        /// 連線電池
        /// </summary>
        /// <param name="sender"></param>
        private void ConnectBattery(object sender)
        {
            bool isSuccess = false;
            string msg = "Cannot detect Battery";

            for (int i = 0; i < 3; i++) // 重複嘗試次數
            {
                foreach (int baudRate in BaudRateList)
                {
                    isSuccess = mBatteryAPI.Connect(mCurrentPort, baudRate, new EncapsulationType[] { EncapsulationType.MPVersion, EncapsulationType.VVersion });
                    if (isSuccess)
                    {
                        break;
                    }
                }
                if (isSuccess)
                {
                    break;
                }
            }

            if (isSuccess)
            {
                byte oz1New = 0;
                byte oz2New = 0;
                object oz1 = SendCMD(BatteryCmd.CMD_OZ1_BYTE_OP_GET, 0x0E);
                object oz2 = SendCMD(BatteryCmd.CMD_OZ2_BYTE_OP_GET, 0x0E);
                if (oz1 != null)
                {
                    oz1New = (byte)((byte)oz1 + 1);
                    SendCMD(BatteryCmd.CMD_OZ1_BYTE_OP_SET, 0x0E, oz1New);
                }
                if (oz2 != null)
                {
                    oz2New = (byte)((byte)oz2 + 1);
                    SendCMD(BatteryCmd.CMD_OZ2_BYTE_OP_SET, 0x0E, oz2New);
                }
                Thread.Sleep(300);
                object tmp = SendCMD(BatteryCmd.CMD_ALL_OZ_RESET_SET);
                if (tmp == null)
                {
                    msg = "Cannot reset Battery";
                }
                isSuccess = (tmp != null);
                Thread.Sleep(300);
                tmp = SendCMD(BatteryCmd.CMD_RESET_BATTERY_MCU);
                if (tmp == null)
                {
                    msg = "Cannot reset MCU";
                }
                isSuccess = isSuccess && (tmp != null);
                Thread.Sleep(500);
                object oz1N = SendCMD(BatteryCmd.CMD_OZ1_BYTE_OP_GET, 0x0E);
                if ((oz1N == null || (byte)oz1N == oz1New))
                {
                    isSuccess = false;
                    msg = "Cannot revise PB1";
                }
                object oz2N = SendCMD(BatteryCmd.CMD_OZ2_BYTE_OP_GET, 0x0E);
                if ((oz2N == null || (byte)oz2N == oz2New))
                {
                    isSuccess =false;
                    msg = "Cannot revise PB2";
                }
            }

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    Button bt = (Button)sender;

                    if (isSuccess)
                    {
                        MessageBox.Show("Success", "Complete");
                    }
                    else
                    {
                        MessageBox.Show(msg, "Warning");
                    }

                    statusValue.Content = "Ready";
                    Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                    bt.IsEnabled = true;
                }));
        }

    }
}
