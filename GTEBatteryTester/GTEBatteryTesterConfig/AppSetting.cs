﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEBatteryTesterConfig
{
    public class AppSetting
    {
        public string Port { get; set; }
        public int BaudRate { get; set; }
        public bool IsAutoBaudRate { get; set; }
        public int[] BaudRateList { get; set; }

        public int RetryTime { get; set; }

        public AppSetting()
        {
            BaudRateList = new int[] { 115200, 38400 };
        }
    }
}
