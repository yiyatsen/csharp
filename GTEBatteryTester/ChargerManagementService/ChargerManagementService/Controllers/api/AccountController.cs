﻿using ChargerManagementService.Models;
using ChargerManagementService.Models.DBConfigure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChargerManagementService.Controllers.api
{
    public class AccountController : ApiController
    {
        GotechDBContext db = new GotechDBContext();

        private UserLoginInfoModel NewUserLoginInfoModel(string account, string userName, string loginCode, UserRoleCode userRoleCode, ErrorMessageModel errorMessageModel)
        {
            UserLoginInfoModel value = new UserLoginInfoModel();

            value.Account = account;
            value.UserName = userName;
            value.LoginCode = loginCode;
            value.UserRoleCode = userRoleCode;
            value.ErrorCode = errorMessageModel;

            return value;
        }

        [HttpPost]
        public UserLoginInfoModel Login(UserLoginModel userLoginModel)
        {
            UserLoginInfoModel value = new UserLoginInfoModel();

            if (this.ModelState.IsValid)
            {
                var session = System.Web.HttpContext.Current.Session;
                if (userLoginModel.captcha.Equals(session["Captcha"].ToString()))
                {
                    /// 1. 帳密正確且尚未登入紀錄 -> 成功登入
                    /// 2. 帳密正確已經有登入紀錄 -> 此帳號已經登入
                    /// 3. 帳密不正確 -> 帳密輸入錯誤
                    var account = (from a in db.UserInfo
                                   where a.Account.Equals(userLoginModel.email) && a.Password.Equals(userLoginModel.password)
                                   select a).SingleOrDefault();

                    if (account != null)
                    {
                        value = NewUserLoginInfoModel(account.Account, account.UserName, "Login Code", account.UserRoleCode, null);
                        session["Captcha"] = null; // Rmove Session Data
                    }
                    else
                    {
                        value = NewUserLoginInfoModel(null, null, null, UserRoleCode.None, new ErrorMessageModel()
                        {
                            title = "Email or Password Error",
                            subtitle = "Please try again"
                        });
                    }
                }
                else
                {
                    value = NewUserLoginInfoModel(null, null, null, UserRoleCode.None, new ErrorMessageModel()
                    {
                        title = "Captcha Error",
                        subtitle = "Please try again"
                    });
                }
            }
            else
            {
                value = NewUserLoginInfoModel(null, null, null, UserRoleCode.None, new ErrorMessageModel()
                {
                    title = this.ModelState.Values.First().Errors[0].ErrorMessage,
                    subtitle = "Please try again"
                });
            }

            return value;
        }

        [HttpPost]
        public UserLoginInfoModel Logout(UserLoginInfoModel value)
        {

            value = NewUserLoginInfoModel(null, null, null, UserRoleCode.None, null);

            return value;
        }

        [HttpPost]
        public UserLoginInfoModel CreateAccount(UserCreateModel userCreateModel)
        {
            UserLoginInfoModel value = new UserLoginInfoModel();

            if (this.ModelState.IsValid)
            {
                if (userCreateModel.password.Equals(userCreateModel.repassword))
                {
                    var session = System.Web.HttpContext.Current.Session;
                    if (userCreateModel.captcha.Equals(session["Captcha"].ToString()))
                    {
                        UserInfo userInfo = (from a in db.UserInfo
                                             where userCreateModel.email.ToLower() == a.Account || userCreateModel.username.ToLower() == a.UserName
                                                   select a).SingleOrDefault();
                        if (userInfo == null)
                        {
                            try
                            {
                                db.UserInfo.Add(new UserInfo
                                {
                                    Account = userCreateModel.email,
                                    UserName = userCreateModel.username,
                                    Password = userCreateModel.password,
                                    Address = userCreateModel.address,
                                    Phone = userCreateModel.phone,
                                    IsEnable = true,
                                    UserRoleCode = Models.UserRoleCode.NonActivityUser
                                });
                                db.SaveChanges();

                                session["Captcha"] = null;// Rmove Session Data
                            }
                            catch (Exception ex)
                            {
                                // save to file

                                value = NewUserLoginInfoModel(null, null, null, UserRoleCode.None, new ErrorMessageModel()
                                {
                                    title = "Database Error",
                                    subtitle = "Please try again"
                                });
                            }
                        }
                        else
                        {
                            value = NewUserLoginInfoModel(null, null, null, UserRoleCode.None, new ErrorMessageModel()
                            {
                                title = "Email or User Name Exist",
                                subtitle = "Please try again"
                            });
                        }
                    }
                    else
                    {
                        value = NewUserLoginInfoModel(null, null, null, UserRoleCode.None, new ErrorMessageModel()
                        {
                            title = "Captcha Error",
                            subtitle = "Please try again"
                        });
                    }
                }
                else
                {
                    value = NewUserLoginInfoModel(null, null, null, UserRoleCode.None, new ErrorMessageModel()
                    {
                        title = "Password Error",
                        subtitle = "Please try again"
                    });
                }
            }
            else
            {
                value = NewUserLoginInfoModel(null, null, null, UserRoleCode.None, new ErrorMessageModel()
                {
                    title = this.ModelState.Values.First().Errors[0].ErrorMessage,
                    subtitle = "Please try again"
                });
            }

            return value;
        }

        [HttpGet]
        public HttpResponseMessage RefreshCaptcha()
        {
            var rand = new Random((int)DateTime.Now.Ticks); 
            //generate new question 
            int a = rand.Next(10, 99); 
            int b = rand.Next(0, 9); 
            var captcha = string.Format("{0} + {1} = ?", a, b);

            var session = System.Web.HttpContext.Current.Session;
            session["Captcha"] = (a + b).ToString();

            byte[] fileData = null; 
 
            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(130, 30)) 
            using (var gfx = Graphics.FromImage((Image)bmp))
            { 
                gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));
 
                //add question 
                gfx.DrawString(captcha, new Font("Tahoma", 15), Brushes.Gray, 2, 3); 
 
                //render as Jpeg 
                bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg); 
                fileData = mem.GetBuffer();
            }

            HttpResponseMessage Response = new HttpResponseMessage(HttpStatusCode.OK);

            if (fileData == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            //S3:Set Response contents and MediaTypeHeaderValue
            Response.Content = new ByteArrayContent(fileData);
            Response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/jpeg");

            return Response;
        }
    }
}
