﻿using ChargerManagementService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChargerManagementService.Controllers.api
{
    public class InfoController : ApiController
    {
        [HttpGet]
        public IList<ChargeDeviceDTO> ChargerInfo()
        {
            List<ChargeDeviceDTO> list = new List<ChargeDeviceDTO>();

            list.Add(new ChargeDeviceDTO()
            {
                DeviceId = 1,
                DeviceType = "4840",

                Name = "Test Station",
                Latitude = 25.06087,
                Longitude = 121.6558738

            });

            list.Add(new ChargeDeviceDTO()
            {
                DeviceId = 2,
                DeviceType = "4840",

                Name = "Test2 Station",
                Latitude = 25.062562,
                Longitude = 121.64665

            });

            return list;
        }

        [HttpGet]
        public IList<MarkerInfo> IconInfo()
        {
            List<MarkerInfo> list = new List<MarkerInfo>();

            list.Add(new MarkerInfo()
            {
                Name = "UserLocation",
                IconPath = "/Content/images/Here.png",
                IconWidth = 48,
                IconHeight = 48
            });

            list.Add(new MarkerInfo()
            {
                Name = "Charger",
                IconPath = "/Content/images/EXN.png",
                IconWidth = 64,
                IconHeight = 64
            });

            return list;
        }
    }
}
