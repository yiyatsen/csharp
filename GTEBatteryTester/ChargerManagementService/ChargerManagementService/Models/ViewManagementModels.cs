﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ChargerManagementService.Models
{
    [Table("MarkerInfo")]
    public class MarkerInfo
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MarkerId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string IconPath { get; set; }
        [Required, DefaultValue(48)]
        public int IconWidth { get; set; }
        [Required, DefaultValue(48)]
        public int IconHeight { get; set; }
    }


    [Table("PlaceInfo")]
    public class PlaceInfo
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PlaceId { get; set; }
        [Required]
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public bool IsEnable { get; set; }
        public bool IsRemove { get; set; }

        public string Description { get; set; }

        public int UserId { get; set; }
        public virtual UserInfo UserInfo { get; set; }

    }
}