﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ChargerManagementService.Models
{
    /// <summary>
    /// Defined User Role
    /// </summary>
    public enum UserRoleCode
    {
        None = 0,
        NonActivityUser = 1, // for 認證
        User = 2,  // 會員 (可以"租?"借或是買"賣?")
        Manager = 4, // => monitor everything but cannot editor for GoTech
        Cooperator = 8, // 營運商

        Administrator = 16 // => Only one, System administrator for GoTech
    }
    /// <summary>
    ///  Defined relationship between users
    /// </summary>
    public enum UserRelationshipCode
    {
        SubManager = 1 //無法買賣
    }

    [Table("UserInfo")]
    public class UserInfo
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        //[Required, EmailAddress]
        public string Account { get; set; } // = Email
        [Required]
        public string Password { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Address { get; set; }
        [DefaultValue(UserRoleCode.NonActivityUser)]
        public UserRoleCode UserRoleCode { get; set; }

        public string Description { get; set; }

        public long RegisterDateTime { get; set; }

        public bool IsEnable { get; set; } // Did the user can be use
        public bool IsRemove { get; set; } // Did the user be removed 
        public int UserRoleCodeMaxNum { get; set; }

        [JsonIgnore]
        public virtual IList<UserItem> OwnerItems { get; set; }
        [JsonIgnore]
        public virtual IList<UserItem> HolderItems { get; set; }
        [JsonIgnore]
        public virtual IList<UserRelationship> Actives { get; set; }

        //public int? PassiveUserId { get; set; }
        [JsonIgnore]
        public virtual UserRelationship Passive { get; set; }

        public UserInfo()
        {
            OwnerItems = new List<UserItem>();
            HolderItems = new List<UserItem>();

            Actives = new List<UserRelationship>();
        }
    }

    [Table("UserRelationship")]
    public class UserRelationship
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RelationshipId { get; set; }

        //[ForeignKey("UserInfo"), Column(Order = 1)]
        public int ActiveUserId { get; set; }
        public virtual UserInfo Active { get; set; }

        //[ForeignKey("UserInfo"), Column(Order = 1)]
        //public int PassiveUserId { get; set; }
        public virtual UserInfo Passive { get; set; }

        public string Description { get; set; }

        public UserRelationshipCode Relationship { get; set; }
    }

    [Table("UserItem")]
    public abstract class UserItem
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ItemId { get; set; }

        //[ForeignKey("UserInfo"), Column(Order = 1)]
        public int OwnerUserId { get; set; }
        public virtual UserInfo Owner { get; set; }

        public int? HolderUserId { get; set; }
        public virtual UserInfo Holder { get; set; }

        [Required]
        public long ValidFromDateTime { get; set; }
        [Required]
        public long ValidThruDateTime { get; set; }

        public bool IsEnable { get; set; }
        public bool IsRemove { get; set; }

        public string Description { get; set; }
    }
    /// <summary>
    /// 
    /// 商業邏輯
    /// 1. 使用者擁有的UserDeviceCode原則上是依據一個Device則有一組UserCode
    /// 2. 也可依據不同UserRole預設不同的Code數量
    /// 3. 若需要增加Code數量可以交易購買增加
    /// 
    ///  防止Code被複製
    ///  1. 每次租借DeviceCode需要重新產生
    /// </summary>
    [Table("UserDeviceCode")]
    public class UserDeviceCode : UserItem
    {
        [Required, StringLength(200), Index(IsUnique = true)]
        public string DeviceCode { get; set; }

        public IList<ChargeDeviceActionRecord> ChargeDeviceActionRecords { get; set; }
        public IList<BatteryDeviceActionRecord> BatteryDeviceActionRecords { get; set; }

        public UserDeviceCode()
        {
            ChargeDeviceActionRecords = new List<ChargeDeviceActionRecord>();
            BatteryDeviceActionRecords = new List<BatteryDeviceActionRecord>();
        }
    }

    [Table("UserChargeDevice")]
    public class UserChargeDevice : UserItem
    {
        [ForeignKey("ChargeDeviceInfo"), Column(Order = 2), Index]
        public int DeviceId { get; set; }
        [ForeignKey("ChargeDeviceInfo"), Column(Order = 3), Index]
        public int DeviceTypeId { get; set; }

        public virtual ChargeDeviceInfo ChargeDeviceInfo { get; set; }
    }

    [Table("UserBatteryDevice")]
    public class UserBatteryDevice : UserItem
    {
        [ForeignKey("BatteryDeviceInfo"), Column(Order = 2), Index]
        public int DeviceId { get; set; }
        [ForeignKey("BatteryDeviceInfo"), Column(Order = 3), Index]
        public int DeviceTypeId { get; set; }

        public virtual BatteryDeviceInfo BatteryDeviceInfo { get; set; }
    }
}