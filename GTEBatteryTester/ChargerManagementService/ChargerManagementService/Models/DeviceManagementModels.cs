﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ChargerManagementService.Models
{
    public enum DeviceStyle
    {
        None,
        Battery,
        Charger,
        ChargeStation
    }

    public enum DeviceActionStyle
    {
        None = 0,
        StartChargeDevice = 1, // 啟動Charger/Charge Station
        ReportChargeDevice = 2, // 回報Charger/Charge Station狀況
        InsertToChargeDevice = 3, // 電池插入
        RemoveFromChargeDevice = 4, // 電池拔出
        StartChargeBattery = 5, // 開始充電
        EndChargeBattery = 6 // 結束充電
    }

    [Table("DeviceType")]
    public abstract class DeviceType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DeviceTypeId { get; set; }
        [Required]
        public string DeviceTypeName { get; set; }

        public DeviceStyle DeviceStyle { get; set; }
        public string Description { get; set; }
    }
    [Table("ChargeDeviceType")]
    public class ChargeDeviceType : DeviceType
    {
        [JsonIgnore]
        public virtual IList<ChargeDeviceInfo> ChargeDeviceInfos { get; set; }

        public ChargeDeviceType()
        {
            ChargeDeviceInfos = new List<ChargeDeviceInfo>();
        }
    }
    [Table("BatteryDeviceType")]
    public class BatteryDeviceType : DeviceType
    {
        // type information
        public int DesignCapacity { get; set; }

        [JsonIgnore]
        public virtual IList<BatteryDeviceInfo> BatteryDeviceInfos { get; set; }

        public BatteryDeviceType()
        {
            BatteryDeviceInfos = new List<BatteryDeviceInfo>();
        }
    }
    [Table("DeviceInfo")]
    public abstract class DeviceInfo
    {
        [Key, Column(Order = 0)]
        public int DeviceId { get; set; }

        public bool IsRemove { get; set; }
        public bool IsEnable { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        // Device Register Info
        public long RegisterDateTime { get; set; }
        public long ManufacturedDateTime { get; set; }
        public string OSVersion { get; set; }
        public string APVersion { get; set; }
    }

    [Table("ChargeDeviceInfo")]
    public class ChargeDeviceInfo : DeviceInfo
    {
        // Charge Device Information ...
        public bool IsOnline { get; set; }
        public long LastStartTime { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Address { get; set; }

        public int SlotNumber { get; set; }

        [ForeignKey("ChargeDeviceType"), Column(Order = 1), Key]
        public int DeviceTypeId { get; set; }
        public virtual ChargeDeviceType ChargeDeviceType { get; set; }

        [JsonIgnore]
        public virtual IList<ChargeDeviceRecord> ChargeDeviceRecords { get; set; }
        [JsonIgnore]
        public virtual IList<BatteryChargeRecord> BatteryChargeRecords { get; set; }
        [JsonIgnore]
        public virtual IList<ChargeDeviceActionRecord> ChargeDeviceActionRecords { get; set; }

        public int? ItemId { get; set; }
        public virtual UserChargeDevice UserChargeDevice { get; set; }

        public ChargeDeviceInfo()
        {
            ChargeDeviceRecords = new List<ChargeDeviceRecord>();
            BatteryChargeRecords = new List<BatteryChargeRecord>();

            ChargeDeviceActionRecords = new List<ChargeDeviceActionRecord>();
        }
    }

    [Table("BatteryDeviceInfo")]
    public class BatteryDeviceInfo : DeviceInfo
    {
        [ForeignKey("BatteryDeviceType"), Column(Order = 1), Key]
        public int DeviceTypeId { get; set; }
        public virtual BatteryDeviceType BatteryDeviceType { get; set; }
        [JsonIgnore]
        public virtual IList<BatteryDeviceRecord> BatteryDeviceRecords { get; set; }
        [JsonIgnore]
        public virtual IList<BatteryChargeRecord> BatteryChargeRecords { get; set; }
        [JsonIgnore]
        public virtual IList<BatteryDeviceActionRecord> BatteryDeviceActionRecords { get; set; }

        public int? ItemId { get; set; }
        public virtual UserBatteryDevice UserBatteryDevice { get; set; }

        public BatteryDeviceInfo()
        {
            BatteryDeviceRecords = new List<BatteryDeviceRecord>();
            BatteryChargeRecords = new List<BatteryChargeRecord>();

            BatteryDeviceActionRecords = new List<BatteryDeviceActionRecord>();
        }
    }

    [Table("DeviceRecord")]
    public abstract class DeviceRecord
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DeviceRecordId { get; set; }

        public long UpdateDateTime { get; set; }

        public string Description { get; set; }
    }

    [Table("ChargeDeviceRecord")]
    public class ChargeDeviceRecord : DeviceRecord
    {
        [ForeignKey("ChargeDeviceInfo"), Column(Order = 1)]
        public int DeviceId { get; set; }
        [ForeignKey("ChargeDeviceInfo"), Column(Order = 2)]
        public int DeviceTypeId { get; set; }

        public virtual ChargeDeviceInfo ChargeDeviceInfo { get; set; }

        public virtual IList<ChargeSlotRecord> ChargerSlotRecords { get; set; }

        public ChargeDeviceRecord()
        {
            ChargerSlotRecords = new List<ChargeSlotRecord>();
        }
    }

    [Table("ChargeSlotRecord")]
    public class ChargeSlotRecord
    {
        [Key, Column(Order = 0)]
        public int SlotId { get; set; }
        [ForeignKey("ChargeDeviceRecord"), Key, Column(Order = 1)]
        public int ChargeDeviceRecordId { get; set; }

        public virtual ChargeDeviceRecord ChargeDeviceRecord { get; set; }

        // ? => System.Nullable<T>
        //public int? BatteryDeviceRecordId { get; set; }
        public virtual BatteryDeviceRecord BatteryDeviceRecord { get; set; }

        // maybe remove
        public bool IsBatteryExist { get; set; }
        public string Description { get; set; }
    }

    [Table("BatteryDeviceRecord")]
    public class BatteryDeviceRecord : DeviceRecord
    {
        [ForeignKey("BatteryDeviceInfo"), Column(Order = 1)]
        public int DeviceId { get; set; }
        [ForeignKey("BatteryDeviceInfo"), Column(Order = 2)]
        public int DeviceTypeId { get; set; }

        public virtual BatteryDeviceInfo BatteryDeviceInfo { get; set; }

        //[ForeignKey("ChargeSlotRecord"), Column(Order = 3)]
        //public int SlotId { get; set; }
        //[ForeignKey("ChargeSlotRecord"), Column(Order = 4)]
        //public int ChargeDeviceRecordId { get; set; }
        [JsonIgnore]
        public virtual ChargeSlotRecord ChargeSlotRecord { get; set; }

        public bool IsCharging { get; set; } //是否正在充電
        public int SOC { get; set; }
        public int BatteryLife { get; set; }
        public int FastChargeCount { get; set; }
        public int HomeChargerCount { get; set; }
        public int ExchangedCount { get; set; }
        public int CycleCount { get; set; }
        public int ChargedCapacity { get; set; }
    }

    /// <summary>
    /// Record information between charge battery of start and charge battery of end at every time
    /// </summary>
    [Table("BatteryChargeRecord")]
    public class BatteryChargeRecord
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ChargeRecordId { get; set; }

        public bool IsDone { get; set; }

        // Which Charge Device is used
        [ForeignKey("ChargeDeviceInfo"), Column(Order = 1)]
        public int ChargeDeviceId { get; set; }
        [ForeignKey("ChargeDeviceInfo"), Column(Order = 2)]
        public int ChargeDeviceTypeId { get; set; }
        public virtual ChargeDeviceInfo ChargeDeviceInfo { get; set; }
        // Which Battery is charging
        [ForeignKey("BatteryDeviceInfo"), Column(Order = 3)]
        public int BatteryDeviceId { get; set; }
        [ForeignKey("BatteryDeviceInfo"), Column(Order = 4)]
        public int BatteryDeviceTypeId { get; set; }
        public virtual BatteryDeviceInfo BatteryDeviceInfo { get; set; }

        public long CCCVChangedTime { get; set; }
        // data Information
        public long ChargeStartTime { get; set; }
        public long ChargeEndTime { get; set; }

        public string CellVolBefore { get; set; }
        public string CellVolAfter { get; set; }

        public string Description { get; set; }
    }

    [Table("ChargeDeviceActionRecord")]
    public class ChargeDeviceActionRecord
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ActionRecordId { get; set; }

        public long UpdateDateTime { get; set; }
        public DeviceActionStyle DeviceActionStyle { get; set; }
        public string Description { get; set; }

        //[ForeignKey("ChargeDeviceInfo"), Column(Order = 1)]
        public int ChargeDeviceId { get; set; }
        //[ForeignKey("ChargeDeviceInfo"), Column(Order = 2)]
        public int ChargeDeviceTypeId { get; set; }
        public virtual ChargeDeviceInfo ChargeDeviceInfo { get; set; }

        public string ChargeCode { get; set; }
        public int? ChargeDeviceCodeId { get; set; }
        public virtual UserDeviceCode ChargeDeviceCode { get; set; }
    }
    [Table("BatteryDeviceActionRecord")]
    public class BatteryDeviceActionRecord : ChargeDeviceActionRecord
    {
        //[ForeignKey("ChargeDeviceInfo"), Column(Order = 3)]
        public int BatteryDeviceId { get; set; }
        //[ForeignKey("ChargeDeviceInfo"), Column(Order = 4)]
        public int BatteryDeviceTypeId { get; set; }
        public virtual BatteryDeviceInfo BatteryDeviceInfo { get; set; }

        public string BatteryCode { get; set; }
        public int? BatteryDeviceCodeId { get; set; }
        public virtual UserDeviceCode BatteryDeviceCode { get; set; }
    }
}