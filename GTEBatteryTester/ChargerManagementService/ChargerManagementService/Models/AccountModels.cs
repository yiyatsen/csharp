﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChargerManagementService.Models
{
    public class UserLoginModel
    {
        [Required]
        //[EmailAddress]
        public string email { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public string captcha { get; set; }
    }

    public class ErrorMessageModel
    {
        public string title { get; set; }
        public string subtitle { get; set; }
    }

    public class UserCreateModel
    {
        [Required]
        //[EmailAddress]
        public string email { get; set; }
        [Required]
        public string username { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public string repassword { get; set; }
        [Required]
        public string phone { get; set; }
        [Required]
        public string address { get; set; }
        [Required]
        public string captcha { get; set; }
    }

    public class UserLoginInfoModel
    {
        public string Account { get; set; }
        public string UserName { get; set; }
        public string LoginCode { get; set; }
        [DefaultValue(UserRoleCode.None)]
        public UserRoleCode UserRoleCode { get; set; }

        public ErrorMessageModel ErrorCode { get; set; }
    }
}