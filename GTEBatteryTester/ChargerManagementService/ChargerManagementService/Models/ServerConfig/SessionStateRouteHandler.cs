﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace ChargerManagementService.Models.ServerConfig
{
    public class SessionStateRouteHandler: IRouteHandler
   {
       IHttpHandler IRouteHandler.GetHttpHandler(RequestContext requestContext)
       {
           return new SessionableControllerHandler(requestContext.RouteData);
       }
   }
}