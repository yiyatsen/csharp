﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargerManagementService.Models
{

    public class UserInfoDTO
    {
        public string Account { get; set; } // = Email
        public string Password { get; set; }
        public string UserName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public UserRoleCode UserRoleCode { get; set; }
    }

    public class ChargeDeviceDTO
    {
        public int DeviceId { get; set; }
        public string DeviceType { get; set; }

        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

    }
}