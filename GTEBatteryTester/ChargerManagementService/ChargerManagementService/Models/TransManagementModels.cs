﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ChargerManagementService.Models
{
    public enum TransStyle
    {
        Rent = 0,
        Sale = 1,
        Transfer = 2
    }

    public abstract class ITrans
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TransId { get; set; }

        public bool IsConfirm { get; set; }
        public long ConfirmDateTime { get; set; }
        public long TransValidDateTime { get; set; }

        public TransStyle TransStyle { get; set; }

        //[ForeignKey("UserInfo"), Column(Order = 1), Index]
        public int ProviderUserId { get; set; }

        public virtual UserInfo Provider { get; set; }

        public bool IsProviderAgree { get; set; }
        public long ProviderAgreeDateTime { get; set; }


        //[ForeignKey("UserInfo"), Column(Order = 2), Index]
        public int ReceiverUserId { get; set; }

        public virtual UserInfo Receiver { get; set; }

        public bool IsReceiverAgree { get; set; }
        public long ReceiverAgreeDateTime { get; set; }


        [Required]
        public long ValidFromDateTime { get; set; }
        [Required]
        public long ValidThruDateTime { get; set; }

        public string Description { get; set; }
    }

    public class DeviceCodeTrans : ITrans
    {
        [ForeignKey("UserDeviceCode"), Column(Order = 3)]
        public int ItemId { get; set; }

        public virtual UserDeviceCode UserDeviceCode { get; set; }
    }

    public class ChargeDeviceTrans : ITrans
    {
        [ForeignKey("UserChargeDevice"), Column(Order = 3)]
        public int ItemId { get; set; }

        public virtual UserChargeDevice UserChargeDevice { get; set; }
    }

    public class BatteryDeviceTrans : ITrans
    {
        [ForeignKey("UserBatteryDevice"), Column(Order = 3)]
        public int ItemId { get; set; }

        public virtual UserBatteryDevice UserBatteryDevice { get; set; }
    }
}