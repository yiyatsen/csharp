﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ChargerManagementService.Models.DBConfigure
{
    public class DeviceTypeMapping : EntityTypeConfiguration<DeviceType>
    {
        public DeviceTypeMapping()
        {
            this.ToTable("DeviceType");
            this.HasKey(x => x.DeviceTypeId);
            this.Property(x => x.DeviceTypeId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.DeviceTypeName)
                .IsRequired();
        }
    }
    public class ChargeDeviceTypeMapping : EntityTypeConfiguration<ChargeDeviceType>
    {
        public ChargeDeviceTypeMapping()
        {
            this.ToTable("ChargeDeviceType");
        }
    }
    public class BatteryDeviceTypeMapping : EntityTypeConfiguration<BatteryDeviceType>
    {
        public BatteryDeviceTypeMapping()
        {
            this.ToTable("BatteryDeviceType");
        }
    }
    public class ChargeDeviceInfoMapping : EntityTypeConfiguration<ChargeDeviceInfo>
    {
        public ChargeDeviceInfoMapping()
        {
            // 先繼承後建立Table名稱
            this.Map(x => x.MapInheritedProperties());
            this.ToTable("ChargeDeviceInfo");
            this.HasKey(x => new { x.DeviceId, x.DeviceTypeId });
            this.HasRequired(x => x.ChargeDeviceType)
                .WithMany(x => x.ChargeDeviceInfos)
                .HasForeignKey(x => x.DeviceTypeId);
            // Attribute of user relationship
            this.HasOptional(x => x.UserChargeDevice)
                .WithMany()
                .HasForeignKey(x => x.ItemId);
        }
    }
    public class BatteryDeviceInfoMapping : EntityTypeConfiguration<BatteryDeviceInfo>
    {
        public BatteryDeviceInfoMapping()
        {
            this.Map(x => x.MapInheritedProperties());
            this.ToTable("BatteryDeviceInfo");
            this.HasKey(x => new { x.DeviceId, x.DeviceTypeId });
            this.HasRequired(x => x.BatteryDeviceType)
                .WithMany(x => x.BatteryDeviceInfos)
                .HasForeignKey(x => x.DeviceTypeId);
            // Attribute of user relationship
            this.HasOptional(x => x.UserBatteryDevice)
                .WithMany()
                .HasForeignKey(x => x.ItemId);
        }
    }
    public class ChargeDeviceRecordMapping : EntityTypeConfiguration<ChargeDeviceRecord>
    {
        public ChargeDeviceRecordMapping()
        {
            this.Map(x => x.MapInheritedProperties());
            this.ToTable("ChargeDeviceRecord");
            this.HasKey(x => x.DeviceRecordId);
            this.Property(x => x.DeviceRecordId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.HasRequired(x => x.ChargeDeviceInfo)
                .WithMany(x => x.ChargeDeviceRecords)
                .HasForeignKey(x => new { x.DeviceId, x.DeviceTypeId });
        }
    }
    public class ChargeSlotRecordMapping : EntityTypeConfiguration<ChargeSlotRecord>
    {
        public ChargeSlotRecordMapping()
        {
            this.ToTable("ChargeSlotRecord");
            this.HasKey(x => new { x.SlotId, x.ChargeDeviceRecordId });
            this.Property(x => x.SlotId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.HasRequired(x => x.ChargeDeviceRecord)
                .WithMany(x => x.ChargerSlotRecords)
                .HasForeignKey(x => x.ChargeDeviceRecordId);
            //this.HasOptional(x => x.BatteryDeviceRecord)
            //    .WithMany()
            //    .HasForeignKey(x => x.BatteryDeviceRecordId);
        }
    }
    public class BatteryDeviceRecordMapping : EntityTypeConfiguration<BatteryDeviceRecord>
    {
        public BatteryDeviceRecordMapping()
        {
            this.Map(x => x.MapInheritedProperties());
            this.ToTable("BatteryDeviceRecord");
            this.HasKey(x => x.DeviceRecordId);
            this.Property(x => x.DeviceRecordId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.HasRequired(x => x.BatteryDeviceInfo)
                .WithMany(x => x.BatteryDeviceRecords)
                .HasForeignKey(x => new { x.DeviceId, x.DeviceTypeId });
            this.HasRequired(x => x.ChargeSlotRecord)
                .WithOptional(x => x.BatteryDeviceRecord);
        }
    }
    public class BatteryChargeRecordMapping : EntityTypeConfiguration<BatteryChargeRecord>
    {
        public BatteryChargeRecordMapping()
        {
            this.ToTable("BatteryChargeRecord");
            this.HasKey(x => x.ChargeRecordId);
            this.Property(x => x.ChargeRecordId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.HasRequired(x => x.ChargeDeviceInfo)
                .WithMany(x => x.BatteryChargeRecords)
                .HasForeignKey(x => new { x.ChargeDeviceId, x.ChargeDeviceTypeId });
            this.HasRequired(x => x.BatteryDeviceInfo)
                .WithMany(x => x.BatteryChargeRecords)
                .HasForeignKey(x => new { x.BatteryDeviceId, x.BatteryDeviceTypeId });
        }
    }

    public class ChargeDeviceActionRecordMapping : EntityTypeConfiguration<ChargeDeviceActionRecord>
    {
        public ChargeDeviceActionRecordMapping()
        {
            this.ToTable("ChargeDeviceActionRecord");
            this.HasKey(x => x.ActionRecordId);
            this.Property(x => x.ActionRecordId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.HasRequired(x => x.ChargeDeviceInfo)
                .WithMany(x => x.ChargeDeviceActionRecords)
                .HasForeignKey(x => new { x.ChargeDeviceId, x.ChargeDeviceTypeId });

            this.HasOptional(x => x.ChargeDeviceCode)
                .WithMany(x => x.ChargeDeviceActionRecords)
                .HasForeignKey(x => x.ChargeDeviceCodeId);
        }
    }

    public class BatteryDeviceActionRecordMapping : EntityTypeConfiguration<BatteryDeviceActionRecord>
    {
        public BatteryDeviceActionRecordMapping()
        {
            this.ToTable("BatteryDeviceActionRecord");

            this.HasRequired(x => x.BatteryDeviceInfo)
                .WithMany(x => x.BatteryDeviceActionRecords)
                .HasForeignKey(x => new { x.BatteryDeviceId, x.BatteryDeviceTypeId });

            this.HasOptional(x => x.BatteryDeviceCode)
                .WithMany(x => x.BatteryDeviceActionRecords)
                .HasForeignKey(x => x.BatteryDeviceCodeId);
        }
    }


    public class UserInfoMapping : EntityTypeConfiguration<UserInfo>
    {
        public UserInfoMapping()
        {
            this.ToTable("UserInfo");
            this.HasKey(x => x.UserId);
            this.Property(x => x.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.Account).IsRequired();
            this.Property(x => x.Password).IsRequired();
            this.Property(x => x.UserName).IsRequired();
            this.Property(x => x.Phone).IsRequired();
            this.Property(x => x.Address).IsRequired();

            //this.HasOptional(x => x.Passive)
            //    .WithMany()
            //    .HasForeignKey(x => x.PassiveUserId);
        }
    }

    public class UserRelationshipMapping : EntityTypeConfiguration<UserRelationship>
    {
        public UserRelationshipMapping()
        {
            this.ToTable("UserRelationship");
            this.HasKey(x => x.RelationshipId);
            this.Property(x => x.RelationshipId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.HasRequired(x => x.Active)
                .WithMany(x => x.Actives)
                .HasForeignKey(x => x.ActiveUserId);
            this.HasRequired(x => x.Passive)
                .WithOptional(x => x.Passive);
        }
    }

    public class UserItemMapping : EntityTypeConfiguration<UserItem>
    {
        public UserItemMapping()
        {
            this.ToTable("UserItem");
            this.HasKey(x => x.ItemId);
            this.Property(x => x.ItemId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.HasRequired(x => x.Owner)
                .WithMany(x => x.OwnerItems)
                .HasForeignKey(x => x.OwnerUserId);

            this.HasOptional(x => x.Holder)
                .WithMany(x => x.HolderItems)
                .HasForeignKey(x => x.HolderUserId);
        }
    }

    public class UserDeviceCodeMapping : EntityTypeConfiguration<UserDeviceCode>
    {
        public UserDeviceCodeMapping()
        {
            this.ToTable("UserDeviceCode");
            this.Property(x => x.DeviceCode)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnAnnotation("Index", 
                new System.Data.Entity.Infrastructure.Annotations.IndexAnnotation(new IndexAttribute() { IsUnique = true }));
        }
    }
    public class UserChargeDeviceMapping : EntityTypeConfiguration<UserChargeDevice>
    {
        public UserChargeDeviceMapping()
        {
            this.ToTable("UserChargeDevice");
            this.HasRequired(x => x.ChargeDeviceInfo)
                .WithMany()
                .HasForeignKey(x => new { x.DeviceId, x.DeviceTypeId });
        }
    }
    public class UserBatteryDeviceMapping : EntityTypeConfiguration<UserBatteryDevice>
    {
        public UserBatteryDeviceMapping()
        {
            this.ToTable("UserBatteryDevice");
            this.HasRequired(x => x.BatteryDeviceInfo)
                .WithMany()
                .HasForeignKey(x => new{ x.DeviceId, x.DeviceTypeId});
        }
    }
}