﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ChargerManagementService.Models.DBConfigure
{
    public class GotechDBContext : DbContext
    {
        public virtual DbSet<DeviceType> DeviceType { get; set; }
        public virtual DbSet<ChargeDeviceType> ChargeDeviceType { get; set; }
        public virtual DbSet<BatteryDeviceType> BatteryDeviceType { get; set; }

        public virtual DbSet<ChargeDeviceInfo> ChargeDeviceInfo { get; set; }
        public virtual DbSet<BatteryDeviceInfo> BatteryDeviceInfo { get; set; }

        public virtual DbSet<ChargeDeviceRecord> ChargeDeviceRecord { get; set; }
        public virtual DbSet<BatteryDeviceRecord> BatteryDeviceRecord { get; set; }
        public virtual DbSet<ChargeSlotRecord> ChargeSlotRecord { get; set; }

        public virtual DbSet<BatteryChargeRecord> BatteryChargeRecord { get; set; }

        public virtual DbSet<ChargeDeviceActionRecord> ChargeDeviceActionRecord { get; set; }
        public virtual DbSet<BatteryDeviceActionRecord> BatteryDeviceActionRecord { get; set; }

        public virtual DbSet<UserInfo> UserInfo { get; set; }
        public virtual DbSet<UserRelationship> UserRelationship { get; set; }
        public virtual DbSet<UserItem> UserItem { get; set; }
        public virtual DbSet<UserDeviceCode> UserDeviceCode { get; set; }
        public virtual DbSet<UserChargeDevice> UserChargeDevice { get; set; }
        public virtual DbSet<UserBatteryDevice> UserBatteryDevice { get; set; }

        public GotechDBContext()
            : base("name=GotechDBContext")
        {
            //this.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.DeviceTypeMapping());
            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.ChargeDeviceTypeMapping());
            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.BatteryDeviceTypeMapping());

            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.ChargeDeviceInfoMapping());
            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.BatteryDeviceInfoMapping());

            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.ChargeDeviceRecordMapping());
            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.ChargeSlotRecordMapping());
            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.BatteryDeviceRecordMapping());

            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.BatteryChargeRecordMapping());

            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.ChargeDeviceActionRecordMapping());
            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.BatteryDeviceActionRecordMapping());


            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.UserInfoMapping());
            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.UserRelationshipMapping());

            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.UserItemMapping());
            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.UserDeviceCodeMapping());
            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.UserChargeDeviceMapping());
            modelBuilder.Configurations.Add(new ChargerManagementService.Models.DBConfigure.UserBatteryDeviceMapping());
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}