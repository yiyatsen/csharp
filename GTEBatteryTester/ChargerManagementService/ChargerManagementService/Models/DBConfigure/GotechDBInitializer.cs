﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ChargerManagementService.Models.DBConfigure
{
    public class GotechDBInitializer : CreateDatabaseIfNotExists<GotechDBContext>
    {
        protected override void Seed(GotechDBContext context)
        {
            var data = new List<UserInfo>{
                new UserInfo{
                    Account = "gtenergy.developer@gmail.com",
                    Password = "1234",
                    Address = "新北市汐止區大同路二段175號11樓之1",
                    UserName = "gtenergy",
                    Phone = "02-7708-0899",
                    IsEnable = true,
                    UserRoleCode = Models.UserRoleCode.Administrator
                },
                new UserInfo{
                    Account = "cpr",
                    UserName = "cpr",
                    Password = "1234",
                    UserRoleCode = UserRoleCode.Cooperator,
                    Address = "新北市汐止區大同路二段175號11樓之1",
                    Phone = "02-7708-0899",
                    IsEnable = true
                },               
                new UserInfo{
                    Account = "user",
                    UserName = "user",
                    Password = "1234",
                    UserRoleCode = UserRoleCode.User,
                    Address = "新北市汐止區大同路二段175號11樓之1",
                    Phone = "02-7708-0899",
                    IsEnable = true
                }
            };
            data.ForEach(d => context.UserInfo.Add(d));

            base.Seed(context);
        }
    }
}