﻿app.controller('mainCtrl', function ($scope, $state, $ionicLoading, $ionicPopup, $ionicHistory, userInfoService, apiService) {

    // Local Variable
    $scope.userLoginModel = {
        email: '',
        password: '',
        captcha:''
    };
    $scope.userCreateModel = {
        email: '',
        username: '',
        password: '',
        repassword: '',
        phone: '',
        address: '',
        captcha: ''
    };

    // Refresh Captcha
    $scope.refreshCaptchaUrl = function () {
        $scope.captchaUrl = apiService.refreshCaptchaUrl();
    };

    // Signin function
    $scope.signIn = function (data) {
        $ionicLoading.show({
            template: 'Loading...'
        });

        userInfoService.login(data, callbackForSignIn);
    };
    // Signout function
    $scope.signOut = function () {
        $ionicLoading.show({
            template: 'Loading...'
        });

        userInfoService.logout(callbackForSignIn);
    };
    // Create Account function
    $scope.createAccount = function (data) {
        $ionicLoading.show({
            template: 'Loading...'
        });

        userInfoService.createAccount(data, callbackForSignIn);
    };


    // Initialize
    $scope.captchaUrl = apiService.refreshCaptchaUrl();
    switchSignInState();

    function callbackForSignIn(result) {
        $ionicLoading.hide();
        if (result !== null) {
            switch (result.functionIndex) {
                case 1:
                    // Sign-In
                    if (result.isSuccess) {
                        switchSignInState();

                        resetUserLoginModel();
                        
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go('menu.map');

                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: result.title,
                            template: result.subTitle
                        });
                    }
                    break;
                case 2:
                    // Sign-Out
                    if (result.isSuccess) {
                        switchSignInState();

                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go('menu.map');

                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: result.title,
                            template: result.subTitle
                        });
                    }
                    break;
                case 3: {
                     // Sign-In
                    if (result.isSuccess) {
                        switchSignInState();

                        resetUserCreateModel();
                        
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go('menu.map');

                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: result.title,
                            template: result.subTitle
                        });
                    }
                    break;
                }
                default:
                    break;
            }
        }
    }

    function resetUserLoginModel() {
        $scope.userLoginModel.email = '';
        $scope.userLoginModel.password = '';
        $scope.userLoginModel.captcha = '';
    }
    function resetUserCreateModel() {
        $scope.userCreateModel.email = '';
        $scope.userCreateModel.username = '';
        $scope.userCreateModel.password = '';
        $scope.userCreateModel.repassword = '';
        $scope.userCreateModel.phone = '';
        $scope.userCreateModel.address = '';
        $scope.userCreateModel.captcha = '';
    }

    function switchSignInState() {
        $scope.isSignIn = userInfoService.isLogged();
        $scope.userRoleCode = userInfoService.getUserRoleCode();
    }
});


app.controller('mapCtrl', function ($scope, $state, $compile, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup, apiService) {

    var param = {
        DEFAULTLOCATION: [25.062562, 121.64665],
        USERLOCATION: 'UserLocation',
        LATITUDE: 'Latitude',
        LONGITUDE: 'Longitude',
        CLUSTERMARKER: 'ClusterMarker',
        MARKERS: 'Markers',
        ICON: 'Icon',
        ICONPATH: 'IconPath',
        ICONWIDTH: 'IconWidth',
        ICONHEIGHT: 'IconHeight',
        CHARGER: "Charger"
    };

    var googleMap;
    var mapInfowindow;
    var markerList;
    var watchUserLocationFlagId;

    // Stop Drag Content
    $ionicSideMenuDelegate.canDragContent(false);
    // Initialize Map
    $scope.$on('mapInitialized', function (event, map) {
        $ionicLoading.show({
            template: 'Loading...'
        });

        // Set Map
        googleMap = map;

        // Set Marker Icon
        markerList = apiService.getIconInfo();
        if (markerList != null) {
            for (item in markerList) {
                markerList[item][param.ICON] =
                    new google.maps.MarkerImage(
                    markerList[item][param.ICONPATH],
                    null,
                    null,
                    null,
                    new google.maps.Size(markerList[item][param.ICONWIDTH], markerList[item][param.ICONHEIGHT]));

                if (item === param.USERLOCATION) {
                    // User Location Markers
                    markerList[item][param.MARKERS] = new google.maps.Marker({
                        map: googleMap
                    });
                    markerList[item][param.MARKERS].setIcon(markerList[item][param.ICON]);
                }
                else {
                    markerList[item][param.CLUSTERMARKER]
                        = new MarkerClusterer(googleMap, []);
                }
            }
        }

        // Set Infowindow
        mapInfowindow = new google.maps.InfoWindow();
        // Hide infowindow when click map
        google.maps.event.addListener(googleMap, 'click', function () {
            if (mapInfowindow) {
                mapInfowindow.close();
            }
        });

        // init UserLocation
        $scope.goUserLocation();
        // init Marker
        $scope.refreshChargerMarker();

        $ionicLoading.hide();
    });
    // Refresh user location
    $scope.goUserLocation = function (isAutoRun) {

        if (watchUserLocationFlagId != null) {
            navigator.geolocation.clearWatch(watchUserLocationFlagId);
            watchUserLocationFlagId = null;
        }

        var options = { enableHighAccuracy: true, timeout: 5000, maximumAge: 0, desiredAccuracy: 0, frequency: 1000 };

        watchUserLocationFlagId = navigator.geolocation.watchPosition(
            function success(position) {
                var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                markerList[param.USERLOCATION][param.MARKERS].setPosition(pos);

                if (!isAutoRun) {
                    isAutoRun = 'trigger';
                    // Move to position
                    googleMap.setCenter(pos);
                }
            },
            function error(err) {
                if (!isAutoRun) {
                    var pos = new google.maps.LatLng(param.DEFAULTLOCATION[0], param.DEFAULTLOCATION[1]);
                    googleMap.setCenter(pos);
                }
            },
            options);
    };
    // Refresh Charger Marker
    $scope.refreshChargerMarker = function () {
        $ionicLoading.show({
            template: 'Loading...'
        });

        apiService.updateChargerInfo(callbackChargerInfo);
    };
    function callbackChargerInfo(data) {

        if (data !== null) {
            clearClusterMarkers(markerList[param.CHARGER]);

            var markers = [];
            for (item in data) {
                var pos = new google.maps.LatLng(data[item][param.LATITUDE], data[item][param.LONGITUDE]);

                var marker = new google.maps.Marker({
                    map: googleMap,
                    position: pos,
                    icon: markerList[param.CHARGER][param.ICON]
                });

                google.maps.event.addListener(
                  marker,
                  'click',
                  (function (marker, scope) {
                      return function () {
                          googleMap.panTo(marker.getPosition());

                          //var content = '<div id="infowindow_content" ng-include src="\'/templates/infowindow.html\'"></div>';
                          //scope.latLng = [];
                          //scope.latLng = [marker.getPosition().A, marker.getPosition().F];
                          //var compiled = $compile(content)(scope);
                          //scope.$apply();
                          //mapInfowindow.setContent(compiled[0].innerHTML);
                          //mapInfowindow.open(googleMap, marker);

                          // content of infowindow must be a file
                          mapInfowindow.setContent('<h4>' + marker.getPosition().A + ',' + marker.getPosition().F + '</h4>');
                          mapInfowindow.open(googleMap, marker);
                      };//return fn()
                  })(marker, $scope)
                );//addListener

                markers.push(marker);
            }

            addClusterMarkers(markerList[param.CHARGER], markers);
        } else {
            $scope.showAlert = function () {
                var alertPopup = $ionicPopup.alert({
                    title: 'Cannot connect to server',
                    template: 'Please try again'
                });
            };
        }

        $ionicLoading.hide();
    }

    function clearClusterMarkers(item) {
        item[param.CLUSTERMARKER].clearMarkers();
    };
    function addClusterMarkers(item, data) {
        item[param.CLUSTERMARKER].addMarkers(data, false);
    };

    $scope.showBatteryNotifyPopup = function () {
        $scope.data = {};

        var notifyPopup = $ionicPopup.show({
            scope: $scope,
            title: 'Charge Notification',
            templateUrl: 'templates/batteryNotifyPopup.html',
            buttons: [
                {
                    text: 'Cancel',
                    onTap: function (e) { return true; }
                },
                {
                    text: '<b>Save</b>',
                    type: 'button-positive',
                    onTap: function (e) {
                        return $scope.data.batteryCaptcha;
                    }
                },
            ]
        });

        notifyPopup.then(function (res) {
            if (res != true) {
                // TO DO
                alert(res);
            }
        });
    };

});


app.controller('signinCtrl', function ($scope) {
    
});

app.controller('createaccountCtrl', function ($scope) {

});

app.controller('forgotpasswordCtrl', function ($scope, $state, $ionicLoading) {

});
