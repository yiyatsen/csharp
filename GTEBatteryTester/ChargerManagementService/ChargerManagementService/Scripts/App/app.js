﻿var app = angular.module('app', ['ionic', 'ngMap']);

// Set APP Constant
app.constant('version', '0.0.1');
app.constant('apiConfigProvider', {
    METHODGET: 'GET',
    METHODPOST: 'POST',
    CONNECTTIMEOUT: 3000,
    PATHLOGIN: '/api/Account/Login',
    PATHLOGOUT: '/api/Account/Logout',
    PATHCREATEACCOUNT: '/api/Account/CreateAccount',
    PATHFORGOTPASSWORD: '/api/Account/ForgotPassword',
    PATHREFRESHCAPTCHA: '/api/Account/RefreshCaptcha',
    PATHICONINFO: '/api/Info/IconInfo',
    PATHCHARGERINFO: '/api/Info/ChargerInfo'
});

app.config(function ($stateProvider, $urlRouterProvider) {

    var baseTab = {
        name: 'menu',
        url: "/menu",
        abstract: true,
        templateUrl: "templates/menu.html"
    };
    var mapTab = {
        name: 'menu.map',
        url: "/map",
        views: {
            'menuContent': {
                templateUrl: "Page/Map",
                controller: 'mapCtrl'
            }
        },
        permission: {
            compareCode: 0,
            userRoleCode: 999
        }
    };
    var chargeNotificationTab = {
        name: 'menu.chargenotification',
        url: "/chargenotification",
        views: {
            'menuContent': {
                templateUrl: "Page/ChargeNotification",
                controller: 'mainCtrl'
            }
        },
        permission: {
            compareCode: 1, 
            userRoleCode: 1
        }
    };
    var signinTab = {
        name: 'menu.signin',
        url: "/signin",
        views: {
            'menuContent': {
                templateUrl: "Page/SignIn",
                controller: 'signinCtrl'
            }
        },
        permission: {
            compareCode: -1,
            userRoleCode: 0
        }
    };

    var createAccountTab = {
        name: 'menu.createaccount',
        url: "/createaccount",
        views: {
            'menuContent': {
                templateUrl: "Page/CreateAccount",
                controller: 'createaccountCtrl'
            }
        },
        permission: {
            compareCode: -1,
            userRoleCode: 0
        }
    };

    var forgotPasswordTab = {
        name: 'menu.forgotpassword',
        url: "/forgotpassword",
        views: {
            'menuContent': {
                templateUrl: "Page/ForgotPassword",
                controller: 'forgotpasswordCtrl'
            }
        },
        permission: {
            compareCode: -1,
            userRoleCode: 0
        }
    };

    // Set up the states
    $stateProvider
        .state(baseTab)
        .state(mapTab)
        .state(chargeNotificationTab)
        .state(signinTab)
        .state(createAccountTab)
        .state(forgotPasswordTab);

    // For any unmatched url, redirect to /default
    $urlRouterProvider.otherwise("/menu/map");
});

// Initialize app run
app.run(function ($rootScope, $state, $ionicHistory, apiService, userInfoService) {

    // Check User Information cookie
    // userInfoService.login("test", "1234");
    apiService.updateIconInfo();

    $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {

        if (toState.permission.userRoleCode == -1 && toState.permission.userRoleCode < userInfoService.getUserRoleCode()) {
            e.preventDefault();
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('menu.map');
        }

        if (toState.permission.userRoleCode == 1 && toState.permission.userRoleCode > userInfoService.getUserRoleCode()) {
            e.preventDefault();
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('menu.map');
        }
    });
});