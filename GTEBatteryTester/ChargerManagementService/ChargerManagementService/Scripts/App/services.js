﻿// Provide Account management Service
app.service('userInfoService', function ($http, apiConfigProvider) {

    var self = this;
    var isConnection = false; // Only one request for account api

    // User Information Object
    var userInfo = {
        isLogged: false,
        account: null,
        userName: null,
        userRoleCode: 0,
        loginCode: null
    };

    self.isLogged = function () {
        return userInfo.isLogged;
    }

    self.getUserName = function () {
        return userInfo.userName;
    };

    self.getUserRoleCode = function () {
        return userInfo.userRoleCode;
    };

    self.login = function (value, callbackFunction) {
        if (!userInfo.isLogged && !isConnection && value !== null) {

            if (value.email === '') {
                if (callbackFunction) {
                    callbackFunction(new responseResult(1, false, 'Email cannot empty', 'Please try again'));
                }
            } else if (value.password === '') {
                if (callbackFunction) {
                    callbackFunction(new responseResult(1, false, 'Password cannot empty', 'Please try again'));
                }
            } else if (value.captcha === '') {
                if (callbackFunction) {
                    callbackFunction(new responseResult(1, false, 'Captcha cannot empty', 'Please try again'));
                }
            } else {

                switchConnectionState();

                var req = new requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHLOGIN, value);

                $http(req)
                    .success(function (data, status, headers, config) {

                        var result = new responseResult(1, false, null, null);

                        if (data.ErrorCode !== null) {
                            userInfo.isLogged = false;

                            result.title = data.ErrorCode.title;
                            result.subTitle = data.ErrorCode.subtitle;
                        } else {
                            userInfo.isLogged = true;
                            result.isSuccess = true;
                        }

                        userInfo.account = data.Account;
                        userInfo.userName = data.UserName;
                        userInfo.userRoleCode = data.UserRoleCode;
                        userInfo.loginCode = data.LoginCode;

                        switchConnectionState();
                        if (callbackFunction) {
                            callbackFunction(result);
                        }
                    })
                    .error(function (data, status, headers, config) {

                        userInfo.isLogged = false;
                        userInfo.userName = null;
                        userInfo.userRoleCode = 0;
                        userInfo.loginCode = null;

                        switchConnectionState();

                        if (callbackFunction) {
                            callbackFunction(new responseResult(1, false, 'Connect Error', 'Please try again'));
                        }
                    });
            }

        } else {
            if (callbackFunction) {
                callbackFunction(null);
            }
        }
    };

    self.logout = function (callbackFunction) {
        if (userInfo.isLogged && !isConnection) {
            switchConnectionState();

            var req = new requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHLOGOUT, userInfo);

            $http(req)
                .success(function (data, status, headers, config) {

                    userInfo.isLogged = false;
                    userInfo.account = data.Account;
                    userInfo.userName = data.UserName;
                    userInfo.userRoleCode = data.UserRoleCode;
                    userInfo.loginCode = data.LoginCode;

                    switchConnectionState();

                    if (callbackFunction) {
                        callbackFunction(new responseResult(2 , true, null, null));
                    }
                })
                .error(function (data, status, headers, config) {
                    switchConnectionState();
                    if (callbackFunction) {
                        callbackFunction(new responseResult(2, false, 'Connect Error', 'Please try again'));
                    }
                });
        } else {
            if (callbackFunction) {
                callbackFunction(null);
            }
        }
    };

    self.createAccount = function (value, callbackFunction) {
        if (!userInfo.isLogged && !isConnection) {

            if (value.email === '') {
                if (callbackFunction) {
                    callbackFunction(new responseResult(3, false, 'Email cannot empty', 'Please try again'));
                }
            } else if (value.username === '') {
                if (callbackFunction) {
                    callbackFunction(new responseResult(3, false, 'User name cannot empty', 'Please try again'));
                }
            } else if (value.password === '') {
                if (callbackFunction) {
                    callbackFunction(new responseResult(3, false, 'Password cannot empty', 'Please try again'));
                }
            } else if (value.repassword === '') {
                if (callbackFunction) {
                    callbackFunction(new responseResult(3, false, 'Password cannot empty', 'Please try again'));
                }
            } else if (value.phone === '') {
                if (callbackFunction) {
                    callbackFunction(new responseResult(3, false, 'Phone cannot empty', 'Please try again'));
                }
            } else if (value.address === '') {
                if (callbackFunction) {
                    callbackFunction(new responseResult(3, false, 'Address cannot empty', 'Please try again'));
                }
            } else if (value.captcha === '') {
                if (callbackFunction) {
                    callbackFunction(new responseResult(3, false, 'Captcha cannot empty', 'Please try again'));
                }
            } else if (value.password !== value.repassword) {
                if (callbackFunction) {
                    callbackFunction(new responseResult(3, false, 'Password not Equal', 'Please try again'));
                }
            } else {
                switchConnectionState();

                var req = new requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHCREATEACCOUNT, value);

                $http(req)
                    .success(function (data, status, headers, config) {

                        var result = new responseResult(3, false, null, null);

                        if (data.ErrorCode !== null) {
                            userInfo.isLogged = false;

                            result.title = data.ErrorCode.title;
                            result.subTitle = data.ErrorCode.subtitle;
                        } else {
                            userInfo.isLogged = true;
                            result.isSuccess = true;
                        }

                        userInfo.account = data.Account;
                        userInfo.userName = data.UserName;
                        userInfo.userRoleCode = data.UserRoleCode;
                        userInfo.loginCode = data.LoginCode;

                        switchConnectionState();
                        if (callbackFunction) {
                            callbackFunction(result);
                        }
                    })
                    .error(function (data, status, headers, config) {

                        userInfo.isLogged = false;
                        userInfo.userName = null;
                        userInfo.userRoleCode = 0;
                        userInfo.loginCode = null;

                        switchConnectionState();

                        if (callbackFunction) {
                            callbackFunction(new responseResult(3, false, 'Connect Error', 'Please try again'));
                        }
                    });
            }
        } else {
            if (callbackFunction) {
                callbackFunction(null);
            }
        }
    };

    function requestFormat(rqMethod, rqURL, rqData) {
        this.method = rqMethod;
        this.url = rqURL;
        if (rqData != null) {
            this.data = rqData;
        }
        this.timeout = apiConfigProvider.CONNECTTIMEOUT;
    }

    function responseResult(rqIndex, rqSuccess, rqTitle, rqSubTitle) {
        this.functionIndex = rqIndex;
        this.isSuccess = rqSuccess;
        this.title = rqTitle;
        this.subTitle = rqSubTitle;
    }

    function switchConnectionState() {
        isConnection = !isConnection;
    }
});

// Provide api Service
app.service('apiService', function ($http, apiConfigProvider) {
    var self = this;
    var isIconSuccess = false;
    var iconList;
    var chargerList;

    self.refreshCaptchaUrl = function () {
        return apiConfigProvider.PATHREFRESHCAPTCHA + '?' + new Date().getTime();
    };

    self.getIconInfo = function () {
        return iconList;
    };

    self.updateIconInfo = function (callbackFunction) {

        if (!isIconSuccess) {
            var req = {
                method: apiConfigProvider.METHODGET,
                url: apiConfigProvider.PATHICONINFO,
                timeout: apiConfigProvider.CONNECTTIMEOUT
            };

            $http(req)
                .success(function (data, status, headers, config) {

                    if (data != null) {
                        iconList = new Array();

                        for (item in data) {
                            iconList[data[item]["Name"]] = data[item];
                        }
                    }

                    isIconSuccess = true;
                    if (callbackFunction) {
                        callbackFunction(isIconSuccess);
                    }
                })
                .error(function (data, status, headers, config) {
                    isIconSuccess = false;
                    if (callbackFunction) {
                        callbackFunction(isIconSuccess);
                    }
                });
        }
    };

    self.getChargerInfo = function () {
        return chargerList;
    };
    self.updateChargerInfo = function (callbackFunction) {

        var req = {
            method: apiConfigProvider.METHODGET,
            url: apiConfigProvider.PATHCHARGERINFO,
            timeout: apiConfigProvider.CONNECTTIMEOUT
        };

        $http(req)
            .success(function (data, status, headers, config) {
                // Setting data display
                if (data !== null) {
                    chargerList = data;
                }

                if (callbackFunction) {
                    callbackFunction(data);
                }
            }).error(function (data, status, headers, config) {
                if (callbackFunction) {
                    callbackFunction(null);
                }
            });
    };
});
