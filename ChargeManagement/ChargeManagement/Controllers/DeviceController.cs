﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChargeManagement.Controllers
{
    public class DeviceController : ApiController
    {

        [HttpGet]
        public ChargeManagement.Models.DeviceMessageFormat Login()
        {
            ChargeManagement.Models.DeviceMessageFormat value = new Models.DeviceMessageFormat() { 
            StationId = "0001",
            Message = "TEst"};


            return value;
        }

        [HttpPost]
        public ChargeManagement.Models.DeviceMessageFormat DeviceUpdate(ChargeManagement.Models.DeviceMessageFormat value)
        {
            // 1. 先判斷數值是否正確
            // 2. 判斷是否可以解碼
            // 3. 針對動作類型不同剖析

            try
            {
                using (TextReader file = new StringReader(value.Message))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    List<ChargeManagement.Models.DeviceManagementModels.SlotRecord> kp = (List<ChargeManagement.Models.DeviceManagementModels.SlotRecord>)serializer.Deserialize(file, typeof(List<ChargeManagement.Models.DeviceManagementModels.SlotRecord>));
                }
            }
            catch 
            { }

            return value;
        }
    }
}
