﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeManagement.Models
{
    public class ChargeTransDeviceRecord
    {
        public long ChargeTransDeviceRecordIndex { get; set; }
        public long ChargeTransDeviceRecordId { get; set; }

        public long ChargeTimeStamp { get; set; }

        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }

        public int SOC { get; set; }
        public string CellsVoltage { get; set; }
        public string CellsTemperature { get; set; }
        public long BatteryLife { get; set; }
        public long CycleCount { get; set; }

        public int Protection { get; set; }
    }
}