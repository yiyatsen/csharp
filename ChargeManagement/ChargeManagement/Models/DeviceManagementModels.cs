﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ChargeManagement.Models
{
    public class DeviceManagementModels
    {
        [Table("ChargeDeviceType")]
        public class ChargeDeviceType
        {
            [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int DeviceTypeId { get; set; }
            [Required]
            public string DeviceTypeName { get; set; }

            public string Description { get; set; }
            [JsonIgnore]
            public virtual IList<ChargeDeviceInfo> ChargeDeviceInfos { get; set; }

            public ChargeDeviceType()
            {
                ChargeDeviceInfos = new List<ChargeDeviceInfo>();
            }
        }

        [Table("ChargeDeviceInfo")]
        public class ChargeDeviceInfo
        {
            [Key, Column(Order = 0)]
            public string StationId { get; set; }
            [ForeignKey("ChargeDeviceType"), Column(Order = 1), Key]
            public int DeviceTypeId { get; set; }
            public virtual ChargeDeviceType ChargeDeviceType { get; set; }

            // Charge Device Information ...
            public bool IsOnline { get; set; }
            public long LastOnlineTime { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public string Name { get; set; }
            public string Address { get; set; }

            public int SlotNumber { get; set; }

            public bool IsRemove { get; set; }
            public bool IsEnable { get; set; }

            // Device Register Info
            public long RegisterDateTime { get; set; }
            public long ManufacturedDateTime { get; set; }
            public string APVersion { get; set; }

            public string Description { get; set; }

            [JsonIgnore]
            public virtual IList<ChargeDeviceRecord> ChargeDeviceRecords { get; set; }

            public ChargeDeviceInfo()
            {
                ChargeDeviceRecords = new List<ChargeDeviceRecord>();
            }
        }

        [Table("BatteryDeviceType")]
        public class BatteryDeviceType
        {
            [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int DeviceTypeId { get; set; }
            [Required]
            public string DeviceTypeName { get; set; }

            // type information
            public int DesignCapacity { get; set; }

            public string Description { get; set; }

            [JsonIgnore]
            public virtual IList<BatteryDeviceInfo> BatteryDeviceInfos { get; set; }

            public BatteryDeviceType()
            {
                BatteryDeviceInfos = new List<BatteryDeviceInfo>();
            }
        }

        [Table("BatteryDeviceInfo")]
        public class BatteryDeviceInfo
        {
            [Key, Column(Order = 0)]
            public string BatteryId { get; set; }
            [Key, Column(Order = 1)]
            public string ManufactureDate { get; set; }
            [ForeignKey("BatteryDeviceType"), Column(Order = 2), Key]
            public int DeviceTypeId { get; set; }
            public virtual BatteryDeviceType BatteryDeviceType { get; set; }

            public bool IsRemove { get; set; }
            public bool IsEnable { get; set; }

            // Device Register Info
            public long RegisterDateTime { get; set; }
            public long ManufacturedDateTime { get; set; }
            public string APVersion { get; set; }

            public string Description { get; set; }
        }

        [Table("ChargeDeviceRecord")]
        public class ChargeDeviceRecord
        {
            [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public long ChargeDeviceRecordIndex { get; set; }
            [Required]
            public string StationId { get; set; }
            [Required]
            public int DeviceTypeId { get; set; }

            public long TimeStamp { get; set; }

            public virtual IList<SlotRecord> SlotRecords { get; set; }

            public ChargeDeviceRecord()
            {
                SlotRecords = new List<SlotRecord>();
            }
        }

        [Table("SlotRecord")]
        public class SlotRecord
        {
            [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public long SlotRecordIndex { get; set; }

            public long SlotRecordId { get; set; }
            public int SlotId { get; set; }

            public long TimeStamp { get; set; }
            
            [ForeignKey("ChargeDeviceRecord"), Key, Column(Order = 1)]
            public long ChargeDeviceRecordIndex { get; set; }
            public virtual ChargeDeviceRecord ChargeDeviceRecord { get; set; }

            // ? => System.Nullable<T>
            //public int? BatteryDeviceRecordId { get; set; }
            public virtual SlotBatteryRecord SlotBatteryRecord { get; set; }

            public string Description { get; set; }
        }

        [Table("SlotBatteryRecord")]
        public class SlotBatteryRecord
        {
            [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public long SlotBatteryRecordIndex { get; set; }
            //[ForeignKey("ChargeSlotRecord"), Column(Order = 3)]
            //public int SlotId { get; set; }
            //[ForeignKey("ChargeSlotRecord"), Column(Order = 4)]
            //public int ChargeDeviceRecordId { get; set; }
            [ForeignKey("SlotRecord"), Key, Column(Order = 1)]
            public long SlotRecordIndex { get; set; }
            [JsonIgnore]
            public virtual SlotRecord SlotRecord { get; set; }

            public long SlotBatteryRecordId { get; set; }
            [Required]
            public string BatteryId { get; set; }
            [Required]
            public string ManufactureDate { get; set; }

            public int SOC { get; set; }
            public double Temperature { get; set; }
            public int Current { get; set; }
            public int Voltage { get; set; }
            public int VMax { get; set; }
            public int VMin { get; set; }
            public int Protection { get; set; }

            public string Description { get; set; }
        }

        [Table("TransRecord")]
        public class TransRecord
        {
            [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public long TransRecordIndex { get; set; }
            [Required]
            public string StationId { get; set; }
            [Required]
            public int DeviceTypeId { get; set; }

            public long TransRecordId { get; set; }
            public long TransTimeStamp { get; set; }
            [Required]
            public string UserId { get; set; }
            [Required]
            public string CardId { get; set; }
            public int TransStatus { get; set; }

            public int OldPoint { get; set; }
            public int Point { get; set; }
            public long InsertedTransDeviceIndex { get; set; }
            public virtual TransDeviceRecord InsertedTransDevice { get; set; }
            public long DrawOutTransDeviceIndex { get; set; }
            public virtual TransDeviceRecord DrawOutTransDevice { get; set; }

            public string Description { get; set; }
        }
        [Table("TransDeviceRecord")]
        public class TransDeviceRecord
        {
            [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public long TransDeviceRecordIndex { get; set; }

            public long TransDeviceRecordId { get; set; }

            public int SlotId { get; set; }
            [Required]
            public string BatteryId { get; set; }
            [Required]
            public string ManufactureDate { get; set; }
            public string OperatorId { get; set; }

            public int SOC { get; set; }
            public string CellsVoltage { get; set; }
            public int Protection { get; set; }

            public string Description { get; set; }
        }

        [Table("ChargeTransRecord")]
        public class ChargeTransRecord
        {
            [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public long ChargeTransRecordIndex { get; set; }

            public long ChargeTransRecordId { get; set; }
            public int SlotId { get; set; }
            public long StartChargeTransDeviceRecordIndex { get; set; }
            public virtual ChargeTransDeviceRecord StartChargeTransDeviceRecord { get; set; }
            public long EndChargeTransDeviceRecordIndex { get; set; }
            public virtual ChargeTransDeviceRecord EndChargeTransDeviceRecord { get; set; }

            public string Description { get; set; }
        }

        [Table("ChargeTransDeviceRecord")]
        public class ChargeTransDeviceRecord
        {
            [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public long ChargeTransDeviceRecordIndex { get; set; }

            public long ChargeTransDeviceRecordId { get; set; }

            public long ChargeTimeStamp { get; set; }
            [Required]
            public string BatteryId { get; set; }
            [Required]
            public string ManufactureDate { get; set; }

            public int SOC { get; set; }
            public string CellsVoltage { get; set; }
            public string CellsTemperature { get; set; }
            public long BatteryLife { get; set; }
            public long CycleCount { get; set; }

            public int Protection { get; set; }

            public string Description { get; set; }
        }
    }
}