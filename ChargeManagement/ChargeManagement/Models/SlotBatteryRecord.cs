﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeManagement.Models
{
    public class SlotBatteryRecord
    {
        public long SlotBatteryRecordIndex { get; set; }

        public long SlotBatteryRecordId { get; set; }
        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public int SOC { get; set; }
        public double Temperature { get; set; }
        public int Current { get; set; }
        public int Voltage { get; set; }
        public int VMax { get; set; }
        public int VMin { get; set; }
        public int Protection { get; set; }
    }
}