﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeManagement.Models
{
    public class TransDeviceRecord
    {
        public long TransDeviceRecordIndex { get; set; }
        public long TransDeviceRecordId { get; set; }

        public int SlotId { get; set; }
        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public string OperatorId { get; set; }

        public int SOC { get; set; }
        public string CellsVoltage { get; set; }
        public int Protection { get; set; }
    }
}