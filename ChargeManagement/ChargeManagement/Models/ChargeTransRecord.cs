﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeManagement.Models
{
    public class ChargeTransRecord
    {
        public long ChargeTransRecordIndex { get; set; }
        public string StationId { get; set; }
        public long ChargeTransRecordId { get; set; }
        public int SlotId { get; set; }
        public ChargeTransDeviceRecord StartChargeTransDeviceRecord { get; set; }
        public ChargeTransDeviceRecord EndChargeTransDeviceRecord { get; set; }
    }
}