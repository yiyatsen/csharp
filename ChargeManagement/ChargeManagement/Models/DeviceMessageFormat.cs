﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeManagement.Models
{
    public class DeviceMessageFormat
    {
        public string StationId { get; set; }
        public string TimeStamp { get; set; }
        public int Action { get; set; }
        public string Message { get; set; }
    }
}