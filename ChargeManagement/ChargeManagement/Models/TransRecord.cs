﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeManagement.Models
{
    public class TransRecord
    {
        public long TransRecordIndex { get; set; }
        public long TransRecordId { get; set; }
        public string StationId { get; set; }
        public long TransTimeStamp { get; set; }
        public string UserId { get; set; }
        public string CardId { get; set; }
        public int TransStatus { get; set; }

        public int OldPoint { get; set; }
        public int Point { get; set; }
        public long InsertedTransDeviceId { get; set; }
        public virtual TransDeviceRecord InsertedTransDevice { get; set; }
        public long DrawOutTransDeviceId { get; set; }
        public virtual TransDeviceRecord DrawOutTransDevice { get; set; }
    }
}