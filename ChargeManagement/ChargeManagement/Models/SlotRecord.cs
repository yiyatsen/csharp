﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeManagement.Models
{
    public class SlotRecord
    {
        public long SlotRecordIndex { get; set; }
        public long SlotRecordId { get; set; }
        public string StationId { get; set; }
        public long TimeStamp { get; set; }
        public int SlotId { get; set; }

        public long SlotBatteryRecordId { get; set; }
        public SlotBatteryRecord SlotBatteryRecord { get; set; }
    }
}