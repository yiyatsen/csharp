﻿using GTE.Command;
using GTE.Encryption;
using GTE.Message;
using GTEBatteryLib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEBatteryTransfer
{
    public class AppSetting
    {
        public int[] BaudRateList { get; set; }
        public string[] BatteryTypeList { get; set; }
        public string ProjectName { get; set; }

        public AppSetting()
        {
            ProjectName = "BZRMA";
            BatteryTypeList = new string[] { "7253", "7238" };
            BaudRateList = new int[] { 115200, 38400 };
        }
    }

    public class StationInfo
    {
        public string stationId { get; set; }
        public string stationType { get; set; }
        public string stationMode { get; set; }
        public string commMode { get; set; }
        public bool disableLogin { get; set; }
        public bool isSupportSchedule { get; set; }
        public bool isRegister { get; set; }

        public StationInfo()
        {
            stationId = "non-Register";
            stationType = "diagnostic";
            stationMode = "BatteryTransfer";
            commMode = "1To1";
            disableLogin = false;
            isSupportSchedule = false;
            isRegister = false;
        }
    }

    public class AutoSetting
    {
        public bool isAutoConn { get; set; }
        public string action { get; set; }
        public int timeout { get; set; }
        public int retryTime { get; set; }
        public bool isDetectedCmd { get; set; }
        public bool isBatteryIn { get; set; }
        public bool isLogMode { get; set; }
        public bool isAutoLog { get; set; }
        public bool enableSchedule { get; set; }
        public bool disableRefreshEEP { get; set; }
        public bool disableRefreshEEPFlag { get; set; }

        public AutoSetting()
        {
            isAutoConn = false;
            action = "retry";
            timeout = 500;
            retryTime = 0;
            isDetectedCmd = true;
            isBatteryIn = true;
            isLogMode = true;
            isAutoLog = false;
            enableSchedule = false;
            disableRefreshEEP = false;
            disableRefreshEEPFlag = false;
        }
    }

    public class CmdSetting
    {
        public bool enableASCII { get; set; }
        public bool enableCipher { get; set; }

        public CmdSetting()
        {
            enableASCII = false;
            enableCipher = false;
        }
    }

    public class BatInfo
    {
        public string timestamp { get; set; }
        public string eepTimestamp { get; set; }
        public int[] MCU_EEP { get; set; }
        public Dictionary<string, int[]> PB_EEP { get; set; }
        public string RTC { get; set; }
        public string MCU_VERSION { get; set; }
        public string DEVICE_NAME { get; set; }
        public string batteryType { get; set; }
        public string batteryId { get; set; }
        public string BARCODE { get; set; }
        public int[] MANUFACTURE_DATE { get; set; }
        public int SOC { get; set; }
        public int SOH { get; set; }
        public int CURRENT { get; set; }
        public uint TOTAL_VOLT { get; set; }
        public int MAX_VOLT { get; set; }
        public int MIN_VOLT { get; set; }
        public int DELTA_VOLT { get; set; }
        public int CELL_NUM { get; set; }
        public int[] CELL_VOLT { get; set; }
        public double TEMPERATURE { get; set; }
        public double MAX_TEMPERATURE { get; set; }
        public double MIN_TEMPERATURE { get; set; }
        public double DELTA_TEMPERATURE { get; set; }
        public double[] CELL_TEMPERATURE { get; set; }
        public int[] PROTECTION { get; set; }
        public ulong DESIGN_VOLTAGE { get; set; }
        public ulong DESIGN_CAPACITY { get; set; }
        public ulong REMAIN_CAPACITY { get; set; }
        public ulong FULL_CHARGE_CAPACITY { get; set; }
        public ulong CYCLE_COUNT { get; set; }
        public int[] LAST_CHARGE { get; set; }
        public int[] LAST_DISCHARGE { get; set; }
        public int[][] LOG_CHARGE { get; set; }
        public int[] DISCHARGE_INTERVAL { get; set; }
        public int[] TEMPERATURE_INTERVAL { get; set; }
    }

    public class LogModel
    {
        public int slotId { get; set; }
        public string path { get; set; }
        public int baudRate { get; set; }
        public string commMode { get; set; }

        public StationInfo stationInfo { get; set; }
        public AutoSetting autoSetting { get; set; }
        public CmdSetting cmdSetting { get; set; }
        public BatInfo batInfo { get; set; }

        public LogModel()
        {
            slotId = 1;
            commMode = "1To1";
            stationInfo = new StationInfo();
            autoSetting = new AutoSetting();
            cmdSetting = new CmdSetting();
            batInfo = new BatInfo();
        }
    }

    public class CallbackInfo
    {
        public int step { get; set; }
        public int status { get; set; }
        public object data { get; set; }
    }

    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        public static bool ISBETA = false;

        #region WIN 32
        [DllImport("user32.dll")]
        private extern static int SetWindowLong(IntPtr hwnd, int index, int value);
        [DllImport("user32.dll")]
        private extern static int GetWindowLong(IntPtr hwnd, int index);

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x00080000;

        void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            WindowInteropHelper wih = new WindowInteropHelper(this);
            int style = GetWindowLong(wih.Handle, GWL_STYLE);
            SetWindowLong(wih.Handle, GWL_STYLE, style & ~WS_SYSMENU);
        }
        #endregion

        #region Property
        private BatteryAPI mDevice = new BatteryAPI();

        private AppSetting mAppSetting; // 預設之連線設定及預設之電池屬性值
        //private FactoryDeviceAdapter mDeviceAdapter;

        private string mAppPath; //視窗程式之路徑
        private bool mEnableAppClose; // 是否可以關閉視窗程式
        private int mModeCode = 5; // 0 for NOR, 1 For ENG, 2 for OZ, 3 of Bizcon, 4 of DEV, 5 of quickly save
        private int mSaveAction = 0;
        private int mClickBarTime = 0;

        int mode = 0;
        bool isBardcodeSuccess = false;
        Regex reg = new Regex("gte.QA");
        Regex reg2 = new Regex("bzrma");


        public DateTime datetime { get; set; }
        private BatInfo mSource { get; set; }
        private BatInfo mDest { get; set; }
        private BatInfo mReDest { get; set; }

        private DispatcherTimer mPortScanTimer; // 監聽Port狀態之定時器
        private DispatcherTimer mRefreshDateTimeTimer; // 更新時間狀態之定時器
        private DispatcherTimer mReceivedVoltageTimer; // 取得電池電壓之定時器

        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();//引用stopwatch物件
        static string LOGPATH = System.AppDomain.CurrentDomain.BaseDirectory + @"Log\";
        public delegate void MessageHandler(object sender, CallbackInfo e);



        #endregion

        private int[] lookupSOH = new int[101] { 
            0,	1,	2,	3,	4,	5,	6,	7,	8,	9,
            10,	11,	12,	13,	14,	15,	16,	17,	18,	19,
            20,	21,	22,	23,	24,	25,	26,	27,	28,	29,
            30,	31,	32,	33,	34,	35,	36,	37,	38,	39,
            40,	41,	42,	43,	44,	45,	46,	47,	48,	49,
            50,	52,	54,	56,	58,	60,	62,	64,	66,	68,
            70,	71,	72,	73,	74,	75,	76,	77,	78,	79,
            80,	81,	82,	83,	84,	85,	86,	87,	88,	89,
            90,	90,	91,	91,	92,	92,	93,	93,	94,	94,
            95,	95,	97,	97,	98,	99,	100,	100,	100,	100, 100};

        public MainWindow()
        {
            SourceInitialized += MainWindow_SourceInitialized;
            InitializeComponent();
            InitializeAppSetting();
            InitializeUI();
            InitializeScanPort();
        }


        /// <summary>
        /// 初始化視窗程式需要之設定檔
        /// </summary>
        private void InitializeAppSetting()
        {
            mAppPath = System.AppDomain.CurrentDomain.BaseDirectory;

            string appSettingFilePath = mAppPath + "AppSetting.ap";
            try
            {
                if (File.Exists(appSettingFilePath))
                {
                    using (StreamReader file = File.OpenText(appSettingFilePath))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        mAppSetting = (AppSetting)serializer.Deserialize(file, typeof(AppSetting));
                    }
                }

                if (mAppSetting == null)
                {
                    mAppSetting = new AppSetting();

                    using (FileStream fs = File.Open(appSettingFilePath, FileMode.Create))
                    using (StreamWriter sw = new StreamWriter(fs))
                    using (JsonWriter jw = new JsonTextWriter(sw))
                    {
                        jw.Formatting = Formatting.Indented;

                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(jw, mAppSetting);
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// 初始化UI數值
        /// </summary>
        private void InitializeUI()
        {
            Version CurrentVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            string ver = CurrentVersion.ToString();
            if (ISBETA)
            {
                versionValue.Content = ver + " (Beta)";
            }
            else
            {
                versionValue.Content = ver;
            }
        }
        /// <summary>
        /// 初始化ComPort監聽時間器
        /// </summary>
        private void InitializeScanPort()
        {
            SetSerialPortToComboBox(rportList, true);
            SetSerialPortToComboBox(tportList, true);
            mPortScanTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(5) };
            mPortScanTimer.Tick += PortScanTimer_Tick;
            mPortScanTimer.Start();
        }
        /// <summary>
        /// 啟動監聽ComPort狀態之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PortScanTimer_Tick(object sender, EventArgs e)
        {
            SetSerialPortToComboBox(rportList, false);
            SetSerialPortToComboBox(tportList, false);
        }
        /// <summary>
        /// 設定ComPort列表至ComboBox
        /// </summary>
        private void SetSerialPortToComboBox(ComboBox portList, bool isFirstTime)
        {
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();
            if (ports != null && ports.Length > 0)
            {
                bool isChanged = false;
                foreach (object data in portList.Items)
                {
                    if (!ports.Contains(data.ToString()))
                    {
                        isChanged = true;
                    }
                }

                if (isChanged || isFirstTime)
                {
                    string port = null;
                    int index = 0;
                    if (portList.SelectedItem != null)
                    {
                        port = portList.SelectedItem.ToString();
                    }

                    portList.Items.Clear();
                    for (int i = 0; i < ports.Length; i++)
                    {
                        if (ports[i].Equals(port))
                        {
                            index = i;
                        }

                        portList.Items.Add(ports[i]);
                    }
                    portList.SelectedIndex = index;
                }
            }
        }

        private void CloseAP_Click(object sender, RoutedEventArgs e)
        {
            mEnableAppClose = true;
            this.Close();
        }

        /// <summary>
        /// 視窗準備關閉之事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (!mEnableAppClose) // 決定可否關閉視窗之條件
            {
                e.Cancel = true;
            }
        }

        private void StatusBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            mClickBarTime++;
            if (mClickBarTime == 10)
            {
                mClickBarTime = 0;
                codeTextBox.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void codeTextBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
           
            if (mode == 1)
            {
                if (isBardcodeSuccess)
                {
                    bool isMatch = reg2.IsMatch(codeTextBox.Password);
                    if (isMatch)
                    {
                        mode = 2;
                        modeValue.Content = "QA";
                    }
                }
                else
                {
                    mode = 0;
                    mClickBarTime = 0;
                    barcodeText.Visibility = System.Windows.Visibility.Collapsed;
                    codeTextBox.Visibility = System.Windows.Visibility.Collapsed;
                    modeValue.Content = "Transfer";
                }
            }
            else if (mode == 2)
            {
                mode = 0;
                barcodeText.Visibility = System.Windows.Visibility.Collapsed;
                barcodeText.Text = "";
                codeTextBox.Password = "";
                modeValue.Content = "Transfer";
            }
            else
            {
                bool isMatch = reg.IsMatch(codeTextBox.Password);
                if (isMatch)
                {
                    barcodeText.Visibility = System.Windows.Visibility.Visible;
                    mode = 1;
                }
                else
                {
                    barcodeText.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        private void BarcodeTextChanged(object sender, TextChangedEventArgs e)
        {
            isBardcodeSuccess = false;
            TextBox textBox = (TextBox)sender;
            if (textBox.Text.Length > 10)
            {
                textBox.Text = textBox.Text.Substring(0, 10);
                textBox.SelectionStart = textBox.Text.Length;
            }
            else
            {
                Regex regex = new Regex(@"^\d{10}$");
                isBardcodeSuccess = regex.IsMatch(textBox.Text);
            }
        }

        private void transfer_Click(object sender, RoutedEventArgs e)
        {
            transferStateTitle.Content = "Transfer Battery";
            transferStateValue.Content = "Ready";

            Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
            rportList.IsEnabled = false;
            tportList.IsEnabled = false;
            transferButton.IsEnabled = false;
            closeButton.IsEnabled = false;
            barcodeText.IsEnabled = false;
            codeTextBox.IsEnabled = false;

            string errorValueMsg = null;
            string errorTitleMsg = null;
            if (mode != 2)
            {
                if ((rportList.SelectedIndex == -1) || (tportList.SelectedIndex == -1))
                {
                    errorTitleMsg = "Alert";
                    errorValueMsg = "Please Select Source/Destination Port!";
                }
                else if (rportList.SelectedIndex == tportList.SelectedIndex)
                {
                    errorTitleMsg = "Alert";
                    errorValueMsg = "Destination must be not equal to Source!";
                }
            }
            else if (isBardcodeSuccess)
            {
                if ((tportList.SelectedIndex == -1))
                {
                    errorTitleMsg = "Alert";
                    errorValueMsg = "Please Select Destination Port!";
                }
            }
            else
            {
                errorTitleMsg = "Alert";
                errorValueMsg = "Barcode Format Error";
            }

            if (errorValueMsg != null)
            {
                transferStateTitle.Content = errorTitleMsg;
                transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                transferStateValue.Content = errorValueMsg;

                Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                rportList.IsEnabled = true;
                tportList.IsEnabled = true;
                transferButton.IsEnabled = true;
                closeButton.IsEnabled = true;
                barcodeText.IsEnabled = true;
                codeTextBox.IsEnabled = true;
            }
            else
            {
                transferStateTitle.Content = "Read Destination Battery";
                transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                transferStateValue.Content = "Please Wait!";


                datetime = DateTime.UtcNow;
                CallbackInfo callbackData = new CallbackInfo()
                {
                    step = (mode != 2) ? 1 : 2,
                    status = -1,
                    data = null,
                };
                mSource = null;
                mDest = null;
                mReDest = null;

                string port = tportList.SelectedValue.ToString();
                ThreadPool.QueueUserWorkItem(o => ConnectBattery(sender, port, callbackData));
            }
        }

         /// <summary>
        /// 使用Thread回傳訊息之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MessageEvent(object sender, CallbackInfo callbackData)
        {
            switch (callbackData.step)
            {
                case 1:
                    {
                        // 讀取來源資訊
                        mDevice.Disconnect();
                        if (callbackData.status == 0)
                        {
                            transferStateTitle.Content = "Read Source Battery";
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                            transferStateValue.Content = "Please Wait!";
                            callbackData.step = 2;
                            mDest = (BatInfo)callbackData.data;
                            string port = rportList.SelectedValue.ToString();
                            ThreadPool.QueueUserWorkItem(o => ConnectBattery(sender, port, callbackData));
                        }
                        else
                        {
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                            transferStateValue.Content = "Load Fail";
                            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                            rportList.IsEnabled = true;
                            tportList.IsEnabled = true;
                            transferButton.IsEnabled = true;
                            closeButton.IsEnabled = true;
                            barcodeText.IsEnabled = true;
                            codeTextBox.IsEnabled = true;
                        }
                        break;
                    }
                case 2:
                    {
                        // 讀取來源資訊
                        mDevice.Disconnect();
                        if (callbackData.status == 0)
                        {
                            transferStateTitle.Content = "Check Battery Information";
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                            transferStateValue.Content = "Please Wait!";
                            if (mode != 2)
                            {
                                callbackData.step = 3;
                                mSource = (BatInfo)callbackData.data;
                            }
                            else
                            {
                                callbackData.step = 4;
                                mDest = (BatInfo)callbackData.data;
                            }

                            ThreadPool.QueueUserWorkItem(o => CheckBatteryInformationBefore(sender, callbackData));
                        }
                        else
                        {
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                            transferStateValue.Content = "Load Fail";
                            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                            rportList.IsEnabled = true;
                            tportList.IsEnabled = true;
                            transferButton.IsEnabled = true;
                            closeButton.IsEnabled = true;
                            barcodeText.IsEnabled = true;
                            codeTextBox.IsEnabled = true;
                        }
                        break;
                    }
                case 3:
                    {
                        // 燒入資訊
                        if (callbackData.status == 0)
                        {
                            transferStateTitle.Content = "Transfer Battery";
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                            transferStateValue.Content = "Please Wait!";

                            string an1310 = System.AppDomain.CurrentDomain.BaseDirectory + "Tools\\cl.exe";
                            System.Diagnostics.Process process = new System.Diagnostics.Process();
                            process.StartInfo.FileName = an1310;
                            process.StartInfo.Arguments = string.Format(" -i {0} -d {1} -p -m -r", rportList.SelectedValue.ToString(), tportList.SelectedValue.ToString());
                            process.StartInfo.UseShellExecute = false;
                            process.StartInfo.CreateNoWindow = true;
                            process.EnableRaisingEvents = true;
                            process.Exited += CopyFirmwareDone;
                            process.Start();
                        }
                        else
                        {
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                            transferStateValue.Content = "Invalid Battery";
                            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                            rportList.IsEnabled = true;
                            tportList.IsEnabled = true;
                            transferButton.IsEnabled = true;
                            closeButton.IsEnabled = true;
                            barcodeText.IsEnabled = true;
                            codeTextBox.IsEnabled = true;
                        }
                        break;
                    }
                case 4:
                    {
                        // 設定Destination board 資訊
                        if (callbackData.status == 0)
                        {
                            transferStateTitle.Content = "Set Battery Configuration";
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                            transferStateValue.Content = "Please Wait!";

                            callbackData.step = 5;
                            string sn = (isBardcodeSuccess) ? barcodeText.Text : null;
                            string port = tportList.SelectedValue.ToString();
                            ThreadPool.QueueUserWorkItem(o => ConfigBattery(sender, port, sn, callbackData));
                        }
                        else
                        {
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                            transferStateValue.Content = "Invalid Battery";
                            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                            rportList.IsEnabled = true;
                            tportList.IsEnabled = true;
                            transferButton.IsEnabled = true;
                            closeButton.IsEnabled = true;
                            barcodeText.IsEnabled = true;
                            codeTextBox.IsEnabled = true;
                        }
                        break;
                    }
                case 5:
                    {
                        // 重讀Destination board 資訊
                        transferStateTitle.Content = "Reload Destination Battery";
                        transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                        transferStateValue.Content = "Please Wait!";
                        callbackData.step = 6;
                        string port = tportList.SelectedValue.ToString();
                        ThreadPool.QueueUserWorkItem(o => ConnectBattery(sender, port, callbackData));
                        break;
                    }
                case 6:
                    {
                        // 確認燒入後的資訊
                        mDevice.Disconnect();
                        if (callbackData.status == 0)
                        {
                            transferStateTitle.Content = "Check Destination Information";
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                            transferStateValue.Content = "Please Wait!";
                            callbackData.step = 7;
                            mReDest = (BatInfo)callbackData.data;
                            callbackData.data = barcodeText.Text.ToString();
                            ThreadPool.QueueUserWorkItem(o => CheckBatteryInformationAfter(sender, callbackData));
                        }
                        else
                        {
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                            transferStateValue.Content = "Reload Fail";
                            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                            rportList.IsEnabled = true;
                            tportList.IsEnabled = true;
                            transferButton.IsEnabled = true;
                            closeButton.IsEnabled = true;
                            barcodeText.IsEnabled = true;
                            codeTextBox.IsEnabled = true;
                        }
                        break;
                    }
                case 7:
                    {
                        // 完成

                        if (callbackData.status == 0)
                        {
                            transferStateTitle.Content = "Transfer Battery";
                            transferStateValue.Content = "Success";
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                        }
                        else if (callbackData.status == -1 || callbackData.status == -2)
                        {
                            transferStateTitle.Content = "Check Battery";
                            transferStateValue.Content = "Fail";
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                        }
                        else if (callbackData.status == -3)
                        {
                            transferStateTitle.Content = "Check RTC";
                            transferStateValue.Content = "Fail";
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                        }

                        Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                        rportList.IsEnabled = true;
                        tportList.IsEnabled = true;
                        transferButton.IsEnabled = true;
                        closeButton.IsEnabled = true;
                        barcodeText.IsEnabled = true;
                        codeTextBox.IsEnabled = true;
                        break;
                    }
                default:
                    {
                        transferStateTitle.Content = "Unknow State";
                        transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                        transferStateValue.Content = "Please Restart Application!";
                        break;
                    }

            }
        }

        /// <summary>
        /// 與電池連線電池
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="port"></param>
        /// <param name="baudRate"></param>
        private void ConnectBattery(object sender, string port, CallbackInfo callbackData)
        {
            callbackData.status = -1;   
                BatInfo ba = new BatInfo();
                for (int l = 0; l < 3; l++)
                { 
                    try { 
                        for (int k = 0; k < mAppSetting.BaudRateList.Length; k++)
                        {
                            bool isSuccess = mDevice.Connect(port, mAppSetting.BaudRateList[k]);
                            if (isSuccess)
                            {
                                callbackData.status = GetBatteryNewAllInformation(ba);

                                if (callbackData.status == 0)
                                {
                                    callbackData.data = ba;
                                    break;
                                }
                            }
                        }
                        if (callbackData.status == 0)
                        {
                            break;
                        }
                    } catch (Exception ex) {
                        MessageBox.Show(ex.Message, "Alert");
                    }
                }

            Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { sender, callbackData });
        }

        private void CopyFirmwareDone(object sender, EventArgs e)
        {
            Thread.Sleep(3000);
            CallbackInfo callbackData = new CallbackInfo()
            {
                step = 4,
                status = 0,
                data = null
            };
            Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { sender, callbackData });
        }

        private void ResetMCU(object sender, EventArgs e)
        {
            Thread.Sleep(3000);
            CallbackInfo callbackData = new CallbackInfo()
            {
                step = 5,
                status = 0,
                data = null
            };
            Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { sender, callbackData });
        }

        /// <summary>
        /// 與電池連線電池
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="port"></param>
        /// <param name="baudRate"></param>
        private void CheckBatteryInformationBefore(object sender, CallbackInfo callbackData)
        {
            callbackData.status = -1;
            try {   
                for (int i = 0; i < mAppSetting.BatteryTypeList.Count(); i++)
                {
                    Regex regex = new Regex(mAppSetting.BatteryTypeList[i]);

                    if (mode != 2)
                    {
                        callbackData.status = (regex.IsMatch(mSource.DEVICE_NAME) && regex.IsMatch(mDest.DEVICE_NAME)) ? 0 : -2;
                    }
                    else
                    {
                        callbackData.status = (regex.IsMatch(mDest.DEVICE_NAME)) ? 0 : -3;
                    }

                    if (callbackData.status == 0)
                    {
                        break;
                    }
                }

                if (callbackData.status == 0)
                {
                    Regex regex = new Regex(mAppSetting.ProjectName);
 
                    if (mDest.PB_EEP.ContainsKey("2")) {
                        List<byte> temp = new List<byte>();
                        int addr = 0x40;
                        for (int i = 0; i < 5; i++)
                        {
                            temp.Add((byte)mDest.PB_EEP["2"][addr + i]);
                        }
                        string ozpn = System.Text.Encoding.ASCII.GetString(temp.ToArray());
                        callbackData.status = (regex.IsMatch(ozpn)) ? 0 : -3;
                    }
                    else
                    {
                        callbackData.status = -4;
                    }
                }
                if (mSource != null)
                {
                    SaveBatteryInformation(mSource, datetime.ToString("yyyyMMddHHmmss") + "_src_");
                }
                SaveBatteryInformation(mDest, datetime.ToString("yyyyMMddHHmmss") + "_dest_before_");
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Alert");
            }

            Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { sender, callbackData });
        }

        private void CheckBatteryInformationAfter(object sender, CallbackInfo callbackData)
        {
            callbackData.status = -1;
            try {
                if ((mReDest.MCU_EEP[941] + 2000) >= datetime.Year && mReDest.MCU_EEP[942] >= datetime.Month && mReDest.MCU_EEP[943] >= datetime.Day)
                {
                    if (mode != 2)
                    {
                        callbackData.status = (mSource.DEVICE_NAME == mReDest.DEVICE_NAME && mSource.BARCODE == mReDest.BARCODE && mSource.MCU_VERSION == mReDest.MCU_VERSION) ? 0 : -1;
                    }
                    else
                    {
                        callbackData.status = ((string)callbackData.data == mReDest.BARCODE) ? 0 : -2;
                    }
                }
                else
                {
                    callbackData.status = -3;
                }

                SaveBatteryInformation(mReDest, datetime.ToString("yyyyMMddHHmmss") + "_dest_after_");
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Alert");
            }
            Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { sender, callbackData });
        }
        /// <summary>
        /// 設定電池資訊
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="port"></param>
        /// <param name="baudRate"></param>
        private void ConfigBattery(object sender, string port, string serialNumber, CallbackInfo callbackData)
        {
            callbackData.status = -1;
            try { 
                BatInfo ba = new BatInfo();
                for (int l = 0; l < 3; l++)
                {
                    for (int k = 0; k < mAppSetting.BaudRateList.Length; k++)
                    {
                        bool isSuccess = mDevice.Connect(port, mAppSetting.BaudRateList[k]);
                        if (isSuccess)
                        {
                            mDevice.SendBatteryCmd(BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET, Convert.ToByte(0x83));

                            if (serialNumber != null)
                            {
                                SetSerialNumber(serialNumber);
                            }

                            SetRTC();

                            mDevice.SendBatteryCmd(BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET, Convert.ToByte(0x03));
                            mDevice.Disconnect();
                            System.Threading.Thread.Sleep(500);
                            string an1310 = System.AppDomain.CurrentDomain.BaseDirectory + "Tools\\cl.exe";
                            System.Diagnostics.Process process = new System.Diagnostics.Process();
                            process.StartInfo.FileName = an1310;
                            process.StartInfo.Arguments = string.Format(" -d {0} -r", port);
                            process.StartInfo.UseShellExecute = false;
                            process.StartInfo.CreateNoWindow = true;
                            process.EnableRaisingEvents = true;
                            process.Exited += ResetMCU;
                            process.Start();
                            callbackData.status = 0;
                            break;
                        }
                    }
                    if (callbackData.status == 0)
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert");
            }
            if (callbackData.status != 0)
            {
                Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { sender, callbackData });
            }
        }

        private void SetSerialNumber(string serialNumber)
        {
            int[] barCodeItem = new int[5];
            barCodeItem[0] = int.Parse(serialNumber.Substring(0, 1)); // 生產商代碼
            barCodeItem[1] = int.Parse(serialNumber.Substring(1, 1)); // 產品商代碼
            barCodeItem[2] = int.Parse(serialNumber.Substring(2, 2)); // 製造年份
            barCodeItem[3] = int.Parse(serialNumber.Substring(4, 2)); // 製造年週別
            barCodeItem[4] = int.Parse(serialNumber.Substring(6, 4)); // 序號

            mDevice.SetMCUEEPCmd(BatteryEEPAttrCmd.ManufactureInfo, Convert.ToByte((barCodeItem[0] << 4) | barCodeItem[1])); // Set Manufacturer & ProductClass

            int year = 2000 + barCodeItem[2]; //barcode只有兩千年後兩碼
            mDevice.SetMCUEEPCmd(BatteryEEPAttrCmd.ManufactureDate, ISO8601WeekNumToThursday(year, barCodeItem[3]));// Set Manufacture Date

            mDevice.SetMCUEEPCmd(BatteryEEPAttrCmd.ManufactureWeekOfYear, Convert.ToByte((barCodeItem[3]))); // Set Manufacture Week Of Year

            mDevice.SetMCUEEPCmd(BatteryEEPAttrCmd.SerialNumber, barCodeItem[4]); // Set Serial Number

            mDevice.SendBatteryCmd(BatteryCmd.CMD_MCUEEP_SET, Convert.ToByte(Convert.ToInt32("C", 16)), Convert.ToByte(Convert.ToInt32("27", 16)), 1);
            mDevice.SendBatteryCmd(BatteryCmd.CMD_MCUEEP_SET, Convert.ToByte(Convert.ToInt32("D", 16)), Convert.ToByte(Convert.ToInt32("10", 16)), 1);
        }

        private void SetRTC()
        {
            DateTime dt = DateTime.UtcNow;
            mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_SEC, Convert.ToByte(dt.Second));
            mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_MIN, Convert.ToByte(dt.Minute));
            mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_HOUR, Convert.ToByte(dt.Hour));
            mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_DAY, Convert.ToByte(dt.Day));
            mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_MONTH, Convert.ToByte(dt.Month));
            mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_YEAR, Convert.ToByte(dt.Year - 2000));
        }

        /// <summary>
        /// 轉換至ISO8601標準的週別日期時間
        /// </summary>
        /// <param name="year"></param>
        /// <param name="weekOfYear"></param>
        /// <returns></returns>
        public int[] ISO8601WeekNumToThursday(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1); //當年分第一天
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;//移動至星期三

            DateTime firstThursday = jan1.AddDays(daysOffset);//當周星期四時間
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            DateTime result = firstThursday.AddDays(weekNum * 7);
            int[] date = new int[3];
            date[0] = result.Year;
            date[1] = result.Month;
            date[2] = result.Day;
            return date;
        }

        #region Battery Method
        /// <summary>
        /// 取得需要的電池資訊
        /// </summary>
        /// <param name="sender"></param>
        private int GetBatteryNewAllInformation(BatInfo ba)
        {
            try
            {
                object value = mDevice.SendBatteryCmd(BatteryCmd.CMD_OZ1_EEPROM_GET);
                if (value != null)
                {
                    List<int> tempData = new List<int>();
                    ba.PB_EEP = new Dictionary<string, int[]>();
                    for (int i = 0; i < ((byte[])value).Length; i++)
                    {
                        tempData.Add(((byte[])value)[i]);
                    }
                    ba.PB_EEP.Add("1", tempData.ToArray());

                    value = mDevice.SendBatteryCmd(BatteryCmd.CMD_OZ2_EEPROM_GET);
                    if (value != null)
                    {
                        tempData.Clear();
                        for (int i = 0; i < ((byte[])value).Length; i++)
                        {
                            tempData.Add(((byte[])value)[i]);
                        }
                        ba.PB_EEP.Add("2", tempData.ToArray());
                    }
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_EEPROM_GET);
                if (value != null)
                {
                    int mcuIndex = 0;
                    int.TryParse(value.ToString(), out mcuIndex);

                    List<int> tempData = new List<int>();
                    for (int i = 1; i <= mcuIndex; i++)
                    {
                        value = mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_EEPROM_GET, (byte)i);
                        if (value != null)
                        {
                            for (int j = 0; j < ((byte[])value).Length; j++)
                            {
                                tempData.Add(((byte[])value)[j]);
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    ba.MCU_EEP = tempData.ToArray();


                    value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CRATE_INFO);
                    if (value != null)
                    {
                        tempData.Clear();
                        for (int i = 0; i < ((int[])value).Length; i++)
                        {
                            tempData.Add(((int[])value)[i]);
                        }
                        ba.DISCHARGE_INTERVAL = tempData.ToArray();
                    }
                    value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_TEMPERATURE_INTERVAL);
                    if (value != null)
                    {
                        tempData.Clear();
                        for (int i = 0; i < ((int[])value).Length; i++)
                        {
                            tempData.Add(((int[])value)[i]);
                        }
                        ba.TEMPERATURE_INTERVAL = tempData.ToArray();
                    }
                }
                else
                {
                    value = mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_EEPROM_H_GET);
                    if (value != null)
                    {
                        List<int> tempData = new List<int>();
                        for (int i = 0; i < ((byte[])value).Length; i++)
                        {
                            tempData.Add(((byte[])value)[i]);
                        }
                        value = mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_EEPROM_L_GET);
                        if (value != null)
                        {
                            for (int i = 0; i < ((byte[])value).Length; i++)
                            {
                                tempData.Add(((byte[])value)[i]);
                            }
                            ba.MCU_EEP = tempData.ToArray();
                        }
                    }

                    value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_00_22, 1);
                    if (value != null)
                    {
                        List<int[]> tempLog = new List<int[]>();
                        tempLog.AddRange((int[][])value);
                        value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_23_45, 1);
                        if (value != null)
                        {
                            tempLog.AddRange((int[][])value);
                            value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_46_49, 1);
                            if (value != null)
                            {
                                tempLog.AddRange((int[][])value);
                                ba.LOG_CHARGE = tempLog.ToArray();
                            }
                        }
                    }
                }

                if (ba.MCU_EEP != null)
                {
                    ba.eepTimestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.000Z");
                    int MANUFACTURE_DATE = ((ba.MCU_EEP[18] << 8) | ba.MCU_EEP[19]);

                    int tmpYear = ((MANUFACTURE_DATE >> 9) & 0x0000007F) + 2000;
                    if (tmpYear > DateTime.Now.Year)
                    {
                        tmpYear = tmpYear - 20;
                    }

                    ba.MANUFACTURE_DATE = new int[] { tmpYear, (MANUFACTURE_DATE >> 5) & 0x0000000F, MANUFACTURE_DATE & 0x0000001F };

                    ba.batteryId = string.Format("{0:0}{1:0}{2:00}{3:00}{4:0000}",
                        ((ba.MCU_EEP[56] >> 4) & 0xF), (ba.MCU_EEP[56] & 0xF), (tmpYear - 2000), ba.MCU_EEP[57], ((ba.MCU_EEP[20] << 8) | ba.MCU_EEP[21]));
                    ba.BARCODE = ba.batteryId;

                    ba.CYCLE_COUNT = (ulong)((ba.MCU_EEP[12] << 8) | ba.MCU_EEP[13]);
                    ba.DESIGN_VOLTAGE = (ulong)(((ba.MCU_EEP[16] << 8) | ba.MCU_EEP[17]) * 1000);
                    ba.DESIGN_CAPACITY = (ulong)(((ba.MCU_EEP[2] << 24) | (ba.MCU_EEP[3] << 16) | (ba.MCU_EEP[4] << 8) | (ba.MCU_EEP[5])) / 3600);
                    ba.FULL_CHARGE_CAPACITY = (ulong)(((ba.MCU_EEP[8] << 24) | (ba.MCU_EEP[9] << 16) | (ba.MCU_EEP[10] << 8) | (ba.MCU_EEP[11])) / 3600);

                    if (ba.DESIGN_CAPACITY != 0)
                    {
                        int index = Convert.ToInt32(ba.FULL_CHARGE_CAPACITY * 100 / ba.DESIGN_CAPACITY);
                        if (index > 100)
                        {
                            ba.SOH = 100;
                        }
                        else if (index < 0)
                        {
                            ba.SOH = 0;
                        }
                        else
                        {
                            ba.SOH = lookupSOH[index];
                        }
                    }
                    else
                    {
                        ba.SOH = 0;
                    }
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_VERSION_GET);
                if (value != null) ba.MCU_VERSION = value.ToString();

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_DEVICE_NAME_GET);
                if (value != null)
                {
                    ba.DEVICE_NAME = value.ToString();
                    ba.batteryType = value.ToString();
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_RTC_REG_DUMP, 1);
                if (value != null)
                {
                    int[] tmpRTC = value as int[];
                    ba.RTC = string.Format("{0:0000}-{1:00}-{2:00}T{3:00}-{4:00}-{5:00}.000Z", tmpRTC[0], tmpRTC[1], tmpRTC[2], tmpRTC[3], tmpRTC[4], tmpRTC[5]);
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_RSOC_GET);
                if (value != null)
                {
                    int numberValue = 0;
                    int.TryParse(value.ToString(), out numberValue);
                    ba.SOC = numberValue;
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_CURRENT_GET);
                if (value != null)
                {
                    int numberValue = 0;
                    int.TryParse(value.ToString(), out numberValue);
                    ba.CURRENT = numberValue;
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_PROTECTION_GET);
                if (value != null)
                {
                    List<int> tempData = new List<int>();
                    for (int i = 0; i < ((byte[])value).Length; i++)
                    {
                        tempData.Add(((byte[])value)[i]);
                    }
                    ba.PROTECTION = tempData.ToArray();
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_REMAIN_CAPACITY_GET);
                if (value != null)
                {
                    ulong numberValue = 0;
                    ulong.TryParse(value.ToString(), out numberValue);
                    ba.REMAIN_CAPACITY = numberValue;
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_VOLTAGE_GET);
                if (value != null)
                {
                    ba.CELL_VOLT = value as int[];

                    if (ba.CELL_VOLT != null)
                    {
                        ba.CELL_NUM = ba.CELL_VOLT.Length;

                        ba.TOTAL_VOLT = 0;
                        ba.MAX_VOLT = 0;
                        ba.MIN_VOLT = 0;
                        bool isFirst = true;
                        foreach (int cellVoltage in ba.CELL_VOLT)
                        {
                            if (isFirst)
                            {
                                isFirst = false;
                                ba.MAX_VOLT = cellVoltage;
                                ba.MIN_VOLT = cellVoltage;
                            }
                            if (cellVoltage > ba.MAX_VOLT)
                            {
                                ba.MAX_VOLT = cellVoltage;
                            }

                            if (cellVoltage < ba.MIN_VOLT)
                            {
                                ba.MIN_VOLT = cellVoltage;
                            }
                            ba.TOTAL_VOLT = ba.TOTAL_VOLT + (uint)cellVoltage;
                        }
                        ba.DELTA_VOLT = ba.MAX_VOLT - ba.MIN_VOLT;
                    }
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_TEMP_GET);
                if (value != null)
                {
                    ba.CELL_TEMPERATURE = value as double[];
                    if (ba.CELL_TEMPERATURE != null)
                    {
                        double minTemp = 0;
                        double maxTemp = 0;
                        bool isFirst = true;
                        for (int i = 0; i < ba.CELL_TEMPERATURE.Length; i++)
                        {
                            ba.CELL_TEMPERATURE[i] = Math.Round(ba.CELL_TEMPERATURE[i], 2);
                            if ((i % 3) != 0) // inner not count
                            {
                                double cell = ba.CELL_TEMPERATURE[i];
                                if (isFirst)
                                {
                                    isFirst = false;
                                    minTemp = cell;
                                    maxTemp = cell;
                                }

                                if (cell > maxTemp)
                                {
                                    maxTemp = cell;
                                }

                                if (cell < minTemp)
                                {
                                    minTemp = cell;
                                }
                            }
                        }
                        ba.TEMPERATURE = maxTemp;
                        ba.MAX_TEMPERATURE = maxTemp;
                        ba.MIN_TEMPERATURE = minTemp;
                        if (ba.MIN_TEMPERATURE < 0 || ba.PROTECTION[5] > 0)
                        {
                            ba.TEMPERATURE = minTemp;
                        }
                    }
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_LAST_CHARGE_END, 1);
                if (value != null)
                {
                    ba.LAST_CHARGE = value as int[];
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_LAST_DISCHARGE_END, 1);
                if (value != null)
                {
                    ba.LAST_DISCHARGE = value as int[];
                }

                ba.timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.000Z");
            }
            catch (Exception ex)
            {
                return -2;
            }

            return 0;
        }

        
        /// <summary>
        /// 儲存電池資訊
        /// </summary>
        /// <param name="sender"></param>
        private int SaveBatteryInformation(BatInfo ba, string nick)
        {
            try
            {
                DateTime dt = DateTime.Now;
                LogModel model = new LogModel();

                model.path = mDevice.GetPort();
                model.baudRate = mDevice.GetBaudRate();
                model.batInfo = ba;
                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    jw.Formatting = Formatting.Indented;

                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(jw, model);
                }

                const string LogSeparator = "===============================================================";
                string result = string.Format(LogSeparator + Environment.NewLine + "Reocrd Time :      <<{0}>>" + Environment.NewLine + LogSeparator + Environment.NewLine
                    , dt.ToString("yyyy/MM/dd HH:mm:ss"));
                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Battery Pack Information]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Device Name          :").PadRight(30) + "{0}" + Environment.NewLine
                    , ba.DEVICE_NAME);

                result = result +
                    string.Format(("Serial Number        :").PadRight(30) + "{0}" + Environment.NewLine
                    , ba.batteryId);

                result = result +
                    string.Format(("Manufacture Date :").PadRight(30) + "{0:0000}/{1:00}/{2:00}" + Environment.NewLine,
                    ba.MANUFACTURE_DATE[0], ba.MANUFACTURE_DATE[1], ba.MANUFACTURE_DATE[2]);

                result = result +
                    string.Format(("Design Voltage :").PadRight(30) + "{0} mV" + Environment.NewLine,
                    ba.DESIGN_VOLTAGE);

                result = result +
                    string.Format(("Design Capacity :").PadRight(30) + "{0} mAh" + Environment.NewLine
                    , ba.DESIGN_CAPACITY);

                result = result +
                    string.Format(("Remain Capacity :").PadRight(30) + "{0} mAh" + Environment.NewLine
                    , ba.REMAIN_CAPACITY);

                result = result +
                    string.Format(("Full Charge Capacity :").PadRight(30) + "{0} mAh" + Environment.NewLine
                    , ba.FULL_CHARGE_CAPACITY);

                result = result +
                    string.Format(("Cycle Counted :").PadRight(30) + "{0}" + Environment.NewLine
                    , ba.CYCLE_COUNT);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[State Of Health]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Battery Health :").PadRight(30) + "{0} %" + Environment.NewLine
                    , ba.SOH);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Relative SOC]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Relative SOC :").PadRight(30) + "{0} %" + Environment.NewLine
                    , ba.SOC);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Recent Current]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Current :").PadRight(30) + "{0} mA" + Environment.NewLine
                    , ba.CURRENT);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Pack Voltage]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Voltage :").PadRight(30) + "{0} mV" + Environment.NewLine
                    , ba.TOTAL_VOLT);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Pack Temperature]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Battery Temperature :").PadRight(30) + "{0:0.00} ℃" + Environment.NewLine
                    , ba.TEMPERATURE);


                if (ba.PROTECTION != null)
                {
                    result = result +
                        string.Format(LogSeparator + Environment.NewLine + "[Usage Current Error Record]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                    if (ba.PROTECTION[1] > 0)
                    {
                        result = result +
                            string.Format(("Error : Under Voltage Discharged") + Environment.NewLine);
                    }
                    if (ba.PROTECTION[0] > 0)
                    {
                        result = result +
                            string.Format(("Error : Over Voltage Charged") + Environment.NewLine);
                    }
                    if (ba.PROTECTION[3] > 0)
                    {
                        result = result +
                            string.Format(("Error : Over Current Discharged") + Environment.NewLine);
                    }
                    if (ba.PROTECTION[2] > 0)
                    {
                        result = result +
                            string.Format(("Error : Over Current Charged") + Environment.NewLine);
                    }
                    if (ba.PROTECTION[4] > 0)
                    {
                        result = result +
                            string.Format(("Error : Over Temp.") + Environment.NewLine);
                    }
                    if (ba.PROTECTION[5] > 0)
                    {
                        result = result +
                            string.Format(("Error : Under Temp.") + Environment.NewLine);
                    }
                    if (ba.PROTECTION[6] > 0)
                    {
                        result = result +
                            string.Format(("Error : Short Circuit") + Environment.NewLine);
                    }
                }

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Cells Voltage]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                if (ba.CELL_VOLT != null)
                {
                    for (int i = 0; i < ba.CELL_VOLT.Length; i++)
                    {
                        result = result +
                            string.Format(string.Format("Cell[{0:00}] Volt. :", i + 1).PadRight(30) + "{0} mV" + Environment.NewLine
                            , ba.CELL_VOLT[i]);
                    }
                    result = result +
                        string.Format(string.Format("Cell Voltage Delta :").PadRight(30) + "{0} mV" + Environment.NewLine
                        , ba.DELTA_VOLT);
                }
                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Pack Detail Info.]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(string.Format("MCU Firmware Ver :").PadRight(30) + "{0}" + Environment.NewLine
                    , ba.MCU_VERSION);

                if (ba.LAST_CHARGE != null)
                {
                    result = result +
                        string.Format(string.Format("Last Charging Time :").PadRight(30) + "{0:00}/{1:00}/{2:00} {3:00}:{4:00}:{5:00} {6}%" + Environment.NewLine
                        , ba.LAST_CHARGE[0], ba.LAST_CHARGE[1], ba.LAST_CHARGE[2], ba.LAST_CHARGE[3], ba.LAST_CHARGE[4], ba.LAST_CHARGE[5], ba.LAST_CHARGE[6]);
                }
                if (ba.LAST_DISCHARGE != null)
                {
                    result = result +
                        string.Format(string.Format("Last Discharging Time :").PadRight(30) + "{0:00}/{1:00}/{2:00} {3:00}:{4:00}:{5:00} {6}%" + Environment.NewLine
                        , ba.LAST_DISCHARGE[0], ba.LAST_DISCHARGE[1], ba.LAST_DISCHARGE[2], ba.LAST_DISCHARGE[3], ba.LAST_DISCHARGE[4], ba.LAST_DISCHARGE[5], ba.LAST_DISCHARGE[6]);
                }

                string fileName = ba.batteryType + "_" + ba.batteryId + "_" + dt.ToString("yyyyMMddHHmmss");

                result = result + LogSeparator
                    + Environment.NewLine;
                result = result + Environment.NewLine
                    + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine
                    + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine;
                result = result + LogSeparator + Environment.NewLine;

                EncryptionAESNeil mEncryptionAlog = new EncryptionAESNeil();
                mEncryptionAlog.SetEncryptionKey(fileName);
                result = result + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.UTF8.GetBytes(sb.ToString()))) + Environment.NewLine;
                result = result + "!" + LogSeparator;

                fileName = LOGPATH + nick + fileName + ".txt";
                bool isCreate = true;

                if (!Directory.Exists(LOGPATH))
                {
                    try
                    {
                        Directory.CreateDirectory(LOGPATH);
                    }
                    catch
                    {
                        isCreate = false;
                    }
                }
                using (StreamWriter outfile = new StreamWriter(fileName))
                {
                    outfile.Write(result);
                }
            }
            catch (Exception ex)
            {
                return -2;
            }

            return 0;
        }
        #endregion
    }
}
