﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GTCore.BatCore;

namespace GTCore
{
    public class PIC18EEP
    {
        public static List<BatCore.CmdConfig> PARAMLIST = new List<CmdConfig>()
        {
            new CmdConfig() { Name = "DESIGN_CAPACITY", Type = "System.UInt64", Address = 0x0002, Length = 4 },
            new CmdConfig() { Name = "FULL_CHARGE_CAPACITY", Type = "System.UInt64", Address = 0x0008, Length = 4 },
            new CmdConfig() { Name = "CYCLE_COUNT", Type = "System.UInt32", Address = 0x000C, Length = 2 },
            new CmdConfig() { Name = "MCU_RESET_COUNT", Type = "System.UInt32", Address = 0x000E, Length = 2 },
            new CmdConfig() { Name = "DESIGN_VOLTAGE", Type = "System.UInt64", Address = 0x0010, Length = 2 },
            new CmdConfig() { Name = "MANUFACTURE_DATE", Type = "System.Int32[]", Address = 0x0012, Length = 2 },
            new CmdConfig() { Name = "BATTERY_ID", Type = "UInt32", Address = 0x0014, Length = 2 },
            new CmdConfig() { Name = "DEVICE_NAME", Type = "System.String", Address = 0x0016, Length = 8 },
            //new CmdConfig() { Name = "EEP_REVISION", IsReadOnly = true, Type = "Byte", Address = 0x0026, Length = 10 },
            new CmdConfig() { Name = "MANUFACTURE_PRODUCT", Type = "System.String", Address = 0x0038, Length = 1 },
            new CmdConfig() { Name = "MANUFACTURE_WEEK", Type = "System.Int32", Address = 0x0039, Length = 1 },
            new CmdConfig() { Name = "EXN_ID", Type = "System.Byte[]", Address = 0x0056, Length = 20 },
            new CmdConfig() { Name = "ALERTHAPPENTIME", IsReadOnly = true, Type = "System.Int32[][]", Address = 0x0074, Length = 91 },
            new CmdConfig() { Name = "LOG_DISCHARGE", IsReadOnly = true, Type = "System.Int32[][]", Address = 0x0114, Length = 125 },
            new CmdConfig() { Name = "LOG_CHARGE", IsReadOnly = true, Type = "System.Int32[][]", Address = 0x0197, Length = 230 },
            new CmdConfig() { Name = "FCC_RESET_TIME", IsReadOnly = true, Type = "System.Int32[]", Address = 0x038B, Length = 7 },
            new CmdConfig() { Name = "RTC_RESET_TIME", IsReadOnly = true, Type = "System.Int32[]", Address = 0x0392, Length = 7 },
            new CmdConfig() { Name = "DISCHARGE_INTERVAL_VALUE", IsReadOnly = true, Type = "System.UInt64[]", Address = 0x0399, Length = 20 },
            new CmdConfig() { Name = "MCU_TIMER", IsReadOnly = true, Type = "System.DateTime", Address = 0x03AD, Length = 4 },
            new CmdConfig() { Name = "TEMPERATURE_INTERVAL_VALUE", IsReadOnly = true, Type = "System.UInt64[]", Address = 0x03B1, Length = 20 }
        };
        
        public static string GetSerialNumber(Dictionary<string, object> batInfo)
        {
            string str = (batInfo.ContainsKey("MANUFACTURE_PRODUCT")) ? (string)(batInfo["MANUFACTURE_PRODUCT"]) : "00";

            Int32 year = (batInfo.ContainsKey("MANUFACTURE_DATE")) ? ((Int32[])batInfo["MANUFACTURE_DATE"])[0] : 2000;
            Int32 week = (batInfo.ContainsKey("MANUFACTURE_WEEK")) ? (Int32)batInfo["MANUFACTURE_WEEK"] : 0;
            UInt32 id = (batInfo.ContainsKey("BATTERY_ID")) ? (UInt32)(batInfo["BATTERY_ID"]) : 0;
            return string.Format("{0}{1:00}{2:00}{3:0000}", str, year - BatCore.DEFAULTYEAR, week, id);
        }

        public static Dictionary<string, byte[]> CreateEEP(Dictionary<string, object> batInfo)
        {
            int maxAddress = 0;
            List<byte[]> array = new List<byte[]>();
            List<CmdConfig> cmdConfigList = new List<CmdConfig>();
            foreach (var item in batInfo)
            {
                CmdConfig cmdConfig = BatCore.GetMCUCmdConfig(item.Key);
                if (cmdConfig != null)
                {
                    int tmpLastAddress = cmdConfig.Address + cmdConfig.Length;
                    if (tmpLastAddress > maxAddress)
                    {
                        maxAddress = tmpLastAddress;
                    }
                    // object to string
                    cmdConfig.Value = BatCore.ConvertObjToString(item.Value);
                    cmdConfigList.Add(cmdConfig);
                }
            }

            Dictionary<string, byte[]> tmp = new Dictionary<string, byte[]>();
            byte[] eepArray = new byte[maxAddress];
            foreach (var item in cmdConfigList)
            {
                if (item.Value != null && item.Value != "")
                {
                    // 1. string to object
                    object obj = BatCore.ConvertStringToObj(item.Type, item.Value);
                    //  2. cmd to byte array
                    byte[] rawData = ConvertObjToArray(item.Name, obj);
                    Array.Copy(rawData, 0, eepArray, item.Address, rawData.Length);
                }
            }
            tmp.Add("1", eepArray);

            return tmp;
        }

        public static CmdConfig GetCmdConfig(string cmd)
        {
            CmdConfig tmp = PIC18EEP.PARAMLIST.FirstOrDefault(item => item.Name == cmd);
            if (tmp == null) return null;
            return new CmdConfig()
            {
                IsReadOnly = tmp.IsReadOnly,
                Name = tmp.Name,
                Type = tmp.Type,
                Address = tmp.Address,
                Length = tmp.Length
            };
        }
        
        public static Dictionary<string, object> ConvertArrayToCmdInfo(string cmd, byte[] data, Dictionary<string, object> val)
        {
            if (val == null) val = new Dictionary<string, object>();
            if (data.Length == 0) return val;

            switch (cmd)
            {
                case "DESIGN_CAPACITY":
                case "FULL_CHARGE_CAPACITY":
                    {
                        val.Add(cmd, (UInt64)(((((data[0] << 24) | (data[1] << 16)) | (data[2] << 8)) | data[3]) / 3600));
                        break;
                    }
                case "DESIGN_VOLTAGE":
                    {
                        val.Add(cmd, (UInt64)(((data[0] << 8) | data[1]) * 1000));
                        break;
                    }
                case "MANUFACTURE_PRODUCT":
                    {
                        val.Add(cmd, string.Format("{0}{1}", (data[0] >> 4) & 0xF, data[0] & 0xF));
                        break;
                    }
                case "MANUFACTURE_WEEK":
                    {
                        val.Add(cmd, (Int32)data[0]);
                        break;
                    }
                case "BATTERY_ID":
                case "CYCLE_COUNT":
                case "MCU_RESET_COUNT":
                    {
                        val.Add(cmd, (UInt32)((data[0] << 8) | data[1]));
                        break;
                    }
                case "MANUFACTURE_DATE":
                    {
                        int tmpValue = (data[0] << 8) | data[1];
                        int tmpYear = ((tmpValue >> 9) & 0x0000007F) + BatCore.DEFAULTYEAR;
                        if (tmpYear > DateTime.UtcNow.Year) tmpYear -= 20;
                        Int32[] dt = new Int32[] { tmpYear, (tmpValue >> 5) & 0x0000000F, tmpValue & 0x0000001F };
                        val.Add(cmd, dt);
                        break;
                    }
                case "ALERTHAPPENTIME":
                    {
                        int size = data.Length / 13;
                        List<int[]> tmpVal = new List<int[]>();

                        for (int i = 0; i < size; i += 1)
                        {
                            List<int> tmpData = new List<int>();
                            int index = i * 13;
                            int vMin = (int)(((data[index + 6] << 8) + (data[index + 7])) * 1.22); // 5000 / 4096;
                            int vMax = (int)(((data[index + 8] << 8) + (data[index + 9])) * 1.22); // 5000 / 4096;
                            tmpData.Add(data[index] + DEFAULTYEAR);
                            tmpData.Add(data[index + 1]);
                            tmpData.Add(data[index + 2]);
                            tmpData.Add(data[index + 3]);
                            tmpData.Add(data[index + 4]);
                            tmpData.Add(data[index + 5]);
                            tmpData.Add((vMin > 0xFFFF) ? 0 : vMin);
                            tmpData.Add((vMax > 0xFFFF) ? 0 : vMax);
                            tmpData.Add((data[index + 10] << 8) + (data[index + 11]));
                            tmpData.Add(data[index + 12]);
                            tmpVal.Add(tmpData.ToArray());
                        }
                        val.Add(cmd, tmpVal.ToArray());
                        break;
                    }
                case "DEVICE_NAME":
                    {
                        string str = "";
                        for (int i = 0; i < data.Length; i++)
                            str += String.Format("{0}", (char)data[i]).ToString().Trim('\0');
                        val.Add(cmd, str.Trim());
                        break;
                    }
                case "LOG_CHARGE":
                    {
                        int size = data.Length / 10;
                        List<int[]> tmpVal = new List<int[]>();

                        for (int i = 0; i < size; i += 1)
                        {
                            List<int> tmpData = new List<int>();
                            int info = (data[i * 10] << 8) | data[(i * 10) + 1];

                            tmpData.Add(((info >> 9) & 0x0000007F) + DEFAULTYEAR); // year
                            tmpData.Add((info >> 5) & 0x0000000F); // month
                            tmpData.Add(info & 0x0000001F); // day
                            tmpData.Add(data[(i * 10) + 2] & 0x0000001F); // hour
                            tmpData.Add(data[(i * 10) + 3] & 0x0000003F); // minute
                            tmpData.Add(((data[(i * 10) + 2] & 0x80) << 9) | (data[(i * 10) + 4] << 8) | data[(i * 10) + 5]); // voltstart
                            tmpData.Add(((data[(i * 10) + 8] & 0x80) << 9) | (data[(i * 10) + 6] << 8) | data[(i * 10) + 7]); // voltstop

                            if (i == 1)
                            {
                                tmpData.Add((((0x7F & data[(i * 10) + 8]) << 8) | (data[(i * 10) + 9]) * 60)); // duration
                            }
                            else
                            {
                                tmpData.Add(((0x7F & data[(i * 10) + 8]) << 8) | data[(i * 10) + 9]); // duration
                            }
                            tmpVal.Add(tmpData.ToArray());
                        }
                        val.Add(cmd, tmpVal.ToArray());
                        break;
                    }
                case "LOG_DISCHARGE":
                    {
                        int size = data.Length / 5;

                        List<int[]> tmpVal = new List<int[]>();
                        for (int i = 0; i < size; i += 1)
                        {
                            List<int> tmpData = new List<int>();
                            int info = (data[i * 5] << 8) | data[(i * 5) + 1];

                            tmpData.Add(((info >> 9) & 0x0000007F) + DEFAULTYEAR); // year
                            tmpData.Add((info >> 5) & 0x0000000F); // month
                            tmpData.Add(info & 0x0000001F); // day
                            tmpData.Add(data[(i * 5) + 2] & 0x0000001F); // hour
                            tmpData.Add(data[(i * 5) + 3] & 0x0000003F); // minute
                            tmpData.Add(((data[(i * 5) + 2] & 0x80) >> 5) | (data[(i * 5) + 4])); //current

                            tmpVal.Add(tmpData.ToArray());
                        }
                        val.Add(cmd, tmpVal.ToArray());
                        break;
                    }
                case "FCC_RESET_TIME":
                case "RTC_RESET_TIME":
                    {
                        List<int> tmpVal = new List<int>();
                        tmpVal.Add(data[0] + DEFAULTYEAR); // year
                        tmpVal.Add(data[1]); // month
                        tmpVal.Add(data[2]); // day
                        tmpVal.Add(data[3]); // hour
                        tmpVal.Add(data[4]); // min
                        tmpVal.Add(((data[5] << 8) + (data[6]))); // Count
                        val.Add(cmd, tmpVal.ToArray());
                        break;
                    }
                case "MCU_TIMER":
                    {
                        val.Add(cmd, DateTime.Parse(string.Format("{0:0000}-{1:00}-{2:00}T{3:00}:00:00.000Z", data[0] + BatCore.DEFAULTYEAR, data[1], data[2], data[3])));
                        break;
                    }
                case "DISCHARGE_INTERVAL_VALUE":
                case "TEMPERATURE_INTERVAL_VALUE":
                    {
                        List<ulong> tmpVal = new List<ulong>();
                        int num = data.Length / 4;
                        for (int i = 0; i < num; i += 1)
                        {
                            tmpVal.Add((ulong)((data[i * 4] << 24) + (data[(i * 4) + 1] << 16) + (data[(i * 4) + 2] << 8) + data[(i * 4) + 3]));
                        }
                        val.Add(cmd, tmpVal.ToArray());
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            return val;
        }
        
        public static byte[] ConvertObjToArray(string cmd, object obj)
        {
            List<byte> array = new List<byte>();
            switch (cmd)
            {
                case "DESIGN_CAPACITY":
                case "FULL_CHARGE_CAPACITY":
                    {
                        ulong tmpData = (ulong)obj * 3600;
                        array.Add((byte)((tmpData >> 24) & 0xFF));
                        array.Add((byte)((tmpData >> 16) & 0xFF));
                        array.Add((byte)((tmpData >> 8) & 0xFF));
                        array.Add((byte)(tmpData & 0xFF));
                        break;
                    }
                case "DESIGN_VOLTAGE":
                    {
                        uint tmpData = (uint)((ulong)obj * 0.001);
                        array.Add((byte)((tmpData >> 8) & 0xFF));
                        array.Add((byte)(tmpData & 0xFF));
                        break;
                    }
                case "MANUFACTURE_PRODUCT":
                    {
                        string str = (string)obj;
                        int p1 = int.Parse(str.Substring(0, 1)); // 生產商代碼
                        int p2 = int.Parse(str.Substring(1, 1)); // 產品商代碼
                        array.Add(Convert.ToByte((p1 << 4) | p2));
                        break;
                    }
                case "MANUFACTURE_WEEK":
                    {
                        array.Add(Convert.ToByte(obj));
                        break;
                    }
                case "BATTERY_ID":
                case "CYCLE_COUNT":
                case "MCU_RESET_COUNT":
                    {
                        uint tmpData = (uint)obj;
                        array.Add((byte)((tmpData >> 8) & 0xFF));
                        array.Add((byte)(tmpData & 0xFF));
                        break;
                    }
                case "DEVICE_NAME":
                    {
                        return Encoding.ASCII.GetBytes((string)obj);
                    }
                case "MANUFACTURE_DATE":
                    {
                        Int32[] dt = (Int32[])obj;
                        uint datetimeByte = (uint)(((dt[0] - BatCore.DEFAULTYEAR) & 0x7F) << 9);
                        datetimeByte |= (uint)((dt[1] & 0x0F) << 5);
                        datetimeByte |= (uint)(dt[2] & 0x1F);

                        array.Add((byte)((datetimeByte >> 8) & 0xFF));
                        array.Add((byte)(datetimeByte & 0xFF));
                        break;
                    }
                case "EXN_ID":
                    {
                        return (byte[])obj;
                    }
                default:
                    {
                        break;
                    }
            }
            return array.ToArray();
        }
    }
}
