﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTCore
{
    public class COMPort
    {
        public string Port { get { return (SerialPortObj.IsOpen) ? SerialPortObj.PortName : ""; } }
        public int BaudRate { get { return (SerialPortObj.IsOpen) ? SerialPortObj.BaudRate : 0; } }
        public bool IsOpen { get { return SerialPortObj.IsOpen; } }
        public List<byte> RecvData = new List<byte>();
        private SerialPort SerialPortObj = new SerialPort();

        public void Disconnect()
        {
            if (SerialPortObj != null)
            {
                SerialPortObj.DataReceived -= SerialPortObj_DataReceived;
                SerialPortObj.Close();
                RecvData.Clear();
            }
        }

        public int Connect(string port, int baudRate)
        {
            try
            {
                this.Disconnect();
                SerialPortObj.PortName = port;
                SerialPortObj.BaudRate = baudRate;
                RecvData.Clear();
                SerialPortObj.DataReceived += SerialPortObj_DataReceived;
                SerialPortObj.Open();
                //SerialPortObj.DtrEnable = true;
            }
            catch (Exception err)
            {
                return -1;
            }
            return 0;
        }

        public int SendData(byte[] sendStream)
        {
            try
            {
                SerialPortObj.DiscardOutBuffer();
                SerialPortObj.Write(sendStream, 0, sendStream.Length);
            }
            catch
            {
                return -1;
            }
            return 0;
        }

        private void SerialPortObj_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            byte[] buffer = new byte[256];
            while (sp.BytesToRead > 0)
            {
                Int32 receivedLen = sp.Read(buffer, 0, buffer.Length);
                Array.Resize(ref buffer, receivedLen);
                RecvData.AddRange(buffer);
            }
        }
    }
}
