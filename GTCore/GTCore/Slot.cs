﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace GTCore
{
    public class Slot
    {
        public class StationInfo
        {
            public string stationId { get; set; }
            public string stationType { get; set; }
            public string stationMode { get; set; }
            public string commMode { get; set; }
            public bool disableLogin { get; set; }
            public bool isSupportSchedule { get; set; }
            public bool isRegister { get; set; }

            public StationInfo()
            {
                stationId = "non-Register";
                stationType = "diagnostic";
                stationMode = "Factory";
                commMode = "1To1";
                disableLogin = false;
                isSupportSchedule = false;
                isRegister = false;
            }
        }
        public class BatInfo
        {
            public DateTime timestamp { get; set; }
            public DateTime eepTimestamp { get; set; }
            public byte[] MCU_EEP { get; set; }
            public Dictionary<string, byte[]> PB_EEP { get; set; }
            public DateTime RTC { get; set; }
            public string MCU_VERSION { get; set; }
            public string DEVICE_NAME { get; set; }
            public string batteryType { get; set; }
            public string batteryId { get; set; }
            public string BARCODE { get; set; }
            public Int32[] MANUFACTURE_DATE { get; set; }
            public uint SOC { get; set; }
            public int SOH { get; set; }
            public long CURRENT { get; set; }
            public ulong TOTAL_VOLT { get; set; }
            public uint MAX_VOLT { get; set; }
            public uint MIN_VOLT { get; set; }
            public uint DELTA_VOLT { get; set; }
            public int CELL_NUM { get; set; }
            public uint[] CELL_VOLT { get; set; }
            public double TEMPERATURE { get; set; }
            public double MAX_TEMPERATURE { get; set; }
            public double MIN_TEMPERATURE { get; set; }
            public double DELTA_TEMPERATURE { get; set; }
            public double[] CELL_TEMPERATURE { get; set; }
            public Int32[] PROTECTION { get; set; }
            public ulong DESIGN_VOLTAGE { get; set; }
            public ulong DESIGN_CAPACITY { get; set; }
            public ulong REMAIN_CAPACITY { get; set; }
            public ulong FULL_CHARGE_CAPACITY { get; set; }
            public uint CYCLE_COUNT { get; set; }
            public int[] LAST_CHARGE { get; set; }
            public int[] LAST_DISCHARGE { get; set; }
            public int[][] LOG_CHARGE { get; set; }
            public int[] DISCHARGE_INTERVAL { get; set; }
            public int[] TEMPERATURE_INTERVAL { get; set; }
        }

        public class COMPortConfig
        {
            public int BaudRate { get; set; }
            public bool EnableASCII { get; set; }
        }

        private static int REFRESHEEPTIMEOUT = 60000;
        private static int MAXRETRYTIME = 2;
        public static string GTCoreVersion { get { return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(); } }

        public static COMPortConfig DEFAULTBAUDRATE = new COMPortConfig() { BaudRate = 115200, EnableASCII = true };
        public static COMPortConfig[] DEFAULTBAUDRATELIST = new COMPortConfig[] {
            new COMPortConfig() { BaudRate = 115200, EnableASCII = true },
            new COMPortConfig() { BaudRate = 38400, EnableASCII = true },
            new COMPortConfig() { BaudRate = 38400, EnableASCII = false }
        };

        public enum SendSlotErrorState : int
        {
            None = 0,
            SendError = 1,
            EEPError = 2
        }
        
        public class AutoSetting
        {
            public bool isAutoConn { get; set; }
            public bool enableCANCmd { get; set; }
            public bool enableSchedule { get; set; }
            public bool isBatteryIn { get; set; }
            public bool isLogMode { get; set; }
            public bool isAutoLog { get; set; }
            public bool isDetectedCmd { get; set; }
            public bool disableRefreshEEP { get; set; }
            public bool disableRefreshEEPFlag { get; set; }
            public int retryTime { get; set; }
            public int timeout { get; set; }
            public string action { get; set; }

            public AutoSetting()
            {
                timeout = 500;
                action = "none";
            }
        }

        public class CmdSetting
        {
            public bool enableASCII { get; set; }
            public bool enableCipher { get; set; }

            public CmdSetting()
            {
                enableASCII = true;
                enableCipher = false;
            }
        }

        public class LogModel
        {
            public int slotId { get; set; }
            public string path { get; set; }
            public int baudRate { get; set; }
            public string commMode { get; set; }

            public StationInfo stationInfo { get; set; }
            public AutoSetting autoSetting { get; set; }
            public CmdSetting cmdSetting { get; set; }
            public BatInfo batInfo { get; set; }

            public LogModel()
            {
                slotId = 1;
                commMode = "1To1";
                stationInfo = new StationInfo();
                autoSetting = new AutoSetting();
                cmdSetting = new CmdSetting();
                batInfo = new BatInfo();
            }
        }

        public class SlotOptions
        {
            public string Error { get; set; }
            public bool EnableCANCmd { get; set; }
            public bool EnableSkip { get; set; }
            public bool EnableRecvTimeout { get; set; }
            public int SkipTime { get; set; }
            public Dictionary<string, object> BatInfo { get; set; }
            public List<BatOptions> List { get; set; }

            public SlotOptions()
            {
                List = new List<BatOptions>();
            }
        }

        private static int[] SOHTABLE = new int[101] {
            0,  1,  2,  3,  4,  5,  6,  7,  8,  9,
            10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
            20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
            30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
            40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
            50, 52, 54, 56, 58, 60, 62, 64, 66, 68,
            70, 71, 72, 73, 74, 75, 76, 77, 78, 79,
            80, 81, 82, 83, 84, 85, 86, 87, 88, 89,
            90, 90, 91, 91, 92, 92, 93, 93, 94, 94,
            95, 95, 97, 97, 98, 99, 100, 100, 100, 100, 100
        };
        public static string[] BASICEEPINFOLIST = new string[] { "DESIGN_CAPACITY", "DESIGN_VOLTAGE", "DEVICE_NAME" };
        public static string[] BASICEEPINFOLIST2 = new string[] { "FULL_CHARGE_CAPACITY", "CYCLE_COUNT", "MANUFACTURE_DATE" };
        public static string[] BATTERYIDLIST = new string[] { "BATTERY_ID", "MANUFACTURE_WEEK", "MANUFACTURE_PRODUCT" };
        public static string[] PROFILEINFOLIST = new string[] {
            "RTC", "PROTECTION", "SOC", "TOTAL_VOLT", "MIN_VOLT", "MAX_VOLT", "DELTA_VOLT", "CELL_NUM", "CELL_VOLT",
          "TEMPERATURE", "CELL_TEMPERATURE", "CURRENT", "REMAIN_CAPACITY", "LAST_CHARGE", "LAST_DISCHARGE"
        };
        private const string Separator = "===============================================================\r\n";

        private static int ConvertMappingSOH(Dictionary<string, object> batInfo) {
            ulong fcc = (batInfo.ContainsKey("FULL_CHARGE_CAPACITY")) ? (ulong)batInfo ["FULL_CHARGE_CAPACITY"] : 0;
            ulong dc = (batInfo.ContainsKey("DESIGN_CAPACITY")) ? (ulong)batInfo["DESIGN_CAPACITY"] : 1;

            ulong soh = (fcc * 100) / dc;
            soh = (soh < 0) ? 0 : soh;
            soh = (soh > 100) ? 100 : soh;
            return SOHTABLE[(int)soh];
        }

        public string path { get { return mCOMPort.Port; } }
        public int baudRate { get { return mCOMPort.BaudRate; } }
        public bool isOpen { get { return mCOMPort.IsOpen; } }
        public int slotId { get; set; }
        public Dictionary<string, object> batInfo { get; set; }

        public AutoSetting autoSetting { get; set; }
        public CmdSetting cmdSetting { get; set; }
        private List<Dictionary<string, object>> lastBatInfo = new List<Dictionary<string, object>>();
        private Dictionary<string, byte[]> mMCU_EEP = new Dictionary<string, byte[]>();
        private Dictionary<string, byte[]> mLOG_CHARGE = new Dictionary<string, byte[]>();
        private List<BatOptions> mUserCmdList = new List<BatOptions>();
        private COMPort mCOMPort = new COMPort();
        private BatCore mBatCore = new BatCore();
        private CancellationTokenSource mRefreshEEPCTS { get; set; }
        private CancellationTokenSource mAutoRunCTS { get; set; }

        private Dictionary<string, object> mUserBatInfo { get; set; }

        public Slot()
        {
            autoSetting = new AutoSetting();
            cmdSetting = new CmdSetting();
            batInfo = new Dictionary<string, object>();
        }

        public BatInfo GetBatInfo()
        {
            BatInfo bat = new BatInfo();
            try
            {
                System.Reflection.PropertyInfo[] pInfoList = bat.GetType().GetProperties();

                foreach (System.Reflection.PropertyInfo pInfo in pInfoList)
                {
                    if (this.batInfo.ContainsKey(pInfo.Name)) pInfo.SetValue(bat, this.batInfo[pInfo.Name]);
                }
            }
            catch (Exception ex)
            {
            }
            return bat;
        }

        public string SaveLog(DateTime dt, BatInfo bat)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                System.Reflection.PropertyInfo[] pInfoList = bat.GetType().GetProperties();

                foreach (System.Reflection.PropertyInfo pInfo in pInfoList)
                {
                    if (this.batInfo.ContainsKey(pInfo.Name)) pInfo.SetValue(bat, this.batInfo[pInfo.Name]);
                }

                System.IO.StringWriter sw = new System.IO.StringWriter(sb);
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    jw.Formatting = Formatting.Indented;

                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(jw, bat);
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            string result = string.Format("{0}Reocrd Time :      <<{1}>>\r\n{0}", Separator, dt.ToString("yyyy/MM/dd HH:mm:ss"));
            result += string.Format("{0}[Battery Pack Information]\r\n{0}", Separator);
            result += string.Format("{0}:{1}\r\n", "Device Name".PadRight(30), bat.DEVICE_NAME);
            result += string.Format("{0}:{1}\r\n", "Serial Number".PadRight(30), bat.batteryId);

            if (bat.MANUFACTURE_DATE != null)
            {
                result += string.Format("{0}:{1:0000}/{2:00}/{3:00}\r\n", "Manufacture Date".PadRight(30), bat.MANUFACTURE_DATE[0], bat.MANUFACTURE_DATE[1], bat.MANUFACTURE_DATE[2]);
            }
            else
            {
                result += string.Format("{0}:2000/01/01\r\n", "Manufacture Date".PadRight(30));
            }

            result += string.Format("{0}:{1} mV\r\n", "Design Voltage".PadRight(30), bat.DESIGN_VOLTAGE);
            result += string.Format("{0}:{1} mAh\r\n", "Design Capacity".PadRight(30), bat.DESIGN_CAPACITY);
            result += string.Format("{0}:{1} mAh\r\n", "Remain Capacity".PadRight(30), bat.REMAIN_CAPACITY);
            result += string.Format("{0}:{1} mAh\r\n", "Full Charge Capacity".PadRight(30), bat.FULL_CHARGE_CAPACITY);
            result += string.Format("{0}:{1}\r\n", "Cycle Counted".PadRight(30), bat.CYCLE_COUNT);

            result += string.Format("{0}[State Of Health]\r\n{0}", Separator);
            result += string.Format("{0}:{1} %\r\n", "Battery Health".PadRight(30), bat.SOH);

            result += string.Format("{0}[Relative SOC]\r\n{0}", Separator);
            result += string.Format("{0}:{1} %\r\n", "Relative SOC".PadRight(30), bat.SOC);

            result += string.Format("{0}[Recent Current]\r\n{0}", Separator);
            result += string.Format("{0}:{1} mA\r\n", "Current".PadRight(30), bat.CURRENT);

            result += string.Format("{0}[Pack Voltage]\r\n{0}", Separator);
            result += string.Format("{0}:{1} mV\r\n", "Voltage".PadRight(30), bat.TOTAL_VOLT);

            result += string.Format("{0}[Pack Temperature]\r\n{0}", Separator);
            result += string.Format("{0}:{1:0.00} ℃\r\n", "Battery Temperature".PadRight(30), bat.TEMPERATURE);

            result += string.Format("{0}[Usage Current Error Record]\r\n{0}", Separator);
            if (bat.PROTECTION != null)
            {
                if (bat.PROTECTION[1] > 0) result += string.Format("Error : Under Voltage Discharged\r\n");
                if (bat.PROTECTION[0] > 0) result += string.Format("Error : Over Voltage Charged\r\n");
                if (bat.PROTECTION[3] > 0) result += string.Format("Error : Over Current Discharged\r\n");
                if (bat.PROTECTION[2] > 0) result += string.Format("Error : Over Current Charged\r\n");
                if (bat.PROTECTION[4] > 0) result += string.Format("Error : Over Temp.\r\n");
                if (bat.PROTECTION[5] > 0) result += string.Format("Error : Under Temp.\r\n");
                if (bat.PROTECTION[6] > 0) result += string.Format("Error : Short Circuit\r\n");
            }

            result += string.Format("{0}[Cells Voltage]\r\n{0}", Separator);
            if (bat.CELL_VOLT != null)
            {
                uint[] tmpCellVolt = (uint[])this.batInfo["CELL_VOLT"];
                for (int i = 0; i < bat.CELL_VOLT.Length; i++)
                {
                    string name = string.Format("Cell[{0:00}] Volt.", (i + 1));
                    result += string.Format("{0}:{1} mV\r\n", name.PadRight(30), bat.CELL_VOLT[i]);
                }
            }

            result += string.Format("{0}:{1} mV\r\n", "Cell Voltage Delta".PadRight(30), bat.DELTA_VOLT);

            result += string.Format("{0}[Pack Detail Info.]\r\n{0}", Separator);
            result += string.Format("{0}:{1}\r\n", "MCU Firmware Ver".PadRight(30), bat.MCU_VERSION);

            if (bat.LAST_CHARGE != null)
            {
                result += string.Format("{0}:{1:00}/{2:00}/{3:00} {4:00}:{5:00}:{6:00} {7}%\r\n", "Last Charging Time".PadRight(30), bat.LAST_CHARGE[0], bat.LAST_CHARGE[1], bat.LAST_CHARGE[2], bat.LAST_CHARGE[3], bat.LAST_CHARGE[4], bat.LAST_CHARGE[5], bat.LAST_CHARGE[6]);
            }

            if (bat.LAST_DISCHARGE != null)
            {
                result += string.Format("{0}:{1:00}/{2:00}/{3:00} {4:00}:{5:00}:{6:00} {7}%\r\n", "Last Discharging Time".PadRight(30), bat.LAST_DISCHARGE[0], bat.LAST_DISCHARGE[1], bat.LAST_DISCHARGE[2], bat.LAST_DISCHARGE[3], bat.LAST_DISCHARGE[4], bat.LAST_DISCHARGE[5], bat.LAST_DISCHARGE[6]);
            }

            result += string.Format("{0}{1}{1}{1}{1}{1}{1}{1}{1}{1}{0}", Separator, "\r\n");


            string fileName = bat.batteryType + "_" + bat.batteryId + "_" + dt.ToString("yyyyMMddHHmmss");

            EncryptionAESNeil mEncryptionAlog = new EncryptionAESNeil();
            mEncryptionAlog.SetEncryptionKey(fileName);
            result += string.Format("{0}\r\n", Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.UTF8.GetBytes(sb.ToString()))));
            result += "!===============================================================";

            return result;
        }

        public void SetUserBatInfo(Dictionary<string, object> userBatInfo)
        {
            mUserBatInfo = userBatInfo;
        }

        public Dictionary<string, object> GetUserBatInfo()
        {
            return mUserBatInfo;
        }

        public void SetCOMPort(COMPort comPort)
        {
            mCOMPort = comPort;
        }

        public int ConnectCOMPort(string port, int baudRate)
        {
            return mCOMPort.Connect(port, baudRate);
        }

        public void DisonnectCOMPort()
        {
            mCOMPort.Disconnect();
        }

        public async Task<string> SetRTC(string rtcValue)
        {
            Dictionary<string, object> tmpBatInfo = new Dictionary<string, object>();
            List<BatOptions> tmpBatOptsList = new List<BatOptions>();
            BatCore.CmdConfig cmdConfig = new BatCore.CmdConfig() { Type = "DateTime", Value = rtcValue };
            DateTime obj = (DateTime)BatCore.ConvertStringToObj(cmdConfig.Type, cmdConfig.Value);

            if (!this.cmdSetting.enableASCII) tmpBatOptsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "eng" });

            tmpBatOptsList.Add(new BatOptions() { Action = "set", CmdName = "RTC", SetCmd = 0x05, SetPara = (byte)obj.Second });
            tmpBatOptsList.Add(new BatOptions() { Action = "set", CmdName = "RTC", SetCmd = 0x04, SetPara = (byte)obj.Minute });
            tmpBatOptsList.Add(new BatOptions() { Action = "set", CmdName = "RTC", SetCmd = 0x03, SetPara = (byte)obj.Hour });
            tmpBatOptsList.Add(new BatOptions() { Action = "set", CmdName = "RTC", SetCmd = 0x02, SetPara = (byte)obj.Day });
            tmpBatOptsList.Add(new BatOptions() { Action = "set", CmdName = "RTC", SetCmd = 0x01, SetPara = (byte)obj.Month });
            tmpBatOptsList.Add(new BatOptions() { Action = "set", CmdName = "RTC", SetCmd = 0x00, SetPara = (byte)(obj.Year - BatCore.DEFAULTYEAR) });

            if (!this.cmdSetting.enableASCII) tmpBatOptsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "nor" });

            var result = await UserMsg(tmpBatOptsList, tmpBatInfo);

            return (result.Error != null) ? "Setting Fail" : "Success";
        }

        public async Task<string> SetSerialNumber(string str)
        {
            Regex regex = new Regex(@"^\d{10}$");
            if (regex.IsMatch(str))
            {
                int year = BatCore.DEFAULTYEAR + int.Parse(str.Substring(2, 2));
                int week = int.Parse(str.Substring(4, 2));
                Dictionary<string, object> tmpBatInfo = new Dictionary<string, object>();
                List<BatOptions> tmpBatOptsList = new List<BatOptions>();

                if (!this.cmdSetting.enableASCII) tmpBatOptsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "eng" });
                
                BatCore.GetCmdListFromMCUObj("MANUFACTURE_PRODUCT", str.Substring(0, 2), tmpBatOptsList);
                BatCore.GetCmdListFromMCUObj("MANUFACTURE_DATE", BatCore.ISO8601WeekNumToThursday(year, week), tmpBatOptsList);
                BatCore.GetCmdListFromMCUObj("MANUFACTURE_WEEK", week, tmpBatOptsList);
                BatCore.GetCmdListFromMCUObj("BATTERY_ID", UInt32.Parse(str.Substring(6, 4)), tmpBatOptsList);

                if (!this.cmdSetting.enableASCII) tmpBatOptsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "nor" });

                var result = await UserMsg(tmpBatOptsList, tmpBatInfo);

                return (result.Error != null) ? "Setting Fail" : "Success";
            }
            return "String is not a valid Serial Number";
        }

        public async Task<string> SetMCUEEPCmd(string name, object obj)
        {
            List<BatOptions> optsList = new List<BatOptions>();
            if (!this.cmdSetting.enableASCII) optsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "eng" });
            BatCore.GetCmdListFromMCUObj(name, obj, optsList);
            if (!this.cmdSetting.enableASCII) optsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "nor" });

            var result = await UserMsg(optsList, new Dictionary<string, object>());
            return (result.Error != null) ? "Setting Fail" : "Success";
        }

        public async Task<string> SetPBEEPCmd(string name, object obj, string pbID)
        {
            byte[] pbEEP = null;
            if (this.batInfo.ContainsKey("PB_EEP"))
            {
                if (((Dictionary<string, byte[]>)this.batInfo["PB_EEP"]).ContainsKey(pbID))
                {
                    pbEEP = ((Dictionary<string, byte[]>)this.batInfo["PB_EEP"])[pbID];
                }
            }
            List<BatOptions> optsList = new List<BatOptions>();
            if (!this.cmdSetting.enableASCII) optsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "eng" });
            BatCore.GetCmdListFromPBObj(name, obj, pbID, pbEEP, optsList);
            if (!this.cmdSetting.enableASCII) optsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "nor" });
            
            var result = await UserMsg(optsList, new Dictionary<string, object>());

            // 要修改已經改過的值

            return (result.Error != null) ? "Setting Fail" : "Success";
        }

        public Dictionary<string, object> GetPBEEPData(string pbID)
        {
            if (this.batInfo.ContainsKey("PB_EEP") && (((Dictionary<string, byte[]>)this.batInfo["PB_EEP"]).ContainsKey(pbID)))
            {
                byte[] pbEEP = ((Dictionary<string, byte[]>)this.batInfo["PB_EEP"])[pbID];
                string[] cmdList = (from o in OZ890EEP.PARAMLIST select o.Name).ToArray();

                Dictionary<string, object> tmp = new Dictionary<string, object>();
                foreach (string cmd in cmdList)
                {
                    OZ890EEP.ConvertArrayToCmdInfo(cmd, pbEEP, tmp);
                }
                return tmp;
            }
            return null;
        }

        private async Task<int> GetMCUEEPIndex()
        { 
            var result = new BatOptions() { EnableRecv = true, Action = "get", CmdName = "MCU_EEP" };
            try
            {
                result = await this.SendMsg(result);
            }
            catch (Exception ex)
            {
                result.Error = string.Format("GetMCUEEPIndex Error({0})", ex.Message);
            }
            if (result.Result != null && result.Result.ContainsKey("MCU_EEP_INDEX"))
            {
                return (int)(result.Result["MCU_EEP_INDEX"]);
            }
            else
            {
                return 0;
            }
        }

        public async Task<string> ConnectBattery(string port)
        {
            if (port == "") return "Please check COM Port";

            try
            {
                for (int i = 0; i < 2; i++)
                {
                    foreach (var item in Slot.DEFAULTBAUDRATELIST)
                    {
                        this.cmdSetting.enableASCII = item.EnableASCII;
                        int result = this.ConnectCOMPort(port, item.BaudRate);
                        if (result == 0)
                        {
                            await this.CheckBattryExist(null);

                            if (this.autoSetting.isDetectedCmd) return "Success";
                            this.DisonnectCOMPort();
                        }
                        await Task.Delay(50);
                    }
                }
            }
            catch (Exception ex)
            {
                return string.Format("{0} connect Error({1})", port, ex.Message);
            }

            return string.Format("{0} not found GT Battery", port); 
        }

        private void ClearAutoRun()
        {
            if (mAutoRunCTS != null)
            {
                mAutoRunCTS.Cancel();
                mAutoRunCTS = null;
            }
        }

        public async void AutoRun()
        {
            mAutoRunCTS = new CancellationTokenSource();
            while (true)
            {
                SlotOptions slotOpts = await this.AutoMsg();

                // 使用者發送之訊息
                if (mUserCmdList.Count > 0)
                {
                    var batOptsList = mUserCmdList.ToList();
                    SlotOptions slotOpts2= await this.UserMsg(batOptsList, mUserBatInfo);

                    SendSlotErrorState errorState = SendSlotErrorState.None;
                    if (slotOpts2.Error != null)
                    {
                        errorState = SendSlotErrorState.SendError;
                    }

                    mUserCmdList.RemoveRange(0, slotOpts2.List.Count);
                }

                await Task.Delay(500, mAutoRunCTS.Token);
            }
        }

        public async Task<SlotOptions> CheckBattryExist(Dictionary<string, object> tmpbatInfo)
        {
            var result = new SlotOptions();
            result.BatInfo = (tmpbatInfo != null) ? tmpbatInfo : this.batInfo;
            if (this.baudRate == 38400)
            {
                result.List = BatCore.GetCmd(BatCore.GTGetCmd.Scan);
                result = await this.GetBatteryCmd(result);
            }
            else
            {
                result.List = BatCore.GetCmd(BatCore.GTGetCmd.ScanCAN);
                result = await this.GetBatteryCmd(result);
                if (result.Error != null)
                {
                    result.List = BatCore.GetCmd(BatCore.GTGetCmd.Scan);
                    result.Error = null;
                    result = await this.GetBatteryCmd(result);
                }
                else
                {
                    this.autoSetting.enableCANCmd = result.EnableCANCmd;
                }
            }

            if (result.Error == null)
            {
                this.autoSetting.retryTime = 0;
                this.autoSetting.isDetectedCmd = true;
                this.autoSetting.disableRefreshEEP = false;

                if (this.autoSetting.enableCANCmd)
                {
                    this.cmdSetting.enableASCII = true;
                }
            }

            return result;
        }

        public async Task<SlotOptions> UserMsg(List<BatOptions> batOpts, Dictionary<string, object> tmpbatInfo)
        {
            var result = new SlotOptions();
            result.List = batOpts;

            result.Error = null;
            result.BatInfo = (tmpbatInfo != null) ? tmpbatInfo : this.batInfo;
            result.EnableSkip = (!this.autoSetting.enableCANCmd);
            result.SkipTime = this.autoSetting.disableRefreshEEP ? 1 : 0;
            return await this.GetBatteryCmd(result);
        }

       public async Task<SlotOptions> AutoMsg()
        {
            var result = new SlotOptions();
            if (autoSetting.isDetectedCmd)
            {
                try
                {
                    DateTime tmpStartDT = DateTime.Now;
                    result.BatInfo = this.batInfo;

                    List<BatOptions> tmpCmdList = BatCore.GetCmd(BatCore.GTGetCmd.Profile);
                    if (this.autoSetting.enableCANCmd)
                    {
                        tmpCmdList = BatCore.GetCmd(BatCore.GTGetCmd.ScanCAN);
                    }

                    if (!this.autoSetting.isBatteryIn)
                    {
                        if (this.autoSetting.enableCANCmd)
                        {
                            result.List = BatCore.GetCmd(BatCore.GTGetCmd.BatInfoCAN).Concat(tmpCmdList).ToList();
                        }
                        else
                        {
                            result.List = BatCore.GetCmd(BatCore.GTGetCmd.Para);
                            result = await this.GetBatteryCmd(result);
                        }
                    }
                    // 判斷是否更新MCU EEP資料
                    if (!this.autoSetting.disableRefreshEEPFlag)
                    {
                        this.autoSetting.disableRefreshEEPFlag = true;
                        this.autoSetting.disableRefreshEEP = false;
                    }
                    Dictionary<string, object> scanBatInfo = this.batInfo;
                    if (!this.autoSetting.disableRefreshEEP)
                    {
                        tmpCmdList = tmpCmdList.Concat(BatCore.GetCmd(BatCore.GTGetCmd.CheckData)).ToList();
                        if (this.batInfo.ContainsKey("MCU_VERSION") && (string)this.batInfo["MCU_VERSION"] == "2.50Rev.09")
                        {
                            //  針對舊版4816讀取資料, 並轉換成共用的EEP格式
                            scanBatInfo = new Dictionary<string, object>();
                            result.List = result.List.Concat(BatCore.GetCmd(BatCore.GTGetCmd.EEP4816)).ToList();
                        }
                        else
                        {
                            int len = await GetMCUEEPIndex();
                            if (len == 0)
                            {
                                result.List = BatCore.GetCmd(BatCore.GTGetCmd.NonCheckData);
                                result = await this.GetBatteryCmd(result);
                            }

                            tmpCmdList = tmpCmdList.Concat(BatCore.GetMCUEEPArrayCmd(len)).ToList();
                        }
                    }

                    result.Error = null;
                    result.BatInfo = scanBatInfo;
                    result.EnableSkip = (!this.autoSetting.enableCANCmd);
                    result.SkipTime = this.autoSetting.disableRefreshEEP ? 1 : 0;
                    result.List = tmpCmdList;
                    result = await this.GetBatteryCmd(result);

                    if (this.autoSetting.enableCANCmd)
                    {
                        BatOptions batOptions = new BatOptions();
                        batOptions.EnableASCII = true;
                        batOptions.EnableRecv = true;
                        batOptions.FirstDelayTime = (!this.autoSetting.disableRefreshEEP) ? 3000 : 1000;
                        batOptions = await this.RecvMsg(batOptions);

                        this.CheckBatAttr(batOptions, result, batInfo);
                    }

                    SendSlotErrorState errorState = SendSlotErrorState.None;
                    if (result.Error != null)
                    {
                        if (!this.autoSetting.isBatteryIn)
                        {
                            this.autoSetting.isDetectedCmd = false;
                        }
                        else
                        {
                            errorState = SendSlotErrorState.SendError;
                        }
                    }
                    else
                    {
                        if (!this.autoSetting.disableRefreshEEP)
                        {
                            if (this.batInfo.ContainsKey("MCU_VERSION") && (string)this.batInfo["MCU_VERSION"] == "2.50Rev.09")
                            {
                                Dictionary<string, object> tmpBatInfo = BatCore.GetAttrInfo(Slot.PROFILEINFOLIST, scanBatInfo);
                                foreach (var item in tmpBatInfo)
                                {
                                    BatCore.CopyAttrData(this.batInfo, item.Key, item.Value);
                                }
                                // 轉成EEPArray
                                this.mMCU_EEP = PIC18EEP.CreateEEP(scanBatInfo);
                            }
                            if (this.mLOG_CHARGE.Count > 0)
                            {
                                Dictionary<string, object> tmpBatInfo = BatCore.ConvertArrayToCmdInfo("LOG_CHARGE", BatCore.ConcatEEPArray(this.mLOG_CHARGE), null);
                                foreach (var item in tmpBatInfo)
                                {
                                    BatCore.CopyAttrData(this.batInfo, item.Key, item.Value);
                                }
                                this.mLOG_CHARGE.Clear();
                            }
                            if (this.mMCU_EEP.Count > 0)
                            {
                                byte[] tmpEEPArray = BatCore.ConcatEEPArray(this.mMCU_EEP);
                                this.mMCU_EEP.Clear();
                                Dictionary<string, object> tmpBatInfo = BatCore.GetMCUCmdInfoList(Slot.BASICEEPINFOLIST, tmpEEPArray, null);
                                bool isCorrect = Utility.CheckAttr(tmpBatInfo, this.batInfo, BASICEEPINFOLIST);
                                if (!this.batInfo.ContainsKey("MCU_EEP") || isCorrect)
                                {
                                    this.autoSetting.disableRefreshEEP = true;
                                    foreach (var item in tmpBatInfo)
                                    {
                                        BatCore.CopyAttrData(this.batInfo, item.Key, item.Value);
                                    }
                                    tmpBatInfo = BatCore.GetMCUCmdInfoList(Slot.BASICEEPINFOLIST2, tmpEEPArray, null);
                                    foreach (var item in tmpBatInfo)
                                    {
                                        BatCore.CopyAttrData(this.batInfo, item.Key, item.Value);
                                    }
                                    BatCore.CopyAttrData(this.batInfo, "MCU_EEP", tmpEEPArray);
                                    BatCore.CopyAttrData(this.batInfo, "eepTimestamp", DateTime.UtcNow);
                                    Dictionary<string, object> tmpIDInfo = BatCore.GetMCUCmdInfoList(Slot.BATTERYIDLIST, tmpEEPArray, null);
                                    BatCore.CopyAttrData(tmpIDInfo, "MANUFACTURE_DATE", this.batInfo.ContainsKey("MANUFACTURE_DATE") ? this.batInfo["MANUFACTURE_DATE"] : new DateTime());
                                    BatCore.CopyAttrData(this.batInfo, "batteryId", BatCore.GetSerialNumber(tmpIDInfo));
                                    BatCore.CopyAttrData(this.batInfo, "SOH", ConvertMappingSOH(this.batInfo));
                                    BatCore.CopyAttrData(this.batInfo, "batteryType", this.batInfo.ContainsKey("DEVICE_NAME") ? this.batInfo["DEVICE_NAME"] : "");

                                    await this.RecheckBatteryInitState();
                                }
                                else
                                {
                                    errorState = SendSlotErrorState.EEPError;
                                }
                            }
                            else if (this.autoSetting.enableCANCmd)
                            {
                                this.autoSetting.disableRefreshEEP = true;
                                // Not GT Battery
                                BatCore.CopyAttrData(batInfo, "SOH", ConvertMappingSOH(this.batInfo));
                                await this.RecheckBatteryInitState();
                            }
                        }
                        else
                        {
                            BatCore.CopyAttrData(batInfo, "SOH", ConvertMappingSOH(batInfo));
                        }

                        BatCore.CopyAttrData(batInfo, "timestamp", DateTime.UtcNow);
                    }

                    switch (errorState)
                    {
                        case SendSlotErrorState.None:
                            {
                                this.autoSetting.retryTime = 0;
                                break;
                            }
                        default:
                            {
                                autoSetting.retryTime += 1;
                                DateTime tmpEndTime = DateTime.Now;

                                if (SendSlotErrorState.EEPError  == errorState)
                                {
                                    autoSetting.disableRefreshEEPFlag = true;
                                }

                                this.ClearEEPTimer();
                                this.ResetRefreshEEPTimer(REFRESHEEPTIMEOUT);
                                break;
                            }
                    }
                }
                catch (Exception ex)
                {
                    this.autoSetting.retryTime += 1;
                    result.Error = string.Format("AutoMsg Error({0})", ex.Message);
                }
            } else
            {
                result.Error = string.Format("AutoMsg no CheckBattryExist");
            }

            return result;
        }

        public void CheckBatAttr(BatOptions val, SlotOptions opts, Dictionary<string, object> btAttr)
        {
            if (val.Result != null)
            {
                if (val.RecvCANInfo.Count > 0)
                {
                    opts.EnableCANCmd = true;
                }

                if (val.Result.ContainsKey("MCU_EEP"))
                {
                    BatCore.CopyEEPData((Dictionary<string, byte[]>)val.Result["MCU_EEP"], mMCU_EEP);
                }
                else if (val.Result.ContainsKey("LOG_CHARGE"))
                {
                    BatCore.CopyEEPData((Dictionary<string, byte[]>)val.Result["LOG_CHARGE"], mLOG_CHARGE);
                }
                else if (val.Result.ContainsKey("PB_EEP"))
                {
                    if (!opts.BatInfo.ContainsKey("PB_EEP"))
                    {
                        opts.BatInfo.Add("PB_EEP", new Dictionary<string, byte[]>());
                    }
                    BatCore.CopyEEPData((Dictionary<string, byte[]>)val.Result["PB_EEP"], (Dictionary<string, byte[]>)opts.BatInfo["PB_EEP"]);
                }
                else if (val.Result.ContainsKey("CELL_TEMPERATURE"))
                {
                    bool isFirstData = true;
                    double[] cellTemp = (double[])val.Result["CELL_TEMPERATURE"];
                    List<double> newCellTemp = new List<double>();

                    double TEMPERATURE = 0;
                    double MAX_TEMPERATURE = 0;
                    double MIN_TEMPERATURE = 0;

                    for (int i = 0; i < cellTemp.Length; i += 1)
                    {
                        bool isInnerTemp = (i % 3) == 0;
                        if (!val.EnableASCII)
                        {
                            if (isInnerTemp)
                            {
                                // 轉換
                                Dictionary<string, byte[]> PB_EEP;
                                int ozValue = 0;
                                string ozNum = string.Format("{0}", (i / 3) + 1);
                                if (btAttr.ContainsKey("PB_EEP"))
                                {
                                    PB_EEP = (Dictionary<string, byte[]>)btAttr["PB_EEP"];
                                    if (PB_EEP[ozNum] != null && PB_EEP[ozNum].Length > 1)
                                    {
                                        ozValue = PB_EEP[ozNum][2];
                                    }
                                }
                                newCellTemp.Add(Math.Round(OZ890EEP.GetInternalTemperature(cellTemp[i], ozValue), 2));
                            }
                            else
                            {
                                newCellTemp.Add(Math.Round(OZ890EEP.GetExternalTemperature(cellTemp[i]), 2));
                            }
                        }
                        else
                        {
                            newCellTemp.Add(Math.Round(cellTemp[i], 2));
                        }

                        bool isMPSVer = btAttr.ContainsKey("MCU_VERSION") && ((string)btAttr["MCU_VERSION"] == "2.50Rev.09");
                        if (!isInnerTemp || isMPSVer)
                        {
                            double tempCellTemp = newCellTemp[i];
                            if (isFirstData)
                            {
                                isFirstData = false;
                                MAX_TEMPERATURE = tempCellTemp;
                                MIN_TEMPERATURE = tempCellTemp;
                            }
                            else if (MIN_TEMPERATURE > tempCellTemp)
                            {
                                MIN_TEMPERATURE = tempCellTemp;
                            }
                            else if (MAX_TEMPERATURE < tempCellTemp)
                            {
                                MAX_TEMPERATURE = tempCellTemp;
                            }
                        }
                    }

                    int[] protection = new int[] { 0, 0, 0, 0, 0, 0, 0 };
                    if (btAttr.ContainsKey("PROTECTION"))
                    {
                        protection = (int[])btAttr["PROTECTION"];
                    }
                    if (protection[5] > 0)
                    {
                        TEMPERATURE = MIN_TEMPERATURE;
                    }
                    else if (MIN_TEMPERATURE < 0)
                    {
                        TEMPERATURE = MIN_TEMPERATURE;
                    }
                    else
                    {
                        TEMPERATURE = MAX_TEMPERATURE;
                    }

                    BatCore.CopyAttrData(btAttr, "TEMPERATURE", TEMPERATURE);
                    BatCore.CopyAttrData(btAttr, "MAX_TEMPERATURE", MAX_TEMPERATURE);
                    BatCore.CopyAttrData(btAttr, "MIN_TEMPERATURE", MIN_TEMPERATURE);
                    BatCore.CopyAttrData(btAttr, "DELTA_TEMPERATURE", Math.Round(MAX_TEMPERATURE - MIN_TEMPERATURE, 2));
                    BatCore.CopyAttrData(btAttr, "CELL_TEMPERATURE", newCellTemp.ToArray());
                }
                else if (!val.Result.ContainsKey("CIPHER_UUID"))
                {
                    foreach (var item in val.Result)
                    {
                        BatCore.CopyAttrData(btAttr, item.Key, item.Value);
                    }
                }

                if (opts.EnableCANCmd)
                {
                    BatCore.RefreshBatInfo(btAttr);
                }
            }
            else
            {
                opts.Error = "\"val.Result\" is empty";
            }
        }
        private void  ClearEEPTimer()
        {
            if (mRefreshEEPCTS != null)
            {
                mRefreshEEPCTS.Cancel();
                mRefreshEEPCTS = null;
            }
        }
        private async Task<int> ResetRefreshEEPTimer(int timeout)
        {
            mRefreshEEPCTS = new CancellationTokenSource();
            while (true)
            {
                await Task.Delay(timeout, mRefreshEEPCTS.Token);
                autoSetting.disableRefreshEEPFlag = false;
            }
        }

        private async Task<int> RecheckBatteryInitState()
        {
            this.ClearEEPTimer();
            this.ResetRefreshEEPTimer(REFRESHEEPTIMEOUT);
            this.autoSetting.retryTime = 0;
            if (!this.autoSetting.isBatteryIn)
            {
                this.autoSetting.isBatteryIn = true;
                this.autoSetting.enableSchedule = false;
                this.lastBatInfo.Clear();
                //  重置充放電事件
                //this.notify.reset();
                //  啟動定時回報
                //this.startReport();
                //  通知事件
                //const tmpBatInfo2 = Utility.getAttrInfo(Object.keys(this.batInfo), this.batInfo);
                //        this.emit('detectBatteryIn', {
                //            slotId: this.slotId,
                //batInfo: tmpBatInfo2
                //        });
            }
            return 0;
        }

        public async Task<int> DisconnBattery()
        {
            this.DisonnectCOMPort();
            Dictionary<string, object> tmpBatInfo = new Dictionary<string, object>();
            foreach (var item in this.batInfo)
            {
                BatCore.CopyAttrData(tmpBatInfo, item.Key, item.Value);
            }
            AutoSetting setting = this.autoSetting;
            this.ClearEEPTimer();
            this.autoSetting = new AutoSetting();

            return 0;
        }

        public async Task<SlotOptions> GetBatteryCmd(SlotOptions opts)
        {
            var result = opts;
            Dictionary<string, object> btAttr = result.BatInfo;
            if (btAttr == null)
            {
                btAttr = batInfo;
            }
            if (result.List.Count == 0)
            {
                result.Error = "\"opts.List\" is empty";
                return result;
            }

            int errorNum = 0;
            foreach (var item in result.List)
            {
                if (item.Action == "get")
                {
                    if (item.CmdName == "PROTECTION" || item.CmdName == "RTC")
                    {
                        item.SetCmd = batInfo.ContainsKey("MCU_VERSION") ? batInfo["MCU_VERSION"] : "";
                    }
                }

                item.EnableRecvTimeout = result.EnableRecvTimeout;
                BatOptions batOpts = await this.SendMsg(item);
                this.CheckBatAttr(batOpts, result, btAttr);
                await Task.Delay(50);
                if (result.Error != null)
                {
                    errorNum += 1;

                    if (result.EnableSkip && (errorNum > result.SkipTime))
                    {
                        result.Error = string.Format("EnableSkip({0}) ErrorNum({1})", result.EnableSkip, errorNum);
                        break;
                    }

                    result.Error = null;
                }
            }

            if (!result.EnableSkip)
            {
                if (errorNum == result.List.Count)
                {
                    result.Error = string.Format("EnableSkip({0}) ErrorNum({1})", result.EnableSkip, errorNum);
                }
            }

            return result;
        }

        public async Task<BatOptions> RecvMsg(BatOptions opts)
        {
            var result = opts;
            int timeout = result.Timeout;
            if (result.EnableRecvTimeout)
            {
                await Task.Delay(timeout);
            }

            if (autoSetting.enableCANCmd)
            {
                if (!result.EnableRecv)
                {
                    return result;
                }
            }

            if (result.OptionStream != null)
            {
                BatParseInfo val = BatCore.ParseStream(result.OptionStream, result);
                if (val.DataList.Count > 0)
                {
                    result.DataList = val.DataList;
                }
            }
            else
            {
                bool isContinue = true;
                await Task.Delay(result.FirstDelayTime);
                long startTime = DateTime.Now.Ticks + (result.Timeout * 10000); // tick * 10000
                int i = 0;
                while (isContinue)
                {
                    i++;
                    BatParseInfo val = BatCore.ParseStream(mCOMPort.RecvData.ToArray(), result);
                    long endTime = DateTime.Now.Ticks;

                    if (!result.DisableParseStream)
                    {
                        if (val.DataList.Count > 0)
                        {
                            if (val.ClearLen > 0)
                            {
                                mCOMPort.RecvData.RemoveRange(0, val.ClearLen);
                            }
                            result.DataList = val.DataList;
                            isContinue = false;
                        }
                        else if (endTime > startTime)
                        {
                            if (val.ClearLen > 0)
                            {
                                mCOMPort.RecvData.RemoveRange(0, val.ClearLen);
                            }
                            result.Error = "recev timeout";
                            return opts;
                        }
                        else
                        {
                            if (val.ClearLen > 0)
                            {
                                mCOMPort.RecvData.RemoveRange(0, val.ClearLen);
                            }
                            await Task.Delay(50);
                        }
                    }
                    else if (endTime > startTime)
                    {
                        if (val.DataList.Count > 0)
                        {
                            result.DataList = val.DataList;
                        }
                        if (val.ClearLen > 0)
                        {
                            mCOMPort.RecvData.RemoveRange(0, val.ClearLen);
                        }
                        isContinue = false;
                    }
                    else
                    {
                        await Task.Delay(50);
                    }
                }
            }

            mBatCore.ParseMsg(result);
            return result;
        }

        public async Task<BatOptions> SendMsg(BatOptions opts)
        {
            var result = opts;
            try
            {
                result.EnableASCII = this.cmdSetting.enableASCII;
                result.EnableCipher = this.cmdSetting.enableCipher;
                result.SendTime = DateTime.Now;
                mBatCore.SetMsg(result);
                if (result.Error != null)
                {
                    return result;
                }

                int val = mCOMPort.SendData(result.SendStream);
                if (val != 0)
                {
                    result.Error = "Writing to COM port Error";
                    return result;
                }

                result = await this.RecvMsg(result);
                result.RecveTime = DateTime.Now;

                long dt = result.RecveTime.Ticks - result.SendTime.Ticks;
            }
            catch (Exception ex)
            {
                result.Error = string.Format("SendMsg Error({})", ex.Message);
            }

            return result;
        }
    }
}
