﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTCore
{
    public class BatAttr
    {
        public string TypeName { get; set; }
        public string Key { get; set; }
        public object Value { get; set; }
    }

    public class BatParseInfo
    {
        private List<List<byte>> mDataList = new List<List<byte>>();
        public int ClearLen { get; set; }
        public List<List<byte>> DataList { get { return mDataList; } }
    }

    public class BatOptions
    {
        public string CmdName { get; set; }
        public string Action { get; set; }
        public string Error { get; set; }
        public byte STX { get; set; }
        public byte Len { get; set; }
        public byte Para { get; set; }
        public byte Cmd { get; set; }
        public byte[] Data { get; set; }
        public byte ETX { get; set; }
        public bool EnableRecv { get; set; }
        public bool EnableASCII { get; set; }
        public bool EnableCipher { get; set; }
        public bool EnableRecvTimeout { get; set; }
        public bool DisableParseStream { get; set; }
        public bool IsCANMode { get; set; }
        public ulong CANID { get; set; }
        public byte CANType { get; set; }
        public byte[] CANData { get; set; }
        public int FirstDelayTime { get; set; }
        public int Timeout { get; set; }
        public byte SetData { get; set; }
        public object SetCmd { get; set; }
        public object SetPara{ get; set; }
        public int SetTimeout { get; set; }
        public byte[] SendStream { get; set; }
        public List<List<byte>> DataList { get; set; }
        public byte[] RecvStream { get; set; }
        public byte[] UnpackStream { get; set; }
        public byte[] OptionStream { get; set; }
        public DateTime SendTime { get; set; }
        public DateTime RecveTime { get; set; }
        public Dictionary<string, object> Result { get; set; }
        public List<GTCAN.GTCANRecvInfo> RecvCANInfo { get; set; }
        public List<byte[]> UnpackStreamList { get; set; }

        public BatOptions()
        {
            CmdName = null;
            Action = "get";
            Error = null;
            STX = BatCore.STX;
            ETX = BatCore.ETX;
            Len = 0x02;
            Para = 0x00;
            FirstDelayTime = 100;
            Timeout = 100;
            SetCmd = null;
            SetPara = null;
            SetTimeout = 100;
            EnableRecv = true;
            EnableASCII = true;
            EnableCipher = true;
            DisableParseStream = false;
            RecvCANInfo = new List<GTCAN.GTCANRecvInfo>();
            UnpackStreamList = new List<byte[]>();
        }
    }

    public class BatCore
    {
        public enum CmdAttrType
        {
            MCU_EEP = 0,
            BAT = 1,
            OZ1_EEP = 2,
            OZ2_EEP = 3
        }

        public enum CmdOperatorType
        {
            NonOperator = 0,
            Equal = 1,
            NotEqual = 2,
            MoreThan = 3,
            LessThan = 4,
            MoreThanOrEqual = 5,
            LessThanOrEqual = 6
        }

        public class CmdConfigResult
        {
            public string Type { get; set; }
            public string Name { get; set; }
            public string DefaultValue { get; set; }
            public string Condition { get; set; }
            public string RealValue { get; set; }
            public bool IsMatch { get; set; }
        }

        public class CmdConfig
        {
            public CmdAttrType AttrType { get; set; }
            public bool IsHidden { get; set; }
            public bool IsReadOnly { get; set; }
            public string Type { get; set; }
            public string Name { get; set; }
            public int Address { get; set; }
            public int Length { get; set; }
            public CmdOperatorType Opts { get; set; }
            public string Value { get; set; }
        }
        public static int DEFAULTYEAR = 2000;
        public static byte STX = 0x02;
        public static byte ETX = 0x03;
        public static byte CANSTX = 0x04;

        public enum DefaultBatCmd : byte
        {
            PB_EEP = 0x00,
            PB_BYTE_EEP = 0x97,
            PB_BYTE_OP = 0x99,
            MCU_EEP_HL = 0x02,
            MCU_EEP = 0x65,
            MCU_BYTE_EEP = 0x93,
            // Dynamic
            RTC = 0x2A,
            SOC = 0x07,
            CELL_VOLT = 0x04,
            CURRENT = 0x05,
            TEMPERATURE = 0x09,
            REMAIN_CAPACITY = 0x06,
            FULL_CHARGE_CAPACITY = 0x0D,
            PROTECTION = 0x0A,
            CYCLE_COUNT = 0x08,
            MCU_RESET_COUNT = 0x13,
            DISCHARGE_INTERVAL = 0x63,
            TEMPERATURE_INTERVAL = 0x66,
            LAST_CHARGE = 0x28,
            LAST_DISCHARGE = 0x29,
            // Static
            MCU_VERSION = 0x14,
            DEVICE_NAME = 0x0E,
            BATTERY_ID = 0x11,
            MANUFACTURE_DATE = 0x12,
            DESIGN_CAPACITY = 0x0B,
            DESIGN_VOLTAGE = 0x0C,
            FCC_RESET_TIME = 0x61,
            LOG_DISCHARGE = 0x62,
            RTC_RESET_TIME = 0x64,
            BARCODE = 0x1D,
            // EEP Support
            LOG_CHARGE = 0x2D,
            ALERTHAPPENTIME = 0xAF,
            // SYS ACT
            RESET_PB = 0x92,
            RESET_MCU = 0xAA,
            CIPHER_CMD = 0xC0,
            CIPHER_UUID = 0xC1,
            SWITCH_MODE = 0x3E,
            SYS_MSG = 0xF1,
            CAN_MODE_STATUS = 0xCA
        }

        public enum DefaultParseBatCmd : byte
        {
            MCU_EEP = 0x65,
            MCU_EEP_H = 0x02,
            MCU_EEP_L = 0x03,
            PB_EEP_OZ1 = 0x00,
            PB_EEP_OZ2 = 0x01,
            MCU_VERSION = 0x14,
            DEVICE_NAME = 0x0E,
            BATTERY_ID = 0x11,
            CELL_VOLT = 0x04,
            CURRENT = 0x05,
            REMAIN_CAPACITY = 0x06,
            FULL_CHARGE_CAPACITY = 0x0D,
            DESIGN_CAPACITY = 0x0B,
            DESIGN_VOLTAGE = 0x0C,
            SOC = 0x07,
            CYCLE_COUNT = 0x08,
            MCU_RESET_COUNT = 0x13,
            BARCODE = 0x1D,
            TEMPERATURE = 0x09,
            PROTECTION = 0x0A,
            LAST_CHARGE = 0x28,
            LAST_DISCHARGE = 0x29,
            RTC_TIMESET = 0x27,
            RTC_REG_DUMP = 0x2A,
            MANUFACTURE_DATE = 0x12,
            OV_HAPPENTIME = 0xAF,
            UV_HAPPENTIME = 0xB0,
            COC_HAPPENTIME = 0xB1,
            SC_HAPPENTIME = 0xB2,
            OT_HAPPENTIME = 0xB3,
            UT_HAPPENTIME = 0xB4,
            DOC_HAPPENTIME = 0xB5,
            CHARGE_LOG_1 = 0x2D,
            CHARGE_LOG_2 = 0x2E,
            CHARGE_LOG_3 = 0x2F,
            FCC_RESET_TIME = 0x61,
            DISCHARGE_LOG = 0x62,
            DISCHARGE_INTERVAL = 0x63,
            RTC_RESET_TIME = 0x64,
            TEMPERATURE_INTERVAL = 0x66,
            RTC_YEAR_SET = 0xA0,
            RTC_MONTH_SET = 0xA1,
            RTC_DAY_SET = 0xA2,
            RTC_HOUR_SET = 0xA3,
            RTC_MIN_SET = 0xA4,
            RTC_SEC_SET = 0xA5,
            RESET_PB = 0x92,
            RESET_MCU = 0xAA,
            MCU_BYTE_EEP_GET = 0x93,
            PB_BYTE_EEP_OZ1_GET = 0x97,
            PB_BYTE_EEP_OZ2_GET = 0x98,
            PB_BYTE_OP_OZ1_GET = 0x99,
            PB_BYTE_OP_OZ2_GET = 0x9A,
            MCU_BYTE_EEP_SET = 0x91,
            PB_BYTE_EEP_OZ1_SET = 0x9C,
            PB_BYTE_EEP_OZ2_SET = 0x9D,
            PB_BYTE_OP_OZ1_SET = 0x9E,
            PB_BYTE_OP_OZ2_SET = 0x9F,
            RESET_PBPOWER = 0xAD,
            CIPHER_UUID = 0xC1,
            SWITCH_MODE = 0x3E,
            SYS_MSG = 0xF1,
            CAN_MODE_STATUS_GET = 0xCA,
            CAN_MODE_STATUS_SET = 0xCB
        }

        public enum GTGetCmd
        {
            ScanCAN,
            BatInfoCAN,
            Scan,
            Para,
            CheckData,
            NonCheckData,
            CBData,
            Profile,
            EEP4816,
            MCU_BYTE_EEP,
            PB_BYTE_EEP
        }
        
        public static List<BatCore.CmdConfig> BATPARAMLIST = new List<CmdConfig>()
        {
            new CmdConfig() { Name = "SOC", IsReadOnly = true, Type = "System.UInt32" },
            new CmdConfig() { Name = "SOH", IsReadOnly = true, Type = "System.Int32" },
            new CmdConfig() { Name = "PROTECTION", IsReadOnly = true, Type = "System.Int32[]" },
            new CmdConfig() { Name = "TOTAL_VOLT", IsReadOnly = true, Type = "System.UInt64" },
            new CmdConfig() { Name = "MIN_VOLT", IsReadOnly = true, Type = "System.UInt32" },
            new CmdConfig() { Name = "MAX_VOLT", IsReadOnly = true, Type = "System.UInt32" },
            new CmdConfig() { Name = "DELTA_VOLT", IsReadOnly = true, Type = "System.UInt32" },
            new CmdConfig() { Name = "CELL_NUM", IsReadOnly = true, Type = "System.Int32" },
            new CmdConfig() { Name = "TEMPERATURE", IsReadOnly = true, Type = "System.Double" },
            new CmdConfig() { Name = "CURRENT", IsReadOnly = true, Type = "System.Int64" },
            new CmdConfig() { Name = "REMAIN_CAPACITY", IsReadOnly = true, Type = "System.UInt64" }
        };

        //public static List<BatCore.CmdConfig> MCUPARAMLIST = new List<CmdConfig>()
        //{
        //    new CmdConfig() { Name = "RTC", IsReadOnly = true, Type = "System.DateTime" }
        //};

        public static string[] PROTECTIONLIST = new string[] { "Over Voltage", "Under Voltage", "Over Current Charge", "Over Current Discharge", "Over Temperature", "Under Temperature", "Short Circuit" };
        
        public static string OptsStr(CmdOperatorType opts)
        {
            switch (opts)
            {
                case BatCore.CmdOperatorType.Equal:
                    {
                        return "==";
                    }
                case BatCore.CmdOperatorType.NotEqual:
                    {
                        return "!=";
                    }
                case BatCore.CmdOperatorType.LessThan:
                    {
                        return "<";
                    }
                case BatCore.CmdOperatorType.LessThanOrEqual:
                    {
                        return "<=";
                    }
                case BatCore.CmdOperatorType.MoreThan:
                    {
                        return ">";
                    }
                case BatCore.CmdOperatorType.MoreThanOrEqual:
                    {
                        return ">=";
                    }
                default:
                    {
                        return "";
                    }
            }
        }

        public static bool OperatorValue(string type, string baseVal, string realValue, CmdOperatorType opts)
        {
            if (opts == CmdOperatorType.NonOperator) return true;
            switch (type)
            {
                case "System.Boolean":
                    {
                        Boolean result1 = (Boolean)ConvertStringToObj(type, baseVal);
                        Boolean result2 = (Boolean)ConvertStringToObj(type, realValue);

                        return (opts == CmdOperatorType.NotEqual) ? (result1 != result2) : (result1 == result2);
                    }
                case "System.Double":
                    {
                        switch (opts)
                        {
                            case CmdOperatorType.NotEqual:
                                {
                                    return (System.Double)ConvertStringToObj(type, baseVal) != (System.Double)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.LessThan:
                                {
                                    return (System.Double)ConvertStringToObj(type, baseVal) < (System.Double)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.LessThanOrEqual:
                                {
                                    return (System.Double)ConvertStringToObj(type, baseVal) <= (System.Double)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.MoreThan:
                                {
                                    return (System.Double)ConvertStringToObj(type, baseVal) > (System.Double)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.MoreThanOrEqual:
                                {
                                    return (System.Double)ConvertStringToObj(type, baseVal) >= (System.Double)ConvertStringToObj(type, realValue);
                                }
                            default:
                                {
                                    return (System.Double)ConvertStringToObj(type, baseVal) == (System.Double)ConvertStringToObj(type, realValue);
                                }
                        }
                    }
                case "System.UInt64":
                    {
                        switch (opts)
                        {
                            case CmdOperatorType.NotEqual:
                                {
                                    return (System.UInt64)ConvertStringToObj(type, baseVal) != (System.UInt64)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.LessThan:
                                {
                                    return (System.UInt64)ConvertStringToObj(type, baseVal) < (System.UInt64)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.LessThanOrEqual:
                                {
                                    return (System.UInt64)ConvertStringToObj(type, baseVal) <= (System.UInt64)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.MoreThan:
                                {
                                    return (System.UInt64)ConvertStringToObj(type, baseVal) > (System.UInt64)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.MoreThanOrEqual:
                                {
                                    return (System.UInt64)ConvertStringToObj(type, baseVal) >= (System.UInt64)ConvertStringToObj(type, realValue);
                                }
                            default:
                                {
                                    return (System.UInt64)ConvertStringToObj(type, baseVal) == (System.UInt64)ConvertStringToObj(type, realValue);
                                }
                        }
                    }
                case "System.UInt32":
                    {
                        switch (opts)
                        {
                            case CmdOperatorType.NotEqual:
                                {
                                    return (System.UInt32)ConvertStringToObj(type, baseVal) != (System.UInt32)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.LessThan:
                                {
                                    return (System.UInt32)ConvertStringToObj(type, baseVal) < (System.UInt32)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.LessThanOrEqual:
                                {
                                    return (System.UInt32)ConvertStringToObj(type, baseVal) <= (System.UInt32)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.MoreThan:
                                {
                                    return (System.UInt32)ConvertStringToObj(type, baseVal) > (System.UInt32)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.MoreThanOrEqual:
                                {
                                    return (System.UInt32)ConvertStringToObj(type, baseVal) >= (System.UInt32)ConvertStringToObj(type, realValue);
                                }
                            default:
                                {
                                    return (System.UInt32)ConvertStringToObj(type, baseVal) == (System.UInt32)ConvertStringToObj(type, realValue);
                                }
                        }
                    }
                case "System.Int64":
                    {
                        switch (opts)
                        {
                            case CmdOperatorType.NotEqual:
                                {
                                    return (System.Int64)ConvertStringToObj(type, baseVal) != (System.Int64)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.LessThan:
                                {
                                    return (System.Int64)ConvertStringToObj(type, baseVal) < (System.Int64)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.LessThanOrEqual:
                                {
                                    return (System.Int64)ConvertStringToObj(type, baseVal) <= (System.Int64)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.MoreThan:
                                {
                                    return (System.Int64)ConvertStringToObj(type, baseVal) > (System.Int64)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.MoreThanOrEqual:
                                {
                                    return (System.Int64)ConvertStringToObj(type, baseVal) >= (System.Int64)ConvertStringToObj(type, realValue);
                                }
                            default:
                                {
                                    return (System.Int64)ConvertStringToObj(type, baseVal) == (System.Int64)ConvertStringToObj(type, realValue);
                                }
                        }
                    }
                case "System.Int32":
                    {
                        switch (opts)
                        {
                            case CmdOperatorType.NotEqual:
                                {
                                    return (System.Int32)ConvertStringToObj(type, baseVal) != (System.Int32)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.LessThan:
                                {
                                    return (System.Int32)ConvertStringToObj(type, baseVal) < (System.Int32)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.LessThanOrEqual:
                                {
                                    return (System.Int32)ConvertStringToObj(type, baseVal) <= (System.Int32)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.MoreThan:
                                {
                                    return (System.Int32)ConvertStringToObj(type, baseVal) > (System.Int32)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.MoreThanOrEqual:
                                {
                                    return (System.Int32)ConvertStringToObj(type, baseVal) >= (System.Int32)ConvertStringToObj(type, realValue);
                                }
                            default:
                                {
                                    return (System.Int32)ConvertStringToObj(type, baseVal) == (System.Int32)ConvertStringToObj(type, realValue);
                                }
                        }
                    }
                case "System.Byte":
                    {
                        switch (opts)
                        {
                            case CmdOperatorType.NotEqual:
                                {
                                    return (System.Byte)ConvertStringToObj(type, baseVal) != (System.Byte)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.LessThan:
                                {
                                    return (System.Byte)ConvertStringToObj(type, baseVal) < (System.Byte)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.LessThanOrEqual:
                                {
                                    return (System.Byte)ConvertStringToObj(type, baseVal) <= (System.Byte)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.MoreThan:
                                {
                                    return (System.Byte)ConvertStringToObj(type, baseVal) > (System.Byte)ConvertStringToObj(type, realValue);
                                }
                            case CmdOperatorType.MoreThanOrEqual:
                                {
                                    return (System.Byte)ConvertStringToObj(type, baseVal) >= (System.Byte)ConvertStringToObj(type, realValue);
                                }
                            default:
                                {
                                    return (System.Byte)ConvertStringToObj(type, baseVal) == (System.Byte)ConvertStringToObj(type, realValue);
                                }
                        }
                    }
                case "System.DateTime":
                    {
                        DateTime result1 = (DateTime)ConvertStringToObj(type, baseVal);
                        DateTime result2 = (DateTime)ConvertStringToObj(type, realValue);
                        switch (opts)
                        {
                            case CmdOperatorType.NotEqual:
                                {
                                    return result1.Ticks != result2.Ticks;
                                }
                            case CmdOperatorType.LessThan:
                                {
                                    return result1.Ticks < result2.Ticks;
                                }
                            case CmdOperatorType.LessThanOrEqual:
                                {
                                    return result1.Ticks <= result2.Ticks;
                                }
                            case CmdOperatorType.MoreThan:
                                {
                                    return result1.Ticks > result2.Ticks;
                                }
                            case CmdOperatorType.MoreThanOrEqual:
                                {
                                    return result1.Ticks >= result2.Ticks;
                                }
                            default:
                                {
                                    return result1.Ticks == result2.Ticks;
                                }
                        }
                    }
                default:
                    {
                        return baseVal == realValue;
                    }
            }
        }

        public static string ConvertObjToString(object value)
        {
            try
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(value);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static object ConvertStringToObj(string type, string value)
        {
            try
            {
                if (type == "System.String") return value;
                object tmpValue = Newtonsoft.Json.JsonConvert.DeserializeObject(value);
                switch (type)
                {
                    case "System.DateTime":
                        {
                            return tmpValue;
                        }
                    case "System.Double":
                    case "System.UInt64":
                    case "System.UInt32":
                    case "System.Int64":
                    case "System.Int32":
                    case "System.Byte":
                        {
                            Type tmpType = Type.GetType(type);
                            if (tmpType != null) return Convert.ChangeType(value, tmpType);
                            return null;
                        }
                    default:
                        {
                            Type tmpType = Type.GetType(type);
                            return  ((Newtonsoft.Json.Linq.JArray)tmpValue).ToObject(tmpType);
                        }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        
        public static byte ConvertHEXToASCII(byte inputData)
        {
            if (inputData < 0x0A)
            {
                return (byte)(inputData + 0x30);
            }
            else
            {
                return (byte)(inputData - 10 + 0x41);
            }
        }

        public static byte ConvertASCIIToHEX(byte inputData)
        {
            if (inputData >= 0x41)
            {
                return (byte)(inputData - 0x41 + 10);
            }
            else if (inputData >= 0x30)
            {
                return (byte)(inputData - 0x30);
            }
            else
            {
                return inputData;
            }
        }

        private static uint PushChar(List<byte> array, byte data, bool enableASCII)
        {
            if (enableASCII)
            {
                array.Add(BatCore.ConvertHEXToASCII((byte)(data >> 4)));
                array.Add(BatCore.ConvertHEXToASCII((byte)(data & 0x0F)));
            }
            else
            {
                array.Add(data);
            }
            return (uint)data;
        }

        public static void CopyEEPData(Dictionary<string, byte[]> input, Dictionary<string, byte[]> output)
        {
            foreach (var item in input)
            {
                if (output.ContainsKey(item.Key))
                {
                    output[item.Key] = item.Value;
                }
                else
                {
                    output.Add(item.Key, item.Value);
                }
            }
        }

        public static void CopyAttrData(Dictionary<string, object> output, string key, object value)
        {
            if (output.ContainsKey(key))
            {
                output[key] = value;
            }
            else
            {
                output.Add(key, value);
            }
        }

        public static Dictionary<string, object> GetAttrInfo(string[] attrList, Dictionary<string, object> batInfo) {
            Dictionary<string, object> tmpBatInfo = new Dictionary<string, object>();

            int len = attrList.Length;
            for (int i = 0; i < len; i += 1) {
                string attr = attrList[i];
                if (batInfo.ContainsKey(attr)) {
                    BatCore.CopyAttrData(tmpBatInfo, attr, batInfo[attr]);
                }
            }

            return tmpBatInfo;
        }

        public static byte[] ConcatEEPArray(Dictionary<string, byte[]> eepList)
        {
            List<byte> tmpArray = new List<byte>();
            foreach (var item in eepList)
            {
                tmpArray = tmpArray.Concat(item.Value.ToList()).ToList();
            }
            return tmpArray.ToArray();
        }

        public static string ConvertAttrToString(string key, object value)
        {
            switch (key)
            {
                case "LAST_CHARGE":
                case "LAST_DISCHARGE":
                    {
                        Int32[] data = (Int32[])value;
                        return string.Format("{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00} {6:000}%", data[0], data[1], data[2], data[3], data[4], data[5], data[6]);
                    }
                case "PROTECTION":
                    {
                        Int32[] data = (Int32[])value;
                        return string.Format("OV: {0}, UV:{1}, COC:{2}, DOC:{3}, OT:{4}, UT:{5}, SC:{6}", data[0], data[1], data[2], data[3], data[4], data[5], data[6]);
                    }
                case "CAN_PACK_STATE":
                    {
                        byte data = (byte)value;
                        if (data == 0)
                        {
                            return "Charge";
                        }
                        else if (data == 1)
                        {
                            return "Discharge";
                        }
                        else
                        {
                            return "Idle";
                        }
                    }
                default:
                    {
                        return value.ToString();
                    }
            }
        }

        public static string[] GetBatCmdNameList()
        {
            return (from o in BatCore.BATPARAMLIST select o.Name).ToArray();
        }

        public static List<CmdConfig> GetMCUEEPCmdConfigList()
        {
            return PIC18EEP.PARAMLIST;
        }

        public static string[] GetMCUEEPCmdNameList()
        {
            return (from o in PIC18EEP.PARAMLIST select o.Name).ToArray();
        }

        public static List<BatOptions> GetMCUEEPArrayCmd(int index)
        {
            if (index == 0)
            {
                return BatCore.GetCmd(BatCore.GTGetCmd.CBData);
            }

            List<BatOptions> opts = new List<BatOptions>();
            for (int i = 0; i < index; i += 1)
            {
                opts.Add(new BatOptions() { SetPara = (byte)(i + 1), Action = "get", CmdName = "MCU_EEP" });
            }
            return opts;
        }

        public static Dictionary<string, object> GetMCUCmdInfoList(string[] cmdList, byte[] data, Dictionary<string, object> val)
        {
            if (val == null) val = new Dictionary<string, object>();

            int len = cmdList.Length;
            for (int i = 0; i < len; i += 1)
            {
                string cmd = cmdList[i];
                Dictionary<string, object> tmpBatInfo = BatCore.ConvertArrayToCmdInfo(cmd, BatCore.GetMCUCmdRawArray(cmd, data), null);
                foreach (var item in tmpBatInfo)
                {
                    BatCore.CopyAttrData(val, item.Key, item.Value);
                }
            }
            return val;
        }

        public static byte[] GetMCUCmdRawArray(string cmd, byte[] data)
        {
            CmdConfig cmdConfig = BatCore.GetMCUCmdConfig(cmd);

            byte[] value = null;
            if (cmdConfig != null && cmdConfig.Length > 0 && data.Length > (cmdConfig.Address + cmdConfig.Length))
            {
                Array.Resize(ref value, cmdConfig.Length);
                Array.Copy(data, cmdConfig.Address, value, 0, cmdConfig.Length);
            }
            else
            {
                value = new byte[0];
            }

            return value;
        }

        public static CmdConfig GetMCUCmdConfig(string cmd)
        {
            return PIC18EEP.GetCmdConfig(cmd);
        }

        public static byte[] ConvertMCUObjToArray(string cmd, object value)
        {
            return PIC18EEP.ConvertObjToArray(cmd, value);
        }

        public static List<BatOptions> GetCmdListFromMCUObj(string name, object value, List<BatOptions> batOpts)
        {
            if (batOpts == null) batOpts = new List<BatOptions>();

            CmdConfig config = BatCore.GetMCUCmdConfig(name);
            if (config == null) return batOpts;

            byte[] obj = BatCore.ConvertMCUObjToArray(name, value);
            for (int i = 0; i < config.Length; i++)
            {
                batOpts.Add(new BatOptions() { Action = "set", CmdName = "MCU_BYTE_EEP", SetPara = Convert.ToByte(config.Address + i), SetData = Convert.ToByte(obj[i]) });
            }
            return batOpts;
        }

        public static List<CmdConfig> GetPBEEPCmdConfigList()
        {
            return OZ890EEP.PARAMLIST;
        }

        public static CmdConfig GetPBCmdConfig(string cmd)
        {
            return OZ890EEP.GetCmdConfig(cmd);
        }

        public static byte[] GetPBCmdRawArray(string cmd, byte[] data)
        {
            CmdConfig cmdConfig = BatCore.GetPBCmdConfig(cmd);

            byte[] value = null;
            if (cmdConfig != null && cmdConfig.Length > 0 && data.Length > (cmdConfig.Address + cmdConfig.Length))
            {
                Array.Resize(ref value, cmdConfig.Length);
                Array.Copy(data, cmdConfig.Address, value, 0, cmdConfig.Length);
            }
            else
            {
                value = new byte[0];
            }

            return value;
        }

        public static byte[] ConvertPBObjToArray(string cmd, object value, byte[] pbEEP)
        {
            return OZ890EEP.ConvertObjToArray(cmd, value, pbEEP);
        }

        public static List<BatOptions> GetCmdListFromPBObj(string name, object value, string pbID, byte[] pbEEP, List<BatOptions> batOpts)
        {
            if (batOpts == null) batOpts = new List<BatOptions>();

            CmdConfig config = BatCore.GetPBCmdConfig(name);
            if (config == null) return batOpts;

            // 修改已經被改過的PB EEP值
            byte[] obj = BatCore.ConvertPBObjToArray(name, value, pbEEP);
            if (obj.Length > 0) Array.Copy(obj, 0, pbEEP, config.Address, obj.Length);

            Int32 pbIndex = (pbID == "1") ? 1 : 2;
            for (int i = 0; i < config.Length; i++)
            {
                batOpts.Add(new BatOptions() { Action = "set", CmdName = "PB_BYTE_EEP", SetCmd= pbIndex, SetPara = Convert.ToByte(config.Address + i), SetData = obj[i] });
            }
            return batOpts;
        }
        /// <summary>
        /// 轉換至ISO8601標準的週別日期時間
        /// </summary>
        /// <param name="year"></param>
        /// <param name="weekOfYear"></param>
        /// <returns></returns>
        public static Int32[] ISO8601WeekNumToThursday(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1); //當年分第一天
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;//移動至星期三

            DateTime firstThursday = jan1.AddDays(daysOffset);//當周星期四時間
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            firstThursday = firstThursday.AddDays(weekNum * 7);

            return new Int32[] { firstThursday.Year, firstThursday.Month, firstThursday.Day };
        }

        public static string GetSerialNumber(Dictionary<string, object> batInfo)
        {
            return PIC18EEP.GetSerialNumber(batInfo);
        }
        
        public static Dictionary<string, object> ConvertArrayToCmdInfo(string cmd, byte[] data, Dictionary<string, object> val)
        {
            return PIC18EEP.ConvertArrayToCmdInfo(cmd, data, val);
        }

        private static void SetCmdInfo(BatOptions opts)
        {
            if (opts.CANID != 0)
            {
                GTCAN.SetCmdInfo(opts);
                return;
            }

            if (opts.CmdName == null)
            {
                opts.Error = "\"options.CmdName\" is invaild";
                return;
            }

            if (opts.Action == null)
            {
                opts.Error = "\"opts.Action\" is not a string";
                return;
            }

            try
            {
                DefaultBatCmd cmd = (DefaultBatCmd)System.Enum.Parse(typeof(DefaultBatCmd), opts.CmdName);

                if (opts.Action == "get")
                {
                    opts.Cmd = (byte)cmd;
                    switch (opts.CmdName)
                    {
                        case "MCU_EEP_HL":
                        case "PB_EEP":
                        case "LOG_CHARGE":
                            {
                                int tmpCmd = ((int)opts.SetCmd > 0x00) ? ((int)opts.SetCmd - 0x01) : 0x00;
                                opts.Cmd = (byte)(cmd + (byte)tmpCmd);
                                opts.Para = (opts.SetPara != null) ? ((byte)opts.SetPara) : (byte)0x00;
                                opts.Timeout = 500;
                                break;
                            }
                        case "MCU_EEP":
                            {
                                opts.Para = (opts.SetPara != null) ? ((byte)opts.SetPara) : (byte)0x00;

                                if (opts.Para != 0x00)
                                {
                                    opts.Timeout = 500;
                                }
                                break;
                            }
                        case "PB_BYTE_EEP":
                        case "PB_BYTE_OP":
                            {
                                int tmpCmd = ((int)opts.SetCmd > 0x00) ? ((int)opts.SetCmd - 0x01) : 0x00;
                                opts.Cmd = (byte)(cmd + (byte)tmpCmd);
                                opts.Para = (opts.SetPara != null) ? ((byte)opts.SetPara) : (byte)0x00;
                                break;
                            }
                        case "MCU_BYTE_EEP":
                            {
                                opts.Cmd = 0x93;
                                opts.Para = (opts.SetPara != null) ? ((byte)opts.SetPara) : (byte)0x00;
                                break;
                            }
                        case "RTC":
                            {
                                opts.Cmd = (byte)(((string)opts.SetCmd == "2.50Rev.09") ? 0x27 : (byte)cmd);
                                break;
                            }
                        case "MCU_VERSION":
                        case "DEVICE_NAME":
                        case "CELL_VOLT":
                        case "TEMPERATURE":
                        case "CIPHER_UUID":
                            {
                                opts.Timeout = 300;
                                break;
                            }
                        case "DISCHARGE_INTERVAL":
                        case "TEMPERATURE_INTERVAL":
                            {
                                opts.Para = (opts.SetPara != null && ((string)opts.SetPara == "value")) ? (byte)0x01 : (byte)0x00;
                                break;
                            }
                        case "ALERTHAPPENTIME":
                            {
                                int tmpCmd = ((int)opts.SetCmd > 0x00) ? (int)opts.SetCmd - 0x01 : 0x00;
                                opts.Cmd = (byte)(cmd + (byte)tmpCmd);
                                break;
                            }
                        case "SWITCH_MODE":
                            {
                                opts.Para = (opts.SetPara != null && ((string)opts.SetPara == "eng")) ? (byte)0x83 : (byte)0x03;
                                break;
                            }
                        case "SYS_MSG":
                            {
                                opts.Para = (opts.SetPara != null) ? (byte)opts.SetPara : (byte)0x00;
                                opts.Timeout = opts.SetTimeout;
                                opts.EnableCipher = false;
                                opts.DisableParseStream = true;
                                break;
                            }
                        case "CAN_MODE_STATUS":
                            {
                                opts.Para = (opts.SetPara != null && ((string)opts.SetPara == "eng")) ? (byte)0x01 : (byte)0x02;
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
                else if (opts.Action == "set")
                {
                    switch (opts.CmdName)
                    {
                        case "PB_BYTE_EEP":
                            {
                                opts.Len = 0x03;
                                int tmpCmd = ((int)opts.SetCmd > 0x00) ? ((int)opts.SetCmd - 0x01) : 0x00;
                                opts.Cmd = (byte)(0x9C + (byte)tmpCmd);
                                opts.Para = (opts.SetPara != null) ? (byte)opts.SetPara : (byte)0x00;
                                opts.Data = new byte[] { opts.SetData };
                                break;
                            }
                        case "PB_BYTE_OP":
                            {
                                opts.Len = 0x03;
                                int tmpCmd = ((int)opts.SetCmd > 0x00) ? ((int)opts.SetCmd - 0x01) : 0x00;
                                opts.Cmd = (byte)(0x9E + (byte)tmpCmd);
                                opts.Para = (opts.SetPara != null) ? (byte)opts.SetPara : (byte)0x00;
                                opts.Data = new byte[] { opts.SetData };
                                break;
                            }
                        case "MCU_BYTE_EEP":
                            {
                                opts.Len = 0x04;
                                opts.Cmd = 0x91;
                                opts.Para = (opts.SetPara != null) ? (byte)opts.SetPara : (byte)0x00;
                                opts.Data = (opts.EnableASCII) ? new byte[] { 0x01, opts.SetData } : new byte[] { opts.SetData, 0xFF };
                                break;
                            }
                        case "RTC":
                            {
                                int tmpCmd = ((int)opts.SetCmd > 0x00) ? ((int)opts.SetCmd) : 0x00;
                                opts.Cmd = (byte)(0xA0 + (byte)tmpCmd);
                                opts.Para = (opts.SetPara != null) ? (byte)opts.SetPara : (byte)0x00;
                                break;
                            }
                        case "CAN_MODE_STATUS": {
                                opts.Len = 0x03;
                                opts.Cmd = (byte)((byte)cmd + 0x01);
                                opts.Para = (opts.SetPara != null) ? (byte)opts.SetPara : (byte)0x00;
                                opts.Data = new byte[] { opts.SetData };
                                break;
                            }
                        default: {
                                opts.Error = string.Format("Not support SetCmd: \"{0}\"", opts.CmdName);
                                break;
                            }
                    }
                }
                else
                {
                    opts.Error = "\"options.Action\" is incorrect type";
                }
            }
            catch (Exception ex)
            {
                opts.Error = string.Format("Not support Cmd: \"{0}\"({1})", opts.CmdName, ex.Message);
                return;
            }
        }

        private static void PackData(BatOptions opts)
        {
            uint sum = 0;
            List<byte> array = new List<byte>();

            if (opts.EnableASCII)  array.Add(opts.STX);
            sum += BatCore.PushChar(array, opts.Len, opts.EnableASCII);
            sum += BatCore.PushChar(array, opts.Para, opts.EnableASCII);
            sum += BatCore.PushChar(array, opts.Cmd, opts.EnableASCII);

            if (opts.Data != null && opts.Data.Length > 0)
            {
                for (int i = 0; i < opts.Data.Length; i += 1)
                {
                    sum += BatCore.PushChar(array, opts.Data[i], opts.EnableASCII);
                }
            }

            if (opts.EnableASCII)
            {
                array.Add(BatCore.ConvertHEXToASCII((byte)((sum >> 12) & 0x0F)));
                array.Add(BatCore.ConvertHEXToASCII((byte)((sum >> 8) & 0x0F)));
                array.Add(BatCore.ConvertHEXToASCII((byte)((sum >> 4) & 0x0F)));
                array.Add(BatCore.ConvertHEXToASCII((byte)((sum >> 0) & 0x0F)));

                if (opts.CANID != 0)
                {
                    array[0] = BatCore.CANSTX;
                }

                array.Add(opts.ETX);
            }

            opts.SendStream = array.ToArray();
        }

        private static void UnpackData(BatOptions opts)
        {
            if (!(opts.RecvStream != null && opts.RecvStream.Length > 0))
            {
                opts.Error = "Not recv stream";
                return;
            }

            if (!opts.EnableASCII)
            {
                opts.UnpackStream = new byte [opts.RecvStream.Length - 1];
                Array.Copy(opts.RecvStream, 0, opts.UnpackStream, 0, opts.UnpackStream.Length);
                return;
            }

            uint sum = 0;
            List<byte> array = new List<byte>();
            int len = opts.RecvStream.Length;
            int EOB = len - 5;

            opts.IsCANMode = (opts.RecvStream[0] == 0x04);  //  CANSTX = 0x04
            for (int i = 1; i < len; i += 2)
            {
                if (i >= EOB)
                {
                    uint checkSum =
                        (uint)(
                            (BatCore.ConvertASCIIToHEX(opts.RecvStream[i]) << 12) |
                            (BatCore.ConvertASCIIToHEX(opts.RecvStream[i + 1]) << 8) |
                            (BatCore.ConvertASCIIToHEX(opts.RecvStream[i + 2]) << 4) |
                            BatCore.ConvertASCIIToHEX(opts.RecvStream[i + 3])
                         );
                    if (sum == checkSum)
                    {
                        opts.UnpackStream = array.ToArray();
                    }
                    else
                    {
                        opts.Error = string.Format("Checksum error ${ 0} !== ${ 1}", sum, checkSum);
                    }
                    break;
                }
                else
                {
                    byte tmp = (byte)((BatCore.ConvertASCIIToHEX(opts.RecvStream[i]) << 4) | BatCore.ConvertASCIIToHEX(opts.RecvStream[i + 1]));
                    array.Add(tmp);
                    sum += array[array.Count - 1];
                }
            }
        }

        public static BatParseInfo ParseStream(byte[] stream, BatOptions opts)
        {
            BatParseInfo obj = new BatParseInfo();
            if (!opts.DisableParseStream)
            {
                int len = stream.Length;
                if (opts.EnableASCII)
                {
                    List<byte> data = new List<byte>();
                    int checkItem = -1;
                    int tmpLen = 0;

                    for (int i = 0; i < len; i += 1)
                    {
                        if ((stream[i] == BatCore.STX || stream[i] == BatCore.CANSTX))
                        {
                            if (checkItem > 0)
                            {
                                obj.ClearLen += data.Count;
                                data = new List<byte>();
                            }

                            checkItem = 1;
                            obj.ClearLen += tmpLen;
                            tmpLen = 0;
                            data.Add(stream[i]);    // Add Header CHAR
                        }
                        else if (checkItem > 0)
                        {
                            data.Add(stream[i]);    // Add CHAR
                            if (stream[i] == BatCore.ETX)
                            {
                                obj.ClearLen += data.Count;
                                obj.DataList.Add(data.ToList());
                                data = new List<byte>();
                                checkItem = 0;
                            }
                        }
                        else
                        {
                            tmpLen += 1;
                        }
                    }

                    if (checkItem < 0 && len > 128)
                    {
                        obj.ClearLen += len;
                    }
                }
                else if (len > 0 && stream[len - 1] == 0xFF)
                {
                    obj.ClearLen = stream.Length;
                    if (len > 3 && len == (stream[0] + 1))
                    {
                        obj.DataList.Add(stream.ToList());
                    }
                }
            }
            else
            {
                obj.ClearLen = stream.Length;
                obj.DataList.Add(stream.ToList());
            }

            return obj;
        }
        
        public static void RefreshBatInfo(Dictionary<string, object> batInfo)
        {
            GTCAN.RefreshBatInfo(batInfo);
        }

        public static List<BatOptions> GetCmdList(GTGetCmd cmd, CmdConfig cmdConfig, List<BatOptions> opts)
        {
            switch (cmd)
            {
                case GTGetCmd.MCU_BYTE_EEP:
                    {
                        if (opts == null) opts = new List<BatOptions>();

                        byte[] obj = (byte[])BatCore.ConvertStringToObj(cmdConfig.Type, cmdConfig.Value);

                        for (int i = 0; i < cmdConfig.Length; i++)
                        {
                            opts.Add(new BatOptions() { Action = "set", CmdName = "MCU_BYTE_EEP", SetPara = (cmdConfig.Address + i), SetData = obj[i] });
                        }
                        return opts;
                    }
                case GTGetCmd.PB_BYTE_EEP:
                    {
                        if (opts == null) opts = new List<BatOptions>();

                        byte[] obj = (byte[])BatCore.ConvertStringToObj(cmdConfig.Type, cmdConfig.Value);

                        for (int i = 0; i < cmdConfig.Length; i++)
                        {
                            opts.Add(new BatOptions() { Action = "set", CmdName = "PB_BYTE_EEP", SetPara = (cmdConfig.Address + i), SetData = obj[i] });
                        }
                        return opts;
                    }
                default:
                    {
                        if (opts == null)
                        {
                            opts = new List<BatOptions>();
                        }
                        return opts;
                    }
            }
        }

        public static List<BatOptions> GetCmd(GTGetCmd cmd)
        {
            switch(cmd)
            {
                case GTGetCmd.ScanCAN:
                case GTGetCmd.BatInfoCAN:
                    {
                        return GTCAN.GetCmd(cmd);
                    }
                case GTGetCmd.Scan:
                    {
                        List<BatOptions> opts = new List<BatOptions>();
                        opts.Add(new BatOptions() { Action = "get", CmdName = "MCU_VERSION" });
                        return opts;
                    }
                case GTGetCmd.Profile:
                    {
                        List<BatOptions> opts = new List<BatOptions>();
                        opts.Add(new BatOptions() { Action = "get", CmdName = "SOC" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "PROTECTION" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "CURRENT" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "REMAIN_CAPACITY" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "CELL_VOLT" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "TEMPERATURE" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "RTC" });
                        return opts;
                    }
                case GTGetCmd.Para:
                    {
                        List<BatOptions> opts = new List<BatOptions>();
                        opts.Add(new BatOptions() { Action = "get", CmdName = "CIPHER_UUID" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "BARCODE" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "DEVICE_NAME" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "DISCHARGE_INTERVAL" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "TEMPERATURE_INTERVAL" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "PB_EEP", SetCmd = 0x01 });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "PB_EEP", SetCmd = 0x02 });
                        return opts;
                    }
                case GTGetCmd.CheckData:
                    {
                        List<BatOptions> opts = new List<BatOptions>();
                        opts.Add(new BatOptions() { Action = "get", CmdName = "LAST_CHARGE" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "LAST_DISCHARGE" });
                        return opts;
                    }
                case GTGetCmd.NonCheckData:
                    {
                        List<BatOptions> opts = new List<BatOptions>();
                        opts.Add(new BatOptions() { Action = "get", CmdName = "LOG_CHARGE", SetCmd = 0x01 });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "LOG_CHARGE", SetCmd = 0x02 });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "LOG_CHARGE", SetCmd = 0x03 });
                        return opts;
                    }
                case GTGetCmd.CBData:
                    {
                        List<BatOptions> opts = new List<BatOptions>();
                        opts.Add(new BatOptions() { Action = "get", CmdName = "MCU_EEP_HL", SetCmd = 0x01 });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "MCU_EEP_HL", SetCmd = 0x02 });
                        return opts;
                    }
                case GTGetCmd.EEP4816:
                    {
                        List<BatOptions> opts = new List<BatOptions>();
                        opts.Add(new BatOptions() { Action = "get", CmdName = "DEVICE_NAME" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "BATTERY_ID" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "MANUFACTURE_DATE" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "DESIGN_VOLTAGE" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "DESIGN_CAPACITY" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "FULL_CHARGE_CAPACITY" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "CYCLE_COUNT" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "MCU_RESET_COUNT" });
                        return opts;
                    }
                default:
                    {
                        return new List<BatOptions>();
                    }
            }
        }

        private CipherAPI mCipher { get; set; }
        private GTCAN mGTCAN { get; set; }

        public BatCore()
        {
            mCipher = new CipherAES();
            mCipher.SetEncryptionKey("qswdefrgthyjukil");
            mGTCAN = new GTCAN();
        }

        public void SetMsg(BatOptions opts)
        {
            BatCore.SetCmdInfo(opts);
            if (opts.Error != null)
            {
                return;
            }

            this.GetCipher(opts);
            if (opts.Error != null)
            {
                return;
            }

            BatCore.PackData(opts);
        }

        public void ParseMsg(BatOptions opts)
        {
            if (opts.DisableParseStream || opts.DataList == null)
            {
                return;
            }

            for (int i = 0; i < opts.DataList.Count; i += 1)
            {
                BatOptions val = new BatOptions();
                val.SetCmd = opts.SetCmd;
                val.Para = opts.Para;
                val.EnableASCII = opts.EnableASCII;
                val.RecvStream = opts.DataList[i].ToArray();
                BatCore.UnpackData(val);

                if (val.Error != null)
                {
                    opts.Error = val.Error;
                }
                else
                {
                    this.GetDecipher(val);
                    if (val.Error != null)
                    {
                        opts.Error = val.Error;
                    }
                    else
                    {
                        this.ParseData(val);
                        if (val.Error != null)
                        {
                            opts.Error = val.Error;
                        }
                    }
                }

                if (val.Error == null)
                {
                    if (val.RecvCANInfo.Count > 0)
                    {
                        opts.RecvCANInfo.AddRange(val.RecvCANInfo);
                    }
                    opts.UnpackStreamList.Add(val.UnpackStream);
                    // 有正確收到並剖析值
                    if (opts.Result == null)
                    {
                        opts.Result = new Dictionary<string, object>();
                    }
                    foreach (var dict in val.Result)
                    {
                        BatCore.CopyAttrData(opts.Result, dict.Key, dict.Value);
                    }
                }
            }
        }

        private void ParseData(BatOptions opts)
        {
            int len = opts.UnpackStream[0];
            int dataLen = (opts.EnableASCII) ? (len - 2) : (len - 3);
            if ((dataLen + 3) != opts.UnpackStream.Length)
            {
                opts.Error = "Recev Stream Error";
                return;
            }

            if (opts.IsCANMode)
            {
                mGTCAN.ParseData(opts);
                return;
            }
            byte cmd = opts.UnpackStream[2];
            byte para = opts.UnpackStream[1];
            byte[] data = null;
            Array.Resize(ref data, opts.UnpackStream.Length - 3);
            Array.Copy(opts.UnpackStream, 3, data, 0, opts.UnpackStream.Length - 3);

            opts.Result = new Dictionary<string, object>();
            try
            {
                DefaultParseBatCmd cmdCode = (DefaultParseBatCmd)cmd;
                switch (cmdCode)
                {
                    case DefaultParseBatCmd.MCU_EEP:
                        {
                            if (dataLen == 0)
                            {
                                opts.Error = "EEP length is Zero";
                                return;
                            }
                            if (opts.Para == 0x00)
                            {
                                BatCore.CopyAttrData(opts.Result, "MCU_EEP_INDEX", (int)(data[0]));
                            }
                            else
                            {
                                Dictionary<string, byte[]> value = new Dictionary<string, byte[]>();
                                value.Add(string.Format("{0}", para), data);
                                BatCore.CopyAttrData(opts.Result, "MCU_EEP", value);
                            }
                            break;
                        }
                    case DefaultParseBatCmd.MCU_EEP_H:
                    case DefaultParseBatCmd.MCU_EEP_L:
                        {
                            Dictionary<string, byte[]> value = new Dictionary<string, byte[]>();
                            value.Add(((DefaultParseBatCmd)cmd == DefaultParseBatCmd.MCU_EEP_H) ? "1" : "2", data);
                            BatCore.CopyAttrData(opts.Result, "MCU_EEP", value);
                            break;
                        }
                    case DefaultParseBatCmd.PB_EEP_OZ1:
                    case DefaultParseBatCmd.PB_EEP_OZ2:
                        {
                            if (dataLen == 0)
                            {
                                opts.Error = "EEP length is Zero";
                                return;
                            }
                            Dictionary<string, byte[]> value = new Dictionary<string, byte[]>();
                            value.Add(((DefaultParseBatCmd)cmd == DefaultParseBatCmd.PB_EEP_OZ1) ? "1" : "2", data);
                            BatCore.CopyAttrData(opts.Result, "PB_EEP", value);
                            break;
                        }
                    case DefaultParseBatCmd.CHARGE_LOG_1:
                    case DefaultParseBatCmd.CHARGE_LOG_2:
                    case DefaultParseBatCmd.CHARGE_LOG_3:
                        {
                            Dictionary<string, byte[]> value = new Dictionary<string, byte[]>();
                            value.Add(string.Format("{0}", opts.SetCmd), data);
                            BatCore.CopyAttrData(opts.Result, "LOG_CHARGE", value);
                            break;
                        }
                    case DefaultParseBatCmd.DISCHARGE_LOG:
                        {
                            BatCore.CopyAttrData(opts.Result, "LOG_DISCHARGE", data);
                            break;
                        }
                    case DefaultParseBatCmd.MCU_VERSION:
                    case DefaultParseBatCmd.DEVICE_NAME:
                    case DefaultParseBatCmd.CIPHER_UUID:
                        {
                            if (dataLen == 0)
                            {
                                opts.Error = "Array length is Zero";
                                return;
                            }
                            string str = "";
                            for (int i = 0; i < data.Length; i++)
                                str += String.Format("{0}", (char)data[i]).ToString().Trim('\0');

                            BatCore.CopyAttrData(opts.Result, cmdCode.ToString(), str.Trim());
                            break;
                        }
                    case DefaultParseBatCmd.CELL_VOLT:
                        {
                            UInt64 TOTAL_VOLT = 0;
                            UInt32 MIN_VOLT = 0;
                            UInt32 MAX_VOLT = 0;
                            UInt32 DELTA_VOLT = 0;
                            Int32 CELL_NUM = 0;
                            List<uint> CELL_VOLT = new List<uint>();
                            if (opts.EnableASCII)
                            {
                                int num = (data[4] + data[5]);
                                for (int i = 0; i < num; i += 1)
                                {
                                    CELL_VOLT.Add((uint)((data[(i * 2) + 6] << 8) + (data[(i * 2) + 7])));
                                }
                            }
                            else
                            {
                                for (int i = 0; i < dataLen; i += 2)
                                {
                                    CELL_VOLT.Add((uint)((((data[i] | (data[i + 1] << 8)) >> 3) * 5000) / 4096));
                                }
                            }

                            int cellLen = CELL_VOLT.Count;
                            if (cellLen > 20 && (cellLen % 2) == 1)
                            { // drop down last cell for 7244
                                CELL_VOLT.RemoveRange(cellLen - 1, 1);
                            }
                            else if (cellLen > 14 && (cellLen % 2) == 1)
                            { // drop down middle cell for other case
                                CELL_VOLT.RemoveRange((cellLen / 2), 1);
                            }
                            CELL_NUM = CELL_VOLT.Count;

                            for (int i = 0; i < CELL_NUM; i++)
                            {
                                uint val = CELL_VOLT[i];
                                if (i == 0)
                                {
                                    MIN_VOLT = val;
                                    MAX_VOLT = val;
                                }
                                else
                                {
                                    if (MIN_VOLT > val)
                                    {
                                        MIN_VOLT = val;
                                    }
                                    if (MAX_VOLT < val)
                                    {
                                        MAX_VOLT = val;
                                    }
                                }
                                TOTAL_VOLT += val;
                            }
                            DELTA_VOLT = MAX_VOLT - MIN_VOLT;

                            BatCore.CopyAttrData(opts.Result, "CELL_VOLT", CELL_VOLT.ToArray());
                            BatCore.CopyAttrData(opts.Result, "TOTAL_VOLT", TOTAL_VOLT);
                            BatCore.CopyAttrData(opts.Result, "CELL_NUM", CELL_NUM);
                            BatCore.CopyAttrData(opts.Result, "MAX_VOLT", MAX_VOLT);
                            BatCore.CopyAttrData(opts.Result, "MIN_VOLT", MIN_VOLT);
                            BatCore.CopyAttrData(opts.Result, "DELTA_VOLT", DELTA_VOLT);
                            break;
                        }
                    case DefaultParseBatCmd.CURRENT:
                        {
                            BatCore.CopyAttrData(opts.Result, cmdCode.ToString(), (Int64)((((data[0] << 24) | (data[1] << 16)) | (data[2] << 8)) | data[3]));
                            break;
                        }
                    case DefaultParseBatCmd.DESIGN_CAPACITY:
                    case DefaultParseBatCmd.REMAIN_CAPACITY:
                    case DefaultParseBatCmd.FULL_CHARGE_CAPACITY:
                        {
                            UInt64 value = (UInt64)((((data[0] << 24) | (data[1] << 16)) | (data[2] << 8)) | data[3]);
                            if (!opts.EnableASCII)
                            {
                                value /= 3600;
                            }
                            BatCore.CopyAttrData(opts.Result, cmdCode.ToString(), value);
                            break;
                        }
                    case DefaultParseBatCmd.SOC:
                        {
                            UInt32 value = (UInt32)((opts.EnableASCII) ? data[0] : ((data[0] << 8) | data[1]));
                            BatCore.CopyAttrData(opts.Result, cmdCode.ToString(), value);
                            break;
                        }
                    case DefaultParseBatCmd.BARCODE:
                        {
                            if (dataLen != 5)
                            {
                                // Not Implement for 4816, Fixed by Neil 2016.7.21
                                opts.Error = "Not support BARCODE CMD";
                                return;
                            }
                            int manufacturer = (data[0] >> 4);
                            int productClass = (data[0] & 0x0F);
                            int year = data[1];
                            int week = data[2];
                            ulong serial = (ulong)data[3] << 8 | (ulong)data[4];
                            BatCore.CopyAttrData(opts.Result, cmdCode.ToString(), string.Format("{0:0}{1:0}{2:00}{3:00}{4:0000}", manufacturer, productClass, year, week, serial));

                            break;
                        }
                    case DefaultParseBatCmd.TEMPERATURE:
                        {
                            List<Double> CELL_TEMPERATURE = new List<Double>();
                            if (opts.EnableASCII)
                            {
                                Double tempZero = 2731.2;
                                Double tempDegree = 10.0;

                                int num = data[0] + data[1]; // total base dwon number (n66 + n88)
                                for (int i = 0; i < num; i += 1)
                                {
                                    Double value = (((data[2 + (i * 2)] << 8) | data[3 + (i * 2)]) - tempZero) / tempDegree;
                                    CELL_TEMPERATURE.Add(value);
                                }
                            }
                            else
                            {
                                for (int i = 0; i < data.Length; i += 2)
                                {
                                    CELL_TEMPERATURE.Add(((data[i + 1] << 5) | (data[i] >> 3)));
                                }
                            }
                            BatCore.CopyAttrData(opts.Result, "CELL_TEMPERATURE", CELL_TEMPERATURE.ToArray());
                            break;
                        }
                    case DefaultParseBatCmd.PROTECTION:
                        {
                            List<int> value = new List<int>();

                            if ((string)opts.SetCmd == "2.50Rev.09")
                            {
                                value.Add(((data[0] >> 0) & 0x01)); // Over Voltage Protection
                                value.Add(((data[0] >> 1) & 0x01) | ((data[0] >> 7) & 0x01)); // Under voltage protection
                                value.Add(((data[0] >> 3) & 0x01)); // Over Current Charge
                                value.Add(((data[0] >> 4) & 0x01)); // Over Current Discharge
                                value.Add(((data[0] >> 5) & 0x01) | ((data[0] >> 6) & 0x01)); // Temp Discharge (OT)
                                value.Add(0); // Temp charge (UT)
                                value.Add(0); // Short Circuit
                                value.Add(((data[0] >> 2) & 0x01)); // CDS
                                value.Add(((data[1] >> 0) & 0x01)); // Enable Charge
                                value.Add(((data[1] >> 1) & 0x01)); // Enable Discharge
                                value.Add(((data[1] >> 2) & 0x01)); // Discharging
                                value.Add(((data[1] >> 3) & 0x01)); // Charging
                            }
                            else
                            {
                                value.Add(0);
                                value.Add(0);
                                value.Add(0);
                                value.Add(0);
                                value.Add(0);
                                value.Add(0);
                                value.Add(0);

                                for (int i = 0; i < dataLen; i += 2)
                                {
                                    value[0] |= ((data[i] >> 0) & 0x01);
                                    value[1] |= ((data[i] >> 1) & 0x01);
                                    value[2] |= (((data[i] >> 2) & 0x01) & ((data[i + 1] >> 3) & 0x01));
                                    value[3] |= (((data[i] >> 2) & 0x01) & ((data[i + 1] >> 2) & 0x01));
                                    value[4] |= ((data[i] >> 4) & 0x01);
                                    value[5] |= ((data[i] >> 5) & 0x01);
                                    value[6] |= ((data[i] >> 3) & 0x01);
                                }
                            }
                            BatCore.CopyAttrData(opts.Result, cmdCode.ToString(), value.ToArray());
                            break;
                        }
                    case DefaultParseBatCmd.LAST_CHARGE:
                    case DefaultParseBatCmd.LAST_DISCHARGE:
                        {
                            List<int> value = new List<int>();
                            value.Add(data[0] + DEFAULTYEAR);
                            value.Add(data[1]);
                            value.Add(data[2]);
                            value.Add(data[3]);
                            value.Add(data[4]);
                            value.Add(data[5]);
                            value.Add((opts.EnableASCII) ? data[12] : data[6]);
                            BatCore.CopyAttrData(opts.Result, cmdCode.ToString(), value.ToArray());
                            break;
                        }
                    case DefaultParseBatCmd.RTC_REG_DUMP:
                        {
                            if (dataLen > 6)
                            {
                                int year = (int.Parse((data[6]).ToString("X")) + DEFAULTYEAR);
                                int month = (int.Parse((data[5] & 0x1F).ToString("X")));
                                int day = (int.Parse((data[4] & 0x3F).ToString("X")));
                                int hour = (int.Parse((data[2] & 0x3F).ToString("X")));
                                int min = (int.Parse((data[1] & 0x7F).ToString("X")));
                                int sec = (int.Parse((data[0] & 0x7F).ToString("X")));
                                BatCore.CopyAttrData(opts.Result, "RTC", DateTime.Parse(string.Format("{0:0000}-{1:00}-{2:00}T{3:00}:{4:00}:{5:00}.000Z", year, month, day, hour, min, sec)));
                            }
                            break;
                        }
                    case DefaultParseBatCmd.RTC_TIMESET:
                        {
                            int year = data[0] + DEFAULTYEAR;
                            int month = data[1];
                            int day = data[2];
                            int hour = data[3];
                            int min = data[4];
                            int sec = data[5];
                            BatCore.CopyAttrData(opts.Result, "RTC", DateTime.Parse(string.Format("{0:0000}-{1:00}-{2:00}T{3:00}:{4:00}:{5:00}.000Z", year, month, day, hour, min, sec)));
                            break;
                        }
                    case DefaultParseBatCmd.DESIGN_VOLTAGE:
                        {
                            ulong value = (ulong)((opts.EnableASCII) ? ((((data[0] << 24) | (data[1] << 16)) | (data[2] << 8)) | data[3]) : (((data[0] << 8) | data[1]) * 1000));
                            BatCore.CopyAttrData(opts.Result, cmdCode.ToString(), value);
                            break;
                        }
                    case DefaultParseBatCmd.BATTERY_ID:
                    case DefaultParseBatCmd.CYCLE_COUNT:
                    case DefaultParseBatCmd.MCU_RESET_COUNT:
                        {
                            BatCore.CopyAttrData(opts.Result, cmdCode.ToString(), (uint)((data[0] << 8) | data[1]));
                            break;
                        }
                    case DefaultParseBatCmd.MANUFACTURE_DATE:
                        {
                            int tmpValue = (data[0] << 8) | data[1];
                            int tmpYear = ((tmpValue >> 9) & 0x0000007F) + DEFAULTYEAR;
                            if (tmpYear > DateTime.Now.Year)
                            {
                                tmpYear -= 20;
                            }
                            BatCore.CopyAttrData(opts.Result, cmdCode.ToString(), new Int32[] { tmpYear, (tmpValue >> 5) & 0x0000000F, tmpValue & 0x0000001F });
                            break;
                        }
                    case DefaultParseBatCmd.OV_HAPPENTIME:
                    case DefaultParseBatCmd.UV_HAPPENTIME:
                    case DefaultParseBatCmd.COC_HAPPENTIME:
                    case DefaultParseBatCmd.DOC_HAPPENTIME:
                    case DefaultParseBatCmd.UT_HAPPENTIME:
                    case DefaultParseBatCmd.OT_HAPPENTIME:
                    case DefaultParseBatCmd.SC_HAPPENTIME:
                        {
                            List<int> value = new List<int>();
                            value.Add(data[0] + DEFAULTYEAR);
                            value.Add(data[1]);
                            value.Add(data[2]);
                            value.Add(data[3]);
                            value.Add(data[4]);
                            value.Add(data[5]);
                            if (opts.EnableASCII)
                            {
                                value.Add((data[6] << 8) + (data[7]));
                                value.Add((data[8] << 8) + (data[9]));
                                value.Add((data[10] << 8) + (data[11]));
                                value.Add(data[12]);
                            }
                            else
                            {
                                int vMin = (int)((((data[6] << 8) + (data[7])) * 5000) / 4096);
                                int vMax = (int)((((data[8] << 8) + (data[9])) * 5000) / 4096);
                                value.Add((vMin > 0xFFFF) ? 0 : vMin);
                                value.Add((vMax > 0xFFFF) ? 0 : vMax);
                                value.Add((data[10] << 8) + (data[11]));
                                value.Add(data[12]);
                            }
                            BatCore.CopyAttrData(opts.Result, cmdCode.ToString(), value.ToArray());
                            break;
                        }
                    case DefaultParseBatCmd.FCC_RESET_TIME:
                    case DefaultParseBatCmd.RTC_RESET_TIME:
                        {
                            List<int> value = new List<int>();
                            value.Add(data[0] + DEFAULTYEAR);
                            value.Add(data[1]);
                            value.Add(data[2]);
                            value.Add(data[3]);
                            value.Add(data[4]);
                            value.Add(((data[5] << 8) + (data[6])));
                            BatCore.CopyAttrData(opts.Result, cmdCode.ToString(), value.ToArray());

                            break;
                        }
                    case DefaultParseBatCmd.DISCHARGE_INTERVAL:
                    case DefaultParseBatCmd.TEMPERATURE_INTERVAL:
                        {
                            if (dataLen == 0)
                            {
                                opts.Error = "array is Zero";
                                return;
                            }
                            List<Int32> value = new List<Int32>();
                            if (opts.Para == 0x00)
                            {
                                for (int i = 0; i < dataLen; i += 1)
                                {
                                    value.Add((Int32)data[i]); // c
                                }

                                BatCore.CopyAttrData(opts.Result, cmdCode.ToString(), value.ToArray());
                            }
                            else
                            {
                                int num = dataLen / 4;
                                for (int i = 0; i < num; i += 1)
                                {
                                    value.Add((data[i * 4] << 24) + (data[(i * 4) + 1] << 16) + (data[(i * 4) + 2] << 8) + data[(i * 4) + 3]);
                                }
                                BatCore.CopyAttrData(opts.Result, string.Format("{0}_INFO", cmdCode.ToString()), value.ToArray());
                            }
                            break;
                        }
                    case DefaultParseBatCmd.RTC_YEAR_SET:
                    case DefaultParseBatCmd.RTC_MONTH_SET:
                    case DefaultParseBatCmd.RTC_DAY_SET:
                    case DefaultParseBatCmd.RTC_HOUR_SET:
                    case DefaultParseBatCmd.RTC_MIN_SET:
                    case DefaultParseBatCmd.RTC_SEC_SET:
                    case DefaultParseBatCmd.RESET_PB:
                    case DefaultParseBatCmd.RESET_MCU:
                    case DefaultParseBatCmd.MCU_BYTE_EEP_SET:
                    case DefaultParseBatCmd.PB_BYTE_EEP_OZ1_SET:
                    case DefaultParseBatCmd.PB_BYTE_EEP_OZ2_SET:
                    case DefaultParseBatCmd.PB_BYTE_OP_OZ1_SET:
                    case DefaultParseBatCmd.PB_BYTE_OP_OZ2_SET:
                    case DefaultParseBatCmd.SWITCH_MODE:
                        {
                            BatCore.CopyAttrData(opts.Result, cmdCode.ToString(), true);
                            break;
                        }
                    case DefaultParseBatCmd.MCU_BYTE_EEP_GET:
                    case DefaultParseBatCmd.PB_BYTE_EEP_OZ1_GET:
                    case DefaultParseBatCmd.PB_BYTE_EEP_OZ2_GET:
                    case DefaultParseBatCmd.PB_BYTE_OP_OZ1_GET:
                    case DefaultParseBatCmd.PB_BYTE_OP_OZ2_GET:
                    case DefaultParseBatCmd.SYS_MSG:
                    case DefaultParseBatCmd.CAN_MODE_STATUS_GET:
                    case DefaultParseBatCmd.CAN_MODE_STATUS_SET:
                        {
                            break;
                        }
                    default:
                        {
                            opts.Error = string.Format("Not support CMD({0})", cmdCode.ToString());
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                opts.Error = string.Format("Recev CMD({0}) Error", cmd);
            }
        }
        private void GetCipher(BatOptions opts)
        {
            if (!opts.EnableASCII || !opts.EnableCipher)
            {
                return;
            }

            int cipherLen = (opts.Len > 16) ? ((opts.Len >> 4) << 4) : (((opts.Len + 16) >> 4) << 4);

            byte[] array = new byte[cipherLen];
            array[0] = opts.Len;
            array[1] = opts.Para;
            array[2] = opts.Cmd;

            if (opts.Data != null)
            {
                for (int i = 0; i < opts.Data.Length; i++)
                {
                    array[i + 3] = opts.Data[i];
                }
            }

            opts.Data = mCipher.EncryptionData(array);
            opts.Len = (byte)(cipherLen + 2);
            opts.Para = 0x00;
            opts.Cmd = 0xC0;
        }

        private void GetDecipher(BatOptions opts)
        {
            if (opts.EnableASCII && (byte)DefaultBatCmd.CIPHER_CMD == opts.UnpackStream[2])
            {
                int encryptedlen = opts.UnpackStream[0] - 2;
                byte[] data = null;
                Array.Resize(ref data, encryptedlen);
                Array.Copy(opts.UnpackStream, 3, data, 0, encryptedlen);
                opts.UnpackStream = mCipher.DecryptionData(data);
            }
        }
    }
}
