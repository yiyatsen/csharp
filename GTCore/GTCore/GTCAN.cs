﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTCore
{
    public class GTCAN
    {
        public class GTCANATTR
        {
            public uint id { get; set; }
            public string name { get; set; }
            public string[] attrList { get; set; }
        }
 
        public class GTCANRecvInfo
        {
            public uint canID { get; set; }
            public byte canType { get; set; }
            public int canDataLen { get; set; }
            public byte[] data { get; set; }
            public string[] attrList { get; set; }
            public Dictionary<string, object> result = new Dictionary<string, object>();
        }

        private static byte CANCMDCODE = 0x40;
        public static GTCANATTR[] CANATTRLIST = new GTCANATTR[] {
            new GTCANATTR() { id = 0x704, name = "Heart Beat", attrList = new string[] { "BATTERY_HEART_BEAT" } },
            new GTCANATTR() { id = 0x300, name = "Status", attrList = new string[] { "SOC", "CURRENT", "TEMPERATURE", "TOTAL_VOLT" } },
            new GTCANATTR() { id = 0x481, name = "Cycle and Flag", attrList = new string[] { "CYCLE_COUNT", "PROTECTION" } },
            new GTCANATTR() { id = 0x482, name = "Last Chrg. Info.", attrList = new string[] { "LAST_CHARGE" } },
            new GTCANATTR() { id = 0x483, name = "Energy", attrList = new string[] { "FULL_CHARGE_CAPACITY", "REMAIN_CAPACITY" } },
            new GTCANATTR() { id = 0x484, name = "ID and Chrg. Status", attrList = new string[] { "batteryId", "PACK_STATE" } },
            new GTCANATTR() { id = 0x506, name = "Energy", attrList = new string[] { "FULL_CHARGE_CAPACITY", "REMAIN_CAPACITY" } },
            new GTCANATTR() { id = 0x507, name = "ID and Chrg. Status", attrList = new string[] { "batteryId", "PACK_STATE" } }
        };
        private Dictionary<uint, string> mCANIdDefineList = new Dictionary<uint, string>();
        private Dictionary<uint, ulong> mCANIDCounterList = new Dictionary<uint, ulong>();

        public static List<BatOptions> GetCmd(BatCore.GTGetCmd cmd)
        {
            switch (cmd)
            {
                case BatCore.GTGetCmd.ScanCAN:
                    {
                        List<BatOptions> opts = new List<BatOptions>();
                        opts.Add(new BatOptions() { CANID = 0x11000001, CANType = 0x01, CANData = new byte[] { 0x01 } });
                        return opts;
                    }
                case BatCore.GTGetCmd.BatInfoCAN:
                    {
                        List<BatOptions> opts = new List<BatOptions>();
                        opts.Add(new BatOptions() { Action = "get", CmdName = "BARCODE" });
                        opts.Add(new BatOptions() { Action = "get", CmdName = "DEVICE_NAME" });
                        return opts;
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        public static void SetCmdInfo(BatOptions opts)
        {
            if (opts.CANID == 0x00)
            {
                opts.Error = "\"opts.CANID\" is not a number";
                return;
            }

            if (opts.CANData == null)
            {
                opts.Error = "\"opts.canData\" is not a array";
                return;
            }

            opts.STX = 0x04;
            opts.EnableASCII = true;
            opts.EnableCipher = false;
            int dataLen = opts.CANData.Length;

            opts.Len = (byte)(7 + dataLen);
            opts.Para = opts.Para;
            opts.Cmd = (opts.Cmd != 0x00) ? opts.Cmd : CANCMDCODE;
            List<byte> tmpData = new List<byte>();

            tmpData.Add((byte)((opts.CANID >> 24) & 0xFF));
            tmpData.Add((byte)((opts.CANID >> 16) & 0xFF));
            tmpData.Add((byte)((opts.CANID >> 8) & 0xFF));
            tmpData.Add((byte)(opts.CANID & 0xFF));

            tmpData.Add((byte)((opts.CANType << 4) | dataLen));
            for (int i = 0; i < dataLen; i += 1)
            {
                tmpData.Add(opts.CANData[i]);
            }
            opts.Data = tmpData.ToArray();
        }

        public static void RefreshBatInfo(Dictionary<string, object> batInfo)
        {
            long tmpCurrent = 0;
            if (batInfo.ContainsKey("CAN_CURRENT"))
            {
                tmpCurrent = (long)batInfo["CAN_CURRENT"];
            }
            long tmpCurrentState = 1;
            if (batInfo.ContainsKey("CAN_PACK_STATE") && (long)batInfo["CAN_PACK_STATE"] == 1)
            {
                tmpCurrentState = -1;
            }

            BatCore.CopyAttrData(batInfo, "CURRENT", tmpCurrent * tmpCurrentState);
            if (batInfo.ContainsKey("DEVICE_NAME"))
            {
                BatCore.CopyAttrData(batInfo, "batteryType", batInfo["DEVICE_NAME"]);
            }
            if (batInfo.ContainsKey("BARCODE"))
            {
                BatCore.CopyAttrData(batInfo, "batteryId", batInfo["BARCODE"]);
            }
            else if (batInfo.ContainsKey("CAN_batteryId"))
            {
                BatCore.CopyAttrData(batInfo, "batteryId", string.Format("{0:0000000000}", batInfo["CAN_batteryId"]));
            }

            if (batInfo.ContainsKey("REFRESH_CONFIG"))
            {
                for (int i = 0; i < CANATTRLIST.Length; i += 1)
                {
                    if ((((byte)batInfo["REFRESH_CONFIG"] >> i) & 0x01) == 0)
                    {
                        uint idInfo = CANATTRLIST[i].id;
                        for (int j = 0; j < CANATTRLIST[i].attrList.Length; j += 1)
                        {
                            if (batInfo.ContainsKey(CANATTRLIST[i].attrList[j]))
                            {
                                batInfo.Remove(CANATTRLIST[i].attrList[j]);
                            }
                        }
                    }
                }
                BatCore.CopyAttrData(batInfo, "PACK_CONFIG", batInfo["REFRESH_CONFIG"]);
                batInfo.Remove("REFRESH_CONFIG");
            }
        }

        public void ParseData(BatOptions opts)
        {
            if (opts.UnpackStream == null)
            {
                opts.Error = "\"opts.UnpackStream\" is invalid";
                return;
            }

            if (opts.UnpackStream.Length < 3)
            {
                opts.Error = "\"opts.UnpackStream\" is invalid";
                return;
            }

            byte cmdLen = opts.UnpackStream[0];
            byte para = opts.UnpackStream[1];
            byte cmdCode = opts.UnpackStream[2];
            int infoLen = (int)(cmdLen - 2);

            if (cmdCode == 0x40 && para == 0x00)
            {
                opts.Result = new Dictionary<string, object>();
                GTCANRecvInfo recvInfo = new GTCANRecvInfo();
                recvInfo.canID = (uint)((opts.UnpackStream[3] << 24) + (opts.UnpackStream[4] << 16) + (opts.UnpackStream[5] << 8) + opts.UnpackStream[6]);
                recvInfo.canType = (byte)(opts.UnpackStream[7] >> 4);
                recvInfo.canDataLen = (opts.UnpackStream[7] & 0x0F);
                recvInfo.data = new byte[recvInfo.canDataLen];
                Array.Copy(opts.UnpackStream, 8, recvInfo.data, 0, recvInfo.canDataLen);

                if ((recvInfo.canDataLen + 7) == cmdLen)
                {
                    switch (recvInfo.canID)
                    {
                        case 0x12000001:
                            {
                                if (recvInfo.data[0] == 0x01)
                                {
                                    BatCore.CopyAttrData(opts.Result, "PACK_CONFIG", recvInfo.data[1]);
                                    BatCore.CopyAttrData(recvInfo.result, "PACK_CONFIG", opts.Result["PACK_CONFIG"]);
                                }
                                else if (recvInfo.data[0] == 0x02)
                                {
                                    BatCore.CopyAttrData(opts.Result, "REFRESH_CONFIG", recvInfo.data[1]);
                                    BatCore.CopyAttrData(recvInfo.result, "REFRESH_CONFIG", opts.Result["REFRESH_CONFIG"]);
                                }
                                else
                                {
                                    BatCore.CopyAttrData(opts.Result, "PACK_CONFIG_NUM", recvInfo.data[1]);
                                    BatCore.CopyAttrData(recvInfo.result, "PACK_CONFIG_NUM", opts.Result["PACK_CONFIG_NUM"]);
                                }
                                break;
                            }
                        case 0x704:
                            {
                                BatCore.CopyAttrData(opts.Result, "BATTERY_HEART_BEAT", recvInfo.data[0]);
                                BatCore.CopyAttrData(recvInfo.result, "BATTERY_HEART_BEAT", opts.Result["BATTERY_HEART_BEAT"]);
                                break;
                            }
                        case 0x300:
                            {
                                BatCore.CopyAttrData(opts.Result, "SOC", recvInfo.data[0]);
                                BatCore.CopyAttrData(opts.Result, "CAN_CURRENT", (((recvInfo.data[3] << 8) | recvInfo.data[2]) * 62.5));
                                BatCore.CopyAttrData(opts.Result, "TEMPERATURE", (double)((recvInfo.data[5] << 8) | recvInfo.data[4]) * 0.1);
                                BatCore.CopyAttrData(opts.Result, "TOTAL_VOLT", (ulong)(((recvInfo.data[7] << 8) | recvInfo.data[6]) * 46));

                                BatCore.CopyAttrData(recvInfo.result, "SOC", opts.Result["SOC"]);
                                BatCore.CopyAttrData(recvInfo.result, "CAN_CURRENT", opts.Result["CAN_CURRENT"]);
                                BatCore.CopyAttrData(recvInfo.result, "TEMPERATURE", opts.Result["TEMPERATURE"]);
                                BatCore.CopyAttrData(recvInfo.result, "TOTAL_VOLT", opts.Result["TOTAL_VOLT"]);
                                break;
                            }
                        case 0x481:
                            {
                                List<int> value = new List<int>();
                                value.Add((recvInfo.data[2] >> 0) & 0x01); // OV
                                value.Add((recvInfo.data[2] >> 1) & 0x01); // UV
                                value.Add((recvInfo.data[2] >> 2) & 0x01); // OC
                                value.Add(0); // DOC
                                value.Add((recvInfo.data[2] >> 4) & 0x01); // OT
                                value.Add((recvInfo.data[2] >> 5) & 0x01); // UT
                                value.Add((recvInfo.data[2] >> 3) & 0x01); // SC
                                BatCore.CopyAttrData(opts.Result, "CYCLE_COUNT", ((recvInfo.data[1] << 8) | recvInfo.data[0]));
                                BatCore.CopyAttrData(opts.Result, "PROTECTION", value.ToArray());

                                BatCore.CopyAttrData(recvInfo.result, "CYCLE_COUNT", opts.Result["CYCLE_COUNT"]);
                                BatCore.CopyAttrData(recvInfo.result, "PROTECTION", opts.Result["PROTECTION"]);
                                break;
                            }
                        case 0x482:
                            {
                                List<int> value = new List<int>();
                                value.Add(recvInfo.data[0] + BatCore.DEFAULTYEAR);
                                value.Add(recvInfo.data[1]);
                                value.Add(recvInfo.data[2]);
                                value.Add(recvInfo.data[3]);
                                value.Add(recvInfo.data[4]);
                                value.Add(recvInfo.data[5]);
                                value.Add(recvInfo.data[6]);
                                BatCore.CopyAttrData(opts.Result, "LAST_CHARGE", value.ToArray());

                                BatCore.CopyAttrData(recvInfo.result, "LAST_CHARGE", opts.Result["LAST_CHARGE"]);
                                break;
                            }
                        case 0x483:
                        case 0x506:
                            {
                                BatCore.CopyAttrData(opts.Result, "FULL_CHARGE_CAPACITY", (ulong)((((recvInfo.data[3] << 24) | (recvInfo.data[2] << 16)) | (recvInfo.data[1] << 8)) | recvInfo.data[0]));
                                BatCore.CopyAttrData(opts.Result, "REMAIN_CAPACITY", (ulong)((((recvInfo.data[7] << 24) | (recvInfo.data[6] << 16)) | (recvInfo.data[5] << 8)) | recvInfo.data[4]));

                                BatCore.CopyAttrData(recvInfo.result, "FULL_CHARGE_CAPACITY", opts.Result["FULL_CHARGE_CAPACITY"]);
                                BatCore.CopyAttrData(recvInfo.result, "REMAIN_CAPACITY", opts.Result["REMAIN_CAPACITY"]);
                                break;
                            }
                        case 0x484:
                        case 0x507:
                            {
                                BatCore.CopyAttrData(opts.Result, "CAN_batteryId", ((((recvInfo.data[3] << 24) | (recvInfo.data[2] << 16)) | (recvInfo.data[1] << 8)) | recvInfo.data[0]));
                                BatCore.CopyAttrData(opts.Result, "CAN_PACK_STATE", recvInfo.data[4]);

                                BatCore.CopyAttrData(recvInfo.result, "CAN_batteryId", opts.Result["CAN_batteryId"]);
                                BatCore.CopyAttrData(recvInfo.result, "CAN_PACK_STATE", opts.Result["CAN_PACK_STATE"]);
                                break;
                            }
                        default:
                            {
                                opts.Error = "No Support CANID";
                                break;
                            }
                    }

                    if (opts.Result != null && opts.Result.Count > 0)
                    {
                        opts.RecvCANInfo.Add(recvInfo);
                    }
                }
            }
        }
    }
}
