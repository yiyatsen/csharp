﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GTCore.BatCore;

namespace GTCore
{
    public class OZ890EEP
    {
        public static List<BatCore.CmdConfig> PARAMLIST = new List<CmdConfig>()
        {
            new CmdConfig() { Name = "SENSE_RESISTOR", IsHidden = true, IsReadOnly = true, Type = "System.Double", Address = 0x34, Length = 2 },

            new CmdConfig() { Name = "TEMP_OFFSET", IsHidden = true, IsReadOnly = true, Type = "System.Int32", Address = 0x02, Length = 1 },
            new CmdConfig() { Name = "COC_OFFSET", IsHidden = true, IsReadOnly = true, Type = "System.Int32", Address = 0x03, Length = 1 },
            new CmdConfig() { Name = "SC_OFFSET", IsHidden = true, IsReadOnly = true, Type = "System.Int32", Address = 0x03, Length = 1 },
            new CmdConfig() { Name = "DOC_OFFSET", IsHidden = true, IsReadOnly = true, Type = "System.Int32", Address = 0x04, Length = 1 },
            new CmdConfig() { Name = "CELL_OFFSET", IsHidden = true, IsReadOnly = true, Type = "System.Byte[]", Address = 0x05, Length = 13 },
            new CmdConfig() { Name = "GPIO_OFFSET", IsHidden = true, IsReadOnly = true, Type = "System.Byte[]", Address = 0x12, Length = 3 },
            new CmdConfig() { Name = "CURRENT_OFFSET", IsHidden = true, IsReadOnly = true, Type = "System.Int32", Address = 0x16, Length = 2 },

            new CmdConfig() { Name = "PF_RECORD", IsHidden = true, IsReadOnly = true, Type = "System.Byte", Address = 0x18, Length = 1 },

            new CmdConfig() { Name = "ATE_FREEZE", IsHidden = true, IsReadOnly = true, Type = "System.Boolean", Address = 0x25, Length = 1 },

            //new CmdConfig() { Name = "BAT_TYPE", IsReadOnly = true, Type = "Int32", Address = 0x26, Length = 1 },
            new CmdConfig() { Name = "CELL_NUMBER", IsReadOnly = true, Type = "System.Int32", Address = 0x26, Length = 1 },

            new CmdConfig() { Name = "SCAN_RATE", IsHidden = true, IsReadOnly = true, Type = "System.Int32", Address = 0x27, Length = 1 },

            new CmdConfig() { Name = "COC_TH", IsReadOnly = true, Type = "System.Double", Address = 0x28, Length = 1 },
            new CmdConfig() { Name = "DOC_TH", IsReadOnly = true, Type = "System.Double", Address = 0x29, Length = 1 },
            new CmdConfig() { Name = "OC_DELAY", IsReadOnly = true, Type = "System.Int32", Address = 0x2A, Length = 1 },
            new CmdConfig() { Name = "SC_TH", Type = "System.Double", Address = 0x2B, Length = 1 },
            new CmdConfig() { Name = "SC_DELAY", IsReadOnly = true, Type = "System.Int32", Address = 0x2C, Length = 1 },

            new CmdConfig() { Name = "COC_RELEASE", Type = "System.Int32", Address = 0x2D, Length = 1 },
            new CmdConfig() { Name = "DOC_RELEASE", Type = "System.Int32", Address = 0x2D, Length = 1 },

            new CmdConfig() { Name = "ENABLE_IDL_BLD", IsHidden = true, IsReadOnly = true, Type = "System.Boolean", Address = 0x2D, Length = 1 },
            new CmdConfig() { Name = "NO_ER_DSPL", IsHidden = true, IsReadOnly = true, Type = "System.Boolean", Address = 0x2D, Length = 1 },

            new CmdConfig() { Name = "OTUT_DELAY", Type = "System.Int32", Address = 0x2E, Length = 1 },
            new CmdConfig() { Name = "OVUV_DELAY", Type = "System.Int32", Address = 0x2E, Length = 1 },

            //new CmdConfig() { Name = "PF_DELAY", IsReadOnly = true, Type = "Int32", Address = 0x2F, Length = 1 },
            //new CmdConfig() { Name = "PF_ENABLE_MF", IsReadOnly = true, Type = "Boolean", Address = 0x2F, Length = 1 },
            //new CmdConfig() { Name = "PF_ENABLE_VH", IsReadOnly = true, Type = "Boolean", Address = 0x2F, Length = 1 },
            //new CmdConfig() { Name = "PF_ENABLE_VL", IsReadOnly = true, Type = "Boolean", Address = 0x2F, Length = 1 },

            new CmdConfig() { Name = "I2C_ADDRESS", IsHidden = true, IsReadOnly = true,  Type = "System.Int32", Address = 0x30, Length = 1 },
            new CmdConfig() { Name = "ENABLE_PEC", IsHidden = true, IsReadOnly = true,  Type = "System.Boolean", Address = 0x30, Length = 1 },
            new CmdConfig() { Name = "SC_RELEASE", IsHidden = true, IsReadOnly = true,  Type = "System.Int32", Address = 0x30, Length = 1 },

            new CmdConfig() { Name = "WAKEUP_CONTROL", IsHidden = true, IsReadOnly = true,  Type = "System.Int32", Address = 0x31, Length = 1 },
            new CmdConfig() { Name = "ENABLE_T1E", IsHidden = true, IsReadOnly = true, Type = "System.Boolean", Address = 0x32, Length = 1 },
            new CmdConfig() { Name = "ENABLE_T2E", IsHidden = true, IsReadOnly = true, Type = "System.Boolean", Address = 0x32, Length = 1 },
            //new CmdConfig() { Name = "MODE_CONTROL", IsReadOnly = true, Type = "Byte", Address = 0x32, Length = 1 },
            //new CmdConfig() { Name = "HARDWARE_BLEEDING", IsReadOnly = true, Type = "Byte", Address = 0x33, Length = 1 },

            new CmdConfig() { Name = "FACTORY_NAME", Type = "System.String", Address = 0x36, Length = 10 },
            new CmdConfig() { Name = "PROJECT_NAME", Type = "System.String", Address = 0x40, Length = 5 },
            new CmdConfig() { Name = "VERSION_NUMBER", Type = "System.String", Address = 0x45, Length = 1 },

            new CmdConfig() { Name = "CELL_UNBALANCE_TH", IsHidden = true, IsReadOnly = true, Type = "System.Int32", Address = 0x46, Length = 2 },
            //new CmdConfig() { Name = "BLEEDING_START_VOLTAGE", IsReadOnly = true, Type = "Int32", Address = 0x48, Length = 2 },

            new CmdConfig() { Name = "OV_TH", Type = "System.Int32", Address = 0x4A, Length = 2 },
            new CmdConfig() { Name = "OV_RELEASE", Type = "System.Int32", Address = 0x4C, Length = 2 },
            new CmdConfig() { Name = "UV_TH", Type = "System.Int32", Address = 0x4E, Length = 2 },
            new CmdConfig() { Name = "UV_RELEASE", Type = "System.Int32", Address = 0x50, Length = 2 },
            new CmdConfig() { Name = "EXTRE_HIGH_VOLT_TH", Type = "System.Int32", Address = 0x52, Length = 2 },    //
            new CmdConfig() { Name = "EXTRE_LOW_VOLT_TH", Type = "System.Int32", Address = 0x54, Length = 2 },     //

            new CmdConfig() { Name = "OTE_TH", Type = "System.Int32", Address = 0x56, Length = 2 },
            new CmdConfig() { Name = "OTE_RELEASE", Type = "System.Int32", Address = 0x58, Length = 2 },
            new CmdConfig() { Name = "UTE_TH", Type = "System.Int32", Address = 0x5A, Length = 2 },
            new CmdConfig() { Name = "UTE_RELEASE", Type = "System.Int32", Address = 0x5C, Length = 2 },
            new CmdConfig() { Name = "OTI_TH", Type = "System.Int32", Address = 0x5E, Length = 2 },
            new CmdConfig() { Name = "OTI_RELEASE", Type = "System.Int32", Address = 0x60, Length = 2 },
            new CmdConfig() { Name = "UTI_TH", Type = "System.Int32", Address = 0x62, Length = 2 },
            new CmdConfig() { Name = "UTI_RELEASE", Type = "System.Int32", Address = 0x64, Length = 2 },

            //new CmdConfig() { Name = "GAS_GAUGE", IsReadOnly = true, Type = "Int32", Address = 0x66, Length = 10 },
            //new CmdConfig() { Name = "PASSWORD", IsReadOnly = true, Type = "Int32", Address = 0x7A, Length = 2 },
            //new CmdConfig() { Name = "AUTHENTICATION_CODE", IsReadOnly = true, Type = "Int32", Address = 0x7C, Length = 2 },
            //new CmdConfig() { Name = "AUTHENTICATION_CONTROL", IsReadOnly = true, Type = "Int32", Address = 0x7F, Length = 1 }
        };

        //private static int[] ERRADCTABLE = new int[] { 1763, 1477, 1231, 1019, 843, 695, 573, 473, 392, 325, 270, 225, 189, 158, 134, 113, 96, 82, 70, 60, 52, 45, 39, 34, 30 };
        //private static int[] STDADCTABLE = new int[] { 1753, 1468, 1222, 1012, 837, 689, 569, 469, 388, 322, 268, 223, 187, 157, 132, 112, 95, 81, 69, 60, 51, 44, 39, 34, 29 };
        //private static int[] DEGCTABLE = new int[] { -30, -25, -20, -15, -10, -5, 0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90 };
        private static double PULLUPRESISTOR = 232000.0;
        private static int[] LOOKUPRESISTORTABLE = new int[] {
            111300, 86430, 67770, 53410, 42470,
            33900, 27280, 22050, 17960, 14690,
            12090, 10000, 8313, 6940, 5827,
            4911, 4160, 3536, 3020, 2588,
            2228, 1924, 1668, 1451, 1266
        };
        private static Int32[] mADCTABLE;
        private static Int32[] ADCTABLE {
            get {
                if (mADCTABLE == null)
                {
                    mADCTABLE = new Int32[25];
                    for (int i= 0; i < OZ890EEP.LOOKUPRESISTORTABLE.Length; i++)
                    {
                        mADCTABLE[i] = Convert.ToInt32((3300.0 * OZ890EEP.LOOKUPRESISTORTABLE[i]) / ((PULLUPRESISTOR + OZ890EEP.LOOKUPRESISTORTABLE[i]) * 0.61035));
                    }
                }
                return mADCTABLE;
            }
        }
        private static int[] DEGCTABLE = new int[] {
            -30, -25, -20, -15, -10,
            -5, 0, 5, 10, 15,
            20, 25, 30, 35, 40,
            45, 50, 55, 60, 65,
            70, 75, 80, 85, 90
        };

        public static double GetExternalTemperature(double adc)
        {
            int i = 0;
            for (i =0; i < 25; i += 1)
            {
                if (ADCTABLE[i] <= adc) break;
            }

            if (i == 0 || i == 24) return DEGCTABLE[i];
            
            int adcInterval = ADCTABLE[i - 1] - ADCTABLE[i];
            int degreeInterval = DEGCTABLE[i] - DEGCTABLE[i - 1];
            double diffValue = ADCTABLE[i - 1] - adc;
            return (((degreeInterval * diffValue) / adcInterval) + DEGCTABLE[i - 1]);
        }
        
        public static Int32 GetExternalADC(double temp)
        {
            int i = 0;
            for (i = 0; i < 25; i += 1)
            {
                if (DEGCTABLE[i] >= temp) break;
            }

            if (i == 0 || i == 24) return ADCTABLE[i];

            int adcInterval = ADCTABLE[i - 1] - ADCTABLE[i];
            int degreeInterval = DEGCTABLE[i] - DEGCTABLE[i - 1];
            double diffValue = DEGCTABLE[i] - temp;
            return (Int32)((ADCTABLE[i] - ((adcInterval * diffValue) / degreeInterval)));
        }

        public static double GetInternalTemperature(double adc, int offset)
        {
            int tmpITV = 550;
            if (offset != 0) tmpITV = offset + 400;

            return ((adc * 0.61035) - tmpITV) / 2.0976 + 30;
        }

        public static Int32 GetInternalADC(double temp, int offset)
        {
            int tmpITV = 550;
            if (offset != 0) tmpITV = offset + 400;

            return Convert.ToInt32((((temp + 30) * 2.0976) + tmpITV) * 1.6384);
        }

        public static CmdConfig GetCmdConfig(string cmd)
        {
            CmdConfig tmp = OZ890EEP.PARAMLIST.FirstOrDefault(item => item.Name == cmd);
            if (tmp == null) return null;
            return new CmdConfig()
            {
                IsReadOnly = tmp.IsReadOnly,
                Name = tmp.Name,
                Type = tmp.Type,
                Address = tmp.Address,
                Length = tmp.Length
            };
        }


        public static Dictionary<string, object> ConvertArrayToCmdInfo(string cmd, byte[] pbEEP, Dictionary<string, object> val)
        {
            if (val == null) val = new Dictionary<string, object>();
            byte[] data = BatCore.GetPBCmdRawArray(cmd, pbEEP);
            if (data.Length == 0) return val;

            switch (cmd)
            {
                case "SENSE_RESISTOR":
                    {
                        val.Add(cmd, (Double)(((data[1] << 8) | data[0]) * 0.001));
                        break;
                    }
                case "TEMP_OFFSET":
                    {
                        val.Add(cmd, (Int32)(data[0]));
                        break;
                    }
                case "CELL_OFFSET":
                case "GPIO_OFFSET":
                    {
                        val.Add(cmd, data);
                        break;
                    }
                case "SC_OFFSET":
                    {
                        Int32 tmp = data[0] & 0x0F;
                        if ((tmp & 0x08) != 0)
                        {
                            tmp = ~tmp;
                            tmp += 1;
                            tmp &= tmp;
                            tmp = 0 - tmp;
                        }
                        val.Add(cmd, tmp);
                        break;
                    }
                case "COC_OFFSET":
                case "DOC_OFFSET":
                    {
                        Int32 tmp = ((data[0] >> 0x04) & 0x0F);
                        if ((tmp & 0x08) != 0)
                        {
                            tmp = ~tmp;
                            tmp += 1;
                            tmp &= tmp;
                            tmp = 0 - tmp;
                        }
                        val.Add(cmd, tmp);
                        break;
                    }
                case "CURRENT_OFFSET":
                    {
                        val.Add(cmd, (Int32)((data[1] << 0x08) | data[0]));
                        break;
                    }
                case "ATE_FREEZE":
                    {
                        val.Add(cmd, (Boolean)(((data[0] >> 0x07) & 0x01) != 0));
                        break;
                    }
                case "CELL_NUMBER":
                    {
                        val.Add(cmd, (Int32)(data[0] & 0x0F));
                        break;
                    }
                case "SCAN_RATE":
                    {
                        Int32 tmp = ((data[0] >> 0x04) & 0x07) << 3;
                        val.Add(cmd, (tmp == 0) ? 1 : tmp);
                        break;
                    }
                case "COC_TH":
                    {
                        if (!val.ContainsKey("COC_OFFSET") || !val.ContainsKey("SENSE_RESISTOR")) return val;
                        Int32 SCC = (data[0] & 0x3F);
                        Int32 OFFSET = (Int32)val["COC_OFFSET"];
                        Double SENSE_RESISTOR = ((Double)val["SENSE_RESISTOR"]);
                        Double TH = Math.Round(Math.Floor((((((data[0] & 0x1F) + OFFSET) - 4) * 5) / SENSE_RESISTOR) * 10) * 0.1, 2);
                        val.Add(cmd, TH);
                        break;
                    }
                case "DOC_TH":
                    {
                        if (!val.ContainsKey("DOC_OFFSET") || !val.ContainsKey("SENSE_RESISTOR")) return val;
                        Int32 OFFSET = (Int32)val["DOC_OFFSET"];
                        Double SENSE_RESISTOR = (Double)val["SENSE_RESISTOR"];
                        Double TH = Math.Round(Math.Floor(((((data[0] & 0x3F) + OFFSET) * -5) / SENSE_RESISTOR) * 10) * 0.1, 2);
                        val.Add(cmd, TH);
                        break;
                    }
                case "OC_DELAY":
                    {
                        val.Add(cmd, (Int32)((0x004 << (data[0] & 0x07)) - 2) * (((data[0] >> 0x03) & 0x1F) + 1));
                        break;
                    }
                case "SC_TH":
                    {
                        if (!val.ContainsKey("SC_OFFSET") || !val.ContainsKey("SENSE_RESISTOR")) return val;
                        Int32 OFFSET = (Int32)val["SC_OFFSET"];
                        Double SENSE_RESISTOR = (Double)val["SENSE_RESISTOR"];
                        Double TH = Math.Round(Math.Floor((((((data[0] & 0x3F) + OFFSET) + 2) * 10) / SENSE_RESISTOR) * 10) * 0.1, 2);
                        val.Add(cmd, TH);
                        break;
                    }
                case "SC_DELAY":
                    {
                        val.Add(cmd, (Int32)(0x008 * (1 << (data[0] & 0x07))) * (((data[0] >> 0x03) & 0x1F) + 1));
                        break;
                    }
                case "COC_RELEASE":
                    {
                        Int32 tmp = (data[0] & 0x07);
                        if ((data[0] & 0x07) == 0) val.Add(cmd, 1);
                        else if ((data[0] & 0x07) == 1) val.Add(cmd, 1);
                        else if ((data[0] & 0x07) == 2) val.Add(cmd, 2);
                        else if ((data[0] & 0x07) == 3) val.Add(cmd, 4);
                        else if ((data[0] & 0x07) == 4) val.Add(cmd, 8);
                        else if ((data[0] & 0x07) == 5) val.Add(cmd, 16);
                        else if ((data[0] & 0x07) == 6) val.Add(cmd, 24);
                        else if ((data[0] & 0x07) == 7) val.Add(cmd, 32);
                        break;
                    }
                case "DOC_RELEASE":
                    {
                        Int32 tmp = ((data[0] >> 3) & 0x07);
                        if (tmp == 0) val.Add(cmd, 0);
                        else if (tmp == 1) val.Add(cmd, 1);
                        else if (tmp == 2) val.Add(cmd, 2);
                        else if (tmp == 3) val.Add(cmd, 4);
                        else if (tmp == 4) val.Add(cmd, 8);
                        else if (tmp == 5) val.Add(cmd, 16);
                        else if (tmp == 6) val.Add(cmd, 24);
                        else if (tmp == 7) val.Add(cmd, 32);
                        break;
                    }
                case "ENABLE_IDL_BLD":
                    {
                        Int32 tmp = ((data[0] >> 3) & 0x07);
                        val.Add(cmd, (Int32)((tmp == 0) ? 0 : (0x01 << (tmp - 1))));
                        break;
                    }
                case "NO_ER_DSPL":
                    {
                        Int32 tmp = ((data[0] >> 3) & 0x07);
                        val.Add(cmd, (Boolean)(((data[0] >> 6) & 0x01) != 0));
                        break;
                    }
                case "OTUT_DELAY":
                    {
                        val.Add(cmd, (Int32)((((data[0] >> 4) & 0x0F) + 1)));
                        break;
                    }
                case "OVUV_DELAY":
                    {
                        val.Add(cmd, (Int32)((data[0] & 0x0F) + 1));
                        break;
                    }
                case "I2C_ADDRESS":
                    {
                        val.Add(cmd, (Int32)(0x60 + (2 * (data[0] & 0x0F))));
                        break;
                    }
                case "ENABLE_PEC":
                    {
                        val.Add(cmd, (Boolean)(((data[0] >> 4) & 0x01) == 1));
                        break;
                    }
                case "SC_RELEASE":
                    {
                        val.Add(cmd, (Int32)((((data[0] >> 5) & 0x07) * 15)));
                        break;
                    }
                case "WAKEUP_CONTROL":
                    {
                        val.Add(cmd, (Int32)(data[0] & 0x0F));
                        break;
                    }
                case "ENABLE_T1E":
                    {
                        val.Add(cmd, (Boolean)(((data[0] >> 7) & 0x01) == 1));
                        break;
                    }
                case "ENABLE_T2E":
                    {
                        val.Add(cmd, (Boolean)(((data[0] >> 6) & 0x01) == 1));
                        break;
                    }
                case "FACTORY_NAME":
                case "PROJECT_NAME":
                    {
                        String str = "";
                        for (int i = 0; i < data.Length; i++)
                            str += String.Format("{0}", (char)data[i]).ToString().Trim('\0');
                        val.Add(cmd, str.Trim());
                        break;
                    }
                case "VERSION_NUMBER":
                    {
                        char c = Convert.ToChar(65 + (data[0] >> 0x04));
                        val.Add(cmd, string.Format("{0}{1:00}", c, (data[0] & 0x0F)));
                        break;
                    }
                case "CELL_UNBALANCE_TH":
                case "OV_TH":
                case "OV_RELEASE":
                case "UV_TH":
                case "UV_RELEASE":
                case "EXTRE_HIGH_VOLT_TH":
                case "EXTRE_LOW_VOLT_TH":
                    {
                        Int32 tmp = Convert.ToInt32(((data[1] << 5) | (data[0] >> 3)) * 1.2207);
                        val.Add(cmd, tmp);
                        break;
                    }
                case "OTE_TH":
                case "OTE_RELEASE":
                case "UTE_TH":
                case "UTE_RELEASE":
                    {
                        Int32 temperature = Convert.ToInt32(OZ890EEP.GetExternalTemperature(((data[1] << 5) | (data[0] >> 3))));
                        val.Add(cmd, temperature);
                        break;
                    }
                case "OTI_TH":
                case "OTI_RELEASE":
                case "UTI_TH":
                case "UTI_RELEASE":
                    {
                        if (!val.ContainsKey("TEMP_OFFSET")) return val;

                        Int32 temperature = Convert.ToInt32(OZ890EEP.GetInternalTemperature(((data[1] << 5) | (data[0] >> 3)), (Int32)val["TEMP_OFFSET"]));
                        val.Add(cmd, temperature);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            return val;
        }

        public static byte[] ConvertObjToArray(string cmd, object obj, byte[] ozeep)
        {
            List<byte> array = new List<byte>();

            switch (cmd)
            {
                case "COC_TH":
                    {
                        if (ozeep == null) return array.ToArray();
                        Dictionary<string, object> paramData = new Dictionary<string, object>();
                        OZ890EEP.ConvertArrayToCmdInfo("COC_OFFSET", ozeep, paramData);
                        OZ890EEP.ConvertArrayToCmdInfo("SENSE_RESISTOR", ozeep, paramData);
                        if (!paramData.ContainsKey("COC_OFFSET") || !paramData.ContainsKey("SENSE_RESISTOR")) return array.ToArray();

                        Int32 OFFSET = (Int32)paramData["COC_OFFSET"];
                        Double SENSE_RESISTOR = (Double)paramData["SENSE_RESISTOR"];

                        array.Add(Convert.ToByte(Convert.ToInt64((((((Double)obj) * SENSE_RESISTOR) / 5) + 4) - OFFSET) & 0x1F));
                        break;
                    }
                case "DOC_TH":
                    {
                        if (ozeep == null) return array.ToArray();
                        Dictionary<string, object> paramData = new Dictionary<string, object>();
                        OZ890EEP.ConvertArrayToCmdInfo("DOC_OFFSET", ozeep, paramData);
                        OZ890EEP.ConvertArrayToCmdInfo("SENSE_RESISTOR", ozeep, paramData);
                        if (!paramData.ContainsKey("DOC_OFFSET") || !paramData.ContainsKey("SENSE_RESISTOR")) return array.ToArray();

                        Int32 OFFSET = (Int32)paramData["DOC_OFFSET"];
                        Double SENSE_RESISTOR = (Double)paramData["SENSE_RESISTOR"];

                        array.Add(Convert.ToByte(Convert.ToInt64((((((Double)obj) * SENSE_RESISTOR)) + 5) - OFFSET) & 0x3F));
                        break;
                    }
                case "OC_DELAY":
                    {
                        break;
                    }
                case "SC_TH":
                    {
                        if (ozeep == null) return array.ToArray();
                        Dictionary<string, object> paramData = new Dictionary<string, object>();
                        OZ890EEP.ConvertArrayToCmdInfo("SC_OFFSET", ozeep, paramData);
                        OZ890EEP.ConvertArrayToCmdInfo("SENSE_RESISTOR", ozeep, paramData);
                        if (!paramData.ContainsKey("SC_OFFSET") || !paramData.ContainsKey("SENSE_RESISTOR")) return array.ToArray();
                        
                        Int32 OFFSET = (Int32)paramData["SC_OFFSET"];
                        Double SENSE_RESISTOR = (Double)paramData["SENSE_RESISTOR"];

                        array.Add(Convert.ToByte(Convert.ToInt64((((((Double)obj) * SENSE_RESISTOR) * 0.1) - 2) - OFFSET) & 0x3F));
                        break;
                    }
                case "SC_DELAY":
                    {
                        break;
                    }
                case "COC_RELEASE":
                    {
                        if (ozeep == null) return array.ToArray();
                        byte[] tmpArray = BatCore.GetPBCmdRawArray("COC_RELEASE", ozeep);
                        if (tmpArray.Length == 0) return array.ToArray();

                        Int32 tmp = (Int32)obj;
                        if (tmp == 0) tmp = 0;
                        else if (tmp == 1) tmp = 1;
                        else if (tmp == 2) tmp = 2;
                        else if (tmp == 4) tmp = 3;
                        else if (tmp == 8) tmp = 4;
                        else if (tmp == 16) tmp = 5;
                        else if (tmp == 24) tmp = 6;
                        else if (tmp == 32) tmp = 7;

                        byte tmpVal = Convert.ToByte((tmp & 0x07));
                        array.Add((byte)((tmpArray[0] & 0xF8) | tmpVal));
                        break;
                    }
                case "DOC_RELEASE":
                    {
                        if (ozeep == null) return array.ToArray();
                        byte[] tmpArray = BatCore.GetPBCmdRawArray("DOC_RELEASE", ozeep);
                        if (tmpArray.Length == 0) return array.ToArray();

                        Int32 tmp = (Int32)obj;
                        if (tmp == 0) tmp = 0;
                        else if (tmp == 1) tmp = 1;
                        else if (tmp == 2) tmp = 2;
                        else if (tmp == 4) tmp = 3;
                        else if (tmp == 8) tmp = 4;
                        else if (tmp == 16) tmp = 5;
                        else if (tmp == 24) tmp = 6;
                        else if (tmp == 32) tmp = 7;

                        byte tmpVal = Convert.ToByte((tmp & 0x07) << 0x03);
                        array.Add((byte)((tmpArray[0] & 0xC7) | tmpVal));
                        break;
                    }
                case "OTUT_DELAY":
                    {
                        if (ozeep == null) return array.ToArray();
                        byte[] tmpArray = BatCore.GetPBCmdRawArray("OTUT_DELAY", ozeep);
                        if (tmpArray.Length == 0) return array.ToArray();

                        byte tmpVal = Convert.ToByte((((Int32)obj - 1) & 0x0F) << 0x04);
                        array.Add((byte)((tmpArray[0] & 0x0F) | tmpVal));
                        break;
                    }
                case "OVUV_DELAY":
                    {
                        if (ozeep == null) return array.ToArray();
                        byte[] tmpArray = BatCore.GetPBCmdRawArray("OVUV_DELAY", ozeep);
                        if (tmpArray.Length == 0) return array.ToArray();
                        
                        byte tmpVal = Convert.ToByte((((Int32)obj -1) & 0x0F));
                        array.Add((byte)((tmpArray[0] & 0xF0) | tmpVal));
                        break;
                    }
                case "SC_RELEASE":
                    {
                        if (ozeep == null) return array.ToArray();
                        byte[] tmpArray = BatCore.GetPBCmdRawArray("SC_RELEASE", ozeep);
                        if (tmpArray.Length == 0) return array.ToArray();

                        byte tmpVal = Convert.ToByte((((Int32)obj / 15) & 0x07) << 5);
                        array.Add((byte)((tmpArray[0] & 0x07) | tmpVal));
                        break;
                    }
                case "FACTORY_NAME":
                case "PROJECT_NAME":
                    {
                        byte[] tmp = Encoding.ASCII.GetBytes((String)obj);
                        for (int i = 0; i < 10; i++)
                        {
                            if (i < tmp.Length)
                            {
                                array.Add(tmp[i]);
                            }
                            else
                            {
                                array.Add(0);
                            }
                        }
                        break;
                    }
                case "VERSION_NUMBER":
                    {
                        string str = obj.ToString();
                        byte aChar = (byte)(Convert.ToByte(str.Substring(0, 1), 16) - 10);
                        byte bChar = Convert.ToByte(str.Substring(1, 2), 16);
                        array.Add((byte)((aChar << 0x04) | (bChar & 0x0F)));
                        break;
                    }
                case "CELL_UNBALANCE_TH":
                case "OV_TH":
                case "OV_RELEASE":
                case "UV_TH":
                case "UV_RELEASE":
                case "EXTRE_HIGH_VOLT_TH":
                case "EXTRE_LOW_VOLT_TH":
                    {
                        Int32 tmp = Convert.ToInt32(((Int32)obj)  * 0.8192);
                        array.Add((byte)((tmp << 3) & 0xF8));
                        array.Add((byte)((tmp >> 5) & 0xFF));
                        break;
                    }
                case "OTE_TH":
                case "OTE_RELEASE":
                case "UTE_TH":
                case "UTE_RELEASE":
                    {
                        Int32 adc = OZ890EEP.GetExternalADC(((Int32)obj));
                        array.Add((byte)((adc << 3) & 0xF8));
                        array.Add((byte)((adc >> 5) & 0xFF));
                        break;
                    }
                case "OTI_TH":
                case "OTI_RELEASE":
                case "UTI_TH":
                case "UTI_RELEASE":
                    {
                        if (ozeep == null) return array.ToArray();
                        Dictionary<string, object> paramData = new Dictionary<string, object>();
                        OZ890EEP.ConvertArrayToCmdInfo("TEMP_OFFSET", ozeep, paramData);
                        if (!paramData.ContainsKey("TEMP_OFFSET")) return array.ToArray();

                        Int32 adc = OZ890EEP.GetInternalADC(((Int32)obj), (Int32)paramData["TEMP_OFFSET"]);
                        array.Add((byte)((adc << 3) & 0xF8));
                        array.Add((byte)((adc >> 5) & 0xFF));
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            return array.ToArray();
        }
    }
}
