﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTCore
{
    public class Utility
    {
        public static bool CheckAttr(Dictionary<string, object> source, Dictionary<string, object> target, string[] attrList)
        {
            foreach (string item in attrList)
            {
                if (source.ContainsKey(item) && target.ContainsKey(item))
                {
                    return source[item].ToString() == target[item].ToString();
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
    }
}
