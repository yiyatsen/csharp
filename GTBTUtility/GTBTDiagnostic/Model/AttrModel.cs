﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTBTDiagnostic.Model
{
    public class AttrModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
