﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTBTDiagnostic.Model
{
    public class UtilitySettingModel
    {
        public string LOGPATH { get; set; }

        public UtilitySettingModel()
        {
            LOGPATH = System.AppDomain.CurrentDomain.BaseDirectory + @"Log\";
        }
    }
}
