﻿using GTBTCore;
using GTBTCore.Cipher;
using GTBTDiagnostic.Model;
using GTBTDiagnostic.ViewModel;
using MahApps.Metro.Controls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GTBTDiagnostic
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public static string APPSETTINGFILE = System.AppDomain.CurrentDomain.BaseDirectory + "AppSetting.ap";

        public static UtilitySettingModel SettingModel {
            get
            {
                if (mSettingModel == null)
                {
                    if (File.Exists(APPSETTINGFILE))
                    {
                        try
                        {
                            using (StreamReader reader = File.OpenText(APPSETTINGFILE))
                            {
                                string txt = reader.ReadToEnd();

                                string fileName = System.IO.Path.GetFileNameWithoutExtension(APPSETTINGFILE);
                                CipherAESNeil mEncryptionAlog = new CipherAESNeil();
                                mEncryptionAlog.SetEncryptionKey(fileName);
                                txt = Encoding.UTF8.GetString(mEncryptionAlog.DecryptionData(Convert.FromBase64String(txt)));

                                using (System.IO.TextReader textReader = new System.IO.StringReader(txt))
                                {
                                    JsonSerializer serializer = new JsonSerializer();
                                    mSettingModel = (UtilitySettingModel)serializer.Deserialize(reader, typeof(UtilitySettingModel));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                if (mSettingModel == null) mSettingModel = new UtilitySettingModel();
                return mSettingModel;
            }
        }

        private static UtilitySettingModel mSettingModel { get; set; }
        private static MainWindow MAINWINDOW { get; set; }

        public static string SaveFile(string folder, string file, string txt)
        {
            try
            {
                if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);

                string fileStr = string.Format("{0}{1}", folder, file);

                using (StreamWriter outfile = new StreamWriter(fileStr))
                {
                    outfile.Write(txt);
                }
                return fileStr;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool RemoveFile(string file)
        {
            try
            {
                if (File.Exists(file)) File.Delete(file);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static bool SetJSONFile(string file, object obj)
        {
            try
            {
                string fileName = System.IO.Path.GetFileNameWithoutExtension(file);
                //string txt = MainWindow.GetJSONString(obj);

                string txt = Utility.ConvertObjToString(obj);
                CipherAESNeil mEncryptionAlog = new CipherAESNeil();
                mEncryptionAlog.SetEncryptionKey(fileName);
                txt = Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.UTF8.GetBytes(txt)));

                using (StreamWriter outfile = new StreamWriter(file))
                {
                    outfile.Write(txt);
                }

            }
            catch (Exception ex)
            {
                // Open File Error
            }
            return true;
        }

        public static void ExitAPP()
        {
            if (MAINWINDOW != null)
            {
                MAINWINDOW.EnableExitAPP = true;
                MAINWINDOW.Close();
            }
        }

        private bool EnableExitAPP = false;
 
        public MainWindow()
        {
            MAINWINDOW = this;
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }

        /// <summary>
        /// 視窗準備關閉之事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (!EnableExitAPP) // 決定可否關閉視窗之條件
            {
                e.Cancel = true;
            }
        }
    }
}
