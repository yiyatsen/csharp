﻿using GTBTCore;
using GTBTDiagnostic.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GTBTDiagnostic.ViewModel
{
    public class UtilityRunDialogViewModel : INotifyPropertyChanged
    {
        public ICommand ConfirmCommand { get { return new MainWindowCommand(Confirm); } }
        public ICommand CancelCommand { get { return new MainWindowCommand(Canecl); } }

        private string[] mPortList = System.IO.Ports.SerialPort.GetPortNames();
        private ObservableCollection<string> mCOMPortList = new ObservableCollection<string>();
        private string mErrorMessage = "";
        private COMPort.COMPortConfig mConfig = new COMPort.COMPortConfig();

        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<string> COMPortList
        {
            get { return mCOMPortList; }
            set { mCOMPortList = value; NotifyPropertyChanged("COMPortList"); }
        }
        public string TargetCOMPort
        {
            get { return mConfig.COMPort; }
            set { mConfig.COMPort = value; NotifyPropertyChanged("TargetCOMPort"); }
        }
        public string ErrorMessage
        {
            get { return mErrorMessage; }
            set { mErrorMessage = value; NotifyPropertyChanged("ErrorMessage"); }
        }

        public UtilityRunDialogViewModel(COMPort.COMPortConfig config)
        {
            mConfig.COMPort = config.COMPort;
            mConfig.BaudRate = config.BaudRate;
            mConfig.EnableASCII = config.EnableASCII;
            SetCOMPortList();
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void NotifyPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged != null) foreach (string propertyName in propertyNames) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SetCOMPortList()
        {
            mCOMPortList.Clear();

            string matchTargetStr = "";
            foreach (string str in mPortList)
            {
                mCOMPortList.Add(str);
                TargetCOMPort = str;
                if (mConfig.COMPort == str) matchTargetStr = str;
            }

            if (matchTargetStr != "") TargetCOMPort = matchTargetStr;
            COMPortList = mCOMPortList;
        }

        private void Confirm(object obj)
        {
            if (mConfig.COMPort != null && mConfig.COMPort == "")
            {
                ErrorMessage = "Target cannot empty.";
                return;
            }
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(mConfig, null);
        }

        private void Canecl(object obj)
        {
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(null, null);
        }
    }
}
