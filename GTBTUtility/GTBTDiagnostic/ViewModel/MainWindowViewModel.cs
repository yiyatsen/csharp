﻿using GTBTCore;
using GTBTCore.MCUEEP;
using GTBTDiagnostic.Model;
using LiveCharts;
using LiveCharts.Wpf;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace GTBTDiagnostic.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private static bool ISBETA = false;
        private static bool ENABLEENGMODE = false;
        private static long REFRESHCHARTTIMER = 6000000000;
        private static int AUTORUNTIMER = 1500;

        public ICommand EngLoginCommand { get { return new MainWindowCommand(EngLogin); } }
        public ICommand ExcuteUtilityDialogCommand { get { return new MainWindowCommand(ExcuteUtilityDialog); } }
        public ICommand StopUtilityDialogCommand { get { return new MainWindowCommand(StopUtilityDialog); } }
        public ICommand SettingsCommand { get { return new MainWindowCommand(Settings); } }
        public ICommand SaveBTCommand { get { return new MainWindowCommand(SaveBT); } }
        public ICommand AutoLogBTCommand { get { return new MainWindowCommand(AutoLogBT); } }
        public ICommand DetailBTCommand { get { return new MainWindowCommand(DetailBT); } }
        public ICommand EditBTCommand { get { return new MainWindowCommand(EditBT); } }
        public ICommand UpdateBTCommand { get { return new MainWindowCommand(UpdateBT); } }
        public ICommand ExitAppCommand { get { return new MainWindowCommand(ExitApp); } }
        
        public bool IsConnection
        {
            get { return mTargetSlot.autoSetting.isBatteryIn; }
            set {
                NotifyPropertyChanged("IsConnection");
                NotifyPropertyChanged("COMPort");
                NotifyPropertyChanged("BaudRate");
                NotifyPropertyChanged("ConnectionVisibility");
                NotifyPropertyChanged("DisconnectionVisibility");
                NotifyPropertyChanged("EngConnectionVisibility");
            }
        }
        private Double mAutoLogOpacity = 0.5;
        public Double AutoLogOpacity
        {
            get { return mAutoLogOpacity; }
            set {
                mAutoLogOpacity = value;
                NotifyPropertyChanged("AutoLogOpacity");
            }
        }
        public System.Windows.Visibility ConnectionVisibility
        {
            get { return (IsConnection) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("ConnectionVisibility"); }
        }
        public System.Windows.Visibility DisconnectionVisibility
        {
            get { return (!IsConnection) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("DisconnectionVisibility"); }
        }
        public System.Windows.Visibility EngConnectionVisibility
        {
            get { return (IsConnection && Utility.EnableEngMode) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EngConnectionVisibility"); }
        }
        public System.Windows.Visibility EngVisibility
        {
            get { return (Utility.EnableEngMode) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EngVisibility"); }
        }
        public string BaudRate
        {
            get { return string.Format("BaudRate: {0}", mTargetSlot.baudRate); }
            set { NotifyPropertyChanged("BaudRate"); }
        }
        public string COMPort
        {
            get { return string.Format("COM: {0}", mTargetSlot.path); }
            set { NotifyPropertyChanged("COMPort"); }
        }
        public string AppMode
        {
            get { return string.Format("Mode: {0}", Utility.EnableEngMode ? "Engineer" : "Normal"); }
            set { NotifyPropertyChanged("AppMode"); }
        }

        public string BTTimestamp
        {
            get {
                if (mBatInfo == null) return "";
                DateTime dt = mBatInfo.timestamp.ToLocalTime();
                return string.Format("Refresh: {0:00}:{1:00}:{2:00}", dt.Hour, dt.Minute, dt.Second);
            }
            set { NotifyPropertyChanged("BTTimestamp"); }
        }
        public string batteryType
        {
            get { return (mBatInfo == null) ? " " : mBatInfo.batteryType; }
            set { NotifyPropertyChanged("batteryType"); }
        }
        public string batteryId
        {
            get { return (mBatInfo == null) ? " " : mBatInfo.batteryId; }
            set { NotifyPropertyChanged("batteryId"); }
        }
        public string MANUFACTURE_DATE
        {
            get { return (mBatInfo != null && mBatInfo.MANUFACTURE_DATE != null) ? string.Format("{0:0000}/{1:00}/{2:00}", mBatInfo.MANUFACTURE_DATE[0], mBatInfo.MANUFACTURE_DATE[1], mBatInfo.MANUFACTURE_DATE[2]) : " "; }
            set { NotifyPropertyChanged("MANUFACTURE_DATE"); }
        }
        public string DESIGN_VOLTAGE
        {
            get { return (mBatInfo == null) ? " " : string.Format("{0}", mBatInfo.DESIGN_VOLTAGE); }
            set { NotifyPropertyChanged("DESIGN_VOLTAGE"); }
        }
        public string DESIGN_CAPACITY
        {
            get { return (mBatInfo == null) ? " " : string.Format("{0}", mBatInfo.DESIGN_CAPACITY); }
            set { NotifyPropertyChanged("DESIGN_CAPACITY"); }
        }
        public string REMAIN_CAPACITY
        {
            get { return (mBatInfo == null) ? " " : string.Format("{0}", mBatInfo.REMAIN_CAPACITY); }
            set { NotifyPropertyChanged("REMAIN_CAPACITY"); }
        }
        public string FULL_CHARGE_CAPACITY
        {
            get { return (mBatInfo == null) ? " " : string.Format("{0}", mBatInfo.FULL_CHARGE_CAPACITY); }
            set { NotifyPropertyChanged("FULL_CHARGE_CAPACITY"); }
        }
        public string CYCLE_COUNT
        {
            get { return (mBatInfo == null) ? " " : string.Format("{0}", mBatInfo.CYCLE_COUNT); }
            set { NotifyPropertyChanged("CYCLE_COUNT"); }
        }
        public uint SOC
        {
            get { return (mBatInfo == null) ? 0 : mBatInfo.SOC; }
            set { NotifyPropertyChanged("SOC"); }
        }
        public uint SOH
        {
            get { return (mBatInfo == null) ? 0 : mBatInfo.SOH; }
            set { NotifyPropertyChanged("SOH"); }
        }
        public string TEMPERATURESTR
        {
            get { return ((mBatInfo == null) ? -30.0 : mBatInfo.TEMPERATURE).ToString(); }
            set { NotifyPropertyChanged("TEMPERATURESTR"); }
        }
        public double TEMPERATURE
        {
            get { return (mBatInfo == null) ? -30.0 : mBatInfo.TEMPERATURE; }
            set { NotifyPropertyChanged("TEMPERATURE"); }
        }
        public string VOLTAGESTR
        {
            get {
                if ((mBatInfo == null)) return "0.0";
                return (mBatInfo.TOTAL_VOLT * 0.001).ToString();
            }
            set { NotifyPropertyChanged("TEMPERATURESTR"); }
        }
        public double VOLTAGE
        {
            get
            {
                if ((mBatInfo == null)) return 0.0;
                return (mBatInfo.TOTAL_VOLT * 0.001);
            }
            set { NotifyPropertyChanged("TEMPERATURESTR"); }
        }
        public string CURRENTSTR
        {
            get
            {
                if ((mBatInfo == null)) return "0.0";
                return (mBatInfo.CURRENT * 0.001).ToString();
            }
            set { NotifyPropertyChanged("CURRENTSTR"); }
        }
        public double CURRENT
        {
            get
            {
                if ((mBatInfo == null)) return 0.0;
                return (mBatInfo.CURRENT * 0.001);
            }
            set { NotifyPropertyChanged("CURRENT"); }
        }
        public ImageSource OVIMAGE
        {
            get
            {
                if ((mBatInfo == null)) return OFFIMAGE;
                return (((mBatInfo.PROTECTION >> 0) & 0x01) != 0) ? ONMAGE : OFFIMAGE;
            }
            set { NotifyPropertyChanged("OVIMAGE"); }
        }
        public ImageSource UVIMAGE
        {
            get
            {
                if ((mBatInfo == null)) return OFFIMAGE;
                return (((mBatInfo.PROTECTION >> 1) & 0x01) != 0) ? ONMAGE : OFFIMAGE;
            }
            set { NotifyPropertyChanged("UVFLAG"); }
        }
        public ImageSource COCIMAGE
        {
            get
            {
                if ((mBatInfo == null)) return OFFIMAGE;
                return (((mBatInfo.PROTECTION >> 2) & 0x01) != 0) ? ONMAGE : OFFIMAGE;
            }
            set { NotifyPropertyChanged("COCVFLAG"); }
        }
        public ImageSource DOCIMAGE
        {
            get
            {
                if ((mBatInfo == null)) return OFFIMAGE;
                return (((mBatInfo.PROTECTION >> 3) & 0x01) != 0) ? ONMAGE : OFFIMAGE;
            }
            set { NotifyPropertyChanged("DOCFLAG"); }
        }
        public ImageSource OTIMAGE
        {
            get
            {
                if ((mBatInfo == null)) return OFFIMAGE;
                return (((mBatInfo.PROTECTION >> 4) & 0x01) != 0) ? ONMAGE : OFFIMAGE;
            }
            set { NotifyPropertyChanged("OTFLAG"); }
        }
        public ImageSource UTIMAGE
        {
            get
            {
                if ((mBatInfo == null)) return OFFIMAGE;
                return (((mBatInfo.PROTECTION >> 5) & 0x01) != 0) ? ONMAGE : OFFIMAGE;
            }
            set { NotifyPropertyChanged("UTIMAGE"); }
        }
        public ImageSource SCIMAGE
        {
            get
            {
                if ((mBatInfo == null)) return OFFIMAGE;
                return (((mBatInfo.PROTECTION >> 6) & 0x01) != 0) ? ONMAGE : OFFIMAGE;
            }
            set { NotifyPropertyChanged("SCIMAGE"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private ImageSource OFFIMAGE = new BitmapImage(new Uri("res/image/SirenOff.png", UriKind.Relative));
        private ImageSource ONMAGE = new BitmapImage(new Uri("res/image/SirenOn.png", UriKind.Relative));

        private bool EnableStopClick = false;
        private COMPort.COMPortConfig mDefaultConfig = GTBTCore.COMPort.DEFAULTBAUDRATEINFO;
        private string mAppVersion { get; set; }
        private string mCoreVersion { get; set; }

        private Slot mTargetSlot = new Slot() { slotId = 1 };

        public string AppVersion
        {
            get { return mAppVersion; }
            set
            {
                mAppVersion = value;
                NotifyPropertyChanged("AppVersion");
            }
        }

        public string CoreVersion
        {
            get { return mCoreVersion; }
            set
            {
                mCoreVersion = value;
                NotifyPropertyChanged("CoreVersion");
            }
        }

        private BatInfo mBatInfo { get; set; }

        private DateTime RecordDT = DateTime.Now;
        private UInt32 Counter = 0;
        private bool IsTriggerProtection = false;
        private bool EnableAutoLog = false;
        private int AutoLogState = 0;

        //public SeriesCollection ChartCollection { get; set; }
        //public List<string> ChartXLabels { get; set; }
        //public Func<double, string> ChartYLabels { get; set; }

        public ChartValues<Int32> CellValues { get; set; }
        public List<string> CellXLabels { get; set; }
        public Func<double, string> CellYLabels { get; set; }

        public MainWindowViewModel()
        {
            Slot.AUTORUNTIMER = AUTORUNTIMER;

            Utility.EnableEngMode = ENABLEENGMODE;

            AppVersion = string.Format("Ver: {0}{1}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(), (ISBETA) ? "(BETA)" : "");
            CoreVersion = string.Format("CoreVer: {0}", GTBTCore.Utility.Version);
            AppMode = "";
            NotifyPropertyChanged("EngVisibility");

            //ResetChartCollection();
            ResetCellCollection();

            mTargetSlot.SlotEvent += Slot_SlotEvent;
        }
        
        //private void ResetChartCollection()
        //{
        //    if (ChartCollection == null)
        //    {
        //        ChartCollection = new SeriesCollection()
        //        {
        //            new LineSeries { Title = "SOC (%)", Values = new ChartValues<double>() },
        //            new LineSeries { Title = "CURRENT (A)", Values = new ChartValues<double>() },
        //            new LineSeries { Title = "RC (A)", Values = new ChartValues<double>() },
        //            new LineSeries { Title = "FCC (A)", Values = new ChartValues<double>() },
        //            new LineSeries { Title = "VOLT (V)", Values = new ChartValues<double>() },
        //            new LineSeries { Title = "TEMP (°C)", Values = new ChartValues<double>() },
        //            new LineSeries { Title = "PROTECTION", Values = new ChartValues<double>() }
        //        };
        //    }

        //    if (ChartXLabels == null) ChartXLabels = new List<string>();

        //    foreach (var item in ChartCollection)
        //    {
        //        item.Values.Clear();
        //    }
        //    ChartXLabels.Clear();
        //    ChartYLabels = value => value.ToString();
        //}

        //private void RefreshChartCollection(BatInfo batInfo)
        //{
        //    DateTime dtStart = DateTime.Now;
        //    string dtStr = dtStart.ToString("HH':'mm':'ss");
        //    if (Counter < 1 || (dtStart.Ticks - RecordDT.Ticks) > REFRESHCHARTTIMER)
        //    {
        //        if (ChartXLabels.Count > 1440)
        //        {
        //            ChartXLabels.RemoveAt(0);
        //            ChartCollection[0].Values.RemoveAt(0);
        //            ChartCollection[1].Values.RemoveAt(0);
        //            ChartCollection[2].Values.RemoveAt(0);
        //            ChartCollection[3].Values.RemoveAt(0);
        //            ChartCollection[4].Values.RemoveAt(0);
        //            ChartCollection[5].Values.RemoveAt(0);
        //            ChartCollection[6].Values.RemoveAt(0);
        //        }
        //        ChartXLabels.Add(dtStr);
        //        ChartCollection[0].Values.Add(batInfo.SOC * 1.0);
        //        ChartCollection[1].Values.Add(batInfo.CURRENT * 0.001);
        //        ChartCollection[2].Values.Add(batInfo.REMAIN_CAPACITY * 0.001);
        //        ChartCollection[3].Values.Add(batInfo.FULL_CHARGE_CAPACITY * 0.001);
        //        ChartCollection[4].Values.Add(batInfo.TOTAL_VOLT * 0.001);
        //        ChartCollection[5].Values.Add(batInfo.TEMPERATURE);
        //        ChartCollection[6].Values.Add(batInfo.PROTECTION * 1.0);

        //        RecordDT = dtStart;
        //        if (Counter < 1) Counter++;
        //    }
        //}

        private void ResetCellCollection()
        {
            if (CellValues == null) CellValues = new ChartValues<Int32>();
            if (CellXLabels == null) CellXLabels = new List<string>();

            CellValues.Clear();
            CellXLabels.Clear();
            CellYLabels = value => value.ToString();

            Counter = 0;
        }

        private void RefreshBat(BatInfo batInfo)
        {
            this.mBatInfo = batInfo;

            NotifyPropertyChanged("BTTimestamp");
            NotifyPropertyChanged("batteryType");
            NotifyPropertyChanged("batteryId");
            NotifyPropertyChanged("MANUFACTURE_DATE");
            NotifyPropertyChanged("DESIGN_VOLTAGE");
            NotifyPropertyChanged("DESIGN_CAPACITY");
            NotifyPropertyChanged("REMAIN_CAPACITY");
            NotifyPropertyChanged("FULL_CHARGE_CAPACITY");
            NotifyPropertyChanged("CYCLE_COUNT");

            NotifyPropertyChanged("SOC");
            NotifyPropertyChanged("SOH");
            NotifyPropertyChanged("TEMPERATURESTR");
            NotifyPropertyChanged("TEMPERATURE");
            NotifyPropertyChanged("VOLTAGESTR");
            NotifyPropertyChanged("VOLTAGE");
            NotifyPropertyChanged("CURRENTSTR");
            NotifyPropertyChanged("CURRENT");

            NotifyPropertyChanged("OVIMAGE");
            NotifyPropertyChanged("UVIMAGE");
            NotifyPropertyChanged("COCIMAGE");
            NotifyPropertyChanged("DOCIMAGE");
            NotifyPropertyChanged("OTIMAGE");
            NotifyPropertyChanged("UTIMAGE");
            NotifyPropertyChanged("SCIMAGE");
            
            CellValues.Clear();
            CellXLabels.Clear();
            if (batInfo != null && batInfo.CELL_VOLT != null)
            {
                for (int i = 0; i < batInfo.CELL_VOLT.Length; i++)
                {
                    CellValues.Add(Convert.ToInt32(batInfo.CELL_VOLT[i]));
                    CellXLabels.Add((i + 1).ToString());
                }
            }
            CellYLabels = value => value.ToString();

        }

        private async void Slot_SlotEvent(int slotId, string name, string[] notifyList, BatInfo batInfo)
        {
            if ((IsTriggerProtection && batInfo.PROTECTION == 0) || (!IsTriggerProtection && batInfo.PROTECTION > 0))
            {
                IsTriggerProtection = !IsTriggerProtection;
                Counter = 0;
            }
            
            switch (name)
            {
                case "detectBatteryIn":
                    {
                        RefreshBat(batInfo);
                        //RefreshChartCollection(batInfo);

                        mTargetSlot.AutoRun(false);
                        IsConnection = true;
                        EnableStopClick = true;
                        break;
                    }
                case "reportSlot":
                    {
                        //if (EnableAutoLog)
                        //{
                        //    mTargetSlot.SaveLogFile(batInfo);
                        //}

                        //foreach (var item in notifyList)
                        //{ 
                        //    if (item == "startCharging" || item == "startDischarging")
                        //    {
                        //        if (!EnableAutoLog)
                        //        {
                        //            mTargetSlot.SaveLogFile(batInfo);
                        //            SwitchAutoLog();
                        //            if (AutoLogState == 0) AutoLogState = 2;
                        //        }
                        //    }
                        //    else if (item == "stopCharging" || item == "stopDischarging")
                        //    {
                        //        if (AutoLogState == 2 && EnableAutoLog)
                        //        {
                        //            if (ENABLEENGMODE)
                        //            {
                        //                //  製作分析表
                        //                //  
                        //            }
                        //            SwitchAutoLog();
                        //        }
                        //    }
                        //}

                        RefreshBat(batInfo);
                        //RefreshChartCollection(batInfo);
                        break;
                    }
                case "detectBatteryOut":
                    {
                        //if (EnableAutoLog)
                        //{
                        //    mTargetSlot.SaveLogFile(batInfo);
                        //    if (ENABLEENGMODE)
                        //    {
                        //        //  製作分析表
                        //    }
                        //    if (AutoLogState == 1)
                        //    {
                        //        //  複製檔案到使用者路徑
                        //    }
                        //    // 刪除暫存
                        //    SwitchAutoLog();
                        //}

                        AutoLogState = 0;
                        IsTriggerProtection = false;
                        mTargetSlot.ResetSlotState();

                        await DialogHost.Show(new NotifyDialog()
                        {
                            DataContext = new NotifyDialogViewModel
                            {
                                EnableYesNoQuestion = false,
                                NotifyTitle = "Warning",
                                NotifyMessage = "Lost Battery"
                            }
                        }, "NotifyDialog", (object sender, DialogClosingEventArgs eventArgs) =>
                        {
                            IsConnection = false;
                            EnableStopClick = false;

                            //ResetChartCollection();
                            RefreshBat(null);
                        });
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void ExcuteUtilityDialog(object o)
        {
            await DialogHost.Show(new UtilityRunDialog()
            {
                DataContext = new UtilityRunDialogViewModel(mDefaultConfig)
            }, "RootDialog", Run);
        }

        private async void Run(object sender, DialogClosingEventArgs eventArgs)
        {
            if (eventArgs.Parameter == null) return;

            if (eventArgs.Parameter.GetType().ToString() == "System.Boolean") return;

            mDefaultConfig = (COMPort.COMPortConfig)eventArgs.Parameter;
            eventArgs.Cancel();
            eventArgs.Session.UpdateContent(new ProgressBarDialog());

            string ret = await mTargetSlot.ConnectGTBattery(mDefaultConfig.COMPort);

            if (ret != "Success")
            {
                await DialogHost.Show(new NotifyDialog()
                {
                    DataContext = new NotifyDialogViewModel
                    {
                        EnableYesNoQuestion = false,
                        NotifyTitle = "Warning",
                        NotifyMessage = ret
                    }
                }, "NotifyDialog");
            }
            
            eventArgs.Session.Close(false);
        }
        
        private async void StopUtilityDialog(object o)
        {
            if (EnableStopClick)
            {
                EnableStopClick = false;
                await DialogHost.Show(new NotifyDialog()
                {
                    DataContext = new NotifyDialogViewModel()
                    {
                        EnableYesNoQuestion = true,
                        NotifyTitle = "Warning",
                        NotifyMessage = "Do you want to disconnect the battery?"
                    }
                }, "RootDialog", (object sender, DialogClosingEventArgs eventArgs) =>
                {
                    if ((bool)eventArgs.Parameter == false)
                    {
                        EnableStopClick = true;
                        return;
                    }
                    mTargetSlot.NotifyDisconnectBattery();
                });
            }
        }

        private async void Settings(object o)
        {
            await DialogHost.Show(new UtilitySettingDialog()
            {
                DataContext = new UtilitySettingDialogViewModel()
            }, "RootDialog");
        }

        private async void SaveBT(object o)
        {
            if (IsConnection)
            {
                await DialogHost.Show(new ProgressBarDialog(), "RootDialog", async (object sender, DialogOpenedEventArgs eventargs) => {
                    DateTime dt = DateTime.Now;
                    string idStr = string.Format("{0}_{1}", mBatInfo.batteryType, mBatInfo.batteryId);
                    string str = mTargetSlot.SaveLog(dt);

                    string saveFile = MainWindow.SaveFile(
                         MainWindow.SettingModel.LOGPATH,
                        string.Format("{0}_{1}.txt", idStr, dt.ToString("yyyyMMddHHmmss"), str),
                        str
                    );

                    if (saveFile == null)
                    {
                        await DialogHost.Show(new UtilitySettingDialog()
                        {
                            DataContext = new UtilitySettingDialogViewModel()
                        }, "NotifyDialog");
                    } else
                    {
                        await DialogHost.Show(new NotifyDialog()
                        {
                            DataContext = new NotifyDialogViewModel
                            {
                                EnableYesNoQuestion = false,
                                NotifyTitle = "Log",
                                NotifyMessage = str
                            }
                        }, "NotifyDialog", async (object sender2, DialogOpenedEventArgs eventargs2) =>
                        {
                            eventargs.Session.Close(false);
                        });
                    }
                });
            }
        }

        private void SwitchAutoLog()
        {
            EnableAutoLog = !EnableAutoLog;
            if (EnableAutoLog) AutoLogOpacity = 1;
            else AutoLogOpacity = 0.5;
        }

        private void AutoLogBT(object o)
        {
            //if (IsConnection)
            //{
            //    if (AutoLogState < 2)
            //    {
            //        AutoLogState = (AutoLogState == 0) ? 1 : 0;
            //        SwitchAutoLog();

            //        if (AutoLogState == 1)
            //        {
            //            //  複製檔案到使用者路徑
            //        }
            //    }
            //}
        }

        private async void DetailBT(object o)
        {
            if (IsConnection)
            {
                Dictionary<string, List<AttrModel>> data = new Dictionary<string, List<AttrModel>>();

                Dictionary<string, object> temp = Utility.GetDataByKey((from item in BatCore.BATPARAMLIST select item.Name).ToArray(), mTargetSlot.batInfo);

                List<AttrModel> tempStr = new List<AttrModel>();
                if (mTargetSlot.batInfo.ContainsKey("MCU_VERSION")) tempStr.Add(new AttrModel() { Key = "MCU_VERSION", Value = Utility.ConvertObjToString(mTargetSlot.batInfo["MCU_VERSION"]) });
                if (mTargetSlot.batInfo.ContainsKey("RTC"))  tempStr.Add(new AttrModel() { Key = "RTC", Value = Utility.ConvertObjToString(mTargetSlot.batInfo["RTC"]) });
                foreach (string key in temp.Keys)
                {
                    tempStr.Add(new AttrModel() { Key = key, Value = Utility.ConvertObjToString(temp[key]) });
                }
                data.Add(CmdNameType.BAT.ToString(), tempStr);

                temp = mTargetSlot.GetCBEEPToObj();
                tempStr = new List<AttrModel>();
                List<AttrModel> tempStr2 = new List<AttrModel>();
                List<AttrModel> tempStr3 = new List<AttrModel>();
                List<AttrModel> tempStr4 = new List<AttrModel>();
                foreach (string key in temp.Keys)
                {
                    if (key == "ALERTHAPPENTIME")
                    {
                        for (int i = 0; i < BatCore.PROTECTIONLIST.Length; i++)
                        {
                            tempStr2.Add(new AttrModel() { Key = BatCore.PROTECTIONLIST[i], Value = ((string[])temp[key])[i] });
                        }
                    }
                    else if (key == "LOG_CHARGE")
                    {
                        string[] tempValue = ((string[])temp[key]);
                        for (int i = 0; i < tempValue.Length; i++)
                        {
                            if (i == 0)
                            {
                                tempStr3.Add(new AttrModel() { Key = "First Time", Value = tempValue[i] });
                            }
                            else if (i == 1)
                            {
                                tempStr3.Add(new AttrModel() { Key = "Longest Gap", Value = tempValue[i] });
                            }
                            else
                            {
                                tempStr3.Add(new AttrModel() { Key = (i - 1).ToString(), Value = tempValue[i] });
                            }
                        }
                    }
                    else if (key == "LOG_DISCHARGE")
                    {
                        string[] tempValue = ((string[])temp[key]);
                        for (int i = 0; i < tempValue.Length; i++)
                        {
                            tempStr4.Add(new AttrModel() { Key = (i + 1).ToString(), Value = tempValue[i] });
                        }
                    }
                    else
                    {
                        tempStr.Add(new AttrModel() { Key = key, Value = Utility.ConvertObjToString(temp[key]) });
                    }
                }
                if (tempStr.Count > 0) data.Add(CmdNameType.MCU_EEP.ToString(), tempStr);
                if (tempStr2.Count > 0) data.Add("ALERTHAPPENTIME", tempStr2);
                if (tempStr3.Count > 0) data.Add("LOG_CHARGE", tempStr3);
                if (tempStr4.Count > 0) data.Add("LOG_DISCHARGE", tempStr4);

                List<CmdConfig> readableOZ890List = EEPOZ890.CMDCONFIGLIST.FindAll(item => item.IsHidden == false);
                string[] oz890List = (from item in readableOZ890List select item.Name).ToArray();
                temp = Utility.GetDataByKey(oz890List, mTargetSlot.GetPBEEPToObj("1"));
                if (temp.Keys.Count > 0)
                {
                    tempStr = new List<AttrModel>();
                    foreach (string key in temp.Keys)
                    {
                        tempStr.Add(new AttrModel() { Key = key, Value = Utility.ConvertObjToString(temp[key]) });
                    }
                    data.Add(CmdNameType.OZ1_EEP.ToString(), tempStr);
                }

                temp = Utility.GetDataByKey(oz890List, mTargetSlot.GetPBEEPToObj("2"));
                if (temp.Keys.Count > 0)
                {
                    tempStr = new List<AttrModel>();
                    foreach (string key in temp.Keys)
                    {
                        tempStr.Add(new AttrModel() { Key = key, Value = Utility.ConvertObjToString(temp[key]) });
                    }
                    data.Add(CmdNameType.OZ2_EEP.ToString(), tempStr);
                }
                
                await DialogHost.Show(new BTDetailDialog()
                {
                    DataContext = new BTDetailDialogViewModel(data)
                }, "RootDialog");
            }
        }

        private async void EditBT(object o)
        {
            await DialogHost.Show(new NotifyDialog()
            {
                DataContext = new NotifyDialogViewModel
                {
                    EnableYesNoQuestion = false,
                    NotifyTitle = "Warning",
                    NotifyMessage = "Not Implement"
                }
            }, "RootDialog");
        }
 
        private async void UpdateBT(object o)
        {
            await DialogHost.Show(new NotifyDialog()
            {
                DataContext = new NotifyDialogViewModel
                {
                    EnableYesNoQuestion = false,
                    NotifyTitle = "Warning",
                    NotifyMessage = "Not Implement"
                }
            }, "RootDialog");
        }

        private async void EngLogin(object o)
        {
            if (!Utility.EnableEngMode)
            {
                await DialogHost.Show(new LoginDialog(), "RootDialog", (object sender2, DialogClosingEventArgs eventArgs) =>
                {
                    if (eventArgs.Parameter == null) return;
                    Utility.EnableEngMode = ((string)eventArgs.Parameter == Utility.EngPwd);
                    NotifyPropertyChanged("EngConnectionVisibility");
                    NotifyPropertyChanged("EngVisibility");
                });
            }
        }

        private async void ExitApp(object obj)
        {
            await DialogHost.Show(new NotifyDialog()
            {
                DataContext = new NotifyDialogViewModel()
                {
                    EnableYesNoQuestion = true,
                    NotifyTitle = "Warning",
                    NotifyMessage = "Do you want to leave now?"
                }
            }, "RootDialog", (object sender, DialogClosingEventArgs eventArgs) =>
            {
                if ((bool)eventArgs.Parameter == false) return;

                MainWindow.ExitAPP();
            });
        }
    }
}
