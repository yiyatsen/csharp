﻿using GTBTDiagnostic.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GTBTDiagnostic.ViewModel
{
    public class UtilitySettingDialogViewModel : INotifyPropertyChanged
    {
        public ICommand SettingLogPathCommand { get { return new MainWindowCommand(SettingLogPath); } }
        public ICommand SubmitCommand { get { return new MainWindowCommand(Submit); } }
        public ICommand CancelCommand { get { return new MainWindowCommand(Cancel); } }
        
        private UtilitySettingModel mUtilitySettingModel = new UtilitySettingModel();

        public event PropertyChangedEventHandler PropertyChanged;
        private string mErrorMsg { get; set; }

        public string LOGPATH
        {
            get { return mUtilitySettingModel.LOGPATH; }
            set
            {
                mUtilitySettingModel.LOGPATH = value;
                NotifyPropertyChanged("LOGPATH");
            }
        }

        public string ErrorMsg
        {
            get { return mErrorMsg; }
            set
            {
                mErrorMsg = value;
                NotifyPropertyChanged("ErrorMsg");
            }

        }

        public UtilitySettingDialogViewModel()
        {
            LOGPATH = MainWindow.SettingModel.LOGPATH;


            string file = MainWindow.SaveFile(LOGPATH, "temp", "");
            if (file != null)
            {
                MainWindow.RemoveFile(file);
            }
            else
            {
                ErrorMsg = "LOG PATH permission denied";
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        
        private void SettingLogPath(object obj)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();

            if (Directory.Exists(LOGPATH))
            {
                dialog.SelectedPath = LOGPATH;
            }
            else
            {
                dialog.SelectedPath = System.AppDomain.CurrentDomain.BaseDirectory;
            }
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                LOGPATH = dialog.SelectedPath;
            }
        }

        private void Submit(object obj)
        {
            string file = MainWindow.SaveFile(LOGPATH, "temp", "");
            if (file == null)
            {
                ErrorMsg = "LOG PATH permission denied";
                return;
            }
            MainWindow.RemoveFile(file);
            // save setting
            MainWindow.SettingModel.LOGPATH = LOGPATH;
            MainWindow.SetJSONFile(MainWindow.APPSETTINGFILE, MainWindow.SettingModel);
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(true, null);
        }

        private void Cancel(object obj)
        {
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(false, null);
        }
    }
}
