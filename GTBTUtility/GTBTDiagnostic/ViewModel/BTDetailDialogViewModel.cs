﻿using GTBTDiagnostic.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GTBTDiagnostic.ViewModel
{
    public class BTDetailDialogViewModel : INotifyPropertyChanged
    {
        public ICommand ConfirmCommand { get { return new MainWindowCommand(Confirm); } }

        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<string> mAttrList = new ObservableCollection<string>();
        public ObservableCollection<string> AttrList
        {
            get { return mAttrList; }
            set { NotifyPropertyChanged("AttrList"); }
        }
        public string mAttrData { get; set; }
        public string AttrData
        {
            get { return mAttrData; }
            set {
                mAttrData = value;
                NotifyPropertyChanged("AttrData");
                ResetTable();
            }
        }
        public Dictionary<string, List<AttrModel>> AllData { get; set; }

        public ObservableCollection<AttrModel> mDataList = new ObservableCollection<AttrModel>();
        public ObservableCollection<AttrModel> DataList
        {
            get { return mDataList; }
            set { NotifyPropertyChanged("DataList"); }
        }

        public BTDetailDialogViewModel(Dictionary<string, List<AttrModel>> data)
        {
            AllData = data;
            foreach (string key in data.Keys)
            {
                AttrList.Add(key);
            }

            AttrData = AttrList[0];
        }

        private void ResetTable()
        {
            mDataList.Clear();

            foreach (var item in AllData[AttrData])
            {
                mDataList.Add(item);
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Confirm(object obj)
        {
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(true, null);
        }
    }
}
