﻿using GTBTCore;
using GTBTCore.Cipher;
using GTBTUtility.Model;
using GTBTUtility.ViewModel;
using MahApps.Metro.Controls;
using MaterialDesignThemes.Wpf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GTBTUtility
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public static string APPSETTINGFILE = System.AppDomain.CurrentDomain.BaseDirectory + "AppSetting.ap";
        public static string LOGPATH = System.AppDomain.CurrentDomain.BaseDirectory + @"Log\";
        private static MainWindow MAINWINDOW { get; set; }
        private static UtilityConfigModel UtilityConfig { get; set; }
        
        public static bool SetJSONFile(string file, object obj)
        {
            try
            {
                string fileName = System.IO.Path.GetFileNameWithoutExtension(file);
                //string txt = MainWindow.GetJSONString(obj);

                string txt = Utility.ConvertObjToString(obj);
                CipherAESNeil mEncryptionAlog = new CipherAESNeil();
                mEncryptionAlog.SetEncryptionKey(fileName);
                txt = Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.UTF8.GetBytes(txt)));

                using (StreamWriter outfile = new StreamWriter(file))
                {
                    outfile.Write(txt);
                }

            }
            catch (Exception ex)
            {
                // Open File Error
            }
            return true;
        }

        public static bool SaveSysLogFile(string folder, string file, string txt)
        {
            try
            {
                if (!Directory.Exists(MainWindow.LOGPATH)) Directory.CreateDirectory(MainWindow.LOGPATH);

                string path = string.Format("{0}{1}{2}", MainWindow.LOGPATH, folder, (folder != "") ? "\\" : "");

                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                string fileStr = string.Format("{0}{1}", path, file);

                if (!File.Exists(fileStr))
                {
                    using (FileStream fs = File.Open(fileStr, FileMode.Create))
                    using (StreamWriter outfile = new StreamWriter(fs))
                    {
                        outfile.Write(string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\r\n", "DateTime".PadRight(10), "Spend Time(Sec)".PadRight(20), "Mode".PadRight(10), "Step".PadRight(30), "BatInfo".PadRight(40), "StepMsg"));
                    }
                }
                File.AppendAllText(fileStr, txt + Environment.NewLine);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static string SaveFile(string folder, string file, string txt)
        {
            try
            {
                if (!Directory.Exists(MainWindow.LOGPATH)) Directory.CreateDirectory(MainWindow.LOGPATH);

                string path = string.Format("{0}{1}{2}", MainWindow.LOGPATH, folder, (folder != "") ? "\\" : "");

                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                string fileStr = string.Format("{0}{1}", path, file);

                using (StreamWriter outfile = new StreamWriter(fileStr))
                {
                    outfile.Write(txt);
                }
                return fileStr;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool RemoveFile(string file)
        {
            try
            {
                if (File.Exists(file)) File.Delete(file);
            }
            catch (Exception ex)
            {
                return false ;
            }
            return true;
        }

        public static UtilityConfigModel GetUtilityConfig()
        {
            if (MainWindow.UtilityConfig != null) return MainWindow.UtilityConfig;
            string file = MainWindow.APPSETTINGFILE;
            
            string txt = MainWindow.GetFileContent(file);
            if (txt != null)
            {
                try
                {
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(file);
                    CipherAESNeil mEncryptionAlog = new CipherAESNeil();
                    mEncryptionAlog.SetEncryptionKey(fileName);
                    txt = Encoding.UTF8.GetString(mEncryptionAlog.DecryptionData(Convert.FromBase64String(txt)));

                    using (System.IO.TextReader reader = new System.IO.StringReader(txt))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        MainWindow.UtilityConfig = (UtilityConfigModel)serializer.Deserialize(reader, typeof(UtilityConfigModel));
                    }
                }
                catch (Exception ex)
                {
                    // Open File Error
                    MainWindow.UtilityConfig = new UtilityConfigModel();
                }
            }
            else
            {
                MainWindow.UtilityConfig = new UtilityConfigModel();
            }
            return MainWindow.UtilityConfig;
        }
        
        public static UtilitySettingModel GetUtilitySetting(string file)
        {
            UtilitySettingModel model = null;
            string txt = MainWindow.GetFileContent(file);
            if (txt != null)
            {
                try
                {
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(file);
                    CipherAESNeil mEncryptionAlog = new CipherAESNeil();
                    mEncryptionAlog.SetEncryptionKey(fileName);
                    txt = Encoding.UTF8.GetString(mEncryptionAlog.DecryptionData(Convert.FromBase64String(txt)));
                    
                    using (System.IO.TextReader reader = new System.IO.StringReader(txt))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        model = (UtilitySettingModel)serializer.Deserialize(reader, typeof(UtilitySettingModel));
                    }
                }
                catch (Exception ex)
                {
                    // Open File Error
                }
            }
            return model;
        }

        public static string GetFileContent(string fileStr)
        {
            if (File.Exists(fileStr))
            {
                try
                {
                    using (StreamReader reader = File.OpenText(fileStr))
                    {
                        return reader.ReadToEnd();
                    }
                }
                catch (Exception ex)
                {
                    // Open File Error
                }
            }
            else
            {
                // Not Found File
            }
            return null;
        }

        public static void ExitAPP()
        {
            if (MAINWINDOW != null)
            {
                MAINWINDOW.EnableExitAPP = true;
                MAINWINDOW.Close();
            }
        }

        private bool EnableExitAPP = false;

        public MainWindow()
        {
            MainWindow.GetUtilityConfig();
            MAINWINDOW = this;
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }
        
        /// <summary>
        /// 視窗準備關閉之事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (!EnableExitAPP) // 決定可否關閉視窗之條件
            {
                e.Cancel = true;
            }
        }
    }
}
