﻿using GTBTUtility.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GTBTUtility.ViewModel
{
    public class UtilityRunDialogViewModel : INotifyPropertyChanged
    {
        public ICommand ConfirmCommand { get { return new MainWindowCommand(Confirm); } }
        public ICommand CancelCommand { get { return new MainWindowCommand(Canecl); } }

        private string[] mPortList = System.IO.Ports.SerialPort.GetPortNames();
        private UtilityRunModel mUtilityRunModel { get; set; }
        private System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"^\d{10}$");
        private ObservableCollection<string> mCOMPortList = new ObservableCollection<string>();
        private ObservableCollection<int> mAppBaudRateList = new ObservableCollection<int>();
        private string mTargetCOMPort = "";
        private string mSourceCOMPort = "";
        private int mAppBaudRate = 115200;
        private string mSerialNumber = "";
        private bool mEnableSource = false;
        private bool mEnableSerialNumber = false;
        private bool mEnableAppBaudRate = false;
        private string mErrorMessage = "";

        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<string> COMPortList
        {
            get { return mCOMPortList; }
            set { mCOMPortList = value; NotifyPropertyChanged("COMPortList"); }
        }
        public ObservableCollection<int> AppBaudRateList
        {
            get { return mAppBaudRateList; }
            set { mAppBaudRateList = value; NotifyPropertyChanged("AppBaudRateList"); }
        }
        public string SourceCOMPort
        {
            get { return mSourceCOMPort; }
            set { mSourceCOMPort = value; NotifyPropertyChanged("SourceCOMPort"); }
        }
        public string TargetCOMPort
        {
            get { return mTargetCOMPort; }
            set { mTargetCOMPort = value; NotifyPropertyChanged("TargetCOMPort"); }
        }
        public int AppBaudRate
        {
            get { return mAppBaudRate; }
            set { mAppBaudRate = value; NotifyPropertyChanged("AppBaudRate"); }
        }
        public string SerialNumber
        {
            get { return mSerialNumber; }
            set { mSerialNumber = value; NotifyPropertyChanged("SerialNumber"); }
        }
        public bool EnableSource
        {
            get { return mEnableSource; }
            set { mEnableSource = value; NotifyPropertyChanged("EnableSourceVisibility"); }
        }
        public bool EnableSerialNumber
        {
            get { return mEnableSerialNumber; }
            set { mEnableSerialNumber = value; NotifyPropertyChanged("EnableSerialNumberVisibility"); }
        }
        public bool EnableAppBaudRate
        {
            get { return mEnableAppBaudRate; }
            set { mEnableAppBaudRate = value; NotifyPropertyChanged("EnableAppBaudRateVisibility"); }
        }
        public System.Windows.Visibility EnableSourceVisibility
        {
            get { return (EnableSource) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableSourceVisibility"); }
        }
        public System.Windows.Visibility EnableSerialNumberVisibility
        {
            get { return (EnableSerialNumber) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableSerialNumberVisibility"); }
        }
        public System.Windows.Visibility EnableAppBaudRateVisibility
        {
            get { return (mEnableAppBaudRate) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableAppBaudRateVisibility"); }
        }
        public string ErrorMessage
        {
            get { return mErrorMessage; }
            set { mErrorMessage = value; NotifyPropertyChanged("ErrorMessage"); }
        }

        public UtilityRunDialogViewModel(UtilityRunModel model)
        {
            mUtilityRunModel = model;
            EnableSource = mUtilityRunModel.EnableSource;
            EnableSerialNumber = mUtilityRunModel.EnableSerialNumber;
            EnableAppBaudRate = model.EnableAppBaudRate;
            AppBaudRate = mUtilityRunModel.AppBaudRate;
            SetCOMPortList();

            mAppBaudRateList.Add(115200);
            mAppBaudRateList.Add(38400);
            AppBaudRateList = mAppBaudRateList;
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void NotifyPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged != null) foreach (string propertyName in propertyNames) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SetCOMPortList()
        {
            mCOMPortList.Clear();

            string matchSourceStr = "";
            string matchTargetStr = "";
            foreach (string str in mPortList)
            {
                mCOMPortList.Add(str);
                SourceCOMPort = str;
                TargetCOMPort = str;
                if (mUtilityRunModel.Source == str) matchSourceStr = str;
                if (mUtilityRunModel.Target == str) matchTargetStr = str;
            }

            if (matchSourceStr != "") SourceCOMPort = matchSourceStr;
            if (matchTargetStr != "") TargetCOMPort = matchTargetStr;
            COMPortList = mCOMPortList;
        }

        private void Confirm(object obj)
        {
            if (EnableSerialNumber && !reg.IsMatch((SerialNumber ?? "").ToString()))
            {
                return;
            }
            if (TargetCOMPort == "")
            {
                ErrorMessage = "Target cannot empty.";
                return;
            }
            if (EnableSource)
            {
                if (SourceCOMPort == "")
                {
                    ErrorMessage = "Source cannot empty.";
                    return;
                }
                if (SourceCOMPort == TargetCOMPort)
                {
                    ErrorMessage = "Source and Target cannot be equal.";
                    return;
                }
            }
            mUtilityRunModel.Source = SourceCOMPort;
            mUtilityRunModel.Target = TargetCOMPort;
            mUtilityRunModel.SerialNumber = SerialNumber;
            mUtilityRunModel.AppBaudRate = AppBaudRate;
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(mUtilityRunModel, null);
        }

        private void Canecl(object obj)
        {
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(null, null);
        }
    }
}
