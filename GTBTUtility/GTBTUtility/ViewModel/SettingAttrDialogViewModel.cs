﻿using GTBTCore;
using GTBTUtility.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GTBTUtility.ViewModel
{
    public class SettingAttrDialogViewModel : INotifyPropertyChanged
    {
        public ICommand SubmitCommand { get { return new MainWindowCommand(Submit); } }
        public ICommand CancelCommand { get { return new MainWindowCommand(Cancel); } }

        private bool mIsCheckingItem { get; set; }
        private string mErrorMsg = "";
        private CmdConfig mCmdConfig = new CmdConfig();
        private List<CmdConfig> mCmdConfigList { get; set; }
        private ObservableCollection<string> mAttrTypeItemList = new ObservableCollection<string>();
        private ObservableCollection<string> mNameItemList = new ObservableCollection<string>();
        private ObservableCollection<string> mOperatorItemList = new ObservableCollection<string>();

        public string AttrTypeItem
        {
            get { return mCmdConfig.CmdType.ToString(); }
            set
            {
                mCmdConfig.CmdType = (CmdNameType)Enum.Parse(typeof(CmdNameType), value);
                NotifyPropertyChanged("AttrTypeItem");
                ResetNameItemList();
            }
        }

        public string NameItem
        {
            get { return mCmdConfig.Name; }
            set
            {
                mCmdConfig.Name = value;
                NotifyPropertyChanged("NameItem");
            }
        }

        public string ValueText
        {
            get { return mCmdConfig.Value; }
            set
            {
                mCmdConfig.Value = value;
                NotifyPropertyChanged("ValueText");
            }
        }

        public string OperatorItem
        {
            get { return mCmdConfig.CmdOperator.ToString(); }
            set
            {
                mCmdConfig.CmdOperator = (CmdOperatorType)Enum.Parse(typeof(CmdOperatorType), value);
                NotifyPropertyChanged("OperatorItem");
            }
        }

        public string ErrorMsg
        {
            get { return mErrorMsg; }
            set
            {
                mErrorMsg = value;
                NotifyPropertyChanged("ErrorMsg");
            }
        }

        public ObservableCollection<string> AttrTypeItemList
        {
            get { return mAttrTypeItemList; }
            set { NotifyPropertyChanged("AttrTypeItemList"); }
        }
        public ObservableCollection<string> NameItemList
        {
            get { return mNameItemList; }
            set { NotifyPropertyChanged("NameItemList"); }
        }
        public ObservableCollection<string> OperatorItemList
        {
            get { return mOperatorItemList; }
            set { NotifyPropertyChanged("OperatorItemList"); }
        }

        public bool IsCheckingItem {
            get { return mIsCheckingItem; }
            set
            {
                mIsCheckingItem = value;
                NotifyPropertyChanged("EnableOptsVisibility");
            }
        }
        
        public System.Windows.Visibility EnableOptsVisibility
        {
            get { return (IsCheckingItem) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableOptsVisibility"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public SettingAttrDialogViewModel(bool isCheckingItem)
        {
            IsCheckingItem = isCheckingItem;
            InitializeSetting();
        }

        public SettingAttrDialogViewModel(bool isCheckingItem, CmdConfig config) : this(isCheckingItem)
        {
            AttrTypeItem = config.CmdType.ToString();
            OperatorItem = config.CmdOperator.ToString();

            string tmpStr = config.Value;
            if (config.Type == "System.String") tmpStr = (String)Utility.ConvertStringToObj(config.Type, tmpStr);
            ValueText = tmpStr;
 
            NameItem = config.Name;
        }

        private void InitializeSetting()
        {
            string[] attrTypeList = Enum.GetNames(typeof(CmdNameType)).ToArray();
            foreach (string item in attrTypeList)
            {
                if (IsCheckingItem || item != "BAT") mAttrTypeItemList.Add(item);
            }

            string[] optsList = Enum.GetNames(typeof(CmdOperatorType)).ToArray();
            foreach (string item in optsList)
            {
                mOperatorItemList.Add(item);
            }

            AttrTypeItemList = mAttrTypeItemList;
            OperatorItemList = mOperatorItemList;
            AttrTypeItem = mCmdConfig.CmdType.ToString();
            OperatorItem = mCmdConfig.CmdOperator.ToString();
            ValueText = mCmdConfig.Value;
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ResetNameItemList()
        {
            mNameItemList.Clear();
            NameItem = "";

            switch (AttrTypeItem)
            {
                case "BAT":
                    {
                        mCmdConfigList = BatCore.BATPARAMLIST;
                        break;
                    }
                case "MCU_EEP":
                    {
                        mCmdConfigList = BatCore.GetCBEEPCmdConfigList();
                        break;
                    }
                case "OZ1_EEP":
                case "OZ2_EEP":
                    {
                        mCmdConfigList = BatCore.GetPBEEPCmdConfigList();
                        break;
                    }
                default:
                    {
                        mCmdConfigList = null;
                        break;
                    }
            }
            
            if (mCmdConfigList != null)
            {
                string tmpName = "";
                bool isFirst = true;
                foreach (var item in mCmdConfigList)
                {
                    if (!item.IsHidden)
                    {
                        if (IsCheckingItem || !item.IsReadOnly)
                        {
                            mNameItemList.Add(item.Name);
                            if (isFirst)
                            {
                                tmpName = item.Name;
                                isFirst = false;
                            }
                            if (item.Name == mCmdConfig.Name)
                            {
                                tmpName = mCmdConfig.Name;
                            }
                        }
                    }
                }
                NameItemList = mNameItemList;
                NameItem = tmpName;
            }
        }


        private void Submit(object obj)
        {
            ErrorMsg = "";
            CmdConfig tmpConfig = mCmdConfigList.FirstOrDefault(item => item.Name == NameItem);

            if (tmpConfig != null)
            {
                mCmdConfig.IsReadOnly = tmpConfig.IsReadOnly;
                mCmdConfig.Address = tmpConfig.Address;
                mCmdConfig.Length = tmpConfig.Length;
                mCmdConfig.Type = tmpConfig.Type;

                string tmpStr = mCmdConfig.Value;
                if (mCmdConfig.Type == "System.String") tmpStr = Utility.ConvertObjToString(tmpStr);
                object result = Utility.ConvertStringToObj(mCmdConfig.Type, tmpStr);
                if (result != null || (IsCheckingItem && mCmdConfig.CmdOperator == CmdOperatorType.NonOperator))
                {
                    mCmdConfig.Value = tmpStr;
                    MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(mCmdConfig, null);
                }
                else
                {
                    ErrorMsg = "Value(Format Error)";
                }
            }
            else
            {
                ErrorMsg = "Attribute not found";
            }
        }

        private void Cancel(object obj)
        {
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(null, null);
        }
    }
}
