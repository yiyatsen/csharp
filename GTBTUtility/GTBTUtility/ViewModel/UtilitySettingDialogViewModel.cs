﻿using GTBTCore;
using GTBTUtility.Model;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GTBTUtility.ViewModel
{
    public class UtilitySettingDialogViewModel : INotifyPropertyChanged
    {
        private ICommand DataGridRemoveCommand { get; set; }
        private ICommand DataGridEditCommand { get; set; }
        public ICommand DownlodSettingFileCommand { get { return new MainWindowCommand(DownloadSettingFile); } }
        public ICommand UploadSettingFileCommand { get { return new MainWindowCommand(UploadSettingFile); } }
        public ICommand UploadFWUtilityCommand { get { return new MainWindowCommand(UploadFWUtility); } }
        public ICommand UploadFWAppCommand { get { return new MainWindowCommand(UploadFWApp); } }
        public ICommand AddAttrCommand { get { return new MainWindowCommand(AddAttr); } }
        public ICommand RemoveCommand
        {
            get
            {
                if (DataGridRemoveCommand == null) DataGridRemoveCommand = new DataGridDelegateCommand(RemoveAttr);
                return DataGridRemoveCommand;
            }
        }
        public ICommand EditCommand
        {
            get {
                if (DataGridEditCommand == null) DataGridEditCommand = new DataGridDelegateCommand(EditAttr);
                return DataGridEditCommand;
            }
        }
        public ICommand SaveAsCommand { get { return new MainWindowCommand(Submit); } }
        public ICommand SubmitCommand { get { return new MainWindowCommand(Submit); } }
        public ICommand CancelCommand { get { return new MainWindowCommand(Cancel); } }

        private UtilityConfigModel mUtilityConfigModel = MainWindow.GetUtilityConfig();
        private UtilitySettingModel mUtilitySettingModel = new UtilitySettingModel();
        private ObservableCollection<string> mModeList = new ObservableCollection<string>();
        private ObservableCollection<string> mStepList = new ObservableCollection<string>();
        private List<CmdConfig> mCurrentParamList { get; set; }
        private ObservableCollection<CmdConfig> mParamList = new ObservableCollection<CmdConfig>();
        private string mStepItem = "";
        private string mConfigModeItem = "";
        private int mStepVisibilityIndex { get; set; }
        private bool mEnableSettingFile { get; set; }
        private string mSettingFile { get; set; }
        private string mSettingModeItem = "";
        private string mParamStr = "";
        
        public event PropertyChangedEventHandler PropertyChanged;
        public bool EnableSetting
        {
            get { return Utility.EnableEngMode; }
        }
        public System.Windows.Visibility EnableSettingVisibility
        {
            get { return (EnableSetting) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableSettingVisibility"); }
        }

        public ObservableCollection<CmdConfig> ParamList
        {
            get { return mParamList; }
            set { NotifyPropertyChanged("ParamList"); }
        }
        public ObservableCollection<string> ModeList
        {
            get { return mModeList; }
            set { NotifyPropertyChanged("ModeList"); }
        }
        public ObservableCollection<string> StepList
        {
            get { return mStepList; }
            set { NotifyPropertyChanged("StepList"); }
        }
        public string StepItem
        {
            get { return mStepItem; }
            set {
                mStepItem = value;
                NotifyPropertyChanged("StepItem");
                SwitchStepItem();
            }
        }
        public string ConfigModeItem
        {
            get { return mConfigModeItem; }
            set { mConfigModeItem = value; NotifyPropertyChanged("ConfigModeItem"); }
        }
        public string SettingModeItem
        {
            get { return mSettingModeItem; }
            set {
                mSettingModeItem = value;
                ResetSettingMode();
                NotifyPropertyChanged("SettingModeItem");
            }
        }
        public bool EnableSettingFile
        {
            get { return mEnableSettingFile; }
            set
            {
                mEnableSettingFile = value;
                NotifyPropertyChanged("EnableSettingFile");
                NotifyPropertyChanged("EnableSettingFileVisibility");
            }
        }
        public string SettingFile
        {
            get { return mSettingFile; }
            set
            {
                mSettingFile = value;
                NotifyPropertyChanged("SettingFile");
            }
        }
        public System.Windows.Visibility EnableSettingFileVisibility
        {
            get { return (EnableSettingFile) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableSettingFileVisibility"); }
        }
        public int StepVisibilityIndex
        {
            get { return mStepVisibilityIndex; }
            set
            {
                mStepVisibilityIndex = value;
                NotifyPropertyChanged("EnableTableVisibility");
                NotifyPropertyChanged("EnableUpdateListVisibility");
                NotifyPropertyChanged("EnableSettingTarget1Visibility");
                NotifyPropertyChanged("EnableSettingTarget2Visibility");
                NotifyPropertyChanged("EnableCheckingTarget1Visibility");
            }
        }
        public System.Windows.Visibility EnableTableVisibility
        {
            get { return (StepVisibilityIndex == 1 || StepVisibilityIndex == 4) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableTableVisibility"); }
        }
        public System.Windows.Visibility EnableUpdateListVisibility
        {
            get { return (StepVisibilityIndex == 2) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableUpdateListVisibility"); }
        }
        public System.Windows.Visibility EnableSettingTarget1Visibility
        {
            get { return (StepVisibilityIndex == 3) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableSettingTarget1Visibility"); }
        }
        public System.Windows.Visibility EnableSettingTarget2Visibility
        {
            get { return (StepVisibilityIndex == 4) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableSettingTarget2Visibility"); }
        }
        public System.Windows.Visibility EnableCheckingTarget1Visibility
        {
            get { return (StepVisibilityIndex == 5) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableCheckingTarget1Visibility"); }
        }
        public bool EnableUpdateFirmware
        {
            get { return mUtilitySettingModel.EnableUpdateFirmware; }
            set
            {
                mUtilitySettingModel.EnableUpdateFirmware = value;
                if (!mUtilitySettingModel.EnableUpdateFirmware)
                {
                    FWUtilityName = "";
                    mUtilitySettingModel.FWUtilityContent = "";
                     FWAppName = "";
                    mUtilitySettingModel.FWAppContent = "";
                }
                NotifyPropertyChanged("EnableUpdateFirmware");
                NotifyPropertyChanged("EnableUpdateFirmwareVisibility");
            }
        }
        public string FWUtilityName
        {
            get { return mUtilitySettingModel.FWUtilityName; }
            set
            {
                mUtilitySettingModel.FWUtilityName = value;
                NotifyPropertyChanged("FWUtilityName");
            }
        }
        public string FWAppName
        {
            get { return mUtilitySettingModel.FWAppName; }
            set
            {
                mUtilitySettingModel.FWAppName = value;
                NotifyPropertyChanged("FWAppName");
            }
        }
        public System.Windows.Visibility EnableUpdateFirmwareVisibility
        {
            get { return (EnableUpdateFirmware && SettingModeItem == UtilityMode.Update.ToString()) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableUpdateFirmwareVisibility"); }
        }
        public System.Windows.Visibility EnableQuicklySaveVisibility
        {
            get { return (SettingModeItem == UtilityMode.Save.ToString()) ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible; }
            set { NotifyPropertyChanged("EnableQuicklySaveVisibility"); }
        }
        public bool EnableConfigSerialNumber
        {
            get { return mUtilitySettingModel.EnableConfigSerialNumber; }
            set
            {
                mUtilitySettingModel.EnableConfigSerialNumber = value;
                NotifyPropertyChanged("EnableConfigSerialNumber");
            }
        }
        public bool EnableConfigRTC
        {
            get { return mUtilitySettingModel.EnableConfigRTC; }
            set
            {
                mUtilitySettingModel.EnableConfigRTC = value;
                NotifyPropertyChanged("EnableConfigRTC");
            }
        }
        public bool EnableCheckRTC
        {
            get { return mUtilitySettingModel.EnableCheckRTC; }
            set
            {
                mUtilitySettingModel.EnableCheckRTC = value;
                NotifyPropertyChanged("EnableCheckRTC");
            }
        }
        public bool EnableCheckOV
        {
            get { return ((mUtilitySettingModel.ProtectionValue & 0x01) > 0); }
            set
            {
                mUtilitySettingModel.ProtectionValue = value ? (mUtilitySettingModel.ProtectionValue |= 0x01) : (mUtilitySettingModel.ProtectionValue ^= 0x01);
                NotifyPropertyChanged("EnableCheckOV");
            }
        }
        public bool EnableCheckUV
        {
            get { return (((mUtilitySettingModel.ProtectionValue >> 1) & 0x01) > 0); }
            set
            {
                mUtilitySettingModel.ProtectionValue = value ? (mUtilitySettingModel.ProtectionValue |= 0x02) : (mUtilitySettingModel.ProtectionValue ^= 0x02);
                NotifyPropertyChanged("EnableCheckUV");
            }
        }
        public bool EnableCheckCOC
        {
            get { return (((mUtilitySettingModel.ProtectionValue >> 2) & 0x01) > 0); }
            set
            {
                mUtilitySettingModel.ProtectionValue = value ? (mUtilitySettingModel.ProtectionValue |= 0x04) : (mUtilitySettingModel.ProtectionValue ^= 0x04);
                NotifyPropertyChanged("EnableCheckCOC");
            }
        }
        public bool EnableCheckDOC
        {
            get { return (((mUtilitySettingModel.ProtectionValue >> 3) & 0x01) > 0); }
            set
            {
                mUtilitySettingModel.ProtectionValue = value ? (mUtilitySettingModel.ProtectionValue |= 0x08) : (mUtilitySettingModel.ProtectionValue ^= 0x08);
                NotifyPropertyChanged("EnableCheckDOC");
            }
        }
        public bool EnableCheckOT
        {
            get { return (((mUtilitySettingModel.ProtectionValue >> 4) & 0x01) > 0); }
            set
            {
                mUtilitySettingModel.ProtectionValue = value ? (mUtilitySettingModel.ProtectionValue |= 0x10) : (mUtilitySettingModel.ProtectionValue ^= 0x10);
                NotifyPropertyChanged("EnableCheckOT");
            }
        }
        public bool EnableCheckUT
        {
            get { return (((mUtilitySettingModel.ProtectionValue >> 5) & 0x01) > 0); }
            set
            {
                mUtilitySettingModel.ProtectionValue = value ? (mUtilitySettingModel.ProtectionValue |= 0x20) : (mUtilitySettingModel.ProtectionValue ^= 0x20);
                NotifyPropertyChanged("EnableCheckUT");
            }
        }
        public bool EnableCheckSC
        {
            get { return (((mUtilitySettingModel.ProtectionValue >> 6) & 0x01) > 0); }
            set
            {
                mUtilitySettingModel.ProtectionValue = value ? (mUtilitySettingModel.ProtectionValue |= 0x40) : (mUtilitySettingModel.ProtectionValue ^= 0x40);
                NotifyPropertyChanged("EnableCheckSC");
            }
        }
        public string LogFolder
        {
            get { return mUtilitySettingModel.LogFolder; }
            set
            {
                mUtilitySettingModel.LogFolder = value;
                NotifyPropertyChanged("LogFolder");
            }
        }
        public string ParamStr
        {
            get { return mParamStr; }
            set
            {
                mParamStr = value;
                NotifyPropertyChanged("ParamStr");
            }
        }

        public System.Windows.Visibility EnableAddAttrVisibility
        {
            get { return (StepVisibilityIndex == 1 || StepVisibilityIndex == 4) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableAddAttrVisibility"); }
        }

        public UtilitySettingDialogViewModel()
        {
            EnableSettingFile = mUtilityConfigModel.EnableSettingFile;
            SettingFile = mUtilityConfigModel.SettingFile;
            
            SetModeAndStyle();

            EnableUpdateFirmware = mUtilityConfigModel.DefaultUtilitySetting.EnableUpdateFirmware;
            FWAppName = mUtilityConfigModel.DefaultUtilitySetting.FWAppName;
            mUtilitySettingModel.FWAppContent = mUtilityConfigModel.DefaultUtilitySetting.FWAppContent;
            FWUtilityName = mUtilityConfigModel.DefaultUtilitySetting.FWUtilityName;
            mUtilitySettingModel.FWUtilityContent = mUtilityConfigModel.DefaultUtilitySetting.FWUtilityContent;

            EnableConfigSerialNumber = mUtilityConfigModel.DefaultUtilitySetting.EnableConfigSerialNumber;
            EnableConfigRTC = mUtilityConfigModel.DefaultUtilitySetting.EnableConfigRTC;

            EnableCheckRTC = mUtilityConfigModel.DefaultUtilitySetting.EnableCheckRTC;
            mUtilitySettingModel.ProtectionValue = mUtilityConfigModel.DefaultUtilitySetting.ProtectionValue;
            
            mUtilitySettingModel.ConfigParamList = mUtilityConfigModel.DefaultUtilitySetting.ConfigParamList;
            mUtilitySettingModel.CheckSourceParamList = mUtilityConfigModel.DefaultUtilitySetting.CheckSourceParamList;
            mUtilitySettingModel.CheckTargetBeforeParamList = mUtilityConfigModel.DefaultUtilitySetting.CheckTargetBeforeParamList;
            mUtilitySettingModel.CheckTargetAfterParamList = mUtilityConfigModel.DefaultUtilitySetting.CheckTargetAfterParamList;

            LogFolder = mUtilityConfigModel.DefaultUtilitySetting.LogFolder;
            SwitchStepItem();
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void NotifyPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged != null)
            {
                foreach (string propertyName in propertyNames) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void SetModeAndStyle()
        {
            mModeList.Clear();
            
            foreach (var item in Enum.GetValues(typeof(UtilityMode)).Cast<UtilityMode>())
            {
                string str = item.ToString();
                mModeList.Add(str);
                if (mUtilityConfigModel.Mode == item)
                {
                    ConfigModeItem = str;
                }
                if (mUtilityConfigModel.DefaultUtilitySetting.Mode == item)
                {
                    SettingModeItem = str;
                }
            }
            ModeList = mModeList;
        }

        private void SwitchStepItem()
        {
            mParamList.Clear();
            ParamStr = StepItem;
            switch (StepItem)
            {
                case "Check Target(Before)":
                    {
                        mCurrentParamList = mUtilitySettingModel.CheckTargetBeforeParamList;
                        StepVisibilityIndex = 1;
                        break;
                    }
                case "Check Source":
                    {
                        mCurrentParamList = mUtilitySettingModel.CheckSourceParamList;
                        StepVisibilityIndex = 1;
                        break;
                    }
                case "Update Firmware":
                    {
                        StepVisibilityIndex = 2;
                        break;
                    }
                case "Setup Target Param 1":
                    {
                        StepVisibilityIndex = 3;
                        break;
                    }
                case "Setup Target Param 2":
                    {
                        mCurrentParamList = mUtilitySettingModel.ConfigParamList;
                        StepVisibilityIndex = 4;
                        break;
                    }
                case "Check Target(After) 1":
                    {
                        StepVisibilityIndex = 5;
                        break;
                    }
                case "Check Target(After) 2":
                    {
                        mCurrentParamList = mUtilitySettingModel.CheckTargetAfterParamList;
                        StepVisibilityIndex = 1;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            if (StepVisibilityIndex == 1 || StepVisibilityIndex == 4)
            {
                foreach (var item in mCurrentParamList)
                {
                    mParamList.Add(item);
                }
                ParamList = mParamList;
            }

            EnableAddAttrVisibility = System.Windows.Visibility.Collapsed;
        }

        private void ResetSettingMode()
        {
            mStepList.Clear();

            mUtilitySettingModel.Mode = (UtilityMode)System.Enum.Parse(typeof(UtilityMode), mSettingModeItem);

            FWUtilityName = "";
            mUtilitySettingModel.FWUtilityContent = "";
            FWAppName = "";
            mUtilitySettingModel.FWAppContent = "";

            if (mUtilitySettingModel.Mode == UtilityMode.Save)
            {
                EnableConfigSerialNumber = false;
                EnableConfigRTC = false;
                EnableCheckRTC = false;
                EnableUpdateFirmware = false;

                mUtilitySettingModel.ProtectionValue = 0;
                
                mUtilitySettingModel.ConfigParamList.Clear();
                mUtilitySettingModel.CheckSourceParamList.Clear();
                mUtilitySettingModel.CheckTargetBeforeParamList.Clear();
                mUtilitySettingModel.CheckTargetAfterParamList.Clear();
            } else
            {
                EnableConfigSerialNumber = mUtilitySettingModel.EnableConfigSerialNumber;
                EnableConfigRTC = mUtilitySettingModel.EnableConfigRTC;
                EnableCheckRTC = mUtilitySettingModel.EnableCheckRTC;

                mStepList.Add("Check Target(Before)");

                if (mUtilitySettingModel.Mode != UtilityMode.Update)
                {
                    EnableUpdateFirmware = true;

                    mStepList.Add("Check Source");
                }
                else
                {
                    EnableUpdateFirmware = false;
                    mUtilitySettingModel.CheckSourceParamList.Clear();

                    mStepList.Add("Update Firmware");
                }

                mStepList.Add("Setup Target Param 1");
                mStepList.Add("Setup Target Param 2");
                mStepList.Add("Check Target(After) 1");
                mStepList.Add("Check Target(After) 2");

                StepItem = "Check Target(Before)";
            }
            NotifyPropertyChanged("EnableQuicklySaveVisibility");
        }

        private void DownloadSettingFile(object obj)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".gtConfig";
            dlg.Filter = "GTE Files (*.gtConfig)|*.gtConfig";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                bool isSuccess = MainWindow.SetJSONFile(dlg.FileName, mUtilitySettingModel);
                if (isSuccess) SettingFile = System.IO.Path.GetFileNameWithoutExtension(dlg.FileName);
            }
        }

        private void UploadSettingFile(object obj)
        {
            if (EnableSettingFile)
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

                // Set filter for file extension and default file extension 
                dlg.DefaultExt = ".gtConfig";
                dlg.Filter = "GTE Files (*.gtConfig)|*.gtConfig";

                // Display OpenFileDialog by calling ShowDialog method 
                Nullable<bool> result = dlg.ShowDialog();

                // Get the selected file name and display in a TextBox 
                if (result == true)
                {
                    UtilitySettingModel model = MainWindow.GetUtilitySetting(dlg.FileName);
                    if (model != null)
                    {
                        SettingFile = System.IO.Path.GetFileNameWithoutExtension(dlg.FileName);

                        SettingModeItem = model.Mode.ToString();

                        EnableUpdateFirmware = model.EnableUpdateFirmware;
                        FWAppName = model.FWAppName;
                        mUtilitySettingModel.FWAppContent = model.FWAppContent;
                        FWUtilityName = model.FWUtilityName;
                        mUtilitySettingModel.FWUtilityContent = model.FWUtilityContent;

                        EnableConfigSerialNumber = model.EnableConfigSerialNumber;
                        EnableConfigRTC = model.EnableConfigRTC;

                        EnableCheckRTC = model.EnableCheckRTC;
                        mUtilitySettingModel.ProtectionValue = model.ProtectionValue;

                        mUtilitySettingModel.ConfigParamList = model.ConfigParamList;
                        mUtilitySettingModel.CheckSourceParamList = model.CheckSourceParamList;
                        mUtilitySettingModel.CheckTargetBeforeParamList = model.CheckTargetBeforeParamList;
                        mUtilitySettingModel.CheckTargetAfterParamList = model.CheckTargetAfterParamList;

                        LogFolder = model.LogFolder;
                        SwitchStepItem();
                    }
                }
            }
        }

        private void UploadFWUtility(object obj)
        {
            if (SettingModeItem == UtilityMode.Update.ToString())
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

                // Set filter for file extension and default file extension 
                dlg.DefaultExt = ".hex";
                dlg.Filter = "HEX Files (*.hex)|*.hex";

                // Display OpenFileDialog by calling ShowDialog method 
                Nullable<bool> result = dlg.ShowDialog();

                // Get the selected file name and display in a TextBox 
                if (result == true)
                {
                    string txt = MainWindow.GetFileContent(dlg.FileName);
                    if (txt != null)
                    {
                        FWUtilityName = System.IO.Path.GetFileNameWithoutExtension(dlg.FileName);
                        mUtilitySettingModel.FWUtilityContent = txt;
                    }
                }
            }
        }

        private void UploadFWApp(object obj)
        {
            if (SettingModeItem == UtilityMode.Update.ToString())
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

                // Set filter for file extension and default file extension 
                dlg.DefaultExt = ".hex";
                dlg.Filter = "HEX Files (*.hex)|*.hex";

                // Display OpenFileDialog by calling ShowDialog method 
                Nullable<bool> result = dlg.ShowDialog();

                // Get the selected file name and display in a TextBox 
                if (result == true)
                {
                    string txt = MainWindow.GetFileContent(dlg.FileName);
                    if (txt != null)
                    {
                        FWAppName = System.IO.Path.GetFileNameWithoutExtension(dlg.FileName);
                        mUtilitySettingModel.FWAppContent = txt;
                    }
                }
            }
        }

        private async void AddAttr(object obj)
        {
            if (mCurrentParamList != null)
            {
                await DialogHost.Show(new SettingAttrDialog() { DataContext = new SettingAttrDialogViewModel(StepVisibilityIndex == 1) }, "InnerDialog", (object sender2, DialogClosingEventArgs eventArgs) =>
                {
                    if (eventArgs.Parameter == null) return;

                    mCurrentParamList.Add((CmdConfig)eventArgs.Parameter);

                    mParamList.Clear();
                    foreach (var item in mCurrentParamList) mParamList.Add(item);

                    ParamList = mParamList;
                });
            }
        }

        private async void EditAttr(object obj)
        {
            CmdConfig item = obj as CmdConfig;
            await DialogHost.Show(new SettingAttrDialog() { DataContext = new SettingAttrDialogViewModel(StepVisibilityIndex == 1, item) }, "InnerDialog", (object sender2, DialogClosingEventArgs eventArgs) =>
            {
                if (eventArgs.Parameter == null) return;
                item.CmdType = ((CmdConfig)eventArgs.Parameter).CmdType;
                item.Name = ((CmdConfig)eventArgs.Parameter).Name;
                item.Value = ((CmdConfig)eventArgs.Parameter).Value;
                item.CmdOperator = ((CmdConfig)eventArgs.Parameter).CmdOperator;

                mParamList.Clear();
                foreach (var i in mCurrentParamList) mParamList.Add(i);

                ParamList = mParamList;
            });
        }

        private void RemoveAttr(object obj)
        {
            int index = ParamList.IndexOf(obj as CmdConfig);
            if (index > -1 && index < ParamList.Count)
            {
                ParamList.RemoveAt(index);
                mCurrentParamList.RemoveAt(index);
            }
        }
        
        private void Submit(object obj)
        {
            mUtilityConfigModel.Mode = (UtilityMode)System.Enum.Parse(typeof(UtilityMode), ConfigModeItem);
            mUtilityConfigModel.EnableSettingFile = EnableSettingFile;
            mUtilityConfigModel.SettingFile = SettingFile;
            mUtilityConfigModel.DefaultUtilitySetting = mUtilitySettingModel;
            // save setting
            MainWindow.SetJSONFile(MainWindow.APPSETTINGFILE, mUtilityConfigModel);
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(true, null);
        }

        private void Cancel(object obj)
        {
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(false, null);
        }
    }
}
