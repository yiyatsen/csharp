﻿using GTBTCore;
using GTBTUtility.Model;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GTBTUtility.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private static bool ENABLEENGMODE = true;

        public ICommand EngLoginCommand { get { return new MainWindowCommand(EngLogin); } }
        public ICommand ExcuteUtilityDialogCommand { get { return new MainWindowCommand(ExcuteUtilityDialog); } }
        public ICommand StopUtilityDialogCommand { get { return new MainWindowCommand(StopUtilityDialog); } }
        public ICommand ExcuteUtilitySettingDialogCommand { get { return new MainWindowCommand(ExcuteUtilitySettingDialog); } }
        public ICommand ExitAppCommand { get { return new MainWindowCommand(ExitApp); } }

        private UtilityConfigModel mUtilityConfigModel = MainWindow.GetUtilityConfig();
        private Slot mTargetSlot = new Slot() { slotId = 1 };
        private Slot mSourceSlot = new Slot() { slotId = 2 };
        private bool mIsUtilityRun = false;
        private string mStepTitle = "Ready";
        private string mStepMsg = "";
        private bool mIsStepNG = false;
        private bool EnableStopClick = true;
        private string mCOMPortMessage = "";
        private CancellationTokenSource mCTS { get; set; }
        private UtilityRunModel mUtilityRunModel = new UtilityRunModel();
        private ObservableCollection<CmdConfigResult> mResultList = new ObservableCollection<CmdConfigResult>();

        public event PropertyChangedEventHandler PropertyChanged;

        private string mAppVersion { get; set; }
        private string mCoreVersion { get; set; }

        public string AppVersion
        {
            get { return mAppVersion; }
            set
            {
                mAppVersion = value;
                NotifyPropertyChanged("AppVersion");
            }
        }

        public string CoreVersion
        {
            get { return mCoreVersion; }
            set
            {
                mCoreVersion = value;
                NotifyPropertyChanged("CoreVersion");
            }
        }
        public ObservableCollection<CmdConfigResult> ResultList
        {
            get { return mResultList; }
            set
            {
                NotifyPropertyChanged("ResultList");
            }
        }
        public bool IsUtilityRun
        {
            get { return mIsUtilityRun; }
            set {
                mIsUtilityRun = value;
                NotifyPropertyChanged("ExcuteUtilityVisibility");
                NotifyPropertyChanged("StopUtilityVisibility");
                NotifyPropertyChanged("EnableSettingVisibility");
            }
        }
        public string COMPortMessage
        {
            get { return mCOMPortMessage; }
            set {
                mCOMPortMessage = value;
                NotifyPropertyChanged("COMPortMessage");
            }
        }
        public string UtilityModeStyle {
            get { return string.Format("Mode: {0}", Utility.EnableEngMode ? "ENG" : mUtilityConfigModel.Mode.ToString()); }
            set { NotifyPropertyChanged("UtilityModeStyle"); }
        }
        public System.Windows.Visibility ExcuteUtilityVisibility
        {
            get { return (mIsUtilityRun) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("ExcuteUtilityVisibility"); }
        }
        public System.Windows.Visibility StopUtilityVisibility
        {
            get { return (!mIsUtilityRun) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("StopUtilityVisibility"); }
        }
        public System.Windows.Visibility EnableSettingVisibility
        {
            get { return (!mIsUtilityRun && (Utility.EnableEngMode || mUtilityConfigModel.EnableSettingFile || mUtilityConfigModel.Mode != UtilityMode.Save)) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableSettingVisibility"); }
        }
        public System.Windows.Visibility EnableCheckingTableVisibility
        {
            get { return (Utility.EnableEngMode || mUtilityConfigModel.Mode != UtilityMode.Transfer) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableCheckingTableVisibility"); }
        }
        
        public string StepTitle
        {
            get { return mStepTitle; }
            set {
                mStepTitle = value;
                NotifyPropertyChanged("StepTitle");
            }
        }
        public string StepMsg
        {
            get { return mStepMsg; }
            set
            {
                mStepMsg = value;
                NotifyPropertyChanged("StepMsg");
            }
        }
        public bool IsStepNG
        {
            get { return mIsStepNG; }
            set
            {
                mIsStepNG = value;
                NotifyPropertyChanged("IsStepNG");
            }
        }

        public MainWindowViewModel()
        {
            Utility.EnableEngMode = ENABLEENGMODE;
            AppVersion = string.Format("Ver: {0}{1}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(), (ENABLEENGMODE) ? "(BETA)" : "");
            CoreVersion = string.Format("CoreVer: {0}", GTBTCore.Utility.Version);
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        
        private Task<string> UpdatePIC18Firmware(string target, string source, int appBaudrate, CancellationToken cancelToken)
        {
            StepMsg = "Updating";
            string cmdStr = string.Format(" -d {0} -r", target);
            if (source != null)
            {
                var regex = new System.Text.RegularExpressions.Regex(@".*\.hex?$");
                cmdStr = (!regex.IsMatch(source)) ? string.Format(" -i {0} -d {1} -p -m -r", source, target) : string.Format(" -d {1} -p -r {0}", source, target);
            }
            System.Diagnostics.Process process = new System.Diagnostics.Process()
            {
                StartInfo = {
                        FileName = System.AppDomain.CurrentDomain.BaseDirectory + "Tools\\cl.exe",
                        Arguments = cmdStr,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    },
                EnableRaisingEvents = true,
            };
            DateTime dtA = DateTime.Now;
            var tcs = new TaskCompletionSource<string>();
            string msg = "";
            SerialPort serialPort = new SerialPort();
            serialPort.PortName = target;
            serialPort.BaudRate = appBaudrate;
            byte[] buffer = new byte[256];
            const string STR = "=========================\r\n";

            var reg = new System.Text.RegularExpressions.Regex(@"Write complete");

            process.Exited += async (sender, args) =>
            {
                if (source != null && !reg.IsMatch(msg))
                {
                    tcs.SetResult("Firmware update not complete");
                    process.Dispose();
                }
                else
                {
                    string msg2 = "";
                    long startTime = DateTime.Now.Ticks + (30000000);
                    serialPort.DataReceived += (sender2, args2) =>
                    {
                        SerialPort sp = (SerialPort)sender2;
                        while (sp.BytesToRead > 0)
                        {
                            Int32 receivedLen = sp.Read(buffer, 0, buffer.Length);
                            Array.Resize(ref buffer, receivedLen);
                            msg2 += System.Text.Encoding.ASCII.GetString(buffer, 0, buffer.Length);
                        }
                    };

                    serialPort.Open();
                    var reg2 = new System.Text.RegularExpressions.Regex(@"EEPROM Write OK");
                    var reg3 = new System.Text.RegularExpressions.Regex(@"Start up");
                    StepMsg = "Checking Start up";
                    while (!reg2.IsMatch(msg2)  && !reg3.IsMatch(msg2) && !cancelToken.IsCancellationRequested)
                    {
                        await Task.Delay(500);
                    }
                    serialPort.Close();
                    
                    COMPortMessage += string.Format("{0}{1}\r\n({3:0.00}s)\r\n{0}{2}\r\n{0}", STR, StepTitle, msg2, (DateTime.Now.Ticks - dtA.Ticks) * 0.0000001);
                    serialPort.Dispose();

                    if (cancelToken.IsCancellationRequested)
                    {
                        tcs.SetResult("Incomplete(User Cancel)");
                    } else
                    {
                        tcs.SetResult("Success");
                    }
                    process.Dispose();
                }
            };

            process.OutputDataReceived += (sender, args) =>
            {
                msg += args.Data;
            };
            //process.ErrorDataReceived += (sender, args) => Console.WriteLine("ERR: " + args.Data);

            bool started = process.Start();
            if (!started)
            {
                tcs.SetResult("Could not start Updating");
                process.Dispose();
            }
            else
            {

                process.BeginOutputReadLine();
                //process.BeginErrorReadLine();
            }


            return tcs.Task;
        }
        
        private async Task<string> CheckGTBat(Slot slot, string port, string serialNumber, bool checkRTC, int protectionValue, List<CmdConfig> checkList, CancellationToken cancelToken)
        {
            List<CmdConfig> tmpCheckList = new List<CmdConfig>();

            StepMsg = "Detect Battery";
            string result = await slot.ConnectGTBattery(port);

            if (result != "Success") return string.Format("Detect Battery Fail: {0}", result);
            BatInfo bat = slot.GetBatInfo();

            if (serialNumber != null)
            {
                StepMsg = "Serial Number";
                
                if (bat.batteryId != serialNumber)
                {
                    return string.Format("Serial Number Fail: Default({0})/Real({1})", serialNumber, bat.batteryId);
                }
            }

            CmdConfig tmpDeltaVolt = checkList.FirstOrDefault(item => item.Name == "DELTA_VOLT");
            bool chekDeltaVolt = (tmpDeltaVolt != null);

            if (chekDeltaVolt)
            {
                StepMsg = "Check Delta Volt";

                Dictionary<string, object> tmpData = new Dictionary<string, object>();
                int index = 0;
                while (!tmpData.ContainsKey("DELTA_VOLT"))
                {
                    await Task.Delay(300);
                    await slot.UserMsg(new List<BatOptions>() {
                        new BatOptions() { Action = "get", CmdName = "CELL_VOLT" }
                    }, tmpData);
                    if (index > 2)
                    {
                        return "Delta Volt Fail: Lost command more than 3";
                    }
                    index++;
                }

                for (int i = 0; i < 5; i++)
                {
                    tmpData = new Dictionary<string, object>();
                    index = 0;
                    if (cancelToken.IsCancellationRequested)
                    {
                        return "Delta Volt Fail(User Cancel)";
                    }
                    await Task.Delay(1000);
                    while (!tmpData.ContainsKey("DELTA_VOLT"))
                    {
                        await Task.Delay(300);
                        await slot.UserMsg(new List<BatOptions>() {
                            new BatOptions() { Action = "get", CmdName = "CELL_VOLT" }
                        }, tmpData);
                        if (index > 2)
                        {
                            return "Delta Volt Fail: Lost command more than 3";
                        }
                        index++;
                    }

                    string realStr = tmpData["DELTA_VOLT"].ToString();
                    string operStr = Utility.OperatorToString(tmpDeltaVolt.CmdOperator);
                    StepMsg = string.Format("Delta Volt: Default({0}) {2} Real({1})", tmpDeltaVolt.Value, realStr, operStr);
                    if (!Utility.IsMatchOperator(tmpDeltaVolt.Type, tmpDeltaVolt.Value, realStr, tmpDeltaVolt.CmdOperator))
                    {
                        return string.Format("Delta Volt Fail: Default({0}) {2} Real({1})", tmpDeltaVolt.Value, realStr, operStr);
                    }
                }
            }

            if (checkRTC)
            {
                StepMsg = "Check RTC";

                Dictionary<string, object> tmpData = new Dictionary<string, object>();
                int index = 0;
                while (!tmpData.ContainsKey("RTC"))
                {
                    await Task.Delay(300);
                    await slot.UserMsg(new List<BatOptions>() {
                        new BatOptions() { Action = "get", CmdName = "RTC" }
                    }, tmpData);
                    if (index > 2)
                    {
                        return "RTC Fail: Lost command more than 3";
                    }
                    index++;
                }

                for (int i = 0; i < 5; i++)
                {
                    DateTime dt1 = (DateTime)tmpData["RTC"];
                    tmpData = new Dictionary<string, object>();
                    index = 0;
                    if (cancelToken.IsCancellationRequested)
                    {
                        return "RTC Fail(User Cancel)";
                    }
                    await Task.Delay(1000);
                    while (!tmpData.ContainsKey("RTC"))
                    {
                        await Task.Delay(300);
                        await slot.UserMsg(new List<BatOptions>() {
                            new BatOptions() { Action = "get", CmdName = "RTC" }
                        }, tmpData);
                        if (index > 2)
                        {
                            return "RTC Fail: Lost command more than 3";
                        }
                        index++;
                    }

                    DateTime dt2 = (DateTime)tmpData["RTC"];
                    string dt1Str = dt1.ToString("yyyy/MM/dd HH:mm:ss");
                    string dt2Str = dt2.ToString("yyyy/MM/dd HH:mm:ss");

                    StepMsg = string.Format("Last RTC: {0}\r\nNew RTC : {1}", dt1Str, dt2Str);
                    if (dt1.Ticks >= dt2.Ticks)
                    {
                        return string.Format("RTC Fail: Last({0}) New({1})", dt1Str, dt2Str);
                    }
                }

                DateTime mcuDT = (DateTime)tmpData["RTC"];

                if (slot.batInfo.ContainsKey("MCU_EEP") && ((byte[])slot.batInfo["MCU_EEP"]).Length > 941)
                {
                    mcuDT = mcuDT.AddMinutes(mcuDT.Minute * -1);
                    mcuDT = mcuDT.AddSeconds(mcuDT.Second * -1);
                    mcuDT = mcuDT.AddMilliseconds(mcuDT.Millisecond * -1);
                    tmpCheckList.Add(new CmdConfig()
                    {
                        CmdType = CmdNameType.MCU_EEP,
                        Name = "MCU_TIMER",
                        Type = mcuDT.GetType().FullName,
                        Value = Utility.ConvertObjToString(mcuDT.AddHours(-2)),
                        CmdOperator = CmdOperatorType.MoreThanOrEqual
                    });
                }
            }

            if (protectionValue > 0)
            {
                Dictionary<string, object> tmpData = new Dictionary<string, object>();
                int len = BatCore.PROTECTIONLIST.Length;
                for (int i = 0; i < len; i++)
                {
                    int tmp = (protectionValue >> i) & 0x01;
                    if (tmp != 0)
                    {
                        StepMsg = BatCore.PROTECTIONLIST[i];
                        int sec = 60;
                        long startTime = DateTime.Now.Ticks + (sec * 10000000);
                        bool isTimeout = false;
                        while (startTime > DateTime.Now.Ticks)
                        {
                            if (cancelToken.IsCancellationRequested)
                            {
                                return string.Format("{0} Fail(User Cancel)", BatCore.PROTECTIONLIST[i]);
                            }
                            await Task.Delay(1000);
                            sec--;
                            StepMsg = string.Format("{0} Timeout({1}s)", BatCore.PROTECTIONLIST[i], sec);

                            await slot.UserMsg(new List<BatOptions>() { new BatOptions() { Action = "get", CmdName = "PROTECTION" } }, tmpData);
                            if (tmpData.ContainsKey("PROTECTION") && ((((Int32)tmpData["PROTECTION"]) >> i) & 0x01) != 0) break;

                            isTimeout = DateTime.Now.Ticks > startTime;
                        }

                        if (isTimeout) return string.Format("{0} Fail", BatCore.PROTECTIONLIST[i]);
                    }
                }
            }

            if (checkList.Count > 0 || tmpCheckList.Count > 0)
            {
                StepMsg = "Other Param";
                ResultList.Clear();

                Dictionary<CmdNameType, Dictionary<string, object>> data = new Dictionary<CmdNameType, Dictionary<string, object>>();
                data.Add(CmdNameType.BAT, Utility.GetDataByKey((from item in BatCore.BATPARAMLIST select item.Name).ToArray(), slot.batInfo));

                data.Add(CmdNameType.MCU_EEP, slot.GetCBEEPToObj());
                data.Add(CmdNameType.OZ1_EEP, slot.GetPBEEPToObj("1"));
                data.Add(CmdNameType.OZ2_EEP, slot.GetPBEEPToObj("2"));
                
                foreach(var item in checkList)
                {
                    tmpCheckList.Add(item);
                }
                string ngAttrStr = "";

                foreach (var item in tmpCheckList)
                {
                    CmdConfigResult resItem = new CmdConfigResult()
                    {
                        Type = item.CmdType.ToString(),
                        Name = item.Name,
                        DefaultValue = item.Value,
                        Condition = Utility.OperatorToString(item.CmdOperator)
                    };

                    object realValue = (data.ContainsKey(item.CmdType) && data[item.CmdType].ContainsKey(item.Name)) ? data[item.CmdType][item.Name] : null;
                    if (realValue != null)
                    {
                        string realValueStr = Utility.ConvertObjToString(realValue);
                        resItem.RealValue = realValueStr;
                        resItem.IsMatch = Utility.IsMatchOperator(item.Type, item.Value, realValueStr, item.CmdOperator);
                    }
                    else
                    {
                        resItem.RealValue = "No Value";
                        resItem.IsMatch = false;
                    }

                    if (!resItem.IsMatch)
                    {
                        ngAttrStr += string.Format("{0},", resItem.Name);
                    }

                    ResultList.Add(resItem);
                }
                ResultList = mResultList;

                if (ngAttrStr != "") return string.Format("Other Param Fail: {0}", ngAttrStr.Trim(','));
            }
            
            return "Success";
        }
 
        private async Task<string> SetupGTBat(Slot slot, string port, List<CmdConfig> configList, bool enableConfigRTC, string serialNumber)
        {
            StepMsg = "Detect Battery";
            string result = await slot.ConnectGTBattery(port);
            if (result != "Success") return string.Format("Detect Battery Fail: {0}", result);
            
            if (serialNumber != null)
            {
                StepMsg = string.Format("Serial Number({0})", serialNumber);
                result = await slot.SetSerialNumber(serialNumber);
                if (result != "Success") return string.Format("Serial Number({0}) Fail: {1}", serialNumber, result);
            }

            if (enableConfigRTC)
            {
                DateTime dt = DateTime.UtcNow;
                StepMsg = string.Format("RTC({0})", dt.ToString("yyyy/MM/dd HH:mm:ss"));
                result = await slot.SetRTC(Utility.ConvertObjToString(dt));
                if (result != "Success") return string.Format("RTC({0}) Fail: {1}", dt.ToString("yyyy/MM/dd HH:mm:ss"), result);
            }

            if (configList.Count > 0)
            {
                StepMsg = "Other Param";
                bool isResetOZ = false;
                string ngAttrStr = "";
                foreach (var item in configList)
                {
                    switch (item.CmdType)
                    {
                        case CmdNameType.MCU_EEP:
                            {
                                result = await slot.SetCBEEPCmd(item.Name, Utility.ConvertStringToObj(item.Type, item.Value));
                                if (result != "Success") ngAttrStr += string.Format("{0}({1})({2}),", item.Name, item.CmdType.ToString(), item.Value);

                                isResetOZ = true;
                                break;
                            }
                        case CmdNameType.OZ1_EEP:
                            {
                                result = await slot.SetPBEEPCmd(item.Name, Utility.ConvertStringToObj(item.Type, item.Value), "1");
                                if (result != "Success") ngAttrStr += string.Format("{0}({1})({2}),", item.Name, item.CmdType.ToString(), item.Value);

                                isResetOZ = true;
                                break;
                            }
                        case CmdNameType.OZ2_EEP:
                            {
                                //  OZ2_
                                result = await slot.SetPBEEPCmd(item.Name, Utility.ConvertStringToObj(item.Type, item.Value), "2");
                                if (result != "Success") ngAttrStr += string.Format("{0}({1})({2}),", item.Name, item.CmdType.ToString(), item.Value);

                                isResetOZ = true;
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }

                if (isResetOZ)
                {
                    await Task.Delay(500);
                    await slot.UserMsg(new List<BatOptions>() { new BatOptions() { Action = "get", CmdName = "RESET_PB" } }, new Dictionary<string, object>());
                    await Task.Delay(500);
                }

                if (ngAttrStr != "") return string.Format("Other Param Fail: {0}", ngAttrStr.Trim(','));
            }
            
            return result;
        }

        private async Task<string> ExecuteFactoryFlow(UtilityRunModel model, DateTime dt, CancellationToken cancelToken)
        {
            UtilitySettingModel setting = mUtilityConfigModel.DefaultUtilitySetting;
            string result = "";
            BatInfo bat = null;
            string mode = setting.Mode.ToString();
            string targetBeforeStr = "";
            string targetBeforeLog = "";
            string sourceStr = "";
            string sourceLog = "";
            string targetAfterStr = "";
            string targetAfterLog = "";
            int appBaudRate = model.AppBaudRate;
            string fwTempFile = "";

            if (!Utility.EnableEngMode && mUtilityConfigModel.Mode != setting.Mode)
            {
                StepTitle = "Check Mode Fail";
                StepMsg = "Not support File Mode";
                return result;
            }

            string sn = setting.EnableConfigSerialNumber ? model.SerialNumber : null;
            if (setting.Mode != UtilityMode.Save)
            {
                if (setting.Mode == UtilityMode.Transfer || setting.CheckTargetBeforeParamList.Count > 0)
                {
                    StepTitle = "Check Target(Before)";
                    Slot slot = mTargetSlot;

                    result = await this.CheckGTBat(slot, model.Target, null, false, 0, setting.CheckTargetBeforeParamList, cancelToken);
                    bat = slot.GetBatInfo();
                    targetBeforeStr = string.Format("{0}_{1}", bat.batteryType, bat.batteryId);
                    targetBeforeLog = slot.SaveLog(dt);
                    if (slot.autoSetting.isBatteryIn) slot.ResetSlotState();

                    if (result != "Success")
                    {
                        StepTitle = "Check Target(Before) Fail";
                        StepMsg = result;
                        return targetBeforeStr;
                    }

                    if (setting.Mode != UtilityMode.Update)
                    {
                        StepTitle = "Check Source";
                        slot = mSourceSlot;

                        result = await this.CheckGTBat(slot, model.Source, null, false, 0, setting.CheckSourceParamList, cancelToken);
                        bat = slot.GetBatInfo();
                        sourceStr = string.Format("{0}_{1}", bat.batteryType, bat.batteryId);
                        sourceLog = slot.SaveLog(dt);
                        appBaudRate = mSourceSlot.baudRate;
                        if (slot.autoSetting.isBatteryIn) slot.ResetSlotState();

                        if (result != "Success")
                        {
                            StepTitle = "Check Source Fail";
                            StepMsg = result;
                            return sourceStr;
                        }
                    }
                }

                if (setting.EnableUpdateFirmware)
                {
                    await Task.Delay(50);
                    if (setting.Mode == UtilityMode.Transfer)
                    {
                        StepTitle = "Transfer Firmware";
                        result = await UpdatePIC18Firmware(model.Target, model.Source, appBaudRate, cancelToken);
                        if (result != "Success")
                        {
                            StepTitle = "Transfer Firmware Fail";
                            StepMsg = result;
                            return string.Format("{0}/{1}", sourceStr, targetBeforeStr);
                        }
                    }
                    else
                    {
                        if (setting.FWUtilityContent != null && setting.FWUtilityContent != "")
                        {
                            StepTitle = "Update Utility Firmware";
                            fwTempFile = MainWindow.SaveFile(setting.LogFolder, "temp.hex", setting.FWUtilityContent);
                            result = await UpdatePIC18Firmware(model.Target, fwTempFile, appBaudRate, cancelToken);
                            MainWindow.RemoveFile(fwTempFile);
                            if (result != "Success")
                            {
                                StepTitle = "Update Utility Firmware Fail";
                                StepMsg = result;
                                return targetBeforeStr;
                            }
                        }

                        if (setting.FWAppContent != null && setting.FWAppContent != "")
                        {
                            StepTitle = "Update App Firmware";
                            fwTempFile = MainWindow.SaveFile(setting.LogFolder, "temp.hex", setting.FWAppContent);
                            result = await UpdatePIC18Firmware(model.Target, fwTempFile, appBaudRate, cancelToken);
                            MainWindow.RemoveFile(fwTempFile);
                            if (result != "Success")
                            {
                                StepTitle = "Update App Firmware Fail";
                                StepMsg = result;
                                return targetBeforeStr;
                            }
                        }
                    }
                }
                
                StepTitle = "Setup Param";
                if (sn != null || setting.EnableConfigRTC || setting.ConfigParamList.Count > 0)
                {
                    result = await this.SetupGTBat(mTargetSlot, model.Target, setting.ConfigParamList, setting.EnableConfigRTC, sn);
                    int tempBaudRate = mTargetSlot.baudRate;
                    if (mTargetSlot.autoSetting.isBatteryIn) mTargetSlot.ResetSlotState();

                    if (result != "Success")
                    {
                        StepTitle = "Setup Param Fail";
                        StepMsg = result;
                        return targetBeforeStr;
                    }

                    StepTitle = "Reboot Battery";
                    result = await UpdatePIC18Firmware(model.Target, null, tempBaudRate, cancelToken);

                    if (result != "Success")
                    {
                        StepTitle = "Reboot Battery Fail";
                        StepMsg = result;
                        return targetBeforeStr;
                    }
                }
            }

            StepTitle = "Check Target(After)";
            result = await this.CheckGTBat(mTargetSlot, model.Target, sn, (setting.EnableCheckRTC || setting.EnableConfigRTC), setting.ProtectionValue, setting.CheckTargetAfterParamList, cancelToken);
            bat = mTargetSlot.GetBatInfo();
            targetAfterStr = string.Format("{0}_{1}", bat.batteryType, bat.batteryId);
            targetAfterLog = mTargetSlot.SaveLog(dt);
            if (mTargetSlot.autoSetting.isBatteryIn) mTargetSlot.ResetSlotState();

            if (result != "Success")
            {
                StepTitle = "Check Target(After) Fail";
                StepMsg = result;
                return targetAfterStr;
            }

            StepTitle = "Done";
            
            if (setting.Mode == UtilityMode.Transfer)
            {
                MainWindow.SaveFile(
                     setting.LogFolder,
                    string.Format("{0}_src_{1}.txt", dt.ToString("yyyyMMddHHmmss"), sourceStr),
                    sourceLog
                );
                MainWindow.SaveFile(
                     setting.LogFolder,
                    string.Format("{0}_dest_before_{1}.txt", dt.ToString("yyyyMMddHHmmss"), targetAfterStr),
                    targetBeforeLog
                );
                MainWindow.SaveFile(
                     setting.LogFolder,
                    string.Format("{0}_dest_after_{1}.txt", dt.ToString("yyyyMMddHHmmss"), targetAfterStr),
                    targetAfterLog
                );
            }
            else
            {
                MainWindow.SaveFile(
                     setting.LogFolder,
                    string.Format("{0}_{1}.txt", targetAfterStr, dt.ToString("yyyyMMddHHmmss")),
                    targetAfterLog
                );
            }

            return targetAfterStr;
        }

        private async void EngLogin(object obj)
        {
            if (!Utility.EnableEngMode)
            {
                await DialogHost.Show(new LoginDialog(), "RootDialog", (object sender2, DialogClosingEventArgs eventArgs) =>
                {
                    if (eventArgs.Parameter == null) return;
                    Utility.EnableEngMode = ((string)eventArgs.Parameter == Utility.EngPwd);
                    NotifyPropertyChanged("EnableSettingVisibility");
                });
            }
        }

        private async void ExcuteUtilityDialog(object obj)
        {
            UtilitySettingModel setting = mUtilityConfigModel.DefaultUtilitySetting;

            if (mUtilityConfigModel.Mode == setting.Mode)
            {
                mUtilityRunModel.EnableSource = (mUtilityConfigModel.Mode == UtilityMode.Transfer);
                mUtilityRunModel.EnableAppBaudRate = ((mUtilityConfigModel.Mode == UtilityMode.Update) && setting.EnableUpdateFirmware);
                mUtilityRunModel.EnableSerialNumber = setting.EnableConfigSerialNumber;

                await DialogHost.Show(new UtilityRunDialog()
                {
                    DataContext = new UtilityRunDialogViewModel(mUtilityRunModel)
                }, "RootDialog", async (object sender, DialogClosingEventArgs eventArgs) =>
                {
                    if (eventArgs.Parameter == null) return;

                    mUtilityRunModel = (UtilityRunModel)eventArgs.Parameter;

                    
                    ResultList.Clear();
                    ResultList = mResultList;
                    COMPortMessage = "";
                    StepTitle = "Ready";
                    StepMsg = "";
                    IsStepNG = false;
                    DateTime startDT = DateTime.UtcNow;

                    IsUtilityRun = true;
                    string ret = "";
                    try
                    {
                        if (mCTS != null) mCTS.Cancel();
                        mCTS = new CancellationTokenSource();
                        CancellationToken cancelToken = mCTS.Token;

                        ret = await ExecuteFactoryFlow(mUtilityRunModel, startDT, cancelToken);
                        IsStepNG = (StepTitle != "Done");
                        if (!IsStepNG)
                        {
                            StepMsg = "";
                        }
                    }
                    catch(Exception ex)
                    {
                        ret = "User Cancel";
                        IsStepNG = true;
                    }

                    if (mTargetSlot.autoSetting.isBatteryIn) mTargetSlot.ResetSlotState();
                    if (mSourceSlot.autoSetting.isBatteryIn) mSourceSlot.ResetSlotState();

                    string spendTime = string.Format("{0:0.00}", (DateTime.UtcNow.Ticks - startDT.Ticks) * 0.0000001);

                    MainWindow.SaveSysLogFile(
                        setting.LogFolder,
                        string.Format("{0}.txt", startDT.ToString("yyyyMMdd")),
                        string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}", startDT.ToString("HH:mm:ss").PadRight(10), spendTime.PadRight(20), setting.Mode.ToString().PadRight(10), StepTitle.PadRight(30), ret.PadRight(40), StepMsg)
                    );

                    IsUtilityRun = false;
                    EnableStopClick = true;
                });
            }
            else
            {
                await DialogHost.Show(new NotifyDialog()
                {
                    DataContext = new NotifyDialogViewModel()
                    {
                        EnableYesNoQuestion = false,
                        NotifyTitle = "Warning",
                        NotifyMessage = "Setting mode is invalid."
                    }
                }, "RootDialog");
            }
        }

        private async void StopUtilityDialog(object obj)
        {
            if (EnableStopClick)
            {
                await DialogHost.Show(new NotifyDialog()
                {
                    DataContext = new NotifyDialogViewModel()
                    {
                        EnableYesNoQuestion = true,
                        NotifyTitle = "Warning",
                        NotifyMessage = "Do you want to cancel the task?"
                    }
                }, "RootDialog", (object sender, DialogClosingEventArgs eventArgs) =>
                {
                    if ((bool)eventArgs.Parameter == false) return;

                    if (mCTS != null) mCTS.Cancel();

                    EnableStopClick = false;
                });
            }
        }

        private async void ExcuteUtilitySettingDialog(object obj)
        {
            await DialogHost.Show(new UtilitySettingDialog() { DataContext = new UtilitySettingDialogViewModel() }, "RootDialog", (object sender, DialogClosingEventArgs eventArgs) =>
            {
                if ((bool)eventArgs.Parameter == false) return;
                NotifyPropertyChanged("UtilityModeStyle");
            });
        }
        
        private async void ExitApp(object obj)
        {
            await DialogHost.Show(new NotifyDialog()
            {
                DataContext = new NotifyDialogViewModel()
                {
                    EnableYesNoQuestion = true,
                    NotifyTitle = "Warning",
                    NotifyMessage = "Do you want to leave now?"
                }
            }, "RootDialog", (object sender, DialogClosingEventArgs eventArgs) =>
            {
                if ((bool)eventArgs.Parameter == false) return;

                MainWindow.ExitAPP();
            });
        }
    }
}
