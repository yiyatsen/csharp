===============================================================
Reocrd Time :      <<2017/10/23 09:30:44>>
===============================================================
===============================================================
[Battery Pack Information]
===============================================================
Device Name                   :YS4840A
Serial Number                 :2216010001
Manufacture Date              :2016/01/07
Design Voltage                :48000 mV
Design Capacity               :40000 mAh
Remain Capacity               :12800 mAh
Full Charge Capacity          :40000 mAh
Cycle Counted                 :50
===============================================================
[State Of Health]
===============================================================
Battery Health                :100 %
===============================================================
[Relative SOC]
===============================================================
Relative SOC                  :32 %
===============================================================
[Recent Current]
===============================================================
Current                       :0 mA
===============================================================
[Pack Voltage]
===============================================================
Voltage                       :50988 mV
===============================================================
[Pack Temperature]
===============================================================
Battery Temperature           :25.56 ℃
===============================================================
[Usage Current Error Record]
===============================================================
===============================================================
[Cells Voltage]
===============================================================
Cell[01] Volt.                :3645 mV
Cell[02] Volt.                :3645 mV
Cell[03] Volt.                :3643 mV
Cell[04] Volt.                :3642 mV
Cell[05] Volt.                :3642 mV
Cell[06] Volt.                :3642 mV
Cell[07] Volt.                :3642 mV
Cell[08] Volt.                :3638 mV
Cell[09] Volt.                :3643 mV
Cell[10] Volt.                :3642 mV
Cell[11] Volt.                :3641 mV
Cell[12] Volt.                :3643 mV
Cell[13] Volt.                :3640 mV
Cell[14] Volt.                :3640 mV
Cell Voltage Delta            :7 mV
===============================================================
[Pack Detail Info.]
===============================================================
MCU Firmware Ver              :V02.02.6.45F
Last Charging Time            :2000/00/00 00:00:00 0%
Last Discharging Time         :2000/00/00 00:00:00 0%
===============================================================









===============================================================
H5teiIdbgNi2EYQjYejeeRTiLt58Eaij3MY8XoBocWr3v54gsdBez/r3ibpiHN6YqztKABWbXnPeyfHt70Gk1IQxslXYiG5jOlEv7RxDKCTOo6DhX0vKFTN9wwe5MNK+aBMoXYMl50XBmvM77WvKSHgm2/YCdV36MtwY4WOFxJKCDXbM8WH28NxgkZgzcLrNrbVWTTbcVCOjPKUJbRWE9gcSekpCaZJOqU+r2++T3ylPFRPT2yE0PCQnHgdJ+h5Xn1K9lks/V7cWrzaVQByuYdRVP9yZah48wJ6JeDJNeOOYsCx/RcArn5xjdcW/cn1Msf1yumzZWoYtP9WP/TtJdRyA2tCyCf/34DWxBuTUtIkxQxL4oYIY5SjljDIi91svOCs/cnjzpTtvkPlWpJMDgTYpjJyLBAowa/XzAlmT/LD3rN/ugMf169IM8o7/ZiQsaB5MVfjj0RWf42Qq32UGGkU40S+be+CCxquowVP5hKQD4n5i5J9cQcR+6ZqHqq34V/h1uZMde+3bIThP7UqEbFqR7ofhoQHiQmZ+RojTtPQoub5QK3WYNtfEWnVsWDnHucU2ngEql6dTRtgeesaDc0RrgFHnqwAWPNpPOL9zlvGx76yCBR9uWjYRXnU3R52kABRU4dUhed9Hf8Xv93K5GiY8MmzFx+eeqrHyew8OT/STibKCs5j18XOdHnrq8yL0buPymEkgwxL8zke8ZYgi/QBXNdV2SVCx6kEdoHF9e1Lw7GTCeIQfYciUNVwkmwbyIqME6FSWVFlcgjnvyGPhqHlqN4F9RjR3eV6XHLMPKWvi5lhg52UYovnJKIJMn4eX1DU8ww8b3nwrJGq1Cx8FtZz2zHQ03Jpnhq5kMV8SMS9ot7ZFVRstP9BM6etT9QQdv302aJwNjGv9/iqNYjb0ucZyjUG8wqzW9JLtuhY/JP1aCpmUAGTVmgke+p2gfWGUsSWldPGbH10k/xS9PCcIsmpEhKvKsa9pAAyZ43SsIR0Kb5CVvji/NtSre7pZRJPJXxS1ZgNDPrlKYeJ8FD2VOyver+TxmJ2u57gk9ykW7tigiVcyHwykMj4WsD46On/03Qpjp3xcCM13aTrCVh8dJxzp8cavjh5Fqyho1dC9cM2NI0qYojwcqBLonuCVHivN6pCS+Qpc/C7840LdOuC7xxoiSLasGh2sVHfzzr0EF9/1CNWDi0rEaMM+OE6U4H2cXW3KZSAXopm+IyFMh92mvtt8HciegWBiJwLodj31UzHZNdV2wGNUhHvVTH6Efm9tfxsE4N/3BSZEDwiphoUIgYW0dc8RrcAS2ENWnNe7TE/VARCWL9BE4yWIiHnN2aONMiDpHx9vTRBthlJMzI98KnEcKhiWItNecXyKMyIwYeNAO12rJmgmIlMXsF/MI8uENaEkizULhI2Uw3c1wfkodl2rthSU2wXPgIGd4O/dhC72yDutYbNRkqa/ve8p8JFkCj06yWB4DrYE76EjaNzZ0p1FqSqlYOI9MmLuIGDWQfDKCEGC1vAXlesfj3IIufaxbOnbRWR4fVrRJYVOXG963iHXqsRbAq3/iZV+3peu+8Y11IFBPiWgD5U1ORGzn8ENv6af3ULEl24iNxlzvPAQN68crjKPCnOU1/KlVGbwc+6HNMN8VpGL+l8TqhKIDy7r7cSB03Id0mJ9HFQCsc7zCkQX3JPW/EtjCXhhiZ+hCspmqZQzTCYOE48XC4n2e4xJlCT0ebQu6P7QLnpUvXPDKt22LaIM370BGo/oHgYgjX6gfQrJq+OVVrRrl4g7djvur+L1Ig7Uq3PX7LY0gD1WsGKs4GapRO2A+kkqu1QcAvivUmpa/l0tqlOe99qobr58FPMFZxps4pg+uAvGAmudTkscnyxyChpc2mJQ6srbBlw0R7WanumTpkZlaTELaQ9gpQY2bdbmshDgzPRALCyf09W1R6ZvK9vayJOu4srwVOjp6a5rhSgUBkIQyjPKZSu7+F6vB0PhDWZGBKV1i3wO/WsSCLsW4A5JeNVH4UYHpwP+1k7tbHz9l6Z5RySq1x8xXJ+DehwjmV2CB53SpNNOhgvTcrCO8vjmEGKPcJEG5vSXRDYHpJ4BdPJ++p0vRW6Mytnh/pQ13oYU+amdANvF7JlGBdULcH3Rf5yKtGBSUPzMidikIFUAsoAhjvZpcptlzlqkz7W9oUYO61rvsO0W8lFPGHQxtWw/Pv5NgH9bdHLmfjUwCvRhWQFXzYzIIDz1s31+2ZSAjXoxMapMLFo+xWHspsxVuEwiftqn7JFEq4YnEt3Kb9NiBBK6V+/JTWS4GW5RoUiowVg472Bd9TdpzDu7OAWr3x2oFitzdMqPSVdvfElKERp/gV5jGNTBa4F2b0nAWc/aDM895wHLn6rnWezP4VFg+jRKJYnRVwJyPMKybwOv6kJXSfLsqXaYEL52nq77GAhPi+IJI4U4ywPSxI5w3RXLtDZ8tN9hLry3LSF4OI83gY5g0UuYobMoDLv/3BB7y0N/tD7vntIJDaiEmvGPfVMZdhlvUu5cPD6TCq7GntrA9paEiUfMhVrTBU2EsX5y3/u27lxegmVujfgP2kqgZw2fsIaK2M9L1ecoWYmONzxWDwCDAbd26wn/nqL3gE2+PIQgPLs8qKg9FiXXWYfKl8lhiQNuxfAuBBvxJ1d0GxZc0HERto506PxZFX7yu3aJgR4KuLg8OPpkIAw+jkxZ7ccgggOCqtf7OjmSfkRuZMKwUmSutqLut1240QAUzKkihyqSTnLHxpjOoTkVPKtFhWbPdytCyyLfXZJ0sy+iSX44yHL6ajtFITcmH5EQsBR+OVDd0egdBZOx6V4OyOq9I5sJB3/REsaz4whB7G8lEY747NhO2+E/6Y1U3C8s+3XiZq4iPmsDqtKWsts68NSKA1vCShwOmhT32EPFGZyVYAFRLSeZ2RMW9KTM15FqoOLQSY/cnaJn8PGZxAOcG0M5dnbXFfW0/jgeFet3/r2jn8dou9mSAuA2TsiJHx/ekOOGtkzFv9SUpDYVmU4N0O46s5/vsZnA3h/mVPeWW+akHNrOtEDxnlua/9DGyofJjjjPyCBMMZYJ/L7wdzo3kef3jS7nrg37ddCIX88+mN1Ld1i9ZiZwkg8d2HzQn63xcxzyYGybT1j0kICovCxyj40Tewyu7w9UBIvAmCV/Q7NE3TbiIobarBkbbYWDtjVkAyTqyv4JryKBGYoQVLU1EhzEXzgeuJ8RSwqbg/syX1KJRG6IqasU8DuZQgOlV9Oq2hj7ri2tfTBgktNBZ/zslPnmRbkATQsJk63WkmBuTdtMfC6Rp5vGzUa27HtghBQ6aYewrQvBqORQ+L6vWGIyGIOpg5a/QLDMblcrTNFyokXZ4qhNPIlenXR9duGIMX/vG749WrgO6AkUZ1VczKDH/LDb5lbsCHyEK1eLMR7PCfaoZeMPWp3QOh+Y5d6JUKGOQguXzYa4FHOmFua92Cu4TgtoNg865Ne8Y/arjFo2Q5Sh9vTo0qRBJUzs7fZOBLV0beRqmA/9AVAoatU6sE9Ha+Tu46D2BdYno/Pvq7O/a3rRRkini2RWZFXK9E+x6bGLDICMaVdfAG860It2EvMvqBetnY8P2JkVsFihDej2pSepTc1+Wzp8AoLzCXBfhFILCzwKR4OH7jNbDQOKx8X0wdHPOqb4nl93AAFixUhVWIOCLkDkVCWmp5it8LzWw9KpEZs81up8abAU9TqqSEf/HCoCYkY4GAeg5Bkoiodwv68n6N3jxp5XexNcBGc5LJV/ZBXyHLv3HZZ+5AZ9T/7ltA5ArEkEdfDViGp5CxZhr9E9A/nLjI8/xLJB1rfO3AmvwraFKTtWdH907lptBGmfv7VFeZQMWi8zwFPKlCE++jXBdJZ7pmaPtmY2hCTxIbS5rpOc/RY6LVbrqF5Ga8LwaUbHMpKBaPYO5gjp/J/A0rJFNuwmmndrdzYdkR9v881IxLjh+1RXZsKdVKIag77sFCCEUfyKvqXpa0AFiw9H6Dgghfb20eVSaNK9eNvXlVOykGoXtlLhuZn4CKTv5B4T15e2LE8dJGKV4CU3uEcPX9lxF9Pws/PPkCsr9binGHDRErug8hgr62ixCZNHtAVhKpBi8AS7lSDhuzk0PchZW2mlyI3Pg2uvV17q28gc7EDHTvYc7mx3to4/JuSFsZXYpFEPzvrHMB1nOjgflkG6YKY7g/CDbxqQrkpFrFOSTfOFbLWamaVU/YExgUETXFxjwyHRdjUwiwchyQEsb1LUZQ+WnF+co0S7sNvIm7/DuiahsmMR5y7vSvfiLz70Z+xXFi5PcQf+6qI5R8BapjmTWgThszFvJQvQY3Kdc0yVgiDx3FsOlF3HMgPNWM/rqmZTu7eAUxxtf4eLj/3PI/6KZgrDMFKw90zUCb7dvtIkvJRTyXkE7ohDt7xY91e5feMfzaNh7Ve3ufd9iL6eQ4uQWlhMwqCVR5HyOqztbrQvkOT9M6VVB0YUCvRvce3mSp0iwnPsDa9NMbu6MTtTkMIHp6VDqUCCtbWPHNL2WbBw8V4W7dcGWTYZtNZoUWN3wDnalHNbJhUehFQQzRjw6Bspqi3v/8JM5oIcCNPfnnZd/MB6jL4+Ldaezoj+at2augPGd/YN5Y+whGCAtw1vkg4yPbuiXnRX6tiG/s0qtztNUQxj32aCDyJGvcYazB8pANe2lMpFDzq1hpI6TakkazJC/NpngR7iPlNfkuOwdmLeMayitKnHOsoc0rXyndr9/cuc8mZjO0gbcp2jcCGu/yLmdvXU/b9UVPvAdpogZvNQbR2msLI2zCBoePAkfuYTv7xBiCJk7fyi7hitQcStqNroJYGRT94kZJ4AxQfm2EUfXiumti2QwlK6ibsd4BcjKlA1emLjXaABSoqhXG+WG4/OSPGWRIRw1dPrnjJ+7ts66ZjnOTytEtwjU6gODxeRxA4WtNPV7bwARe2EyDL3tCSHSHNACiSGzMlHRAJ1sN9mmkhspD3HRuLmkEFAoG2AjkQZxcoLCAF202fJbZK11oMYzcCPtViDu43IHda10c3p6z/LsFfjlNlvrZITxUKpNeJyVXzehSM24o8Td2Cn/Z1JG28XfL/AsxLHmSXHHxuY60GOkDVmvlGDpLiops2GdKW714owiuSmM2KrMClQbfMeK8+vv14Fd/DzoqJRW1d58qvYWdMMsovAxKLFACtFIeOqMvchkbng6g4Qlm6bFa1nZgs=
!===============================================================