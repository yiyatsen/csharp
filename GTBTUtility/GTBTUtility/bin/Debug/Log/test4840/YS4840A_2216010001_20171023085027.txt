===============================================================
Reocrd Time :      <<2017/10/23 08:50:27>>
===============================================================
===============================================================
[Battery Pack Information]
===============================================================
Device Name                   :YS4840A
Serial Number                 :2216010001
Manufacture Date              :2016/01/07
Design Voltage                :48000 mV
Design Capacity               :40000 mAh
Remain Capacity               :12800 mAh
Full Charge Capacity          :40000 mAh
Cycle Counted                 :50
===============================================================
[State Of Health]
===============================================================
Battery Health                :100 %
===============================================================
[Relative SOC]
===============================================================
Relative SOC                  :32 %
===============================================================
[Recent Current]
===============================================================
Current                       :0 mA
===============================================================
[Pack Voltage]
===============================================================
Voltage                       :50989 mV
===============================================================
[Pack Temperature]
===============================================================
Battery Temperature           :25.69 ℃
===============================================================
[Usage Current Error Record]
===============================================================
===============================================================
[Cells Voltage]
===============================================================
Cell[01] Volt.                :3645 mV
Cell[02] Volt.                :3645 mV
Cell[03] Volt.                :3643 mV
Cell[04] Volt.                :3642 mV
Cell[05] Volt.                :3642 mV
Cell[06] Volt.                :3641 mV
Cell[07] Volt.                :3642 mV
Cell[08] Volt.                :3638 mV
Cell[09] Volt.                :3643 mV
Cell[10] Volt.                :3642 mV
Cell[11] Volt.                :3641 mV
Cell[12] Volt.                :3642 mV
Cell[13] Volt.                :3641 mV
Cell[14] Volt.                :3642 mV
Cell Voltage Delta            :7 mV
===============================================================
[Pack Detail Info.]
===============================================================
MCU Firmware Ver              :V02.02.6.45F
Last Charging Time            :2000/00/00 00:00:00 0%
Last Discharging Time         :2000/00/00 00:00:00 0%
===============================================================









===============================================================
Y3dI0X4di4xQaJq95E8t3LWZhLv71rcUoXxzI7O0XwMnmRd7fVsiup5NcE4AaBUKu/kz6T/Ng1xhLoqrenNyqodqQs6hKyUdVHQerGqXQkyC5J++1QTyobzCXN6ulFYgYyu09y5nVSkz/KdRMIP9lg0ZNg66gMck7WF2NljUErNcylbCw97Z3CuCtRy5Db/5YMlxm2eCjJGp2jZs3/BntHzZ2i+xtf+98+r+r1g4+bUckltj4BfH5FD5h5y+4XOA454UJY5bX4Dst6EA2oF7bmfK3JAi14roUX/DGjtmWhY05pCSkC+T2GADuUWRgT3iBTrfghWeKW15ecIrb+CZaM6QgZuwEX5FjxOaBU1X3x+/AoS0aGvSpod5sTNLRlBCUbuVNHcIjVPsh9nWFVUFbV6cpjdlHFAc7v3aTZVaYVj/TMR0wXc52eI9iwjDVO6jZ/ED28o+sy4A4HlYwM8c8a+5d6PhEUYg/DtjhF9R/Vuu3sokZmYre3ib5bdZQ6f7WxMrd+SXNkrMMbzeayma5BpLUVOISOqt9x0HcDUd5nwuQTObZ/tqf2nqFAx9LHITZ8HUXpHb571PABNqAPLNn42ea01KOIW58GGmWtPLsjaYS25wAFxKcbFu9Dk3ZiZQLWUp+/fFYcykY3/kp0Kz1tjJjl6uq2WapRFcnoQbLAOBG1/y/rfd3DmOWweQhhMHQi8o4+8/pdc2rEqPIvg3e5dg4LGnGpICflznPARaMyKVVqta5tUbZ3S5N9XlRMf4uyvPULebJx5k2haXjV+S76sTW10/tacs4omON31krR/nxBR8/RXKHZnpbgJH+Dxtd3E68H2Y7rAk+U6s/j3DC44kg+SkMkK4XGK49qkz0oaz67Qu8nBxjiTBA+AUK5g6MJTI0RJ4+ypggF7IdxBrJLMr153KIlNVFqbOFuHLDtgjLiPbbaqkOE7b8Ym5IobDA+EWqS/G8rHyNfVwY3Y0q6AfTZHIDJu/Eg47g6QZjUKn2Nmuw0liqT62v3j07L2NjNNQiIT1ZZzfvj3WNGgoVHM1IRQmeugsid065LU7UUD0mpLz7M+BLA39uMBKMCNx5lpZk34p/br7HwaY1jALl7OSaxC0VOgbinMX6GCB0JGXk6hEO4pNytGdOhD+qEw6nFRpDNx4wyRLJFZIZN/wW+j8HOHgDtHFkBDNShrY61fi9iItrHV5Nm+U4ehNEkxuf/TyjPzKFyQ//3FADJkQ/OwJC9/XwUxV+bZ9qmR/opupgCg63iiikeks4A82IUwWFCFMKi8EvfH3h4eYs6zAx9gisWmRYl9HvQZXqys0c7hY9Gd2PQv/uxUaMUBOlL3UtIqUg7OKX99x6Mq9Aaj9qYv/zBTs3UOBRBYOg05HkQc6wB6hryOZJhUwVshuTcC3r1RaLqJvEp6UahYEBActgCK5bnuyeQcvPonX6WXjC/0ak58BSIFUxwbAsH8Rrxwi4HCXgnHUb+j4pqrW+JzSkUjZivtY32m3rRMhgGxjwKHchM3LhskDOHgfQgOokW0ukm+Lee/sqvCsTCRcnztDbRrD4GD7ZZMvQuK0xW/imRbe2vT1KzO7qQR44iH9a8AEBfuADSsgNikfkQMPIPubhtMFsUVmpF08CsvnmTtkrcykweus8yt+Zz+8W5BWTxrEkjrk1VF5rO3BRZoxgiBMHFILjestYVJI2RF7zFSkvuSUFNQLOAAjfA6rmFP3LCyPOu5ICxbGXP2flqaL0ZzpzQrEd5NxqcElkmSY5bLDzfsViFGeBcuDu1o6tWY2C3uyz5PWViebbIBuC18Q4Y8KFQHVxsD0yIAmfOkJTs2+aoeQLU4tuZA4yLVcEY2nDGLUe6OpSUcD1OJ12QAUZ197ng7lwEOji7pV2yiHgnsh8vmhnLZd1b0hW9gM/GcG8LkdbsgDsO1kJh7M2ZRBEeOFLW1nm3vdQa8Py1EUuyi/iZnxLLPDe02f7haUqGplYOEakSF/H9vX9WJxit3lSyCEGeL3b1+RiV1E2uGgwdynjYcF4IaM0VjJoBz4ywefXj7dsJPmY5FZn488b6zFlkzO1QwKrcE9GfLhtGOATF8YMgdMl70Ks2c/0eSqEVy4EXYy3trbarD3+zGMSjOuA14+n4T/+3nCAvWVvRLzeqTmpPB7f0fRrYLMflhhBm/SufLQniPcQ912OTsoMgnjQoolKri/vOmSofXwEiYEO8EoUPLPhTuUNsmf5P6NSDMNKzn9whFEGo7hetY4123FVD4325TwQoiojLZQdd2fwvYECFVibFlE83Sx9Lj3cuEK5jf4csw8QeZKBj23mKy35t0FKNTJiNrbmzGKzjxtLExrSsQjH1x8TjWTsm/W9noOzOJTjrKuxyjsYz0hvq2lWalukPFM0zZgKkA7pfTzj0+rLcgjxIiITqHWqMNyAMTSTNKy2Y4ku3KKSlElREzy5Oh946ioGgL1NJNWPRi/Uo4PKW6Eymh/Cg36H0o6WMpc76KLxyAgWBA6YpFRe7QoflMzTnKqtSmU/H0VDsV5V+ajF+XJqOl7ou7HUlq3KOuY3JY+DINi2sfV6GF0YiegVj2WE7a1ttTrKZ2zjngWWOukMMqe7bpusuMlOHEhNh6twBF3Vd+LTECQ65o34JSc6FI7orH6d9Bs0S73pP7hGqlWYQXDTciyPLR4Df/jLlQcBZj3bgdnmA4XJan/VLJC+xkNiaHpEi2Q/RORPHc9cbnN3BkDTfmpLrtFYIwV/0K5nj+XYKPkGmm4uDZByq0aWem8E3qm0NBa1znOsPe2dsZjWIPeNIcwN4YFZu6k4CbTuThIlEZugWEYohA7qvLCK5D6elXuDuZsFyY4VbIGLZaABGOHpqIpgiU/EeY5GT1S9GptppbeeXGETkjgneMjk1BxIiQ1sV31LFbqdLavt8cYOujUB3dO+sXb7LfNExvT/EPx3qJV9Mzlvvy2vYTue2R5ggXmiYg0Jc8m0BZWz/1s1F7GR54RAgMeNopQZB/ysmauEtrzy/784D6f1sa4WPOaoZUFwrSGkBMt72XYMt260gXZcXSi+tNAAZO/ZDaeqR4fnGVz/m72FQlcJpH4+Sr2YOD18rCmg9WK6OiS6tLVga74tv+JX3iwBykyqHxnNSrMNplsdiqUwEQCSylUiTS1ib2/dGody/aAvPxJDuGFFI+Gs1ZCHLlBLW1IPPF52zreNTFPBDV+xBn6g1M+07HAGFbyCIsWpSv2MPbCZhNrsQZbP3dBT5fjCtNMv2U9M2IeSnWyHkT8Ds0a2VWnsF7YPw5SB3OLD6JiRMjN63UjWK7qZhBv333IhOQpvfJpmUvYcCH25shYin7nFmUfg0liBowMLW5Mh8DZDANSssifIqmpwZXayjSZrh7YfWyp3nZ3Ht5PMKRzedSb3j5Piz+WVebGUUYRdTYDsYMFf3CSBKsv5qh2I5AAs++EEd5t6zhNLxlZ1olAso4W3h1CLfZ6WKCAYIj5UJ/CQ+WvHqnk1iRVp4g5kIMG6eNMeUf4gjXjNSp5hTfNvszKT3vAfMQYuOwm7GShZGkC9+ZTnX+ZAM3e4oSYvzaTOIjcmpGWD6p51DU08198YVXE4OC+r+phaQW4nd7PxMFPEF85DwzwZ1v1e0ruP8CX+2rr1zfvlgac1uVQjFLjHthzKiWHMo2SZ/RZRifnyi3Y42f5pKjRhBZ10r5d/XF8fpCgCcYeXv7EhJqnuGnNavnfC/3a4lNB/5/G3Sq8PsHuVeixhR36/qxP5rMBkKQkeJpDc0gljceMq+aH8i70WhsVdW/drqO8czFgJFVLKdw0pG2EwEDzv2ushs5CGD773SH/fpQuNq9cr2pD3dHU7kUkMd9wtEWR4LuKD63MRHE/DdzSBclHPURXdaYuMLlCSXFfr8lP2i8vOnQLP7RKCqzt6nOr2z1//fOSWZCDYMWXmaj3XTEQNufaDzbjkPHItqjddcbkZ2FDzn72wdEoHrHSgrBahh5G7KZ9gthwzC5EjOnS2ZGzK9jMrkC1rJ02vrvDFLUHnUmg9Lo6Cf72BGbOvZHvoHFicOp3oDupDzyQfyCNaT+3lWInaC32nF+XeKCyk9jm1MS5dgqTdB8ZiLfP5Bl/MDlQUOipDzBeFWhuWkXd48nDPJ996IWNO5oZNFZpTsRbPHOn5IK9A3kwTrP+ShVPKCUVTTq0VPmov5cju36NVlgYo8pWMkloUnfvYeJse4t7HciMq5Qssy41cL5LHgRGVBxWuRO5kDnyuj5/BLG4gVZhtz6vSAmDM+o4OTJ8UxdYDxhpP9l1kHnULQ4e+XHp94ROolkp+HiGNc0wPfI2BXiWwJciSGskYUTzK6zuTqsIVWjYueJO70BGbdGQtu1HjR5FY9OrAtjfrb9nzftP9hRnS9KcYhiV8gQ8UM52fe544nz9FGnyQ/i8syGH254kPzyo2jDYjrJqyPTl8IRDX9NWBSY7DQbyIyFEAfQW7YTAx6NYx/woDGIdy2WJ+xV06ITsz3QHb5iIBxQ3bh28XM/C0knKlGDxejMqIr1XLW+TezZTEDvrMTMhqpaOLtOukQMqRVDQS4cDk1N/wOcF0FvIpiSbLXlQWYeLhrq8KtVrMrnQpHAVIDS88wJQ5u29C0W//bCZlTHDj75jqVZuQkJeIQcsaHxPFEMczO5UfSSBvdQKQTX2wcfTQLKjqw5s9OwrQThr6YCKosMNpnyb+u9WNUtrVeDn9cBBMavrWPiVHWcvzfReZL/Oodgeq1fDn1Zfnc5lLvMMMWUaQmpSg36g0+SageItqUDQ5xiKTNKY7Y/p+5O0vLzYidLhPzo0DfBIMVhU6JZVrqjVPBjp9WM8RlX2kxjha/ojX1pEnfji64OCDLmkqkOC4h4NxepISX0ozAMSxUhbIsmIydqkgGHVdYIWmzl6DvrlNPyiLYhKpnKemiia0v/IGXbR8UcwyiAbOy4KRwGipbcPSqDulkCn1chvT/j5HZWbzR3lCd5MUySvtRrVu4re478moHCAFzbZkUyuvmrEZirM0jXXWbhX+loWovBMtBv/867VlTfUCrm/uK2sP2leUovhUk08w04pbJqGUE1wXhtRCFWbJTXiMaXUI/jTVF2w/TKTQ0FARBaDUJU8BgDJwgkSmQzR90WsTHYw1dNposgKhWro8xYFePdFgi1dxREvEu8XaDc+qEbik3edDqEu1YHffoqtIXJ+EezCAtcD4h/f3TLLyfrsFBU=
!===============================================================