===============================================================
Reocrd Time :      <<2017/10/23 08:01:27>>
===============================================================
===============================================================
[Battery Pack Information]
===============================================================
Device Name                   :YS4840A
Serial Number                 :2216010001
Manufacture Date              :2016/01/07
Design Voltage                :48000 mV
Design Capacity               :40000 mAh
Remain Capacity               :12800 mAh
Full Charge Capacity          :40000 mAh
Cycle Counted                 :50
===============================================================
[State Of Health]
===============================================================
Battery Health                :100 %
===============================================================
[Relative SOC]
===============================================================
Relative SOC                  :32 %
===============================================================
[Recent Current]
===============================================================
Current                       :0 mA
===============================================================
[Pack Voltage]
===============================================================
Voltage                       :50992 mV
===============================================================
[Pack Temperature]
===============================================================
Battery Temperature           :25.56 ℃
===============================================================
[Usage Current Error Record]
===============================================================
===============================================================
[Cells Voltage]
===============================================================
Cell[01] Volt.                :3645 mV
Cell[02] Volt.                :3645 mV
Cell[03] Volt.                :3645 mV
Cell[04] Volt.                :3642 mV
Cell[05] Volt.                :3642 mV
Cell[06] Volt.                :3642 mV
Cell[07] Volt.                :3642 mV
Cell[08] Volt.                :3638 mV
Cell[09] Volt.                :3643 mV
Cell[10] Volt.                :3642 mV
Cell[11] Volt.                :3641 mV
Cell[12] Volt.                :3642 mV
Cell[13] Volt.                :3641 mV
Cell[14] Volt.                :3642 mV
Cell Voltage Delta            :7 mV
===============================================================
[Pack Detail Info.]
===============================================================
MCU Firmware Ver              :V02.02.6.45F
Last Charging Time            :2000/00/00 00:00:00 0%
Last Discharging Time         :2000/00/00 00:00:00 0%
===============================================================









===============================================================
kpg6IDNvv84DnmlclqQJNcfbwvYIIuJeIZqrP1QvipK0mi20Yv+jYE6robhFwoClWG9mLgDPUEZbugVEaqB4QdQ413gOZHxKTqq5Kbua40ljroxcCStf/BfTfZouuosswebXRBywjyQvfqKwCQwdpe9PkrgO0+853BQKcLZnxZVnKAphJ7klupvzTtXOhgLgbZ0lMuV+M6g59iTIdZ1c71/SAJNr0X80AEt+EmMpuorIaP2hZefRocyqFYMBdN68NHX0jOfZmBv0D6fwGu8HxGOndmpn5FoPdj1t0YZhm+nIlM2zG37i3FxOKhpwbTqj35gBKIIPNI/uhXpQiyScqCad0HgOpahBEIMV3+pkCopPk8DAXSkyH3P5o0vaEb1Gj50CEdN0qyD7NFQX8DqbE6Erj6AndSsACxJNv3dPnXURz7YgZh1vR5ctJzAxMCkZRwRx6O9ZXgr8IisfpCIwuN2EOza/ey25eqOUmPMv7KjQ+HCuok7aCeRVDyn5uRgz/7jjGpZHMHhDhmrK3/MlovuMUf1MWmiBZw9QIgtznpWteZ+luQ2UO9i/tups08pX4gA7uhY5C7fKIy6QaKnysfjeLI24A71Nxpis7IUFmEDvfnEwpy8gccmVjjpp0BBen8ZO9lz6xzr0YEvuqairXL1fZqi88CNK1uOcI5bS0lDJD6m9REKxuXMRutFZ6rJDThfKlvd6fn2xQIbbWWwQBCzIzZrp3s49kUgdqIt8JIRu0osX760qNbDxzFujmrPh2fb+ovTbxjjg0IwlpqK2Z50dlQ0eS2qQXZ2r5IBFArMPVGBvTRlUGf7gP6qVXzeMZZ7AfoFAoZldt0nkGY5qjC0+UrxzMgcDQAm263T6QYtZUZnNtK3WI9vhvxAPiJgQ2zHlaXz4K1FdhZhCDqWuMfJ1bpgYA2i28w/KXlxalnBv3nAUhez7V5Wr/+lOTyIZ33apf4lb16EekHAQguiGOwBo4xijsLodj7V+wc2Ny3PQiqd/cKhLz4x2EDHFd2a/2NaAVq7VvZ/k7Xnr6ay8mV3DC/TpLMDGyvkawOVk8Tvr4YiyvaT8wn6VHezDMa+utqHfJG5IlefIno5oL5nYo5CGcIyedPuXl2mhwl2GGM7Z6WKishKi5o2sVvvBAPuDgAHPi/t8Hviv1FmOOe6ihUDY5zoyQQzkDrknMIpCnxx+jUgHik4E6P2a6Vq8JL2CQyy8dyaWGuOPzEubLibyuS0kMVrFRQPzJxS9MSBTIfx85oEUNSJL3tIywtEYlhEDgNWOebqVJ0HJZRZgLF71sLHZqp3h6psE5w7OmVx81ipS1t/0ziLfhQlsBiEi1gDc0oRC1HCRhTcW6SbiRr3n35fJgZeHErpllD5Cni4EvmiFyPgLRczVKWKaXPIivnbz8Nuk9iaPn4M8rtNIYVwxM8Xf9kPTmdBP2pg4dzzkD/8Me/3pWdMzGHsrMpsHfwxXaL77yPH6jRkLvWBm30GUJTbehwFQK0bU+OaqSDCrY+b+T1I55tGsvuJI3BLPqHmwdc1FRwkAYBfDdFR55Lgbt9an9cZCWyycaqzlpiW1VNRrcWSjiDSVOE0ve6AlyHuonViakdJex31p5D0FXV91I6SnsgiKfLULjsFi+oOZRh49mOIyhLESd8Ll1QC4YRV3Y3wPce5apzoCLvG36KVbGYnYXJYGpqJw9LpP/0abl9p85rS8xyTJ1pIig63Z6Qks3U50PdcgtYecGbSuGncIZFC3tPkDh+nnHg7GuvMHRboh+Fmk5dg/5aGjLdc/ow5pfaDxIaQTd67I8ghvqaTQRDFkf/vc/3jhbxHXzLrgAsJaUPgK0AWzGa9CedJKaxQDJf4mTRWuWiGE0lOWhxtjQAW5fuQeOYeC69UNkyPyYN1ZlytNGbvWBquYVnOTEPiV/zZnEGFdh5WVppg3Gz+tfaXD/cUOLGQRp8q8OSA0/D/WEkBTZw5ieFiH045uingoL6pyWe88aa68yFQ6E+2pvCVGHSt3DR99oVXOkA6pyYqHwlvJysq3WMkC8wE1f2/3MvVdwm2jtrTo5V7V0P82kSLV2O5sAevrcf5pnz1xooxWW5twW67zDYjuUGsgZtn+lkgobFZQC0XhV7LM1j8P3cBCvf4OnV2bNF7lLxFCtk9rIPU15y97irYxXprgwq76Z7dR1JID5utFOYSjDAzKGIiG7oKRtYVus7uV3ilNQUrP1gDK9RBvaT4axPgoICUD30ELNQZrOKnwyYpsy6C1mNutSRFndYuMSWz/4NyB5WoCxXk2z+7aTZ3IJEgCk98FAYL/KsKiJQJPFVQjAq+38Oj/56vq2JETXjDjAieRoSUIwhoUPBErgi0Fd2Rm7MHeeEfJSFsewIE3mCARxTWnAcnOzFmNPZ0mn923/FQQFC1SZqTfupBA6xWdZob+c++NB7fAH1Z2GrPYQSjVXlnejWQgvqqE8yOU8pSwcf8ni9YiG7HxOBJrnuz4tdbMtciAan97shIC0z8fwhOVXKDZJ3bjyG4E5u3TZL86MpdJVUzXi2JnbsyVLi/BOd8O1B2eeBaG+FkNtZQnAOi760euDhKpCZAnpYtOVBKme/U0ZUOBzwQOcZzsC16zwCXwlUnr8sEZQgoQr5INoDk11meUoONvU9qk3oe4BjRSpOrXiUMRpVW+WL/wEqFWVm3+Drv7o6Pu9S3hezcLZK0PLlY6a/6xM121exZAtbdDfP5DwDIAecThbfBtsLF1ubEIC2+J6VfwKVzASMl7Tn1ogL6inGjwhLdq4ujoRIjozi1vYxFuAvlTp3zGodKTLlupMhDvtCJeBZtTQlhC4vxm3GYkcPg69AgbFbWV2pCMd89+8nkVtL5maYkzGeq+V8Y7lGImQghmI/A1+dKhMKfr8TLRl6vlDOI0wXDQCBMxdtRg7gAvE9qDy3hKv8zk2C8qMHBggjhPwWYVJZLQAwV5AymLNl+/Jgr4L4XYXqtYNhTrluntYYBpiuiKgz2z7dNmhO75TyyI2c3IRBbRxvM+x2GyMMLu44RSROvFVehCSumYS3Yzd+Achs2qcaEE51G5mqkfOzZ1i5a66oR1BlBsA7KHG5VcLZ/Q4I7/r4GU9Pl7zm1trcbfASEmFHtQ2Sus8s9RFEiu3UM/V7RYq2swR62jITXRQ9Q7LhwNYiQuxia0dKkLkpwJ/P6hqnfKl2xLes/0JMGUVTBdEg3PfO7Ca+H5+B04SQhH3ca5cIeF7fYOfoJSruXb1O93SwwP43jLi73r2NjH5/glYDEsc/b9wo/7CVt6l/NE+WQYyD0/5h3up23C//MF+awuwUWkhHKR3hEfasmABqU5PIkJq//4SwbFoQ==
!===============================================================