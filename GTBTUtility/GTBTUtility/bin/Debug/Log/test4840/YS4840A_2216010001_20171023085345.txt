===============================================================
Reocrd Time :      <<2017/10/23 08:53:45>>
===============================================================
===============================================================
[Battery Pack Information]
===============================================================
Device Name                   :YS4840A
Serial Number                 :2216010001
Manufacture Date              :2016/01/07
Design Voltage                :48000 mV
Design Capacity               :40000 mAh
Remain Capacity               :12800 mAh
Full Charge Capacity          :40000 mAh
Cycle Counted                 :50
===============================================================
[State Of Health]
===============================================================
Battery Health                :100 %
===============================================================
[Relative SOC]
===============================================================
Relative SOC                  :32 %
===============================================================
[Recent Current]
===============================================================
Current                       :0 mA
===============================================================
[Pack Voltage]
===============================================================
Voltage                       :50990 mV
===============================================================
[Pack Temperature]
===============================================================
Battery Temperature           :25.56 ℃
===============================================================
[Usage Current Error Record]
===============================================================
===============================================================
[Cells Voltage]
===============================================================
Cell[01] Volt.                :3645 mV
Cell[02] Volt.                :3645 mV
Cell[03] Volt.                :3643 mV
Cell[04] Volt.                :3642 mV
Cell[05] Volt.                :3642 mV
Cell[06] Volt.                :3641 mV
Cell[07] Volt.                :3642 mV
Cell[08] Volt.                :3638 mV
Cell[09] Volt.                :3643 mV
Cell[10] Volt.                :3642 mV
Cell[11] Volt.                :3642 mV
Cell[12] Volt.                :3643 mV
Cell[13] Volt.                :3641 mV
Cell[14] Volt.                :3641 mV
Cell Voltage Delta            :7 mV
===============================================================
[Pack Detail Info.]
===============================================================
MCU Firmware Ver              :V02.02.6.45F
Last Charging Time            :2000/00/00 00:00:00 0%
Last Discharging Time         :2000/00/00 00:00:00 0%
===============================================================









===============================================================
ZfMGhJH1DtceLyDHp0h5Hpj25Wtfsc6t9X7o8AAlSElIB/fktwekS737IM1Jgn/lnq3zGUVEaQmXjcKmuYjenfBQfOyplSt2VO6pSVhOiD3spNoiI4UmvhUbnSh5kCmvdaRBpv33aHIBVNIaopuEht3LInc377Z+BKjAVspBPP4rCmETlJYdHmOCHN5UbqOZ29HN6SUhGgpCWdE9dLo6sZe+F4CV6g8p4mvcpE8WpJt2wxwm6GfqL1jZkZTSrnH6iR9Z3zCq6g75uSVdvXsFMBwr1okXDhbck3tYGQAN6I7u/yK0HUaRdAJwHP8j49pNbigkiDYqzZKD11fsJKU2gqPRtqSVMs1CnuvIcCg4DDH1axSV8BYm6lPGJ8K0vyBzDtBuDt370z6Gaz5lQlvKgkPOLNOEqouSx316vYw34D1USNxk+4dKqhZTEomN0kiYzcmMV3ZmzdleOof8qxETUqukVQ3wTPXKHLTwcGlpwFQC/KWPAzuYWHKg0fdcgrUG18vxOnxjwLSCkjQwWpANIZpg4cuYCFBihrqVRqQMK0qGSnCqUbcna+6iyhfgnK7FMwxcFnUqOk9s6XTclFy0ZDHWDatVWl2t/G/uVwPkMkBVhpdKNG58zvxA3vmwQZzFPRnEQq4xAuRnz70dVlj/c7d6vaMzAZg4EFnzmXLgcUE15RLSY7nWlanTMtNLj3XuBcMflqn1JCAXZSlyC2+AEuFClGpmDlveUY5ZiAYWDwlSHkxejXxdwW/YdmFRJxDf1NPEmrGtXIDrrSoc5FBaePXEu0pHAZ3r+QiFyuIMqRe7CTeSLrkZYkC5tweWYWkLHyaiSnEDkMfbA3bw9ABEZdYspqVZN+pQcS3pGycMGEKU2Is0lqTpCPs3H9vs3mTvGjM+SBhcso+Q35hoDfpZIrSmdgyEBabxPnSCqwAEgIFgZw/49msDcAkiDP3ak8Ox6zqYA70sxm3zx5dz5ZFiSwhwfcXG07AkiMi7ifWgUNDSCW88/Sl4zjqY/+FhkN41McB3BVXoFOKlHiUCPnWAbp/E3hEojJcpXDN73R210ZgEJ4kOJ5DL0yx6sN/tKYSX0Du1EcuPgpHmDL/wjYuHx8n4lRa94iEmwkDewXb0AySv/xngfuc4o3ohZIoBsNKPQ57n6gmejcykmisfROIqNASwFVSE//Hf6h7tIwV5F1ZvfutVUnpJW+5Zj9Awg03TS18LKnnQjCWFPmEH15gZ/RJ1aups+g3fwMCAHTOrTqv57s2E/07BlY0k4pgAyF9fkg5Vm3q8xM6MlOXZNQra4qoB/6hEdEv1lKVFiYWgJ9nCWvmqZwFdF5EKuxGoIkquYmzRENFD5jahq6xsp55O8jDauLF13zwNmYUWJP+WWrhJac/mc1W2SRLP8xbJubrOpFFLMe2WXh7lB63jcuumXYPIdDskpKeKFMy8fm0L74EQM8XLqoFPqXT0MGter0TQ9OdzvzU1xRGGYYBGFjctfDWhZUvoeXC+bHOq9K2hmGr/KgDcCT77rYhdh1O5+Z1pOB+/rJu+oMVFawxDKoKisyWe+JuBpa+xG9M0rYbDoNX3q8/UOmDfBPzw2rgwtkLQRcsDXfHh/XJL839dXnry4mqYWa1h0WHtw/InZz2ZQs3wqEV1DTeptW/6yfyfPKBpSDMWB3bunVPghwh3yk1RBXggxC9kgxGdROAh6bqjsK3LydId+ZxoNhqXnvR+JYd3shBLnn2SG7Jt6b0ONvYGUAvA2LETWOOCITQk6N9oQjyh4SSrjn4YeaTyF8hJ7wZNM/MN7crIn+S0nOnRcytzjDVuDs5467C3yne7UMSWSlP61zbGZPMjbW2vVGHMkvcb3UVfrpTylN0I3XLXYORaRF/dsx5pzqn5QqO/6Q4+cRDFKsGD8zaoMF64tG6kXF3Mbn+zsnS5DcovKxzc6YVsgVIElBGH+p+QO1rKH81WrbUObLgH3VKvvAMutHEPHpksal0aJxS9b7+16e1xIzmEYhAOsyEkIYYknMaIWAIbjT6/u+6k14U6/cbELTI8WfV1aGtXtchhyw440UwL29Fs+4yUmCEQ0OIqr4YtqY4gRfSLX7i5mwV1BgqJiPbn+UPIH2B4TklVEQe0rO+2Vn+RcSqYn0vP65MJABRkuK309LzVYbcUykY+KpZD82U4rZ6sa3Bh1MUVUsQtrNNhalqnXdq2wHg6AwEK2jOLHUuW1rtQU3G1nQ6P7wD0wXWZQMANdlJFuF+Ppv35uqmqBk34L7eNCiL3NL2RY0EZu0atpdz/UQR1L9G3TzUSxpZTtzdJ4PgVoPhnQTl/L90KWyJ5hhHbmsL/7Z1xelSwv3xYp42ZNIaOPNcoRd6mD99K0wGNo9vSncUg0sICdhDkDgr9p4wcPpQPaJy4wvctax0caZcjBXeLgf2rM/6SqNPUwqzk+FJ7QUEvytn1k+Bh6msDPEEgmXnGyZLWeGq0OsQrpevvbnWojMJonPKaTYv+t10a1bour74hEiWi3t9rDJSF23aWsO1wRJS4A0i+dr5TsVTcRARE+oSWOLKyI09mmOD5Z0FBDU4+4C8eR6bxk+KsvOw5lfS2vvf8GqXesd0Asd1Bh05CTekm4ro7I00d7t5w62JHLz1Up/u9G8KK17eCORvJYV53hqyTwAgCP5VZvac/KSnQGBK6g4VWvMDBBTySxdHtpU/i7tbXq+9mofUzeCwtsG6T1OyLlcIES9qI95Du9atrujYlttloutxHhhBq0o0TNLjEcXiYnYhN7j+C+UbPa6uwnAmCagsGVJPBtyW7me1qr6nQBC+XKrlbpNSiY2w1SyMONtlXCd5RW/61+btLEoNZsOqVSMy3Y34rvkHBr9Q3WPWRAN8F94RasQMzaKC13l++/tjdC97O7BE6WaU/oTCHd8v/x+YqWvB9HXViGDe/JWguAuZ7WM2pYyV9FvL1SXH4tOVoRWeZGVQDm4VCFFtTCaXj5BDKSF+R5fWTyjRnjZO209mA6HieEtSNBLPxewvgsP7Uv3poMaSpzT6mhHzPSxWU1GdNHH1dZi+TVJPkMJ5hHcHKL2vASxK3qgyBqEO9sVw5Ne6GuqU0sKLIAh6S3xBSm69bHUGWa2JosDnrgWeVeW8cw21AwKrlx2KCFjdPxKd2CWETQW7AV0vFJV11/D8SoVjXIjpPXh0Ab3jIT7ewW/Y2KHdBdZhGXc6nqaKiq6/nAdUjZZ6g7qD0nefJJMnWiYeMkqgmaRWTCf29nXWZPrx/crY3PzU940RxWtiAenN9uembWSXRhwTjGEQATkmK0D2VpimgdcMKaAmkeEWR0LdKhRdgIUKIa7f0SkImsgqhdY6NuDK6tqpt1j+jL3eq217hXpTov9V9Mbbipi2LqTtagQ1heaDo41YoIZuZH3/kkP3uABlHBF0C2FyotgJkugWoHLdH6qiumbNhyDDdSTFA1lX5auIpW5t2wvj5hwmpDKUMe6IjBlzVt2gT8lKBGpK2USp+oP9HtLvIl3yB+9dsnTFYOHCXaxHo9Z5jszGjMIbVV3RIeAxaWJrEGKjOyVDf59xIZ4d1azKozINX65BZXWl8nohCZPP2gIPBtqjj5SLieAr4sIGU/dpU42C5IZL5JSL2EaT7g/Hhgx8zZSNVzDYopELoK4wkKO7bsqXxgTM5I7rWy0NdWn9cUeCVuEz0IEYgoULPAaMECbBAjqxlIspTw0OJhBX+OWSvkpjPz0TUS9EZt+wjyt/zU25lneoOOyaczziH+ZKHFfcs+hKXR7QU01tAcIVe27vZDonfplOXDlWOeR6DnKPMg0yfO5diVZAnsR55Tl28c5SMqOVEHmSQ9t/ek7ASOVIAAEMgaAxYQ7VE7GpUyQbBiZibbfu3Ai+oQQlDxHxr+dG/WPY2aq8ChQTfVUfIlGDNH6gM48BwZ5pPqWgj/ItFEa+/Nifr7Oe8oA7U2JroCykFqCOCCwCapnp/rgwZEjNOl5SF77956aKvojT3n1si+ZXfIPbmoSB0y3FarZ3U1M0jUwJoWoWczBdG8NBbI0K7Wi2z7CEyk34Bdw4Yzo4Ng93FYO/5Kl5oOp26cdlMXG5D+l4+IqRqKc6XmyfdgEYfKTxxmAZ+1S1n1iHxyzSzjMqdUlebwZi7WpJv9jH/aXmXpsjtQcMYHHl8OTM4cIyS90ULR0Tbuj2kYcVeymLLQ7g55gwjL4BkZXOPXlIZskLGqmFRlXQt3ee8mHGahQB7Zh9hWHPuDvrWTMLowlEcT2Uah9KM+Z8NmvUK31sBRHkJfSb/xp+Vt8nlcLEXsFStixPRXssz2L1ePI4qzEeQ6/aVEKFtD2pBWjxBRwgc2PH+rJF6TaYlSySkPxl9jajJpe70379fqu5jRPy3KJcnG/oU9NKuieYakiYZjnzgZehKiuzC7QQUFpOmGlHMXO2oB7El1R07ZZGeO0I6Eh4uXeeIK5VEZ2K1vNlQN+N7wYCdzktNcOvbKzb6DtAlnBqsytJc85Vd6uZlkK4HVrUSYBr3lKBg1oP+baZEwQ3ETMDBrUDzryhaLtMwJFQU4y3Vzv47DibaMMXso/bmcRyOeAPeM2iV9iFXNz1+oqEcoCtSK9o/CoC39uU2Wlcxz/GEI8oPen61B1pPALiywx5xKiXo5jWaCG5837r1uYZpM/kIso2VJ9pJ9skvNHCChh/P0znniZM6Br5dNj98FYp4rKd51A3V5UUKYoiWH8XRSVqkhpgP08r0okRseSgcdXoi4qrSZo/HecllJy9LxVLF4yDIynCbCzKcVbFh4WQnyXReAH8Q5wmXETaleisjW79uTBFlAiz0ryqvF2C+b2LlmLEjDWEhBQqBlQH4GTLVMK9ws0AeBw/TgUr76l6Hf4wzobDbANGbJZ6WFcvLu2so3fG5QQEDKZSr2uje7Hd7ZRMFJNASG5SVcOPN6mVu2zLxw05h4vPtVZg175RAAeKT8rbv2eMpfjPYHhAwh654h9s6Ao0pGRu7nYxRTiiLxaM/6ewi6g1zd27hjlAhhLpTnauIohgC+hqbzlpCPqgIsGpPYEYSaDIRFDMSG12wWOGz4JK1XB5cbHumtw6OAX6ey36mKQ1MPvAFekkAdnQzCYo911bdream1P6JhcPBaFKW6v3BjTzgtyiPTS4arRu/cnnq0H0RnC0WK+JPn3Xm8c4mZIF86PFTlL8vcOt/nsSUaNMBNKmRTu31B8JN8KH63IXKuzsavXLAyqk=
!===============================================================