﻿using GTBTCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTBTUtility.Model
{
    public enum UtilityMode
    {
        Save = 0,
        Update = 1,
        Transfer =2
    }


    public class UtilitySettingModel
    {
        public UtilityMode Mode { get; set; }

        public bool EnableUpdateFirmware { get; set; }
        public string FWUtilityName { get; set; }
        public string FWUtilityContent { get; set; }
        public string FWAppName { get; set; }
        public string FWAppContent { get; set; }

        public bool EnableConfigSerialNumber { get; set; }
        public bool EnableConfigRTC { get; set; }
        
        public List<CmdConfig> ConfigParamList { get; set; }

        public bool EnableCheckRTC { get; set; }
        public int ProtectionValue { get; set; }

        public string LogFolder { get; set; }
        
        public List<CmdConfig> CheckSourceParamList { get; set; }
        public List<CmdConfig> CheckTargetBeforeParamList { get; set; }
        public List<CmdConfig> CheckTargetAfterParamList { get; set; }

        public UtilitySettingModel()
        {
            FWUtilityName = "";
            FWUtilityContent = "";
            FWAppName = "";
            FWAppContent = "";

            ConfigParamList = new List<CmdConfig>();
            CheckSourceParamList = new List<CmdConfig>();
            CheckTargetBeforeParamList = new List<CmdConfig>();
            CheckTargetAfterParamList = new List<CmdConfig>();

            LogFolder = "";
        }
    }

    public class UtilityConfigModel
    {
        public UtilityMode Mode { get; set; }
        
        public bool EnableSettingFile { get; set; }
        public string SettingFile { get; set; }
        public UtilitySettingModel DefaultUtilitySetting { get; set; }

        public UtilityConfigModel()
        {
            Mode = UtilityMode.Save;
            EnableSettingFile = false;
            SettingFile = "";
            DefaultUtilitySetting = new UtilitySettingModel();
        }
    }
}
