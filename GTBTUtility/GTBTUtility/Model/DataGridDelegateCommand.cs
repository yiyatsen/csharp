﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GTBTUtility.Model
{
    public class DataGridDelegateCommand : ICommand
    {
        Action<object> execute;
        Predicate<object> canExecute;

        public DataGridDelegateCommand(Action<object> _execute) : this(_execute, null)
        { }

        public DataGridDelegateCommand(Action<object> _execute, Predicate<object> _canexecute)
        {
            canExecute = _canexecute;
            execute = _execute;
        }


        public bool CanExecute(object parameter)
        {
            return canExecute == null ? true : canExecute(parameter);
        }
        public event EventHandler CanExecuteChanged;
        public void Execute(object parameter)
        {
            execute(parameter);
        }
    }
}
