﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace GTBTUtility.Model
{
    public class SerialNumberValidationRule : ValidationRule
    {
        System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"^\d{10}$");
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            return !reg.IsMatch((value ?? "").ToString())
                ? new ValidationResult(false, "Serial Number invalid.")
                : ValidationResult.ValidResult;
        }
    }
}
