﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTBTUtility.Model
{
    public class UtilityRunModel
    {
        public bool EnableSource { get; set; }
        public bool EnableSerialNumber { get; set; }
        public bool EnableAppBaudRate { get; set; }
        public string Source { get; set; }
        public string Target { get; set; }
        public string SerialNumber { get; set; }
        public int AppBaudRate { get; set; }

        public UtilityRunModel()
        {
            Source = "";
            Target = "";
            SerialNumber = "";
            AppBaudRate = 115200;
        }
    }
}
