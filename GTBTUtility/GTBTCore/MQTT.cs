﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace GTBTCore
{
    public class MQTT
    {
        public MQTT()
        {
            MqttClient client = new MqttClient("localhost");
            byte code = client.Connect(Guid.NewGuid().ToString());
            client.MqttMsgSubscribed += client_MqttMsgSubscribed;
            client.MqttMsgPublished += client_MqttMsgPublished;
            client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;

            ushort msgId2 = client.Subscribe(new string[] { "/topic" },
                new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });


            ushort msgId = client.Publish("/topic/1", // topic
                                          Encoding.UTF8.GetBytes("MyMessageBody"), // message body
                                          MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, // QoS level
                                          false); // retained
        }

        void client_MqttMsgSubscribed(object sender, MqttMsgSubscribedEventArgs e)
        {
            Debug.WriteLine("Subscribed for id = " + e.MessageId);
        }

        void client_MqttMsgPublished(object sender, MqttMsgPublishedEventArgs e)
        {
            Debug.WriteLine("MessageId = " + e.MessageId + " Published = " + e.IsPublished);
        }

        void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            Debug.WriteLine("Received = " + Encoding.UTF8.GetString(e.Message) + " on topic " + e.Topic);
        }
    }
}
