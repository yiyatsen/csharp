﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTBTCore.MCUEEP
{
    public abstract class EEPAPI
    {
        public class EEPCmdConfig
        {
            public string Type { get; set; }
            public string Name { get; set; }
            public int Address { get; set; }
            public int Length { get; set; }
            public object Value { get; set; }
        }

        public static byte[] GetCmdConfigEEPArray(CmdConfig config, byte[] data)
        {
            byte[] value = null;
            if (config != null && config.Length > 0 && data.Length > (config.Address + config.Length))
            {
                Array.Resize(ref value, config.Length);
                Array.Copy(data, config.Address, value, 0, config.Length);
            }
            else
            {
                value = new byte[0];
            }

            return value;
        }

        public abstract List<CmdConfig> GetCmdConfigList();
        
        public CmdConfig GetCmdConfig(string name)
        {
            return GetCmdConfigList().FirstOrDefault(item => item.Name == name);
        }

        public byte[] GetEEPList(Dictionary<string, object> batInfo, byte[] eep)
        {
            int maxAddress = 0;
            List<byte[]> array = new List<byte[]>();
            List<EEPCmdConfig> configList = new List<EEPCmdConfig>();
            foreach (var item in batInfo)
            {
                CmdConfig config = GetCmdConfig(item.Key);
                if (config != null && item.Value != null)
                {
                    int tmpLastAddress = config.Address + config.Length;
                    if (tmpLastAddress > maxAddress) maxAddress = tmpLastAddress;

                    configList.Add(new EEPCmdConfig() { Type = config.Type, Name = item.Key, Value = item.Value, Address = config.Address, Length = config.Length });
                }
            }

            byte[] eepArray = new byte[maxAddress];
            foreach (var item in configList)
            {
                byte[] rawData = ConvertObjToCmdArray(item.Name, item.Value, eep);
                Array.Copy(rawData, 0, eepArray, item.Address, rawData.Length);
            }

            return eepArray;
        }

        public abstract Dictionary<string, object> ConvertCmdArrayToObj(string name, byte[] eep, Dictionary<string, object> obj);

        public abstract byte[] ConvertObjToCmdArray(string name, object obj, byte[] eep);
    }
}
