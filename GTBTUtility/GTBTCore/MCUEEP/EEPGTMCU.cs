﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTBTCore.MCUEEP
{
    public class EEPGTMCU : EEPAPI
    {
        public static List<CmdConfig> CMDCONFIGLIST = new List<CmdConfig>()
        {
            new CmdConfig() { Name = "DESIGN_CAPACITY", Type = "System.UInt64", Address = 0x0002, Length = 4 },
            new CmdConfig() { Name = "FULL_CHARGE_CAPACITY", Type = "System.UInt64", Address = 0x0008, Length = 4 },
            new CmdConfig() { Name = "CYCLE_COUNT", Type = "System.UInt32", Address = 0x000C, Length = 2 },
            new CmdConfig() { Name = "MCU_RESET_COUNT", Type = "System.UInt32", Address = 0x000E, Length = 2 },
            new CmdConfig() { Name = "DESIGN_VOLTAGE", Type = "System.UInt64", Address = 0x0010, Length = 2 },
            new CmdConfig() { Name = "MANUFACTURE_DATE", Type = "System.Int32[]", Address = 0x0012, Length = 2 },
            new CmdConfig() { Name = "BATTERY_ID", Type = "System.UInt32", Address = 0x0014, Length = 2 },
            new CmdConfig() { Name = "DEVICE_NAME", Type = "System.String", Address = 0x0016, Length = 8 },
            //new CmdConfig() { Name = "EEP_REVISION", IsReadOnly = true, Type = "Byte", Address = 0x0026, Length = 10 },
            new CmdConfig() { Name = "MANUFACTURE_PRODUCT", Type = "System.String", Address = 0x0038, Length = 1 },
            new CmdConfig() { Name = "MANUFACTURE_WEEK", Type = "System.Int32", Address = 0x0039, Length = 1 },
            new CmdConfig() { Name = "EXN_ID", Type = "System.Byte[]", Address = 0x0056, Length = 20 },
            new CmdConfig() { Name = "ALERTHAPPENTIME", IsReadOnly = true, Type = "System.String[]", Address = 0x0074, Length = 91 },
            new CmdConfig() { Name = "LOG_DISCHARGE", IsReadOnly = true, Type = "System.String[]", Address = 0x0114, Length = 125 },
            new CmdConfig() { Name = "LOG_CHARGE", IsReadOnly = true, Type = "System.String[]", Address = 0x0197, Length = 230 },
            new CmdConfig() { Name = "FCC_RESET_TIME", IsReadOnly = true, Type = "System.String", Address = 0x038B, Length = 7 },
            new CmdConfig() { Name = "RTC_RESET_TIME", IsReadOnly = true, Type = "System.String", Address = 0x0392, Length = 7 },
            new CmdConfig() { Name = "DISCHARGE_INTERVAL_VALUE", IsReadOnly = true, Type = "System.UInt64[]", Address = 0x0399, Length = 20 },
            new CmdConfig() { Name = "MCU_TIMER", IsReadOnly = true, Type = "System.DateTime", Address = 0x03AD, Length = 4 },
            new CmdConfig() { Name = "TEMPERATURE_INTERVAL_VALUE", IsReadOnly = true, Type = "System.UInt64[]", Address = 0x03B1, Length = 20 }
        };
        
        public override List<CmdConfig> GetCmdConfigList()
        {
            return CMDCONFIGLIST;
        }
        
        public override Dictionary<string, object> ConvertCmdArrayToObj(string name, byte[] eep, Dictionary<string, object> obj)
        {
            if (obj == null) obj = new Dictionary<string, object>();

            CmdConfig config = GetCmdConfig(name);
            byte[] data = EEPAPI.GetCmdConfigEEPArray(config, eep);
            if (data.Length == 0) return obj;
            
            switch (config.Name)
            {
                case "DESIGN_CAPACITY":
                case "FULL_CHARGE_CAPACITY":
                    {
                        obj.Add(config.Name, (UInt64)(((((data[0] << 24) | (data[1] << 16)) | (data[2] << 8)) | data[3]) / 3600));
                        break;
                    }
                case "DESIGN_VOLTAGE":
                    {
                        obj.Add(config.Name, (UInt64)(((data[0] << 8) | data[1]) * 1000));
                        break;
                    }
                case "MANUFACTURE_PRODUCT":
                    {
                        obj.Add(config.Name, string.Format("{0}{1}", (data[0] >> 4) & 0xF, data[0] & 0xF));
                        break;
                    }
                case "MANUFACTURE_WEEK":
                    {
                        obj.Add(config.Name, (Int32)data[0]);
                        break;
                    }
                case "BATTERY_ID":
                case "CYCLE_COUNT":
                case "MCU_RESET_COUNT":
                    {
                        obj.Add(config.Name, (UInt32)((data[0] << 8) | data[1]));
                        break;
                    }
                case "MANUFACTURE_DATE":
                    {
                        int tmpValue = (data[0] << 8) | data[1];
                        int tmpYear = ((tmpValue >> 9) & 0x0000007F) + Utility.DEFAULTYEAR;
                        if (tmpYear > DateTime.UtcNow.Year) tmpYear -= 20;
                        Int32[] dt = new Int32[] { tmpYear, (tmpValue >> 5) & 0x0000000F, tmpValue & 0x0000001F };
                        obj.Add(config.Name, dt);
                        break;
                    }
                case "ALERTHAPPENTIME":
                    {
                        int size = data.Length / 13;
                        List<string> tmpVal = new List<string>();

                        for (int i = 0; i < size; i += 1)
                        {
                            List<int> tmpData = new List<int>();
                            int index = i * 13;
                            int vMin = (int)(((data[index + 6] << 8) + (data[index + 7])) * 1.2207); // 5000 / 4096;
                            int vMax = (int)(((data[index + 8] << 8) + (data[index + 9])) * 1.2207); // 5000 / 4096;
                            tmpData.Add(data[index] + Utility.DEFAULTYEAR);
                            tmpData.Add(data[index + 1]);
                            tmpData.Add(data[index + 2]);
                            tmpData.Add(data[index + 3]);
                            tmpData.Add(data[index + 4]);
                            tmpData.Add(data[index + 5]);
                            tmpData.Add((vMin > 0xFFFF) ? 0 : vMin);
                            tmpData.Add((vMax > 0xFFFF) ? 0 : vMax);
                            tmpData.Add((data[index + 10] << 8) + (data[index + 11]));
                            tmpData.Add(data[index + 12]);
                            tmpVal.Add(string.Format("{0:0000}-{1:00}-{2:00}T{3:00}:{4:00}:{5:00} {6:0000}(mV) {7:0000}(mV) {8:000}(%) {9}", tmpData[0], tmpData[1], tmpData[2], tmpData[3], tmpData[4], tmpData[5], tmpData[6], tmpData[7], tmpData[8], tmpData[9]));
                        }
                        obj.Add(config.Name, tmpVal.ToArray());
                        break;
                    }
                case "DEVICE_NAME":
                    {
                        string str = "";

                        for (int i = 0; i < data.Length; i++) str += String.Format("{0}", (char)data[i]).ToString().Trim('\0');

                        obj.Add(config.Name, str.Trim());
                        break;
                    }
                case "LOG_CHARGE":
                    {
                        int size = data.Length / 10;
                        List<string> tmpVal = new List<string>();

                        for (int i = 0; i < size; i += 1)
                        {
                            List<int> tmpData = new List<int>();
                            int info = (data[i * 10] << 8) | data[(i * 10) + 1];

                            tmpData.Add(((info >> 9) & 0x0000007F) + Utility.DEFAULTYEAR); // year
                            tmpData.Add((info >> 5) & 0x0000000F); // month
                            tmpData.Add(info & 0x0000001F); // day
                            tmpData.Add(data[(i * 10) + 2] & 0x0000001F); // hour
                            tmpData.Add(data[(i * 10) + 3] & 0x0000003F); // minute
                            tmpData.Add(((data[(i * 10) + 2] & 0x80) << 9) | (data[(i * 10) + 4] << 8) | data[(i * 10) + 5]); // voltstart
                            tmpData.Add(((data[(i * 10) + 8] & 0x80) << 9) | (data[(i * 10) + 6] << 8) | data[(i * 10) + 7]); // voltstop

                            if (i == 1)
                            {
                                tmpData.Add((((0x7F & data[(i * 10) + 8]) << 8) | (data[(i * 10) + 9]) * 60)); // duration
                            }
                            else
                            {
                                tmpData.Add(((0x7F & data[(i * 10) + 8]) << 8) | data[(i * 10) + 9]); // duration
                            }
                            tmpVal.Add(string.Format("{0:0000}-{1:00}-{2:00}T{3:00}:{4:00} {5:0000}(mV) {6:0000}(mV) {7:00000} (min)", tmpData[0], tmpData[1], tmpData[2], tmpData[3], tmpData[4], tmpData[5], tmpData[6], tmpData[7]));
                        }

                        obj.Add(config.Name, tmpVal.ToArray());
                        break;
                    }
                case "LOG_DISCHARGE":
                    {
                        int size = data.Length / 5;

                        List<string> tmpVal = new List<string>();
                        for (int i = 0; i < size; i += 1)
                        {
                            List<int> tmpData = new List<int>();
                            int info = (data[i * 5] << 8) | data[(i * 5) + 1];

                            tmpData.Add(((info >> 9) & 0x0000007F) + Utility.DEFAULTYEAR); // year
                            tmpData.Add((info >> 5) & 0x0000000F); // month
                            tmpData.Add(info & 0x0000001F); // day
                            tmpData.Add(data[(i * 5) + 2] & 0x0000001F); // hour
                            tmpData.Add(data[(i * 5) + 3] & 0x0000003F); // minute
                            tmpData.Add(((data[(i * 5) + 2] & 0x80) >> 5) | (data[(i * 5) + 4])); //current

                            tmpVal.Add(string.Format("{0:0000}-{1:00}-{2:00}T{3:00}:{4:00} {5:000}(Ah)", tmpData[0], tmpData[1], tmpData[2], tmpData[3], tmpData[4], tmpData[5]));
                        }

                        obj.Add(config.Name, tmpVal.ToArray());
                        break;
                    }
                case "FCC_RESET_TIME":
                case "RTC_RESET_TIME":
                    {
                        List<int> tmpVal = new List<int>();
                        tmpVal.Add(data[0] + Utility.DEFAULTYEAR); // year
                        tmpVal.Add(data[1]); // month
                        tmpVal.Add(data[2]); // day
                        tmpVal.Add(data[3]); // hour
                        tmpVal.Add(data[4]); // min
                        tmpVal.Add(((data[5] << 8) + (data[6]))); // Count
                        
                        obj.Add(config.Name, string.Format("{0:0000}-{1:00}-{2:00}T{3:00}:{4:00} {5}", tmpVal[0], tmpVal[1], tmpVal[2], tmpVal[3], tmpVal[4], tmpVal[5]));
                        break;
                    }
                case "MCU_TIMER":
                    {
                        try
                        {
                            obj.Add(config.Name, DateTime.Parse(string.Format("{0:0000}-{1:00}-{2:00}T{3:00}:00:00.000Z", data[0] + Utility.DEFAULTYEAR, data[1], data[2], data[3])).ToUniversalTime());
                        } catch (Exception ex)
                        {
                            obj.Add(config.Name, DateTime.Parse("2000-01-01T00:00:00.000Z").ToUniversalTime());
                        }
                        
                        break;
                    }
                case "DISCHARGE_INTERVAL_VALUE":
                case "TEMPERATURE_INTERVAL_VALUE":
                    {
                        List<ulong> tmpVal = new List<ulong>();
                        int num = data.Length / 4;
                        for (int i = 0; i < num; i += 1)
                        {
                            tmpVal.Add((ulong)((data[i * 4] << 24) + (data[(i * 4) + 1] << 16) + (data[(i * 4) + 2] << 8) + data[(i * 4) + 3]));
                        }

                        obj.Add(config.Name, tmpVal.ToArray());
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            return obj;
        }

        public override byte[] ConvertObjToCmdArray(string name, object obj, byte[] eep)
        {
            List<byte> array = new List<byte>();
            switch (name)
            {
                case "DESIGN_CAPACITY":
                case "FULL_CHARGE_CAPACITY":
                    {
                        ulong tmpData = (ulong)obj * 3600;
                        array.Add((byte)((tmpData >> 24) & 0xFF));
                        array.Add((byte)((tmpData >> 16) & 0xFF));
                        array.Add((byte)((tmpData >> 8) & 0xFF));
                        array.Add((byte)(tmpData & 0xFF));
                        break;
                    }
                case "DESIGN_VOLTAGE":
                    {
                        uint tmpData = (uint)((ulong)obj * 0.001);
                        array.Add((byte)((tmpData >> 8) & 0xFF));
                        array.Add((byte)(tmpData & 0xFF));
                        break;
                    }
                case "MANUFACTURE_PRODUCT":
                    {
                        string str = (string)obj;
                        int p1 = int.Parse(str.Substring(0, 1)); // 生產商代碼
                        int p2 = int.Parse(str.Substring(1, 1)); // 產品商代碼
                        array.Add(Convert.ToByte((p1 << 4) | p2));
                        break;
                    }
                case "MANUFACTURE_WEEK":
                    {
                        array.Add(Convert.ToByte(obj));
                        break;
                    }
                case "BATTERY_ID":
                case "CYCLE_COUNT":
                case "MCU_RESET_COUNT":
                    {
                        uint tmpData = (uint)obj;
                        array.Add((byte)((tmpData >> 8) & 0xFF));
                        array.Add((byte)(tmpData & 0xFF));
                        break;
                    }
                case "DEVICE_NAME":
                    {
                        return Encoding.ASCII.GetBytes((string)obj);
                    }
                case "MANUFACTURE_DATE":
                    {
                        Int32[] dt = (Int32[])obj;
                        uint datetimeByte = (uint)(((dt[0] - Utility.DEFAULTYEAR) & 0x7F) << 9);
                        datetimeByte |= (uint)((dt[1] & 0x0F) << 5);
                        datetimeByte |= (uint)(dt[2] & 0x1F);

                        array.Add((byte)((datetimeByte >> 8) & 0xFF));
                        array.Add((byte)(datetimeByte & 0xFF));
                        break;
                    }
                case "EXN_ID":
                    {
                        return (byte[])obj;
                    }
                default:
                    {
                        break;
                    }
            }
            return array.ToArray();
        }
    }
}
