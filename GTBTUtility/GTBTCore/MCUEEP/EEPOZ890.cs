﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTBTCore.MCUEEP
{
    public class EEPOZ890 : EEPAPI
    {
        private static Int32[] ADCTABLE = new int[] { 1763, 1477, 1231, 1019, 843, 695, 573, 473, 392, 325, 270, 225, 189, 158, 134, 113, 96, 82, 70, 60, 52, 45, 39, 34, 30 };
        private static int[] DEGCTABLE = new int[] { -30, -25, -20, -15, -10, -5, 0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90 };

        //private static int[] STDADCTABLE = new int[] { 1753, 1468, 1222, 1012, 837, 689, 569, 469, 388, 322, 268, 223, 187, 157, 132, 112, 95, 81, 69, 60, 51, 44, 39, 34, 29 };
        //private static double PULLUPRESISTOR = 232000.0;
        //private static int[] LOOKUPRESISTORTABLE = new int[] {
        //    111300, 86430, 67770, 53410, 42470,
        //    33900, 27280, 22050, 17960, 14690,
        //    12090, 10000, 8313, 6940, 5827,
        //    4911, 4160, 3536, 3020, 2588,
        //    2228, 1924, 1668, 1451, 1266
        //};
        //private static int[] DEGCTABLE = new int[] {
        //    -30, -25, -20, -15, -10,
        //    -5, 0, 5, 10, 15,
        //    20, 25, 30, 35, 40,
        //    45, 50, 55, 60, 65,
        //    70, 75, 80, 85, 90
        //};
        //private static Int32[] mADCTABLE;
        //private static Int32[] ADCTABLE
        //{
        //    get
        //    {
        //        if (mADCTABLE == null)
        //        {
        //            mADCTABLE = new Int32[25];
        //            for (int i = 0; i < EEPOZ890.LOOKUPRESISTORTABLE.Length; i++)
        //            {
        //                mADCTABLE[i] = Convert.ToInt32((3300.0 * EEPOZ890.LOOKUPRESISTORTABLE[i]) / ((PULLUPRESISTOR + EEPOZ890.LOOKUPRESISTORTABLE[i]) * 0.61035));
        //            }
        //        }
        //        return mADCTABLE;
        //    }
        //}

        public static List<CmdConfig> CMDCONFIGLIST = new List<CmdConfig>()
        {
            new CmdConfig() { Name = "SENSE_RESISTOR", IsHidden = true, IsReadOnly = true, Type = "System.Double", Address = 0x34, Length = 2 },

            new CmdConfig() { Name = "TEMP_OFFSET", IsHidden = true, IsReadOnly = true, Type = "System.Int32", Address = 0x02, Length = 1 },
            new CmdConfig() { Name = "COC_OFFSET", IsHidden = true, IsReadOnly = true, Type = "System.Int32", Address = 0x03, Length = 1 },
            new CmdConfig() { Name = "SC_OFFSET", IsHidden = true, IsReadOnly = true, Type = "System.Int32", Address = 0x03, Length = 1 },
            new CmdConfig() { Name = "DOC_OFFSET", IsHidden = true, IsReadOnly = true, Type = "System.Int32", Address = 0x04, Length = 1 },
            new CmdConfig() { Name = "CELL_OFFSET", IsHidden = true, IsReadOnly = true, Type = "System.Byte[]", Address = 0x05, Length = 13 },
            new CmdConfig() { Name = "GPIO_OFFSET", IsHidden = true, IsReadOnly = true, Type = "System.Byte[]", Address = 0x12, Length = 3 },
            new CmdConfig() { Name = "CURRENT_OFFSET", IsHidden = true, IsReadOnly = true, Type = "System.Int32", Address = 0x16, Length = 2 },

            new CmdConfig() { Name = "PF_RECORD", IsHidden = true, IsReadOnly = true, Type = "System.Byte", Address = 0x18, Length = 1 },

            new CmdConfig() { Name = "ATE_FREEZE", IsHidden = true, IsReadOnly = true, Type = "System.Boolean", Address = 0x25, Length = 1 },

            //new CmdConfig() { Name = "BAT_TYPE", IsReadOnly = true, Type = "Int32", Address = 0x26, Length = 1 },
            new CmdConfig() { Name = "CELL_NUMBER", IsReadOnly = true, Type = "System.Int32", Address = 0x26, Length = 1 },

            new CmdConfig() { Name = "SCAN_RATE", IsHidden = true, IsReadOnly = true, Type = "System.Int32", Address = 0x27, Length = 1 },

            new CmdConfig() { Name = "COC_TH", IsReadOnly = true, Type = "System.Double", Address = 0x28, Length = 1 },
            new CmdConfig() { Name = "DOC_TH", IsReadOnly = true, Type = "System.Double", Address = 0x29, Length = 1 },
            new CmdConfig() { Name = "OC_DELAY", IsReadOnly = true, Type = "System.Int32", Address = 0x2A, Length = 1 },
            new CmdConfig() { Name = "SC_TH", Type = "System.Double", Address = 0x2B, Length = 1 },
            new CmdConfig() { Name = "SC_DELAY", IsReadOnly = true, Type = "System.Int32", Address = 0x2C, Length = 1 },

            new CmdConfig() { Name = "COC_RELEASE", Type = "System.Int32", Address = 0x2D, Length = 1 },
            new CmdConfig() { Name = "DOC_RELEASE", Type = "System.Int32", Address = 0x2D, Length = 1 },

            new CmdConfig() { Name = "ENABLE_IDL_BLD", IsHidden = true, IsReadOnly = true, Type = "System.Boolean", Address = 0x2D, Length = 1 },
            new CmdConfig() { Name = "NO_ER_DSPL", IsHidden = true, IsReadOnly = true, Type = "System.Boolean", Address = 0x2D, Length = 1 },

            new CmdConfig() { Name = "OTUT_DELAY", Type = "System.Int32", Address = 0x2E, Length = 1 },
            new CmdConfig() { Name = "OVUV_DELAY", Type = "System.Int32", Address = 0x2E, Length = 1 },

            //new CmdConfig() { Name = "PF_DELAY", IsReadOnly = true, Type = "Int32", Address = 0x2F, Length = 1 },
            //new CmdConfig() { Name = "PF_ENABLE_MF", IsReadOnly = true, Type = "Boolean", Address = 0x2F, Length = 1 },
            //new CmdConfig() { Name = "PF_ENABLE_VH", IsReadOnly = true, Type = "Boolean", Address = 0x2F, Length = 1 },
            //new CmdConfig() { Name = "PF_ENABLE_VL", IsReadOnly = true, Type = "Boolean", Address = 0x2F, Length = 1 },

            new CmdConfig() { Name = "I2C_ADDRESS", IsHidden = true, IsReadOnly = true,  Type = "System.Int32", Address = 0x30, Length = 1 },
            new CmdConfig() { Name = "ENABLE_PEC", IsHidden = true, IsReadOnly = true,  Type = "System.Boolean", Address = 0x30, Length = 1 },
            new CmdConfig() { Name = "SC_RELEASE", IsHidden = true, IsReadOnly = true,  Type = "System.Int32", Address = 0x30, Length = 1 },

            new CmdConfig() { Name = "WAKEUP_CONTROL", IsHidden = true, IsReadOnly = true,  Type = "System.Int32", Address = 0x31, Length = 1 },
            new CmdConfig() { Name = "ENABLE_T1E", IsHidden = true, IsReadOnly = true, Type = "System.Boolean", Address = 0x32, Length = 1 },
            new CmdConfig() { Name = "ENABLE_T2E", IsHidden = true, IsReadOnly = true, Type = "System.Boolean", Address = 0x32, Length = 1 },
            //new CmdConfig() { Name = "MODE_CONTROL", IsReadOnly = true, Type = "Byte", Address = 0x32, Length = 1 },
            //new CmdConfig() { Name = "HARDWARE_BLEEDING", IsReadOnly = true, Type = "Byte", Address = 0x33, Length = 1 },

            new CmdConfig() { Name = "FACTORY_NAME", Type = "System.String", Address = 0x36, Length = 10 },
            new CmdConfig() { Name = "PROJECT_NAME", Type = "System.String", Address = 0x40, Length = 5 },
            new CmdConfig() { Name = "VERSION_NUMBER", Type = "System.String", Address = 0x45, Length = 1 },

            new CmdConfig() { Name = "CELL_UNBALANCE_TH", IsHidden = true, IsReadOnly = true, Type = "System.Int32", Address = 0x46, Length = 2 },
            //new CmdConfig() { Name = "BLEEDING_START_VOLTAGE", IsReadOnly = true, Type = "Int32", Address = 0x48, Length = 2 },

            new CmdConfig() { Name = "OV_TH", Type = "System.Int32", Address = 0x4A, Length = 2 },
            new CmdConfig() { Name = "OV_RELEASE", Type = "System.Int32", Address = 0x4C, Length = 2 },
            new CmdConfig() { Name = "UV_TH", Type = "System.Int32", Address = 0x4E, Length = 2 },
            new CmdConfig() { Name = "UV_RELEASE", Type = "System.Int32", Address = 0x50, Length = 2 },
            new CmdConfig() { Name = "EXTRE_HIGH_VOLT_TH", Type = "System.Int32", Address = 0x52, Length = 2 },    //
            new CmdConfig() { Name = "EXTRE_LOW_VOLT_TH", Type = "System.Int32", Address = 0x54, Length = 2 },     //

            new CmdConfig() { Name = "OTE_TH", Type = "System.Int32", Address = 0x56, Length = 2 },
            new CmdConfig() { Name = "OTE_RELEASE", Type = "System.Int32", Address = 0x58, Length = 2 },
            new CmdConfig() { Name = "UTE_TH", Type = "System.Int32", Address = 0x5A, Length = 2 },
            new CmdConfig() { Name = "UTE_RELEASE", Type = "System.Int32", Address = 0x5C, Length = 2 },
            new CmdConfig() { Name = "OTI_TH", Type = "System.Int32", Address = 0x5E, Length = 2 },
            new CmdConfig() { Name = "OTI_RELEASE", Type = "System.Int32", Address = 0x60, Length = 2 },
            new CmdConfig() { Name = "UTI_TH", Type = "System.Int32", Address = 0x62, Length = 2 },
            new CmdConfig() { Name = "UTI_RELEASE", Type = "System.Int32", Address = 0x64, Length = 2 },

            //new CmdConfig() { Name = "GAS_GAUGE", IsReadOnly = true, Type = "Int32", Address = 0x66, Length = 10 },
            //new CmdConfig() { Name = "PASSWORD", IsReadOnly = true, Type = "Int32", Address = 0x7A, Length = 2 },
            //new CmdConfig() { Name = "AUTHENTICATION_CODE", IsReadOnly = true, Type = "Int32", Address = 0x7C, Length = 2 },
            //new CmdConfig() { Name = "AUTHENTICATION_CONTROL", IsReadOnly = true, Type = "Int32", Address = 0x7F, Length = 1 }
        };

        public static double GetExternalTemperature(double adc)
        {
            int i = 0;
            for (i = 0; i < 25; i += 1)
            {
                if (ADCTABLE[i] <= adc) break;
            }

            if (i == 0 || i == 24) return DEGCTABLE[i];

            int adcInterval = ADCTABLE[i - 1] - ADCTABLE[i];
            int degreeInterval = DEGCTABLE[i] - DEGCTABLE[i - 1];
            double diffValue = ADCTABLE[i - 1] - adc;
            return (((degreeInterval * diffValue) / adcInterval) + DEGCTABLE[i - 1]);
        }

        public static double GetInternalTemperature(double adc, int offset)
        {
            int tmpITV = 550;
            if (offset != 0) tmpITV = offset + 400;

            return ((adc * 0.61035) - tmpITV) / 2.0976 + 30;
        }

        public static Int32 GetExternalADC(double temp)
        {
            int i = 0;
            for (i = 0; i < 25; i += 1)
            {
                if (DEGCTABLE[i] >= temp) break;
            }

            if (i == 0 || i == 24) return ADCTABLE[i];

            int adcInterval = ADCTABLE[i - 1] - ADCTABLE[i];
            int degreeInterval = DEGCTABLE[i] - DEGCTABLE[i - 1];
            double diffValue = DEGCTABLE[i] - temp;
            return (Int32)((ADCTABLE[i] - ((adcInterval * diffValue) / degreeInterval)));
        }
        
        public static Int32 GetInternalADC(double temp, int offset)
        {
            int tmpITV = 550;
            if (offset != 0) tmpITV = offset + 400;

            return Convert.ToInt32((((temp + 30) * 2.0976) + tmpITV) * 1.6384);
        }
        
        public override List<CmdConfig> GetCmdConfigList()
        {
            return CMDCONFIGLIST;
        }
        
        public override Dictionary<string, object> ConvertCmdArrayToObj(string name, byte[] eep, Dictionary<string, object> obj)
        {
            if (obj == null) obj = new Dictionary<string, object>();

            CmdConfig config = GetCmdConfig(name);
            byte[] data = EEPAPI.GetCmdConfigEEPArray(config, eep);
            if (data.Length == 0) return obj;

            switch (config.Name)
            {
                case "SENSE_RESISTOR":
                    {
                        obj.Add(config.Name, (Double)(((data[1] << 8) | data[0]) * 0.001));
                        break;
                    }
                case "TEMP_OFFSET":
                    {
                        obj.Add(config.Name, (Int32)(data[0]));
                        break;
                    }
                case "CELL_OFFSET":
                case "GPIO_OFFSET":
                    {
                        obj.Add(config.Name, data);
                        break;
                    }
                case "SC_OFFSET":
                    {
                        Int32 tmp = data[0] & 0x0F;
                        if ((tmp & 0x08) != 0)
                        {
                            tmp = ~tmp;
                            tmp += 1;
                            tmp &= tmp;
                            tmp = 0 - tmp;
                        }
                        obj.Add(config.Name, tmp);
                        break;
                    }
                case "COC_OFFSET":
                case "DOC_OFFSET":
                    {
                        Int32 tmp = ((data[0] >> 0x04) & 0x0F);
                        if ((tmp & 0x08) != 0)
                        {
                            tmp = ~tmp;
                            tmp += 1;
                            tmp &= tmp;
                            tmp = 0 - tmp;
                        }
                        obj.Add(config.Name, tmp);
                        break;
                    }
                case "CURRENT_OFFSET":
                    {
                        obj.Add(config.Name, (Int32)((data[1] << 0x08) | data[0]));
                        break;
                    }
                case "ATE_FREEZE":
                    {
                        obj.Add(config.Name, (Boolean)(((data[0] >> 0x07) & 0x01) != 0));
                        break;
                    }
                case "CELL_NUMBER":
                case "WAKEUP_CONTROL":
                    {
                        obj.Add(config.Name, (Int32)(data[0] & 0x0F));
                        break;
                    }
                case "SCAN_RATE":
                    {
                        Int32 tmp = ((data[0] >> 0x04) & 0x07) ;
                        obj.Add(config.Name, (tmp == 0) ? 1 : tmp);
                        break;
                    }
                case "COC_TH":
                    {
                        if (!obj.ContainsKey("COC_OFFSET") || !obj.ContainsKey("SENSE_RESISTOR"))
                        {
                            ConvertCmdArrayToObj("COC_OFFSET", eep, obj);
                            ConvertCmdArrayToObj("SENSE_RESISTOR", eep, obj);
                        }
                        Int32 SCC = (data[0] & 0x3F);
                        Int32 OFFSET = (Int32)obj["COC_OFFSET"];
                        Double SENSE_RESISTOR = ((Double)obj["SENSE_RESISTOR"]);
                        Double TH = Math.Round(Math.Floor((((((data[0] & 0x1F) + OFFSET) - 4) * 5) / SENSE_RESISTOR) * 10) * 0.1, 2);
                        obj.Add(config.Name, TH);
                        break;
                    }
                case "DOC_TH":
                    {
                        if (!obj.ContainsKey("DOC_OFFSET") || !obj.ContainsKey("SENSE_RESISTOR"))
                        {
                            ConvertCmdArrayToObj("DOC_OFFSET", eep, obj);
                            ConvertCmdArrayToObj("SENSE_RESISTOR", eep, obj);
                        }
                        Int32 OFFSET = (Int32)obj["DOC_OFFSET"];
                        Double SENSE_RESISTOR = (Double)obj["SENSE_RESISTOR"];
                        Double TH = Math.Round(Math.Floor(((((data[0] & 0x3F) + OFFSET) * -5) / SENSE_RESISTOR) * 10) * 0.1, 2);
                        obj.Add(config.Name, TH);
                        break;
                    }
                case "OC_DELAY":
                    {
                        obj.Add(config.Name, (Int32)((0x004 << (data[0] & 0x07)) - 2) * (((data[0] >> 0x03) & 0x1F) + 1));
                        break;
                    }
                case "SC_TH":
                    {
                        if (!obj.ContainsKey("SC_OFFSET") || !obj.ContainsKey("SENSE_RESISTOR"))
                        {
                            ConvertCmdArrayToObj("SC_OFFSET", eep, obj);
                            ConvertCmdArrayToObj("SENSE_RESISTOR", eep, obj);
                        }
                        Int32 OFFSET = (Int32)obj["SC_OFFSET"];
                        Double SENSE_RESISTOR = (Double)obj["SENSE_RESISTOR"];
                        Double TH = Math.Round(Math.Floor((((((data[0] & 0x3F) + OFFSET) + 2) * 10) / SENSE_RESISTOR) * 10) * 0.1, 2);
                        obj.Add(config.Name, TH);
                        break;
                    }
                case "SC_DELAY":
                    {
                        obj.Add(config.Name, (Int32)(0x008 * (1 << (data[0] & 0x07))) * (((data[0] >> 0x03) & 0x1F) + 1));
                        break;
                    }
                case "COC_RELEASE":
                    {
                        Int32 tmp = (data[0] & 0x07);
                        if (tmp == 0) tmp = 1;
                        else if (tmp == 1) tmp = 1;
                        else if (tmp == 2) tmp = 2;
                        else if (tmp == 3) tmp = 4;
                        else if (tmp == 4) tmp = 8;
                        else if (tmp == 5) tmp = 16;
                        else if (tmp == 6) tmp = 24;
                        else if (tmp == 7) tmp = 32;

                        obj.Add(config.Name, tmp);
                        break;
                    }
                case "DOC_RELEASE":
                    {
                        Int32 tmp = ((data[0] >> 3) & 0x07);
                        if (tmp == 0) tmp = 0;
                        else if (tmp == 1) tmp = 1;
                        else if (tmp == 2) tmp = 2;
                        else if (tmp == 3) tmp = 4;
                        else if (tmp == 4) tmp = 8;
                        else if (tmp == 5) tmp = 16;
                        else if (tmp == 6) tmp = 24;
                        else if (tmp == 7) tmp = 32;

                        obj.Add(config.Name, tmp);
                        break;
                    }
                case "ENABLE_IDL_BLD":
                case "ENABLE_T1E":
                    {
                        obj.Add(config.Name, (Boolean)(((data[0] >> 6) & 0x01) != 0));
                        break;
                    }
                case "NO_ER_DSPL":
                case "ENABLE_T2E":
                    {
                        obj.Add(config.Name, (Boolean)(((data[0] >> 7) & 0x01) != 0));
                        break;
                    }
                case "OTUT_DELAY":
                    {
                        obj.Add(config.Name, (Int32)((((data[0] >> 4) & 0x0F) + 1)));
                        break;
                    }
                case "OVUV_DELAY":
                    {
                        obj.Add(config.Name, (Int32)((data[0] & 0x0F) + 1));
                        break;
                    }
                case "I2C_ADDRESS":
                    {
                        obj.Add(config.Name, (Int32)(0x60 + (2 * (data[0] & 0x0F))));
                        break;
                    }
                case "ENABLE_PEC":
                    {
                        obj.Add(config.Name, (Boolean)(((data[0] >> 4) & 0x01) == 1));
                        break;
                    }
                case "SC_RELEASE":
                    {
                        obj.Add(config.Name, (Int32)((((data[0] >> 5) & 0x07) * 15)));
                        break;
                    }
                case "FACTORY_NAME":
                case "PROJECT_NAME":
                    {
                        String str = "";
                        for (int i = 0; i < data.Length; i++)
                            str += String.Format("{0}", (char)data[i]).ToString().Trim('\0');
                        obj.Add(config.Name, str.Trim());
                        break;
                    }
                case "VERSION_NUMBER":
                    {
                        char c = Convert.ToChar(65 + (data[0] >> 0x04));
                        obj.Add(config.Name, string.Format("{0}{1:00}", c, (data[0] & 0x0F)));
                        break;
                    }
                case "CELL_UNBALANCE_TH":
                case "OV_TH":
                case "OV_RELEASE":
                case "UV_TH":
                case "UV_RELEASE":
                case "EXTRE_HIGH_VOLT_TH":
                case "EXTRE_LOW_VOLT_TH":
                    {
                        Int32 tmp = Convert.ToInt32(((data[1] << 5) | (data[0] >> 3)) * 1.2207);
                        obj.Add(config.Name, tmp);
                        break;
                    }
                case "OTE_TH":
                case "OTE_RELEASE":
                case "UTE_TH":
                case "UTE_RELEASE":
                    {
                        Int32 temperature = Convert.ToInt32(EEPOZ890.GetExternalTemperature(((data[1] << 5) | (data[0] >> 3))));
                        obj.Add(config.Name, temperature);
                        break;
                    }
                case "OTI_TH":
                case "OTI_RELEASE":
                case "UTI_TH":
                case "UTI_RELEASE":
                    {
                        if (!obj.ContainsKey("TEMP_OFFSET"))
                        {
                            ConvertCmdArrayToObj("TEMP_OFFSET", eep, obj);
                        }

                        Int32 temperature = Convert.ToInt32(EEPOZ890.GetInternalTemperature(((data[1] << 5) | (data[0] >> 3)), (Int32)obj["TEMP_OFFSET"]));
                        obj.Add(config.Name, temperature);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            return obj;
        }

        public override byte[] ConvertObjToCmdArray(string name, object obj, byte[] eep)
        {
            List<byte> array = new List<byte>();

            switch (name)
            {
                case "COC_TH":
                    {
                        if (eep == null) return array.ToArray();
                        Dictionary<string, object> paramData = new Dictionary<string, object>();
                        ConvertCmdArrayToObj("COC_OFFSET", eep, paramData);
                        ConvertCmdArrayToObj("SENSE_RESISTOR", eep, paramData);
                        if (!paramData.ContainsKey("COC_OFFSET") || !paramData.ContainsKey("SENSE_RESISTOR")) return array.ToArray();

                        Int32 OFFSET = (Int32)paramData["COC_OFFSET"];
                        Double SENSE_RESISTOR = (Double)paramData["SENSE_RESISTOR"];

                        array.Add(Convert.ToByte(Convert.ToInt64((((((Double)obj) * SENSE_RESISTOR) / 5) + 4) - OFFSET) & 0x1F));
                        break;
                    }
                case "DOC_TH":
                    {
                        if (eep == null) return array.ToArray();
                        Dictionary<string, object> paramData = new Dictionary<string, object>();
                        ConvertCmdArrayToObj("DOC_OFFSET", eep, paramData);
                        ConvertCmdArrayToObj("SENSE_RESISTOR", eep, paramData);
                        if (!paramData.ContainsKey("DOC_OFFSET") || !paramData.ContainsKey("SENSE_RESISTOR")) return array.ToArray();

                        Int32 OFFSET = (Int32)paramData["DOC_OFFSET"];
                        Double SENSE_RESISTOR = (Double)paramData["SENSE_RESISTOR"];

                        array.Add(Convert.ToByte(Convert.ToInt64((((((Double)obj) * SENSE_RESISTOR)) + 5) - OFFSET) & 0x3F));
                        break;
                    }
                case "OC_DELAY":
                    {
                        break;
                    }
                case "SC_TH":
                    {
                        if (eep == null) return array.ToArray();

                        Dictionary<string, object> paramData = new Dictionary<string, object>();
                        ConvertCmdArrayToObj("SC_OFFSET", eep, paramData);
                        ConvertCmdArrayToObj("SENSE_RESISTOR", eep, paramData);
                        if (!paramData.ContainsKey("SC_OFFSET") || !paramData.ContainsKey("SENSE_RESISTOR")) return array.ToArray();

                        Int32 OFFSET = (Int32)paramData["SC_OFFSET"];
                        Double SENSE_RESISTOR = (Double)paramData["SENSE_RESISTOR"];

                        array.Add(Convert.ToByte(Convert.ToInt64(((((Double)obj) * SENSE_RESISTOR) - 2) - OFFSET) & 0x3F));
                        break;
                    }
                case "SC_DELAY":
                    {
                        break;
                    }
                case "COC_RELEASE":
                case "DOC_RELEASE":
                    {
                        if (eep == null) return array.ToArray();

                        CmdConfig config = GetCmdConfig(name);
                        byte[] tmpArray = EEPAPI.GetCmdConfigEEPArray(config, eep);
                        if (tmpArray.Length == 0) return array.ToArray();

                        Int32 tmp = (Int32)obj;
                        if (tmp == 0) tmp = 0;
                        else if (tmp == 1) tmp = 1;
                        else if (tmp == 2) tmp = 2;
                        else if (tmp == 4) tmp = 3;
                        else if (tmp == 8) tmp = 4;
                        else if (tmp == 16) tmp = 5;
                        else if (tmp == 24) tmp = 6;
                        else if (tmp == 32) tmp = 7;

                        if (name == "COC_RELEASE")
                        {
                            byte tmpVal = Convert.ToByte((tmp & 0x07));
                            array.Add((byte)((tmpArray[0] & 0xF8) | tmpVal));
                        }
                        else
                        {
                            byte tmpVal = Convert.ToByte((tmp & 0x07) << 0x03);
                            array.Add((byte)((tmpArray[0] & 0xC7) | tmpVal));
                        }
                        break;
                    }
                case "OTUT_DELAY":
                case "OVUV_DELAY":
                    {
                        if (eep == null) return array.ToArray();

                        CmdConfig config = GetCmdConfig(name);
                        byte[] tmpArray = EEPAPI.GetCmdConfigEEPArray(config, eep);
                        if (tmpArray.Length == 0) return array.ToArray();

                        if (name == "OTUT_DELAY")
                        {
                            byte tmpVal = Convert.ToByte((((Int32)obj - 1) & 0x0F) << 0x04);
                            array.Add((byte)((tmpArray[0] & 0x0F) | tmpVal));
                        }
                        else
                        {
                            byte tmpVal = Convert.ToByte((((Int32)obj - 1) & 0x0F));
                            array.Add((byte)((tmpArray[0] & 0xF0) | tmpVal));
                        }
                        break;
                    }
                case "SC_RELEASE":
                    {
                        if (eep == null) return array.ToArray();

                        CmdConfig config = GetCmdConfig(name);
                        byte[] tmpArray = EEPAPI.GetCmdConfigEEPArray(config, eep);
                        if (tmpArray.Length == 0) return array.ToArray();

                        byte tmpVal = Convert.ToByte((((Int32)obj / 15) & 0x07) << 5);
                        array.Add((byte)((tmpArray[0] & 0x07) | tmpVal));
                        break;
                    }
                case "FACTORY_NAME":
                case "PROJECT_NAME":
                    {
                        byte[] tmp = Encoding.ASCII.GetBytes((String)obj);
                        for (int i = 0; i < 10; i++)
                        {
                            if (i < tmp.Length)
                            {
                                array.Add(tmp[i]);
                            }
                            else
                            {
                                array.Add(0);
                            }
                        }
                        break;
                    }
                case "VERSION_NUMBER":
                    {
                        string str = obj.ToString();
                        byte aChar = (byte)(Convert.ToByte(str.Substring(0, 1), 16) - 10);
                        byte bChar = Convert.ToByte(str.Substring(1, 2), 16);
                        array.Add((byte)((aChar << 0x04) | (bChar & 0x0F)));
                        break;
                    }
                case "CELL_UNBALANCE_TH":
                case "OV_TH":
                case "OV_RELEASE":
                case "UV_TH":
                case "UV_RELEASE":
                case "EXTRE_HIGH_VOLT_TH":
                case "EXTRE_LOW_VOLT_TH":
                    {
                        Int32 tmp = Convert.ToInt32(((Int32)obj) * 0.8192);
                        array.Add((byte)((tmp << 3) & 0xF8));
                        array.Add((byte)((tmp >> 5) & 0xFF));
                        break;
                    }
                case "OTE_TH":
                case "OTE_RELEASE":
                case "UTE_TH":
                case "UTE_RELEASE":
                    {
                        Int32 adc = EEPOZ890.GetExternalADC(((Int32)obj));
                        array.Add((byte)((adc << 3) & 0xF8));
                        array.Add((byte)((adc >> 5) & 0xFF));
                        break;
                    }
                case "OTI_TH":
                case "OTI_RELEASE":
                case "UTI_TH":
                case "UTI_RELEASE":
                    {
                        if (eep == null) return array.ToArray();
                        Dictionary<string, object> paramData = new Dictionary<string, object>();
                        ConvertCmdArrayToObj("TEMP_OFFSET", eep, paramData);
                        if (!paramData.ContainsKey("TEMP_OFFSET")) return array.ToArray();

                        Int32 adc = EEPOZ890.GetInternalADC(((Int32)obj), (Int32)paramData["TEMP_OFFSET"]);
                        array.Add((byte)((adc << 3) & 0xF8));
                        array.Add((byte)((adc >> 5) & 0xFF));
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            return array.ToArray();
        }
    }
}
