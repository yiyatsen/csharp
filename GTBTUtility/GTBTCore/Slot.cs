﻿using GTBTCore.Cipher;
using GTBTCore.MCUEEP;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using static GTBTCore.BatCore;

namespace GTBTCore
{
    public class Slot
    {
        public delegate void SlotEventHandler(int slotId, string name, string[] notifyList, BatInfo message);

        public enum SlotSendState : Int32
        {
            None = 0,
            SendError = 1,
            EEPError = 2
        }

        public class SlotOptions
        {
            public string Error { get; set; }
            public bool EnableCANCmd { get; set; }
            public bool EnableSkip { get; set; }
            public bool EnableRecvTimeout { get; set; }
            public int SkipTime { get; set; }
            public Dictionary<string, object> BatInfo { get; set; }
            public List<BatOptions> List { get; set; }

            public SlotOptions()
            {
                List = new List<BatOptions>();
            }
        }

        public class AutoSetting
        {
            public bool isAutoConn { get; set; }
            public bool enableCANCmd { get; set; }
            public bool enableSchedule { get; set; }
            public bool isBatteryIn { get; set; }
            public bool isLogMode { get; set; }
            public bool isAutoLog { get; set; }
            public bool isDetectedCmd { get; set; }
            public bool disableRefreshEEP { get; set; }
            public bool disableRefreshEEPFlag { get; set; }
            public bool enableLostBattery { get; set; }
            public int retryTime { get; set; }
            public int timeout { get; set; }
            public string action { get; set; }

            public AutoSetting()
            {
                timeout = 500;
                action = "none";
                enableLostBattery = false;
            }
        }

        public class CmdSetting
        {
            public bool enableASCII { get; set; }
            public bool enableCipher { get; set; }

            public CmdSetting()
            {
                enableASCII = true;
                enableCipher = false;
            }
        }

        public class SlotInfo
        {
            public int slotId { get; set; }
            public string path { get; set; }
            public int baudRate { get; set; }
            public string commMode { get; set; }

            public StationInfo stationInfo { get; set; }
            public AutoSetting autoSetting { get; set; }
            public CmdSetting cmdSetting { get; set; }
            public BatInfo batInfo { get; set; }

            public SlotInfo()
            {
                slotId = 1;
                commMode = "1To1";
                stationInfo = new StationInfo();
                autoSetting = new AutoSetting();
                cmdSetting = new CmdSetting();
                batInfo = new BatInfo();
            }
        }

        public static string[] BASICBATINFOLIST = new string[] { "DESIGN_CAPACITY", "DESIGN_VOLTAGE", "DEVICE_NAME" };
        public static string[] BASICBATINFOLIST2 = new string[] { "FULL_CHARGE_CAPACITY", "CYCLE_COUNT", "MANUFACTURE_DATE" };
        //public static string[] BATTERYIDLIST = new string[] { "BATTERY_ID", "MANUFACTURE_WEEK", "MANUFACTURE_PRODUCT" };
        //public static string[] PROFILEINFOLIST = new string[] {
        //    "RTC", "PROTECTION", "SOC", "TOTAL_VOLT", "MIN_VOLT", "MAX_VOLT", "DELTA_VOLT", "CELL_NUM", "CELL_VOLT",
        //  "TEMPERATURE", "CELL_TEMPERATURE", "CURRENT", "REMAIN_CAPACITY", "LAST_CHARGE", "LAST_DISCHARGE"
        //};
        public static int AUTORUNTIMER = 1000;  //  ms
        private static int REFRESHEEPTIMEOUT = 60000;
        private static int REPORTTIMEOUT = 3000;
        private static int MAXRETRYTIME = 3;

        public string path { get { return mCOMPort.Port; } }
        public int baudRate { get { return mCOMPort.BaudRate; } }
        public bool isOpen { get { return mCOMPort.IsOpen; } }
        public int slotId { get; set; }
        public Dictionary<string, object> batInfo { get; set; }

        public AutoSetting autoSetting { get; set; }
        public CmdSetting cmdSetting { get; set; }

        private List<BatInfo> mLastBatInfo = new List<BatInfo>();
        private Dictionary<string, byte[]> mMCU_EEP = new Dictionary<string, byte[]>();
        private Dictionary<string, byte[]> mLOG_CHARGE = new Dictionary<string, byte[]>();
        private List<BatOptions> mUserCmdList = new List<BatOptions>();
        private COMPort mCOMPort = new COMPort();
        private BatCore mBatCore = new BatCore();
        private Dictionary<string, object> mUserBatInfo { get; set; }

        private CancellationTokenSource mRefreshEEPCTS { get; set; }
        private CancellationTokenSource mAutoRunCTS { get; set; }
        private CancellationTokenSource mReportCTS { get; set; }

        private SlotNotify mSlotNotify { get; set; }
        public event SlotEventHandler SlotEvent;

        //public string SaveLogFile(BatInfo bat)
        //{
        //    string logTitle = "index,timestamp,interval,SOH,SOC,CURRENT,FULL_CHARGE_CAPACITY,REMAIN_CAPACITY,";
        //    logTitle += "CYCLE_COUNT,TOTAL_VOLT,MAX_VOLT,MIN_VOLT,DELTA_VOLT,TEMPERATURE,MAX_TEMPERATURE,MIN_TEMPERATURE,DELTA_TEMPERATURE";
        //    if (bat.CELL_VOLT != null)
        //    {
        //        for (int i = 0; i < bat.CELL_VOLT.Length; i++)
        //        {
        //            logTitle += string.Format(",Cell {0}", (i + 1).ToString());
        //        }
        //    }
        //    if (bat.CELL_TEMPERATURE != null)
        //    {
        //        for (int i = 0; i < bat.CELL_TEMPERATURE.Length; i++)
        //        {
        //            logTitle += string.Format(", Temp {0}", (i + 1).ToString());
        //        }
        //    }
        //    logTitle += ",ov,uv,coc,doc,ot,ut,sc\r\n";


        //    string log = string.Format("{0},{1},{2},{3},{4},", "index", batInfo.timestamp, ("interval"), batInfo.SOH, batInfo.SOC);
        //    log += string.Format("{0},{1},{2},{3},{4},", batInfo.CURRENT, batInfo.FULL_CHARGE_CAPACITY, batInfo.REMAIN_CAPACITY, batInfo.CYCLE_COUNT);
        //    log += string.Format("{0},{1},{2},{3},{4},", batInfo.TOTAL_VOLT, batInfo.MAX_VOLT, batInfo.MIN_VOLT, batInfo.DELTA_VOLT);
        //    log += string.Format("{0},{1},{2},{3},{4},", batInfo.TEMPERATURE, batInfo.MAX_TEMPERATURE, batInfo.MIN_TEMPERATURE, batInfo.DELTA_TEMPERATURE);
        //    if (batInfo.CELL_VOLT != null)
        //    {
        //        for (int i = 0; i < batInfo.CELL_VOLT.Length; i++)
        //        {
        //            log += string.Format(",{0}", batInfo.CELL_VOLT[i]);
        //        }
        //    }
        //    if (batInfo.CELL_TEMPERATURE != null)
        //    {
        //        for (int i = 0; i < batInfo.CELL_TEMPERATURE.Length; i++)
        //        {
        //            log += string.Format(",{0}", batInfo.CELL_TEMPERATURE[i]);
        //        }
        //    }
        //}


        public Slot()
        {
            autoSetting = new AutoSetting();
            cmdSetting = new CmdSetting();
            batInfo = new Dictionary<string, object>();

            mSlotNotify = new SlotNotify();
        }

        public void SetCOMPort(COMPort comPort)
        {
            mCOMPort = comPort;
        }

        public int ConnectCOMPort(string port, int baudRate)
        {
            return mCOMPort.Connect(port, baudRate);
        }

        public void DisonnectCOMPort()
        {
            mCOMPort.Disconnect();
        }

        //public void SetUserBatInfo(Dictionary<string, object> userBatInfo)
        //{
        //    mUserBatInfo = userBatInfo;
        //}

        //public Dictionary<string, object> GetUserBatInfo()
        //{
        //    return mUserBatInfo;
        //}

        public BatInfo GetBatInfo()
        {
            return BatCore.GetBatInfo(this.batInfo);
        }

        /// <summary>
        /// 儲存目前的電池資訊
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public string SaveLog(DateTime dt)
        {
            BatInfo bat = BatCore.GetBatInfo(this.batInfo);
            string result = BatCore.SaveLog(dt, bat);
            
            SlotInfo slotInfo = new SlotInfo() {
                batInfo = bat,
                slotId = this.slotId,
                path = this.path,
                baudRate = this.baudRate,
                autoSetting = this.autoSetting,
                cmdSetting = this.cmdSetting,
            };
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (JsonWriter jw = new JsonTextWriter(sw))
            {
                jw.Formatting = Formatting.Indented;

                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(jw, slotInfo);
            }

            string fileName = bat.batteryType + "_" + bat.batteryId + "_" + dt.ToString("yyyyMMddHHmmss");

            CipherAESNeil mEncryptionAlog = new CipherAESNeil();
            mEncryptionAlog.SetEncryptionKey(fileName);
            result = result + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.UTF8.GetBytes(sb.ToString()))) + Environment.NewLine;
            result += "!===============================================================";
            return result;
        }
        /// <summary>
        /// 設定MCU之RTC
        /// </summary>
        /// <param name="rtcValue"></param>
        /// <returns></returns>
        public async Task<string> SetRTC(string rtcValue)
        {
            Dictionary<string, object> tmpBatInfo = new Dictionary<string, object>();
            List<BatOptions> tmpBatOptsList = new List<BatOptions>();
            CmdConfig cmdConfig = new CmdConfig() { Type = "System.DateTime", Value = rtcValue };
            DateTime obj = (DateTime)Utility.ConvertStringToObj(cmdConfig.Type, cmdConfig.Value);

            if (!this.cmdSetting.enableASCII) tmpBatOptsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "eng" });

            tmpBatOptsList.Add(new BatOptions() { Action = "set", CmdName = "RTC", SetCmd = 0x05, SetPara = (byte)obj.Second });
            tmpBatOptsList.Add(new BatOptions() { Action = "set", CmdName = "RTC", SetCmd = 0x04, SetPara = (byte)obj.Minute });
            tmpBatOptsList.Add(new BatOptions() { Action = "set", CmdName = "RTC", SetCmd = 0x03, SetPara = (byte)obj.Hour });
            tmpBatOptsList.Add(new BatOptions() { Action = "set", CmdName = "RTC", SetCmd = 0x02, SetPara = (byte)obj.Day });
            tmpBatOptsList.Add(new BatOptions() { Action = "set", CmdName = "RTC", SetCmd = 0x01, SetPara = (byte)obj.Month });
            tmpBatOptsList.Add(new BatOptions() { Action = "set", CmdName = "RTC", SetCmd = 0x00, SetPara = (byte)(obj.Year - Utility.DEFAULTYEAR) });

            if (!this.cmdSetting.enableASCII) tmpBatOptsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "nor" });

            var result = await UserMsg(tmpBatOptsList, tmpBatInfo);

            return (result.Error != null) ? "Setting Fail" : "Success";
        }
        /// <summary>
        /// 設定電池之序號
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public async Task<string> SetSerialNumber(string str)
        {
            Regex regex = new Regex(@"^\d{10}$");

            if (!regex.IsMatch(str)) return "String is not a valid Serial Number";

            int year = Utility.DEFAULTYEAR + int.Parse(str.Substring(2, 2));
            int week = int.Parse(str.Substring(4, 2));
            Dictionary<string, object> tmpBatInfo = new Dictionary<string, object>();
            List<BatOptions> tmpBatOptsList = new List<BatOptions>();

            if (!this.cmdSetting.enableASCII) tmpBatOptsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "eng" });

            mBatCore.GetBatOptionsListForCBSettingValue("MANUFACTURE_PRODUCT", str.Substring(0, 2), tmpBatOptsList, null);
            mBatCore.GetBatOptionsListForCBSettingValue("MANUFACTURE_DATE", Utility.ISO8601WeekNumToThursday(year, week), tmpBatOptsList, null);
            mBatCore.GetBatOptionsListForCBSettingValue("MANUFACTURE_WEEK", week, tmpBatOptsList, null);
            mBatCore.GetBatOptionsListForCBSettingValue("BATTERY_ID", UInt32.Parse(str.Substring(6, 4)), tmpBatOptsList, null);

            if (!this.cmdSetting.enableASCII) tmpBatOptsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "nor" });

            var result = await UserMsg(tmpBatOptsList, tmpBatInfo);

            return (result.Error != null) ? "Setting Fail" : "Success";
        }
        /// <summary>
        /// 設定CB EEP之參數值(ex. PIC18 MCU)
        /// </summary>
        /// <param name="name"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task<string> SetCBEEPCmd(string name, object obj)
        {
            List<BatOptions> optsList = new List<BatOptions>();
            if (!this.cmdSetting.enableASCII) optsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "eng" });
            mBatCore.GetBatOptionsListForCBSettingValue(name, obj, optsList, null);
            if (!this.cmdSetting.enableASCII) optsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "nor" });

            var result = await UserMsg(optsList, new Dictionary<string, object>());
            return (result.Error != null) ? "Setting Fail" : "Success";
        }
        /// <summary>
        /// 設定PB EEP之參數值(ex. OZ890)
        /// PS. 全部更新完後需要重新讀取PB之資訊, 避免參數值不正確
        /// </summary>
        /// <param name="name"></param>
        /// <param name="obj"></param>
        /// <param name="pbID"></param>
        /// <returns></returns>
        public async Task<string> SetPBEEPCmd(string name, object obj, string pbID)
        {
            if (!(this.batInfo.ContainsKey("PB_EEP") && ((Dictionary<string, byte[]>)this.batInfo["PB_EEP"]).ContainsKey(pbID))) return "Not PB EEP Data";

            byte[] eep = ((Dictionary<string, byte[]>)this.batInfo["PB_EEP"])[pbID];

            List<BatOptions> optsList = new List<BatOptions>();
            if (!this.cmdSetting.enableASCII) optsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "eng" });
            mBatCore.GetBatOptionsListForPBSettingValue(name, obj, optsList, eep, pbID);
            if (!this.cmdSetting.enableASCII) optsList.Add(new BatOptions() { Action = "get", CmdName = "SWITCH_MODE", SetPara = "nor" });

            var result = await UserMsg(optsList, new Dictionary<string, object>());
            
            return (result.Error != null) ? "Setting Fail" : "Success";
        }
        /// <summary>
        /// 轉換通訊板上的EEP Array成為對應的屬性資訊
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> GetCBEEPToObj()
        {
            if (!(this.batInfo.ContainsKey("MCU_EEP"))) return null;
            
            Dictionary<string, object> obj = new Dictionary<string, object>();
            mBatCore.GetCBEEPToObj((byte[])this.batInfo["MCU_EEP"], obj, null);

            return obj;
        }
        /// <summary>
        /// 轉換保護板上的EEP Array成為對應的屬性資訊
        /// </summary>
        /// <param name="pbID">保護板ID</param>
        /// <returns></returns>
        public Dictionary<string, object> GetPBEEPToObj(string pbID)
        {
            if (!(this.batInfo.ContainsKey("PB_EEP") && (((Dictionary<string, byte[]>)this.batInfo["PB_EEP"]).ContainsKey(pbID)))) return null;

            byte[] eep = ((Dictionary<string, byte[]>)this.batInfo["PB_EEP"])[pbID];

            Dictionary<string, object> obj = new Dictionary<string, object>();
            mBatCore.GetPBEEPToObj(eep, obj, null);

            return obj;
        }

        private async Task<Int32> GetCBEEPIndex()
        {
            var result = new BatOptions() { EnableRecv = true, Action = "get", CmdName = "MCU_EEP" };
            try
            {
                result = await this.SendMsg(result);
            }
            catch (Exception ex)
            {
                result.Error = string.Format("GetMCUEEPIndex Error({0})", ex.Message);
            }
            if (result.Result != null && result.Result.ContainsKey("MCU_EEP_INDEX"))
            {
                return (Int32)(result.Result["MCU_EEP_INDEX"]);
            }
            else
            {
                return 0;
            }
        }

        public async Task<string> ConnectGTBattery(string port)
        {
            string result = await this.ConnectBattery(port);
            if (result == "Success")
            {
                int i = 0;
                while (!this.autoSetting.isBatteryIn)
                {
                    await this.AutoMsg();

                    if (!this.autoSetting.isBatteryIn)
                    {
                        if (i > 1)
                        {
                            this.ResetSlotState();
                            return "Not a valid GT Battery";
                        }
                        i++;
                        await Task.Delay(1000);
                        await this.CheckBatExist(null);
                    }
                }
            }

            return result;
        }

        private async Task<string> ConnectBattery(string port)
        {
            if (port == "") return "Please check COM Port";

            try
            {
                for (int i = 0; i < 2; i++)
                {
                    foreach (var item in COMPort.DEFAULTBAUDRATEINFOLIST)
                    {
                        this.cmdSetting.enableASCII = item.EnableASCII;
                        int result = this.ConnectCOMPort(port, item.BaudRate);
                        if (result == 0)
                        {
                            await this.CheckBatExist(null);

                            if (this.autoSetting.isDetectedCmd) return "Success";
                            this.DisonnectCOMPort();
                        }
                        await Task.Delay(50);
                    }
                }
            }
            catch (Exception ex)
            {
                return string.Format("{0} connect Error({1})", port, ex.Message);
            }

            return string.Format("{0} not found GT Battery", port);
        }
        
        public int ResetSlotState()
        {
            this.StopReport();

            this.StopAutoRun();
            this.DisonnectCOMPort();
            
            this.mSlotNotify.Reset(); //  重置通知事件

            this.ClearEEPTimer();
            this.autoSetting = new AutoSetting();

            return 0;
        }

        private async Task<int> RecheckBatteryInitState()
        {
            this.ClearEEPTimer();
            this.ResetRefreshEEPTimer(REFRESHEEPTIMEOUT);
            this.autoSetting.retryTime = 0;
            if (!this.autoSetting.isBatteryIn)
            {
                this.autoSetting.isBatteryIn = true;
                this.autoSetting.enableSchedule = false;
                this.mLastBatInfo.Clear();

                BatInfo tmpBatInfo = GetBatInfo();
                this.mLastBatInfo.Add(tmpBatInfo);
                //  偵測電池事件
                TriggerSlotEvent("detectBatteryIn", new string[0], tmpBatInfo);

                //  啟動定時回報
                this.StartReport();
            }
            return 0;
        }

        public void NotifyDisconnectBattery()
        {
            this.autoSetting.enableLostBattery = true;
            this.StopReport();

            BatInfo tmpBatInfo = GetBatInfo();
            this.mLastBatInfo.Add(tmpBatInfo);
            if (this.mLastBatInfo.Count > 3) this.mLastBatInfo.RemoveAt(0);

            TriggerSlotEvent("detectBatteryOut", this.mSlotNotify.Check(tmpBatInfo), tmpBatInfo);

            this.mSlotNotify.Reset(); //  重置通知事件
        }

        public void AutoLog()
        {
        }

        private void ClearAutoRun()
        {
            if (mAutoRunCTS != null)
            {
                mAutoRunCTS.Cancel();
                mAutoRunCTS.Dispose();
                 mAutoRunCTS = null;
            }
        }

        private void ClearEEPTimer()
        {
            if (mRefreshEEPCTS != null)
            {
                mRefreshEEPCTS.Cancel();
                mRefreshEEPCTS.Dispose();
                mRefreshEEPCTS = null;
            }
        }

        private async Task<int> ResetRefreshEEPTimer(int timeout)
        {
            mRefreshEEPCTS = new CancellationTokenSource();
            try
            {
                while (true)
                {
                    if (mRefreshEEPCTS == null) break;
                    await Task.Delay(timeout, mRefreshEEPCTS.Token);
                    autoSetting.disableRefreshEEPFlag = false;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
            return 0;
        }

        private void StopReport()
        {
            if (mReportCTS != null)
            {
                mReportCTS.Cancel();
                mReportCTS.Dispose();
                mReportCTS = null;
            }
        }

        private async Task<int> StartReport()
        {
            mReportCTS = new CancellationTokenSource();
            try
            {
                while (true)
                {
                    if (mReportCTS == null) break;
                    await Task.Delay(REPORTTIMEOUT, mReportCTS.Token);
                    BatInfo tmpBatInfo = GetBatInfo();
                    this.mLastBatInfo.Add(tmpBatInfo);
                    if (this.mLastBatInfo.Count > 3) this.mLastBatInfo.RemoveAt(0);
                    // 確認是否有觸發的事件
                    TriggerSlotEvent("reportSlot", this.mSlotNotify.Check(tmpBatInfo), tmpBatInfo);
                }

            }
            catch (Exception ex)
            {
                return -1;
            }
            return 0;
        }
        
        private void TriggerSlotEvent(string name, string[] notifyList, BatInfo data)
        {
            if (SlotEvent != null) SlotEvent(this.slotId, name, notifyList, data);
        }

        public async Task<SlotOptions> GetBatteryCmd(SlotOptions opts)
        {
            var result = opts;
            Dictionary<string, object> btAttr = result.BatInfo;
            if (btAttr == null) btAttr = this.batInfo;

            if (result.List.Count == 0)
            {
                result.Error = "\"opts.List\" is empty";
                return result;
            }

            int errorNum = 0;
            foreach (var item in result.List)
            {
                if (item.Action == "get" && (item.CmdName == "PROTECTION" || item.CmdName == "RTC"))
                {
                    item.SetCmd = this.batInfo.ContainsKey("MCU_VERSION") ? this.batInfo["MCU_VERSION"] : "";
                }

                item.EnableRecvTimeout = result.EnableRecvTimeout;
                BatOptions batOpts = await this.SendMsg(item);
                this.CheckRecevBatAttr(batOpts, result, btAttr);
                await Task.Delay(50);
                if (result.Error != null)
                {
                    errorNum += 1;

                    if (result.EnableSkip && (errorNum > result.SkipTime))
                    {
                        result.Error = string.Format("EnableSkip({0}) ErrorNum({1})", result.EnableSkip, errorNum);
                        break;
                    }

                    result.Error = null;
                }
            }

            if (!result.EnableSkip && errorNum == result.List.Count)
            {
                result.Error = string.Format("EnableSkip({0}) ErrorNum({1})", result.EnableSkip, errorNum);
            }

            return result;
        }

        public async Task<SlotOptions> UserMsg(List<BatOptions> batOpts, Dictionary<string, object> tmpBatInfo)
        {
            var result = new SlotOptions();
            result.List = batOpts;

            result.Error = null;
            result.BatInfo = (tmpBatInfo != null) ? tmpBatInfo : this.batInfo;
            result.EnableSkip = (!this.autoSetting.enableCANCmd);
            result.SkipTime = this.autoSetting.disableRefreshEEP ? 1 : 0;
            return await this.GetBatteryCmd(result);
        }

        public async Task<SlotOptions> AutoMsg()
        {
            var result = new SlotOptions();
            if (autoSetting.isDetectedCmd)
            {
                try
                {
                    DateTime tmpStartDT = DateTime.Now;
                    result.BatInfo = this.batInfo;

                    List<BatOptions> batOpts = (this.autoSetting.enableCANCmd) ?
                        BatCore.GetBatOptionsListForType(BatOptionsCmdType.DetectCAN) :
                        BatCore.GetBatOptionsListForType(BatOptionsCmdType.DynamicInfo);

                    if (!this.autoSetting.isBatteryIn)
                    {
                        if (this.autoSetting.enableCANCmd)
                        {
                            result.List = BatCore.GetBatOptionsListForType(BatOptionsCmdType.BasicInfo).Concat(batOpts).ToList();
                        }
                        else
                        {
                            result.List = BatCore.GetBatOptionsListForType(BatOptionsCmdType.BasicParam)
                                .Concat(BatCore.GetBatOptionsListForType(BatOptionsCmdType.BasicInfo) ).ToList()
                                .Concat(BatCore.GetBatOptionsListForType(BatOptionsCmdType.EEPPB)).ToList();
                            result = await this.GetBatteryCmd(result);
                        }
                    }
                    // 判斷是否更新MCU EEP資料
                    if (!this.autoSetting.disableRefreshEEPFlag)
                    {
                        this.autoSetting.disableRefreshEEPFlag = true;
                        this.autoSetting.disableRefreshEEP = false;
                    }
                    Dictionary<string, object> scanBatInfo = this.batInfo;
                    if (!this.autoSetting.disableRefreshEEP)
                    {
                        batOpts = batOpts.Concat(BatCore.GetBatOptionsListForType(BatOptionsCmdType.RecordLastTime)).ToList();
                        if (this.batInfo.ContainsKey("MCU_VERSION") && (string)this.batInfo["MCU_VERSION"] == "2.50Rev.09")
                        {
                            //  針對舊版4816讀取資料, 並轉換成共用的EEP格式
                            scanBatInfo = new Dictionary<string, object>();
                            result.List = result.List.Concat(BatCore.GetBatOptionsListForType(BatOptionsCmdType.EEP4816)).ToList();
                        }
                        else
                        {
                            int len = await GetCBEEPIndex();

                            if (len == 0)
                            {
                                result.List = BatCore.GetBatOptionsListForType(BatOptionsCmdType.RecordTotalTime);
                                result = await this.GetBatteryCmd(result);
                            }

                            batOpts = batOpts.Concat(BatCore.GetBatOptionsListForCBType(len)).ToList();
                        }
                    }

                    result.Error = null;
                    result.BatInfo = scanBatInfo;
                    result.EnableSkip = (!this.autoSetting.enableCANCmd);
                    result.SkipTime = this.autoSetting.disableRefreshEEP ? 1 : 0;
                    result.List = batOpts;
                    result = await this.GetBatteryCmd(result);

                    if (this.autoSetting.enableCANCmd)
                    {
                        BatOptions batOptions = new BatOptions();
                        batOptions.EnableASCII = true;
                        batOptions.EnableRecv = true;
                        batOptions.FirstDelayTime = (!this.autoSetting.disableRefreshEEP) ? 3000 : 1000;
                        batOptions = await this.RecvMsg(batOptions);

                        this.CheckRecevBatAttr(batOptions, result, batInfo);
                    }

                    SlotSendState errorState = SlotSendState.None;
                    if (result.Error != null)
                    {
                        if (this.autoSetting.isBatteryIn) errorState = SlotSendState.SendError;
                        else this.autoSetting.isDetectedCmd = false;
                    }
                    else
                    {
                        if (!this.autoSetting.disableRefreshEEP)
                        {
                            if (this.batInfo.ContainsKey("MCU_VERSION") && (string)this.batInfo["MCU_VERSION"] == "2.50Rev.09")
                            {
                                this.mMCU_EEP = new Dictionary<string, byte[]>();
                                this.mMCU_EEP.Add("1", mBatCore.GetCBEEP(scanBatInfo));
                            }

                            if (this.mMCU_EEP.Count > 0)
                            {
                                byte[] tmpCBEEP = Utility.ConcatByteArray(this.mMCU_EEP);
                                if (this.mLOG_CHARGE.Count > 0)
                                {
                                    byte[] tmp = null;
                                    byte[] tmpLog = Utility.ConcatByteArray(this.mLOG_CHARGE);
                                    this.mLOG_CHARGE.Clear();

                                    CmdConfig config = mBatCore.GetCBCmdConfig("LOG_CHARGE");

                                    int len = config.Address + tmpLog.Length;
                                    if (len > tmpCBEEP.Length)
                                    {
                                        Array.Resize(ref tmp, len);
                                    } else
                                    {
                                        Array.Resize(ref tmp, tmpCBEEP.Length);
                                    }
                                    Array.Copy(tmpCBEEP, 0, tmp, 0, tmpCBEEP.Length);
                                    Array.Copy(tmpLog, 0, tmp, config.Address, tmpLog.Length);
                                    tmpCBEEP = tmp;
                                }
                                this.mMCU_EEP.Clear();

                                //  啟動比對機制
                                bool isCorrect = !this.batInfo.ContainsKey("MCU_EEP");
                                if (!isCorrect)
                                {
                                    foreach (string name in BASICBATINFOLIST)
                                    {
                                        byte[] before = MCUEEP.EEPAPI.GetCmdConfigEEPArray(mBatCore.GetCBCmdConfig(name), (byte[])this.batInfo["MCU_EEP"]);
                                        byte[] after = MCUEEP.EEPAPI.GetCmdConfigEEPArray(mBatCore.GetCBCmdConfig(name), tmpCBEEP);

                                        isCorrect = Utility.IsMatchOperator("System.String", Utility.ConvertObjToString(before), Utility.ConvertObjToString(after), CmdOperatorType.Equal);
                                        isCorrect = isCorrect && (before.Length > 0 && after.Length > 0);
                                        if (!isCorrect)
                                        {
                                            break;
                                        }
                                    }
                                }

                                if (isCorrect)
                                {
                                    this.autoSetting.disableRefreshEEP = true;

                                    Utility.CopyData(this.batInfo, "MCU_EEP", tmpCBEEP);
                                    Utility.CopyData(this.batInfo, "eepTimestamp", DateTime.UtcNow);
                                    Dictionary<string, object> tmpCBObj = mBatCore.GetCBEEPToObj(tmpCBEEP, null, null);

                                    foreach (string name in BASICBATINFOLIST)
                                    {
                                        if (tmpCBObj.ContainsKey(name)) Utility.CopyData(this.batInfo, name, tmpCBObj[name]);
                                    }
                                    foreach (string name in BASICBATINFOLIST2)
                                    {
                                        if (tmpCBObj.ContainsKey(name)) Utility.CopyData(this.batInfo, name, tmpCBObj[name]);
                                    }

                                    Utility.CopyData(this.batInfo, "SOH", BatCore.GetMappingSOH(tmpCBObj));
                                    Utility.CopyData(this.batInfo, "batteryId", BatCore.GetSerialNumber(tmpCBObj));
                                    Utility.CopyData(this.batInfo, "batteryType", tmpCBObj.ContainsKey("DEVICE_NAME") ? tmpCBObj ["DEVICE_NAME"] : "" );

                                    await this.RecheckBatteryInitState();
                                }
                                else
                                {
                                    errorState = SlotSendState.EEPError;
                                }
                            }
                            else if (this.autoSetting.enableCANCmd)
                            {
                                this.autoSetting.disableRefreshEEP = true;
                                // Not GT Battery
                                Utility.CopyData(this.batInfo, "SOH", BatCore.GetMappingSOH(this.batInfo));
                                await this.RecheckBatteryInitState();
                            }
                        }
                        else
                        {
                            Utility.CopyData(this.batInfo, "SOH", BatCore.GetMappingSOH(this.batInfo));
                        }

                        Utility.CopyData(this.batInfo, "timestamp", DateTime.UtcNow);
                    }

                    switch (errorState)
                    {
                        case SlotSendState.None:
                            {
                                this.autoSetting.retryTime = 0;
                                break;
                            }
                        default:
                            {
                                autoSetting.retryTime += 1;
                                DateTime tmpEndTime = DateTime.Now;

                                if (SlotSendState.EEPError == errorState)
                                {
                                    autoSetting.disableRefreshEEPFlag = true;
                                }

                                this.ClearEEPTimer();
                                this.ResetRefreshEEPTimer(REFRESHEEPTIMEOUT);
                                break;
                            }
                    }
                }
                catch (Exception ex)
                {
                    this.autoSetting.retryTime += 1;
                    result.Error = string.Format("AutoMsg Error({0})", ex.Message);
                }
            }
            else
            {
                result.Error = string.Format("AutoMsg no CheckBattryExist");
            }

            return result;
        }

        private async Task<SlotOptions> CheckBatExist(Dictionary<string, object> tmpBatInfo)
        {
            var result = new SlotOptions();
            result.BatInfo = (tmpBatInfo != null) ? tmpBatInfo : this.batInfo;

            this.autoSetting.isBatteryIn = false;

            if (this.baudRate == 38400)
            {
                result.List = BatCore.GetBatOptionsListForType(BatOptionsCmdType.Detect);
                result.Error = null;
                result = await GetBatteryCmd(result);
            }
            else
            {
                result.List = BatCore.GetBatOptionsListForType(BatOptionsCmdType.DetectCAN);
                result = await GetBatteryCmd(result);

                if (result.Error == null)
                {
                    this.autoSetting.enableCANCmd = result.EnableCANCmd;
                }
                else
                {
                    result.List = BatCore.GetBatOptionsListForType(BatOptionsCmdType.Detect);
                    result.Error = null;
                    result = await GetBatteryCmd(result);
                }
            }

            if (result.Error == null)
            {
                this.autoSetting.enableLostBattery = false;
                this.autoSetting.retryTime = 0;
                this.autoSetting.isDetectedCmd = true;
                this.autoSetting.disableRefreshEEP = false;

                if (this.autoSetting.enableCANCmd)
                {
                    this.cmdSetting.enableASCII = true;
                }
            }

            return result;
        }

        public void StopAutoRun()
        {
            if (mAutoRunCTS != null)
            {
                mAutoRunCTS.Cancel();
                mAutoRunCTS = null;
            }
        }
        
        public async Task<int> AutoRun(bool enableLoopRetry)
        {
            mAutoRunCTS = new CancellationTokenSource();
            try
            {
                while (true)
                {
                    if (!this.autoSetting.enableLostBattery)
                    {
                        SlotOptions slotOpts = await this.AutoMsg();
                    }
                    else if (enableLoopRetry)
                    {
                        SlotOptions slotOpts = await this.CheckBatExist(null);
                    }

                    // 使用者發送之訊息
                    if (mUserCmdList.Count > 0)
                    {
                        var batOptsList = mUserCmdList.ToList();
                        SlotOptions slotOpts2 = await this.UserMsg(batOptsList, mUserBatInfo);
                        mUserCmdList.RemoveRange(0, slotOpts2.List.Count);
                    }

                    //  尚未通知電池已經遺失
                    if (this.autoSetting.retryTime >= MAXRETRYTIME && !this.autoSetting.enableLostBattery)
                    {
                        NotifyDisconnectBattery();
                    }

                    if (mAutoRunCTS == null) break;
                    await Task.Delay(AUTORUNTIMER, mAutoRunCTS.Token);
                }
            }
            catch(Exception ex)
            {
                return -1;
            }
            return 0;
        }
        
        public void CheckRecevBatAttr(BatOptions val, SlotOptions opts, Dictionary<string, object> btAttr)
        {
            if (val.Result != null)
            {
                if (val.RecvCANInfoList.Count > 0)
                {
                    opts.EnableCANCmd = true;
                }

                if (val.Result.ContainsKey("MCU_EEP")) Utility.CopyByteDataList((Dictionary<string, byte[]>)val.Result["MCU_EEP"], mMCU_EEP);
                else if (val.Result.ContainsKey("LOG_CHARGE")) Utility.CopyByteDataList((Dictionary<string, byte[]>)val.Result["LOG_CHARGE"], mLOG_CHARGE);
                else if (val.Result.ContainsKey("PB_EEP"))
                {
                    if (!opts.BatInfo.ContainsKey("PB_EEP")) opts.BatInfo.Add("PB_EEP", new Dictionary<string, byte[]>());
                    Utility.CopyByteDataList((Dictionary<string, byte[]>)val.Result["PB_EEP"], (Dictionary<string, byte[]>)opts.BatInfo["PB_EEP"]);
                }
                else if (val.Result.ContainsKey("CELL_TEMPERATURE"))
                {
                    bool isFirstData = true;
                    double[] cellTemp = (double[])val.Result["CELL_TEMPERATURE"];
                    List<double> newCellTemp = new List<double>();

                    double TEMPERATURE = 0;
                    double MAX_TEMPERATURE = 0;
                    double MIN_TEMPERATURE = 0;

                    for (int i = 0; i < cellTemp.Length; i += 1)
                    {
                        bool isInnerTemp = (i % 3) == 0;
                        if (!val.EnableASCII)
                        {
                            if (isInnerTemp)
                            {
                                // 轉換
                                Dictionary<string, byte[]> PB_EEP;
                                int ozValue = 0;
                                string ozNum = string.Format("{0}", (i / 3) + 1);
                                if (btAttr.ContainsKey("PB_EEP"))
                                {
                                    PB_EEP = (Dictionary<string, byte[]>)btAttr["PB_EEP"];
                                    if (PB_EEP[ozNum] != null && PB_EEP[ozNum].Length > 1)
                                    {
                                        ozValue = PB_EEP[ozNum][2];
                                    }
                                }
                                newCellTemp.Add(Math.Round(EEPOZ890.GetInternalTemperature(cellTemp[i], ozValue), 2));
                            }
                            else
                            {
                                newCellTemp.Add(Math.Round(EEPOZ890.GetExternalTemperature(cellTemp[i]), 2));
                            }
                        }
                        else
                        {
                            newCellTemp.Add(Math.Round(cellTemp[i], 2));
                        }

                        bool isMPSVer = btAttr.ContainsKey("MCU_VERSION") && ((string)btAttr["MCU_VERSION"] == "2.50Rev.09");
                        if (!isInnerTemp || isMPSVer)
                        {
                            double tempCellTemp = newCellTemp[i];
                            if (isFirstData)
                            {
                                isFirstData = false;
                                MAX_TEMPERATURE = tempCellTemp;
                                MIN_TEMPERATURE = tempCellTemp;
                            }
                            else if (MIN_TEMPERATURE > tempCellTemp)
                            {
                                MIN_TEMPERATURE = tempCellTemp;
                            }
                            else if (MAX_TEMPERATURE < tempCellTemp)
                            {
                                MAX_TEMPERATURE = tempCellTemp;
                            }
                        }
                    }

                    Int32 protection = (btAttr.ContainsKey("PROTECTION")) ? ((Int32)btAttr["PROTECTION"]) : 0;
                    if (((protection >> 5) & 0x01) != 0 || MIN_TEMPERATURE < 0)
                    {
                        TEMPERATURE = MIN_TEMPERATURE;
                    }
                    else
                    {
                        TEMPERATURE = MAX_TEMPERATURE;
                    }

                    Utility.CopyData(btAttr, "TEMPERATURE", TEMPERATURE);
                    Utility.CopyData(btAttr, "MAX_TEMPERATURE", MAX_TEMPERATURE);
                    Utility.CopyData(btAttr, "MIN_TEMPERATURE", MIN_TEMPERATURE);
                    Utility.CopyData(btAttr, "DELTA_TEMPERATURE", Math.Round(MAX_TEMPERATURE - MIN_TEMPERATURE, 2));
                    Utility.CopyData(btAttr, "CELL_TEMPERATURE", newCellTemp.ToArray());
                }
                else if (!val.Result.ContainsKey("CIPHER_UUID"))
                {
                    foreach (var item in val.Result)
                    {
                        Utility.CopyData(btAttr, item.Key, item.Value);
                    }
                }

                BatCore.RefreshBatInfo(btAttr, opts.EnableCANCmd);
            }
            else
            {
                opts.Error = "\"val.Result\" is empty";
            }
        }
        
        public async Task<BatOptions> RecvMsg(BatOptions opts)
        {
            var result = opts;
            int timeout = result.Timeout;
            if (result.EnableRecvTimeout)
            {
                await Task.Delay(timeout);
            }

            if (autoSetting.enableCANCmd)
            {
                if (!result.EnableRecv)
                {
                    return result;
                }
            }

            if (result.OptionStream != null)
            {
                BatParseInfo val = BatCore.ParseStream(result.OptionStream, result);
                if (val.DataList.Count > 0)
                {
                    result.DataList = val.DataList;
                }
            }
            else
            {
                bool isContinue = true;
                await Task.Delay(result.FirstDelayTime);
                long startTime = DateTime.Now.Ticks + (result.Timeout * 10000); // tick * 10000
                int i = 0;
                while (isContinue)
                {
                    i++;
                    BatParseInfo val = BatCore.ParseStream(mCOMPort.RecvData.ToArray(), result);
                    long endTime = DateTime.Now.Ticks;

                    if (!result.DisableParseStream)
                    {
                        if (val.DataList.Count > 0)
                        {
                            if (val.ClearLen > 0)
                            {
                                mCOMPort.RecvData.RemoveRange(0, val.ClearLen);
                            }
                            result.DataList = val.DataList;
                            isContinue = false;
                        }
                        else if (endTime > startTime)
                        {
                            if (val.ClearLen > 0)
                            {
                                mCOMPort.RecvData.RemoveRange(0, val.ClearLen);
                            }
                            result.Error = "recev timeout";
                            return opts;
                        }
                        else
                        {
                            if (val.ClearLen > 0)
                            {
                                mCOMPort.RecvData.RemoveRange(0, val.ClearLen);
                            }
                            await Task.Delay(50);
                        }
                    }
                    else if (endTime > startTime)
                    {
                        if (val.DataList.Count > 0)
                        {
                            result.DataList = val.DataList;
                        }
                        if (val.ClearLen > 0)
                        {
                            mCOMPort.RecvData.RemoveRange(0, val.ClearLen);
                        }
                        isContinue = false;
                    }
                    else
                    {
                        await Task.Delay(50);
                    }
                }
            }

            mBatCore.ParseMsg(result);
            return result;
        }

        public async Task<BatOptions> SendMsg(BatOptions opts)
        {
            var result = opts;
            try
            {
                result.EnableASCII = this.cmdSetting.enableASCII;
                result.EnableCipher = this.cmdSetting.enableCipher;
                result.SendTime = DateTime.Now;
                mBatCore.SetMsg(result);
                if (result.Error != null)
                {
                    return result;
                }

                int val = mCOMPort.SendData(result.SendStream);
                if (val != 0)
                {
                    result.Error = "Writing to COM port Error";
                    return result;
                }

                result = await this.RecvMsg(result);
                result.RecveTime = DateTime.Now;

                long dt = result.RecveTime.Ticks - result.SendTime.Ticks;
            }
            catch (Exception ex)
            {
                result.Error = string.Format("SendMsg Error({})", ex.Message);
            }

            return result;
        }
    }
}
