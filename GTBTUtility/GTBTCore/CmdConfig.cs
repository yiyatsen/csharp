﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTBTCore
{
    public enum CmdNameType
    {
        MCU_EEP = 0,
        BAT = 1,
        OZ1_EEP = 2,
        OZ2_EEP = 3
    }

    public enum CmdOperatorType
    {
        NonOperator = 0,
        Equal = 1,
        NotEqual = 2,
        MoreThan = 3,
        LessThan = 4,
        MoreThanOrEqual = 5,
        LessThanOrEqual = 6
    }

    public class CmdConfig
    {
        public UInt32 ID { get; set; }
        public CmdNameType CmdType { get; set; }
        public bool IsHidden { get; set; }
        public bool IsReadOnly { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public int Address { get; set; }
        public int Length { get; set; }
        public CmdOperatorType CmdOperator { get; set; }
        public string Value { get; set; }
        public string[] AttrList { get; set; }
    }

    public class CmdConfigResult
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string DefaultValue { get; set; }
        public string Condition { get; set; }
        public string RealValue { get; set; }
        public bool IsMatch { get; set; }
    }
}
