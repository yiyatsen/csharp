﻿using GTBTCore.Cipher;
using GTBTCore.MCUEEP;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GTBTCore
{
    public class BatCore
    {
        public enum BatOptionsCmdType
        {
            Detect,
            DetectCAN,
            BasicParam,
            BasicInfo,
            DynamicInfo,
            EEPCB,
            EEPPB,
            EEP4816,
            RecordLastTime,
            RecordTotalTime
        }

        public class GTCANRecvInfo
        {
            public uint canID { get; set; }
            public byte canType { get; set; }
            public int canDataLen { get; set; }
            public byte[] data { get; set; }
            public Dictionary<string, object> result = new Dictionary<string, object>();
        }

        public class BatParseInfo
        {
            private List<List<byte>> mDataList = new List<List<byte>>();
            public int ClearLen { get; set; }
            public List<List<byte>> DataList { get { return mDataList; } }
        }
        
        public enum DefaultBatCmd : byte
        {
            PB_EEP = 0x00,
            PB_BYTE_EEP = 0x97,
            PB_BYTE_OP = 0x99,
            MCU_EEP_HL = 0x02,
            MCU_EEP = 0x65,
            MCU_BYTE_EEP = 0x93,
            // Dynamic
            RTC = 0x2A,
            SOC = 0x07,
            CELL_VOLT = 0x04,
            CURRENT = 0x05,
            TEMPERATURE = 0x09,
            REMAIN_CAPACITY = 0x06,
            FULL_CHARGE_CAPACITY = 0x0D,
            PROTECTION = 0x0A,
            CYCLE_COUNT = 0x08,
            MCU_RESET_COUNT = 0x13,
            DISCHARGE_INTERVAL = 0x63,
            TEMPERATURE_INTERVAL = 0x66,
            LAST_CHARGE = 0x28,
            LAST_DISCHARGE = 0x29,
            // Static
            MCU_VERSION = 0x14,
            DEVICE_NAME = 0x0E,
            BATTERY_ID = 0x11,
            MANUFACTURE_DATE = 0x12,
            DESIGN_CAPACITY = 0x0B,
            DESIGN_VOLTAGE = 0x0C,
            FCC_RESET_TIME = 0x61,
            LOG_DISCHARGE = 0x62,
            RTC_RESET_TIME = 0x64,
            BARCODE = 0x1D,
            // EEP Support
            LOG_CHARGE = 0x2D,
            ALERTHAPPENTIME = 0xAF,
            // SYS ACT
            RESET_PB = 0x92,
            RESET_MCU = 0xAA,
            CIPHER_CMD = 0xC0,
            CIPHER_UUID = 0xC1,
            SWITCH_MODE = 0x3E,
            SYS_MSG = 0xF1,
            CAN_MODE_STATUS = 0xCA
        }

        public enum DefaultParseBatCmd : byte
        {
            MCU_EEP = 0x65,
            MCU_EEP_H = 0x02,
            MCU_EEP_L = 0x03,
            PB_EEP_OZ1 = 0x00,
            PB_EEP_OZ2 = 0x01,
            MCU_VERSION = 0x14,
            DEVICE_NAME = 0x0E,
            BATTERY_ID = 0x11,
            CELL_VOLT = 0x04,
            CURRENT = 0x05,
            REMAIN_CAPACITY = 0x06,
            FULL_CHARGE_CAPACITY = 0x0D,
            DESIGN_CAPACITY = 0x0B,
            DESIGN_VOLTAGE = 0x0C,
            SOC = 0x07,
            CYCLE_COUNT = 0x08,
            MCU_RESET_COUNT = 0x13,
            BARCODE = 0x1D,
            TEMPERATURE = 0x09,
            PROTECTION = 0x0A,
            LAST_CHARGE = 0x28,
            LAST_DISCHARGE = 0x29,
            RTC_TIMESET = 0x27,
            RTC_REG_DUMP = 0x2A,
            MANUFACTURE_DATE = 0x12,
            //OV_HAPPENTIME = 0xAF,
            //UV_HAPPENTIME = 0xB0,
            //COC_HAPPENTIME = 0xB1,
            //SC_HAPPENTIME = 0xB2,
            //OT_HAPPENTIME = 0xB3,
            //UT_HAPPENTIME = 0xB4,
            //DOC_HAPPENTIME = 0xB5,
            CHARGE_LOG_1 = 0x2D,
            CHARGE_LOG_2 = 0x2E,
            CHARGE_LOG_3 = 0x2F,
            FCC_RESET_TIME = 0x61,
            DISCHARGE_LOG = 0x62,
            DISCHARGE_INTERVAL = 0x63,
            RTC_RESET_TIME = 0x64,
            TEMPERATURE_INTERVAL = 0x66,
            RTC_YEAR_SET = 0xA0,
            RTC_MONTH_SET = 0xA1,
            RTC_DAY_SET = 0xA2,
            RTC_HOUR_SET = 0xA3,
            RTC_MIN_SET = 0xA4,
            RTC_SEC_SET = 0xA5,
            RESET_PB = 0x92,
            RESET_MCU = 0xAA,
            MCU_BYTE_EEP_GET = 0x93,
            PB_BYTE_EEP_OZ1_GET = 0x97,
            PB_BYTE_EEP_OZ2_GET = 0x98,
            PB_BYTE_OP_OZ1_GET = 0x99,
            PB_BYTE_OP_OZ2_GET = 0x9A,
            MCU_BYTE_EEP_SET = 0x91,
            PB_BYTE_EEP_OZ1_SET = 0x9C,
            PB_BYTE_EEP_OZ2_SET = 0x9D,
            PB_BYTE_OP_OZ1_SET = 0x9E,
            PB_BYTE_OP_OZ2_SET = 0x9F,
            RESET_PBPOWER = 0xAD,
            CIPHER_UUID = 0xC1,
            SWITCH_MODE = 0x3E,
            SYS_MSG = 0xF1,
            CAN_MODE_STATUS_GET = 0xCA,
            CAN_MODE_STATUS_SET = 0xCB
        }
        
        public static byte STX = 0x02;
        public static byte ETX = 0x03;
        public static byte CANSTX = 0x04;
        private static byte CANCMDCODE = 0x40;

        public static string[] PROTECTIONLIST = new string[] { "Over Voltage", "Under Voltage", "Over Current Charge", "Over Current Discharge", "Over Temperature", "Under Temperature", "Short Circuit" };

        private static UInt32[] SOHTABLE = new UInt32[101] {
            0,  1,  2,  3,  4,  5,  6,  7,  8,  9,
            10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
            20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
            30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
            40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
            50, 52, 54, 56, 58, 60, 62, 64, 66, 68,
            70, 71, 72, 73, 74, 75, 76, 77, 78, 79,
            80, 81, 82, 83, 84, 85, 86, 87, 88, 89,
            90, 90, 91, 91, 92, 92, 93, 93, 94, 94,
            95, 95, 97, 97, 98, 99, 100, 100, 100, 100, 100
        };

        public static List<CmdConfig> BATPARAMLIST = new List<CmdConfig>()
        {
            new CmdConfig() { ID =0x07, Name = "SOC", IsReadOnly = true, Type = "System.UInt32" },
            new CmdConfig() { Name = "SOH", IsReadOnly = true, Type = "System.UInt32" },
            new CmdConfig() { ID =0x0A, Name = "PROTECTION", IsReadOnly = true, Type = "Int32" },
            new CmdConfig() { ID =0x04, Name = "TOTAL_VOLT", IsReadOnly = true, Type = "System.UInt64" },
            new CmdConfig() { ID =0x04, Name = "MIN_VOLT", IsReadOnly = true, Type = "System.UInt32" },
            new CmdConfig() { ID =0x04, Name = "MAX_VOLT", IsReadOnly = true, Type = "System.UInt32" },
            new CmdConfig() { ID =0x04, Name = "DELTA_VOLT", IsReadOnly = true, Type = "System.UInt32" },
            new CmdConfig() { ID =0x04, Name = "CELL_NUM", IsReadOnly = true, Type = "System.Int32" },
            new CmdConfig() { ID =0x09, Name = "TEMPERATURE", IsReadOnly = true, Type = "System.Double" },
            new CmdConfig() { ID =0x05, Name = "CURRENT", IsReadOnly = true, Type = "System.Int64" },
            new CmdConfig() { ID =0x06, Name = "REMAIN_CAPACITY", IsReadOnly = true, Type = "System.UInt64" }
        };

        public static List<CmdConfig> MCUPARAMLIST = new List<CmdConfig>()
        {
            new CmdConfig() { ID =0x2A, Name = "RTC", IsReadOnly = true, Type = "System.DateTime" },
            new CmdConfig() { ID =0x65, Name = "MCU_EEP", IsReadOnly = true, IsHidden = true, Type = "System.Byte[]" }
        };

        public static List<CmdConfig> CANBATPARAMLIST = new List<CmdConfig>() {
            new CmdConfig() { ID = 0x704, Name = "Heart Beat", AttrList = new string[] { "BATTERY_HEART_BEAT" }, IsReadOnly = true, IsHidden = true, Type = "System.Int32[]" },
            new CmdConfig() { ID = 0x300, Name = "Status", AttrList = new string[] { "SOC", "CURRENT", "TEMPERATURE", "TOTAL_VOLT" }, IsReadOnly = true, IsHidden = true, Type = "System.Byte[]" },
            new CmdConfig() { ID = 0x481, Name = "Cycle and Flag", AttrList = new string[] { "CYCLE_COUNT", "PROTECTION" }, IsReadOnly = true, IsHidden = true, Type = "System.Byte[]" },
            new CmdConfig() { ID = 0x482, Name = "Last Chrg. Info.", AttrList = new string[] { "LAST_CHARGE" }, IsReadOnly = true, IsHidden = true, Type = "System.Byte[]" },
            new CmdConfig() { ID = 0x483, Name = "Energy", AttrList = new string[] { "FULL_CHARGE_CAPACITY", "REMAIN_CAPACITY" }, IsReadOnly = true, IsHidden = true, Type = "System.Byte[]" },
            new CmdConfig() { ID = 0x484, Name = "ID and Chrg. Status", AttrList = new string[] { "batteryId", "PACK_STATE" }, IsReadOnly = true, IsHidden = true, Type = "System.Byte[]" },
            new CmdConfig() { ID = 0x506, Name = "Energy", AttrList = new string[] { "FULL_CHARGE_CAPACITY", "REMAIN_CAPACITY" }, IsReadOnly = true, IsHidden = true, Type = "System.Byte[]" },
            new CmdConfig() { ID = 0x507, Name = "ID and Chrg. Status", AttrList = new string[] { "batteryId", "PACK_STATE" }, IsReadOnly = true, IsHidden = true, Type = "System.Byte[]" }
        };
        
        public static string ConvertCmdToString(string key, object value)
        {
            switch (key)
            {
                case "PROTECTION":
                    {
                        Int32 data = (Int32)value;
                        return string.Format("OV: {0}, UV:{1}, COC:{2}, DOC:{3}, OT:{4}, UT:{5}, SC:{6}", ((data >> 0) & 0x01), ((data >> 1) & 0x01), ((data >> 2) & 0x01), ((data >> 3) & 0x01), ((data >> 4) & 0x01), ((data >> 5) & 0x01), ((data >> 6) & 0x01));
                    }
                case "CAN_PACK_STATE":
                    {
                        Int64 data = (Int64)value;
                        if (data == 0) return "Charge";
                        else if (data == 1) return "Discharge";
                        else return "Idle";
                    }
                default:
                    {
                        return value.ToString();
                    }
            }
        }

        public static List<CmdConfig> GetCBEEPCmdConfigList()
        {
            return EEPGTMCU.CMDCONFIGLIST;
        }

        public static List<CmdConfig> GetPBEEPCmdConfigList()
        {
            return EEPOZ890.CMDCONFIGLIST;
        }

        public static BatInfo GetBatInfo(Dictionary<string, object> batInfo)
        {
            BatInfo bat = new BatInfo();

            PropertyInfo[] pInfoList = bat.GetType().GetProperties();

            foreach (PropertyInfo pInfo in pInfoList)
            {
                if (batInfo.ContainsKey(pInfo.Name))
                {
                    if ("MCU_EEP" == pInfo.Name)
                    {
                        bat.MCU_EEP = Array.ConvertAll((byte[])batInfo[pInfo.Name], c => (Int32)c);
                    }
                    else if ("PB_EEP" == pInfo.Name)
                    {
                        Dictionary<string, byte[]> tmpVal = (Dictionary<string, byte[]>)batInfo[pInfo.Name];
                        bat.PB_EEP = new Dictionary<string, Int32[]>();
                        foreach(string id in tmpVal.Keys)
                        {
                            bat.PB_EEP.Add(id, Array.ConvertAll(((Dictionary<string, byte[]>)batInfo[pInfo.Name])[id], c => (Int32)c));
                        }
                    }
                    else
                    {
                        pInfo.SetValue(bat, batInfo[pInfo.Name]);
                    }
                }
            }

            return bat;
        }
        
        public static string SaveLog(DateTime dt, BatInfo bat)
        {
            string result = string.Format("{0}{1}:      <<{2}>>\r\n{0}", Utility.Separator, "Record Time".PadRight(30), dt.ToString("yyyy/MM/dd HH:mm:ss"));
            result += string.Format("{0}[Battery Pack Information]\r\n{0}", Utility.Separator);
            result += string.Format("{0}:{1}\r\n", "Device Name".PadRight(30), bat.batteryType);
            result += string.Format("{0}:{1}\r\n", "Serial Number".PadRight(30), bat.batteryId);

            if (bat.MANUFACTURE_DATE != null)
            {
                result += string.Format("{0}:{1:0000}/{2:00}/{3:00}\r\n", "Manufacture Date".PadRight(30), bat.MANUFACTURE_DATE[0], bat.MANUFACTURE_DATE[1], bat.MANUFACTURE_DATE[2]);
            }
            else
            {
                result += string.Format("{0}:2000/01/01\r\n", "Manufacture Date".PadRight(30));
            }

            result += string.Format("{0}:{1} mV\r\n", "Design Voltage".PadRight(30), bat.DESIGN_VOLTAGE);
            result += string.Format("{0}:{1} mAh\r\n", "Design Capacity".PadRight(30), bat.DESIGN_CAPACITY);
            result += string.Format("{0}:{1} mAh\r\n", "Remain Capacity".PadRight(30), bat.REMAIN_CAPACITY);
            result += string.Format("{0}:{1} mAh\r\n", "Full Charge Capacity".PadRight(30), bat.FULL_CHARGE_CAPACITY);
            result += string.Format("{0}:{1}\r\n", "Cycle Counted".PadRight(30), bat.CYCLE_COUNT);

            result += string.Format("{0}[State Of Health]\r\n{0}", Utility.Separator);
            result += string.Format("{0}:{1} %\r\n", "Battery Health".PadRight(30), bat.SOH);

            result += string.Format("{0}[Relative SOC]\r\n{0}", Utility.Separator);
            result += string.Format("{0}:{1} %\r\n", "Relative SOC".PadRight(30), bat.SOC);

            result += string.Format("{0}[Recent Current]\r\n{0}", Utility.Separator);
            result += string.Format("{0}:{1} mA\r\n", "Current".PadRight(30), bat.CURRENT);

            result += string.Format("{0}[Pack Voltage]\r\n{0}", Utility.Separator);
            result += string.Format("{0}:{1} mV\r\n", "Voltage".PadRight(30), bat.TOTAL_VOLT);

            result += string.Format("{0}[Pack Temperature]\r\n{0}", Utility.Separator);
            result += string.Format("{0}:{1:0.00} ℃\r\n", "Battery Temperature".PadRight(30), bat.TEMPERATURE);

            result += string.Format("{0}[Usage Current Error Record]\r\n{0}", Utility.Separator);
            if (bat.PROTECTION > 0)
            {
                if (((bat.PROTECTION >> 0) & 0x01) != 0) result += string.Format("Error : Over Voltage Charged\r\n");
                if (((bat.PROTECTION >> 1) & 0x01) != 0) result += string.Format("Error : Under Voltage Discharged\r\n");
                if (((bat.PROTECTION >> 2) & 0x01) != 0) result += string.Format("Error : Over Current Charged\r\n");
                if (((bat.PROTECTION >> 3) & 0x01) != 0) result += string.Format("Error : Over Current Discharged\r\n");
                if (((bat.PROTECTION >> 4) & 0x01) != 0) result += string.Format("Error : Over Temp.\r\n");
                if (((bat.PROTECTION >> 5) & 0x01) != 0) result += string.Format("Error : Under Temp.\r\n");
                if (((bat.PROTECTION >> 6) & 0x01) != 0) result += string.Format("Error : Short Circuit\r\n");
            }

            result += string.Format("{0}[Cells Voltage]\r\n{0}", Utility.Separator);
            if (bat.CELL_VOLT != null)
            {
                for (int i = 0; i < bat.CELL_VOLT.Length; i++)
                {
                    string name = string.Format("Cell[{0:00}] Volt.", (i + 1));
                    result += string.Format("{0}:{1} mV\r\n", name.PadRight(30), bat.CELL_VOLT[i]);
                }
            }

            result += string.Format("{0}:{1} mV\r\n", "Cell Voltage Delta".PadRight(30), bat.DELTA_VOLT);

            result += string.Format("{0}[Pack Detail Info.]\r\n{0}", Utility.Separator);
            result += string.Format("{0}:{1}\r\n", "MCU Firmware Ver".PadRight(30), bat.MCU_VERSION);

            if (bat.LAST_CHARGE != null)
            {
                result += string.Format("{0}:{1}\r\n", "Last Charging Time".PadRight(30), bat.LAST_CHARGE);
            }

            if (bat.LAST_DISCHARGE != null)
            {
                result += string.Format("{0}:{1}\r\n", "Last Discharging Time".PadRight(30), bat.LAST_DISCHARGE);
            }

            result += string.Format("{0}{1}{1}{1}{1}{1}{1}{1}{1}{1}{0}", Utility.Separator, "\r\n");


            return result;
        }

        public static List<BatOptions> GetBatOptionsListForType(BatOptionsCmdType cmd)
        {
            switch (cmd)
            {
                case BatOptionsCmdType.Detect:
                    {
                        return new List<BatOptions>()
                        {
                            new BatOptions() { Action = "get", CmdName = "MCU_VERSION" }
                        };
                    }
                case BatOptionsCmdType.DetectCAN:
                    {
                        return new List<BatOptions>()
                        {
                            new BatOptions() { CANID = 0x11000001, CANType = 0x01, CANData = new byte[] { 0x01 } }
                        };
                    }
                case BatOptionsCmdType.BasicInfo:
                    {
                        return new List<BatOptions>()
                        {
                            new BatOptions() { Action = "get", CmdName = "BARCODE" },
                            new BatOptions() { Action = "get", CmdName = "DEVICE_NAME" }
                        };
                    }
                case BatOptionsCmdType.BasicParam:
                    {
                        return new List<BatOptions>()
                        {
                            new BatOptions() { Action = "get", CmdName = "CIPHER_UUID" },
                            new BatOptions() { Action = "get", CmdName = "DISCHARGE_INTERVAL" },
                            new BatOptions() { Action = "get", CmdName = "TEMPERATURE_INTERVAL" }
                        };
                    }
                case BatOptionsCmdType.DynamicInfo:
                    {
                        return new List<BatOptions>()
                        {
                            new BatOptions() { Action = "get", CmdName = "SOC" },
                            new BatOptions() { Action = "get", CmdName = "PROTECTION" },
                            new BatOptions() { Action = "get", CmdName = "CURRENT" },
                            new BatOptions() { Action = "get", CmdName = "REMAIN_CAPACITY" },
                            new BatOptions() { Action = "get", CmdName = "CELL_VOLT" },
                            new BatOptions() { Action = "get", CmdName = "TEMPERATURE" },
                            new BatOptions() { Action = "get", CmdName = "RTC" }
                        };
                    }
                case BatOptionsCmdType.EEPCB:
                    {
                        return new List<BatOptions>()
                        {
                            new BatOptions() { Action = "get", CmdName = "MCU_EEP_HL", SetCmd = 0x01 },
                            new BatOptions() { Action = "get", CmdName = "MCU_EEP_HL", SetCmd = 0x02 }
                        };
                    }
                case BatOptionsCmdType.EEPPB:
                    {
                        return new List<BatOptions>()
                        {
                            new BatOptions() { Action = "get", CmdName = "PB_EEP", SetCmd = 0x01 },
                            new BatOptions() { Action = "get", CmdName = "PB_EEP", SetCmd = 0x02 }
                        };
                    }
                case BatOptionsCmdType.EEP4816:
                    {
                        return new List<BatOptions>()
                        {
                            new BatOptions() { Action = "get", CmdName = "DEVICE_NAME" },
                            new BatOptions() { Action = "get", CmdName = "BATTERY_ID" },
                            new BatOptions() { Action = "get", CmdName = "MANUFACTURE_DATE" },
                            new BatOptions() { Action = "get", CmdName = "DESIGN_VOLTAGE" },
                            new BatOptions() { Action = "get", CmdName = "DESIGN_CAPACITY" },
                            new BatOptions() { Action = "get", CmdName = "FULL_CHARGE_CAPACITY" },
                            new BatOptions() { Action = "get", CmdName = "CYCLE_COUNT" },
                            new BatOptions() { Action = "get", CmdName = "MCU_RESET_COUNT" }
                        };
                    }
                case BatOptionsCmdType.RecordLastTime:
                    {
                        return new List<BatOptions>()
                        {
                            new BatOptions() { Action = "get", CmdName = "LAST_CHARGE" },
                            new BatOptions() { Action = "get", CmdName = "LAST_DISCHARGE" }
                        };
                    }
                case BatOptionsCmdType.RecordTotalTime:
                    {
                        return new List<BatOptions>()
                        {
                            new BatOptions() { Action = "get", CmdName = "LOG_CHARGE", SetCmd = 0x01 },
                            new BatOptions() { Action = "get", CmdName = "LOG_CHARGE", SetCmd = 0x02 },
                            new BatOptions() { Action = "get", CmdName = "LOG_CHARGE", SetCmd = 0x03 }
                        };
                    }
                default:
                    {
                        return new List<BatOptions>();
                    }
            }
        }

        public static List<BatOptions> GetBatOptionsListForCBType(Int32 index)
        {
            if (index == 0) return BatCore.GetBatOptionsListForType(BatOptionsCmdType.EEPCB);

            List<BatOptions> opts = new List<BatOptions>();

            for (int i = 0; i < index; i += 1)
            {
                opts.Add(new BatOptions() { SetPara = (byte)(i + 1), Action = "get", CmdName = "MCU_EEP" });
            }

            return opts;
        }

        public static string GetSerialNumber(Dictionary<string, object> batInfo)
        {
            string str = (batInfo.ContainsKey("MANUFACTURE_PRODUCT")) ? (string)(batInfo["MANUFACTURE_PRODUCT"]) : "00";

            Int32 year = (batInfo.ContainsKey("MANUFACTURE_DATE")) ? ((Int32[])batInfo["MANUFACTURE_DATE"])[0] : Utility.DEFAULTYEAR;
            Int32 week = (batInfo.ContainsKey("MANUFACTURE_WEEK")) ? (Int32)batInfo["MANUFACTURE_WEEK"] : 1;
            UInt32 id = (batInfo.ContainsKey("BATTERY_ID")) ? (UInt32)(batInfo["BATTERY_ID"]) : 0;
            return string.Format("{0}{1:00}{2:00}{3:0000}", str, year - Utility.DEFAULTYEAR, week, id);
        }
        
        public static UInt32 GetMappingSOH(Dictionary<string, object> batInfo)
        {
            UInt64 fcc = (batInfo.ContainsKey("FULL_CHARGE_CAPACITY")) ? (UInt64)batInfo["FULL_CHARGE_CAPACITY"] : 0;
            UInt64 dc = (batInfo.ContainsKey("DESIGN_CAPACITY")) ? (UInt64)batInfo["DESIGN_CAPACITY"] : 1;

            UInt64 soh = (fcc * 100) / dc;
            if (soh < 0) soh = 0;
            else if (soh > 100) soh = 100;

            return BatCore.SOHTABLE[(int)soh];
        }

        public static void RefreshBatInfo(Dictionary<string, object> batInfo, bool EnableCANCmd)
        {
            if (!EnableCANCmd) return;

            if (batInfo.ContainsKey("DEVICE_NAME")) Utility.CopyData(batInfo, "batteryType", batInfo["DEVICE_NAME"]);

            if (batInfo.ContainsKey("BARCODE")) Utility.CopyData(batInfo, "batteryId", batInfo["BARCODE"]);

            // For CAN DATA
            Int64 tmpCurrent = (batInfo.ContainsKey("CAN_CURRENT")) ? (Int64)batInfo["CAN_CURRENT"] : 0;

            Int64 tmpCurrentState = 1;
            if (batInfo.ContainsKey("CAN_PACK_STATE") && ((Int64)(batInfo["CAN_PACK_STATE"]) == 1))
            {
                tmpCurrentState = -1;
            }

            Utility.CopyData(batInfo, "CURRENT", (Int64)(tmpCurrent * tmpCurrentState));

            if (batInfo.ContainsKey("CAN_batteryId")) Utility.CopyData(batInfo, "batteryId", string.Format("{0:0000000000}", batInfo["CAN_batteryId"]));
            
            if (batInfo.ContainsKey("REFRESH_CONFIG"))
            {
                Utility.CopyData(batInfo, "PACK_CONFIG", batInfo["REFRESH_CONFIG"]);
                for (int i = 0; i < CANBATPARAMLIST.Count; i += 1)
                {
                    if ((((byte)batInfo["PACK_CONFIG"] >> i) & 0x01) == 0)
                    {
                        //UInt32 idInfo = CANBATPARAMLIST[i].ID;
                        for (int j = 0; j < CANBATPARAMLIST[i].AttrList.Length; j += 1)
                        {
                            if (batInfo.ContainsKey(CANBATPARAMLIST[i].AttrList[j])) batInfo.Remove(CANBATPARAMLIST[i].AttrList[j]);
                        }
                    }
                }
                batInfo.Remove("REFRESH_CONFIG");
            }
        }

        public static void SetCmdInfo(BatOptions opts)
        {
            if (opts.CANID != 0)
            {
                if (opts.CANData == null)
                {
                    opts.Error = "\"opts.canData\" is not a array";
                    return;
                }

                opts.STX = CANSTX;
                opts.EnableASCII = true;
                opts.EnableCipher = false;
                int dataLen = opts.CANData.Length;

                opts.Len = (byte)(7 + dataLen);
                opts.Para = opts.Para;
                opts.Cmd = (opts.Cmd != 0x00) ? opts.Cmd : CANCMDCODE;
                List<byte> tmpData = new List<byte>();

                tmpData.Add((byte)((opts.CANID >> 24) & 0xFF));
                tmpData.Add((byte)((opts.CANID >> 16) & 0xFF));
                tmpData.Add((byte)((opts.CANID >> 8) & 0xFF));
                tmpData.Add((byte)(opts.CANID & 0xFF));

                tmpData.Add((byte)((opts.CANType << 4) | dataLen));
                for (int i = 0; i < dataLen; i += 1)
                {
                    tmpData.Add(opts.CANData[i]);
                }
                opts.Data = tmpData.ToArray();
                return;
            }

            if (opts.CmdName == null)
            {
                opts.Error = "\"options.CmdName\" is invaild";
                return;
            }

            if (opts.Action == null)
            {
                opts.Error = "\"opts.Action\" is not a string";
                return;
            }

            try
            {
                DefaultBatCmd cmd = (DefaultBatCmd)System.Enum.Parse(typeof(DefaultBatCmd), opts.CmdName);

                if (opts.Action == "get")
                {
                    opts.Cmd = (byte)cmd;
                    switch (opts.CmdName)
                    {
                        case "MCU_EEP_HL":
                        case "PB_EEP":
                        case "LOG_CHARGE":
                            {
                                int tmpCmd = ((int)opts.SetCmd > 0x00) ? ((int)opts.SetCmd - 0x01) : 0x00;
                                opts.Cmd = (byte)(cmd + (byte)tmpCmd);
                                opts.Para = (opts.SetPara != null) ? ((byte)opts.SetPara) : (byte)0x00;
                                opts.Timeout = 500;
                                break;
                            }
                        case "MCU_EEP":
                            {
                                opts.Para = (opts.SetPara != null) ? ((byte)opts.SetPara) : (byte)0x00;

                                if (opts.Para != 0x00)
                                {
                                    opts.Timeout = 500;
                                }
                                break;
                            }
                        case "PB_BYTE_EEP":
                        case "PB_BYTE_OP":
                            {
                                int tmpCmd = ((int)opts.SetCmd > 0x00) ? ((int)opts.SetCmd - 0x01) : 0x00;
                                opts.Cmd = (byte)(cmd + (byte)tmpCmd);
                                opts.Para = (opts.SetPara != null) ? ((byte)opts.SetPara) : (byte)0x00;
                                break;
                            }
                        case "MCU_BYTE_EEP":
                            {
                                opts.Cmd = 0x93;
                                opts.Para = (opts.SetPara != null) ? ((byte)opts.SetPara) : (byte)0x00;
                                break;
                            }
                        case "RTC":
                            {
                                opts.Cmd = (byte)(((string)opts.SetCmd == "2.50Rev.09") ? 0x27 : (byte)cmd);
                                break;
                            }
                        case "MCU_VERSION":
                        case "DEVICE_NAME":
                        case "CELL_VOLT":
                        case "TEMPERATURE":
                        case "CIPHER_UUID":
                            {
                                opts.Timeout = 300;
                                break;
                            }
                        case "DISCHARGE_INTERVAL":
                        case "TEMPERATURE_INTERVAL":
                            {
                                opts.Para = (opts.SetPara != null && ((string)opts.SetPara == "value")) ? (byte)0x01 : (byte)0x00;
                                break;
                            }
                        case "ALERTHAPPENTIME":
                            {
                                int tmpCmd = ((int)opts.SetCmd > 0x00) ? (int)opts.SetCmd - 0x01 : 0x00;
                                opts.Cmd = (byte)(cmd + (byte)tmpCmd);
                                break;
                            }
                        case "SWITCH_MODE":
                            {
                                opts.Para = (opts.SetPara != null && ((string)opts.SetPara == "eng")) ? (byte)0x83 : (byte)0x03;
                                break;
                            }
                        case "SYS_MSG":
                            {
                                opts.Para = (opts.SetPara != null) ? (byte)opts.SetPara : (byte)0x00;
                                opts.Timeout = opts.SetTimeout;
                                opts.EnableCipher = false;
                                opts.DisableParseStream = true;
                                break;
                            }
                        case "CAN_MODE_STATUS":
                            {
                                opts.Para = (opts.SetPara != null && ((string)opts.SetPara == "eng")) ? (byte)0x01 : (byte)0x02;
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
                else if (opts.Action == "set")
                {
                    switch (opts.CmdName)
                    {
                        case "PB_BYTE_EEP":
                        case "PB_BYTE_OP":
                            {
                                Int32 tmpSetCmd = (opts.CmdName == "PB_BYTE_EEP") ? 0x9C : 0x9E;
                                opts.Len = 0x03;
                                int tmpCmd = ((int)opts.SetCmd > 0x00) ? ((int)opts.SetCmd - 0x01) : 0x00;
                                opts.Cmd = (byte)(tmpSetCmd + (byte)tmpCmd);
                                opts.Para = (opts.SetPara != null) ? (byte)opts.SetPara : (byte)0x00;
                                opts.Data = new byte[] { opts.SetData };
                                break;
                            }
                        case "MCU_BYTE_EEP":
                            {
                                opts.Len = 0x04;
                                opts.Cmd = 0x91;
                                opts.Para = (opts.SetPara != null) ? (byte)opts.SetPara : (byte)0x00;
                                opts.Data = (opts.EnableASCII) ? new byte[] { 0x01, opts.SetData } : new byte[] { opts.SetData, 0xFF };
                                break;
                            }
                        case "RTC":
                            {
                                int tmpCmd = ((int)opts.SetCmd > 0x00) ? ((int)opts.SetCmd) : 0x00;
                                opts.Cmd = (byte)(0xA0 + (byte)tmpCmd);
                                opts.Para = (opts.SetPara != null) ? (byte)opts.SetPara : (byte)0x00;
                                break;
                            }
                        case "CAN_MODE_STATUS":
                            {
                                opts.Len = 0x03;
                                opts.Cmd = (byte)((byte)cmd + 0x01);
                                opts.Para = (opts.SetPara != null) ? (byte)opts.SetPara : (byte)0x00;
                                opts.Data = new byte[] { opts.SetData };
                                break;
                            }
                        default:
                            {
                                opts.Error = string.Format("Not support SetCmd({0})", opts.CmdName);
                                break;
                            }
                    }
                }
                else
                {
                    opts.Error = "\"options.Action\" is incorrect type";
                }
            }
            catch (Exception ex)
            {
                opts.Error = string.Format("Not support Cmd({0}): ({1})", opts.CmdName, ex.Message);
                return;
            }
        }

        private static void PackData(BatOptions opts)
        {
            uint sum = 0;
            List<byte> array = new List<byte>();

            if (opts.EnableASCII) array.Add(opts.STX);
            sum += Utility.PushChar(array, opts.Len, opts.EnableASCII);
            sum += Utility.PushChar(array, opts.Para, opts.EnableASCII);
            sum += Utility.PushChar(array, opts.Cmd, opts.EnableASCII);

            if (opts.Data != null && opts.Data.Length > 0)
            {
                for (int i = 0; i < opts.Data.Length; i += 1)
                {
                    sum += Utility.PushChar(array, opts.Data[i], opts.EnableASCII);
                }
            }

            if (opts.EnableASCII)
            {
                array.Add(Utility.ConvertHEXToASCII((byte)((sum >> 12) & 0x0F)));
                array.Add(Utility.ConvertHEXToASCII((byte)((sum >> 8) & 0x0F)));
                array.Add(Utility.ConvertHEXToASCII((byte)((sum >> 4) & 0x0F)));
                array.Add(Utility.ConvertHEXToASCII((byte)((sum >> 0) & 0x0F)));

                if (opts.CANID != 0)
                {
                    array[0] = BatCore.CANSTX;
                }

                array.Add(opts.ETX);
            }

            opts.SendStream = array.ToArray();
        }

        private static void UnpackData(BatOptions opts)
        {
            if (!(opts.RecvStream != null && opts.RecvStream.Length > 0))
            {
                opts.Error = "Not recv stream";
                return;
            }

            if (!opts.EnableASCII)
            {
                opts.UnpackStream = new byte[opts.RecvStream.Length - 1];
                Array.Copy(opts.RecvStream, 0, opts.UnpackStream, 0, opts.UnpackStream.Length);
                return;
            }

            uint sum = 0;
            List<byte> array = new List<byte>();
            int len = opts.RecvStream.Length;
            int EOB = len - 5;

            opts.IsCANMode = (opts.RecvStream[0] == BatCore.CANSTX);
            for (int i = 1; i < len; i += 2)
            {
                if (i >= EOB)
                {
                    uint checkSum =
                        (uint)(
                            (Utility.ConvertASCIIToHEX(opts.RecvStream[i]) << 12) |
                            (Utility.ConvertASCIIToHEX(opts.RecvStream[i + 1]) << 8) |
                            (Utility.ConvertASCIIToHEX(opts.RecvStream[i + 2]) << 4) |
                            Utility.ConvertASCIIToHEX(opts.RecvStream[i + 3])
                         );
                    if (sum == checkSum)
                    {
                        opts.UnpackStream = array.ToArray();
                    }
                    else
                    {
                        opts.Error = string.Format("Checksum error ${ 0} !== ${ 1}", sum, checkSum);
                    }
                    break;
                }
                else
                {
                    byte tmp = (byte)((Utility.ConvertASCIIToHEX(opts.RecvStream[i]) << 4) | Utility.ConvertASCIIToHEX(opts.RecvStream[i + 1]));
                    array.Add(tmp);
                    sum += array[array.Count - 1];
                }
            }
        }

        public static BatParseInfo ParseStream(byte[] stream, BatOptions opts)
        {
            BatParseInfo batParseInfo = new BatParseInfo();
            if (!opts.DisableParseStream)
            {
                int len = stream.Length;
                if (opts.EnableASCII)
                {
                    List<byte> data = new List<byte>();
                    int checkItem = -1;
                    int tmpLen = 0;

                    for (int i = 0; i < len; i += 1)
                    {
                        if ((stream[i] == BatCore.STX || stream[i] == BatCore.CANSTX))
                        {
                            if (checkItem > 0)
                            {
                                batParseInfo.ClearLen += data.Count;
                                data = new List<byte>();
                            }

                            checkItem = 1;
                            batParseInfo.ClearLen += tmpLen;
                            tmpLen = 0;
                            data.Add(stream[i]);    // Add Header CHAR
                        }
                        else if (checkItem > 0)
                        {
                            data.Add(stream[i]);    // Add CHAR
                            if (stream[i] == BatCore.ETX)
                            {
                                batParseInfo.ClearLen += data.Count;
                                batParseInfo.DataList.Add(data.ToList());
                                data = new List<byte>();
                                checkItem = 0;
                            }
                        }
                        else
                        {
                            tmpLen += 1;
                        }
                    }

                    if (checkItem < 0 && len > 128)
                    {
                        batParseInfo.ClearLen = len;
                    }
                }
                else if (len > 0 && stream[len - 1] == 0xFF)
                {
                    batParseInfo.ClearLen = stream.Length;
                    if (len > 3 && len == (stream[0] + 1))
                    {
                        batParseInfo.DataList.Add(stream.ToList());
                    }
                }
            }
            else
            {
                batParseInfo.ClearLen = stream.Length;
                batParseInfo.DataList.Add(stream.ToList());
            }

            return batParseInfo;
        }

        private static void ParseCANData(BatOptions opts)
        {
            if (!(opts.UnpackStream != null && opts.UnpackStream.Length >= 3))
            {
                opts.Error = "\"opts.UnpackStream\" is invalid";
                return;
            }

            byte cmdLen = opts.UnpackStream[0];
            byte para = opts.UnpackStream[1];
            byte cmdCode = opts.UnpackStream[2];

            if (cmdCode != BatCore.CANCMDCODE)
            {
                opts.Error = "Error Cmd";
                return;
            }

            if (para != 0x00)
            {
                opts.Error = "Not Implement";
                return;
            }

            GTCANRecvInfo recvCANInfo = new GTCANRecvInfo();
            recvCANInfo.canDataLen = (opts.UnpackStream[7] & 0x0F);

            if ((recvCANInfo.canDataLen + 7) != cmdLen)
            {
                opts.Error = "Error Format";
                return;
            }

            recvCANInfo.canID = (uint)((opts.UnpackStream[3] << 24) + (opts.UnpackStream[4] << 16) + (opts.UnpackStream[5] << 8) + opts.UnpackStream[6]);
            recvCANInfo.canType = (byte)(opts.UnpackStream[7] >> 4);
            recvCANInfo.data = new byte[recvCANInfo.canDataLen];
            Array.Copy(opts.UnpackStream, 8, recvCANInfo.data, 0, recvCANInfo.canDataLen);

            opts.Result = new Dictionary<string, object>();

            switch (recvCANInfo.canID)
            {
                case 0x12000001:
                    {
                        if (recvCANInfo.data[0] == 0x01)
                        {
                            Utility.CopyData(opts.Result, "PACK_CONFIG", recvCANInfo.data[1]);
                            Utility.CopyData(recvCANInfo.result, "PACK_CONFIG", opts.Result["PACK_CONFIG"]);
                        }
                        else if (recvCANInfo.data[0] == 0x02)
                        {
                            Utility.CopyData(opts.Result, "REFRESH_CONFIG", recvCANInfo.data[1]);
                            Utility.CopyData(recvCANInfo.result, "REFRESH_CONFIG", opts.Result["REFRESH_CONFIG"]);
                        }
                        else
                        {
                            Utility.CopyData(opts.Result, "PACK_CONFIG_NUM", recvCANInfo.data[1]);
                            Utility.CopyData(recvCANInfo.result, "PACK_CONFIG_NUM", opts.Result["PACK_CONFIG_NUM"]);
                        }
                        break;
                    }
                case 0x704:
                    {
                        Utility.CopyData(opts.Result, "BATTERY_HEART_BEAT", recvCANInfo.data[0]);
                        Utility.CopyData(recvCANInfo.result, "BATTERY_HEART_BEAT", opts.Result["BATTERY_HEART_BEAT"]);
                        break;
                    }
                case 0x300:
                    {
                        Utility.CopyData(opts.Result, "SOC", (UInt32)recvCANInfo.data[0]);
                        Utility.CopyData(opts.Result, "CAN_CURRENT", Convert.ToInt64(((recvCANInfo.data[3] << 8) | recvCANInfo.data[2]) * 62.5));
                        Utility.CopyData(opts.Result, "TEMPERATURE", Math.Round((Double)((recvCANInfo.data[5] << 8) | recvCANInfo.data[4]) * 0.1, 2));
                        Utility.CopyData(opts.Result, "TOTAL_VOLT", (UInt64)(((recvCANInfo.data[7] << 8) | recvCANInfo.data[6]) * 46));

                        Utility.CopyData(recvCANInfo.result, "SOC", opts.Result["SOC"]);
                        Utility.CopyData(recvCANInfo.result, "CAN_CURRENT", opts.Result["CAN_CURRENT"]);
                        Utility.CopyData(recvCANInfo.result, "TEMPERATURE", opts.Result["TEMPERATURE"]);
                        Utility.CopyData(recvCANInfo.result, "TOTAL_VOLT", opts.Result["TOTAL_VOLT"]);
                        break;
                    }
                case 0x481:
                    {
                        Int32 value = 0;
                        value |= (((recvCANInfo.data[2] >> 0) & 0x01) << 0);
                        value |= (((recvCANInfo.data[2] >> 1) & 0x01) << 1);
                        value |= (((recvCANInfo.data[2] >> 2) & 0x01) << 2);
                        value |= (((recvCANInfo.data[2] >> 4) & 0x01) << 4);
                        value |= (((recvCANInfo.data[2] >> 5) & 0x01) << 5);
                        value |= (((recvCANInfo.data[2] >> 3) & 0x01) << 6);
                        Utility.CopyData(opts.Result, "CYCLE_COUNT", (UInt32)((recvCANInfo.data[1] << 8) | recvCANInfo.data[0]));
                        Utility.CopyData(opts.Result, "PROTECTION", value);

                        Utility.CopyData(recvCANInfo.result, "CYCLE_COUNT", opts.Result["CYCLE_COUNT"]);
                        Utility.CopyData(recvCANInfo.result, "PROTECTION", opts.Result["PROTECTION"]);
                        break;
                    }
                case 0x482:
                    {
                        List<int> value = new List<int>();
                        value.Add(recvCANInfo.data[0] + Utility.DEFAULTYEAR);
                        value.Add(recvCANInfo.data[1]);
                        value.Add(recvCANInfo.data[2]);
                        value.Add(recvCANInfo.data[3]);
                        value.Add(recvCANInfo.data[4]);
                        value.Add(recvCANInfo.data[5]);
                        value.Add(recvCANInfo.data[6]);
                        
                        Utility.CopyData(opts.Result, "LAST_CHARGE", string.Format("{0:0000}-{1:00}-{2:00}T{3:00}:{4:00}:{5:00} {6:000}%", value[0], value[1], value[2], value[3], value[4], value[5], value[6]));
                        Utility.CopyData(recvCANInfo.result, "LAST_CHARGE", opts.Result["LAST_CHARGE"]);
                        break;
                    }
                case 0x483:
                case 0x506:
                    {
                        Utility.CopyData(opts.Result, "FULL_CHARGE_CAPACITY", (UInt64)((((recvCANInfo.data[3] << 24) | (recvCANInfo.data[2] << 16)) | (recvCANInfo.data[1] << 8)) | recvCANInfo.data[0]));
                        Utility.CopyData(opts.Result, "REMAIN_CAPACITY", (UInt64)((((recvCANInfo.data[7] << 24) | (recvCANInfo.data[6] << 16)) | (recvCANInfo.data[5] << 8)) | recvCANInfo.data[4]));

                        Utility.CopyData(recvCANInfo.result, "FULL_CHARGE_CAPACITY", opts.Result["FULL_CHARGE_CAPACITY"]);
                        Utility.CopyData(recvCANInfo.result, "REMAIN_CAPACITY", opts.Result["REMAIN_CAPACITY"]);
                        break;
                    }
                case 0x484:
                case 0x507:
                    {
                        Utility.CopyData(opts.Result, "CAN_batteryId", (Int64)((((recvCANInfo.data[3] << 24) | (recvCANInfo.data[2] << 16)) | (recvCANInfo.data[1] << 8)) | recvCANInfo.data[0]));
                        Utility.CopyData(opts.Result, "CAN_PACK_STATE", (Int64)(recvCANInfo.data[4]));

                        Utility.CopyData(recvCANInfo.result, "CAN_batteryId", opts.Result["CAN_batteryId"]);
                        Utility.CopyData(recvCANInfo.result, "CAN_PACK_STATE", opts.Result["CAN_PACK_STATE"]);
                        break;
                    }
                default:
                    {
                        opts.Error = "No Support CANID";
                        break;
                    }
            }

            if (opts.Result != null && opts.Result.Count > 0)
            {
                opts.RecvCANInfoList.Add(recvCANInfo);
            }
        }
        
        private static void ParseData(BatOptions opts)
        {
            int len = opts.UnpackStream[0];
            int dataLen = (opts.EnableASCII) ? (len - 2) : (len - 3);
            if ((dataLen + 3) != opts.UnpackStream.Length)
            {
                opts.Error = "Recev Stream Error";
                return;
            }

            if (opts.IsCANMode)
            {
                BatCore.ParseCANData(opts);
                return;
            }

            byte cmd = opts.UnpackStream[2];
            byte para = opts.UnpackStream[1];
            byte[] data = null;
            Array.Resize(ref data, opts.UnpackStream.Length - 3);
            Array.Copy(opts.UnpackStream, 3, data, 0, opts.UnpackStream.Length - 3);

            opts.Result = new Dictionary<string, object>();
            try
            {
                DefaultParseBatCmd cmdCode = (DefaultParseBatCmd)cmd;
                switch (cmdCode)
                {
                    case DefaultParseBatCmd.MCU_EEP:
                        {
                            if (dataLen == 0)
                            {
                                opts.Error = "EEP length is Zero";
                                return;
                            }
                            if (opts.Para == 0x00)
                            {
                                Utility.CopyData(opts.Result, "MCU_EEP_INDEX", (int)(data[0]));
                            }
                            else
                            {
                                Dictionary<string, byte[]> value = new Dictionary<string, byte[]>();
                                value.Add(string.Format("{0}", para), data);
                                Utility.CopyData(opts.Result, "MCU_EEP", value);
                            }
                            break;
                        }
                    case DefaultParseBatCmd.MCU_EEP_H:
                    case DefaultParseBatCmd.MCU_EEP_L:
                        {
                            Dictionary<string, byte[]> value = new Dictionary<string, byte[]>();
                            value.Add(((DefaultParseBatCmd)cmd == DefaultParseBatCmd.MCU_EEP_H) ? "1" : "2", data);
                            Utility.CopyData(opts.Result, "MCU_EEP", value);
                            break;
                        }
                    case DefaultParseBatCmd.PB_EEP_OZ1:
                    case DefaultParseBatCmd.PB_EEP_OZ2:
                        {
                            if (dataLen == 0)
                            {
                                opts.Error = "EEP length is Zero";
                                return;
                            }
                            Dictionary<string, byte[]> value = new Dictionary<string, byte[]>();
                            value.Add(((DefaultParseBatCmd)cmd == DefaultParseBatCmd.PB_EEP_OZ1) ? "1" : "2", data);
                            Utility.CopyData(opts.Result, "PB_EEP", value);
                            break;
                        }
                    case DefaultParseBatCmd.CHARGE_LOG_1:
                    case DefaultParseBatCmd.CHARGE_LOG_2:
                    case DefaultParseBatCmd.CHARGE_LOG_3:
                        {
                            Dictionary<string, byte[]> value = new Dictionary<string, byte[]>();
                            value.Add(string.Format("{0}", opts.SetCmd), data);
                            Utility.CopyData(opts.Result, "LOG_CHARGE", value);
                            break;
                        }
                    case DefaultParseBatCmd.DISCHARGE_LOG:
                        {
                            Utility.CopyData(opts.Result, "LOG_DISCHARGE", data);
                            break;
                        }
                    case DefaultParseBatCmd.MCU_VERSION:
                    case DefaultParseBatCmd.DEVICE_NAME:
                    case DefaultParseBatCmd.CIPHER_UUID:
                        {
                            if (dataLen == 0)
                            {
                                opts.Error = "Array length is Zero";
                                return;
                            }
                            string value = "";
                            for (int i = 0; i < data.Length; i++)
                                value += String.Format("{0}", (char)data[i]).ToString().Trim('\0');

                            Utility.CopyData(opts.Result, cmdCode.ToString(), value.Trim());
                            break;
                        }
                    case DefaultParseBatCmd.CELL_VOLT:
                        {
                            UInt64 TOTAL_VOLT = 0;
                            UInt32 MIN_VOLT = 0;
                            UInt32 MAX_VOLT = 0;
                            UInt32 DELTA_VOLT = 0;
                            Int32 CELL_NUM = 0;
                            List<uint> CELL_VOLT = new List<uint>();
                            if (opts.EnableASCII)
                            {
                                int num = (data[4] + data[5]);
                                for (int i = 0; i < num; i += 1)
                                {
                                    CELL_VOLT.Add((uint)((data[(i * 2) + 6] << 8) + (data[(i * 2) + 7])));
                                }
                            }
                            else
                            {
                                for (int i = 0; i < dataLen; i += 2)
                                {
                                    CELL_VOLT.Add(Convert.ToUInt32(((data[i] | (data[i + 1] << 8)) >> 3) * 1.2207));
                                }
                            }

                            int cellLen = CELL_VOLT.Count;
                            if (cellLen > 20 && (cellLen % 2) == 1)
                            { // drop down last cell for 7244
                                CELL_VOLT.RemoveRange(cellLen - 1, 1);
                            }
                            else if (cellLen > 14 && (cellLen % 2) == 1)
                            { // drop down middle cell for other case
                                CELL_VOLT.RemoveRange((cellLen / 2), 1);
                            }
                            CELL_NUM = CELL_VOLT.Count;

                            for (int i = 0; i < CELL_NUM; i++)
                            {
                                uint val = CELL_VOLT[i];
                                if (i == 0)
                                {
                                    MIN_VOLT = val;
                                    MAX_VOLT = val;
                                }
                                else
                                {
                                    if (MIN_VOLT > val)
                                    {
                                        MIN_VOLT = val;
                                    }
                                    if (MAX_VOLT < val)
                                    {
                                        MAX_VOLT = val;
                                    }
                                }
                                TOTAL_VOLT += val;
                            }
                            DELTA_VOLT = MAX_VOLT - MIN_VOLT;

                            Utility.CopyData(opts.Result, "CELL_VOLT", CELL_VOLT.ToArray());
                            Utility.CopyData(opts.Result, "TOTAL_VOLT", TOTAL_VOLT);
                            Utility.CopyData(opts.Result, "CELL_NUM", CELL_NUM);
                            Utility.CopyData(opts.Result, "MAX_VOLT", MAX_VOLT);
                            Utility.CopyData(opts.Result, "MIN_VOLT", MIN_VOLT);
                            Utility.CopyData(opts.Result, "DELTA_VOLT", DELTA_VOLT);
                            break;
                        }
                    case DefaultParseBatCmd.CURRENT:
                        {
                            Utility.CopyData(opts.Result, cmdCode.ToString(), (Int64)((((data[0] << 24) | (data[1] << 16)) | (data[2] << 8)) | data[3]));
                            break;
                        }
                    case DefaultParseBatCmd.DESIGN_CAPACITY:
                    case DefaultParseBatCmd.REMAIN_CAPACITY:
                    case DefaultParseBatCmd.FULL_CHARGE_CAPACITY:
                        {
                            UInt64 value = (UInt64)((((data[0] << 24) | (data[1] << 16)) | (data[2] << 8)) | data[3]);
                            if (!opts.EnableASCII)
                            {
                                value /= 3600;
                            }
                            Utility.CopyData(opts.Result, cmdCode.ToString(), value);
                            break;
                        }
                    case DefaultParseBatCmd.SOC:
                        {
                            UInt32 value = (UInt32)((opts.EnableASCII) ? data[0] : ((data[0] << 8) | data[1]));
                            Utility.CopyData(opts.Result, cmdCode.ToString(), value);
                            break;
                        }
                    case DefaultParseBatCmd.BARCODE:
                        {
                            if (dataLen != 5)
                            {
                                // Not Implement for 4816, Fixed by Neil 2016.7.21
                                opts.Error = "Not support BARCODE CMD";
                                return;
                            }
                            int manufacturer = (data[0] >> 4);
                            int productClass = (data[0] & 0x0F);
                            int year = data[1];
                            int week = data[2];
                            ulong serial = (ulong)data[3] << 8 | (ulong)data[4];
                            Utility.CopyData(opts.Result, cmdCode.ToString(), string.Format("{0:0}{1:0}{2:00}{3:00}{4:0000}", manufacturer, productClass, year, week, serial));

                            break;
                        }
                    case DefaultParseBatCmd.TEMPERATURE:
                        {
                            List<Double> CELL_TEMPERATURE = new List<Double>();
                            if (opts.EnableASCII)
                            {
                                Double tempZero = 2731.2;
                                Double tempDegree = 10.0;

                                int num = data[0] + data[1]; // total base dwon number (n66 + n88)
                                for (int i = 0; i < num; i += 1)
                                {
                                    Double value = (((data[2 + (i * 2)] << 8) | data[3 + (i * 2)]) - tempZero) / tempDegree;
                                    CELL_TEMPERATURE.Add(value);
                                }
                            }
                            else
                            {
                                for (int i = 0; i < data.Length; i += 2)
                                {
                                    CELL_TEMPERATURE.Add(((data[i + 1] << 5) | (data[i] >> 3)));
                                }
                            }
                            Utility.CopyData(opts.Result, "CELL_TEMPERATURE", CELL_TEMPERATURE.ToArray());
                            break;
                        }
                    case DefaultParseBatCmd.PROTECTION:
                        {
                            Int32 value = 0;

                            if ((string)opts.SetCmd == "2.50Rev.09")
                            {
                                value |= ((((data[0] >> 0) & 0x01)) << 0); //   OV
                                value |= ((((data[0] >> 1) & 0x01) | ((data[0] >> 7) & 0x01)) << 1); // UV
                                value |= ((((data[0] >> 3) & 0x01)) << 2);  //   COC
                                value |= ((((data[0] >> 4) & 0x01)) << 3);  //   DOC
                                value |= ((((data[0] >> 5) & 0x01) | ((data[0] >> 6) & 0x01)) << 4);  //   OT
                                value |= (0 << 5);  //   UT
                                value |= (0 << 6);  //   SC

                                value |= ((((data[0] >> 4) & 0x01)) << 7);  //   CDS
                                value |= ((((data[1] >> 0) & 0x01)) << 8);  //   Enable Charge
                                value |= ((((data[1] >> 1) & 0x01)) << 9);  //   Enable Discharge
                                value |= ((((data[1] >> 2) & 0x01)) << 10);  //   Discharging
                                value |= ((((data[1] >> 3) & 0x01)) << 11);  //   Charging
                            }
                            else
                            {
                                for (int i = 0; i < dataLen; i += 2)
                                {
                                    value |= (((data[i] >> 0) & 0x01) << 0); //   OV
                                    value |= (((data[i] >> 1) & 0x01) << 1); // UV
                                    value |= ((((data[i] >> 2) & 0x01) & ((data[i + 1] >> 3) & 0x01)) << 2);  //   COC
                                    value |= ((((data[i] >> 2) & 0x01) & ((data[i + 1] >> 2) & 0x01)) << 3);  //   DOC
                                    value |= (((data[i] >> 4) & 0x01) << 4);  //   OT
                                    value |= (((data[i] >> 5) & 0x01) << 5);  //   UT
                                    value |= (((data[i] >> 3) & 0x01) << 6);  //   SC
                                }
                            }
                            Utility.CopyData(opts.Result, cmdCode.ToString(), value);
                            break;
                        }
                    case DefaultParseBatCmd.LAST_CHARGE:
                    case DefaultParseBatCmd.LAST_DISCHARGE:
                        {
                            List<int> value = new List<int>();
                            value.Add(data[0] + Utility.DEFAULTYEAR);
                            value.Add(data[1]);
                            value.Add(data[2]);
                            value.Add(data[3]);
                            value.Add(data[4]);
                            value.Add(data[5]);
                            value.Add((opts.EnableASCII) ? data[12] : data[6]);
                            Utility.CopyData(opts.Result, cmdCode.ToString(), string.Format("{0:0000}-{1:00}-{2:00}T{3:00}:{4:00}:{5:00} {6:000}%", value[0], value[1], value[2], value[3], value[4], value[5], value[6]));
                            break;
                        }
                    case DefaultParseBatCmd.RTC_REG_DUMP:
                        {
                            if (dataLen > 6)
                            {
                                int year = (int.Parse((data[6]).ToString("X")) + Utility.DEFAULTYEAR);
                                int month = (int.Parse((data[5] & 0x1F).ToString("X")));
                                int day = (int.Parse((data[4] & 0x3F).ToString("X")));
                                int hour = (int.Parse((data[2] & 0x3F).ToString("X")));
                                int min = (int.Parse((data[1] & 0x7F).ToString("X")));
                                int sec = (int.Parse((data[0] & 0x7F).ToString("X")));
                                Utility.CopyData(opts.Result, "RTC", DateTime.Parse(string.Format("{0:0000}-{1:00}-{2:00}T{3:00}:{4:00}:{5:00}.000Z", year, month, day, hour, min, sec)).ToUniversalTime());
                            }
                            break;
                        }
                    case DefaultParseBatCmd.RTC_TIMESET:
                        {
                            int year = data[0] + Utility.DEFAULTYEAR;
                            int month = data[1];
                            int day = data[2];
                            int hour = data[3];
                            int min = data[4];
                            int sec = data[5];
                            Utility.CopyData(opts.Result, "RTC", DateTime.Parse(string.Format("{0:0000}-{1:00}-{2:00}T{3:00}:{4:00}:{5:00}.000Z", year, month, day, hour, min, sec)));
                            break;
                        }
                    case DefaultParseBatCmd.DESIGN_VOLTAGE:
                        {
                            ulong value = (ulong)((opts.EnableASCII) ? ((((data[0] << 24) | (data[1] << 16)) | (data[2] << 8)) | data[3]) : (((data[0] << 8) | data[1]) * 1000));
                            Utility.CopyData(opts.Result, cmdCode.ToString(), value);
                            break;
                        }
                    case DefaultParseBatCmd.BATTERY_ID:
                    case DefaultParseBatCmd.CYCLE_COUNT:
                    case DefaultParseBatCmd.MCU_RESET_COUNT:
                        {
                            Utility.CopyData(opts.Result, cmdCode.ToString(), (uint)((data[0] << 8) | data[1]));
                            break;
                        }
                    case DefaultParseBatCmd.MANUFACTURE_DATE:
                        {
                            int tmpValue = (data[0] << 8) | data[1];
                            int tmpYear = ((tmpValue >> 9) & 0x0000007F) + Utility.DEFAULTYEAR;
                            if (tmpYear > DateTime.Now.Year)
                            {
                                tmpYear -= 20;
                            }
                            Utility.CopyData(opts.Result, cmdCode.ToString(), new Int32[] { tmpYear, (tmpValue >> 5) & 0x0000000F, tmpValue & 0x0000001F });
                            break;
                        }
                    //case DefaultParseBatCmd.OV_HAPPENTIME:
                    //case DefaultParseBatCmd.UV_HAPPENTIME:
                    //case DefaultParseBatCmd.COC_HAPPENTIME:
                    //case DefaultParseBatCmd.DOC_HAPPENTIME:
                    //case DefaultParseBatCmd.UT_HAPPENTIME:
                    //case DefaultParseBatCmd.OT_HAPPENTIME:
                    //case DefaultParseBatCmd.SC_HAPPENTIME:
                    //    {
                    //        List<int> value = new List<int>();
                    //        value.Add(data[0] + Utility.DEFAULTYEAR);
                    //        value.Add(data[1]);
                    //        value.Add(data[2]);
                    //        value.Add(data[3]);
                    //        value.Add(data[4]);
                    //        value.Add(data[5]);
                    //        if (opts.EnableASCII)
                    //        {
                    //            value.Add((data[6] << 8) + (data[7]));
                    //            value.Add((data[8] << 8) + (data[9]));
                    //            value.Add((data[10] << 8) + (data[11]));
                    //            value.Add(data[12]);
                    //        }
                    //        else
                    //        {
                    //            int vMin = (int)(((data[6] << 8) + (data[7])) * 1.2207);
                    //            int vMax = (int)(((data[8] << 8) + (data[9])) * 1.2207);
                    //            value.Add((vMin > 0xFFFF) ? 0 : vMin);
                    //            value.Add((vMax > 0xFFFF) ? 0 : vMax);
                    //            value.Add((data[10] << 8) + (data[11]));
                    //            value.Add(data[12]);
                    //        }
                    //        Utility.CopyData(opts.Result, cmdCode.ToString(), value.ToArray());
                    //        break;
                    //    }
                    case DefaultParseBatCmd.FCC_RESET_TIME:
                    case DefaultParseBatCmd.RTC_RESET_TIME:
                        {
                            List<int> value = new List<int>();
                            value.Add(data[0] + Utility.DEFAULTYEAR);
                            value.Add(data[1]);
                            value.Add(data[2]);
                            value.Add(data[3]);
                            value.Add(data[4]);
                            value.Add(((data[5] << 8) + (data[6])));
                            Utility.CopyData(opts.Result, cmdCode.ToString(), value.ToArray());

                            break;
                        }
                    case DefaultParseBatCmd.DISCHARGE_INTERVAL:
                    case DefaultParseBatCmd.TEMPERATURE_INTERVAL:
                        {
                            if (dataLen == 0)
                            {
                                opts.Error = "array is Zero";
                                return;
                            }
                            List<Int32> value = new List<Int32>();
                            if (opts.Para == 0x00)
                            {
                                for (int i = 0; i < dataLen; i += 1)
                                {
                                    value.Add((Int32)data[i]); // c
                                }

                                Utility.CopyData(opts.Result, cmdCode.ToString(), value.ToArray());
                            }
                            else
                            {
                                int num = dataLen / 4;
                                for (int i = 0; i < num; i += 1)
                                {
                                    value.Add((data[i * 4] << 24) + (data[(i * 4) + 1] << 16) + (data[(i * 4) + 2] << 8) + data[(i * 4) + 3]);
                                }
                                Utility.CopyData(opts.Result, string.Format("{0}_INFO", cmdCode.ToString()), value.ToArray());
                            }
                            break;
                        }
                    case DefaultParseBatCmd.RTC_YEAR_SET:
                    case DefaultParseBatCmd.RTC_MONTH_SET:
                    case DefaultParseBatCmd.RTC_DAY_SET:
                    case DefaultParseBatCmd.RTC_HOUR_SET:
                    case DefaultParseBatCmd.RTC_MIN_SET:
                    case DefaultParseBatCmd.RTC_SEC_SET:
                    case DefaultParseBatCmd.RESET_PB:
                    case DefaultParseBatCmd.RESET_MCU:
                    case DefaultParseBatCmd.MCU_BYTE_EEP_SET:
                    case DefaultParseBatCmd.PB_BYTE_EEP_OZ1_SET:
                    case DefaultParseBatCmd.PB_BYTE_EEP_OZ2_SET:
                    case DefaultParseBatCmd.PB_BYTE_OP_OZ1_SET:
                    case DefaultParseBatCmd.PB_BYTE_OP_OZ2_SET:
                    case DefaultParseBatCmd.SWITCH_MODE:
                        {
                            Utility.CopyData(opts.Result, cmdCode.ToString(), true);
                            break;
                        }
                    case DefaultParseBatCmd.MCU_BYTE_EEP_GET:
                    case DefaultParseBatCmd.PB_BYTE_EEP_OZ1_GET:
                    case DefaultParseBatCmd.PB_BYTE_EEP_OZ2_GET:
                    case DefaultParseBatCmd.PB_BYTE_OP_OZ1_GET:
                    case DefaultParseBatCmd.PB_BYTE_OP_OZ2_GET:
                    case DefaultParseBatCmd.SYS_MSG:
                    case DefaultParseBatCmd.CAN_MODE_STATUS_GET:
                    case DefaultParseBatCmd.CAN_MODE_STATUS_SET:
                        {
                            break;
                        }
                    default:
                        {
                            opts.Error = string.Format("Not support CMD({0})", cmdCode.ToString());
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                opts.Error = string.Format("Recev CMD({0}) Error", cmd);
            }
        }

        private CipherAPI mCipher = new CipherAES();
        private EEPAPI mCBEEP = new EEPGTMCU();
        private EEPAPI mPBEEP = new EEPOZ890();
        //private GTCAN mGTCAN { get; set; }

        public BatCore()
        {
            mCipher.SetEncryptionKey("qswdefrgthyjukil");
            //mGTCAN = new GTCAN();
        }

        public CmdConfig GetCBCmdConfig(string name)
        {
            return mCBEEP.GetCmdConfigList().FirstOrDefault(item => item.Name == name);
        }

        public CmdConfig GetPBCmdConfig(string name)
        {
            return mPBEEP.GetCmdConfigList().FirstOrDefault(item => item.Name == name);
        }

        public Dictionary<string, object> GetCBEEPToObj(byte[] eep, Dictionary<string, object> obj, string name)
        {
            if (name != null) return mCBEEP.ConvertCmdArrayToObj(name, eep, obj);

            if (obj == null) obj = new Dictionary<string, object>();
            string[] nameList = (from item in mCBEEP.GetCmdConfigList() select item.Name).ToArray();

            foreach (string item in nameList)
            {
                mCBEEP.ConvertCmdArrayToObj(item, eep, obj);
            }

            return obj;
        }
        
        public Dictionary<string, object> GetPBEEPToObj(byte[] eep, Dictionary<string, object> obj, string name)
        {
            if (name != null) return mPBEEP.ConvertCmdArrayToObj(name, eep, obj);

            if (obj == null) obj = new Dictionary<string, object>();
            string[] nameList = (from item in mPBEEP.GetCmdConfigList() select item.Name).ToArray();

            foreach (string item in nameList)
            {
                mPBEEP.ConvertCmdArrayToObj(item, eep, obj);
            }

            return obj;
        }

        public byte[] GetCBEEP(Dictionary<string, object> obj)
        {
            return mCBEEP.GetEEPList(obj, null);
        }

        public List<BatOptions> GetBatOptionsListForCBSettingValue(string name, object value, List<BatOptions> batOptsList, byte[] eep)
        {
            if (batOptsList == null) batOptsList = new List<BatOptions>();

            CmdConfig config = mCBEEP.GetCmdConfig(name);
            if (config == null) return batOptsList;

            byte[] array = mCBEEP.ConvertObjToCmdArray(name, value, eep);

            for (int i = 0; i < array.Length; i++)
            {
                batOptsList.Add(new BatOptions() { Action = "set", CmdName = "MCU_BYTE_EEP", SetPara = Convert.ToByte(config.Address + i), SetData = Convert.ToByte(array[i]) });
            }

            return batOptsList;
        }

        public List<BatOptions> GetBatOptionsListForPBSettingValue(string name, object value, List<BatOptions> batOptsList, byte[] eep, string pbID)
        {
            if (batOptsList == null) batOptsList = new List<BatOptions>();

            CmdConfig config = mPBEEP.GetCmdConfig(name);
            if (config == null) return batOptsList;

            byte[] array = mPBEEP.ConvertObjToCmdArray(name, value, eep);
            if (array.Length > 0) Array.Copy(array, 0, eep, config.Address, array.Length); // 修正已經更新的EEP Array值

            Int32 pbIndex = (pbID == "1") ? 1 : 2;
            for (int i = 0; i < array.Length; i++)
            {
                batOptsList.Add(new BatOptions() { Action = "set", CmdName = "PB_BYTE_EEP", SetCmd = pbIndex, SetPara = Convert.ToByte(config.Address + i), SetData = array[i] });
            }

            return batOptsList;
        }

        public void SetMsg(BatOptions opts)
        {
            BatCore.SetCmdInfo(opts);
            if (opts.Error != null)
            {
                return;
            }

            this.GetCipher(opts);
            if (opts.Error != null)
            {
                return;
            }

            BatCore.PackData(opts);
        }

        public void ParseMsg(BatOptions opts)
        {
            if (opts.DisableParseStream || opts.DataList == null)
            {
                return;
            }

            for (int i = 0; i < opts.DataList.Count; i += 1)
            {
                BatOptions batOpts = new BatOptions();
                batOpts.SetCmd = opts.SetCmd;
                batOpts.Para = opts.Para;
                batOpts.EnableASCII = opts.EnableASCII;
                batOpts.RecvStream = opts.DataList[i].ToArray();
                BatCore.UnpackData(batOpts);

                if (batOpts.Error != null)
                {
                    opts.Error = batOpts.Error;
                }
                else
                {
                    this.GetDecipher(batOpts);
                    if (batOpts.Error != null)
                    {
                        opts.Error = batOpts.Error;
                    }
                    else
                    {
                        BatCore.ParseData(batOpts);
                        if (batOpts.Error != null)
                        {
                            opts.Error = batOpts.Error;
                        }
                    }
                }

                if (batOpts.Error == null)
                {
                    if (batOpts.RecvCANInfoList.Count > 0)
                    {
                        opts.RecvCANInfoList.AddRange(batOpts.RecvCANInfoList);
                    }
                    opts.UnpackStreamList.Add(batOpts.UnpackStream);
                    // 有正確收到並剖析值
                    if (opts.Result == null)
                    {
                        opts.Result = new Dictionary<string, object>();
                    }
                    foreach (var dict in batOpts.Result)
                    {
                        Utility.CopyData(opts.Result, dict.Key, dict.Value);
                    }
                }
            }
        }
        
        private void GetCipher(BatOptions opts)
        {
            if (!opts.EnableASCII || !opts.EnableCipher)
            {
                return;
            }

            int cipherLen = (opts.Len > 16) ? ((opts.Len >> 4) << 4) : (((opts.Len + 16) >> 4) << 4);

            byte[] data = new byte[cipherLen];
            data[0] = opts.Len;
            data[1] = opts.Para;
            data[2] = opts.Cmd;

            if (opts.Data != null)
            {
                for (int i = 0; i < opts.Data.Length; i++)
                {
                    data[i + 3] = opts.Data[i];
                }
            }

            opts.Data = mCipher.EncryptionData(data);
            opts.Len = (byte)(cipherLen + 2);
            opts.Para = 0x00;
            opts.Cmd = (byte)DefaultBatCmd.CIPHER_CMD;
        }

        private void GetDecipher(BatOptions opts)
        {
            if (opts.EnableASCII && (byte)DefaultBatCmd.CIPHER_CMD == opts.UnpackStream[2])
            {
                int encryptedlen = opts.UnpackStream[0] - 2;
                byte[] data = null;
                Array.Resize(ref data, encryptedlen);
                Array.Copy(opts.UnpackStream, 3, data, 0, encryptedlen);
                opts.UnpackStream = mCipher.DecryptionData(data);
            }
        }
    }
}

