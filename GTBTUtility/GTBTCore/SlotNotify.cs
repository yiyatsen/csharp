﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTBTCore
{
    public class SlotNotify
    {
        private static Int64 MINCHARGECURRENT = 199;
        private static Int64 MINDISCHARGECURRENT = -199;
        private static Int32 CHECKCOUNT = 2;

        private static Boolean IsCharging(Int64 current)
        {
            return (current > MINCHARGECURRENT);
        }

        private static Boolean IsDischarging(Int64 current)
        {
            return (current < MINDISCHARGECURRENT);
        }

        private Int32 mCurrentState = 0;
        private Int32 mCheckCount = 0;
        private Int32 mEnableProtection = 0;

        public void Reset()
        {
            mCurrentState = 0;
            mCheckCount = 0;
            mEnableProtection = 0;
        }

        public string[] Check(BatInfo batInfo)
        {
            List<string> tmpEvent = new List<string>();

            Boolean isCharging = SlotNotify.IsCharging(batInfo.CURRENT);
            Boolean isDischarging = SlotNotify.IsDischarging(batInfo.CURRENT);

            if (batInfo.PROTECTION != this.mEnableProtection)
            {
                if ((((batInfo.PROTECTION >> 0) & 0x01) == 1) && (((this.mEnableProtection >> 0) & 0x01) != 1)) tmpEvent.Add("alertOV");
                else if ((((batInfo.PROTECTION >> 1) & 0x01) == 1) && (((this.mEnableProtection >> 1) & 0x01) != 1)) tmpEvent.Add("alertUV");
                else if ((((batInfo.PROTECTION >> 2) & 0x01) == 1) && (((this.mEnableProtection >> 2) & 0x01) != 1)) tmpEvent.Add("alertCOC");
                else if ((((batInfo.PROTECTION >> 3) & 0x01) == 1) && (((this.mEnableProtection >> 3) & 0x01) != 1)) tmpEvent.Add("alertDOC");
                else if ((((batInfo.PROTECTION >> 4) & 0x01) == 1) && (((this.mEnableProtection >> 4) & 0x01) != 1)) tmpEvent.Add("alertOT");
                else if ((((batInfo.PROTECTION >> 5) & 0x01) == 1) && (((this.mEnableProtection >> 5) & 0x01) != 1)) tmpEvent.Add("alertUT");
                else if ((((batInfo.PROTECTION >> 6) & 0x01) == 1) && (((this.mEnableProtection >> 6) & 0x01) != 1)) tmpEvent.Add("alertSC");

                this.mEnableProtection = batInfo.PROTECTION;
            }

            if (this.mCurrentState > 0)
            {
                if (isCharging) this.mCheckCount = CHECKCOUNT;
                else if (isDischarging)
                {
                    this.mCheckCount = CHECKCOUNT;
                    this.mCurrentState = -1;
                    tmpEvent.Add("stopCharging");
                    tmpEvent.Add("startDischarging");
                }
                else
                {
                    this.mCheckCount -= 1;
                    if (this.mCheckCount < 1)
                    {
                        this.mCurrentState = 0;
                        tmpEvent.Add("stopCharging");
                    }
                }
            }
            else if (this.mCurrentState < 0)
            {
                if (isDischarging) this.mCheckCount = CHECKCOUNT;
                else if (isCharging)
                {
                    this.mCheckCount = CHECKCOUNT;
                    this.mCurrentState = 1;
                    tmpEvent.Add("stopDischarging");
                    tmpEvent.Add("startCharging");
                }
                else
                {
                    this.mCheckCount -= 1;
                    if (this.mCheckCount < 1)
                    {
                        this.mCurrentState = 0;
                        tmpEvent.Add("stopDischarging");
                    }
                }
            }
            else if (isCharging || isDischarging)
            {
                this.mCheckCount = CHECKCOUNT;
                if (isCharging)
                {
                    this.mCurrentState = 1;
                    tmpEvent.Add("startCharging");
                }
                else
                {
                    this.mCurrentState = -1;
                    tmpEvent.Add("startDischarging");
                }
            }

            return tmpEvent.ToArray();
        }
    }
}
