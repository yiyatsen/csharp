﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTBTCore.Cipher
{
    public abstract class CipherAPI
    {
        /// <summary>
        /// 設定使用的密鑰
        /// </summary>
        /// <param name="encryptionKey">密鑰</param>
        public abstract void SetEncryptionKey(string encryptionKey);
        /// <summary>
        /// 將資料進行加密
        /// </summary>
        /// <param name="inputData">需要加密的資料</param>
        /// <returns>加密後的資料</returns>
        public abstract byte[] EncryptionData(byte[] inputData);
        /// <summary>
        /// 將資料進行解密
        /// </summary>
        /// <param name="inputData">需要解密的資料</param>
        /// <returns>解密後的資料</returns>
        public abstract byte[] DecryptionData(byte[] inputData);
    }

}
