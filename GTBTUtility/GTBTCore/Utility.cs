﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace GTBTCore
{
    public class Utility
    {
        private static string mEngPwd { get; set; }

        public static string EngPwd
        {
            get
            {
                if (mEngPwd == null)
                {            //  cmd: wmic cpu get processorid
                    ManagementClass mc = new ManagementClass("win32_processor");
                    ManagementObjectCollection moc = mc.GetInstances();

                    foreach (ManagementObject mo in moc)
                    {
                        string engKey = mo.Properties["processorID"].Value.ToString();
                        if (engKey != null && engKey != "" && engKey.Length > 2)
                        {
                            mEngPwd = string.Format("gte{0}{1}", engKey.Substring(engKey.Length - 2, 2), engKey.Substring(0, 2));
                            break;
                        }
                    }

                    if (mEngPwd == null) mEngPwd = "gte0000";
                }

                return mEngPwd;
            }
        }

        public static bool EnableEngMode { get; set; }

        public const string Separator = "===============================================================\r\n";

        public const int DEFAULTYEAR = 2000;

        public static string Version { get { return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(); } }
        
        public static byte[] ConcatByteArray(Dictionary<string, byte[]> data)
        {
            List<byte> newArray = new List<byte>();

            foreach (var item in data) newArray = newArray.Concat(item.Value.ToList()).ToList();

            return newArray.ToArray();
        }

        public static void CopyData(Dictionary<string, object> output, string key, object value)
        {
            if (output.ContainsKey(key))
            {
                output[key] = value;
            }
            else
            {
                output.Add(key, value);
            }
        }

        public static Dictionary<string, object> GetDataByKey(string[] keyList, Dictionary<string, object> data)
        {
            Dictionary<string, object> tmpBatInfo = new Dictionary<string, object>();

            int len = keyList.Length;
            for (int i = 0; i < len; i += 1)
            {
                if (data.ContainsKey(keyList[i]))
                {
                    CopyData(tmpBatInfo, keyList[i], data[keyList[i]]);
                }
            }

            return tmpBatInfo;
        }

        public static void CopyByteDataList(Dictionary<string, byte[]> input, Dictionary<string, byte[]> output)
        {
            if (input == null) return;
            if (output == null) output = new Dictionary<string, byte[]>();

            foreach (var item in input)
            {
                if (output.ContainsKey(item.Key))
                {
                    output[item.Key] = item.Value;
                }
                else
                {
                    output.Add(item.Key, item.Value);
                }
            }
        }

        public static Byte ConvertHEXToASCII(Byte inputData)
        {
            if (inputData < 0x0A)
            {
                return (Byte)(inputData + 0x30);
            }
            else
            {
                return (Byte)(inputData - 10 + 0x41);
            }
        }

        public static Byte ConvertASCIIToHEX(Byte inputData)
        {
            if (inputData >= 0x41)
            {
                return (Byte)(inputData - 0x41 + 10);
            }
            else if (inputData >= 0x30)
            {
                return (Byte)(inputData - 0x30);
            }
            else
            {
                return inputData;
            }
        }
        
        public static UInt32 PushChar(List<Byte> array, Byte data, Boolean enableASCII)
        {
            if (enableASCII)
            {
                array.Add(ConvertHEXToASCII((Byte)(data >> 4)));
                array.Add(ConvertHEXToASCII((Byte)(data & 0x0F)));
            }
            else
            {
                array.Add(data);
            }
            return (UInt32)data;
        }

        /// <summary>
        /// SO8601標準
        /// 年份/週別 => 年/月/日
        /// </summary>
        /// <param name="year">年份</param>
        /// <param name="weekOfYear">週別</param>
        /// <returns></returns>
        public static Int32[] ISO8601WeekNumToThursday(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1); //當年分第一天
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;//移動至星期三

            DateTime firstThursday = jan1.AddDays(daysOffset);//當周星期四時間
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            firstThursday = firstThursday.AddDays(weekNum * 7);

            return new Int32[] { firstThursday.Year, firstThursday.Month, firstThursday.Day };
        }

        public static string ConvertObjToString(object value)
        {
            try
            {
                return JsonConvert.SerializeObject(value);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static object ConvertStringToObj(string type, string value)
        {
            try
            {
                object tmpValue = JsonConvert.DeserializeObject(value);
                switch (type)
                {
                    case "System.DateTime":
                        {
                            return tmpValue;
                        }
                    case "System.Double":
                    case "System.UInt64":
                    case "System.UInt32":
                    case "System.Int64":
                    case "System.Int32":
                    case "System.Byte":
                    case "System.String":
                        {
                            Type tmpType = Type.GetType(type);
                            if (tmpType != null) return Convert.ChangeType(tmpValue, tmpType);
                            return null;
                        }
                    default:
                        {
                            Type tmpType = Type.GetType(type);
                            return ((Newtonsoft.Json.Linq.JArray)tmpValue).ToObject(tmpType);
                        }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string OperatorToString(CmdOperatorType optr)
        {
            switch (optr)
            {
                case CmdOperatorType.Equal:
                    {
                        return "==";
                    }
                case CmdOperatorType.NotEqual:
                    {
                        return "!=";
                    }
                case CmdOperatorType.LessThan:
                    {
                        return ">";
                    }
                case CmdOperatorType.LessThanOrEqual:
                    {
                        return ">=";
                    }
                case CmdOperatorType.MoreThan:
                    {
                        return "<";
                    }
                case CmdOperatorType.MoreThanOrEqual:
                    {
                        return "<=";
                    }
                default:
                    {
                        return "";
                    }
            }
        }

        public static bool IsMatchOperator(string type, string baseVal, string realVal, CmdOperatorType optr)
        {
            if (optr == CmdOperatorType.NonOperator) return true;
            switch (type)
            {
                case "System.Boolean":
                    {
                        Boolean result1 = (Boolean)ConvertStringToObj(type, baseVal);
                        Boolean result2 = (Boolean)ConvertStringToObj(type, realVal);

                        return (optr == CmdOperatorType.NotEqual) ? (result1 != result2) : (result1 == result2);
                    }
                case "System.Double":
                    {
                        switch (optr)
                        {
                            case CmdOperatorType.NotEqual:
                                {
                                    return (System.Double)ConvertStringToObj(type, baseVal) != (System.Double)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.MoreThan:
                                {
                                    return (System.Double)ConvertStringToObj(type, baseVal) < (System.Double)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.MoreThanOrEqual:
                                {
                                    return (System.Double)ConvertStringToObj(type, baseVal) <= (System.Double)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.LessThan:
                                {
                                    return (System.Double)ConvertStringToObj(type, baseVal) > (System.Double)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.LessThanOrEqual:
                                {
                                    return (System.Double)ConvertStringToObj(type, baseVal) >= (System.Double)ConvertStringToObj(type, realVal);
                                }
                            default:
                                {
                                    return (System.Double)ConvertStringToObj(type, baseVal) == (System.Double)ConvertStringToObj(type, realVal);
                                }
                        }
                    }
                case "System.UInt64":
                    {
                        switch (optr)
                        {
                            case CmdOperatorType.NotEqual:
                                {
                                    return (System.UInt64)ConvertStringToObj(type, baseVal) != (System.UInt64)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.MoreThan:
                                {
                                    return (System.UInt64)ConvertStringToObj(type, baseVal) < (System.UInt64)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.MoreThanOrEqual:
                                {
                                    return (System.UInt64)ConvertStringToObj(type, baseVal) <= (System.UInt64)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.LessThan:
                                {
                                    return (System.UInt64)ConvertStringToObj(type, baseVal) > (System.UInt64)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.LessThanOrEqual:
                                {
                                    return (System.UInt64)ConvertStringToObj(type, baseVal) >= (System.UInt64)ConvertStringToObj(type, realVal);
                                }
                            default:
                                {
                                    return (System.UInt64)ConvertStringToObj(type, baseVal) == (System.UInt64)ConvertStringToObj(type, realVal);
                                }
                        }
                    }
                case "System.UInt32":
                    {
                        switch (optr)
                        {
                            case CmdOperatorType.NotEqual:
                                {
                                    return (System.UInt32)ConvertStringToObj(type, baseVal) != (System.UInt32)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.MoreThan:
                                {
                                    return (System.UInt32)ConvertStringToObj(type, baseVal) < (System.UInt32)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.MoreThanOrEqual:
                                {
                                    return (System.UInt32)ConvertStringToObj(type, baseVal) <= (System.UInt32)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.LessThan:
                                {
                                    return (System.UInt32)ConvertStringToObj(type, baseVal) > (System.UInt32)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.LessThanOrEqual:
                                {
                                    return (System.UInt32)ConvertStringToObj(type, baseVal) >= (System.UInt32)ConvertStringToObj(type, realVal);
                                }
                            default:
                                {
                                    return (System.UInt32)ConvertStringToObj(type, baseVal) == (System.UInt32)ConvertStringToObj(type, realVal);
                                }
                        }
                    }
                case "System.Int64":
                    {
                        switch (optr)
                        {
                            case CmdOperatorType.NotEqual:
                                {
                                    return (System.Int64)ConvertStringToObj(type, baseVal) != (System.Int64)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.MoreThan:
                                {
                                    return (System.Int64)ConvertStringToObj(type, baseVal) < (System.Int64)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.MoreThanOrEqual:
                                {
                                    return (System.Int64)ConvertStringToObj(type, baseVal) <= (System.Int64)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.LessThan:
                                {
                                    return (System.Int64)ConvertStringToObj(type, baseVal) > (System.Int64)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.LessThanOrEqual:
                                {
                                    return (System.Int64)ConvertStringToObj(type, baseVal) >= (System.Int64)ConvertStringToObj(type, realVal);
                                }
                            default:
                                {
                                    return (System.Int64)ConvertStringToObj(type, baseVal) == (System.Int64)ConvertStringToObj(type, realVal);
                                }
                        }
                    }
                case "System.Int32":
                    {
                        switch (optr)
                        {
                            case CmdOperatorType.NotEqual:
                                {
                                    return (System.Int32)ConvertStringToObj(type, baseVal) != (System.Int32)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.MoreThan:
                                {
                                    return (System.Int32)ConvertStringToObj(type, baseVal) < (System.Int32)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.MoreThanOrEqual:
                                {
                                    return (System.Int32)ConvertStringToObj(type, baseVal) <= (System.Int32)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.LessThan:
                                {
                                    return (System.Int32)ConvertStringToObj(type, baseVal) > (System.Int32)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.LessThanOrEqual:
                                {
                                    return (System.Int32)ConvertStringToObj(type, baseVal) >= (System.Int32)ConvertStringToObj(type, realVal);
                                }
                            default:
                                {
                                    return (System.Int32)ConvertStringToObj(type, baseVal) == (System.Int32)ConvertStringToObj(type, realVal);
                                }
                        }
                    }
                case "System.Byte":
                    {
                        switch (optr)
                        {
                            case CmdOperatorType.NotEqual:
                                {
                                    return (System.Byte)ConvertStringToObj(type, baseVal) != (System.Byte)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.MoreThan:
                                {
                                    return (System.Byte)ConvertStringToObj(type, baseVal) < (System.Byte)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.MoreThanOrEqual:
                                {
                                    return (System.Byte)ConvertStringToObj(type, baseVal) <= (System.Byte)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.LessThan:
                                {
                                    return (System.Byte)ConvertStringToObj(type, baseVal) > (System.Byte)ConvertStringToObj(type, realVal);
                                }
                            case CmdOperatorType.LessThanOrEqual:
                                {
                                    return (System.Byte)ConvertStringToObj(type, baseVal) >= (System.Byte)ConvertStringToObj(type, realVal);
                                }
                            default:
                                {
                                    return (System.Byte)ConvertStringToObj(type, baseVal) == (System.Byte)ConvertStringToObj(type, realVal);
                                }
                        }
                    }
                case "System.DateTime":
                    {
                        DateTime result1 = (DateTime)ConvertStringToObj(type, baseVal);
                        DateTime result2 = (DateTime)ConvertStringToObj(type, realVal);
                        switch (optr)
                        {
                            case CmdOperatorType.NotEqual:
                                {
                                    return result1.Ticks != result2.Ticks;
                                }
                            case CmdOperatorType.MoreThan:
                                {
                                    return result1.Ticks < result2.Ticks;
                                }
                            case CmdOperatorType.MoreThanOrEqual:
                                {
                                    return result1.Ticks <= result2.Ticks;
                                }
                            case CmdOperatorType.LessThan:
                                {
                                    return result1.Ticks > result2.Ticks;
                                }
                            case CmdOperatorType.LessThanOrEqual:
                                {
                                    return result1.Ticks >= result2.Ticks;
                                }
                            default:
                                {
                                    return result1.Ticks == result2.Ticks;
                                }
                        }
                    }
                default:
                    {
                        return baseVal == realVal;
                    }
            }
        }
    }
}
