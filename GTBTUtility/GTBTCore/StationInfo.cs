﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTBTCore
{
    public class StationInfo
    {
        public string stationId { get; set; }
        public string stationType { get; set; }
        public string stationMode { get; set; }
        public string commMode { get; set; }
        public bool disableLogin { get; set; }
        public bool isSupportSchedule { get; set; }
        public bool isRegister { get; set; }

        public StationInfo()
        {
            stationId = "non-Register";
            stationType = "diagnostic";
            stationMode = "Factory";
            commMode = "1To1";
            disableLogin = false;
            isSupportSchedule = false;
            isRegister = false;
        }
    }
}
