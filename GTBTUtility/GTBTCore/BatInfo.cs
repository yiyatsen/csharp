﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTBTCore
{
    public class BatInfo
    {
        public DateTime timestamp { get; set; }
        public DateTime eepTimestamp { get; set; }
        public Int32[] MCU_EEP { get; set; }
        public Dictionary<string, Int32[]> PB_EEP { get; set; }
        public DateTime RTC { get; set; }
        public string MCU_VERSION { get; set; }
        public string DEVICE_NAME { get; set; }
        public string batteryType { get; set; }
        public string batteryId { get; set; }
        public string BARCODE { get; set; }
        public Int32[] MANUFACTURE_DATE { get; set; }
        public UInt32 SOC { get; set; }
        public UInt32 SOH { get; set; }
        public Int64 CURRENT { get; set; }
        public UInt64 TOTAL_VOLT { get; set; }
        public UInt32 MAX_VOLT { get; set; }
        public UInt32 MIN_VOLT { get; set; }
        public UInt32 DELTA_VOLT { get; set; }
        public Int32 CELL_NUM { get; set; }
        public UInt32[] CELL_VOLT { get; set; }
        public Double TEMPERATURE { get; set; }
        public Double MAX_TEMPERATURE { get; set; }
        public Double MIN_TEMPERATURE { get; set; }
        public Double DELTA_TEMPERATURE { get; set; }
        public Double[] CELL_TEMPERATURE { get; set; }
        public Int32 PROTECTION { get; set; }
        public UInt64 DESIGN_VOLTAGE { get; set; }
        public UInt64 DESIGN_CAPACITY { get; set; }
        public UInt64 REMAIN_CAPACITY { get; set; }
        public UInt64 FULL_CHARGE_CAPACITY { get; set; }
        public UInt32 CYCLE_COUNT { get; set; }
        public string LAST_CHARGE { get; set; }
        public string LAST_DISCHARGE { get; set; }
        public int[] DISCHARGE_INTERVAL { get; set; }
        public int[] TEMPERATURE_INTERVAL { get; set; }
    }

    public class BatOptions
    {
        public string CmdName { get; set; }
        public string Action { get; set; }
        public string Error { get; set; }
        public byte STX { get; set; }
        public byte Len { get; set; }
        public byte Para { get; set; }
        public byte Cmd { get; set; }
        public byte[] Data { get; set; }
        public byte ETX { get; set; }
        public bool EnableRecv { get; set; }
        public bool EnableASCII { get; set; }
        public bool EnableCipher { get; set; }
        public bool EnableRecvTimeout { get; set; }
        public bool DisableParseStream { get; set; }
        public bool IsCANMode { get; set; }
        public ulong CANID { get; set; }
        public byte CANType { get; set; }
        public byte[] CANData { get; set; }
        public int FirstDelayTime { get; set; }
        public int Timeout { get; set; }
        public byte SetData { get; set; }
        public object SetCmd { get; set; }
        public object SetPara { get; set; }
        public int SetTimeout { get; set; }
        public byte[] SendStream { get; set; }
        public List<List<byte>> DataList { get; set; }
        public byte[] RecvStream { get; set; }
        public byte[] UnpackStream { get; set; }
        public byte[] OptionStream { get; set; }
        public DateTime SendTime { get; set; }
        public DateTime RecveTime { get; set; }
        public Dictionary<string, object> Result { get; set; }
        public List<BatCore.GTCANRecvInfo> RecvCANInfoList { get; set; }
        public List<byte[]> UnpackStreamList { get; set; }

        public BatOptions()
        {
            CmdName = null;
            Action = "get";
            Error = null;
            STX = BatCore.STX;
            ETX = BatCore.ETX;
            Len = 0x02;
            Para = 0x00;
            FirstDelayTime = 100;
            Timeout = 100;
            SetCmd = null;
            SetPara = null;
            SetTimeout = 100;
            EnableRecv = true;
            EnableASCII = true;
            EnableCipher = true;
            DisableParseStream = false;
            RecvCANInfoList = new List<BatCore.GTCANRecvInfo>();
            UnpackStreamList = new List<byte[]>();
        }
    }
}
