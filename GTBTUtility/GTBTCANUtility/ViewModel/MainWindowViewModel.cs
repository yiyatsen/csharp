﻿using GTBTCANUtility.Model;
using GTBTCore;
using LiveCharts;
using LiveCharts.Wpf;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GTBTCANUtility.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public ICommand ExcuteUtilityDialogCommand { get { return new MainWindowCommand(ExcuteUtilityDialog); } }
        public ICommand StopUtilityDialogCommand { get { return new MainWindowCommand(StopUtilityDialog); } }
        public ICommand EditCommand { get { return new MainWindowCommand(EditConfig); } }
        public ICommand SaveLogCommand { get { return new MainWindowCommand(SaveLog); } }
        public ICommand ExitAppCommand { get { return new MainWindowCommand(ExitApp); } }


        public event PropertyChangedEventHandler PropertyChanged;
        private CancellationTokenSource mCTS { get; set; }
        private Slot mTargetSlot = new Slot() { slotId = 1 };
        private COMPort.COMPortConfig mDefaultConfig = GTBTCore.COMPort.DEFAULTBAUDRATEINFO;
        private bool mIsGetCANConfig { get; set; }
        private List<string> mRawList = new List<string>();
        private readonly ObservableCollection<CANAttrModel> mCANAttrModelList = new ObservableCollection<CANAttrModel>();
        public ObservableCollection<CANAttrModel> CANAttrModelList { get { return mCANAttrModelList; } set { NotifyPropertyChanged("CANAttrModelList"); } }
        public SeriesCollection ChartCollection { get; set; }
        public List<string> ChartXLabels { get; set; }
        public Func<double, string> ChartYLabels { get; set; }

        private bool ISBETA = false;
        private string mAppVersion { get; set; }
        private string mCoreVersion { get; set; }

        public string AppVersion
        {
            get { return mAppVersion; }
            set
            {
                mAppVersion = value;
                NotifyPropertyChanged("AppVersion");
            }
        }

        public string CoreVersion
        {
            get { return mCoreVersion; }
            set
            {
                mCoreVersion = value;
                NotifyPropertyChanged("CoreVersion");
            }
        }

        public System.Windows.Visibility ConnectionVisibility
        {
            get { return (mTargetSlot.isOpen) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set
            {
                NotifyPropertyChanged("ConnectionVisibility");
            }
        }

        public System.Windows.Visibility DisconnectionVisibility
        {
            get { return (!mTargetSlot.isOpen) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set
            {
                NotifyPropertyChanged("DisconnectionVisibility");
            }
        }

        public string COMPort
        {
            get { return string.Format("Port: {0}", (mTargetSlot.path != "") ? mTargetSlot.path : "None"); }
            set
            {
                NotifyPropertyChanged("COMPort");
                NotifyPropertyChanged("ConnectionVisibility");
                NotifyPropertyChanged("DisconnectionVisibility");
            }
        }

        public MainWindowViewModel()
        {
            AppVersion = string.Format("Ver: {0}{1}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(), (ISBETA) ? "(BETA)" : "");
            CoreVersion = string.Format("CoreVer: {0}", GTBTCore.Utility.Version);
            COMPort = "";

            mTargetSlot.autoSetting.enableCANCmd = true;
            foreach (CmdConfig config in BatCore.CANBATPARAMLIST)
            {
                mCANAttrModelList.Add(new CANAttrModel(config.ID, config.Name));
            }

            ResetChartCollection();
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ResetChartCollection()
        {
            if (ChartCollection == null)
            {
                ChartCollection = new SeriesCollection()
                {
                    new LineSeries { Title = "SOC (%)", Values = new ChartValues<double>() },
                    new LineSeries { Title = "CURRENT (A)", Values = new ChartValues<double>() },
                    new LineSeries { Title = "RC (A)", Values = new ChartValues<double>() },
                    new LineSeries { Title = "FCC (A)", Values = new ChartValues<double>() },
                    new LineSeries { Title = "VOLT (V)", Values = new ChartValues<double>() },
                    new LineSeries { Title = "TEMP (°C)", Values = new ChartValues<double>() }
                };
            }

            if (ChartXLabels == null) ChartXLabels = new List<string>();

            foreach (var item in ChartCollection)
            {
                item.Values.Clear();
            }
            ChartXLabels.Clear();
            ChartYLabels = value => value.ToString();
        }
        
        public async Task ScanData(CancellationToken cancellationToken)
        {
            DateTime RecordDT = DateTime.Now;
            UInt32 Counter = 0;
            while (true)
            {
                DateTime dtStart = DateTime.Now;
                if (!mIsGetCANConfig)
                {
                    await mTargetSlot.SendMsg(new BatOptions() {
                        EnableRecv = false,
                        STX = BatCore.CANSTX,
                        CANID = 0x11000001,
                        CANType = 0x01,
                        CANData = new byte[] { 0x01 }
                    });

                    Counter = 0;
                }

                BatOptions batOpts = await mTargetSlot.RecvMsg(new BatOptions() { EnableRecv = true, FirstDelayTime = 0 });
                string dtStr = dtStart.ToString("HH':'mm':'ss");
                if (Counter < 1 ||  (dtStart.Ticks - RecordDT.Ticks) > 1800000000)
                {
                    double tmpSOC = -1;
                    double tmpCURRENT = -1;
                    double tmpRC = -1;
                    double tmpFCC = -1;
                    double tmpVOLT = -1;
                    double tmpTemp = -1;
                    if (batOpts.Result != null)
                    {
                        if (batOpts.Result.ContainsKey("SOC")) tmpSOC = (UInt32)batOpts.Result["SOC"] * 1.0;
                        if (batOpts.Result.ContainsKey("CAN_CURRENT")) tmpCURRENT = (Int64)batOpts.Result["CAN_CURRENT"] * 0.001;
                        if (batOpts.Result.ContainsKey("REMAIN_CAPACITY")) tmpRC = (UInt64)batOpts.Result["REMAIN_CAPACITY"] * 0.001;
                        if (batOpts.Result.ContainsKey("FULL_CHARGE_CAPACITY")) tmpFCC = (UInt64)batOpts.Result["FULL_CHARGE_CAPACITY"] * 0.001;
                        if (batOpts.Result.ContainsKey("TOTAL_VOLT")) tmpVOLT = (UInt64)batOpts.Result["TOTAL_VOLT"] * 0.001;
                        if (batOpts.Result.ContainsKey("TEMPERATURE")) tmpTemp = (Double)batOpts.Result["TEMPERATURE"];
                    }
                    if (ChartXLabels.Count > 1440)
                    {
                        ChartXLabels.RemoveAt(0);
                        ChartCollection[0].Values.RemoveAt(0);
                        ChartCollection[1].Values.RemoveAt(0);
                        ChartCollection[2].Values.RemoveAt(0);
                        ChartCollection[3].Values.RemoveAt(0);
                        ChartCollection[4].Values.RemoveAt(0);
                        ChartCollection[5].Values.RemoveAt(0);
                    }
                    ChartXLabels.Add(dtStr);
                    ChartCollection[0].Values.Add(tmpSOC);
                    ChartCollection[1].Values.Add(tmpCURRENT);
                    ChartCollection[2].Values.Add(tmpRC);
                    ChartCollection[3].Values.Add(tmpFCC);
                    ChartCollection[4].Values.Add(tmpVOLT);
                    ChartCollection[5].Values.Add(tmpTemp);

                    RecordDT = dtStart;
                    if (Counter < 1) Counter++;
                }
                try
                {

                    foreach (var item in batOpts.RecvCANInfoList)
                    {
                        if (0x12000001 == item.canID)
                        {
                            if (item.result.ContainsKey("PACK_CONFIG"))
                            {
                                for (int i = 0; i < CANAttrModelList.Count; i++)
                                {
                                    CANAttrModelList[i].Enable = (((byte)(item.result["PACK_CONFIG"]) >> i) & 0x01) == 0x01;
                                }
                                mIsGetCANConfig = true;
                            }
                        }
                        else
                        {
                            CANAttrModel model = CANAttrModelList.Where(x => x.IsMatchCANID(item.canID)).First();
                            model.TimeStamp = dtStr;
                            model.Sum += 1;
                            model.RawData = "[" + string.Join(",", item.data) + "]";
                            string str = "";
                            foreach (var attr in item.result)
                            {
                                str += string.Format("[{0}: {1}]", attr.Key, BatCore.ConvertCmdToString(attr.Key, attr.Value));
                            }
                            model.Attribute = str;

                            mRawList.Add(string.Format("{0}\t{1}\t{2}\t{3}\r\n", model.TimeStamp.PadRight(10), model.ID.PadRight(30), model.RawData.PadRight(50), model.Attribute));
                            if (mRawList.Count > 864000) mRawList.RemoveAt(0);
                        }
                    }
                }
                catch (Exception err)
                {

                }
                
                await Task.Delay(1000, cancellationToken);
            }
        }

        private async void ExcuteUtilityDialog(object o)
        {
            mRawList.Clear();
            foreach (var item in CANAttrModelList)
            {
                item.Enable = false;
                item.TimeStamp = "";
                item.RawData = "";
                item.Attribute = "";
                item.Sum = 0;
            }

            await DialogHost.Show(new UtilityRunDialog()
            {
                DataContext = new UtilityRunDialogViewModel(mDefaultConfig)
            }, "RootDialog", async (object sender, DialogClosingEventArgs eventArgs) =>
            {
                if (eventArgs.Parameter == null) return;

                ResetChartCollection();

                mDefaultConfig = (COMPort.COMPortConfig)eventArgs.Parameter;

                int ret = mTargetSlot.ConnectCOMPort(mDefaultConfig.COMPort, mDefaultConfig.BaudRate);
                COMPort = "";
                if (ret != 0)
                {
                    await DialogHost.Show(new NotifyDialog()
                    {
                        DataContext = new NotifyDialogViewModel
                        {
                            EnableYesNoQuestion = false,
                            NotifyTitle = "Warning",
                            NotifyMessage = "Please check COM Port!"
                        }
                    }, "NotifyDialog");
                }
                else
                {
                    mCTS = new CancellationTokenSource();
                    ScanData(mCTS.Token);
                }
            });
        }

        private void StopUtilityDialog(object o)
        {
            if (mTargetSlot.isOpen)
            {
                if (mCTS != null) mCTS.Cancel();
                
                mTargetSlot.DisonnectCOMPort();

                mIsGetCANConfig = false;

                COMPort = "";
            }
        }

        private async void EditConfig(object obj)
        {
            if (mTargetSlot.isOpen)
            {
                await DialogHost.Show(new ProgressBarDialog(), "RootDialog", async (object sender, DialogOpenedEventArgs eventargs) => {

                    await Task.Delay(500);
                    int value = 0;

                    for (int i = 0; i < CANAttrModelList.Count; i++)
                    {
                        if (CANAttrModelList[i].Enable) value |= (0x01 << i);
                    }

                    mIsGetCANConfig = false;

                    BatOptions batOpts = await mTargetSlot.SendMsg(new BatOptions()
                    {
                        EnableRecv = false,
                        STX = BatCore.CANSTX,
                        CANID = 0x11000001,
                        CANType = 0x01,
                        CANData = new byte[] { 0x02, (byte)value }
                    });
                    eventargs.Session.Close(false);
                });
            }
        }

        private async void SaveLog(object obj)
        {
            if (mRawList.Count > 0)
            {
                DateTime dt = DateTime.Now;
                try
                {
                    if (!Directory.Exists(MainWindow.LOGPATH)) Directory.CreateDirectory(MainWindow.LOGPATH);

                    string txt = string.Format("{0}\t{1}\t{2}\t{3}\r\n", "TimeStamp".PadRight(10), "ID".PadRight(30), "RawData".PadRight(50), "Attribute");
                    foreach (string str in mRawList)
                    {
                        txt += str;
                    }

                    string file = string.Format("{0}{1}_raw.txt", MainWindow.LOGPATH, dt.ToString("yyyyMMddHHmmss"));
                    File.AppendAllText(file, txt);

                    await DialogHost.Show(new NotifyDialog()
                    {
                        DataContext = new NotifyDialogViewModel
                        {
                            EnableYesNoQuestion = false,
                            NotifyTitle = "Success",
                            NotifyMessage = "Save Log Success!"
                        }
                    }, "NotifyDialog");
                }
                catch (Exception ex)
                {
                    await DialogHost.Show(new NotifyDialog()
                    {
                        DataContext = new NotifyDialogViewModel
                        {
                            EnableYesNoQuestion = false,
                            NotifyTitle = "Alert",
                            NotifyMessage = ex.Message
                        }
                    }, "NotifyDialog");
                }
            }
            else
            {
                await DialogHost.Show(new NotifyDialog()
                {
                    DataContext = new NotifyDialogViewModel
                    {
                        EnableYesNoQuestion = false,
                        NotifyTitle = "Warning",
                        NotifyMessage = "No Record"
                    }
                }, "NotifyDialog");
            }
        }

        private async void ExitApp(object obj)
        {
            await DialogHost.Show(new NotifyDialog()
            {
                DataContext = new NotifyDialogViewModel()
                {
                    EnableYesNoQuestion = true,
                    NotifyTitle = "Warning",
                    NotifyMessage = "Do you want to leave now?"
                }
            }, "RootDialog", (object sender, DialogClosingEventArgs eventArgs) =>
            {
                if ((bool)eventArgs.Parameter == false) return;

                MainWindow.ExitAPP();
            });
        }
    }
}
