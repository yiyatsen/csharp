﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTBTCANUtility.Model
{
    public class CANAttrModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private bool mEnable { get; set; }
        private uint mID { get; set; }
        private string mName { get; set; }
        private int mSum { get; set; }
        private string mTimeStamp { get; set; }
        private string mAttribute { get; set; }
        private string mRawData { get; set; }

        public bool Enable
        {
            get { return mEnable; }
            set
            {
                if (mEnable == value) return;
                mEnable = value;
                NotifyPropertyChanged("Enable");
            }
        }

        public string ID
        {
            get { return string.Format("0x{0:X03} ({1})", mID, mName); }
        }

        public int Sum
        {
            get { return mSum; }
            set
            {
                if (mSum == value) return;
                mSum = value;
                NotifyPropertyChanged("Sum");
            }
        }

        public string TimeStamp
        {
            get { return mTimeStamp; }
            set
            {
                if (mTimeStamp == value) return;
                mTimeStamp = value;
                NotifyPropertyChanged("TimeStamp");
            }
        }

        public string Attribute
        {
            get { return mAttribute; }
            set
            {
                if (mAttribute == value) return;
                mAttribute = value;
                NotifyPropertyChanged("Attribute");
            }
        }

        public string RawData
        {
            get { return mRawData; }
            set
            {
                if (mRawData == value) return;
                mRawData = value;
                NotifyPropertyChanged("RawData");
            }
        }

        public CANAttrModel(uint id, string name)
        {
            mID = id;
            mName = name;
        }

        public bool IsMatchCANID(uint id)
        {
            return (mID == id);
        }

        protected void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
