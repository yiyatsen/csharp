﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTAUOAGVBat
{
    public class COMPort
    {
        public class RecvFormat
        {
            public DateTime timestamp { get; set; }
            public List<byte> data = new List<byte>();
        }
        public class COMPortConfig
        {
            public string port { get; set; }
            public int baudRate { get; set; }
        }

        public string port { get { return (SerialPortObj.IsOpen) ? SerialPortObj.PortName : ""; } }
        public int baudRate { get { return (SerialPortObj.IsOpen) ? SerialPortObj.BaudRate : 0; } }
        public bool isOpen { get { return SerialPortObj.IsOpen; } }
        public List<RecvFormat> recvData = new List<RecvFormat>();
        private SerialPort SerialPortObj = new SerialPort();

        public void Disconnect()
        {
            if (SerialPortObj != null)
            {
                SerialPortObj.DataReceived -= SerialPortObj_DataReceived;
                SerialPortObj.Close();
                recvData.Clear();
            }
        }

        public int Connect(string port, int baudRate)
        {
            try
            {
                this.Disconnect();
                SerialPortObj.PortName = port;
                SerialPortObj.BaudRate = baudRate;
                SerialPortObj.Parity = Parity.Even;
                recvData.Clear();
                SerialPortObj.DataReceived += SerialPortObj_DataReceived;
                SerialPortObj.Open();
                //SerialPortObj.DtrEnable = true;
            }
            catch (Exception err)
            {
                return -1;
            }
            return 0;
        }

        public int SendData(byte[] sendStream)
        {
            try
            {
                SerialPortObj.DiscardOutBuffer();
                SerialPortObj.Write(sendStream, 0, sendStream.Length);
            }
            catch
            {
                return -1;
            }
            return 0;
        }

        private void SerialPortObj_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            byte[] buffer = new byte[1024];
            RecvFormat recvFormat = new RecvFormat();
            while (sp.BytesToRead > 0)
            {
                Int32 receivedLen = sp.Read(buffer, 0, buffer.Length);
                Array.Resize(ref buffer, receivedLen);
                recvFormat.data.AddRange(buffer);
            }
            recvFormat.timestamp = DateTime.UtcNow;
            recvData.Add(recvFormat);
        }
    }
}
