﻿using GTAUOAGVBat.Model;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using static GTAUOAGVBat.COMPort;

namespace GTAUOAGVBat.ViewModel
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        public class ParamConfig
        {
            public string Name { get; set; }
            public string Type { get; set; }
            public string Value { get; set; }
        }

        public ICommand ExcuteUtilityDialogCommand { get { return new MainWindowCommand(ExcuteUtilityDialog); } }
        public ICommand StopUtilityDialogCommand { get { return new MainWindowCommand(StopUtilityDialog); } }
        public ICommand ExitAppCommand { get { return new MainWindowCommand(ExitApp); } }
        public ICommand RefreshValueCommand { get { return new MainWindowCommand(RefreshValue); } }

        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<ParamConfig> mInfo1List = new ObservableCollection<ParamConfig>();
        private ObservableCollection<ParamConfig> mInfo2List = new ObservableCollection<ParamConfig>();

        public ObservableCollection<ParamConfig> BaInfo1List
        {
            get { return mInfo1List; }
            set { NotifyPropertyChanged("BaInfo1List"); }
        }
        public ObservableCollection<ParamConfig> BaInfo2List
        {
            get { return mInfo2List; }
            set { NotifyPropertyChanged("BaInfo2List"); }
        }

        private CancellationTokenSource mCTS { get; set; }
        private bool ISBETA = true;
        private string mAppVersion { get; set; }
        private string mTime { get; set; }
        private string mUserValue1 = "";
        private string mUserValue2 = "";
        private COMPort mCOMPort = new COMPort();
        private COMPortConfig mCOMPortConfig = new COMPortConfig();


        public string AppVersion
        {
            get { return mAppVersion; }
            set
            {
                mAppVersion = value;
                NotifyPropertyChanged("AppVersion");
            }
        }

        public System.Windows.Visibility ConnectionVisibility
        {
            get { return (mCOMPort.isOpen) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set
            {
                NotifyPropertyChanged("ConnectionVisibility");
            }
        }

        public System.Windows.Visibility DisconnectionVisibility
        {
            get { return (!mCOMPort.isOpen) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set
            {
                NotifyPropertyChanged("DisconnectionVisibility");
            }
        }
 
        public string COMPort
        {
            get { return string.Format("Port: {0}", (mCOMPort.port != "") ? mCOMPort.port : "None"); }
            set
            {
                NotifyPropertyChanged("COMPort");
                NotifyPropertyChanged("ConnectionVisibility");
                NotifyPropertyChanged("DisconnectionVisibility");
            }
        }

        public string UserValue1
        {
            get { return mUserValue1; }
            set
            {
                mUserValue1 = value;
                NotifyPropertyChanged("UserValue1");
            }
        }
 
        public string UserValue2
        {
            get { return mUserValue2; }
            set
            {
                mUserValue2 = value;
                NotifyPropertyChanged("UserValue2");
            }
        }
 
        public string Time
        {
            get { return mTime; }
            set
            {
                mTime = value;
                NotifyPropertyChanged("Time");
            }
        }
   
        public enum BatCmd : byte
        {
            Unknow = 0x00,
            GetBatInfo = 0x01,
            SetBatParam = 0x03,
            GetBatParam = 0x04,
            GetBatAlarmStatus = 0x05
        }

        //public enum INSTEK : string
        //{
        //    ISET = 0x00,
        //    VSET = 0x01,
        //    IOUT = 0x03,
        //    VOUT = 0x04,
        //    TRACK = 0x05,
        //    BEEP = 0x05,
        //    OUT = 0x05,
        //    TRACK = 0x05
        //}

        public MainWindowViewModel()
        {
            AppVersion = string.Format("Ver: {0}{1}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(), (ISBETA) ? "(BETA)" : "");
            COMPort = "";
            if (!Directory.Exists(MainWindow.SYS_LOG_PATH)) Directory.CreateDirectory(MainWindow.SYS_LOG_PATH);
            if (!Directory.Exists(MainWindow.LOG_PATH)) Directory.CreateDirectory(MainWindow.LOG_PATH);
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void GetData(ObservableCollection<ParamConfig> info, List<byte> list)
        {
            string value = "";
            info.Clear();
            if (list != null && list.Count > 38)
            {
                int checkSum = 0;
                for (int i = 2; i < 37; i++)
                {
                    checkSum += list[i];
                }

                value = System.Text.Encoding.ASCII.GetString(list.GetRange(37, 2).ToArray());
                info.Add(new ParamConfig() { Name = "Check Sum(LRC)", Value = string.Format("{0}({1})", value, checkSum.ToString("X4")) });

                int index = 0;
                int count = 1;
                if (list.Count > 39)
                {
                    value = BitConverter.ToString(list.GetRange(39, 1).ToArray());
                    info.Add(new ParamConfig() { Name = "End", Value = value });
                }
                value = BitConverter.ToString(list.GetRange(index, count).ToArray());
                info.Add(new ParamConfig() { Name = "Start", Value = value });
                index += count;
                value = System.Text.Encoding.ASCII.GetString(list.GetRange(index, count).ToArray());
                info.Add(new ParamConfig() { Name = "Address", Value = value });
                index += count;
                count = 2;
                value = System.Text.Encoding.ASCII.GetString(list.GetRange(index, count).ToArray());
                info.Add(new ParamConfig() { Name = "ID", Value = value });
                index += count;
                count = 3;
                value = System.Text.Encoding.ASCII.GetString(list.GetRange(index, count).ToArray());
                info.Add(new ParamConfig() { Name = "Cell 1", Value = value });
                index += count;
                value = System.Text.Encoding.ASCII.GetString(list.GetRange(index, count).ToArray());
                info.Add(new ParamConfig() { Name = "Cell 2", Value = value });
                index += count;
                value = System.Text.Encoding.ASCII.GetString(list.GetRange(index, count).ToArray());
                info.Add(new ParamConfig() { Name = "Cell 3", Value = value });
                index += count;
                value = System.Text.Encoding.ASCII.GetString(list.GetRange(index, count).ToArray());
                info.Add(new ParamConfig() { Name = "Cell 4", Value = value });
                index += count;
                value = System.Text.Encoding.ASCII.GetString(list.GetRange(index, count).ToArray());
                info.Add(new ParamConfig() { Name = "Cell 5", Value = value });
                index += count;
                value = System.Text.Encoding.ASCII.GetString(list.GetRange(index, count).ToArray());
                info.Add(new ParamConfig() { Name = "Cell 6", Value = value });
                index += count;
                value = System.Text.Encoding.ASCII.GetString(list.GetRange(index, count).ToArray());
                info.Add(new ParamConfig() { Name = "Cell 7", Value = value });
                index += count;
                count = 2;
                value = System.Text.Encoding.ASCII.GetString(list.GetRange(index, count).ToArray());
                info.Add(new ParamConfig() { Name = "Temp", Value = value });
                index += count;
                count = 3;
                value = System.Text.Encoding.ASCII.GetString(list.GetRange(index, count).ToArray());
                info.Add(new ParamConfig() { Name = "Other", Value = value });
                index += count;
                value = System.Text.Encoding.ASCII.GetString(list.GetRange(index, count).ToArray());
                info.Add(new ParamConfig() { Name = "ErrorCode", Value = value });
                index += count;
                count = 4;
                value = System.Text.Encoding.ASCII.GetString(list.GetRange(index, count).ToArray());
                info.Add(new ParamConfig() { Name = "Timer", Value = value });
            }
        }

        public async Task ScanRecvData(CancellationToken cancellationToken)
        {
            DateTime RecordDT = DateTime.UtcNow;
            string parserFile = string.Format("{0}{1}.txt", MainWindow.LOG_PATH, RecordDT.ToString("yyyyMMddHH"));
            string testFile = string.Format("{0}{1}_test.txt", MainWindow.LOG_PATH, RecordDT.ToString("yyyyMMddHH"));
            int recordHour = RecordDT.Hour;
            bool isCmdHeader = false;
            List<byte> tmpDataList = new List<byte>();
            string timestamp = "";
            bool isRefresh = false;
            while (true)
            {
                DateTime dtStart = DateTime.UtcNow;
                try
                {
                    int count = mCOMPort.recvData.Count;
                    if (count > 0)
                    {
                        string tmpTxt = "";
                        string rawTxt = "";
                        for (int i = 0; i < count; i++)
                        {
                            RecvFormat recvFormat = mCOMPort.recvData[i];
                            for (int j = 0; j < recvFormat.data.Count; j++)
                            {
                                byte data = recvFormat.data[j];
                                
                                if (data == 0x00)
                                {
                                    if (tmpDataList.Count > 0)
                                    {
                                        // 有結尾, 先處理
                                        tmpTxt += string.Format("{0}\t{1}\r\n", timestamp, BitConverter.ToString(tmpDataList.ToArray()));
                                        tmpDataList.Clear();
                                    }
                                    tmpDataList.Add(data);
                                    // 有結尾, 先處理
                                    timestamp = recvFormat.timestamp.ToString("yyyy/MM/ddTHH:mm:ss.ffff");
                                    tmpTxt += string.Format("{0}\t{1}\r\n", timestamp, BitConverter.ToString(tmpDataList.ToArray()));
                                    tmpDataList.Clear();
                                    isRefresh = true;
                                }
                                else if(data == 0x21)
                                {
                                    if (isRefresh)
                                    {
                                        tmpTxt += string.Format("{0}\tGET 0x21\r\n", recvFormat.timestamp.ToString("yyyy/MM/ddTHH:mm:ss.ffff"));
                                        isRefresh = false;
                                    }
                                    if (tmpDataList.Count > 0)
                                    {
                                        // 有結尾, 先處理
                                        tmpTxt += string.Format("{0}\t{1}\r\n", timestamp, BitConverter.ToString(tmpDataList.ToArray()));
                                        tmpDataList.Clear();
                                    }
                                    // 開始剖析
                                    isCmdHeader = true;
                                    tmpDataList.Add(data);
                                }
                                else if (isCmdHeader)
                                {
                                    tmpDataList.Add(data);

                                    if (data == 0x0D)
                                    {
                                        // 顯示的資訊
                                        string tmp = System.Text.Encoding.ASCII.GetString(tmpDataList.ToArray());
                                        //BitConverter.ToString(tmpDataList.ToArray());
                                        if (tmpDataList[1] == 0x41)
                                        {
                                            mUserValue1 = tmp;
                                        }
                                        else if (tmpDataList[1] == 0x42)
                                        {
                                            mUserValue2 = tmp;
                                        }

                                        // 有結尾, 先處理
                                        timestamp = recvFormat.timestamp.ToString("yyyy/MM/ddTHH:mm:ss.ffff");
                                        tmpTxt += string.Format("{0}\t{1}\r\n", timestamp, BitConverter.ToString(tmpDataList.ToArray()));
                                        tmpDataList.Clear();
                                    }
                                }
                                else
                                {
                                    tmpDataList.Add(data);
                                }
                            }

                            timestamp = recvFormat.timestamp.ToString("yyyy/MM/ddTHH:mm:ss.ffff");

                            rawTxt += string.Format("{0}\t{1}\r\n", timestamp, BitConverter.ToString(recvFormat.data.ToArray()));
                        }
                        mCOMPort.recvData.RemoveRange(0, count);

                        File.AppendAllText(parserFile, tmpTxt);
                        tmpTxt = "";
                        File.AppendAllText(testFile, rawTxt);
                        rawTxt = "";

                        //mTime = dtStart.ToString("HH:mm:ss");
                        //if (isRefresh)
                        //{
                        //    //List<byte> aa = new List<byte> { 0x21, 0x41, 0x31, 0x32, 0x33, 0x38, 0x43, 0x33, 0x38, 0x38, 0x33, 0x38, 0x39, 0x33, 0x38, 0x39, 0x33, 0x38, 0x39, 0x33, 0x38, 0x39, 0x33, 0x38, 0x41, 0x37, 0x41, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x41, 0x34, 0x35, 0x44, 0x0D };
                        //    //GetData(mInfo1List, aa);
                        //    string testAllStr = string.Format("{0}\t{1}\t", mTime, mUserValue1);
                        //    foreach(ParamConfig conf in mInfo1List)
                        //    {
                        //        testAllStr += string.Format("{0}\t", conf.Value);
                        //    }
                        //    testAllStr += "\r\n";
                        //    File.AppendAllText(testFile, testAllStr);
                        //}
                    }
                }
                catch (Exception err)
                {
                    string sysfile = string.Format("{0}{1}.txt", MainWindow.SYS_LOG_PATH, dtStart.ToString("yyyyMMdd"));
                    File.AppendAllText(sysfile, string.Format("{0}\t{1}\t{2}\r\n", dtStart.ToString("yyyyMMddHHmmss"), "Parser Error", err.Message));
                }

                try
                {
                    if (dtStart.Hour != recordHour)  // 超過一個小時
                    {
                        RecordDT = DateTime.UtcNow;
                        recordHour = RecordDT.Hour;
                        parserFile = string.Format("{0}{1}.txt", MainWindow.LOG_PATH, RecordDT.ToString("yyyyMMddHH"));
                        testFile = string.Format("{0}{1}_test.txt", MainWindow.LOG_PATH, RecordDT.ToString("yyyyMMddHH"));
                    }
                }
                catch (Exception err)
                {
                    string sysfile = string.Format("{0}{1}.txt", MainWindow.SYS_LOG_PATH, dtStart.ToString("yyyyMMdd"));
                    File.AppendAllText(sysfile, string.Format("{0}\t{1}\t{2}\r\n", dtStart.ToString("yyyyMMddHHmmss"), "Task Error", err.Message));
                }

                // 有需要修改資料
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                    () =>
                    {
                        RefreshValue(null);
                    }));
                await Task.Delay(3000, cancellationToken);
            }
        }
 
        private async void ExcuteUtilityDialog(object obj)
        {
            await DialogHost.Show(new UtilityRunDialog()
            {
                DataContext = new UtilityRunDialogViewModel(mCOMPortConfig)
            }, "RootDialog", async (object sender, DialogClosingEventArgs eventArgs) =>
            {
                if (eventArgs.Parameter == null) return;

                mCOMPortConfig = (COMPortConfig)eventArgs.Parameter;

                int ret = mCOMPort.Connect(mCOMPortConfig.port, mCOMPortConfig.baudRate);
                COMPort = "";
                if (ret == 0)
                {
                    mCTS = new CancellationTokenSource();
                    ScanRecvData(mCTS.Token);
                }
                else
                {
                    await DialogHost.Show(new NotifyDialog()
                    {
                        DataContext = new NotifyDialogViewModel
                        {
                            EnableYesNoQuestion = false,
                            NotifyTitle = "Warning",
                            NotifyMessage = "Please check COM Port!"
                        }
                    }, "NotifyDialog");
                }
            });
        }

        private void StopUtility()
        {
            if (mCTS != null) mCTS.Cancel();
            if (mCOMPort != null) mCOMPort.Disconnect();
            COMPort = "";
        }

        private async void StopUtilityDialog(object obj)
        {
            await DialogHost.Show(new NotifyDialog()
            {
                DataContext = new NotifyDialogViewModel()
                {
                    EnableYesNoQuestion = true,
                    NotifyTitle = "Warning",
                    NotifyMessage = "Do you want to cancel the task?"
                }
            }, "RootDialog", (object sender, DialogClosingEventArgs eventArgs) =>
            {
                if ((bool)eventArgs.Parameter == false) return;

                StopUtility();
            });
        }

        private async void ExitApp(object obj)
        {
            await DialogHost.Show(new NotifyDialog()
            {
                DataContext = new NotifyDialogViewModel()
                {
                    EnableYesNoQuestion = true,
                    NotifyTitle = "Warning",
                    NotifyMessage = "Do you want to leave now?"
                }
            }, "RootDialog", (object sender, DialogClosingEventArgs eventArgs) =>
            {
                if ((bool)eventArgs.Parameter == false) return;

                StopUtility();
                MainWindow.ExitAPP();
            });
        }


        private async void RefreshValue(object obj)
        {
            NotifyPropertyChanged("UserValue1");
            NotifyPropertyChanged("UserValue2");
            
            GetData(mInfo1List, Encoding.Default.GetBytes(UserValue1).ToList());
            GetData(mInfo2List, Encoding.Default.GetBytes(UserValue2).ToList());
        }
    }
}
