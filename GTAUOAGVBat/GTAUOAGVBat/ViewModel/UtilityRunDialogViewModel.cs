﻿using GTAUOAGVBat.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using static GTAUOAGVBat.COMPort;

namespace GTAUOAGVBat.ViewModel
{
    public class UtilityRunDialogViewModel : INotifyPropertyChanged
    {
        public ICommand ConfirmCommand { get { return new MainWindowCommand(Confirm); } }
        public ICommand CancelCommand { get { return new MainWindowCommand(Canecl); } }

        private string[] mPortList = System.IO.Ports.SerialPort.GetPortNames();
        private COMPortConfig mCOMPortConfig { get; set; }
        private ObservableCollection<string> mCOMPortList = new ObservableCollection<string>();
        private ObservableCollection<int> mAppBaudRateList = new ObservableCollection<int>();
        private string mTargetCOMPort = "";
        private int mAppBaudRate = 2400;
        private string mErrorMessage = "";

        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<string> COMPortList
        {
            get { return mCOMPortList; }
            set { mCOMPortList = value; NotifyPropertyChanged("COMPortList"); }
        }
        public ObservableCollection<int> AppBaudRateList
        {
            get { return mAppBaudRateList; }
            set { mAppBaudRateList = value; NotifyPropertyChanged("AppBaudRateList"); }
        }
        public string TargetCOMPort
        {
            get { return mTargetCOMPort; }
            set { mTargetCOMPort = value; NotifyPropertyChanged("TargetCOMPort"); }
        }
        public int AppBaudRate
        {
            get { return mAppBaudRate; }
            set { mAppBaudRate = value; NotifyPropertyChanged("AppBaudRate"); }
        }
        public string ErrorMessage
        {
            get { return mErrorMessage; }
            set { mErrorMessage = value; NotifyPropertyChanged("ErrorMessage"); }
        }

        public UtilityRunDialogViewModel(COMPortConfig config)
        {
            mCOMPortConfig = config;
            SetCOMPortList();

            mAppBaudRateList.Add(2400);
            AppBaudRateList = mAppBaudRateList;
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void NotifyPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged != null) foreach (string propertyName in propertyNames) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SetCOMPortList()
        {
            mCOMPortList.Clear();
            
            string matchTargetStr = "";
            foreach (string str in mPortList)
            {
                mCOMPortList.Add(str);
                TargetCOMPort = str;
                if (mCOMPortConfig.port == str) matchTargetStr = str;
            }
            
            if (matchTargetStr != "") TargetCOMPort = matchTargetStr;
            COMPortList = mCOMPortList;
        }

        private void Confirm(object obj)
        {
            if (TargetCOMPort == "")
            {
                ErrorMessage = "Target cannot empty";
                return;
            }
            mCOMPortConfig.port = TargetCOMPort;
            mCOMPortConfig.baudRate = AppBaudRate;
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(mCOMPortConfig, null);
        }

        private void Canecl(object obj)
        {
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(null, null);
        }
    }
}
