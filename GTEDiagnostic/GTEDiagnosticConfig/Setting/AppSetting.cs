﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEDiagnosticConfig.Setting
{
    public class AppSetting
    {
        public string Port { get; set; }
        public int BaudRate { get; set; }
        public int[] BaudRateList { get; set; }

        public int RefreshTimer { get; set; }
        public string SaveLogPath { get; set; }

        public int ResolutionWidth { get; set; }
        public int ResolutionHeight { get; set; }

        public bool IsAutoBaudRate { get; set; }

        public AppSetting()
        {
            BaudRateList = new int[] { 115200, 38400 };
        }
    }
}
