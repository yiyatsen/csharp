﻿using GTE.Command;
using GTEBatteryLib;
using GTEDiagnosticConfig.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEDiagnosticConfig.Config
{
    public class DiagnosticConfig_MPM : DiagnosticDeviceAdapter
    {
        public DiagnosticConfig_MPM(BatteryAPI device, CustomCode customCode)
            : base(device, customCode)
        {
        }

        public override BatteryAttributes GetDeviceAllInformation(BatteryAttributes batteryAttributes)
        {
            mErrorDataTime = 0;

            BatteryAttributes ba = batteryAttributes;
            object value = null;
            if (ba == null)
            {
                ba = new BatteryAttributes();


                bool isSuccess = mDevice.RefreshPBEEP();

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_VERSION_GET));
                if (value != null)
                {
                    ba.MCUVERSION = value.ToString();
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_DEVICE_NAME_GET));
                if (value != null)
                {
                    ba.DeviceName = value.ToString();
                }

                if (ba.ManufactureDate != null && ba.ManufactureDate.Count() > 0 && ba.ManufactureDate[0] < 2015)
                {
                    value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_BATTERY_NO_GET));
                    if (value != null)
                    {
                        ba.Barcode = value.ToString();
                    }
                }
                else
                {
                    value = mDevice.SendBatteryCmd(BatteryCmd.CMD_BARCODE_DATA_GET, 1);
                    if (value != null)
                    {
                        ba.Barcode = value.ToString();
                    }
                    else
                    {
                        value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_BATTERY_NO_GET));
                        if (value != null)
                        {
                            ba.Barcode = value.ToString();
                        }
                    }
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_MANUFACTURE_NAME_GET, 1);
                if (value != null)
                {
                    ba.ManufacturerName = value.ToString();
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_MANUFACTURE_DATE_GET));
                if (value != null)
                {
                    ba.ManufactureDate = value as int[];
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_DESIGN_CAPACITY_GET));
                if (value != null)
                {
                    ulong numberValue = 0;
                    ulong.TryParse(value.ToString(), out numberValue);
                    ba.DesignCapcity = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_DESIGN_VOLTAGE_GET));
                if (value != null)
                {
                    ulong numberValue = 0;
                    ulong.TryParse(value.ToString(), out numberValue);
                    ba.DesignVoltage = numberValue;
                }
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_RSOC_GET));
            if (value != null)
            {
                int numberValue = 0;
                int.TryParse(value.ToString(), out numberValue);
                ba.SOC = numberValue;
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_FULL_CC_GET));
            if (value != null)
            {
                ulong numberValue = 0;
                ulong.TryParse(value.ToString(), out numberValue);
                ba.FullChargeCapacity = numberValue;

                //舊版要調整變化量
                ba.SOH = mCustomBatteryAttribute.GetSOH(ba.FullChargeCapacity, ba.DesignCapcity);
                if (ba.SOH > 100)
                {
                    ba.SOH = 100;
                }
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_VOLTAGE_GET));
            if (value != null)
            {
                ba.CellsVoltage = value as int[];
                
                if (ba.CellsVoltage != null)
                {
                    ba.CellNumber = ba.CellsVoltage.Length;

                    ba.Voltage = 0;
                    foreach (int cellVoltage in ba.CellsVoltage)
                    {
                        ba.Voltage = ba.Voltage + (uint)cellVoltage;
                    }
                }
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_CYCLE_COUNT_GET));
            if (value != null)
            {
                ulong numberValue = 0;
                ulong.TryParse(value.ToString(), out numberValue);
                ba.CycleCounted = numberValue;
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_REMAIN_CAPACITY_GET));
            if (value != null)
            {
                ulong numberValue = 0;
                ulong.TryParse(value.ToString(), out numberValue);
                ba.RemainCapacity = numberValue;
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_CURRENT_GET));
            if (value != null)
            {
                int numberValue = 0;
                int.TryParse(value.ToString(), out numberValue);
                ba.Current = numberValue;
            }

            value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_LAST_CHARGE_END, 1);
            if (value != null)
            {
                ba.LastChargeEnd = value as int[];
            }

            value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_LAST_DISCHARGE_END, 1);
            if (value != null)
            {
                ba.LastDischargeEnd = value as int[];
            }

            value = mDevice.SendBatteryCmd(BatteryCmd.CMD_RTC_REG_DUMP, 1);
            if (value != null)
            {
                ba.RTCDateTime = value as int[];
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_PROTECTION_GET));
            if (value != null)
            {
                ba.Protection = value as byte[];
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_TEMP_GET));
            if (value != null)
            {
                ba.CellsTemperature = value as double[];
                if (ba.CellsTemperature != null)
                {
                    double minTemp = 0;
                    double maxTemp = 0;
                    bool isFirst = true;
                    for (int i = 0; i < ba.CellsTemperature.Length; i++)
                    {
                        if ((i % 3) != 0) // inner not count
                        {
                            if (isFirst)
                            {
                                isFirst = false;
                                minTemp = ba.CellsTemperature[i];
                                maxTemp = ba.CellsTemperature[i];
                            }

                            if (ba.CellsTemperature[i] > maxTemp)
                            {
                                maxTemp = ba.CellsTemperature[i];
                            }

                            if (ba.CellsTemperature[i] < minTemp)
                            {
                                minTemp = ba.CellsTemperature[i];
                            }
                        }
                    }
                    ba.Temperature = maxTemp;
                    if (ba != null && ba.Protection[5] > 0)
                    {
                        ba.Temperature = minTemp;
                    }
                }             
            }

            value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_UV_HAPPENTIME, 1);
            if (value != null)
            {
                ba.UV_HappenInfo = value as int[];
            }

            value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_OV_HAPPENTIME, 1);
            if (value != null)
            {
                ba.OV_HappenInfo = value as int[];
            }

            value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_COC_HAPPENTIME, 1);
            if (value != null)
            {
                ba.COC_HappenInfo = value as int[];
            }

            value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_DOC_HAPPENTIME, 1);
            if (value != null)
            {
                ba.DOC_HappenInfo = value as int[];
            }

            value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_SC_HAPPENTIME, 1);
            if (value != null)
            {
                ba.SC_HappenInfo = value as int[];
            }

            value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_OT_HAPPENTIME, 1);
            if (value != null)
            {
                ba.OT_HappenInfo = value as int[];
            }

            value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_UT_HAPPENTIME, 1);
            if (value != null)
            {
                ba.UT_HappenInfo = value as int[];
            }

            bool isGetLog = false;
            List<int[]> tempLog = new List<int[]>();
            value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_00_22, 1);
            if (value != null)
            {
                isGetLog = true;
                tempLog.AddRange((int[][])value);
            }
            if (isGetLog)
            {
                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_23_45, 1);
                if (isGetLog && value != null)
                {
                    isGetLog = true;
                    tempLog.AddRange((int[][])value);
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_46_49, 1);
                if (isGetLog && value != null)
                {
                    isGetLog = true;
                    tempLog.AddRange((int[][])value);

                    ba.ChargeLog = tempLog.ToArray();
                }
            }

            if (mIsRefreshSaveLog)
            {

                List<int[]> tempdisLog = new List<int[]>();
                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_DISCHARGE_LOG, 1);
                if (value != null)
                {
                    tempdisLog.AddRange((int[][])value);
                    ba.DischargeLog = tempdisLog.ToArray();
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_RESET_COUNT_GET, 1);
                if (value != null)
                {
                    int numberValue = 0;
                    int.TryParse(value.ToString(), out numberValue);
                    ba.MCURestCount = numberValue;
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_LAST_RESET_RTC_INFO, 1);
                if (value != null)
                {
                    ba.LastRestInfo = value as int[];
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_LAST_FCC_INFO, 1);
                if (value != null)
                {
                    ba.LastFCCInfo = value as int[];
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CRATE_INFO, 0, null, 1);
                if (value != null)
                {
                    ba.CRateDefine = value as int[];
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CRATE_INFO, 1, null, 1);
                if (value != null)
                {
                    ba.CRateTime = value as long[];
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_EEPROM_GET, 0, null, 1);
                if (value != null)
                {
                    int numberValue = 0;
                    int.TryParse(value.ToString(), out numberValue);
                    ba.MCUEEPNumber = numberValue;

                    if (ba.MCUEEPNumber > 0)
                    {
                        ba.MCUEEP = new byte[ba.MCUEEPNumber][];
                        for (int i = 0; i < ba.MCUEEPNumber; i++)
                        {
                            value = mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_EEPROM_GET, (byte)(i + 1), null, 1);
                            if (value != null)
                            {
                                ba.MCUEEP[i] = (byte[])value;
                            }
                        }
                    }
                }

                mIsRefreshSaveLog = false;
            }

            if (mEngCode > 0)
            {
                if (mIsResetRTC)
                {
                    mIsResetRTC = false;
                    DateTime dt = DateTime.UtcNow;

                    mDevice.SendBatteryCmd(BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET,Convert.ToByte( 0x83));

                    mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_SEC, Convert.ToByte(dt.Second));
                    mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_MIN, Convert.ToByte(dt.Minute));
                    mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_HOUR, Convert.ToByte(dt.Hour));
                    mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_DAY, Convert.ToByte(dt.Day));
                    mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_MONTH, Convert.ToByte(dt.Month));
                    mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_YEAR, Convert.ToByte(dt.Year - 2000));

                    mDevice.SendBatteryCmd(BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET, 0x03);
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_RTC_REG_DUMP);
                if (value != null)
                {
                    ba.RTCTime = value as int[];
                }
            }

            //switch (mVersionType)
            //{
            //    case GTE.Encapsulation.EncapsulationType.MPVersion:
            //        {
            //            try
            //            {
            //                bool isGetLog = false;
            //                List<int[]> tempLog = new List<int[]>();
            //                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_00_22));
            //                if (value != null)
            //                {
            //                    isGetLog = true;
            //                    tempLog.AddRange((int[][])value);
            //                }

            //                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_23_45));
            //                if (isGetLog && value != null)
            //                {
            //                    isGetLog = true;
            //                    tempLog.AddRange((int[][])value);
            //                }

            //                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_46_49));
            //                if (isGetLog && value != null)
            //                {
            //                    isGetLog = true;
            //                    tempLog.AddRange((int[][])value);

            //                    ba.ChargeLog = tempLog.ToArray();
            //                }
            //            }
            //            catch{

            //            }


            //            break;
            //        }
            //}

            return ba;
        }



        public override string GetAutoSaveLogTitle(BatteryAttributes batteryAttributes)
        {
            string result = null;
            if (batteryAttributes != null)
            {
                result = "Time,RSOC,Current,Remain Capacity,FCC,Cycle Count,Reset Count";

                if (batteryAttributes.CellsTemperature != null)
                {
                    int ozNumber = batteryAttributes.CellsTemperature.Length / 3;
                    for (int i = 0; i < ozNumber; i++)
                    {
                        result = result + string.Format(",Temperature Oz{0} In,Temperature Oz{0} Ex-1,Temperature Oz{0} Ex-2", (i + 1));
                    }
                }
                if (batteryAttributes.CellsVoltage != null)
                {
                    for (int i = 0; i < batteryAttributes.CellsVoltage.Length; i++)
                    {
                        result = result + ",Cell[" + (i + 1).ToString() + "]";
                    }
                }

                result = result + ",Total volt,VCELL Max,VCELL Min";
                result = result + ",Over Voltage Protection,OV Occur Time,OV Time SOC,OV Minimum Voltage,OV Maxmum Voltage,OV Error Count";
                result = result + ",Under Voltage Protection,UV Occur Time,UV Time SOC,UV Minimum Voltage,UV Maxmum Voltage,UV Error Count";
                result = result + ",COC Protection,COC Occur Time,COC Time SOC,COC Minimum Voltage,COC Maxmum Voltage,COC Error Count";
                result = result + ",DOC Protection,DOC Occur Time,DOC Time SOC,DOC Minimum Voltage,DOC Maxmum Voltage,DOC Error Count";
                result = result + ",OT Protection,OT Occur Time,OT Time SOC,OT Minimum Voltage,OT Maxmum Voltage,OT Error Count";
                result = result + ",UT Protection,UT Occur Time,UT Time SOC,UT Minimum Voltage,UT Maxmum Voltage,UT Error Count";
                result = result + ",SC Protection,SC Occur Time,SC Time SOC,SC Minimum Voltage,SC Maxmum Voltage,SC Error Count";
                result = result + "," + "Last Charging Time" + "," + "Last Discharging Time";
            }

            return result;
        }

        public override string GetAutoSaveLogInfo(BatteryAttributes batteryAttributes)
        {
            string result = null;
            if (batteryAttributes != null)
            {
                result = "" + batteryAttributes.SOC;
                result = result + "," + batteryAttributes.Current;
                result = result + "," + batteryAttributes.RemainCapacity;
                result = result + "," + batteryAttributes.FullChargeCapacity;
                result = result + "," + batteryAttributes.CycleCounted;
                result = result + "," + batteryAttributes.MCURestCount;

                if (batteryAttributes.CellsTemperature != null)
                {
                    for (int i = 0; i < batteryAttributes.CellsTemperature.Length; i++)
                    {
                        result = result + "," + string.Format("{0:0.00}", batteryAttributes.CellsTemperature[i]);
                    }
                }

                int totalVolt = 0;
                int minVolt = 0;
                int maxVolt = 0;
                if (batteryAttributes.CellsVoltage != null)
                {
                    for (int i = 0; i < batteryAttributes.CellsVoltage.Length; i++)
                    {
                        if (i == 0)
                        {
                            minVolt = batteryAttributes.CellsVoltage[i];
                            maxVolt = batteryAttributes.CellsVoltage[i];
                        }
                        else
                        {
                            if (batteryAttributes.CellsVoltage[i] < minVolt)
                            {
                                minVolt = batteryAttributes.CellsVoltage[i];
                            }
                            if (batteryAttributes.CellsVoltage[i] > maxVolt)
                            {
                                maxVolt = batteryAttributes.CellsVoltage[i];
                            }
                        }
                        totalVolt = totalVolt + batteryAttributes.CellsVoltage[i];
                        result = result + "," + batteryAttributes.CellsVoltage[i];
                    }
                }
                result = result + "," + totalVolt;
                result = result + "," + maxVolt;
                result = result + "," + minVolt;


                result = result + "," + GetProtectionString(batteryAttributes.Protection, 0, batteryAttributes.OV_HappenInfo);

                result = result + "," + GetProtectionString(batteryAttributes.Protection, 1, batteryAttributes.UV_HappenInfo);

                result = result + "," + GetProtectionString(batteryAttributes.Protection, 2, batteryAttributes.COC_HappenInfo);

                result = result + "," + GetProtectionString(batteryAttributes.Protection, 3, batteryAttributes.DOC_HappenInfo);

                result = result + "," + GetProtectionString(batteryAttributes.Protection, 4, batteryAttributes.OT_HappenInfo);

                result = result + "," + GetProtectionString(batteryAttributes.Protection, 5, batteryAttributes.UT_HappenInfo);

                result = result + "," + GetProtectionString(batteryAttributes.Protection, 6, batteryAttributes.SC_HappenInfo);

                result = result + "," + GetLastChargeInfoString(batteryAttributes.LastChargeEnd);

                result = result + "," + GetLastChargeInfoString(batteryAttributes.LastDischargeEnd);
            }

            return result;
        }

        public override string GetSaveLogInfo(DateTime dt, BatteryAttributes batteryAttributes)
        {
            string result = null;
            if (batteryAttributes != null)
            {
                string encryptionString = "";

                if (batteryAttributes.RTCDateTime != null)
                {
                    encryptionString = encryptionString +
                        "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(string.Format("RTCDateTime->{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}"
                        , batteryAttributes.RTCDateTime[0], batteryAttributes.RTCDateTime[1], batteryAttributes.RTCDateTime[2]
                        , batteryAttributes.RTCDateTime[3], batteryAttributes.RTCDateTime[4], batteryAttributes.RTCDateTime[5]))))
                        + Environment.NewLine;
                }
                encryptionString = encryptionString +
                    "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(string.Format("RestCount->{0}"
                    , batteryAttributes.MCURestCount.ToString()))))
                    + Environment.NewLine;
                encryptionString = encryptionString +
                    "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(string.Format("ManufacturerName->{0}"
                    , batteryAttributes.ManufacturerName))))
                    + Environment.NewLine;

                encryptionString = encryptionString +
                    "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(string.Format("ManufactureDate->{0:0000}/{1:00}/{2:00}"
                    , batteryAttributes.ManufactureDate[0], batteryAttributes.ManufactureDate[1], batteryAttributes.ManufactureDate[2]))))
                    + Environment.NewLine;
                // other information

                if (batteryAttributes.LastFCCInfo != null)
                {
                    encryptionString = encryptionString +
                        "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(
                        string.Format("LastFCCInfo->{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:00, Count->{5}"
                        , batteryAttributes.LastFCCInfo[0], batteryAttributes.LastFCCInfo[1], batteryAttributes.LastFCCInfo[2]
                        , batteryAttributes.LastFCCInfo[3], batteryAttributes.LastFCCInfo[4], batteryAttributes.LastFCCInfo[5]))))
                        + Environment.NewLine;
                }

                if (batteryAttributes.LastRestInfo != null)
                {
                    encryptionString = encryptionString +
                        "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(
                        string.Format("LastRestRTCInfo->{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:00, Count->{5}"
                        , batteryAttributes.LastRestInfo[0], batteryAttributes.LastRestInfo[1], batteryAttributes.LastRestInfo[2]
                        , batteryAttributes.LastRestInfo[3], batteryAttributes.LastRestInfo[4], batteryAttributes.LastRestInfo[5]))))
                        + Environment.NewLine;
                }

                if (batteryAttributes.CRateDefine != null && batteryAttributes.CRateTime != null)
                {
                    encryptionString = encryptionString +
                        "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(
                        string.Format("===============C RATE INFO=============="))))
                        + Environment.NewLine;

                    encryptionString = encryptionString +
                        "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(
                        string.Format("0A--------------{0:000}A--------------{1:000}A--------------{2:000}A--------------{3:000}A--------------"
                        , batteryAttributes.CRateDefine[0], batteryAttributes.CRateDefine[1], batteryAttributes.CRateDefine[2]
                        , batteryAttributes.CRateDefine[3]))))
                        + Environment.NewLine;

                                        encryptionString = encryptionString +
                        "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(
                        string.Format("     {0:0000000000}          {1:0000000000}           {2:0000000000}           {3:0000000000}           {4:0000000000}"
                        , batteryAttributes.CRateTime[0], batteryAttributes.CRateTime[1], batteryAttributes.CRateTime[2]
                        , batteryAttributes.CRateTime[3], batteryAttributes.CRateTime[4]))))
                        + Environment.NewLine;

                                        encryptionString = encryptionString +
                        "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(
                        string.Format("========================================"))))
                        + Environment.NewLine;
                }

                if (batteryAttributes.ChargeLog != null && batteryAttributes.ChargeLog.Length >= 2)
                {
                    for (int i = 0; i < batteryAttributes.ChargeLog.Length; i++)
                    {
                        if (batteryAttributes.ChargeLog[i] != null)
                        {
                            if (i == 0)
                            {
                                encryptionString = encryptionString +
                                    "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(
                                    string.Format("FisrtChargeTime->{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:00, StartVolt->{5}, EndVolt->{6}, Duration->{7}"
                                    , batteryAttributes.ChargeLog[i][0], batteryAttributes.ChargeLog[i][1], batteryAttributes.ChargeLog[i][2]
                                    , batteryAttributes.ChargeLog[i][3], batteryAttributes.ChargeLog[i][4]
                                    , batteryAttributes.ChargeLog[i][5], batteryAttributes.ChargeLog[i][6], batteryAttributes.ChargeLog[i][7]))))
                                    + Environment.NewLine;
                            }
                            else if (i == 1)
                            {
                                encryptionString = encryptionString +
                                    "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(
                                    string.Format("TheLongestChargeGap->{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:00, StartVolt->{5}, EndVolt->{6}, Duration->{7}"
                                    , batteryAttributes.ChargeLog[i][0], batteryAttributes.ChargeLog[i][1], batteryAttributes.ChargeLog[i][2]
                                    , batteryAttributes.ChargeLog[i][3], batteryAttributes.ChargeLog[i][4]
                                    , batteryAttributes.ChargeLog[i][5], batteryAttributes.ChargeLog[i][6], batteryAttributes.ChargeLog[i][7]))))
                                    + Environment.NewLine;
                            }
                            else
                            {
                                encryptionString = encryptionString +
                                    "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(
                                    string.Format("The{0}ThCharge->{1:0000}/{2:00}/{3:00} {4:00}:{5:00}:00, StartVolt->{6}, EndVolt->{7}, Duration->{8}"
                                    , i
                                    , batteryAttributes.ChargeLog[i][0], batteryAttributes.ChargeLog[i][1], batteryAttributes.ChargeLog[i][2]
                                    , batteryAttributes.ChargeLog[i][3], batteryAttributes.ChargeLog[i][4]
                                    , batteryAttributes.ChargeLog[i][5], batteryAttributes.ChargeLog[i][6], batteryAttributes.ChargeLog[i][7]))))
                                    + Environment.NewLine;
                            }
                        }
                    }
                }

                if (batteryAttributes.DischargeLog != null)
                {
                    for (int i = 0; i < batteryAttributes.DischargeLog.Length; i++)
                    {
                        if (batteryAttributes.DischargeLog[i] != null)
                        {
                            encryptionString = encryptionString +
                                "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(
                                string.Format("The{0}ThDischarge->{1:0000}/{2:00}/{3:00} {4:00}:{5:00}:00, Max Current->{6}A"
                                , i + 1
                                , batteryAttributes.DischargeLog[i][0], batteryAttributes.DischargeLog[i][1], batteryAttributes.DischargeLog[i][2]
                                , batteryAttributes.DischargeLog[i][3], batteryAttributes.DischargeLog[i][4]
                                , batteryAttributes.DischargeLog[i][5]))))
                                + Environment.NewLine;
                        }
                    }
                }

                if (batteryAttributes.MCUEEP != null)
                {
                    for (int i = 0; i < batteryAttributes.MCUEEP.Length; i++)
                    {
                        if (batteryAttributes.MCUEEP[i] != null)
                        {
                            string temp = null;
                            for (int j = 0; j < batteryAttributes.MCUEEP[i].Length; j++)
                            {
                                temp = temp + batteryAttributes.MCUEEP[i][j].ToString("X2") + " ";
                            }
                            temp.Trim();
                            encryptionString = encryptionString +
                                   "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(
                                   string.Format("MCUEEP{0}TH->" + temp, i)))) + Environment.NewLine;
                        }
                    }
                }

                result = string.Format(LogSeparator + Environment.NewLine + "Reocrd Time :\t\t\t<<{0}>>" + Environment.NewLine + LogSeparator + Environment.NewLine
                    , dt.ToString("yyyy/MM/dd HH:mm:ss"));

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Battery Pack Information]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Device Name :").PadRight(30) + "{0}" + Environment.NewLine
                    , batteryAttributes.DeviceName);

                result = result +
                    string.Format(("Serial Number :").PadRight(30) + "{0}" + Environment.NewLine
                    , batteryAttributes.Barcode);

                if (batteryAttributes.ManufactureDate != null)
                {
                    result = result +
                                               string.Format(("Manufacture Date :").PadRight(30) + "{0:0000}/{1:00}/{2:00}" + Environment.NewLine
                                               , batteryAttributes.ManufactureDate[0], batteryAttributes.ManufactureDate[1], batteryAttributes.ManufactureDate[2]);
                }
                else
                {
                    result = result +
                        string.Format(("Manufacture Date :").PadRight(30) + "No Information" + Environment.NewLine);
                }

                result = result +
                    string.Format(("Design Voltage :").PadRight(30) + "{0} mV" + Environment.NewLine
                    , batteryAttributes.DesignVoltage);

                result = result +
                    string.Format(("Design Capacity :").PadRight(30) + "{0} mAh" + Environment.NewLine
                    , batteryAttributes.DesignCapcity);

                result = result +
                    string.Format(("Remain Capacity :").PadRight(30) + "{0} mAh" + Environment.NewLine
                    , batteryAttributes.RemainCapacity);

                result = result +
                    string.Format(("Full Charge Capacity :").PadRight(30) + "{0} mAh" + Environment.NewLine
                    , batteryAttributes.FullChargeCapacity);

                result = result +
                    string.Format(("Cycle Counted :").PadRight(30) + "{0}" + Environment.NewLine
                    , batteryAttributes.CycleCounted);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[State Of Health]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Battery Health :").PadRight(30) + "{0} %" + Environment.NewLine
                    , batteryAttributes.SOH);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Relative SOC]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Relative SOC :").PadRight(30) + "{0} %" + Environment.NewLine
                    , batteryAttributes.SOC);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Recent Current]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Current :").PadRight(30) + "{0} mA" + Environment.NewLine
                    , batteryAttributes.Current);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Pack Voltage]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Voltage :").PadRight(30) + "{0} mV" + Environment.NewLine
                    , batteryAttributes.Voltage);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Pack Temperature]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Battery Temperature :").PadRight(30) + "{0:0.00} ℃" + Environment.NewLine
                    , batteryAttributes.Temperature);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Usage Current Error Record]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                if (batteryAttributes.Protection != null)
                {
                    if (batteryAttributes.Protection[1] > 0)
                    {
                        result = result +
                            string.Format(("Error : Under Voltage Discharged") + Environment.NewLine);

                        encryptionString = encryptionString + GetProtectionEncryInfoString("UV", batteryAttributes.UV_HappenInfo);
                    }

                    if (batteryAttributes.Protection[0] > 0)
                    {
                        result = result +
                            string.Format(("Error : Over Voltage Charged") + Environment.NewLine);

                        encryptionString = encryptionString + GetProtectionEncryInfoString("OV", batteryAttributes.OV_HappenInfo);
                    }

                    if (batteryAttributes.Protection[3] > 0)
                    {
                        result = result +
                            string.Format(("Error : Over Current Discharged") + Environment.NewLine);

                        encryptionString = encryptionString + GetProtectionEncryInfoString("DOC", batteryAttributes.DOC_HappenInfo);
                    }

                    if (batteryAttributes.Protection[2] > 0)
                    {
                        result = result +
                            string.Format(("Error : Over Current Charged") + Environment.NewLine);

                        encryptionString = encryptionString + GetProtectionEncryInfoString("COC", batteryAttributes.COC_HappenInfo);
                    }

                    if (batteryAttributes.Protection[4] > 0)
                    {
                        result = result +
                            string.Format(("Error : Over Temp.") + Environment.NewLine);

                        encryptionString = encryptionString + GetProtectionEncryInfoString("OT", batteryAttributes.OT_HappenInfo);
                    }

                    if (batteryAttributes.Protection[5] > 0)
                    {
                        result = result +
                            string.Format(("Error : Under Temp.") + Environment.NewLine);

                        encryptionString = encryptionString + GetProtectionEncryInfoString("UT", batteryAttributes.UT_HappenInfo);
                    }

                    if (batteryAttributes.Protection[6] > 0)
                    {
                        result = result +
                            string.Format(("Error : Short Circuit") + Environment.NewLine);

                        encryptionString = encryptionString + GetProtectionEncryInfoString("SC", batteryAttributes.SC_HappenInfo);
                    }
                }

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Cells Voltage]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                if (batteryAttributes.CellsVoltage != null)
                {
                    int maxCellVolt = 0;
                    int minCellVolt = 0;
                    for (int i = 0; i < batteryAttributes.CellsVoltage.Length; i++)
                    {
                        if (i == 0)
                        {
                            maxCellVolt = batteryAttributes.CellsVoltage[i];
                            minCellVolt = batteryAttributes.CellsVoltage[i];
                        }
                        else
                        {
                            if (maxCellVolt < batteryAttributes.CellsVoltage[i])
                            {
                                maxCellVolt = batteryAttributes.CellsVoltage[i];
                            }
                            if (minCellVolt > batteryAttributes.CellsVoltage[i])
                            {
                                minCellVolt = batteryAttributes.CellsVoltage[i];
                            }
                        }
                        result = result +
                            string.Format(string.Format("Cell[{0:00}] Volt. :", i + 1).PadRight(30) + "{0} mV" + Environment.NewLine
                            , batteryAttributes.CellsVoltage[i]);
                    }
                    result = result +
                        string.Format(string.Format("Cell Voltage Delta :").PadRight(30) + "{0} mV" + Environment.NewLine
                        , (maxCellVolt - minCellVolt));
                }

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Pack Detail Info.]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(string.Format("MCU Firmware Ver :").PadRight(30) + "{0}" + Environment.NewLine
                    , batteryAttributes.MCUVERSION);

                if (batteryAttributes.LastChargeEnd != null)
                {
                    result = result +
                        string.Format(string.Format("Last Charging Time :").PadRight(30) + "{0}" + Environment.NewLine
                        , GetLastChargeInfoString(batteryAttributes.LastChargeEnd));
                }
                if (batteryAttributes.LastDischargeEnd != null)
                {
                    result = result +
                        string.Format(string.Format("Last Discharging Time :").PadRight(30) + "{0}" + Environment.NewLine
                        , GetLastChargeInfoString(batteryAttributes.LastDischargeEnd));
                }

                result = result + LogSeparator 
                    + Environment.NewLine;
                result = result + Environment.NewLine
                    + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine
                    +Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine;
                result = result + LogSeparator + Environment.NewLine;
                result = result + encryptionString + Environment.NewLine;
                result = result + LogSeparator + Environment.NewLine;
            }

            return result;
        }

        protected string GetLastChargeInfoString(int[] chargeInfo)
        {
            string result = null;
            if (chargeInfo != null)
            {
                result = ""
                    + string.Format("{0:00}/{1:00}/{2:00} {3:00}:{4:00}:{5:00} {6}%",
                    chargeInfo[0],
                    chargeInfo[1],
                    chargeInfo[2],
                    chargeInfo[3],
                    chargeInfo[4],
                    chargeInfo[5],
                    chargeInfo[6]);
            }
            else
            {
                result = "";
            }

            return result;
        }
    }
}
