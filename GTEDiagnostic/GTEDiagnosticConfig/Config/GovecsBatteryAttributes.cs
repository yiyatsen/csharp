﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEDiagnosticConfig.Config
{
    public class GovecsBatteryAttributes : CustomBatteryAttribute
    {
        public override double GetSOH(ulong fcc, ulong dc)
        {
            if (dc == 0)
            {
                return 0;
            }
            int index = Convert.ToInt32(fcc * 100 / dc);
            if (index > 100) // Fixed by Neil 2016.04.15   4.0.1.7 BUG  101 to 100
            {
                return 100;
            }
            else if (index < 0)
            {
                return 0;
            }
            else
            {
                return lookupSOH[index];
            }
        }

        private double[] lookupSOH = new double[101] { 
            0,	1,	2,	3,	4,	5,	6,	7,	8,	9,
            10,	11,	12,	13,	14,	15,	16,	17,	18,	19,
            20,	21,	22,	23,	24,	25,	26,	27,	28,	29,
            30,	31,	32,	33,	34,	35,	36,	37,	38,	39,
            40,	41,	42,	43,	44,	45,	46,	47,	48,	49,
            50,	52,	54,	56,	58,	60,	62,	64,	66,	68,
            70,	71,	72,	73,	74,	75,	76,	77,	78,	79,
            80,	81,	82,	83,	84,	85,	86,	87,	88,	89,
            90,	90,	91,	91,	92,	92,	93,	93,	94,	94,
            95,	95,	97,	97,	98,	99,	100,	100,	100,	100, 100};
    }
}
