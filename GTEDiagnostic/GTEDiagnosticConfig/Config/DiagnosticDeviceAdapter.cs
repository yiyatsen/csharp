﻿using GTE.DeivceConfiguration;
using GTE.Encapsulation;
using GTE.Encryption;
using GTEBatteryLib;
using GTEDiagnosticConfig.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GTEDiagnosticConfig.Config
{
    public abstract class DiagnosticDeviceAdapter : AppDeviceAdapter
    {
        protected const string LogSeparator = "===============================================================";
        protected EncryptionAESAnto mEncryptionAlog = new EncryptionAESAnto();

        protected BatteryAPI mDevice { get; set; }

        protected EncapsulationType mVersionType { get; set; }

        protected int mEngCode { get; set; }
        protected bool mIsResetRTC { get; set; }
        protected bool mIsRefreshSaveLog { get; set; }

        protected CustomBatteryAttribute mCustomBatteryAttribute { get; set; }

        public DiagnosticDeviceAdapter(BatteryAPI device, CustomCode customCode)
            : base()
        {
            mDevice = device;
            mMaxErrorConnectTime = 2;

            if (mDevice != null && mDevice.IsConnect())
            {
                string deviceVer = mDevice.GetVersion();
                Regex regex = new Regex(@"^V");
                if (regex.IsMatch(deviceVer))
                {
                    mVersionType = EncapsulationType.VVersion;
                }
                else
                {
                    mVersionType = EncapsulationType.MPVersion;
                }
            }

            switch (customCode)
            {
                case CustomCode.GOVECS:
                    {
                        mCustomBatteryAttribute = new GovecsBatteryAttributes();
                        break;
                    }
                default:
                    {
                        mCustomBatteryAttribute = new CustomBatteryAttribute();
                        break;
                    }
            }
        }

        public abstract BatteryAttributes GetDeviceAllInformation(BatteryAttributes batteryAttributes);

        public abstract string GetAutoSaveLogTitle(BatteryAttributes batteryAttributes);

        public abstract string GetAutoSaveLogInfo(BatteryAttributes batteryAttributes);

        public abstract string GetSaveLogInfo(DateTime dt, BatteryAttributes batteryAttributes);


        protected string GetProtectionString(byte[] protection, int index, int[] protectionInfo)
        {
            string result = null;
            if (protection != null)
            {
                result = "" + protection[index];
            }
            else
            {
                result = "";
            }
            if (protectionInfo != null && protectionInfo.Length == 10)
            {
                result = result + ","
                    + string.Format("{0:00}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}",
                    protectionInfo[0],
                    protectionInfo[1],
                    protectionInfo[2],
                    protectionInfo[3],
                    protectionInfo[4],
                    protectionInfo[5])
                    + "," + protectionInfo[9] + "," + protectionInfo[6]
                    + "," + protectionInfo[7] + "," + protectionInfo[8];
            }
            else
            {
                result = result + ",,,,,";
            }

            return result;
        }

        protected string GetProtectionEncryInfoString(string protectionName, int[] protectionInfo)
        {
            if (protectionInfo != null)
            {
                return "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(
                            string.Format("{10}Time->{0:00}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}, {10}TimeSOC->{6}, {10}MinVoltage->{7}, {10}MaxVoltage->{8}, {10}ErrorCount->{9}",
                                protectionInfo[0],
                                protectionInfo[1],
                                protectionInfo[2],
                                protectionInfo[3],
                                protectionInfo[4],
                                protectionInfo[5],
                                protectionInfo[9],
                                protectionInfo[6],
                                protectionInfo[7],
                                protectionInfo[8],
                                protectionName)))) + Environment.NewLine;
            }
            return null;
        }

        public void SetEngCode(int engCode)
        {
            mEngCode = engCode;
        }

        public void IsResetRTC(bool isResetRTC)
        {
            mIsResetRTC = isResetRTC;
        }

        public void SetRefreshSaveLog(bool isRefreshSaveLog)
        {
            mIsRefreshSaveLog = isRefreshSaveLog;
        }

        public bool IsRefreshSaveLog()
        {
            return mIsRefreshSaveLog;
        }
    }
}
