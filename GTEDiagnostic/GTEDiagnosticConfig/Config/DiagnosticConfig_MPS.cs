﻿using GTE.Command;
using GTEBatteryLib;
using GTEDiagnosticConfig.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEDiagnosticConfig.Config
{
    public class DiagnosticConfig_MPS : DiagnosticDeviceAdapter
    {
        public DiagnosticConfig_MPS(BatteryAPI device, CustomCode customCode)
            : base(device, customCode)
        {
        }

        bool isFirstVoltage = true;

        public override BatteryAttributes GetDeviceAllInformation(BatteryAttributes batteryAttributes)
        {
            mErrorDataTime = 0;

            BatteryAttributes ba = batteryAttributes;
            object value = null;
            if (ba == null)
            {
                ba = new BatteryAttributes();

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_VERSION_GET));
                if (value != null)
                {
                    ba.MCUVERSION = value.ToString();
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_DEVICE_NAME_GET));
                if (value != null)
                {
                    ba.DeviceName = value.ToString();
                }

                if (ba.ManufactureDate != null && ba.ManufactureDate.Count() > 0 && ba.ManufactureDate[0] < 2015)
                {
                    value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_BATTERY_NO_GET));
                    if (value != null)
                    {
                        ba.Barcode = value.ToString();
                    }
                }
                else
                {
                    value = mDevice.SendBatteryCmd(BatteryCmd.CMD_BARCODE_DATA_GET);
                    if (value != null)
                    {
                        ba.Barcode = value.ToString();
                    }
                    else
                    {
                        value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_BATTERY_NO_GET));
                        if (value != null)
                        {
                            ba.Barcode = value.ToString();
                        }
                    }
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_MANUFACTURE_DATA_GET));
                if (value != null)
                {
                    ba.ManufacturerName = value.ToString();
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_MANUFACTURE_DATE_GET));
                if (value != null)
                {
                    ba.ManufactureDate = value as int[];
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_DESIGN_CAPACITY_GET));
                if (value != null)
                {
                    ulong numberValue = 0;
                    ulong.TryParse(value.ToString(), out numberValue);
                    ba.DesignCapcity = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_DESIGN_VOLTAGE_GET));
                if (value != null)
                {
                    ulong numberValue = 0;
                    ulong.TryParse(value.ToString(), out numberValue);
                    ba.DesignVoltage = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_RESET_COUNT_GET));
                if (value != null)
                {
                    int numberValue = 0;
                    int.TryParse(value.ToString(), out numberValue);
                    ba.MCURestCount = numberValue;
                }
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_RSOC_GET));
            if (value != null)
            {
                int numberValue = 0;
                int.TryParse(value.ToString(), out numberValue);
                ba.SOC = numberValue;
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_FULL_CC_GET));
            if (value != null)
            {
                ulong numberValue = 0;
                ulong.TryParse(value.ToString(), out numberValue);
                ba.FullChargeCapacity = numberValue;

                //舊版要調整變化量
                ba.SOH = Math.Floor((double)ba.FullChargeCapacity / (double)ba.DesignCapcity * 10000.0) / 100.0;//取小數點2位
                if (ba.SOH > 100)
                {
                    ba.SOH = 100;
                }
            }
          
            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_VOLTAGE_GET));
            if (value != null)
            {
                ba.CellsVoltage = value as int[];

                if (ba.CellsVoltage != null)
                {
                    ba.CellNumber = ba.CellsVoltage.Length;

                    ba.Voltage = 0;
                    foreach (int cellVoltage in ba.CellsVoltage)
                    {
                        ba.Voltage = ba.Voltage + (uint)cellVoltage;
                    }
                }
            }


            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_CYCLE_COUNT_GET));
            if (value != null)
            {
                ulong numberValue = 0;
                ulong.TryParse(value.ToString(), out numberValue);
                ba.CycleCounted = numberValue;
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_REMAIN_CAPACITY_GET));
            if (value != null)
            {
                ulong numberValue = 0;
                ulong.TryParse(value.ToString(), out numberValue);
                ba.RemainCapacity = numberValue;
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_CURRENT_GET));
            if (value != null)
            {
                int numberValue = 0;
                int.TryParse(value.ToString(), out numberValue);
                ba.Current = numberValue;
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_LAST_CHARGE_END));
            if (value != null)
            {
                ba.LastChargeEnd = value as int[];
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_LAST_DISCHARGE_END));
            if (value != null)
            {
                ba.LastDischargeEnd = value as int[];
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_TIMESET));
            if (value != null)
            {
                ba.RTCDateTime = value as int[];
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_PROTECTION_GET));
            if (value != null)
            {
                ba.Protection = value as byte[];
            }

            value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_TEMP_GET));
            if (value != null)
            {
                ba.CellsTemperature = value as double[];

                if (ba.CellsTemperature != null)
                {
                    double minTemp = 0;
                    double maxTemp = 0;
                    bool isFirst = true;
                    for (int i = 0; i < ba.CellsTemperature.Length; i++)
                    {
                        //if ((i % 3) != 0) // inner not count
                        //{
                        if (isFirst)
                        {
                            isFirst = false;
                            minTemp = ba.CellsTemperature[i];
                            maxTemp = ba.CellsTemperature[i];
                        }

                        if (ba.CellsTemperature[i] > maxTemp)
                        {
                            maxTemp = ba.CellsTemperature[i];
                        }

                        if (ba.CellsTemperature[i] < minTemp)
                        {
                            minTemp = ba.CellsTemperature[i];
                        }
                        //}
                    }
                    ba.Temperature = maxTemp;
                    //if (ba != null && ba.Protection[5] > 0)
                    //{
                    //    ba.Temperature = minTemp;
                    //}
                }

            }

            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_UV_HAPPENTIME));
            //if (value != null)
            //{
            //    ba.UV_HappenInfo = (int[])value;
            //}

            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_OV_HAPPENTIME));
            //if (value != null)
            //{
            //    ba.OV_HappenInfo = (int[])value;
            //}

            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_COC_HAPPENTIME));
            //if (value != null)
            //{
            //    ba.COC_HappenInfo = (int[])value;
            //}

            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_DOC_HAPPENTIME));
            //if (value != null)
            //{
            //    ba.DOC_HappenInfo = (int[])value;
            //}

            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_SC_HAPPENTIME));
            //if (value != null)
            //{
            //    ba.SC_HappenInfo = (int[])value;
            //}

            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_OT_HAPPENTIME));
            //if (value != null)
            //{
            //    ba.OT_HappenInfo = (int[])value;
            //}

            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_UT_HAPPENTIME));
            //if (value != null)
            //{
            //    ba.UT_HappenInfo = (int[])value;
            //}

            //bool isGetLog = false;
            //List<int[]> tempLog = new List<int[]>();
            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_00_22));
            //if (value != null)
            //{
            //    isGetLog = true;
            //    tempLog.AddRange((int[][])value);
            //}

            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_23_45));
            //if (isGetLog && value != null)
            //{
            //    isGetLog = true;
            //    tempLog.AddRange((int[][])value);
            //}

            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_46_49));
            //if (isGetLog && value != null)
            //{
            //    isGetLog = true;
            //    tempLog.AddRange((int[][])value);

            //    ba.ChargeLog = tempLog.ToArray();
            //}

            if (mEngCode > 0)
            {
                if (mIsResetRTC)
                {
                    mIsResetRTC = false;
                    DateTime dt = DateTime.UtcNow;

                    mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_SEC, Convert.ToByte(dt.Second));
                    mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_MIN, Convert.ToByte(dt.Minute));
                    mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_HOUR, Convert.ToByte(dt.Hour));
                    mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_DAY, Convert.ToByte(dt.Day));
                    mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_MONTH, Convert.ToByte(dt.Month));
                    mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_YEAR, Convert.ToByte(dt.Year - 2000));
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_TIMESET);
                if (value != null)
                {
                    ba.RTCTime = value as int[];
                }
            }


            return ba;
        }

        public override string GetAutoSaveLogTitle(BatteryAttributes batteryAttributes)
        {
            string result = null;
            if (batteryAttributes != null)
            {
                result = "Time,RSOC,Current,Remain Capacity,FCC,Cycle Count,Reset Count";

                if (batteryAttributes.CellsTemperature != null)
                {
                    for (int i = 0; i < batteryAttributes.CellsTemperature.Length; i++)
                    {
                        result = result + string.Format(",Temperature {0}", (i + 1));
                    }
                }
                if (batteryAttributes.CellsVoltage != null)
                {
                    for (int i = 0; i < batteryAttributes.CellsVoltage.Length; i++)
                    {
                        result = result + ",Cell[" + (i + 1).ToString() + "]";
                    }
                }

                result = result + ",Total volt,VCELL Max,VCELL Min";
                result = result + ",Over Voltage Protection,OV Occur Time,OV Time SOC,OV Minimum Voltage,OV Maxmum Voltage,OV Error Count";
                result = result + ",Under Voltage Protection,UV Occur Time,UV Time SOC,UV Minimum Voltage,UV Maxmum Voltage,UV Error Count";
                result = result + ",COC Protection,COC Occur Time,COC Time SOC,COC Minimum Voltage,COC Maxmum Voltage,COC Error Count";
                result = result + ",DOC Protection,DOC Occur Time,DOC Time SOC,DOC Minimum Voltage,DOC Maxmum Voltage,DOC Error Count";
                result = result + ",OT Protection,OT Occur Time,OT Time SOC,OT Minimum Voltage,OT Maxmum Voltage,OT Error Count";
                result = result + ",UT Protection,UT Occur Time,UT Time SOC,UT Minimum Voltage,UT Maxmum Voltage,UT Error Count";
                result = result + ",SC Protection,SC Occur Time,SC Time SOC,SC Minimum Voltage,SC Maxmum Voltage,SC Error Count";
                result = result + "," + "Last Charging Time" + "," + "Last Discharging Time";
            }

            return result;
        }

        public override string GetAutoSaveLogInfo(BatteryAttributes batteryAttributes)
        {
            string result = null;
            if (batteryAttributes != null)
            {
                result = "" + batteryAttributes.SOC;
                result = result + "," + batteryAttributes.Current;
                result = result + "," + batteryAttributes.RemainCapacity;
                result = result + "," + batteryAttributes.FullChargeCapacity;
                result = result + "," + batteryAttributes.CycleCounted;
                result = result + "," + batteryAttributes.MCURestCount;

                if (batteryAttributes.CellsTemperature != null)
                {
                    for (int i = 0; i < batteryAttributes.CellsTemperature.Length; i++)
                    {
                        result = result + "," + string.Format("{0:0.00}", batteryAttributes.CellsTemperature[i]);
                    }
                }

                int totalVolt = 0;
                int minVolt = 0;
                int maxVolt = 0;
                if (batteryAttributes.CellsVoltage != null)
                {
                    for (int i = 0; i < batteryAttributes.CellsVoltage.Length; i++)
                    {
                        if (i == 0)
                        {
                            minVolt = batteryAttributes.CellsVoltage[i];
                            maxVolt = batteryAttributes.CellsVoltage[i];
                        }
                        else
                        {
                            if (batteryAttributes.CellsVoltage[i] < minVolt)
                            {
                                minVolt = batteryAttributes.CellsVoltage[i];
                            }
                            if (batteryAttributes.CellsVoltage[i] > maxVolt)
                            {
                                maxVolt = batteryAttributes.CellsVoltage[i];
                            }
                        }
                        totalVolt = totalVolt + batteryAttributes.CellsVoltage[i];
                        result = result + "," + batteryAttributes.CellsVoltage[i];
                    }
                }
                result = result + "," + totalVolt;
                result = result + "," + maxVolt;
                result = result + "," + minVolt;


                result = result + "," + GetProtectionString(batteryAttributes.Protection, 0, batteryAttributes.OV_HappenInfo);

                result = result + "," + GetProtectionString(batteryAttributes.Protection, 1, batteryAttributes.UV_HappenInfo);

                result = result + "," + GetProtectionString(batteryAttributes.Protection, 2, batteryAttributes.COC_HappenInfo);

                result = result + "," + GetProtectionString(batteryAttributes.Protection, 3, batteryAttributes.DOC_HappenInfo);

                result = result + "," + GetProtectionString(batteryAttributes.Protection, 4, batteryAttributes.OT_HappenInfo);

                result = result + "," + GetProtectionString(batteryAttributes.Protection, 5, batteryAttributes.UT_HappenInfo);

                result = result + "," + GetProtectionString(batteryAttributes.Protection, 6, batteryAttributes.SC_HappenInfo);

                result = result + "," + GetLastChargeInfoString(batteryAttributes.LastChargeEnd);

                result = result + "," + GetLastChargeInfoString(batteryAttributes.LastDischargeEnd);
            }

            return result;
        }

        public override string GetSaveLogInfo(DateTime dt, BatteryAttributes batteryAttributes)
        {
            string result = null;
            if (batteryAttributes != null)
            {
                string encryptionString = "";

                if (batteryAttributes.RTCDateTime != null)
                {
                    encryptionString = encryptionString +
                        "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(string.Format("RTCDateTime->{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}"
                        , batteryAttributes.RTCDateTime[0], batteryAttributes.RTCDateTime[1], batteryAttributes.RTCDateTime[2]
                        , batteryAttributes.RTCDateTime[3], batteryAttributes.RTCDateTime[4], batteryAttributes.RTCDateTime[5]))))
                        + Environment.NewLine;
                }
                encryptionString = encryptionString +
                    "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(string.Format("RestCount->{0}"
                    , batteryAttributes.MCURestCount.ToString()))))
                    + Environment.NewLine;
                encryptionString = encryptionString +
                    "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(string.Format("ManufacturerName->{0}"
                    , batteryAttributes.ManufacturerName))))
                    + Environment.NewLine;

                encryptionString = encryptionString +
                    "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(string.Format("ManufactureDate->{0:0000}/{1:00}/{2:00}"
                    , batteryAttributes.ManufactureDate[0], batteryAttributes.ManufactureDate[1], batteryAttributes.ManufactureDate[2]))))
                    + Environment.NewLine;
                // other information
                //if (batteryAttributes.ChargeLog != null && batteryAttributes.ChargeLog.Length >= 2)
                //{
                //    if (batteryAttributes.ChargeLog[0] != null)
                //    {
                //        encryptionString = encryptionString +
                //            "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(
                //            string.Format("FisrtChargeTime->{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:00, StartVolt->{5}, EndVolt->{6}, Duration->{7}"
                //            , batteryAttributes.ChargeLog[0][0], batteryAttributes.ChargeLog[0][1], batteryAttributes.ChargeLog[0][2]
                //            , batteryAttributes.ChargeLog[0][3], batteryAttributes.ChargeLog[0][4]
                //            , batteryAttributes.ChargeLog[0][5], batteryAttributes.ChargeLog[0][6], batteryAttributes.ChargeLog[0][7]))))
                //            + Environment.NewLine;
                //    }

                //    if (batteryAttributes.ChargeLog[1] != null)
                //    {
                //        encryptionString = encryptionString +
                //            "!" + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(
                //            string.Format("TheLongestChargeGap->{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:00, StartVolt->{5}, EndVolt->{6}, Duration->{7}"
                //            , batteryAttributes.ChargeLog[1][0], batteryAttributes.ChargeLog[1][1], batteryAttributes.ChargeLog[1][2]
                //            , batteryAttributes.ChargeLog[1][3], batteryAttributes.ChargeLog[1][4]
                //            , batteryAttributes.ChargeLog[1][5], batteryAttributes.ChargeLog[1][6], batteryAttributes.ChargeLog[1][7]))))
                //            + Environment.NewLine;
                //    }
                //}

                result = string.Format(LogSeparator + Environment.NewLine + "Reocrd Time :\t\t\t<<{0}>>" + Environment.NewLine + LogSeparator + Environment.NewLine
                    , dt.ToString("yyyy/MM/dd HH:mm:ss"));

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Battery Pack Information]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Device Name :").PadRight(30) + "{0}" + Environment.NewLine
                    , batteryAttributes.DeviceName);

                result = result +
                    string.Format(("Serial Number :").PadRight(30) + "{0}" + Environment.NewLine
                    , batteryAttributes.Barcode);

                if (batteryAttributes.ManufactureDate != null)
                {
                    result = result +
                                               string.Format(("Manufacture Date :").PadRight(30) + "{0:0000}/{1:00}/{2:00}" + Environment.NewLine
                                               , batteryAttributes.ManufactureDate[0], batteryAttributes.ManufactureDate[1], batteryAttributes.ManufactureDate[2]);
                }
                else
                {
                    result = result +
                        string.Format(("Manufacture Date :").PadRight(30) + "No Information" + Environment.NewLine);
                }

                result = result +
                    string.Format(("Design Voltage :").PadRight(30) + "{0} mV" + Environment.NewLine
                    , batteryAttributes.DesignVoltage);

                result = result +
                    string.Format(("Design Capacity :").PadRight(30) + "{0} mAh" + Environment.NewLine
                    , batteryAttributes.DesignCapcity);

                result = result +
                    string.Format(("Remain Capacity :").PadRight(30) + "{0} mAh" + Environment.NewLine
                    , batteryAttributes.RemainCapacity);

                result = result +
                    string.Format(("Full Charge Capacity :").PadRight(30) + "{0} mAh" + Environment.NewLine
                    , batteryAttributes.FullChargeCapacity);

                result = result +
                    string.Format(("Cycle Counted :").PadRight(30) + "{0}" + Environment.NewLine
                    , batteryAttributes.CycleCounted);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[State Of Health]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Battery Health :").PadRight(30) + "{0} %" + Environment.NewLine
                    , batteryAttributes.SOH);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Relative SOC]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Relative SOC :").PadRight(30) + "{0} %" + Environment.NewLine
                    , batteryAttributes.SOC);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Recent Current]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Current :").PadRight(30) + "{0} mA" + Environment.NewLine
                    , batteryAttributes.Current);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Pack Voltage]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Voltage :").PadRight(30) + "{0} mV" + Environment.NewLine
                    , batteryAttributes.Voltage);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Pack Temperature]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(("Battery Temperature :").PadRight(30) + "{0:0.00} ℃" + Environment.NewLine
                    , batteryAttributes.Temperature);

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Usage Current Error Record]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                //if (batteryAttributes.Protection != null)
                //{
                //    if (batteryAttributes.Protection[1] > 0)
                //    {
                //        result = result +
                //            string.Format(("Error : Under Voltage Discharged") + Environment.NewLine);

                //        //encryptionString = encryptionString + GetProtectionEncryInfoString("UV", batteryAttributes.UV_HappenInfo);
                //    }

                //    if (batteryAttributes.Protection[0] > 0)
                //    {
                //        result = result +
                //            string.Format(("Error : Over Voltage Charged") + Environment.NewLine);

                //        //encryptionString = encryptionString + GetProtectionEncryInfoString("OV", batteryAttributes.OV_HappenInfo);
                //    }

                //    if (batteryAttributes.Protection[3] > 0)
                //    {
                //        result = result +
                //            string.Format(("Error : Over Current Discharged") + Environment.NewLine);

                //        //encryptionString = encryptionString + GetProtectionEncryInfoString("DOC", batteryAttributes.DOC_HappenInfo);
                //    }

                //    if (batteryAttributes.Protection[2] > 0)
                //    {
                //        result = result +
                //            string.Format(("Error : Over Current Charged") + Environment.NewLine);

                //        //encryptionString = encryptionString + GetProtectionEncryInfoString("COC", batteryAttributes.COC_HappenInfo);
                //    }

                //    if (batteryAttributes.Protection[4] > 0)
                //    {
                //        result = result +
                //            string.Format(("Error : Over Temp.") + Environment.NewLine);

                //        //encryptionString = encryptionString + GetProtectionEncryInfoString("OT", batteryAttributes.OT_HappenInfo);
                //    }

                //    if (batteryAttributes.Protection[5] > 0)
                //    {
                //        result = result +
                //            string.Format(("Error : Under Temp.") + Environment.NewLine);

                //        //encryptionString = encryptionString + GetProtectionEncryInfoString("UT", batteryAttributes.UT_HappenInfo);
                //    }

                //    if (batteryAttributes.Protection[6] > 0)
                //    {
                //        result = result +
                //            string.Format(("Error : Short Circuit") + Environment.NewLine);

                //        //encryptionString = encryptionString + GetProtectionEncryInfoString("SC", batteryAttributes.SC_HappenInfo);
                //    }
                //}

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Cells Voltage]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                if (batteryAttributes.CellsVoltage != null)
                {
                    int maxCellVolt = 0;
                    int minCellVolt = 0;
                    for (int i = 0; i < batteryAttributes.CellsVoltage.Length; i++)
                    {
                        if (i == 0)
                        {
                            maxCellVolt = batteryAttributes.CellsVoltage[i];
                            minCellVolt = batteryAttributes.CellsVoltage[i];
                        }
                        else
                        {
                            if (maxCellVolt < batteryAttributes.CellsVoltage[i])
                            {
                                maxCellVolt = batteryAttributes.CellsVoltage[i];
                            }
                            if (minCellVolt > batteryAttributes.CellsVoltage[i])
                            {
                                minCellVolt = batteryAttributes.CellsVoltage[i];
                            }
                        }
                        result = result +
                            string.Format(string.Format("Cell[{0:00}] Volt. :", i + 1).PadRight(30) + "{0} mV" + Environment.NewLine
                            , batteryAttributes.CellsVoltage[i]);
                    }
                    result = result +
                        string.Format(string.Format("Cell Voltage Delta :").PadRight(30) + "{0} mV" + Environment.NewLine
                        , (maxCellVolt - minCellVolt));
                }

                result = result +
                    string.Format(LogSeparator + Environment.NewLine + "[Pack Detail Info.]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                result = result +
                    string.Format(string.Format("MCU Firmware Ver :").PadRight(30) + "{0}" + Environment.NewLine
                    , batteryAttributes.MCUVERSION);

                if (batteryAttributes.LastChargeEnd != null)
                {
                    result = result +
                        string.Format(string.Format("Last Charging Time :").PadRight(30) + "{0}" + Environment.NewLine
                        , GetLastChargeInfoString(batteryAttributes.LastChargeEnd));
                }
                if (batteryAttributes.LastDischargeEnd != null)
                {
                    result = result +
                        string.Format(string.Format("Last Discharging Time :").PadRight(30) + "{0}" + Environment.NewLine
                        , GetLastChargeInfoString(batteryAttributes.LastDischargeEnd));
                }

                result = result + LogSeparator
                    + Environment.NewLine;
                result = result + Environment.NewLine
                    + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine
                    + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine;
                result = result + LogSeparator + Environment.NewLine;
                result = result + encryptionString + Environment.NewLine;
                result = result + LogSeparator + Environment.NewLine;
            }

            return result;
        }

        protected string GetLastChargeInfoString(int[] chargeInfo)
        {
            string result = null;
            if (chargeInfo != null)
            {
                result = ""
                    + string.Format("{0:00}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}",
                    chargeInfo[0],
                    chargeInfo[1],
                    chargeInfo[2],
                    chargeInfo[3],
                    chargeInfo[4],
                    chargeInfo[5]);
            }
            else
            {
                result = "";
            }

            return result;
        }
    }
}
