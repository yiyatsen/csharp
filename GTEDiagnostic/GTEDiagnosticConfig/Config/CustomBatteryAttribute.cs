﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEDiagnosticConfig.Config
{
    public class CustomBatteryAttribute
    {
        public virtual double GetSOH(ulong fcc, ulong dc)
        {
            if (dc == 0)
            {
                return 0;
            }

            return Math.Floor((double)fcc / (double)dc * 10000.0) / 100.0;
        }

    }
}
