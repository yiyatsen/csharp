﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEDiagnosticConfig.InfoModel
{
    public class ChargeLogItem
    {
        public string Item { get; set; }
        public string StartTime { get; set; }
        public int StartVolt { get; set; }
        public string EndTime { get; set; }
        public int EndVolt { get; set; }
        public int Duration { get; set; }
    }
}
