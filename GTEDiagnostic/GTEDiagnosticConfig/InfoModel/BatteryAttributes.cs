﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEDiagnosticConfig.InfoModel
{
    public class BatteryAttributes
    {
        public int[] RTCDateTime { get; set; }

        public string MCUVERSION { get; set; }
        public double SOH { get; set; }
        public int SOC { get; set; }
        public int Current { get; set; }
        public uint Voltage { get; set; }
        public double Temperature { get; set; }
        public string DeviceName { get; set; }
        public int[] ManufactureDate { get; set; }
        public string ManufacturerName { get; set; }
        public string Barcode { get; set; }
        public ulong DesignVoltage { get; set; }
        public ulong DesignCapcity { get; set; }
        public ulong RemainCapacity { get; set; }
        public ulong FullChargeCapacity { get; set; }
        public ulong CycleCounted { get; set; }
        public int MCURestCount { get; set; }

        public int CellNumber { get; set; }
        public int[] CellsVoltage { get; set; }
        public double[] CellsTemperature { get; set; }

        public int[] LastChargeEnd { get; set; }
        public int[] LastDischargeEnd { get; set; }

        public byte[] Protection { get; set; }
        public int[] UV_HappenInfo { get; set; }
        public int[] OV_HappenInfo { get; set; }
        public int[] COC_HappenInfo { get; set; }
        public int[] DOC_HappenInfo { get; set; }
        public int[] SC_HappenInfo { get; set; }
        public int[] OT_HappenInfo { get; set; }
        public int[] UT_HappenInfo { get; set; }
        public int[][] ChargeLog { get; set; }

        public int[] RTCTime { get; set; }

        public int MCUEEPNumber { get; set; }

        public byte[][] MCUEEP { get; set; }
        public int[][] DischargeLog { get; set; }
        public int[] LastFCCInfo { get; set; }
        public int[] LastRestInfo { get; set; }
        public int[] CRateDefine { get; set; }
        public long[] CRateTime { get; set; }

        public BatteryAttributes Clone()
        {
            return this.MemberwiseClone() as BatteryAttributes;
        }
    }
}
