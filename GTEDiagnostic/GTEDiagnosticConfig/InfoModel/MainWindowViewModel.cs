﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace GTEDiagnosticConfig.InfoModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private int voltageAngleValue;
        public int VoltageAngleValue
        {
            get
            {
                return this.voltageAngleValue;
            }
            set
            {
                this.voltageAngleValue = value;
                RaisePropertyChanged("VoltageAngleValue");
            }
        }

        private int currentAngleValue;
        public int CurrentAngleValue
        {
            get
            {
                return this.currentAngleValue;
            }
            set
            {
                this.currentAngleValue = value;
                RaisePropertyChanged("CurrentAngleValue");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }

}
