===============================================================
Reocrd Time :			<<2016/08/23 09:40:50>>
===============================================================
===============================================================
[Battery Pack Information]
===============================================================
Device Name :                 YS4840A
Serial Number :               2215280016
Manufacture Date :            2015/07/09
Design Voltage :              48000 mV
Design Capacity :             40000 mAh
Remain Capacity :             41258 mAh
Full Charge Capacity :        41867 mAh
Cycle Counted :               2
===============================================================
[State Of Health]
===============================================================
Battery Health :              100 %
===============================================================
[Relative SOC]
===============================================================
Relative SOC :                98 %
===============================================================
[Recent Current]
===============================================================
Current :                     0 mA
===============================================================
[Pack Voltage]
===============================================================
Voltage :                     56881 mV
===============================================================
[Pack Temperature]
===============================================================
Battery Temperature :         27.68 ℃
===============================================================
[Usage Current Error Record]
===============================================================
===============================================================
[Cells Voltage]
===============================================================
Cell[01] Volt. :              4063 mV
Cell[02] Volt. :              4066 mV
Cell[03] Volt. :              4065 mV
Cell[04] Volt. :              4065 mV
Cell[05] Volt. :              4062 mV
Cell[06] Volt. :              4063 mV
Cell[07] Volt. :              4061 mV
Cell[08] Volt. :              4060 mV
Cell[09] Volt. :              4066 mV
Cell[10] Volt. :              4063 mV
Cell[11] Volt. :              4065 mV
Cell[12] Volt. :              4061 mV
Cell[13] Volt. :              4063 mV
Cell[14] Volt. :              4058 mV
Cell Voltage Delta :          8 mV
===============================================================
[Pack Detail Info.]
===============================================================
MCU Firmware Ver :            
Last Charging Time :          2016/08/23 01:17:17 100%
Last Discharging Time :       2016/08/23 01:21:54 98%
===============================================================











===============================================================
!gUTqXsY2sg5LpKpVXML2b2h1afVpkJCiCDuE0DxmK3wzEZ2kPxv9bZDFLD8TziMe+rdAcHDbrI5LNke+aBm7lh6R3zRBG3BeXiXEhVbevxg=
!1+oa9mBOTIxgg9+TCKq0C93vAA6/ERB3MxDkfy0U9TQ=
!IBuxbnv669hebJibWkjkPKUMtcY1FI85FCkdjvZTrgTnUTj9q05mBK04HNnDt7aPu5w3c3evRfK1fHUyreNd6SmmREfOYZ8wq/ZmSghPFJE=
!IBuxbnv669hebJibWkjkPGFBbuhawuo9OzHF7qR6NvFlCsaBlY4UJfX9wAhkxIqJrZYAlsmkhzigD0X41+I7Ng==
!NwMt7I/4dnWVeASwiGs9cgPLVTg4VYKuTP+zOni3eEmVziu4wUxg+v90F0GUPIXwrrcXcL+t7LLW9SqUxMwcXlI9bIvzNhaqv3ZSovNS7gW4ZhARZJCYOWImxd6WxRqUGtY7LutmWUPutvlz3cySmKR1Azhmnl87f0parrWXdcfR6gdaoK07TbAb0XR4GuTgZf8XpbTJvc41InIbX4YdF7UWwqaIOqzEuB5/eweLlvs=
!oUVgE7X7Typb6Ccc/mG78easWSapPl/Fw4M9amPwGqEO81M+H4chtphWU9z5JGRUS46J1pzDFKpZ/7SLRadGdpjJU8/bJqaX2LRDduKgXObQx3nqM74rHWU47QiAy8ykBndbvT8gOJIaqWp6XoodwPQkY1Gh2CrDLlnkVPEEVtuZZg2fA1ZQcwNJ+crYHAmy13RRU6r3XGyTv8UPQuf2RC3W92HTAbeWlFjKcF0hHlo5/LndPOO1xB1RhMVtttC2
!e9rVL31wWzxwl8gn6Aa9QtL85t35udODHZnyepwH/KyAxx6kvKMaKqpGR1G0SdHZOMZmdSs0miBh5ZgoG8YT2pn1W30Bbt9+BtXN1pxj+T3G4Rgvaa1r+7L9k5YlpFM6YUcQRGfkcN0KFQJbzIgBpv5eK/jA69JFts+IC/ouMq5PzeqdJZq9tyCc69w5N1ognY+j1bLKARy/wAZNuwyc8NJ1hmdZXV6Js9ql/DERsgs=
!CsosDCowxC/DdFJjPF48YANjKxEWfDNGXO3zz2XHZf7tSIcgWJddiEqjWCrkVPDlPs/CuTLuQWd/2jtwdZNjP/oww0JNdaiNHlviwzUWR8brwdHmLeQBLXzRsfNbNN/8pcYxyiJ9zwWBsrdoApSph4gt4KetTNWk9AwNquqdwQV+n+wYnCFXV06nmEMMVRYvDTUE3a9rv5pjCzgSyyYGqx0ys9NcSU/qFKH2DVtgZmE=
!klAo9jDxj769VWxOnGoowWe6l8WJKQm12uFmnEQcWI28KzWbpnsSgx7wDY+JmiDdT4Hsg2zqclMmAqPrLKryJfAETI1+yQi7wgyo652GgGxFYB4vu99Vm1xT3ogPH+vtnn0/NDO7NbM6H3Jqpb4XM/mcToBMAf7RaugEoDVCFidZV9ZhbSlCs6uBXKeOuwFyV5c8SU3n9WPEs+Z7+uOHAmUmO979/YEogSSei9BGeXM=
!1TfMop+KZGLTZB5z59YDl5KTbKHbIWkyQjfBpMw8a/MnLcKximf1hIE0WTY+MCMPxnYoe/esJ86tXqDClFNo7HWvriuXCHCG/iUMqUitQwcYOY2lctT8EdZgOxW4N953jERERFn66HSISb4kvqisdjq6MREO6LeR7BrtGztKdScm8gAiP1ai/EVKE9KLX5AnD9EjvS7hN4xBxYFA06AbkmuK8t0BNPyo/0UHv/Qh/g4=
!PCeFyVivCNIS0Ieq60YaQfnm3c3uGngnMohmERkzWPNYFyAohNzsG0nED42Pxxp1VmY2P/5EIgtIb3nf0tXnujiLYMkpmKdzIOy35yOffCggH0vx3Dk4eUDMcTPkbENxpaJg3uejEZ+QEKirijaxrwauVBsx6ui9U+rqfpeHzkNhabfrMWYxmaDwZwfOFQ2K29/B3Wzfc7xeABf2TXgSvz2BZJb4QDCdUmpJjlaJ2Ts=
!K+dRJlvlLxEw1lCJTA4v5kDFiVCOLTsX4Akegyf+WTNpHib3lGD8Hw6o8X7npr/yVxRce4/KYaf4b7GwG41lPAUNXb0pXJsAqTT12dpjd7YiuCX+PQ9ecvDsMQsngxcvRvKDLLOE7evXrPvXy6hEud7vrUrYZjyijEwvRznaQjiarofDztuf8Lqj08eNY0eVOkCtCPo/CAsH/4pF9VMaVkLDT+s81pmgYOuvBASdT/U=
!tMYwltLaJcCZ4k6BJ/EDI/mQBIAiaokEkwigjXmqlq3F+a0Rv1ncSl6rywNRyPxgW1jdgrZyCvHS7cvxCu6WH/1TT9INAF3N+6LtMD14tr+Ij3X1eGCEYsQa6TOFNkSR7Uhu6y4jbjoKQ3Pq5u6sWxr1v1FJDoATJDyYdz5w/4rNs9i06IMtgFhKDKGt3Utf6dy3/vFqNHzcFd5catuCVKhkvaNjq9oKcO7X83q+iAk=
!RbN1A2iapMxVcmXrY4peTBvIG3w//AyG/yfNFaCvk04uE3R5IPX8ylBQdYH1N8pDObPpVbRjw+AdDzgC+cHPy7vqUYqjBheHYbotN3hinq6laJxL6H39Boj6dE5oCa+oSw/WifeVuxV7GiyC7rbLHRPgi/54eTcjauu+YX6tIQKTUPbVIxEOm4Px3MTliZ5xhNYK+X14hvUbn9oy6lALv9qBJOvhZGMP4p5cC4e/2JM=
!pAMa+DYLf0Ssq/jPtb+CQ4f8RYjqxXxsybHCZ6Wbe0VELkeTZkDWw70M9xGgIAd3LY9UGA9F86uIfxCVGJbF8r6gJncRRXn5ye/8XGuziTepSKvtl6OVeoRe38JdgoQACDWmWMapeyl+MVRWYsOpsSdJZtgInp8YyoZTg8R8LL0Mu7fS2haLMuYtnT3/jV54by0fzb4LmVyo/Zitg+tQ7Q9hNva4yC5XJY451JskZ18=
!y4SegkxMQavL/qeQRCPGdvI9FIDzOndP0IvbpT+L2epfLD3Gk8xhHkVzByisYrmCNLzojHaQBMdoP88kc8zzZyskmE+iKc2Su4Nela+weCljGw60l8dAx8PilREPyMg3kbNiHD5eXQ+KxWN3bMcMYEllcgDT2NEx08lhIftfxHbFHY4zbQEdNj55O5Rlko3GiNd0mSxWAQgX0VSvhWMBjqLHEpK3aPqEed9fBRj5FLs=
!8nU0WJpiCkokr9vhqv7B+pbMzdbk+1DsPQnrN1Cee0XS6a/Vaav4T49wp1HDMSDbHUdTbLi1ojBM2GxEIfHXEGTZqz1+PmIj/rpI8jqKSRePByf3UHQY2cc9TkzjbCDWDNMJZTWuZdCTdzAXFhSblQx1JAOsyo18/yGwkPnwHkv9OGBfX/9q2cN4OcpMsvIcL7yAHoztd8e+UMOjx89ARdSplSfvWsaPGVI3S2QTF0E=
!Xo62BS+O96zmM+jYxCnJTi+qHyRHwJ/PXkuzM5DMyhvB36lf6olQ9TTu9KgaJQl89wEiaWHNVHlRRVyuwP7t2wvAUjlMRf/HmAK0l8kyQk2xMebuU7eZW2OokSYYOuXV+4Hfi8/ClNhoSO6ilLG0xIIbwe5/YXMqLsCUTon1PcvQjEc6j1c6uM8CcxFpxVQjww3+5NHih7DkfZpTsQxPekA13iz/a3BfkwON9lWOhCQ=
!WfDa7A3b003OKsOGFa10KaeeYOAitD5MZwyjURSX8F/kUnke4kVaTH1zMEPCYokXXj7xqpP6VLGhAsvj/rwXYjkz7uhEwQG/3/hOJxMtT89uGHvfFWqMGJwm4cDaaYXMVEO+H5djTr0/cV8xldjmPzodHVA2N2zjLfk7pHPvad9V0X7ycXOeAMc0b1PyGSXqRp4iCDLCh4/+C3ZD+PI2gp/MHNlqxWkseBbsx8q5oao=
!cyeetg/Vp0EYDor2hQ65mpIMPAWA5towh15bIroKKzj/lT3YTtRm2ChLLRij6rG0MPqfIrTnF/vc63Liu08rU+yz/Zd6klmV8xCL7roR5G1r7rzFw77oqQY8Hv4VB9INbXWkqPmg8k5IC3D6+7xUygK6ZPaBLi6MtqV+Sc0ZO7IAgl1mfiFc/d9uR7ZjC5Bk2Qdi9eXzgrmc7IxF7lB8HXbcO9gobkuni3rQ3FCn8KE=
!n4tvh5m0rLaZwL4GlEL0opst5CNYfhJ/cgObLh0TAOPX4alr/eBR1p008vzG1DjJqH5ILgC0by3+SjDiP1dPIIC26iWXwYglUPWMd33bwmgPXTf+gYFY6Z3MB0Y2K6ChgrtKns/wHAcXdk/Wwsgi07PwF9jSThszQuC5jL71qpbRcJjJVmvbj1avdT5iOK2cVpN7M5xXjN7Eit4oZd+AAgx0+n4AS0OA7+tBuPwJJKk=
!ptjfoPt1GzqYCqddTnmyHrizpjVl+bn3Fl6NzxaX71TAR6o0EW/UGrQlNugxq+60XCFvyMeFZssdN4wQ29WHV0ZY9fxG+g0ckJVFzibkMhqAVH5jYssyS9OM6W05aoiMpidP2+tl35ZWeT+m4aDcYHXFrmm5nFPVTZA4rQDdjCCk4WNPY9ZmhMgRCjRYlHzYiS+4m5jSVvKewGXf8XBTZQ==
!K3GDFkyxanMiQ3myr6+Fpmx5HCFHZ+QInHn5a714xf0hTVE5mM6qfRDxLOlIQIUzmjnmiWxVNP6OYUFT8JgdKYqtUx7kissu06DzuCxSAw+m59ga1hmHb+eJhzzc4sVrXS7DDBTHVt1uu9uBY/mkVe8aVfQEwY/4zSgeWWmgWWXLgiUrS9YXjPlIBwixTP7TL1kEYMFEBqUeOBP3iFmqlg==
!iGGLMffjzVPqzz8xZ8tLBifTcOGO28khgEnail9BPwXd0SiwFTLwdORzREp5LI/M3yqVZzakgGJw5tQpkC3n43r5D/o+XgW7yAxSyB6h30QM1EJ9bSrVK/DDr/U/ohuHSbcU1zQkPprBQscKuaNYnbeutpf00HwlkXrVuIepdbz0f3c90EwTA2KMEpuQ5ab5j+ekyvaOdF4ZV3r+8lXPjA==
!9pao5F9QnXCEotHgFB4HXv5sEGFVcg/uBKtRwZVhnDP7leIEHKEujSpaqeChCBiNXU66/cEctE7tJiFOjSLfouJA0muvzDtCVvLR7v6QzWYZFQ6uwIWTK6hRIfE59vmo23I5SaLgqGjH9HLCvRVbDdOcPWEbr8sp9QdTz39C467X4rvhIEOan3x3N7SVhmrinRLaLfo1hWZbx4sWr7hhPw==
!UZupdEzv491XgIOVMW+G89lE0KbCDVnDaIZHIAdilCNfM8u1tm7xYeER8TIgMKh0+EemeNtFNRzfGLXKnbhtwq+bayWxDCb77AITw+6IwEeYnZQVDVuJQ48d8TFnuvn9nbpnCObtYBzadndiyzHdiPfEtrS+EOiZW/JjhNFdyPW8SpD5+/pJHsRH+vtA7zdC0lz/uEooNwKR2nzJla49zQ==
!UyiVm5r7LrfKqarG+Dc+xmxwVSu89AjQiwknEPXP8FeQxmOnduiRDZ835UIXyPh07a6ju9NKl/+NVWa+k0Xtp9IQ6TFDnUQQRHYZso+PFugUCq9azIkjrQ262ipJD4aWQ2XFir+mpAAHwz0jtJSMvUN/sxflA+FbvBzjgFE5BPwvu6gM3YqeoAwDI2UNwypCK0frsdqq0Jar75eAfGQ0Fg==
!q8N6C4DiWXtDOW/RwnwNrioUrEFR+ZsAUUQEMdSQMc0Y+3sTZyXbiMHcgKIcAjgaHUMdw9AExOAY84tRTxMzdTYaIrmqoqvmcZlLgO3l34japoM3rcPSXJ+EeZpaYWxwBUk36Y9noOiZpFkJSp93jf4D+y13R2ZrCR8FV5W12PYgX4FsIVrgwsJLP12nR8K6RlMG8i7rSNzjz2YiFup3Rg==
!H6Ky7EJiDM7GU3PDD8V3V1kA2j8eAbnxi6HrKB7tA02n2y93h5kMm7WXKI89Qtk2cfImY1zppfdD//CZEmFnzVRr6wT/taDFE6LOQSfed8qUipnCxDv/mIIqvLxYdxwN4kQoUm7020W42D6hSdi8sfGpZlbE04rAO5MQJLDY0x7oAoUeZywLwqJugwC09ikfwfwbdD4Z31FWjmmh32ga6Q==
!2JuQtcbrYd8xUIhuyxcgkpH0lZ9gEMTlBe+WDI2OQQ1lKuoVUZo16US9k+v9e6LPDMZTd27/J8IKzALVTZwD+AfGNaK/Vc7i8sUeIGU2Z0aoAzv4I3s873uTQmOcrOy3+egYnFwNAj2L4yfoBYS/d8gPevBwhg7Ta86XLD/Jp8ObrhUR0FNE9c9ue/KrQlistjlSs1QLyYC/lw4By0If5w==
!eKu4Oz02qlYBUhUCkvN+jNG+hjB1+Mn68SDPbQFaXyrUIJRwzQus+Fiou/4CxcEVmF9GHAHNfNg1JcirahLmlDx/tGocJm6swDt13x1ZjLYxXWFd8IHTP43NF4K/tyI1U3v8SHY8gI/4movQmhlTUTj57CMIJiZWKrAjKgFoaIPRmM5R8nxdoOVb8z3bQCD6N+AH/mknVucU0iAY9kebaA==
!F1uqy+nvMH6ctxjR/CO2lEUuntFbUAGdzCUnj6aUoYhVjMJ+4VBtJG2FFaULnQC41PGmkTZyBRZEDxsGDO4Y1LcjCQm9wKoilaXxtfaVQnQDc3xkwt5FROWRvvw0jCaVXqoV3FLOvC3Npq2zYfteub6i9G/0+011L1MbETTKUF/BMq0W0Z68Tk1sLhYp3ViUgH3T2FLu375tSmHYE7xZZw==
!JAlTVQTfDRDua9zwD/V7mW9bImHhWNlL4GdjASg9GZfC5cghxafsnzIzTXFPXMfwLceDAgsND0hwgguU7hRQLOrA6aAfsr8+tR18ZDN/MLcuL6usSaf7fsO2im60Cf6tWbiLpY5ewLqs+kjMoUkuApDA20CwTLqxDh9p1S+IAago+iiKrVXoHxaDJcWkE4vaMz7oWWK1JXY8QH27HbhWRQ==
!1T4sEMHmdat2kra5EbrFJ6SY6FZLUhjxRxeuRMRFw+qZupq0n4hbFX8AUaWbOC5/wtIA6TA3s3UYFAP/XoNoK8kcgirpKjs7BFbEj2oUoFxC7kFY+hotvZmxSLt9dZAhtIGzb3uIhcy8hGk+mGN+rZRvB+lJlAY6ZOTKYekFDXt6X0JurcxBjH5nmBItYQfZ1mVI3D8LWTryfh9wzP71/Q==
!as1Oi/WLzWSrbj19+jJOBzb1RpVrSAqhLoig7s/0bdtUardpG1VBqqrP+CrFvmle3xjbN7YXqasroQOudBwczmNifAFEJxeA8KQKgJyv0C5UXorwBhSGPFU4Of4/ifHmPb20R4fNpA2dVUICyqeqYk+qAVMXqtGeodDz0nfE5f6KT7VQV8HG5cjP7IDBtBqCdV06FLJx4t19iUaE+pAlgA==
!tSRPbiAXDKvKYuqIDl9E+UmNoHh5yaIIgePPDPeUMTMxsxnUPjmcGO1OvWa09iILC2KKgezrQT6bsVB6Bkl72myEuLCrADzWGba4ecAb06O2zZ5trsNYBYOTBPzyijDENJm/K3NZGKjbGHP01J3gHQCNYy2Juay6VvKE9V4zBME7SJHMFQThbP1T6Rzc59z0Kd/5W83TOoTL6QsTm/3YYA==
!TSIWBLM+5r2LmdR/zNUmg8OZKnkW+upfDt0fQ2ppiy5wIF9X3xGPG6OKG+aH1dyKIx41tsGnFWUzwJpTjkaym92ft8mUK4oLkEhhRrpIqitQQ4ROLErb/I8Ya8dxtiAgU6zbE81nyoMOabhiZQ65z4zcfI8ksiohrPykgvh6vgaUJt/eyEb3h35wwMXlDKt/nUu9GXQ9xIpvhqQCxXCPkg==
!IPyIRdBhJRVIlW0QN5ZIjS8sS2sg4YAI4gj+DWJkGB7TKFcKQ/QU6z00sZTsQtkB12HCQQsh6uVGuzK8r7JR6Iih/HTjEcJNOfALC0/5I+zwwRGDlh4jcqmoD1YFAZqMeOWTUYKrKup43RDuHgnozvC8zc6cmyDEOIzJGQulUt0OZxVQOxOUyzIIz5HgyNn80WY9tSiRlGFhye1542bxOw==
!PG87+FPgP58u4ssWJr85lTqufvKkLpdVppSjacfSI+jwlyyvQpXEQiYVTqnUWNhOKN3T/9qpigC51Sdpihmoncs+gVj2/smuWA+P/JEOCekizjwoBX+xO+3CDRNvbojnvMFyT/QYtFuWXV/N/JLlNSfKlkvzuOU+PbZrGO6/dbDU09xvkgFoq2ByVnpVH/48Q8PaUX62yoO3ahR2DBtkgw==
!GXD7qsrD5rToz3am0GQoc/72ovZ1o5+7mtz/8RRiRK34UuPArjpgsAdOZ1OqDbv3RepN6cAkugNqe4g6DSkafy8nQoAUH1kOJph33KVtGNqYmZWhw0c/34kYeftT2tX18VCPUYq8+aEt/lPJYOTej9tT97w8TQTvuV04CnLuEPeG7gzKgoT7hIOgHpyaZjPnwpvPigfn/4ZT62iIxGXOwg==
!PZ5WOTAbkWO0nA5q65HGLhXkOld3DLoevv0sGhoQ4yklZvUUW2gyRNarBO+/+Xxb7D7LB7uNqWyhRDO+PpI8th2JUpXu5/Lgt5Bngg0JWoj1DYJtOUI9oZ0ourPFs58ESMaDcww1YvM8LX5fkECh657aRm40AJXCFdFi26rWmlZbY+ehAr+65JIaQdF6yxRSchoLVFFcNW5PX9lXGvRCKw==
!br2VwOFAz9/JpWY74Q7ZVXLBDk0/nHkacZQZD57drQ7qchIdXRQ1FjFY9TQYmRCHqGdl8C/hqeQB9PgBQNKGd66GUQc94UyyM8X7qZ+/K9fUshCNE+6Bx3JAQyrpE3DUGhVOmOTJgf3hjAyQEcQQOmzVWCzTqHyny+wEbjMOtWnM/Ej7KbZ4nDXduFLsOG9sm6vjA2vuG1vYHpw2yq0Khw==
!0pQyrIvqEiq2ASRAS+/BD+YGTOYxSAdXfXpnnrx3NWFuAQFQINLhonOacsstefOJL7LbX6Jrxv+xpCABZTkR92T/regv0dbU9XdZnphK7d4sO5TEuTuRPssrgvOJlP5nPyjKOxD3Nd18rxiV0DvhY2eIk4ElKL9hFsIsw8pdPK8oSQ9Xc1Z8EY9pHL2sOBuCpS3eL2RFXqpjMKlrTyyxLQ==
!aBObzLt+i5Ra6JmGWPTbTXVgumABLnYNchLEEQLPNPYGygfMt/KGpZr4nwtkuchm1Ez57qlB/ylATCvM+ntNCP3pK40T+PTJalD3RRCNQT4EE788xedJyHU8FAjNScHlmsiUFw9lijk/gSOW4b6fz5e0GqIP2EfNX/tTyobca81hm0Z7O/hoSSHHRVzanZQvH5BjEeJOi3VSeboL4SIdmA==
!5gIdQ8Zmo/G/oXViPM3eZ6euEHz+d61vsLdJSDMMgqgT4mqOwVrvtQMNCFjCaHI6EBsb6YD1PaRHHehb2O+u17RTmUtaETGOAgeFUTTzMlYP/pQk5RGl/Rz4dUM+FQBPfDay0ss949MH4zXAutZQZ2ha2KcKCUax9L4EVEW5QgdJ2BDDoLqqkuTf1JjY3FO2iHKLMZ/Aszgnfsy5mML1DQ==
!gQh/SghA8VImVb9sq+OGBbEW+oStGIOvGMxiiPwkwfOaj7N28WI/GnaRMcL21qU3bQx3QlAuSm4jHlf1pNE+BbUDzpAdXQx8Ir49IGvmyBd485x5r+f9Pzq0eouqMSwFoCnoOd25ksJsmqZV98tzov0aLeDlkFN3+TICbIWABcr4NhVDXnhXzO9it0IMnXfK/+gUL2jNWda1AYmCMnSmGQ==
!ur/hc/V5Ig6KdGrZNKu//v0CTsKRIqvVH7iqsBaALTIGGaEfFGuQyXAk5+m0Rf2hiFFLbDc0yMfX9dDpM2bQG9+Hg4DNWLAqiEJ8bw08kakXa2DFF13NSS1lIsZDKF0nnpm9gyNv3gM5MQxOzmuc4aM/Iy03CgAHN2z+PqDVruCiVGOAjApxhae3rjDW1YywOWZuQZhnHCDI7gur2V6sbA==
!+cflGysw1xdLp1H5izG4LpZKwaEMnT3Y+nygKnIeQt5+Or3XxpYU5z5YmvGLvY58iZifmJM2V8lw/s/lLKgc8NN0BXtjaGgM6YXghVPt9YZ7jooHeTxp6ZzxMkf+afafO5PhetynsWOwPXI9zdTqfw/avGdbDvyk9+4+LobypRZZ6L4rLlkwJnh+KcZC59JaM4y3QnYAualHfbrbedjGrQ==
!bb8kElxMqjX1JIp3e/w+WxIu648b063T/Yk293u2YaaiUAcBD26VjHoqWMvkjZlo35eGjEtqUs+kviIIVW1rUIhnnk3SR7odW1OAziyMbPK/haFPDRJc018sds71Li2nj+fY8fYJg4XltinaCEunoPdl10r7tWiLBv4XnkGKp0roBxj0GGYMJjZK4HJ4XvnGqsq16WhnP9vMLZG8SyRHYQ==
!A8gS9EA0QhJg+EM+iAIiqL6vX6HkLB+4uFzeZ265hROEe7RXwggI7alqdXgFs317iTykgYKp+3yEHGx+AEPCG/uA8MdVrvfjQs/bikGz9uVxUK0rQqqKP4i1xXwF//O0M+plTZyJ8enKhZ3vq/DVeSFnARe7V0TSCjdHE3VyAhhOKAI5dZMYSCez9B8fsgD3HI0uCqTwiT6eKWfhq1vO4w==
!eCOGMBiVKSRM6dn1HETiBjWcaEifa997a9zsaHRwPriJwuVlg7a9Y9Z7g93l7nBaAA+jHXp05sklb0igW4snLHtK+aeanX0lwCcZApkibBoLXTeHIfljwDrv/15hqQ2AmECpKrE4hb/QkE+Ul/WViAykkgBHE6JVNHcGs4GKucTSyiTc7KIciH9NMziwOkQoAVjoYLKbBXcFMM5vQRdSQQ==
!RI5hXZfFzFRc9U2F4wVCukaBNjFCB7O4oSh8MJRNqg+peyRQFjJWb/1y0PdwaIG2XdsUSjFhr12cWlFUq//pdgKUqRMFKuE/oRxsrOpl+q6tM8Knbq4ZwD7A2BNY5+wkMhxlN64pLYgz4HkPvl9GXsaPXxST4C0dRafZcwHFrft1zTGxzAOWiUIniimKOjRse8LfWEXveH4FMqTWK1uEzQ==
!00RkZd6UITC/KeAZvImpCrRXPS17ldCTnVmSsgTNuHuZUhoki9jZ7sTyMR5Xfs/aZkPd0bg/JLf+p8wl5QdYivKVa1g98TxEyDltAvdiCXq6l5et0lDrH6Hszd/13/n/g0VHu/PpKycRMtMhjMnjV2yzhyd/gHsafr2iQZhXwg3Rmp3UdA+u7670IzQGPyMsTwI4SN9jmcQqrMpOhtQd0w==
!jO5zSFuO+85fl16QO89Xtcz67rveqCI5KwpM5XgCjy6aCjj+1tozfHpujRPCfr5HpD3JzRcH16P3bQFBte4bz3MStaLZrs5PVCj0IOFGNIN/Bcm76UZamXms5oTsS+PBN/zGLTS/diri1SjPMyuqanECfX8mLanXEBOYvDiZKKMQXwCFagPNJYUYqOLpwOnB3c6JQXpgvdqfQq+9jz48Pg==

===============================================================
