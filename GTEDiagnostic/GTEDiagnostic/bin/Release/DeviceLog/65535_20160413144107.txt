===============================================================
Reocrd Time :			<<2016/04/13 14:41:07>>
===============================================================
===============================================================
[Battery Pack Information]
===============================================================
Device Name :                 GV7253
Serial Number :               65535
Manufacture Date :            2107/15/31
Design Voltage :              72000 mV
Design Capacity :             53000 mAh
Remain Capacity :             39750 mAh
Full Charge Capacity :        53000 mAh
Cycle Counted :               0
===============================================================
[State Of Health]
===============================================================
Battery Health :              100 %
===============================================================
[Relative SOC]
===============================================================
Relative SOC :                75 %
===============================================================
[Recent Current]
===============================================================
Current :                     0 mA
===============================================================
[Pack Voltage]
===============================================================
Voltage :                     78195 mV
===============================================================
[Pack Temperature]
===============================================================
Battery Temperature :         24.89 ℃
===============================================================
[Usage Current Error Record]
===============================================================
===============================================================
[Cells Voltage]
===============================================================
Cell[01] Volt. :              3913 mV
Cell[02] Volt. :              3913 mV
Cell[03] Volt. :              3913 mV
Cell[04] Volt. :              3912 mV
Cell[05] Volt. :              3913 mV
Cell[06] Volt. :              3914 mV
Cell[07] Volt. :              3912 mV
Cell[08] Volt. :              3912 mV
Cell[09] Volt. :              3912 mV
Cell[10] Volt. :              3909 mV
Cell[11] Volt. :              3903 mV
Cell[12] Volt. :              3912 mV
Cell[13] Volt. :              3908 mV
Cell[14] Volt. :              3908 mV
Cell[15] Volt. :              3906 mV
Cell[16] Volt. :              3906 mV
Cell[17] Volt. :              3907 mV
Cell[18] Volt. :              3908 mV
Cell[19] Volt. :              3906 mV
Cell[20] Volt. :              3908 mV
Cell Voltage Delta :          11 mV
===============================================================
[Pack Detail Info.]
===============================================================
MCU Firmware Ver :            GV-72-53-V1.00-Rev01
Last Charging Time :          2000/00/00 00:00:00 0%
Last Discharging Time :       2000/00/00 00:00:00 0%
===============================================================











===============================================================
!gUTqXsY2sg5LpKpVXML2b2h1afVpkJCiCDuE0DxmK3wNSHTdR5r8LVeDIyX+iiyK+hTWFxSfNNndUNKjv5zlA3Na89HHLdFPD16FuH6/zvc=
!1+oa9mBOTIxgg9+TCKq0CzHxjOmwR+LDRKvYepwoiiU=
!IBuxbnv669hebJibWkjkPKUMtcY1FI85FCkdjvZTrgTcfRFjV+uQ5HJR45hciljJ
!IBuxbnv669hebJibWkjkPGFBbuhawuo9OzHF7qR6NvHo2pNkCfJpxhFouz4uC3zhTybTx7cGBzqn7B3QbpaMsA==
!kc49klAa58swnbb0iz36d/Zh4OLkSQan/Jvb/AXXXrRx9yQ/Zsc9MypYjaQf1jXo3sYE2HRe36mmH+s9D9EywmXkliLzP08OTFW38BVSEkhVS/DITEd2Uzinod94sDrzPXOW/koLBZgSry3A2ZTuEg==
!VVuRvthiCJrl2pB/CXCl+m5xBmQUiuTSUpSe5cwRxBnXl5JdLy/8LX+AMvdfSnN5pTz3DP2nK0WbtnEGzSKJBQlp49K78VS/WSbhY/g4Hq1wD14fphZ0xBinRfGiOyDD
!sPZCiRvoezJCmG1e4pZQbt5B8y4oiSN+LsVVPblH6nQcun/y/DAxmj7aRLeaot/EcFTg32OspGnKO2Kumglf+mfZyjBSFE6tn/PywN6WRe7gpVVhXKiOj8CNc6R8k3tF
!Nb7rXSj50B24i+YZV4Yl5WBq7O7HbuiHDHNUejpPFvVh/jnu4sgXyp8j8sR8hAvWo+Mt0JaaFVQl2HesrJt/gxY+8Tay3i5npKtXH18Asc/0OqTRNEuZ10mEAGGQy30JaU+mQx+D7HgmTaaD1DpEQulBRWV5gCP6hXvOX7E4WPfuzPXfEbh9m5RIR3U5FDarYODdusbRd52vvcB5Dp7ZmOcryNrIQaF2o8rxUyEbI/o=
!DZV3PoeuRVh5sN27STfhD9PZBqbvXAy19lh8XN+F8wQt4fSLRrfrpqnJmE1k1PZB6Bs5kwKhbKicMeYB9cSbi9REjX6cZ9B9/rq1a/281880M9biroTA6WDPt2NuWgJhLkz6Bh7BkwZ+XV6TWjCE5dpvCK8GNsNAp8CwRDWzSnPe25iuQmqgT1BVrZGvVKN7xinDSmSWc4mQFV2BauRnXVtAuSAMYFM6w47VeTZokTfaRhWoQDZIhRsJGo7Xz1I4X3wrVW9+oLv8ANper+3bL+zlBbfmE8KxucFNvVJfjzs8eJFb+QxJGHfbFvbYmSe4
!sPZCiRvoezJCmG1e4pZQbh9Ws6EVfLZO1RIBqpI84wk3p350dY/CtNQ8VliRzBE01ucZR5pk76B1mPH47GUaliHHcU25RhLsYajhCbpUVbMmEI7xGb3wLGPq/VRAIdNy
!NwMt7I/4dnWVeASwiGs9cgPLVTg4VYKuTP+zOni3eEkz81xq4HStfH0WlDGpaSlFIotfvf6mIh5Qh4l1/fc7s2wrBxk4URplbh1g6jM9z8+2WBqvdEcn+2b1rKGJz5gxpYq1DGifc6+j8AurN2Ye8UOsV9idn1m8YUwOY1lbhrvDTeYG7i5Y9xW07gwBgwP0RsRIHr/6+7Bs8Omipr5IRw==
!oUVgE7X7Typb6Ccc/mG78easWSapPl/Fw4M9amPwGqHJ286sobk9n0ZZ3b3QviZydfYHTT00r4YWRQWGJEEV541VACT+0FTJtndFgOPNB47htV1oK2eaLa1N1C5ga00fnLpGwrDzE8wv9+MZpFNETMU91jBetSfJIR7VwI7rVcYGqDm/TAYCUuyQw8IXwdKBGkhygxiEGKsc4TzT8nuaAQ==
!e9rVL31wWzxwl8gn6Aa9QtL85t35udODHZnyepwH/Kw5ib0AvptbtN+mxUGVS0xUsVSvKt5KPmvfcOFpYUg2OR/7wd7/Vr0OcPiAXqOaYBT10FdxPUDY/ISqhSNcrqRc80euhnJ0g5pmQtoZ1/1oZneE3wVHCZByDdwLvGa9sSRlv9OfRj4UXfrOKX1Y0fCmgiZD50Sn3FMgNPlZEZJ90g==
!CsosDCowxC/DdFJjPF48YANjKxEWfDNGXO3zz2XHZf4pHa6LF8aU3xhGa03IDJlW9JHUCi83vlAPuo8kwitvyJWsezkH7cmChPReDCH7ZPNZSBnkD+9zitW/8R3QcQnKmPUM+TzCSC2Xv5Ne2lK+U4U/NKn6eZk9nysXzohe7m13ksfuG68oDVMIzIXHJix+Pc+I6r8RepRLlQkoLFrJGw==
!klAo9jDxj769VWxOnGoowWe6l8WJKQm12uFmnEQcWI1GJrrf4XKZhnWnqbEwMNKfQcy6rn78t8k2cYCqqnmaZ1cHT9r/hf4q7OGG2zahmx/uc+7ifkIqweT7OkX/son6rtzQNJMArJqJ8VLzw4tqHSYBmOdEkk57v8+DsH45aPSF16iQw6DcWSfAEgoPvPh90k2CEdwL3FzyRIjLlBushg==
!1TfMop+KZGLTZB5z59YDl5KTbKHbIWkyQjfBpMw8a/NMZwDaWhwr6Qo29RFKGq3dbBHlLGQMZFn/aepc1GH32di+3qN3R/vtMBNrEgwoIyWuSWF8tMcshfWM8iq58ydPuZwmLqgQZlshS3ytJYLOjt9w/986PDTfAMgs9qwDWKpbfF8lNpORPpSJqoTngMg+sdc+2Mmx0jaYdl9C8IXzwA==
!PCeFyVivCNIS0Ieq60YaQfnm3c3uGngnMohmERkzWPMftwyjEM51GqZU+pYIzkkIv7R48jVQF2V5fd7sDZVP+yvFj2VjswOZ6ftxOlboX76bbXR8MvDypTq2dTJ20ycAtV+V6aTbly3jMectn3uuDY8iaaMSLcL97NHrbBAANCi+MoR/b6/1LUXyRk4zXeobOA38+V3+2pcTZTNPcznnqA==
!K+dRJlvlLxEw1lCJTA4v5kDFiVCOLTsX4Akegyf+WTMrmEG1KhoaO3pfPcRkZDBGMTsWYlO3JVpzP3Y1ERab8VHLMqbvF52ADdBj4Cboc9hbHLqztVCJwje6Jjs5j1EsFWGvrEaVX0g+/6YDVZ3kdug6vjWX6EMHD7b02Y3ipyhcGiCd3lE3jaiDjdvo7ZWVOORJQzyB9AtqWCq5lkLsLQ==
!tMYwltLaJcCZ4k6BJ/EDI/mQBIAiaokEkwigjXmqlq0rLxB/H29S8NsI7ZP1Cm0110MhJqTFwgKUtmmHk7Av63onTOQAKZAyIYS3kgXcyaVqEamcn7K90ysBSnTz7YXFBHq8dJznT0QmUSn9+swKTfX18irA51Ue8J0e0nPnHbVMLF0t1CCOwjKBQZIxCcRmrK9N3LzsvwlAREzQoBg7xw==
!RbN1A2iapMxVcmXrY4peTBvIG3w//AyG/yfNFaCvk07lF4ZmFBHp22izTj/C94eYestq1tE9S7o3sanp+6KWS2Teggm3yaTQPDHE6Ue3PIzNKFrTHlm3BaUWCRdjlMX+sg9Y5dRzeMGukulmPL+OMGucvnpWGcfYmxNpBcQs/LS3n/8BNEQjurxc1zmNNwZm7a0URemTgm9A1CR9UrVUlA==
!pAMa+DYLf0Ssq/jPtb+CQ4f8RYjqxXxsybHCZ6Wbe0WnI4jqxjM5PtDLUIP6bgGs4WgpQbTZ8k735iTCfQvIcuNYYsdIy2BynfRLjEM911oOY0uI1xkeRa373GZ7p4FXMUnozCZEcpMkYrA7c1Fb2Qwdn6A/3+WZNNGx6uj/UehSByvd3aXln08qGEr8TrOc0H7K5XErQ+OD9Q7qb3EGDw==
!y4SegkxMQavL/qeQRCPGdvI9FIDzOndP0IvbpT+L2eoCPtX/V6iJDO/xJrit0hYmtwyggg2FdetOM19lB4H0xB15/9Uk0T2vrZuPQtq5fJWghHw2mkhBcVPm0oDB6OqDZxzLz/nEJzGzaPKoNf+ZH+PhPeUNjRsh1zsLvdXh03pNVDWtvDuLJ+o6AolymEbql27J0Ukby5A+ofT4bqD7cg==
!8nU0WJpiCkokr9vhqv7B+pbMzdbk+1DsPQnrN1Cee0VFW+Ppp40D3jrfX6y2/hacogdB9zd4SjsqjySkAkwzRnzfYcKUukq5HYe5ryrIJc6zH6/1h1mhWIWVdcLdgUBIKrwMfccMytNAePBpWB/mLQM8hWS8rEe83LqXOaO89IASiKtyEzNGbvXceFofBh+OW2XygMQCirDqLhC9rXqrdw==
!Xo62BS+O96zmM+jYxCnJTi+qHyRHwJ/PXkuzM5DMyhtfb0r9k0hzGQZEzWH1akD68d03YAnQ4x7nI4nZuBImUyPqpStiBan7hHMTc4ZuYyz+AeeQ/coFSyX4mygoYbGvkTeqKbD/QEnH27psEW6H6ItbE96zF0VXesX7KO9cm8n59YT4lLiV26ivNEQoFCr+8VkjN+6inT6At4Hard2fcA==
!WfDa7A3b003OKsOGFa10KaeeYOAitD5MZwyjURSX8F/rGjSXx9tfoZjg5UdENRIaSkXHrUlLOs6awmdRsH4ffC/dzGbqRT/7J8grCBHv1M+ihzSJUfreGKGn6bItzX7/+TK+5dffI+eyjE7rwWl+ZdUJT2x74YijjLdfN58EBqCjRQOLWK7eNcvljzzalHMFfZEOMG8HwJn6pkFv7SUgGQ==
!cyeetg/Vp0EYDor2hQ65mpIMPAWA5towh15bIroKKziDpW2uoCEP8hiKCKULzFd6BlXNslLaXhr4fFONL43Ht5tcU7S59ToQqSMVbfS+j/zITmyBP4GUAZY3YNMpmeAeCbtAvCocCtkk4zcBpJ7pwjIPEKKynpI9VdC+fyBsXDRcjvhw2C/K/iVurlccO+IAS5L20OUKaP6kgeu8WC7jZw==
!n4tvh5m0rLaZwL4GlEL0opst5CNYfhJ/cgObLh0TAOMEsMGsezaCIwao1HDbje/ryOnJ7tT3ajW/ZAVFOrKNhQ0CubsP9YIgVR5OzSsY7Ma4SbHFexJjYKHxDhbgIDjGSpLyqw8FBgaMyH/Bd1yNQP+unQItdvEPT2TZGjNmk1Rz/ygAzTYs+IVPZxI777BuVusQ1EUwfcFiVYgtZ8McPA==
!ptjfoPt1GzqYCqddTnmyHrizpjVl+bn3Fl6NzxaX71TAR6o0EW/UGrQlNugxq+60XCFvyMeFZssdN4wQ29WHV0ZY9fxG+g0ckJVFzibkMhqAVH5jYssyS9OM6W05aoiMpidP2+tl35ZWeT+m4aDcYHXFrmm5nFPVTZA4rQDdjCCk4WNPY9ZmhMgRCjRYlHzYiS+4m5jSVvKewGXf8XBTZQ==
!K3GDFkyxanMiQ3myr6+Fpmx5HCFHZ+QInHn5a714xf0hTVE5mM6qfRDxLOlIQIUzmjnmiWxVNP6OYUFT8JgdKYqtUx7kissu06DzuCxSAw+m59ga1hmHb+eJhzzc4sVrXS7DDBTHVt1uu9uBY/mkVe8aVfQEwY/4zSgeWWmgWWXLgiUrS9YXjPlIBwixTP7TL1kEYMFEBqUeOBP3iFmqlg==
!iGGLMffjzVPqzz8xZ8tLBifTcOGO28khgEnail9BPwXd0SiwFTLwdORzREp5LI/M3yqVZzakgGJw5tQpkC3n43r5D/o+XgW7yAxSyB6h30QM1EJ9bSrVK/DDr/U/ohuHSbcU1zQkPprBQscKuaNYnbeutpf00HwlkXrVuIepdbz0f3c90EwTA2KMEpuQ5ab5j+ekyvaOdF4ZV3r+8lXPjA==
!9pao5F9QnXCEotHgFB4HXv5sEGFVcg/uBKtRwZVhnDP7leIEHKEujSpaqeChCBiNXU66/cEctE7tJiFOjSLfouJA0muvzDtCVvLR7v6QzWYZFQ6uwIWTK6hRIfE59vmo23I5SaLgqGjH9HLCvRVbDdOcPWEbr8sp9QdTz39C467X4rvhIEOan3x3N7SVhmrinRLaLfo1hWZbx4sWr7hhPw==
!UZupdEzv491XgIOVMW+G89lE0KbCDVnDaIZHIAdilCNfM8u1tm7xYeER8TIgMKh0+EemeNtFNRzfGLXKnbhtwq+bayWxDCb77AITw+6IwEeYnZQVDVuJQ48d8TFnuvn9nbpnCObtYBzadndiyzHdiPfEtrS+EOiZW/JjhNFdyPW8SpD5+/pJHsRH+vtA7zdC0lz/uEooNwKR2nzJla49zQ==
!UyiVm5r7LrfKqarG+Dc+xmxwVSu89AjQiwknEPXP8FeQxmOnduiRDZ835UIXyPh07a6ju9NKl/+NVWa+k0Xtp9IQ6TFDnUQQRHYZso+PFugUCq9azIkjrQ262ipJD4aWQ2XFir+mpAAHwz0jtJSMvUN/sxflA+FbvBzjgFE5BPwvu6gM3YqeoAwDI2UNwypCK0frsdqq0Jar75eAfGQ0Fg==
!q8N6C4DiWXtDOW/RwnwNrioUrEFR+ZsAUUQEMdSQMc0Y+3sTZyXbiMHcgKIcAjgaHUMdw9AExOAY84tRTxMzdTYaIrmqoqvmcZlLgO3l34japoM3rcPSXJ+EeZpaYWxwBUk36Y9noOiZpFkJSp93jf4D+y13R2ZrCR8FV5W12PYgX4FsIVrgwsJLP12nR8K6RlMG8i7rSNzjz2YiFup3Rg==
!H6Ky7EJiDM7GU3PDD8V3V1kA2j8eAbnxi6HrKB7tA02n2y93h5kMm7WXKI89Qtk2cfImY1zppfdD//CZEmFnzVRr6wT/taDFE6LOQSfed8qUipnCxDv/mIIqvLxYdxwN4kQoUm7020W42D6hSdi8sfGpZlbE04rAO5MQJLDY0x7oAoUeZywLwqJugwC09ikfwfwbdD4Z31FWjmmh32ga6Q==
!2JuQtcbrYd8xUIhuyxcgkpH0lZ9gEMTlBe+WDI2OQQ1lKuoVUZo16US9k+v9e6LPDMZTd27/J8IKzALVTZwD+AfGNaK/Vc7i8sUeIGU2Z0aoAzv4I3s873uTQmOcrOy3+egYnFwNAj2L4yfoBYS/d8gPevBwhg7Ta86XLD/Jp8ObrhUR0FNE9c9ue/KrQlistjlSs1QLyYC/lw4By0If5w==
!eKu4Oz02qlYBUhUCkvN+jNG+hjB1+Mn68SDPbQFaXyrUIJRwzQus+Fiou/4CxcEVmF9GHAHNfNg1JcirahLmlDx/tGocJm6swDt13x1ZjLYxXWFd8IHTP43NF4K/tyI1U3v8SHY8gI/4movQmhlTUTj57CMIJiZWKrAjKgFoaIPRmM5R8nxdoOVb8z3bQCD6N+AH/mknVucU0iAY9kebaA==
!F1uqy+nvMH6ctxjR/CO2lEUuntFbUAGdzCUnj6aUoYhVjMJ+4VBtJG2FFaULnQC41PGmkTZyBRZEDxsGDO4Y1LcjCQm9wKoilaXxtfaVQnQDc3xkwt5FROWRvvw0jCaVXqoV3FLOvC3Npq2zYfteub6i9G/0+011L1MbETTKUF/BMq0W0Z68Tk1sLhYp3ViUgH3T2FLu375tSmHYE7xZZw==
!JAlTVQTfDRDua9zwD/V7mW9bImHhWNlL4GdjASg9GZfC5cghxafsnzIzTXFPXMfwLceDAgsND0hwgguU7hRQLOrA6aAfsr8+tR18ZDN/MLcuL6usSaf7fsO2im60Cf6tWbiLpY5ewLqs+kjMoUkuApDA20CwTLqxDh9p1S+IAago+iiKrVXoHxaDJcWkE4vaMz7oWWK1JXY8QH27HbhWRQ==
!1T4sEMHmdat2kra5EbrFJ6SY6FZLUhjxRxeuRMRFw+qZupq0n4hbFX8AUaWbOC5/wtIA6TA3s3UYFAP/XoNoK8kcgirpKjs7BFbEj2oUoFxC7kFY+hotvZmxSLt9dZAhtIGzb3uIhcy8hGk+mGN+rZRvB+lJlAY6ZOTKYekFDXt6X0JurcxBjH5nmBItYQfZ1mVI3D8LWTryfh9wzP71/Q==
!as1Oi/WLzWSrbj19+jJOBzb1RpVrSAqhLoig7s/0bdtUardpG1VBqqrP+CrFvmle3xjbN7YXqasroQOudBwczmNifAFEJxeA8KQKgJyv0C5UXorwBhSGPFU4Of4/ifHmPb20R4fNpA2dVUICyqeqYk+qAVMXqtGeodDz0nfE5f6KT7VQV8HG5cjP7IDBtBqCdV06FLJx4t19iUaE+pAlgA==
!tSRPbiAXDKvKYuqIDl9E+UmNoHh5yaIIgePPDPeUMTMxsxnUPjmcGO1OvWa09iILC2KKgezrQT6bsVB6Bkl72myEuLCrADzWGba4ecAb06O2zZ5trsNYBYOTBPzyijDENJm/K3NZGKjbGHP01J3gHQCNYy2Juay6VvKE9V4zBME7SJHMFQThbP1T6Rzc59z0Kd/5W83TOoTL6QsTm/3YYA==
!TSIWBLM+5r2LmdR/zNUmg8OZKnkW+upfDt0fQ2ppiy5wIF9X3xGPG6OKG+aH1dyKIx41tsGnFWUzwJpTjkaym92ft8mUK4oLkEhhRrpIqitQQ4ROLErb/I8Ya8dxtiAgU6zbE81nyoMOabhiZQ65z4zcfI8ksiohrPykgvh6vgaUJt/eyEb3h35wwMXlDKt/nUu9GXQ9xIpvhqQCxXCPkg==
!IPyIRdBhJRVIlW0QN5ZIjS8sS2sg4YAI4gj+DWJkGB7TKFcKQ/QU6z00sZTsQtkB12HCQQsh6uVGuzK8r7JR6Iih/HTjEcJNOfALC0/5I+zwwRGDlh4jcqmoD1YFAZqMeOWTUYKrKup43RDuHgnozvC8zc6cmyDEOIzJGQulUt0OZxVQOxOUyzIIz5HgyNn80WY9tSiRlGFhye1542bxOw==
!PG87+FPgP58u4ssWJr85lTqufvKkLpdVppSjacfSI+jwlyyvQpXEQiYVTqnUWNhOKN3T/9qpigC51Sdpihmoncs+gVj2/smuWA+P/JEOCekizjwoBX+xO+3CDRNvbojnvMFyT/QYtFuWXV/N/JLlNSfKlkvzuOU+PbZrGO6/dbDU09xvkgFoq2ByVnpVH/48Q8PaUX62yoO3ahR2DBtkgw==
!GXD7qsrD5rToz3am0GQoc/72ovZ1o5+7mtz/8RRiRK34UuPArjpgsAdOZ1OqDbv3RepN6cAkugNqe4g6DSkafy8nQoAUH1kOJph33KVtGNqYmZWhw0c/34kYeftT2tX18VCPUYq8+aEt/lPJYOTej9tT97w8TQTvuV04CnLuEPeG7gzKgoT7hIOgHpyaZjPnwpvPigfn/4ZT62iIxGXOwg==
!PZ5WOTAbkWO0nA5q65HGLhXkOld3DLoevv0sGhoQ4yklZvUUW2gyRNarBO+/+Xxb7D7LB7uNqWyhRDO+PpI8th2JUpXu5/Lgt5Bngg0JWoj1DYJtOUI9oZ0ourPFs58ESMaDcww1YvM8LX5fkECh657aRm40AJXCFdFi26rWmlZbY+ehAr+65JIaQdF6yxRSchoLVFFcNW5PX9lXGvRCKw==
!br2VwOFAz9/JpWY74Q7ZVXLBDk0/nHkacZQZD57drQ7qchIdXRQ1FjFY9TQYmRCHqGdl8C/hqeQB9PgBQNKGd66GUQc94UyyM8X7qZ+/K9fUshCNE+6Bx3JAQyrpE3DUGhVOmOTJgf3hjAyQEcQQOmzVWCzTqHyny+wEbjMOtWnM/Ej7KbZ4nDXduFLsOG9sm6vjA2vuG1vYHpw2yq0Khw==
!0pQyrIvqEiq2ASRAS+/BD+YGTOYxSAdXfXpnnrx3NWFuAQFQINLhonOacsstefOJL7LbX6Jrxv+xpCABZTkR92T/regv0dbU9XdZnphK7d4sO5TEuTuRPssrgvOJlP5nPyjKOxD3Nd18rxiV0DvhY2eIk4ElKL9hFsIsw8pdPK8oSQ9Xc1Z8EY9pHL2sOBuCpS3eL2RFXqpjMKlrTyyxLQ==
!aBObzLt+i5Ra6JmGWPTbTXVgumABLnYNchLEEQLPNPYGygfMt/KGpZr4nwtkuchm1Ez57qlB/ylATCvM+ntNCP3pK40T+PTJalD3RRCNQT4EE788xedJyHU8FAjNScHlmsiUFw9lijk/gSOW4b6fz5e0GqIP2EfNX/tTyobca81hm0Z7O/hoSSHHRVzanZQvH5BjEeJOi3VSeboL4SIdmA==
!5gIdQ8Zmo/G/oXViPM3eZ6euEHz+d61vsLdJSDMMgqgT4mqOwVrvtQMNCFjCaHI6EBsb6YD1PaRHHehb2O+u17RTmUtaETGOAgeFUTTzMlYP/pQk5RGl/Rz4dUM+FQBPfDay0ss949MH4zXAutZQZ2ha2KcKCUax9L4EVEW5QgdJ2BDDoLqqkuTf1JjY3FO2iHKLMZ/Aszgnfsy5mML1DQ==
!gQh/SghA8VImVb9sq+OGBbEW+oStGIOvGMxiiPwkwfOaj7N28WI/GnaRMcL21qU3bQx3QlAuSm4jHlf1pNE+BbUDzpAdXQx8Ir49IGvmyBd485x5r+f9Pzq0eouqMSwFoCnoOd25ksJsmqZV98tzov0aLeDlkFN3+TICbIWABcr4NhVDXnhXzO9it0IMnXfK/+gUL2jNWda1AYmCMnSmGQ==
!ur/hc/V5Ig6KdGrZNKu//v0CTsKRIqvVH7iqsBaALTIGGaEfFGuQyXAk5+m0Rf2hiFFLbDc0yMfX9dDpM2bQG9+Hg4DNWLAqiEJ8bw08kakXa2DFF13NSS1lIsZDKF0nnpm9gyNv3gM5MQxOzmuc4aM/Iy03CgAHN2z+PqDVruCiVGOAjApxhae3rjDW1YywOWZuQZhnHCDI7gur2V6sbA==
!+cflGysw1xdLp1H5izG4LpZKwaEMnT3Y+nygKnIeQt5+Or3XxpYU5z5YmvGLvY58iZifmJM2V8lw/s/lLKgc8NN0BXtjaGgM6YXghVPt9YZ7jooHeTxp6ZzxMkf+afafO5PhetynsWOwPXI9zdTqfw/avGdbDvyk9+4+LobypRZZ6L4rLlkwJnh+KcZC59JaM4y3QnYAualHfbrbedjGrQ==
!bb8kElxMqjX1JIp3e/w+WxIu648b063T/Yk293u2YaaiUAcBD26VjHoqWMvkjZlo35eGjEtqUs+kviIIVW1rUIhnnk3SR7odW1OAziyMbPK/haFPDRJc018sds71Li2nj+fY8fYJg4XltinaCEunoPdl10r7tWiLBv4XnkGKp0roBxj0GGYMJjZK4HJ4XvnGqsq16WhnP9vMLZG8SyRHYQ==
!A8gS9EA0QhJg+EM+iAIiqL6vX6HkLB+4uFzeZ265hROEe7RXwggI7alqdXgFs317iTykgYKp+3yEHGx+AEPCG/uA8MdVrvfjQs/bikGz9uVxUK0rQqqKP4i1xXwF//O0M+plTZyJ8enKhZ3vq/DVeSFnARe7V0TSCjdHE3VyAhhOKAI5dZMYSCez9B8fsgD3HI0uCqTwiT6eKWfhq1vO4w==
!eCOGMBiVKSRM6dn1HETiBjWcaEifa997a9zsaHRwPriJwuVlg7a9Y9Z7g93l7nBaAA+jHXp05sklb0igW4snLHtK+aeanX0lwCcZApkibBoLXTeHIfljwDrv/15hqQ2AmECpKrE4hb/QkE+Ul/WViAykkgBHE6JVNHcGs4GKucTSyiTc7KIciH9NMziwOkQoAVjoYLKbBXcFMM5vQRdSQQ==
!RI5hXZfFzFRc9U2F4wVCukaBNjFCB7O4oSh8MJRNqg+peyRQFjJWb/1y0PdwaIG2XdsUSjFhr12cWlFUq//pdgKUqRMFKuE/oRxsrOpl+q6tM8Knbq4ZwD7A2BNY5+wkMhxlN64pLYgz4HkPvl9GXsaPXxST4C0dRafZcwHFrft1zTGxzAOWiUIniimKOjRse8LfWEXveH4FMqTWK1uEzQ==
!00RkZd6UITC/KeAZvImpCrRXPS17ldCTnVmSsgTNuHuZUhoki9jZ7sTyMR5Xfs/aZkPd0bg/JLf+p8wl5QdYivKVa1g98TxEyDltAvdiCXq6l5et0lDrH6Hszd/13/n/g0VHu/PpKycRMtMhjMnjV2yzhyd/gHsafr2iQZhXwg3Rmp3UdA+u7670IzQGPyMsTwI4SN9jmcQqrMpOhtQd0w==
!jO5zSFuO+85fl16QO89Xtcz67rveqCI5KwpM5XgCjy6aCjj+1tozfHpujRPCfr5HpD3JzRcH16P3bQFBte4bz3MStaLZrs5PVCj0IOFGNIN/Bcm76UZamXms5oTsS+PBN/zGLTS/diri1SjPMyuqanECfX8mLanXEBOYvDiZKKMQXwCFagPNJYUYqOLpwOnB3c6JQXpgvdqfQq+9jz48Pg==
!Iu0Jbw7nTS+o9RORc5GoJeCKQiQwRcH2L2Wt/a3gDmhcbwcrswbf3wcmASgaEqaR7ei66wfDoraXPh8s6gcbD796B1NnUQwgQ1buxJj0qxXE+cYHi5XfVvbgWW1KWR7xBuM15ySb2VDWnqqQycbWww==
!yz3R2Qx6dbhxqwZeuI3pJhs5TaxxyRNBYVPDKsD7gAHEn+XTfzXi9xVNKgGjWmZ/4nEUL7Jmg3DC2B8nrHjXSqJZQm6IsE/AmolkhPEHriDwN4AxlHMuBg2YvhaYGfbFwFh5ydNTG/TLYHOOH3zFxQ==
!6h7lnHRw0BwLtp+hDCWUTYB9yXt5j3z7e1VQOlYu3wfnVrgMFhjo0Au2ytZYXfNtNqDex+lhBWXjMDBI5fEINeSm5rW/cMb5OHwOjVPlp46igNmEaoer7frkyg0gSMPtvp20VwwtCa0bCndQ9LQ1kg==
!/Adw/kgLk/7O58Ik62hFN8kelFewm2t8GrqsVy3r6VRg0EQIsifavWhlJ1rUCHpLN09ioPF9y+XK1CUmx1TOZx73MwvZ0DewvHTaqkjFNbualsUIbDEQOl7CXVySj+Sn5ZN6p9zEGyy++EVe50SH3g==
!hGuW9qFgsFpquTV+vL7PAkTPgpnzt8xwGVA4VC3oHXaPutUQHAy6nvqM6oyJHXcnI5iaFqh+Iox9ezJ2vUsZ08sL5lVVozlxbzqC/WoBEaqZa3hhi0HpxzPRdgYsIzPi+ZjXGd9xqYPji9lV9rc3qw==
!lRhLtF2zCYRQB219qxJz+gp6xvjkwSHekCPAh5zcyv6A0BXFdgKcWQncuwqiveZPQ5JHe2SW59bwYwZKAQ7kKUcIC0nNpITUwWfeZFcnm+V9MpLZHCeRqRrNolb201vO5XPwWDe1f1cRogE1Rs8PYg==
!I/h92BKXQS0qaUztUrYqBw6exa13dz3q4t4JD6d2zDb3/QYceN59za5LEGtSLLCQR7n+eMrwGRCODSp2E6lZGBlqfD7od9FNr5XsKfHxyG7lrD2dMPr56BT3ugZlI1rN+zdLNOlQgs/LKuyNrmqWxg==
!elJeQ51mOicg1p4BjSxBRxLjPR0LlYWDTgeGytjxsW4Q6qrOXE2bBnkr4RZKwhXXycsPc4/acfpt1KI+ppZafbtBEPYUw53HyG7Mmx37NthQ1y8XpDaePkyFfCCz/MeqsiIRrfQ2t+n0Or+Y0egmIw==
!pOAROk6OPOVFVX0RrQpbL5Til97My0l6ZBKSs3VAg26r0DaAuEex0a/pQ8w9i4b2Yuv+cA6a2DdNz006qOPwYmMP6Ml/d5Q8wlDNQPhSdHobCjbQpp7ndkoqeYaaGwMaGanyDaqARtu6I9R4Qn/8Uw==
!qdvvKqv/ou5I0A8eUosZe4f4WNBTVRVGxM7CTYxaW5BeE+RlYVViOp4tGkyh8vomcDU3Iuv/eXnnOakREVI4EQWUWY9xvBxiAeersae1axKvA2xoL2OnXvVZt2rGLP/MpE8KavgKhrueHZbWBosYcg==
!u8y4RbnfuPMFGLgq4E9uyGGwq4d8NXcafemG4cLCRe6J4v5uzE3C0pe3tqdSEdzGubOwfaZzlblTQguRMC72alzZ9xw/9mt8/bubly4IUevTLJBs2RPOJTp1lcEuaXS7oGDdDM8LuGHhUedbpqjZ3g==
!UMvrnKe4wf+BaBfGAFB9BYStINMWM4CpPRMEgHITwfzQx7eTsm6BEfvjGHQsngGoja0OHdkzY1m9P4vxP7jEh4zTIo6Jnkfjxue+sIUiWFRnmao/3ea1XVkQi+hbCUfC2eXf9KF3EYI1eMqKEURrhg==
!dMWgSOHNyWEMLEVhk5nEtRcvqzcj5Yhp0QkSnIO4h5Ji5haWOqJ6buvvH8Mt/nNHsks4P1wy+Tfe4yF5MJcsUOnLlfl528Q6TorT2y5wr5HI+kuvoAIBhGyiycIC3k6CAffzrO63w/T164P6/uKERQ==
!DMHa6A46DaCNYpnwCW2jQsvSBshvdKghNjNTH1JN4fSVpGBRhwR9d0Jy4mc8zCFHVoLheG2uwBlEziiiyVWrVqdaSHZtjhmRzMcvXp3k/koMVgc3fIoA2Tu59D702remcdS0I4epJp31pdWLv051fA==
!LaDDNBtwiY+3sfpC7eN0Os7BAPn9XCUBzj5q4K/eZE3CKDr8KFGgj+Zk3JWCHjmP6iHa9d63OUI8Cf0RKBHt88urEDUyYz42aMm4vJSuMwOukZG9jao9SjPV47wVsdeVfbOEBt5mogKezq9CIQ8BlA==
!o8PzdLLyLh4Qxwyvk+H9M9qwXHByyrvtnePg8dXnG42bnWdlJ0Vu0lZ1JEJVe9Cm7ZS0bI1bHRFDtoTD5OTf8UT/G0P4co0I1EaKB2ghIfvf5Tw/psz3+AkHqhvrJwZl5MO3ipl84iauguGo9oChsw==
!+GUMbc1bFx0yoFuSW0OLtiExpyZ7vsd2ep1Y9c7X3j7EVVJ08jiZaaMQEi25ZWLNGDYLtLoG66k6o4dD1hBjMQhxN3hxFeV9jzyhZ29j4kN9j1h2WGd3HoQE2zYK767wk6qvjN6q1Fr9Pnj27aMD0Q==
!EcnLqFiUU8WN5i5MJs4CZT12v6rQhUQlJt9tDOr2RGMaQsG6fgEMaXLUlO3eiNBPVhJjSoBgcagpACBg7wlaJdujSduefdwIEmayX82DZJ4e5iZ6HR4x8wG7q4OCxV+e4mZK7Kpcce3IAzQc3wjtBw==
!diUYdfbNERsI4uYNFn4fJ7HsUeWSnVPp+fc4i7cccA6u8UxlhwX+cEJfVqodjiuMA82xp+ttoJn9VsTdEb7FU16SQSbyYt2gdKO0u8mTohgWcaH2/stKOlohMunar0lNLyfi7WciYU+kJVlfisnb/A==
!ty5/gDaTjqjdhaMkiJF5V9bY2Ek9aidMny+vB4Vgquo6IA+0ZgcBKgmMh2qomLhqWOB3MLSRqQizlfVm+Y8/Sr40uX0OmGuR3fy/I3c2fBxXd7rwf6J0N8xJw8YLuaKFp4uzlyEWbGzKagJvWkP9qA==
!RqjodtZlmZ+DyiPIK1eeFJhbxYQc+7jemzADHwYHHd9KzecQagi1E6/GmOg1++5NJJQ9ibEhQ4IH4i7peS80oP3EXeRGq35RLV4cXylKh821FKNf48LyOZNhHCVLXUOkKsIUVxvfVZR0n4X8wJ/nIQ==
!frqDSLsq4XokWgOFjLl+L4NMDZZmSm8CudkLa+nrCLIsPC1/3GQ2r0glISd4DsjIW/FckLtmK7z/rEX7e1AWdZAPwH4STwdDEmYTgH7nNnU6Y7muo88DsVMYhUFfOBwSLM4rRpqswEtXNzhR9ErW4A==
!Wl1uhOAPSe7z5Ox1dQzN3Gz++BbukaQI7vcFjDuHrGuRH3qKyxSNDd0CowFeXcSsqIeAxwf4uY/mnkeM5IuCPm3PnT1tGtyToIQN3guKgM+tsMiaFNAinD6goynq9YEVncIWsShBCyINUNnWnfm6BQ==
!ucQ65oXeMIkgc91ruJrypBycFMty9zQeuPfcwKjf6kuaFUQu6nJGEKM74l6CD5TvFBuryny7dN3ARtaJEa7wv67Fh00QvHjUQxl2osxIWrjhqpVA8ZzxYlPyyUgMDkBbB/OCSK2RTvMJ5ZK8i+Ge6w==
!yI6pu+3nvs/0PVAlwZqr695OCnr3UXQkaMvKeASWN7S6uR6fgWzoZrT6McSoFF739EtF+iBvFFsrMhW8lmA96TTyWu2xxseOuU4oLOfi02MqwHgMto6QjzD+RsgBxcDuwsNoZw4h2LXuRKNsEn8hmQ==
!pvvfHJRaqpPFMDaEfT/yzW245AInkdGcgrAS1Wk75Xs/necyga5H6GxPszc6vNbD2N2MyWhtz9y+XL5079/qjWWwmHutnkOrL/f/FeGQ9quoFuflq2nxQARXNzjCdmFzSGlo7t2MnSj/VZdKrepEktpHbhHskCmHT+TPAHvAQRAhAbWhAooMkfNnkC7i6UfGOj3EUfPtCsxm9c2IOrI4B/jH7ckmUKVf6KUgOsrmOaWULuCHZ9QqY+Pht8+t47cyLxCZi4AiLw26tO9l+U1CK7IKE4HwElXOkehiPiV2wArU0cQS0LVuz4xX9o2+tlcbM7BaNo6KK1ASltt3Pe/fDhSYzsT9AKdRvsE0GJXwluA5a4lElHnpwyfGQOg94YKMsUecrUE0zTuqRgb8qFjDNFV4FR/SNHxsIa2VIF575WLsEXVGlz37wGwah5u+wSM/oC6SjSZTimrMl4Jw0Px0ePSU3hdeG/X5yCVZheVBhs/3nfyTKdDarrRLSEWKtyVnwSx86CDurIlu/hvD2a14c9bxmXvCOF44840dISXMowxNBG4yCGCgYeg9wOEq71o6SpM8JXv0Sp46/kakCvTTAG9v9hLPRxCkZkEaQAFKEei6Y1udvSj4YDhI+Rt7vI5arR0zkXlk1j3S1sHdgLyQrjRtbzubbHCXctWoVDiWER1F2sLhYpo9TNOBAobBiydK3foELRgREjL0xOsruw0Nbig+0B871fbayVTapkNJuyugeM/tUFxLoiyujYKCqPRY/CMhYM6QwiQn1uu2inMh8OFDu4pUPBAvecuqrmY/y/oHkgKcojRx65eQVwAc1w7LN09llrfDQI+8a6XUn5viPqYTRwMSjTAdmGt34ZNSUwvZAdBC6ahBMY3ZnfVTWkf4YQZnQfTflZiaD7g0rhCGUNbo47q8wJn5gw2RxCXWU0ncnw1z6+4XmGFxaNxCindZhxFSPauc0nSRojl29toU2o0guoH9LkyHH5D2eBmGzA6aeI06bu7EdSfPSfGHAisEv2DlLxzSmUIeU9hN/MwPGp3LC979nuobvx+PwHsni28=
!K/bp+mInopenB4K5N0w/Mb+WSOZ6t2NmQ1vm9j5ApTd/zzn3+/PaZ59Hbtf/qi/THROG1hZLjDpmouJmIb8QYG5kxuEmVYPACpgiV/3qJ7+w/OyW8s6FssJ9GZTNVsga0ULlGOhY84TmiYhpo+A2vH5tO1aK/0TreH5HFpKj/E331uODiiiviPwV7iiUd1at7pRzai8FR+zD2RgV0B+l5As1I5tVau5N2ZrnTg+baaw6Fs7n+QojbXb8bHSecFiY8hxYwp4HhS50odTpZ0Zucq1ZL7oL1JsUrUWAYeFnsS5HfSXdauJvcUM3nJIxNM3joPqi7LKLYFJ6OWDybrDb4reTLpe1Eef76YlS2X6ZUrDG7q4EtBNEfUUhD5ZgqD+cC/+PpcBLo0lnwTBvtiXHETucYFe9IVVFMReCzbDA6PscTm3fswSLYs9Qob4SUhDflv6ykY8e7mwS/njfan7+JdzhkaWjIsNz8hZ9/88gGalbFN1nSQn3hkD88t8jkIkblMeKnemG0ppZWnVY1f93XyiCSD7E/Y4VRE4/M6j+Pk8TFQXoYqg5dpQbtMom6vMuH2fPSgNQb7sVkyW0Oqzdj7Gdd7aQlWQ2DlvPrZIGeCib1MfTaItzLEXjDj+sisfvgQmL4jIVHfctUSUgE55KseEdds3MnJioPYoCqVotQVvgUHSJ+DUXBI6DGDp1fvuu8+7Kbiizxp6Kx8WpM20lZKPo7T6HQsGlkIPR7yHfQyHK+771geiRwBzc+Oojbyf6beLLpyqTciEe25mPPqnmiv8Aq3R/jwDfl0NdK/L7vWMWdbfVfwn4+2AwWt8+vBszBgfmvKe48gbazaE+NfNimnE0H9NLkPYA5wkzhJV5FEyefUU67PaoKhtBi23Js+nJ3Jy+OogTNWYexQat9qLlxDMIBLFX9QHi8Y87Wj+ohpZbYUQGFgxPXCcjR88nxsyukI9CMCL997gsAldmSwT1EtO+2rslIz/CRWioQZSR4GEOM8C5S+5IbKTUoJpB+rN4PQEdL9rYEOlJkspNauVxGAe/kXqvK9mHkKf/IR5X51c=
!dXSxnfnGB+ozadhi3w3vBjch/kIvWJNciMxWoxc+tdVFnjtuG4ahIaY9jyMjzCzaPA0C2EiuaBexACs1b/2ZOR9JnyC0OPvCZ0mVSqoGLwIPHsv04OMVZVk2y4nhKgP8pbXcrI3Pb6L7IIeKi1/WCGKtAhC48T7J04EmYx8EX7kKFEMfdcYJ0K4Jewtd7KY5wzkYjS8p6TR35X87Q4JMMbmyAM3lHdiZbF7Woe+jjy+RoPLdLOWdZQzX6+o53xL3pI5GABBm2Df6byZVYEWsjeGixfXkzvft791Wr+ETMiOPl3ow4Mx2QFN0idqdAZ+DoXEUNLyxgE5Bv3M3fhiKwBvs0hOHu2iVYYKiuMOQ+m5DKZOWxRCHEn6K2YXkEHWKNcggt/hBef0U1HFlWHCQE6rbTYG3x9dA+Jwp7XMKzrx0TE4Y6/+CY/2L0tqI0n6CgV5ZmJbustXmWxkgqd+Yp2Kzs04O8kjNOwxzegm0XRHF+GcyXF6SMyj1iYr/7gXwmMNVZ2siHecwCUSEs4hhgOhQrI2bd2UH3Mx5j+RDLW5pZ8Hcl09/k88mhtsIHqhU1UG2ie9TvK4Xyt8D3pl/OkDDTleB9iO7qKZFupTmk21S2xmwedlMINttoHDst0htzaArVmhHR5d4UZPAJSRSJWaiArnOmC0u14pT2EhmFlLffjIWEK5PNINppnUdIKBORgtAXBRNuvIO1Dxqpfh6aysrJ0eAPtLmMbO1xwyHi8umf62/C6VbTLel88bPhjNY46UpI19WmPw5/BQcFKDLASCc32Yv2W0n4BvOWl70O/pgYzaEiJ49gFmOcQkDWmKS5M+SAZim7gGOuylNiTd0M/4P4UZ60zF+8mwTATe88mD74AjqxYRJYjfgUq4AqMBRBZKdwKita+4HEl+CZzAm5hVDuJqVerT2MVu067lMs0g51CnFOgUMCYw/ChXp6sSEn693BNsXTUsa9lvTJnWRy04l6Ku9qpXoogDHvvP2m08Yos3lQl4D56YYTKvHYDmy6RmqSJvwRBtDLJGraICX3EJirTA2HQP0/1FWzkIn+RA=
!vBfAjj3MvuoVP63QZQr4zvdGKFbff9S98KkOhxi7w6JYhUQNj5fenvODe+0E925enb43JL72J3STxUcQq1GmOLmwP2mZ8bdtfQGf9A+il1cwDYeH9f87/NM+nF3bnbN4D7nHSN49FaMSIotlAiZKKPiw/iwsyRhg4+Z4aO/3pssyxvy8Ne8opfi9NsR/9a272cpKoASStm4Taq2ddjfzvoWoCCfWwjW0jmDFZT3EuqGXzHIQSPR22Kw3qEdgNiShSE2HsMOxJRRN5gMJkFUIoMDt6E37LIu5Q4b02lmccde3cnkYePXFFALTmdj2olHZ9pD3Fc2GyDW1vnmtp3otyAfIZuYuUB7C0SAGmVBzHrWlYpPztn+3VAR4jqQ+h5i4MzIlg2Xud6xNZeBkNNu8Jl4rGYeSAGgv8Li3FOXsYFvSvcBWJgq6eU6rgnehdZnhw2IACNv5xO/ji+go0FWWyqQ1mXSSo/e9yBZvQWMB/hAjjtMDww8P6sWK+Z/jRShSHIyiiZ6v2tEd16Rq8oPIIlDPLW1+jwzQAwf/CjBU6G4jookI49luNGbAfy8GdjZmX+v61ewv0Ml1THy5n9ATy3LjWX3QxAI1el+82GxDyh5IATlItF08++APInQaHgX20QjOl3Je/N0rQ+/I2vC/shWrz21pL0lVwdrq3keVlq2FPmbewqvXl5sWC+yx0GOjE7Ipr94dkfQQo/EFE+gPgwAUBMLEl9Gji/o8Ws+m/7Hqq3rAkaEMdDEkHGrtVSMSL+Dd3QBLtJvip83vWYIaLHqbKeeqwHO4QjkH5hmBg+k+9Sz7zxGX/J75aGQnLDt+Il+PrewxvY/AowDJX1rhcBvsz0dG980wcnZjIgV0Hfg3oqHzip6i/Maa35HcuYf/vPdPZI1gx968hBE2HQO36UoECIm/aaXhNqAZvaUh3JOfxAuW1ycrQRi2NfES2nGfPvuTgPQKGzN8cjLunfmV6x6eyDhrmFwXar22wtFac5BW3yXv8KEHU+GHvg5yNwF2w0ZtpdK71wbDGePNb/Y/YJO/WfSQyYAUjiF/S3whLlc=
!wiMjABBYWlG0TPsPzgjZQ53ZqUhofJR0keY5ibTBtaMRkvcJNV5YKvdsD+d/Z5nykzdDKqn9Ko5+uVQ1u/neKZ7/0o9h7Yz4dKFoLwfF1xEPKUWEu1LzdM2vyOczhwFbOYrAYhBaqrg1ESauFkjl2oIKTvhApK5jzPeLlJS9zcr2rowrX7e8/FR59opG4KJJLcCWaxRspmGoNScp+vAGtlsjESXavy2T/JF34S945zYN02CWlPZhMmHgHzRLoXmwMWlqG1kIMCTg4mgZ0zJYnhXte2RmGNzZ3v3X5Po2UQPvR/sa6AhKLk8YW0TKpS3uBO4eRy07z7vvHuxagPVHyFARgIJWiR+xzn/i5MAJ7E0UShNglRI1dAQSAW59URMvj8ekLeKeNmenp0rDGqDF4EUFLCUbyQ+7SWvShjNl4InKvSsJBfSeVq5yetY1d/fQ2XXAN3pLN9Ot9fTloUVsc6axhvSgvQ+oh7uDSM6/YR6sT0CsD68ZHoybAFfJm++CGSZeF17PIXelAWGoqv4Jpv+jOEpr3aK549SCeN+ahF9VgBln/be9U91qIPoz2tZ2cET3106g+rRjqFpcCYyTeemm1UhtfMvC/x4xwIQ43P4gHaFQvts0F9uYNKOPWx19dNpvm+U5rycXsLCzY3IC29039h6IsBJMdVpPSahw20WNweAsZ2I4pousoGI/chWwy/3PpELcRIZgUGn1m2yqGJpgYf9ajfifeBrjPWWRbdx23buTvqztBoa9OfQM4AbOKzhdHe45qQ29BMFr9pHY65b165cG/dr2wg8SR50tZ77CACtquAeWKCLJ/9bz4ooL0071Z7ajmWw5lQgxM4bl69HJzkM8M0wtc59St876I6rkwZd6iZiSsaDcj6P5dwhNibpo+VXWrh73BED1smtGPQM0zRhzg5J03N2HCBM3czS+KCJ2dkhiepqS5vH7mxeLk8W1JDsXBEU0PzUnhXAl2pPrIRxMNAY2XTZDGsXWb971hiullIFLaaBgeR1lIfZJPZqdM8M5JEfUkt2tsuHy9yM6Cml2yreZLx2tJHDyWN4=
!ENSQ34um/0JV2m72R6DXt9Zrt8hrUUvECHpYGoOhqJTQ206KzTwWVN8TQssVLU69/3ATD2bfHrREKuXD9e/NtpeuX58qQDHT5ld3C0r+e0UGbcfwhW1yePk4Pn8hMP9GetEVXeFe0OKRdvWRsc4cDdPDgSTxEiCc59atl7J81+ku0XZH/6f/F65u8K1iJhFRrJW5oe5MvEWezaZDD8qXO+Wg3gtmnV7uWcOC+0OaxnOGmdOxTOg8JNRhm4s+wEMyG6GmYNLG54W4lMXF8nsAW1mIthngoHnpRoM0CM6XuYrcJdVEiQbV7NWqQybjjSY5sDS8C6vK7T7PjYdLihbScGU05k3Tz+F5eSuTJuaTwUqfWeFF+soSbKKychComSyMivtLA8fJMxoLXyOMimtufBx9rKhynEMherz2h9nDe6IqtGelMQjH90CsBuyPT1IlkAQKnmxUKzUKoecM4JrOE+4oD6MZnUV2fS3hnqt2ke23tugqayOFUor1/U+65WDLHjopgmiXDwKtEwjiVVo0Ncj+MHCTtjrebtDZjSnYZJvYzpBVaA4OUpGO8heuUzfE+q2rkiNMd7A/i5Rl/pvEFUjbeUdPABwL0rhx+rZkXV9+UaKA14rczEFDvSVB5FkIH3+lPvFqD6TWUx8Q1hEbp+/765XVYoZz2BuG4tuDTqcuUienQ70iMAUuV+E+Sfc3K5DJ2kyU3WP8+NUQaK8CuUIXkJOfmJYATFL1Z2JFLkM10EyJvJgW422fCGh9ZvGUd2ofW3GvAmc8UIM9KqcIDnE75pTKo8VwoWuyOZb2HTweumpogUKkIztjZqvEDziNsmyJKr0vuzugwn0AxV7uTHsgztDTLsOgYXVRFrrPgGqAaL4jGrpAp7eSwJO0bXmdVB7PscW06W+6a73gGMTJb6YRz4ZRQqnlF4fHWw9ZYtXQHpbMP5rrBBNgwgdFpzDfS+POjIdVKzLN4vuCtzbnXDUd/ykc7TKSMeqbD4/VzJSx6THF/JKiZ4Fx6VHN5sUNwOXlXGtjvnkLpvTg5b+NcVF7bd8WttW/2oiyYex80u4=
!swGwDXx3NlqHebgmyqzZy8jpY6pu+lAXZGvT0qxXXKO+r/NCaFS0bo7GfZa4kTrGJTrkREVgoztREF4ffjbYWqe62rh2TlIg6BxH0pJYhD2CXXQfRsJ+gD6ULZIxkj1ogStFMRC+mhbmwm8SJFC4EVEE0Dm7Xax3QzLfNfK7EZ5A5N9zqSg/9qsAlrA4j+xEfI8UhivqWImfBPxbqQev9Ld42YzgQWhzdpO524QydL7ZclRfr5/6mol1m8bkLFtyuLtTEvNt2maN0oMTA2TBm4DlmuxeD/lkC5NTp0e9ayh7LFBy70z2Rrwaf06QPLQCdLQzaMOnliiAY7/zT6rxLOLoK3Qy7cJ47PgiOZj+WI0qP5B9Jeic0EzKvZ7jMdcOWcHITB2aeccv4497dbU9J2ZZXolqYS2UEpcGopvJxxJpUf1PmyNwiMhfLJWWmIwu1r601YggWz2Gl9LgFkDmfpeZIZeZj5G57WwNIVwxX9r5GhL3Ahl7oTJd9qniAYlnzztZ0Wm/Vk6qi5sNS8hzu4PCgeJ0RZMYNcwt4sXLGDZV5667iW1JtwHLMV18JARTPhFmrBZI+DxZAWLmnViqZngwtm2IPnafnPprytff5yo5g3OeVV22JjQBbYM9JFmarhHiE3KltgCNPGhUfxVSi1Z5zALNpCWqHrtNXW3k7Lt3t2ak2OL2qIDyMHe2KM0jwZ6ETZGXreXyEfZpfO8BhO5vvwd8u/gfdSDXUGLo+aJfFEHZt3ypzdtlOHDAlcUF8Hh9I0gst1S8EotU9xSuy3i2UQmg9Hfn037CukXLSzNkcNtUv+h3N3YUggfcyvGoRVxooN60CNTj4sX3cLmw8oYZnQOFSdY4dTarpfVofQlfiOK8UGlIBG/0F0jrsjATplpLxdtCYCXmA2sQpkrOj7oJYICZB75IYlfdRU5NRpL7c2rNQZ28f6Z67QKCspGapIwGNlM2PXhb9aSonplfK9UmHDy1r2xGkDjHhzNAwYNwlIjR2G4iJov5RfqN6y+bp5ZwKh8Stfu5klKYNK5OVLU/5kH/l2zj9Kjktiaojxw=
!Esdy1dzUXCcCNu9fm9poMC1hJGWowzzICkqKh+kmHxKByWiDyWPfqD9mwG8/hJ/bdfpcDPBUpzXUJpUdsbwqz58QGpUFkvpVC3x+ATnKamjJ1GWiTtCnTHgXIKoUTXUhAY6W7Wj2zKxEYBLoPm7jHsoNc4OGUQ5IEJagg13yC+so/U+SMeM8VcDY5FgmiXP5rd7hsFHUzmE3EWCf1dOoMq2JfTaLd+JHAeX+RshNSXBfEMmT7fcHmnCvTt9bxusHgn0KrzBwVp140Y9UMX+ASk6+LxYyVSTtP45BbhMyY3jILVsiHX85Mef3BdNN1ZhzIaBrysCYU6G9V1cGOwqQBIvnRcx95LG2p4m01Ow9xpZwrPpPAj1SJmrHDd8Oze6hJ1y1Wz470IcEobj7OCDIg4frObyTmSNxXjv/8Z21ZTL+jHUTo8OjyQMNCa0n38oUet/cSGYPw/71BO8+JasckOHBPWHQy50XTYC0/tg7PN8QFPIQxKm/GGx6cwgw5a7Fzuwuo8qM5f6rLM0TJpTpzGa+e9D/V1YAVJpW63gNN2daGqDfuNlhGChQGrftFIPJyLOvJNSz5iGDfc82ii9kPM6ulFQrVNSTqkrdVge7w0yJO4Z1ui39kTL/RzegeRTNSf1662c6JqJR35YA/FYtXZxc6D/BU1h6qyl43y8SbhO0x/xBjvrU3Kpri+tNN0bHwUZGfKBWf/06+XrDinj+/obS9eCoNN6eodO22L0Ja7xGdpvjN8ILzE6YLJobtWy0vQ0wDv50i0lCnuBad5ED0c+vbzGuo7UP02nR8Taf6EjWNLcaHAqJUMlfFH5N37Ih50d/MZa/RZveM1ENbsTp/x3QOnkdlGyOSdHD0+p2Nku97WFa923FtHWxA/DwFhJ8n1lVs8NdlZ47GKmym/bD6L940IZVIb/1MhxSp+Kv/WldoleZj3t2uEj9e5idSJ0Euiu46NkKpkDwwJwb8UUAZXypCtD0fb1ly4OSDj0qjb7Lg3JoY0nkwv5C6h/MGvw52nW/cofPRa06jEIN1rtd3eRBtA2cnak2Pv8mIJIiSk4=

===============================================================
