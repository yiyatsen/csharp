===============================================================
Reocrd Time :			<<2016/04/12 17:32:39>>
===============================================================
===============================================================
[Battery Pack Information]
===============================================================
Device Name :                 GV7253
Serial Number :               65535
Manufacture Date :            2107/15/31
Design Voltage :              72000 mV
Design Capacity :             53000 mAh
Remain Capacity :             39750 mAh
Full Charge Capacity :        53000 mAh
Cycle Counted :               0
===============================================================
[State Of Health]
===============================================================
Battery Health :              100 %
===============================================================
[Relative SOC]
===============================================================
Relative SOC :                75 %
===============================================================
[Recent Current]
===============================================================
Current :                     0 mA
===============================================================
[Pack Voltage]
===============================================================
Voltage :                     79205 mV
===============================================================
[Pack Temperature]
===============================================================
Battery Temperature :         25.83 ℃
===============================================================
[Usage Current Error Record]
===============================================================
===============================================================
[Cells Voltage]
===============================================================
Cell[01] Volt. :              3964 mV
Cell[02] Volt. :              3964 mV
Cell[03] Volt. :              3963 mV
Cell[04] Volt. :              3963 mV
Cell[05] Volt. :              3963 mV
Cell[06] Volt. :              3964 mV
Cell[07] Volt. :              3964 mV
Cell[08] Volt. :              3962 mV
Cell[09] Volt. :              3963 mV
Cell[10] Volt. :              3958 mV
Cell[11] Volt. :              3952 mV
Cell[12] Volt. :              3963 mV
Cell[13] Volt. :              3957 mV
Cell[14] Volt. :              3958 mV
Cell[15] Volt. :              3955 mV
Cell[16] Volt. :              3958 mV
Cell[17] Volt. :              3959 mV
Cell[18] Volt. :              3959 mV
Cell[19] Volt. :              3957 mV
Cell[20] Volt. :              3959 mV
Cell Voltage Delta :          12 mV
===============================================================
[Pack Detail Info.]
===============================================================
MCU Firmware Ver :            GV-72-53-V1.00-Rev01
Last Charging Time :          2000/00/00 00:00:00 0%
Last Discharging Time :       2000/00/00 00:00:00 0%
===============================================================











===============================================================
!gUTqXsY2sg5LpKpVXML2b2h1afVpkJCiCDuE0DxmK3wNSHTdR5r8LVeDIyX+iiyKJDrvBzpdICvPs1eTWQz9DvhKUQFRS1Z2pY+Z/WzpsdM=
!1+oa9mBOTIxgg9+TCKq0C0UmMUuw0jTFVYqc7KCDR5E=
!IBuxbnv669hebJibWkjkPKUMtcY1FI85FCkdjvZTrgTcfRFjV+uQ5HJR45hciljJ
!IBuxbnv669hebJibWkjkPGFBbuhawuo9OzHF7qR6NvHo2pNkCfJpxhFouz4uC3zhTybTx7cGBzqn7B3QbpaMsA==
!kc49klAa58swnbb0iz36d/Zh4OLkSQan/Jvb/AXXXrRx9yQ/Zsc9MypYjaQf1jXo3sYE2HRe36mmH+s9D9EywmXkliLzP08OTFW38BVSEkhVS/DITEd2Uzinod94sDrzPXOW/koLBZgSry3A2ZTuEg==
!VVuRvthiCJrl2pB/CXCl+pbc5o2KXgWb8W7b+9VoesHbpd1sjuipWc7n1USlWH3IGMyLO3kWRe8nDrHlZTrW9Qt4rALaHAoYI5xANtFUTCmY4zngMW85dmTFKNT9MQPM
!vDa+Z1lNOfsbGwCw86HAgueKU56b4bRJnDmFu/F14NzPqGs4IwyZKP1wuFIHxUNX+KyJbS467P6pdOakC9qpu4vOiib5U8esdVu2U2JBgYU=
!WyfrMQhMvv1a75lgC0DY82UTRb72C3K5L4UhHT2Ao7IPNbQ+gKzs0mEOp5+85ID2HMfCjG5LFSwgoGSc0fo4TKEuF2f9Lrm8uHFN59Gc+b5oaQsnahTFmZurdFVKGarEwIuR2ZL/DigJWKl5CffjJ//7CVLb3Ia9fQ6XHzl8W2Q=
!NwMt7I/4dnWVeASwiGs9cgPLVTg4VYKuTP+zOni3eEkz81xq4HStfH0WlDGpaSlFIotfvf6mIh5Qh4l1/fc7s2wrBxk4URplbh1g6jM9z8+2WBqvdEcn+2b1rKGJz5gxpYq1DGifc6+j8AurN2Ye8UOsV9idn1m8YUwOY1lbhrvDTeYG7i5Y9xW07gwBgwP0RsRIHr/6+7Bs8Omipr5IRw==
!oUVgE7X7Typb6Ccc/mG78easWSapPl/Fw4M9amPwGqHJ286sobk9n0ZZ3b3QviZydfYHTT00r4YWRQWGJEEV541VACT+0FTJtndFgOPNB47htV1oK2eaLa1N1C5ga00fnLpGwrDzE8wv9+MZpFNETMU91jBetSfJIR7VwI7rVcYGqDm/TAYCUuyQw8IXwdKBGkhygxiEGKsc4TzT8nuaAQ==
!e9rVL31wWzxwl8gn6Aa9QtL85t35udODHZnyepwH/Kw5ib0AvptbtN+mxUGVS0xUsVSvKt5KPmvfcOFpYUg2OR/7wd7/Vr0OcPiAXqOaYBT10FdxPUDY/ISqhSNcrqRc80euhnJ0g5pmQtoZ1/1oZneE3wVHCZByDdwLvGa9sSRlv9OfRj4UXfrOKX1Y0fCmgiZD50Sn3FMgNPlZEZJ90g==
!CsosDCowxC/DdFJjPF48YANjKxEWfDNGXO3zz2XHZf4pHa6LF8aU3xhGa03IDJlW9JHUCi83vlAPuo8kwitvyJWsezkH7cmChPReDCH7ZPNZSBnkD+9zitW/8R3QcQnKmPUM+TzCSC2Xv5Ne2lK+U4U/NKn6eZk9nysXzohe7m13ksfuG68oDVMIzIXHJix+Pc+I6r8RepRLlQkoLFrJGw==
!klAo9jDxj769VWxOnGoowWe6l8WJKQm12uFmnEQcWI1GJrrf4XKZhnWnqbEwMNKfQcy6rn78t8k2cYCqqnmaZ1cHT9r/hf4q7OGG2zahmx/uc+7ifkIqweT7OkX/son6rtzQNJMArJqJ8VLzw4tqHSYBmOdEkk57v8+DsH45aPSF16iQw6DcWSfAEgoPvPh90k2CEdwL3FzyRIjLlBushg==
!1TfMop+KZGLTZB5z59YDl5KTbKHbIWkyQjfBpMw8a/NMZwDaWhwr6Qo29RFKGq3dbBHlLGQMZFn/aepc1GH32di+3qN3R/vtMBNrEgwoIyWuSWF8tMcshfWM8iq58ydPuZwmLqgQZlshS3ytJYLOjt9w/986PDTfAMgs9qwDWKpbfF8lNpORPpSJqoTngMg+sdc+2Mmx0jaYdl9C8IXzwA==
!PCeFyVivCNIS0Ieq60YaQfnm3c3uGngnMohmERkzWPMftwyjEM51GqZU+pYIzkkIv7R48jVQF2V5fd7sDZVP+yvFj2VjswOZ6ftxOlboX76bbXR8MvDypTq2dTJ20ycAtV+V6aTbly3jMectn3uuDY8iaaMSLcL97NHrbBAANCi+MoR/b6/1LUXyRk4zXeobOA38+V3+2pcTZTNPcznnqA==
!K+dRJlvlLxEw1lCJTA4v5kDFiVCOLTsX4Akegyf+WTMrmEG1KhoaO3pfPcRkZDBGMTsWYlO3JVpzP3Y1ERab8VHLMqbvF52ADdBj4Cboc9hbHLqztVCJwje6Jjs5j1EsFWGvrEaVX0g+/6YDVZ3kdug6vjWX6EMHD7b02Y3ipyhcGiCd3lE3jaiDjdvo7ZWVOORJQzyB9AtqWCq5lkLsLQ==
!tMYwltLaJcCZ4k6BJ/EDI/mQBIAiaokEkwigjXmqlq0rLxB/H29S8NsI7ZP1Cm0110MhJqTFwgKUtmmHk7Av63onTOQAKZAyIYS3kgXcyaVqEamcn7K90ysBSnTz7YXFBHq8dJznT0QmUSn9+swKTfX18irA51Ue8J0e0nPnHbVMLF0t1CCOwjKBQZIxCcRmrK9N3LzsvwlAREzQoBg7xw==
!RbN1A2iapMxVcmXrY4peTBvIG3w//AyG/yfNFaCvk07lF4ZmFBHp22izTj/C94eYestq1tE9S7o3sanp+6KWS2Teggm3yaTQPDHE6Ue3PIzNKFrTHlm3BaUWCRdjlMX+sg9Y5dRzeMGukulmPL+OMGucvnpWGcfYmxNpBcQs/LS3n/8BNEQjurxc1zmNNwZm7a0URemTgm9A1CR9UrVUlA==
!pAMa+DYLf0Ssq/jPtb+CQ4f8RYjqxXxsybHCZ6Wbe0WnI4jqxjM5PtDLUIP6bgGs4WgpQbTZ8k735iTCfQvIcuNYYsdIy2BynfRLjEM911oOY0uI1xkeRa373GZ7p4FXMUnozCZEcpMkYrA7c1Fb2Qwdn6A/3+WZNNGx6uj/UehSByvd3aXln08qGEr8TrOc0H7K5XErQ+OD9Q7qb3EGDw==
!y4SegkxMQavL/qeQRCPGdvI9FIDzOndP0IvbpT+L2eoCPtX/V6iJDO/xJrit0hYmtwyggg2FdetOM19lB4H0xB15/9Uk0T2vrZuPQtq5fJWghHw2mkhBcVPm0oDB6OqDZxzLz/nEJzGzaPKoNf+ZH+PhPeUNjRsh1zsLvdXh03pNVDWtvDuLJ+o6AolymEbql27J0Ukby5A+ofT4bqD7cg==
!8nU0WJpiCkokr9vhqv7B+pbMzdbk+1DsPQnrN1Cee0VFW+Ppp40D3jrfX6y2/hacogdB9zd4SjsqjySkAkwzRnzfYcKUukq5HYe5ryrIJc6zH6/1h1mhWIWVdcLdgUBIKrwMfccMytNAePBpWB/mLQM8hWS8rEe83LqXOaO89IASiKtyEzNGbvXceFofBh+OW2XygMQCirDqLhC9rXqrdw==
!Xo62BS+O96zmM+jYxCnJTi+qHyRHwJ/PXkuzM5DMyhtfb0r9k0hzGQZEzWH1akD68d03YAnQ4x7nI4nZuBImUyPqpStiBan7hHMTc4ZuYyz+AeeQ/coFSyX4mygoYbGvkTeqKbD/QEnH27psEW6H6ItbE96zF0VXesX7KO9cm8n59YT4lLiV26ivNEQoFCr+8VkjN+6inT6At4Hard2fcA==
!WfDa7A3b003OKsOGFa10KaeeYOAitD5MZwyjURSX8F/rGjSXx9tfoZjg5UdENRIaSkXHrUlLOs6awmdRsH4ffC/dzGbqRT/7J8grCBHv1M+ihzSJUfreGKGn6bItzX7/+TK+5dffI+eyjE7rwWl+ZdUJT2x74YijjLdfN58EBqCjRQOLWK7eNcvljzzalHMFfZEOMG8HwJn6pkFv7SUgGQ==
!cyeetg/Vp0EYDor2hQ65mpIMPAWA5towh15bIroKKziDpW2uoCEP8hiKCKULzFd6BlXNslLaXhr4fFONL43Ht5tcU7S59ToQqSMVbfS+j/zITmyBP4GUAZY3YNMpmeAeCbtAvCocCtkk4zcBpJ7pwjIPEKKynpI9VdC+fyBsXDRcjvhw2C/K/iVurlccO+IAS5L20OUKaP6kgeu8WC7jZw==
!n4tvh5m0rLaZwL4GlEL0opst5CNYfhJ/cgObLh0TAOMEsMGsezaCIwao1HDbje/ryOnJ7tT3ajW/ZAVFOrKNhQ0CubsP9YIgVR5OzSsY7Ma4SbHFexJjYKHxDhbgIDjGSpLyqw8FBgaMyH/Bd1yNQP+unQItdvEPT2TZGjNmk1Rz/ygAzTYs+IVPZxI777BuVusQ1EUwfcFiVYgtZ8McPA==
!ptjfoPt1GzqYCqddTnmyHrizpjVl+bn3Fl6NzxaX71TAR6o0EW/UGrQlNugxq+60XCFvyMeFZssdN4wQ29WHV0ZY9fxG+g0ckJVFzibkMhqAVH5jYssyS9OM6W05aoiMpidP2+tl35ZWeT+m4aDcYHXFrmm5nFPVTZA4rQDdjCCk4WNPY9ZmhMgRCjRYlHzYiS+4m5jSVvKewGXf8XBTZQ==
!K3GDFkyxanMiQ3myr6+Fpmx5HCFHZ+QInHn5a714xf0hTVE5mM6qfRDxLOlIQIUzmjnmiWxVNP6OYUFT8JgdKYqtUx7kissu06DzuCxSAw+m59ga1hmHb+eJhzzc4sVrXS7DDBTHVt1uu9uBY/mkVe8aVfQEwY/4zSgeWWmgWWXLgiUrS9YXjPlIBwixTP7TL1kEYMFEBqUeOBP3iFmqlg==
!iGGLMffjzVPqzz8xZ8tLBifTcOGO28khgEnail9BPwXd0SiwFTLwdORzREp5LI/M3yqVZzakgGJw5tQpkC3n43r5D/o+XgW7yAxSyB6h30QM1EJ9bSrVK/DDr/U/ohuHSbcU1zQkPprBQscKuaNYnbeutpf00HwlkXrVuIepdbz0f3c90EwTA2KMEpuQ5ab5j+ekyvaOdF4ZV3r+8lXPjA==
!9pao5F9QnXCEotHgFB4HXv5sEGFVcg/uBKtRwZVhnDP7leIEHKEujSpaqeChCBiNXU66/cEctE7tJiFOjSLfouJA0muvzDtCVvLR7v6QzWYZFQ6uwIWTK6hRIfE59vmo23I5SaLgqGjH9HLCvRVbDdOcPWEbr8sp9QdTz39C467X4rvhIEOan3x3N7SVhmrinRLaLfo1hWZbx4sWr7hhPw==
!UZupdEzv491XgIOVMW+G89lE0KbCDVnDaIZHIAdilCNfM8u1tm7xYeER8TIgMKh0+EemeNtFNRzfGLXKnbhtwq+bayWxDCb77AITw+6IwEeYnZQVDVuJQ48d8TFnuvn9nbpnCObtYBzadndiyzHdiPfEtrS+EOiZW/JjhNFdyPW8SpD5+/pJHsRH+vtA7zdC0lz/uEooNwKR2nzJla49zQ==
!UyiVm5r7LrfKqarG+Dc+xmxwVSu89AjQiwknEPXP8FeQxmOnduiRDZ835UIXyPh07a6ju9NKl/+NVWa+k0Xtp9IQ6TFDnUQQRHYZso+PFugUCq9azIkjrQ262ipJD4aWQ2XFir+mpAAHwz0jtJSMvUN/sxflA+FbvBzjgFE5BPwvu6gM3YqeoAwDI2UNwypCK0frsdqq0Jar75eAfGQ0Fg==
!q8N6C4DiWXtDOW/RwnwNrioUrEFR+ZsAUUQEMdSQMc0Y+3sTZyXbiMHcgKIcAjgaHUMdw9AExOAY84tRTxMzdTYaIrmqoqvmcZlLgO3l34japoM3rcPSXJ+EeZpaYWxwBUk36Y9noOiZpFkJSp93jf4D+y13R2ZrCR8FV5W12PYgX4FsIVrgwsJLP12nR8K6RlMG8i7rSNzjz2YiFup3Rg==
!H6Ky7EJiDM7GU3PDD8V3V1kA2j8eAbnxi6HrKB7tA02n2y93h5kMm7WXKI89Qtk2cfImY1zppfdD//CZEmFnzVRr6wT/taDFE6LOQSfed8qUipnCxDv/mIIqvLxYdxwN4kQoUm7020W42D6hSdi8sfGpZlbE04rAO5MQJLDY0x7oAoUeZywLwqJugwC09ikfwfwbdD4Z31FWjmmh32ga6Q==
!2JuQtcbrYd8xUIhuyxcgkpH0lZ9gEMTlBe+WDI2OQQ1lKuoVUZo16US9k+v9e6LPDMZTd27/J8IKzALVTZwD+AfGNaK/Vc7i8sUeIGU2Z0aoAzv4I3s873uTQmOcrOy3+egYnFwNAj2L4yfoBYS/d8gPevBwhg7Ta86XLD/Jp8ObrhUR0FNE9c9ue/KrQlistjlSs1QLyYC/lw4By0If5w==
!eKu4Oz02qlYBUhUCkvN+jNG+hjB1+Mn68SDPbQFaXyrUIJRwzQus+Fiou/4CxcEVmF9GHAHNfNg1JcirahLmlDx/tGocJm6swDt13x1ZjLYxXWFd8IHTP43NF4K/tyI1U3v8SHY8gI/4movQmhlTUTj57CMIJiZWKrAjKgFoaIPRmM5R8nxdoOVb8z3bQCD6N+AH/mknVucU0iAY9kebaA==
!F1uqy+nvMH6ctxjR/CO2lEUuntFbUAGdzCUnj6aUoYhVjMJ+4VBtJG2FFaULnQC41PGmkTZyBRZEDxsGDO4Y1LcjCQm9wKoilaXxtfaVQnQDc3xkwt5FROWRvvw0jCaVXqoV3FLOvC3Npq2zYfteub6i9G/0+011L1MbETTKUF/BMq0W0Z68Tk1sLhYp3ViUgH3T2FLu375tSmHYE7xZZw==
!JAlTVQTfDRDua9zwD/V7mW9bImHhWNlL4GdjASg9GZfC5cghxafsnzIzTXFPXMfwLceDAgsND0hwgguU7hRQLOrA6aAfsr8+tR18ZDN/MLcuL6usSaf7fsO2im60Cf6tWbiLpY5ewLqs+kjMoUkuApDA20CwTLqxDh9p1S+IAago+iiKrVXoHxaDJcWkE4vaMz7oWWK1JXY8QH27HbhWRQ==
!1T4sEMHmdat2kra5EbrFJ6SY6FZLUhjxRxeuRMRFw+qZupq0n4hbFX8AUaWbOC5/wtIA6TA3s3UYFAP/XoNoK8kcgirpKjs7BFbEj2oUoFxC7kFY+hotvZmxSLt9dZAhtIGzb3uIhcy8hGk+mGN+rZRvB+lJlAY6ZOTKYekFDXt6X0JurcxBjH5nmBItYQfZ1mVI3D8LWTryfh9wzP71/Q==
!as1Oi/WLzWSrbj19+jJOBzb1RpVrSAqhLoig7s/0bdtUardpG1VBqqrP+CrFvmle3xjbN7YXqasroQOudBwczmNifAFEJxeA8KQKgJyv0C5UXorwBhSGPFU4Of4/ifHmPb20R4fNpA2dVUICyqeqYk+qAVMXqtGeodDz0nfE5f6KT7VQV8HG5cjP7IDBtBqCdV06FLJx4t19iUaE+pAlgA==
!tSRPbiAXDKvKYuqIDl9E+UmNoHh5yaIIgePPDPeUMTMxsxnUPjmcGO1OvWa09iILC2KKgezrQT6bsVB6Bkl72myEuLCrADzWGba4ecAb06O2zZ5trsNYBYOTBPzyijDENJm/K3NZGKjbGHP01J3gHQCNYy2Juay6VvKE9V4zBME7SJHMFQThbP1T6Rzc59z0Kd/5W83TOoTL6QsTm/3YYA==
!TSIWBLM+5r2LmdR/zNUmg8OZKnkW+upfDt0fQ2ppiy5wIF9X3xGPG6OKG+aH1dyKIx41tsGnFWUzwJpTjkaym92ft8mUK4oLkEhhRrpIqitQQ4ROLErb/I8Ya8dxtiAgU6zbE81nyoMOabhiZQ65z4zcfI8ksiohrPykgvh6vgaUJt/eyEb3h35wwMXlDKt/nUu9GXQ9xIpvhqQCxXCPkg==
!IPyIRdBhJRVIlW0QN5ZIjS8sS2sg4YAI4gj+DWJkGB7TKFcKQ/QU6z00sZTsQtkB12HCQQsh6uVGuzK8r7JR6Iih/HTjEcJNOfALC0/5I+zwwRGDlh4jcqmoD1YFAZqMeOWTUYKrKup43RDuHgnozvC8zc6cmyDEOIzJGQulUt0OZxVQOxOUyzIIz5HgyNn80WY9tSiRlGFhye1542bxOw==
!PG87+FPgP58u4ssWJr85lTqufvKkLpdVppSjacfSI+jwlyyvQpXEQiYVTqnUWNhOKN3T/9qpigC51Sdpihmoncs+gVj2/smuWA+P/JEOCekizjwoBX+xO+3CDRNvbojnvMFyT/QYtFuWXV/N/JLlNSfKlkvzuOU+PbZrGO6/dbDU09xvkgFoq2ByVnpVH/48Q8PaUX62yoO3ahR2DBtkgw==
!GXD7qsrD5rToz3am0GQoc/72ovZ1o5+7mtz/8RRiRK34UuPArjpgsAdOZ1OqDbv3RepN6cAkugNqe4g6DSkafy8nQoAUH1kOJph33KVtGNqYmZWhw0c/34kYeftT2tX18VCPUYq8+aEt/lPJYOTej9tT97w8TQTvuV04CnLuEPeG7gzKgoT7hIOgHpyaZjPnwpvPigfn/4ZT62iIxGXOwg==
!PZ5WOTAbkWO0nA5q65HGLhXkOld3DLoevv0sGhoQ4yklZvUUW2gyRNarBO+/+Xxb7D7LB7uNqWyhRDO+PpI8th2JUpXu5/Lgt5Bngg0JWoj1DYJtOUI9oZ0ourPFs58ESMaDcww1YvM8LX5fkECh657aRm40AJXCFdFi26rWmlZbY+ehAr+65JIaQdF6yxRSchoLVFFcNW5PX9lXGvRCKw==
!br2VwOFAz9/JpWY74Q7ZVXLBDk0/nHkacZQZD57drQ7qchIdXRQ1FjFY9TQYmRCHqGdl8C/hqeQB9PgBQNKGd66GUQc94UyyM8X7qZ+/K9fUshCNE+6Bx3JAQyrpE3DUGhVOmOTJgf3hjAyQEcQQOmzVWCzTqHyny+wEbjMOtWnM/Ej7KbZ4nDXduFLsOG9sm6vjA2vuG1vYHpw2yq0Khw==
!0pQyrIvqEiq2ASRAS+/BD+YGTOYxSAdXfXpnnrx3NWFuAQFQINLhonOacsstefOJL7LbX6Jrxv+xpCABZTkR92T/regv0dbU9XdZnphK7d4sO5TEuTuRPssrgvOJlP5nPyjKOxD3Nd18rxiV0DvhY2eIk4ElKL9hFsIsw8pdPK8oSQ9Xc1Z8EY9pHL2sOBuCpS3eL2RFXqpjMKlrTyyxLQ==
!aBObzLt+i5Ra6JmGWPTbTXVgumABLnYNchLEEQLPNPYGygfMt/KGpZr4nwtkuchm1Ez57qlB/ylATCvM+ntNCP3pK40T+PTJalD3RRCNQT4EE788xedJyHU8FAjNScHlmsiUFw9lijk/gSOW4b6fz5e0GqIP2EfNX/tTyobca81hm0Z7O/hoSSHHRVzanZQvH5BjEeJOi3VSeboL4SIdmA==
!5gIdQ8Zmo/G/oXViPM3eZ6euEHz+d61vsLdJSDMMgqgT4mqOwVrvtQMNCFjCaHI6EBsb6YD1PaRHHehb2O+u17RTmUtaETGOAgeFUTTzMlYP/pQk5RGl/Rz4dUM+FQBPfDay0ss949MH4zXAutZQZ2ha2KcKCUax9L4EVEW5QgdJ2BDDoLqqkuTf1JjY3FO2iHKLMZ/Aszgnfsy5mML1DQ==
!gQh/SghA8VImVb9sq+OGBbEW+oStGIOvGMxiiPwkwfOaj7N28WI/GnaRMcL21qU3bQx3QlAuSm4jHlf1pNE+BbUDzpAdXQx8Ir49IGvmyBd485x5r+f9Pzq0eouqMSwFoCnoOd25ksJsmqZV98tzov0aLeDlkFN3+TICbIWABcr4NhVDXnhXzO9it0IMnXfK/+gUL2jNWda1AYmCMnSmGQ==
!ur/hc/V5Ig6KdGrZNKu//v0CTsKRIqvVH7iqsBaALTIGGaEfFGuQyXAk5+m0Rf2hiFFLbDc0yMfX9dDpM2bQG9+Hg4DNWLAqiEJ8bw08kakXa2DFF13NSS1lIsZDKF0nnpm9gyNv3gM5MQxOzmuc4aM/Iy03CgAHN2z+PqDVruCiVGOAjApxhae3rjDW1YywOWZuQZhnHCDI7gur2V6sbA==
!+cflGysw1xdLp1H5izG4LpZKwaEMnT3Y+nygKnIeQt5+Or3XxpYU5z5YmvGLvY58iZifmJM2V8lw/s/lLKgc8NN0BXtjaGgM6YXghVPt9YZ7jooHeTxp6ZzxMkf+afafO5PhetynsWOwPXI9zdTqfw/avGdbDvyk9+4+LobypRZZ6L4rLlkwJnh+KcZC59JaM4y3QnYAualHfbrbedjGrQ==
!bb8kElxMqjX1JIp3e/w+WxIu648b063T/Yk293u2YaaiUAcBD26VjHoqWMvkjZlo35eGjEtqUs+kviIIVW1rUIhnnk3SR7odW1OAziyMbPK/haFPDRJc018sds71Li2nj+fY8fYJg4XltinaCEunoPdl10r7tWiLBv4XnkGKp0roBxj0GGYMJjZK4HJ4XvnGqsq16WhnP9vMLZG8SyRHYQ==
!A8gS9EA0QhJg+EM+iAIiqL6vX6HkLB+4uFzeZ265hROEe7RXwggI7alqdXgFs317iTykgYKp+3yEHGx+AEPCG/uA8MdVrvfjQs/bikGz9uVxUK0rQqqKP4i1xXwF//O0M+plTZyJ8enKhZ3vq/DVeSFnARe7V0TSCjdHE3VyAhhOKAI5dZMYSCez9B8fsgD3HI0uCqTwiT6eKWfhq1vO4w==
!eCOGMBiVKSRM6dn1HETiBjWcaEifa997a9zsaHRwPriJwuVlg7a9Y9Z7g93l7nBaAA+jHXp05sklb0igW4snLHtK+aeanX0lwCcZApkibBoLXTeHIfljwDrv/15hqQ2AmECpKrE4hb/QkE+Ul/WViAykkgBHE6JVNHcGs4GKucTSyiTc7KIciH9NMziwOkQoAVjoYLKbBXcFMM5vQRdSQQ==
!RI5hXZfFzFRc9U2F4wVCukaBNjFCB7O4oSh8MJRNqg+peyRQFjJWb/1y0PdwaIG2XdsUSjFhr12cWlFUq//pdgKUqRMFKuE/oRxsrOpl+q6tM8Knbq4ZwD7A2BNY5+wkMhxlN64pLYgz4HkPvl9GXsaPXxST4C0dRafZcwHFrft1zTGxzAOWiUIniimKOjRse8LfWEXveH4FMqTWK1uEzQ==
!00RkZd6UITC/KeAZvImpCrRXPS17ldCTnVmSsgTNuHuZUhoki9jZ7sTyMR5Xfs/aZkPd0bg/JLf+p8wl5QdYivKVa1g98TxEyDltAvdiCXq6l5et0lDrH6Hszd/13/n/g0VHu/PpKycRMtMhjMnjV2yzhyd/gHsafr2iQZhXwg3Rmp3UdA+u7670IzQGPyMsTwI4SN9jmcQqrMpOhtQd0w==
!jO5zSFuO+85fl16QO89Xtcz67rveqCI5KwpM5XgCjy6aCjj+1tozfHpujRPCfr5HpD3JzRcH16P3bQFBte4bz3MStaLZrs5PVCj0IOFGNIN/Bcm76UZamXms5oTsS+PBN/zGLTS/diri1SjPMyuqanECfX8mLanXEBOYvDiZKKMQXwCFagPNJYUYqOLpwOnB3c6JQXpgvdqfQq+9jz48Pg==
!pvvfHJRaqpPFMDaEfT/yzQoPYqTktOdDldo3LmvgtGQ=
!K/bp+mInopenB4K5N0w/MUWjiIIDx0WD5bL+NJ9sW2o=
!dXSxnfnGB+ozadhi3w3vBjI1AyiOvEENHC5IxLrLAyE=
!vBfAjj3MvuoVP63QZQr4ztvnPnxeokkPWK14wQrN6gs=
!wiMjABBYWlG0TPsPzgjZQxRWmQ0J0GBst1j9IMpzMTQ=
!ENSQ34um/0JV2m72R6DXt4uoo+H4R0RHONIozqpHhlg=
!swGwDXx3NlqHebgmyqzZywv7KEqVdt1+jMwIQOt9yjY=
!Esdy1dzUXCcCNu9fm9poMGrzvsbJPVKOKDLPRoNZDcE=

===============================================================
