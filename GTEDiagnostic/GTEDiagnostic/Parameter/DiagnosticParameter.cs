﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEDiagnostic.Parameter
{
    public class DiagnosticParameter
    {
        public const string AUTHOR = "HYChen";
        public const string APPSETTINGFILEPATH = "AppSetting.ap";

        public const string APPCONTENT = @"Content.jpg";
        public const string APPLOGO = @"Logo.png";
        public const string APPIMAGEPATH = @"Image\";
        public const string APPDEVICELOGPATH = @"DeviceLog\";
        public const string MESSAGELOGPATH = @"MessageLog\";
        public const int SCANPORTTIME = 5;
        public const int MINIMUMUPDATEDEVICETIME = 1;
        public const string CMD_ENGMODE = "ENG";
    }
}
