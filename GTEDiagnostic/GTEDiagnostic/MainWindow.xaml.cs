﻿using GTE.Message;
using GTEBatteryLib;
using GTEDiagnostic.Parameter;
using GTEDiagnosticConfig.Config;
using GTEDiagnosticConfig.InfoModel;
using GTEDiagnosticConfig.Setting;
using Newtonsoft.Json;
using PiPiWPFControl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEDiagnostic
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        public static int IsENGMode = 2;//0: NORMAL, 1:ENG MODE, 2:FOR QA
        public static CustomCode CCode = CustomCode.GOTECH; // 0: GOTECH, 1:GOVECS

        private const double MAXCELLVOLTAGE = 5.0;
        private const double METERBASEVALUE = -225.0;

        #region WIN 32
        [DllImport("user32.dll")]
        private extern static int SetWindowLong(IntPtr hwnd, int index, int value);
        [DllImport("user32.dll")]
        private extern static int GetWindowLong(IntPtr hwnd, int index);

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x00080000;

        void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            WindowInteropHelper wih = new WindowInteropHelper(this);
            int style = GetWindowLong(wih.Handle, GWL_STYLE);
            SetWindowLong(wih.Handle, GWL_STYLE, style & ~WS_SYSMENU);
        }
        #endregion

        #region Property
        private static AppSetting mAppSetting { get; set; }// 預設之連線設定及預設之電池屬性值
        public static AppSetting AppSetting { get { return mAppSetting; } }
        public static bool IsAutoSaveLog = false;

        public static string mENGKey = DiagnosticParameter.AUTHOR;
        private int mENGKeyLength = 5;
        private BatteryAPI mBattery = new BatteryAPI();
        private bool mEnableAppClose = false; // 是否可以關閉視窗程式
        private bool mIsConnectToDevice = false; //判斷是否已經連線到裝置
        private bool mEnableUpdateDeviceInformation = false;

        private int mLogoClickTime = 0;

        private string mFirmwarePath = null;

        private static string mAppPath; //視窗程式之路徑
        private DiagnosticDeviceAdapter mDeviceAdapter;
        private Thread mUpdateDeviceThread; //更新電池之線程
        private BatteryAttributes mBatteryAttrInfo; //電池目前屬性之資訊
        private int mRecordVoltageNumber = 0;
        private MainWindowViewModel mainWindowViewModel; //ViewModel

        private string mMessageFileName = null;
        private string mMessagePath = null;
        private string mMessageData = null;

        public delegate void MessageHandler(object sender, CallbackInfo e);
        #endregion

        public MainWindow()
        {
            ReferenceOnly();

            InitializeComponent();

            ResetLogo();
            ResetCPUID();

            InitializeAppSetting();

            InitializeBattery();

            InitializeUI();
        }

        private void ReferenceOnly()
        {
            if (IsENGMode < 1)
            {
                string text =
                                        "This Application of Information is provided for REFERENCE ONLY, " +
                                        "     does NOT influence to any product real status or warrenty;\r\n" +
                                        "        If agree please press YES, if disagree please press NO.";
                MessageBoxResult result = MessageBox.Show(text, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                {
                    Application.Current.Shutdown();
                }
            }
        }

        private void ResetCPUID()
        {
            ManagementClass mc = new ManagementClass("win32_processor");
            ManagementObjectCollection moc = mc.GetInstances();

            foreach (ManagementObject mo in moc)
            {
                mENGKey = mo.Properties["processorID"].Value.ToString();
                break;
            }

            if (mENGKey != null && mENGKey.Length > mENGKeyLength)
            {
                string start = mENGKey.Substring(mENGKey.Length - mENGKeyLength, mENGKeyLength);
                string end = mENGKey.Substring(0, 2);
                mENGKey = start + end;
            }
            else
            {
                mENGKey = DiagnosticParameter.AUTHOR;
            }
        }

        #region UI Method
        /// <summary>
        /// 初始化UI數值
        /// </summary>
        private void InitializeUI()
        {
            SourceInitialized += MainWindow_SourceInitialized;

            mainWindowViewModel = new MainWindowViewModel();
            this.DataContext = mainWindowViewModel;

            mainWindowViewModel.VoltageAngleValue = Convert.ToInt32(METERBASEVALUE);

            mainWindowViewModel.CurrentAngleValue = Convert.ToInt32(METERBASEVALUE);

            

            ResetENGMode();
        }

        private void ResetLogo()
        {
            if (CCode != CustomCode.GOTECH)
            {
                string path = System.AppDomain.CurrentDomain.BaseDirectory + DiagnosticParameter.APPIMAGEPATH;
                if (File.Exists(path + DiagnosticParameter.APPLOGO))
                {
                    BitmapImage logo = new BitmapImage();
                    logo.BeginInit();
                    logo.UriSource = new Uri(path + DiagnosticParameter.APPLOGO);
                    logo.EndInit();
                    logoImage.Source = logo;
                }
            }
        }
        private void ResetENGMode()
        {
            Version CurrentVersion = Assembly.GetExecutingAssembly().GetName().Version;
            string ver = CurrentVersion.ToString();
            if (MainWindow.IsENGMode > 0)
            {
                isAutoLogS.Visibility = System.Windows.Visibility.Visible;
                isAutoLogText.Visibility = System.Windows.Visibility.Visible;
                isAutoLogValue.Visibility = System.Windows.Visibility.Visible;

                rtcCountedText.Visibility = System.Windows.Visibility.Visible;
                rtcResetButton.Visibility = System.Windows.Visibility.Visible;

                if (IsENGMode > 1)
                {
                    versionValue.Content = ver + " (For QA)";
                }
                else
                {
                    versionValue.Content = ver + " (ENG Mode)";
                }

                userRecordButton.IsMouseStop = !mIsConnectToDevice;
            }
            else
            {
                refreshTimeStatusText.Content = "Refresh Time:";
                versionValue.Content = ver;// +" (Beta)";

                userRecordButton.IsMouseStop = true;
            }

            if (mDeviceAdapter != null)
            {
                mDeviceAdapter.SetEngCode(IsENGMode);
            }
        }
        #region Refresh UI Method
        /// <summary>
        /// 設定Port至StatusBar
        /// </summary>
        /// <param name="Port"></param>
        private void SetPortToStatusBar(string Port)
        {
            portStatusValue.Content = Port;
        }
        /// <summary>
        /// 設定BaudRate至StatusBar
        /// </summary>
        /// <param name="baudRate"></param>
        private void SetBaudRateToStatusBar(string baudRate)
        {
            baudRateStatusValue.Content = baudRate;
        }
        private void SetRealRefreshTimeToStatusBar(double sec)
        {
            refreshTimeStatusValue.Content = string.Format("{0:0.00}", sec);
        }
        private void SetAutoSaveLogToStatusBar(bool isAutoSaveLog)
        {
            if (isAutoSaveLog)
            {
                isAutoLogValue.Content = "YES";
            }
            else
            {
                isAutoLogValue.Content = "NO";
            }
        }
        private void SetConnectStatusToStatusBar(string status)
        {
            statusValue.Content = status;
        }

        private void SwitchTabButton(bool isStart)
        {
            battSOCButton.IsMouseStop = !isStart;
            if (IsENGMode > 0)
            {
                userRecordButton.IsMouseStop = !isStart;
                if (IsENGMode > 1)
                {
                    firmwareButton.IsMouseStop = !isStart;
                }
            }
            else
            {
                userRecordButton.IsMouseStop = true;
                firmwareButton.IsMouseStop = true;
            }
            saveLogButton.IsMouseStop = !isStart;
        }

        private void RefreshSetting()
        {
            SetPortToStatusBar(mAppSetting.Port);
            SetBaudRateToStatusBar(mAppSetting.BaudRate.ToString());
            SetRealRefreshTimeToStatusBar(0);
            SetAutoSaveLogToStatusBar(IsAutoSaveLog);
            ResizeWindow();

            ResetENGMode();
        }
        private void ResizeWindow()
        {
            this.Width = mAppSetting.ResolutionWidth;
            this.Height = mAppSetting.ResolutionHeight;

            int resize = 64;
            if (mAppSetting.ResolutionWidth >= 1024)
            {
                resize = 128;

                ResizeMeterPath(voltagePath, new Thickness(82, 72, 36, 72), 50, 10);
                ResizeMeterPath(currentPath, new Thickness(82, 72, 36, 72), 50, 10);
                ResizeAbnormalStatus(12);

                sohProgressBar.Height = 127;
                sohProgressBar.Width = 52;
                Canvas.SetLeft(sohProgressBar, 78);
                Canvas.SetTop(sohProgressBar, 13);

                socProgressBar.Height = 115;
                socProgressBar.Width = 80;
                Canvas.SetLeft(socProgressBar, 44);
                Canvas.SetTop(socProgressBar, 25);

                tempProgressBar.Height = 315;
                tempProgressBar.Width = 25;
                Canvas.SetLeft(tempProgressBar, 52);
                Canvas.SetTop(tempProgressBar, 35);
            }
            else
            {
                resize = 96;

                ResizeMeterPath(voltagePath, new Thickness(20, 0, 0, 0), 25, 5);
                ResizeMeterPath(currentPath, new Thickness(20, 0, 0, 0), 25, 5);
                ResizeAbnormalStatus(10);

                sohProgressBar.Height = 96;
                sohProgressBar.Width = 42;
                Canvas.SetLeft(sohProgressBar, 60);
                Canvas.SetTop(sohProgressBar, 10);

                socProgressBar.Height = 90;
                socProgressBar.Width = 61;
                Canvas.SetLeft(socProgressBar, 35);
                Canvas.SetTop(socProgressBar, 18);

                tempProgressBar.Height = 245;
                tempProgressBar.Width = 19;
                Canvas.SetLeft(tempProgressBar, 41);
                Canvas.SetTop(tempProgressBar, 23);
            }

            ResizeImageButton(diagnosticButton, resize);
            ResizeImageButton(battSOCButton, resize);
            ResizeImageButton(userRecordButton, resize);
            ResizeImageButton(saveLogButton, resize);
            ResizeImageButton(firmwareButton, resize);
            ResizeImageButton(settingButton, resize);
            ResizeImageButton(closeButton, resize);

            if (batterySocGrid.Children.Count > 0)
            {
                foreach (object item in batterySocGrid.Children)
                {
                    ResizeCellProgressBar(((CellProgressBar)item));
                }
            }
        }

        private CellProgressBar ResizeCellProgressBar(CellProgressBar item)
        {
            if (mAppSetting.ResolutionWidth >= 1024)
            {
                item.ProgressBarWidth = 56;
                item.ProgressBarHeight = 107;
                item.CanvasLeft = 0;
                item.CanvasTop = 22;
            }
            else
            {
                item.ProgressBarWidth = 42;
                item.ProgressBarHeight = 85;
                item.CanvasLeft = 1;
                item.CanvasTop = 17;
            }

            return item;
        }

        private void ResizeImageButton(ImageButton imageButton, int size)
        {
            imageButton.ImageWidth = size;
            imageButton.ImageHeight = size;
        }
        private void ResizeMeterPath(System.Windows.Shapes.Path meterPath, System.Windows.Thickness margin, int width, int height)
        {
            meterPath.Margin = margin;
            meterPath.Width = width;
            meterPath.Height = height;
        }

        private void UpdateDeviceAttrUI(BatteryAttributes batteryAttributes,double updateSpanTime)
        {
            if (IsENGMode == 0)
            {
                DateTime dt1 = DateTime.Now;
                refreshTimeStatusValue.Content = dt1.ToString("HH:mm:ss");
            }
            else
            {
                SetRealRefreshTimeToStatusBar(updateSpanTime);
            }

            if (batteryAttributes != null)
            {
                sOCText.Text = batteryAttributes.SOC.ToString();
                sOHText.Text = string.Format("{0:0.00}", batteryAttributes.SOH);

                double current = (Convert.ToDouble(batteryAttributes.Current) / 1000.0);
                double voltage = (Convert.ToDouble(batteryAttributes.Voltage) / 1000.0);

                sohProgressBar.Value = batteryAttributes.SOH;
                socProgressBar.Value = batteryAttributes.SOC;
                tempProgressBar.Value = batteryAttributes.Temperature;

                currentText.Text = current.ToString();
                voltageText.Text = voltage.ToString();

                mainWindowViewModel.CurrentAngleValue = ConvertValuetoAngle(current);
                mainWindowViewModel.VoltageAngleValue = ConvertValuetoAngle(voltage);

                tempText.Text = string.Format("{0:0.00}", batteryAttributes.Temperature);

                if (batteryAttributes.DeviceName != null)
                {
                    deviceNameText.Text = batteryAttributes.DeviceName.ToString();
                }
                if (batteryAttributes.ManufactureDate != null && batteryAttributes.ManufactureDate.Length > 2)
                {
                    manufactureDateText.Text =
                        string.Format("{0:0000}/{1:00}/{2:00}", batteryAttributes.ManufactureDate[0], batteryAttributes.ManufactureDate[1], batteryAttributes.ManufactureDate[2]);
                }
                if (batteryAttributes.Barcode != null)
                {
                    barcodeText.Text = batteryAttributes.Barcode.ToString();
                }
                designVoltageText.Text = batteryAttributes.DesignVoltage.ToString();
                designCapacityText.Text = batteryAttributes.DesignCapcity.ToString();
                remainCapacityText.Text = batteryAttributes.RemainCapacity.ToString();
                fCCText.Text = batteryAttributes.FullChargeCapacity.ToString();
                cycleCountedText.Text = batteryAttributes.CycleCounted.ToString();

                if (batteryAttributes.CellsVoltage != null)
                {
                    if (mRecordVoltageNumber != batteryAttributes.CellsVoltage.Length)
                    {
                        mRecordVoltageNumber = batteryAttributes.CellsVoltage.Length;

                        batterySocGrid.Children.Clear();
                        for (int i = 0; i < batteryAttributes.CellsVoltage.Length; i++)
                        {
                            double data = Convert.ToDouble(batteryAttributes.CellsVoltage[i] / 1000.0);
                            CellProgressBar item = new CellProgressBar();
                            item.Title = (i + 1).ToString();
                            item.VoltageText = string.Format("{0:0.000}", data);
                            item.VoltageValue = data / MAXCELLVOLTAGE;
                            item = ResizeCellProgressBar(item);
                            batterySocGrid.Children.Add(item);
                        }

                        if (batteryAttributes.CellsVoltage.Length < 14) //增加數目
                        {
                            int hideNumber = 14 - batteryAttributes.CellsVoltage.Length;
                            for (int i = 0; i < hideNumber; i++)
                            {
                                CellProgressBar item = new CellProgressBar();
                                item.Visibility = System.Windows.Visibility.Hidden;
                                batterySocGrid.Children.Add(item);
                            }
                        }
                    }
                    else
                    {
                        bool isSuccess = true;
                        for (int i = 0; i < batteryAttributes.CellsVoltage.Length; i++)
                        {
                            double data = Convert.ToDouble(batteryAttributes.CellsVoltage[i] / 1000.0);
                            if (batterySocGrid.Children.Count > i)
                            {
                                ((CellProgressBar)batterySocGrid.Children[i]).VoltageText = string.Format("{0:0.000}", data);
                                ((CellProgressBar)batterySocGrid.Children[i]).VoltageValue = data / MAXCELLVOLTAGE;
                            }
                            else
                            {
                                isSuccess = false;
                            }
                        }
                        if (isSuccess)
                        {
                            mRecordVoltageNumber = 0;
                        }
                    }
                }
                else
                {
                    mRecordVoltageNumber = 0;
                }

                if (batteryAttributes.RTCTime != null && batteryAttributes.RTCTime.Length > 5)
                {
                    rtcCountedText.Text = string.Format("{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}", 
                        batteryAttributes.RTCTime[0], batteryAttributes.RTCTime[1], batteryAttributes.RTCTime[2],
                        batteryAttributes.RTCTime[3], batteryAttributes.RTCTime[4], batteryAttributes.RTCTime[5]);
                }
                else
                {
                    rtcCountedText.Text = "";
                }

                if (IsENGMode > 0 && batteryAttributes.ChargeLog != null)  // Fixed by Neil 2016.03.31 測試是否是ChargeLog造成 Memory leak
                {
                    bool isRefreshLogData = (deviceAttrListView.Items.Count != batteryAttributes.ChargeLog.Length);
                    if (isRefreshLogData)
                    {
                        deviceAttrListView.Items.Clear();
                    }

                    for (int i = 0; i < batteryAttributes.ChargeLog.Length; i++)
                    {
                        if (batteryAttributes.ChargeLog[i].Length > 0)
                        {
                            ChargeLogItem value = new ChargeLogItem();
                            if (batteryAttributes.ChargeLog[i][1] == 0 || batteryAttributes.ChargeLog[i][2] == 0)
                            {
                                value.Item = "No Data";
                            }
                            else
                            {
                                try
                                {
                                    DateTime dt = new DateTime(batteryAttributes.ChargeLog[i][0], batteryAttributes.ChargeLog[i][1], batteryAttributes.ChargeLog[i][2], batteryAttributes.ChargeLog[i][3], batteryAttributes.ChargeLog[i][4], 0);
                                    if (i == 0)
                                    {
                                        value.Item = "Fisrt Charge Time";
                                    }
                                    else if (i == 1)
                                    {
                                        value.Item = "The Longest Charge Gap";
                                    }
                                    else
                                    {
                                        value.Item = string.Format("{0}th Record", i - 1);
                                    }

                                    value.StartTime = dt.ToString("yyyy/MM/dd HH:mm");
                                    value.EndTime = dt.AddMinutes(Convert.ToDouble(batteryAttributes.ChargeLog[i][7])).ToString("yyyy/MM/dd HH:mm");
                                    value.StartVolt = batteryAttributes.ChargeLog[i][5];
                                    value.EndVolt = batteryAttributes.ChargeLog[i][6];
                                    value.Duration = batteryAttributes.ChargeLog[i][7];
                                }
                                catch
                                {
                                    value.Item = "Error Data";
                                }
                            }
                            if (isRefreshLogData)
                            {
                                deviceAttrListView.Items.Add(value);
                            }
                            else
                            {
                                deviceAttrListView.Items[i] = value;
                            }
                        }
                    }
                }
                else
                {
                    deviceAttrListView.Items.Clear();
                }

                if (batteryAttributes.Protection != null && batteryAttributes.Protection.Length > 6)
                {
                    overVoltStatus.IsSwitchOn = (batteryAttributes.Protection[0] > 0);
                    underVoltStatus.IsSwitchOn = (batteryAttributes.Protection[1] > 0);
                    overCurrChargedStatus.IsSwitchOn = (batteryAttributes.Protection[2] > 0);
                    overCurrDischargedStatus.IsSwitchOn = (batteryAttributes.Protection[3] > 0);

                    overTempStatus.IsSwitchOn = (batteryAttributes.Protection[4] > 0);
                    underTempStatus.IsSwitchOn = (batteryAttributes.Protection[5] > 0);
                    shortCircuitStatus.IsSwitchOn = (batteryAttributes.Protection[6] > 0);
                }
            }
            else
            {
                sOCText.Text = "";
                sOHText.Text = "";
                currentText.Text = "";
                voltageText.Text = "";

                mainWindowViewModel.CurrentAngleValue = ConvertValuetoAngle(0.0 / 1000.0);
                mainWindowViewModel.VoltageAngleValue = ConvertValuetoAngle(0.0 / 1000.0);

                sohProgressBar.Value = 100;
                socProgressBar.Value = 100;
                tempProgressBar.Value = 0;

                tempText.Text = "";

                deviceNameText.Text = "";
                manufactureDateText.Text = "";
                barcodeText.Text = "";

                designVoltageText.Text = "";
                designCapacityText.Text = "";
                remainCapacityText.Text = "";
                fCCText.Text = "";
                cycleCountedText.Text = "";

                rtcCountedText.Text = "";

                batterySocGrid.Children.Clear();
                deviceAttrListView.Items.Clear();

                underVoltStatus.IsSwitchOn = false;
                overVoltStatus.IsSwitchOn = false;
                overCurrDischargedStatus.IsSwitchOn = false;
                overCurrChargedStatus.IsSwitchOn = false;
                overTempStatus.IsSwitchOn = false;
                underTempStatus.IsSwitchOn = false;
                shortCircuitStatus.IsSwitchOn = false;
            }
            DebugLog(8);
        }

        private void ResizeAbnormalStatus(int size)
        {
            uvd.FontSize = size;
            ovc.FontSize = size;
            ocd.FontSize = size;
            occ.FontSize = size;
            ot.FontSize = size;
            ut.FontSize = size;
            sc.FontSize = size;
        }

        /// <summary>
        /// 時速表之值與角度轉換
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private int ConvertValuetoAngle(double value)
        {
            return Convert.ToInt32(Math.Abs(value - 0.0) * 2.25 + METERBASEVALUE);
        }
        #endregion
        #endregion
        #region APP Setting
        /// <summary>
        /// 視窗準備關閉之事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (!mEnableAppClose) // 決定可否關閉視窗之條件
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// 啟動關閉視窗之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseAP_Click(object sender, MouseButtonEventArgs e)
        {
            if (MessageBox.Show("Close Application?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                ThreadPool.QueueUserWorkItem(o => DisconnectBattery(true));
                ThreadPool.QueueUserWorkItem(o => CloseAP());
            }
        }
        /// <summary>
        /// 結束所有Thread再關閉
        /// </summary>
        private void CloseAP()
        {
            while (!mEnableAppClose)
            {
                Thread.Sleep(20);
            }

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    this.Close();
                }));
        }


        private void logoImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            mLogoClickTime++;
            if (mLogoClickTime > 10)
            {
                MessageBox.Show(((int)CCode).ToString(), "Ver");
                mLogoClickTime = 0;
            }
        }

        /// <summary>
        /// 初始化視窗程式需要之設定檔
        /// </summary>
        private void InitializeAppSetting()
        {
            mAppPath = System.AppDomain.CurrentDomain.BaseDirectory;

            string appSettingFilePath = mAppPath + DiagnosticParameter.APPSETTINGFILEPATH;
            try
            {
                if (File.Exists(appSettingFilePath))
                {
                    using (StreamReader file = File.OpenText(appSettingFilePath))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        mAppSetting = (AppSetting)serializer.Deserialize(file, typeof(AppSetting));
                    }
                }
            }
            catch { }

            if (mAppSetting == null)
            {
                mAppSetting = new AppSetting();

                string[] portList = System.IO.Ports.SerialPort.GetPortNames();
                if (portList != null && portList.Length > 0)
                {
                    mAppSetting.Port = portList[0];
                }

                mAppSetting.BaudRate = mAppSetting.BaudRateList[0];

                mAppSetting.IsAutoBaudRate = true;

                mAppSetting.RefreshTimer = DiagnosticParameter.MINIMUMUPDATEDEVICETIME;

                mAppSetting.ResolutionWidth = 800;
                mAppSetting.ResolutionHeight = 600;

                mAppSetting.SaveLogPath = mAppPath + DiagnosticParameter.APPDEVICELOGPATH;

                SaveAppSetting();
            }

            RefreshSetting();
        }
        /// <summary>
        /// 儲存AppSetting資訊至檔案(AppSetting.ap)內
        /// </summary>
        /// <returns></returns>
        public static bool SaveAppSetting()
        {
            bool isSuccess = false;
            string appSettingFilePath = mAppPath + DiagnosticParameter.APPSETTINGFILEPATH;

            if (mAppSetting != null)
            {
                using (FileStream fs = File.Open(appSettingFilePath, FileMode.Create))
                using (StreamWriter sw = new StreamWriter(fs))
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    jw.Formatting = Formatting.Indented;

                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(jw, mAppSetting);
                    isSuccess = true;
                }
            }

            return isSuccess;
        }
        #endregion

        #region Battery Method
        /// <summary>
        /// 初始化電池物件
        /// </summary>
        private void InitializeBattery()
        {
            // For Debug
            if (IsENGMode > 1)
            {
                mMessageFileName = DateTime.Now.ToString("yyyyMMdd") + ".txt";
                mMessagePath = mAppPath + DiagnosticParameter.MESSAGELOGPATH;
                mBattery.SendDataEvent += mBattery_SendDataEvent;
                mBattery.ReceivedDataEvent += mBattery_ReceivedDataEvent;
            }
        }

        private void mBattery_SendDataEvent(string port, GTE.Command.BatteryCmd cmdType, object data)
        {
            byte[] item = (byte[])data;

            mMessageData = mMessageData + "(Send)" + ParserData(port, cmdType, item) + Environment.NewLine;
        }

        private void mBattery_ReceivedDataEvent(string port, GTE.Command.BatteryCmd cmdType, object data)
        {
            byte[] item = (byte[])data;

            mMessageData = mMessageData + "(Received)" + ParserData(port, cmdType, item) + Environment.NewLine;
        }

        private string ParserData(string port, GTE.Command.BatteryCmd cmdType, byte[] item)
        {
            string result = null;
            if (item != null)
            {
                string value = "(" + string.Format("{0:X02}", (int)(byte)cmdType) + ")" + "(" + item.Length + ")";

                value = value + ">>";
                foreach (byte d in item)
                {
                    value = value + string.Format("{0:X02} ", d);
                }
                result = value;
            }
            return result;
        }

        private bool RecordMessageData(string fileName, string data)
        {
            bool isSuccess = true;

            if (File.Exists(fileName))
            {
                File.AppendAllText(fileName, data);
            }
            else
            {
                if (!Directory.Exists(mMessagePath))
                {
                    Directory.CreateDirectory(mMessagePath);
                }

                using (FileStream fs = File.Open(fileName, FileMode.Create))
                using (StreamWriter outfile = new StreamWriter(fs))
                {
                    outfile.WriteLine(data);
                }

            }

            return isSuccess;
        }

        #region Connect/Disconnect
        /// <summary>
        /// 與電池連線電池
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="port"></param>
        /// <param name="baudRate"></param>
        private void ConnectBattery(object sender, string port, int baudRate)
        {
            bool isSuccess = false;
            if (port != null && baudRate != 0)
            {
                if (mAppSetting.IsAutoBaudRate && mAppSetting.BaudRateList != null && mAppSetting.BaudRateList.Length > 0)
                {
                    foreach (int baudrate in mAppSetting.BaudRateList)
                    {
                        isSuccess = mBattery.Connect(port, baudrate);
                        if (isSuccess)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < 3; i++)
                    {
                        isSuccess = mBattery.Connect(port, baudRate);
                        if (isSuccess)
                        {
                            break;
                        }
                    }
                }         

                if (isSuccess)
                {
                    isSuccess = isSuccess && SetDeviceAdapter();
                    if (isSuccess)
                    {
                        mRecordVoltageNumber = 0;
                        StartUpdateDevice();
                    }
                }
            }

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    if (isSuccess)
                    {
                        mIsConnectToDevice = true;
                        SwitchTabButton(true);

                        SetConnectStatusToStatusBar("Connected");
                        SetBaudRateToStatusBar(mBattery.GetBaudRate().ToString());
                    }
                    else
                    {
                        ThreadPool.QueueUserWorkItem(o => DisconnectBattery(false));
                        MessageBox.Show("Check Port and Baud Rate", "Warning");
                    }

                    infoTabControl.IsEnabled = true;
                    diagnosticButton.IsEnabled = true;
                    Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                }));
        }
        /// <summary>
        /// 與電池溝通之物件斷線
        /// </summary>
        private void DisconnectBattery(bool isCloseAP)
        {
            StopUpdateDevice();

            mBattery.Disconnect();

            ReleaseDeviceAdapter();

            mBatteryAttrInfo = null;

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    UpdateDeviceAttrUI(mBatteryAttrInfo, 0);

                    mIsConnectToDevice = false;
                    SwitchTabButton(false);

                    SetConnectStatusToStatusBar("Disconnect");

                    if (isCloseAP)
                    {
                        mEnableAppClose = true;
                    }
                }));
        }
        #endregion
        private bool SetDeviceAdapter()
        {
            mDeviceAdapter = null;
            string version = mBattery.GetVersion();
            if (version != null && !version.Equals(""))
            {
                Regex regex = new Regex(@"^MPM");
                if (regex.IsMatch(version))
                {
                    mDeviceAdapter = new DiagnosticConfig_MPM(mBattery, CCode);
                }
                else
                {
                    regex = new Regex(@"^V");
                    if (regex.IsMatch(version))
                    {
                        mDeviceAdapter = new DiagnosticConfig_MPM(mBattery, CCode);
                    }
                    else
                    {
                        mDeviceAdapter = new DiagnosticConfig_MPS(mBattery, CCode);
                    }
                }
            }

            ResetDeviceAdapter();

            return (mDeviceAdapter != null);
        }

        private void ResetDeviceAdapter()
        {
            if (mDeviceAdapter != null)
            {
                mDeviceAdapter.SetEngCode(IsENGMode);

                mDeviceAdapter.ErrorMessageEvent += mDeviceAdapter_ErrorMessageEvent;
            }
        }

        private void ReleaseDeviceAdapter()
        {
            if (mDeviceAdapter != null)
            {
                mDeviceAdapter.ErrorMessageEvent -= mDeviceAdapter_ErrorMessageEvent;
                mDeviceAdapter = null;
            }
        }

        private void mDeviceAdapter_ErrorMessageEvent(ErrorType type, ErrorLevelType level, string message, string stack)
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    if (mUpdateDeviceThread != null)
                    {
                        mUpdateDeviceThread.Abort();
                    }

                    DisconnectBattery(false);
                    MessageBox.Show("Lost Device", "Error");
                }));
        }
        #region Update Device Information
        /// <summary>
        /// 更新電池資訊
        /// </summary>
        private void StartUpdateDevice()
        {
            mUpdateDeviceThread = new Thread(new ThreadStart(GetDeviceInformation));
            mUpdateDeviceThread.Start();
        }
        /// <summary>
        /// 停止更新電池資訊
        /// </summary>
        private void StopUpdateDevice()
        {
            mEnableUpdateDeviceInformation = false;
            while (mUpdateDeviceThread != null)
            {
                Thread.Sleep(20);

                if (!mUpdateDeviceThread.IsAlive)
                {
                    mUpdateDeviceThread = null;
                }
            }
        }
        /// <summary>
        /// 取得需要的電池資訊
        /// </summary>
        private void GetDeviceInformation()
        {
            if (mAppSetting.RefreshTimer < DiagnosticParameter.MINIMUMUPDATEDEVICETIME)//更新時間不可小於MINIMUMUPDATEDEVICETIME
            {
                mAppSetting.RefreshTimer = DiagnosticParameter.MINIMUMUPDATEDEVICETIME;
                SaveAppSetting();
            }

            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();//引用stopwatch物件

            sw.Reset();//碼表歸零
            sw.Start();//碼表開始計時

            mEnableUpdateDeviceInformation = true;
            while (mEnableUpdateDeviceInformation)
            {
                DebugLog(1);
                if (mDeviceAdapter != null)
                {
                    try
                    {
                        if (mBatteryAttrInfo == null)
                        {
                            mBatteryAttrInfo = mDeviceAdapter.GetDeviceAllInformation(null);
                        }
                        else
                        {
                            mBatteryAttrInfo = mDeviceAdapter.GetDeviceAllInformation(mBatteryAttrInfo);
                        }
                    }
                    catch
                    {
                        mBatteryAttrInfo = null; //嘗試重新讀取
                    }
                }
                DebugLog(2);
                if (IsAutoSaveLog)
                {
                    SaveLogAuto(mBatteryAttrInfo);
                }
                DebugLog(3);
                sw.Stop();//碼表停止

                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                    () =>
                    {
                        DebugLog(4);
                        UpdateDeviceAttrUI(mBatteryAttrInfo, sw.Elapsed.TotalSeconds);
                        DebugLog(5);
                        // For Debug
                        if (IsENGMode > 2)
                        {
                            string number = "";
                            if (mBatteryAttrInfo != null)
                            {
                                number = mBatteryAttrInfo.Barcode;
                            }
                            string fileName = mMessagePath + number + "_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                            RecordMessageData(fileName, mMessageData.Clone().ToString());
                            mMessageData = null;
                        }
                    }));

                if (mEnableUpdateDeviceInformation && sw.Elapsed.TotalSeconds < 3)
                {
                    Thread.Sleep(mAppSetting.RefreshTimer * 1000);
                }

                sw.Reset();//碼表歸零
                sw.Start();//碼表開始計時
            }
        }
        #endregion
        #endregion

        #region Save Device Log
        /// <summary>
        /// 自動記錄電池更新狀態
        /// </summary>
        private void SaveLogAuto(BatteryAttributes batteryAttributes)
        {
            DateTime dt = DateTime.Now;

            string fileName = mAppSetting.SaveLogPath + batteryAttributes.Barcode + "_" + dt.ToString("yyyyMMdd") + ".txt";
            string recordText = null;
            if (File.Exists(fileName))
            {
                if (mDeviceAdapter != null)
                {
                    recordText = dt.ToString("HH:mm:ss") + "," + mDeviceAdapter.GetAutoSaveLogInfo(batteryAttributes);
                }
                File.AppendAllText(fileName, recordText + Environment.NewLine);
            }
            else
            {
                string titleList = null;
                if (mDeviceAdapter != null)
                {
                    titleList = mDeviceAdapter.GetAutoSaveLogTitle(batteryAttributes);
                    recordText = dt.ToString("HH:mm:ss") + "," + mDeviceAdapter.GetAutoSaveLogInfo(batteryAttributes);
                }
                if (titleList != null)
                {
                    if (!Directory.Exists(mAppSetting.SaveLogPath))
                    {
                        Directory.CreateDirectory(mAppSetting.SaveLogPath);
                    }

                    using (FileStream fs = File.Open(fileName, FileMode.Create))
                    using (StreamWriter outfile = new StreamWriter(fs))
                    {
                        outfile.WriteLine(titleList);
                        outfile.WriteLine(recordText);
                    }
                }
            }
        }
        private void PreSaveLog()
        {
            if (mDeviceAdapter != null)
            {
                mDeviceAdapter.SetRefreshSaveLog(true);


                while (mDeviceAdapter.IsRefreshSaveLog())
                {

                }
            }
            SaveLog();
        }

        /// <summary>
        /// 記錄當下的電池狀態
        /// </summary>
        private void SaveLog()
        {
            if (mBatteryAttrInfo != null)
            {
                bool isCreate = true;

                string recordText = null;
                BatteryAttributes batteryAttributes = mBatteryAttrInfo.Clone();
                DateTime dt = DateTime.Now;
                string fileName = mAppSetting.SaveLogPath + batteryAttributes.Barcode + "_" + dt.ToString("yyyyMMddHHmmss") + ".txt";

                if (mDeviceAdapter != null)
                {
                    recordText = mDeviceAdapter.GetSaveLogInfo(dt, batteryAttributes);

                    if (File.Exists(fileName))
                    {
                        using (StreamWriter outfile = new StreamWriter(fileName))
                        {
                            outfile.Write(recordText);
                        }
                    }
                    else
                    {

                        if (!Directory.Exists(mAppSetting.SaveLogPath))
                        {
                            try
                            {
                                Directory.CreateDirectory(mAppSetting.SaveLogPath);
                            }
                            catch
                            {
                                isCreate = false;
                            }
                        }
                        if (isCreate)
                        {
                            using (FileStream fs = File.Open(fileName, FileMode.Create))
                            using (StreamWriter outfile = new StreamWriter(fs))
                            {
                                outfile.Write(recordText);
                            }
                        }
                    }
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                        () =>
                        {
                            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                            if (!isCreate)
                            {
                                MessageBox.Show("Not found Directory.");
                            }
                            saveLogText.Text = recordText;
                        }));
                }
            }
        }

        private void DebugLog(int section)
        {
            //if (IsENGMode > 0)
            //{
            //    string recordText = null;
            //    DateTime dt = DateTime.Now;
            //    string fileName = mAppPath + "Bug_" + dt.ToString("yyyyMMdd") + ".txt";

            //    if (mDeviceAdapter != null)
            //    {
            //        recordText = section.ToString();

            //        if (File.Exists(fileName))
            //        {
            //            using (StreamWriter outfile = new StreamWriter(fileName))
            //            {
            //                outfile.Write(recordText);
            //            }
            //        }
            //        else
            //        {
            //            using (FileStream fs = File.Open(fileName, FileMode.Create))
            //            using (StreamWriter outfile = new StreamWriter(fs))
            //            {
            //                outfile.Write(recordText);
            //            }
            //        }
            //    }
            //}
        }
        #endregion

        #region Switch TabItem
        /// <summary>
        /// 處理TabItem點擊後的動作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void infoTabControl_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            TabControl control = (TabControl)sender;

            if (e.Source is ImageButton)
            {
                ImageButton imageButton = (ImageButton)e.Source;
                if (imageButton.IsMouseStop)
                {
                    e.Handled = true;
                }
                else
                {
                    if (imageButton.Equals(diagnosticButton))
                    {
                        if (!mIsConnectToDevice && mAppSetting.Port != null)
                        {
                            diagnosticButton.IsEnabled = false;
                            infoTabControl.IsEnabled = false;
                            Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                            SetConnectStatusToStatusBar("Connecting...");
                            ThreadPool.QueueUserWorkItem(o => ConnectBattery(diagnosticButton, mAppSetting.Port, mAppSetting.BaudRate));
                        }
                    }
                    else if (imageButton.Equals(battSOCButton))
                    {
                    }
                    else if (imageButton.Equals(userRecordButton))
                    {

                    }
                    else if (imageButton.Equals(firmwareButton))
                    {
                        var dialog = new System.Windows.Forms.OpenFileDialog();
                        dialog.Filter = "Hex files (*.hex)|*.hex";
                        if (Directory.Exists(mFirmwarePath))
                        {
                            dialog.InitialDirectory = mFirmwarePath;
                        }
                        else
                        {
                            dialog.InitialDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
                        }
                        if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            mFirmwarePath = System.IO.Path.GetDirectoryName(dialog.FileName);
                            if (mFirmwarePath.Contains(" "))
                            {
                                MessageBox.Show("Path cannot exist space char");
                            }
                            else
                            {
                                if (mUpdateDeviceThread != null)
                                {
                                    mUpdateDeviceThread.Abort();
                                }
                                DisconnectBattery(false);

                                string an1310 = System.AppDomain.CurrentDomain.BaseDirectory + "Tools\\AN1310cl.exe";

                                System.Diagnostics.Process process = new System.Diagnostics.Process();
                                process.StartInfo.FileName = an1310;
                                process.StartInfo.Arguments = string.Format(" -d {0} -p -r {1}", mAppSetting.Port, dialog.FileName);
                                process.Start();
                            }
                        }

                        e.Handled = true;
                    }
                    else if (imageButton.Equals(saveLogButton))
                    {
                        Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                        ThreadPool.QueueUserWorkItem(o => PreSaveLog());
                    }
                    else if (imageButton.Equals(settingButton))
                    {
                        SettingWindow settingWindow = new SettingWindow();
                        settingWindow.Owner = this; // maybe not
                        settingWindow.ShowDialog();
                        RefreshSetting();
                        e.Handled = true;
                    }
                    else
                    {
                        e.Handled = true;

                    }
                }
            }
            else if (e.Source is Button)
            {
                Button bt = e.Source as Button;
                if (bt != null && bt.Name.Equals("rtcResetButton"))
                {

                }
                else
                {
                    e.Handled = true;
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void rtcResetButton_Click(object sender, RoutedEventArgs e)
        {
            if (mDeviceAdapter != null)
            {
                mDeviceAdapter.IsResetRTC(true);
            }
        }
        #endregion

        #region BatteryInformation
        //private void InitializeBatteryInformation()
        //{
        //    DisposeBatteryInformation();
        //    if (mBattery != null)
        //    {
        //        BatteryVersion bv = mBattery.GetVersion();

        //        switch (bv)
        //        {
        //            case BatteryVersion.MPM:
        //                {
        //                    mBatteryInformation = new BatteryMPM();
        //                    break;
        //                }
        //            case BatteryVersion.MPS:
        //                {
        //                    mBatteryInformation = new BatteryMPS();
        //                    break;
        //                }
        //            case BatteryVersion.MPMV:
        //                {
        //                    mBatteryInformation = new BatteryMPMV();
        //                    break;
        //                }
        //            case BatteryVersion.VM:
        //                {
        //                    mBatteryInformation = new BatteryV();

        //                    break;
        //                }
        //        }

        //        if (mBatteryInformation != null)
        //        {
        //            mBatteryInformation.SetGTEBattery(mBattery);

        //            mBatteryInformation.MessageEvent += mBatteryInformation_MessageEvent;
        //        }
        //    }
        //}

        //private void DisposeBatteryInformation()
        //{
        //    if (mBatteryInformation != null)
        //    {
        //        mBatteryInformation.MessageEvent -= mBatteryInformation_MessageEvent;
        //        mBatteryInformation = null;
        //    }
        //}

        //private void mBatteryInformation_MessageEvent(MessageType messageType, string message)
        //{
        //    switch (messageType)
        //    {
        //        case MessageType.Error_GetData:
        //            {
        //                CallbackMessage callbackData = new CallbackMessage()
        //                {
        //                    Style = MessageName.DisconnectBattery,
        //                    Status = MessageStatus.Disconnect,
        //                    Data = message
        //                };

        //                Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { null, callbackData });
        //                break;
        //            }
        //    }
        //}
        #endregion
    }
}
