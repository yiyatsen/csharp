﻿using GTE.Encryption;
using GTEDiagnostic.Parameter;
using GTEDiagnosticConfig.Setting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEDiagnostic
{
    /// <summary>
    /// SettingWindow.xaml 的互動邏輯
    /// </summary>
    public partial class SettingWindow : Window
    {
        #region WIN 32
        [DllImport("user32.dll")]
        private extern static int SetWindowLong(IntPtr hwnd, int index, int value);
        [DllImport("user32.dll")]
        private extern static int GetWindowLong(IntPtr hwnd, int index);

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x00080000;

        void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            WindowInteropHelper wih = new WindowInteropHelper(this);
            int style = GetWindowLong(wih.Handle, GWL_STYLE);
            SetWindowLong(wih.Handle, GWL_STYLE, style & ~WS_SYSMENU);
        }
        #endregion

        #region Property
        private const string SERIALNUMBERFILE = "SerialNumber_DateTime.txt";
        private AppSetting mAppSetting = MainWindow.AppSetting; //電池目前屬性之資訊
        private DispatcherTimer mPortScanTimer; // 監聽Port狀態之定時器
        private EncryptionAESAnto mEncryptionAlog = new EncryptionAESAnto();

        private string[] mResolutionSize = { "1024*768", "800*600" };

        private string ENGKEYINPUT = "";
        #endregion

        public SettingWindow()
        {
            SourceInitialized += MainWindow_SourceInitialized;

            InitializeComponent();

            InitializeAppSetting();
            InitializeScanPort();

            InitializeUI();
        }

        #region App Setting Method
        /// <summary>
        /// 取消設定並且關閉視窗之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 確認設定並且關閉視窗之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfrimButton_Click(object sender, RoutedEventArgs e)
        {
            SaveSettingInfo();
            this.Close();
        }
        /// <summary>
        /// 儲存修改的設定值
        /// </summary>
        private void SaveSettingInfo()
        {
            if (portList.SelectedItem != null)
            {
                mAppSetting.Port = portList.SelectedItem.ToString();
            }

            if (baudRateList.SelectedItem != null)
            {
                mAppSetting.BaudRate = int.Parse(baudRateList.SelectedItem.ToString());
            }

            mAppSetting.IsAutoBaudRate = autoBaudRate.IsChecked.Value;

            if (resolutionList.SelectedItem != null)
            {
                string[] resolutionItem = resolutionList.SelectedItem.ToString().Split('*');
                if (resolutionItem != null && resolutionItem.Length == 2)
                {
                    mAppSetting.ResolutionWidth = int.Parse(resolutionItem[0]);
                    mAppSetting.ResolutionHeight = int.Parse(resolutionItem[1]);
                }
            }

            mAppSetting.RefreshTimer = Convert.ToInt32(updateDeviceTimerText.Text);
            mAppSetting.SaveLogPath = recordLogPathText.Text.Replace(SERIALNUMBERFILE, "");
            if (isAutoLogSwitch.IsChecked.Value)
            {
                if (!Directory.Exists(mAppSetting.SaveLogPath))
                {
                    Directory.CreateDirectory(mAppSetting.SaveLogPath);
                }
            }

            MainWindow.IsAutoSaveLog = isAutoLogSwitch.IsChecked.Value;

            MainWindow.SaveAppSetting();
        }
        /// <summary>
        /// 初始化視窗程式需要之設定檔
        /// </summary>
        private void InitializeAppSetting()
        {
            if (mAppSetting != null)
            {

            }
        }
        #endregion

        #region Scan Port
        /// <summary>
        /// 初始化ComPort監聽時間器
        /// </summary>
        private void InitializeScanPort()
        {
            SetSerialPortToComboBox(true);
            mPortScanTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(DiagnosticParameter.SCANPORTTIME) };
            mPortScanTimer.Tick += PortScanTimer_Tick;
            mPortScanTimer.Start();
        }
        /// <summary>
        /// 啟動監聽ComPort狀態之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PortScanTimer_Tick(object sender, EventArgs e)
        {
            SetSerialPortToComboBox(false);
        }
        /// <summary>
        /// 設定Port列表至ComboBox
        /// </summary>
        private void SetSerialPortToComboBox(bool isFirstTime)
        {
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();
            if (ports != null && ports.Length > 0)
            {
                bool isChanged = false;
                foreach (object data in portList.Items)
                {
                    if (!ports.Contains(data.ToString()))
                    {
                        isChanged = true;
                    }
                }

                if (isChanged || isFirstTime)
                {
                    string port = null;
                    int index = 0;
                    if (portList.SelectedItem != null)
                    {
                        port = portList.SelectedItem.ToString();
                    }

                    portList.Items.Clear();
                    for (int i = 0; i < ports.Length; i++)
                    {
                        if (isFirstTime)
                        {
                            if (ports[i].Equals(mAppSetting.Port))
                            {
                                index = i;
                            }
                        }
                        else
                        {
                            if (ports[i].Equals(port))
                            {
                                index = i;
                            }
                        }

                        portList.Items.Add(ports[i]);
                    }
                    portList.SelectedIndex = index;
                }
            }
        }
        #endregion

        #region Baud Rate
        /// <summary>
        /// 設定BaudRate列表至ComboBox
        /// </summary>
        private void SetBaudRateToComboBox()
        {
            for (int i = 0; i < mAppSetting.BaudRateList.Length; i++)
            {
                baudRateList.Items.Add(mAppSetting.BaudRateList[i]);
                if (mAppSetting.BaudRate == mAppSetting.BaudRateList[i])
                {
                    baudRateList.SelectedIndex = i;
                }
            }
        }

        private void autoBaudRate_Checked(object sender, RoutedEventArgs e)
        {
            if (autoBaudRate.IsChecked.Value)
            {
                baudRateList.IsEnabled = false;
            }
            else
            {
                baudRateList.IsEnabled = true;
            }
        }
        #endregion

        #region UI Method
        /// <summary>
        /// 初始化UI數值
        /// </summary>
        private void InitializeUI()
        {
            SetBaudRateToComboBox();
            SetResolutionToComboBox();

            updateDeviceTimerText.Text = mAppSetting.RefreshTimer.ToString();
            recordLogPathText.Text = mAppSetting.SaveLogPath + SERIALNUMBERFILE;
            IsAutoLogSwitch(isAutoLogSwitch, MainWindow.IsAutoSaveLog);

            if (mAppSetting.IsAutoBaudRate)
            {
                autoBaudRate.IsChecked = true;
                baudRateList.IsEnabled = false;
            }

            ResetENGMode();
        }

        private void ResetENGMode()
        {
            if (MainWindow.IsENGMode > 0)
            {
                isAutoLogText.Visibility = System.Windows.Visibility.Visible;
                isAutoLogSwitch.Visibility = System.Windows.Visibility.Visible;
                DecryptionTab.Visibility = System.Windows.Visibility.Visible;
                updateTimerText.Visibility = System.Windows.Visibility.Visible;
                updateDeviceTimerText.Visibility = System.Windows.Visibility.Visible;
            }
        }
        private void isAutoLogSwitch_Click(object sender, RoutedEventArgs e)
        {
            ToggleButton tb = (ToggleButton)sender;
            IsAutoLogSwitch(tb, tb.IsChecked.Value);
        }
        private void IsAutoLogSwitch(ToggleButton button, bool isSwitchOn)
        {
            button.IsChecked = isSwitchOn;

            if (isSwitchOn)
            {
                button.Content = "On";
            }
            else
            {
                button.Content = "Off";
            }
        }
        #region Display Resolution
        /// <summary>
        /// 設定螢幕解析度至ComboBox
        /// </summary>
        private void SetResolutionToComboBox()
        {
            resolutionList.Items.Clear();
            for (int i = 0; i < mResolutionSize.Length; i++)
            {
                resolutionList.Items.Add(mResolutionSize[i]);
                if (mResolutionSize[i].Equals(mAppSetting.ResolutionWidth.ToString() + "*" + mAppSetting.ResolutionHeight.ToString()))
                {
                    resolutionList.SelectedIndex = i;
                }
            }
        }
        #endregion
        #region Record Path Setting Method
        private void savePathButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            string path = recordLogPathText.Text.Replace(SERIALNUMBERFILE, "");
            if (Directory.Exists(path))
            {
                dialog.SelectedPath = path;
            }
            else
            {
                dialog.SelectedPath = System.AppDomain.CurrentDomain.BaseDirectory;
            }
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                recordLogPathText.Text = dialog.SelectedPath + "\\" + SERIALNUMBERFILE;
            }

            this.Activate();
        }
        #endregion
        #region Update Device Timer Method
        private void updateDeviceTimerText_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !onlyNumeric(e.Text);
        }
        private bool onlyNumeric(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that allows numeric input only
            return !regex.IsMatch(text);
        }
        #endregion
        #endregion

        #region Decryption Infromation
        private void enButton_Click(object sender, RoutedEventArgs e)
        {
            string outputData = "";
            string data = decryptionDataText.Text;
            string[] lines = data.Split('\n');
            foreach (string line in lines)
            {
                outputData = outputData
                    + "!"
                    + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.Unicode.GetBytes(line.Trim('\r'))))
                    + Environment.NewLine;
            }
            encryptionDataText.Text = outputData;
        }
        private void deButton_Click(object sender, RoutedEventArgs e)
        {
            string outputData = "";
            string data = encryptionDataText.Text;
            string[] lines = data.Split('\n');
            foreach (string line in lines)
            {
                outputData = outputData
                    + Encoding.Unicode.GetString(mEncryptionAlog.DecryptionData(Convert.FromBase64String(line.Trim('\r').TrimStart('!'))))
                    + Environment.NewLine;
            }
            decryptionDataText.Text = outputData;
        }
        #endregion

        private void Window_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(MainWindow.IsENGMode > 0))
            {
                ENGKEYINPUT = ENGKEYINPUT + e.Text;
                Regex regex = new Regex(@"^" + ENGKEYINPUT);

                if (!MainWindow.mENGKey.Equals(ENGKEYINPUT))
                {
                    if (!regex.IsMatch(MainWindow.mENGKey))
                    {
                        ENGKEYINPUT = "";
                    }
                }
                else
                {
                    MainWindow.IsENGMode = 1;

                    ResetENGMode();
                }
            }
        }
    }
}
