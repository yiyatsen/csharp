﻿#pragma checksum "..\..\MainWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "08BD1258BBAB877FC4C32485BD66C117D3964CF8F64224BAD86801DA78944A0E"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using PiPiWPFControl;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace GTEDiagnostic {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 40 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.StatusBarItem statusValue;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.StatusBarItem portStatusValue;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.StatusBarItem baudRateStatusValue;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.StatusBarItem versionValue;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Separator refreshTimeStatusSeparator;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.StatusBarItem refreshTimeStatusText;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.StatusBarItem refreshTimeStatusValue;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Separator isAutoLogS;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.StatusBarItem isAutoLogText;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.StatusBarItem isAutoLogValue;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image logoImage;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl infoTabControl;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PiPiWPFControl.ImageButton diagnosticButton;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar sohProgressBar;
        
        #line default
        #line hidden
        
        
        #line 166 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar socProgressBar;
        
        #line default
        #line hidden
        
        
        #line 209 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path currentPath;
        
        #line default
        #line hidden
        
        
        #line 228 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path voltagePath;
        
        #line default
        #line hidden
        
        
        #line 248 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar tempProgressBar;
        
        #line default
        #line hidden
        
        
        #line 297 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox sOHText;
        
        #line default
        #line hidden
        
        
        #line 311 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox sOCText;
        
        #line default
        #line hidden
        
        
        #line 325 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox currentText;
        
        #line default
        #line hidden
        
        
        #line 339 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox voltageText;
        
        #line default
        #line hidden
        
        
        #line 353 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tempText;
        
        #line default
        #line hidden
        
        
        #line 383 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox deviceNameText;
        
        #line default
        #line hidden
        
        
        #line 390 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox manufactureDateText;
        
        #line default
        #line hidden
        
        
        #line 397 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox barcodeText;
        
        #line default
        #line hidden
        
        
        #line 404 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox designVoltageText;
        
        #line default
        #line hidden
        
        
        #line 414 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox designCapacityText;
        
        #line default
        #line hidden
        
        
        #line 424 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox remainCapacityText;
        
        #line default
        #line hidden
        
        
        #line 434 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox fCCText;
        
        #line default
        #line hidden
        
        
        #line 444 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox cycleCountedText;
        
        #line default
        #line hidden
        
        
        #line 448 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button rtcResetButton;
        
        #line default
        #line hidden
        
        
        #line 454 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox rtcCountedText;
        
        #line default
        #line hidden
        
        
        #line 483 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock uvd;
        
        #line default
        #line hidden
        
        
        #line 486 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PiPiWPFControl.SwitchImage underVoltStatus;
        
        #line default
        #line hidden
        
        
        #line 492 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ovc;
        
        #line default
        #line hidden
        
        
        #line 495 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PiPiWPFControl.SwitchImage overVoltStatus;
        
        #line default
        #line hidden
        
        
        #line 501 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ocd;
        
        #line default
        #line hidden
        
        
        #line 504 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PiPiWPFControl.SwitchImage overCurrDischargedStatus;
        
        #line default
        #line hidden
        
        
        #line 510 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock occ;
        
        #line default
        #line hidden
        
        
        #line 513 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PiPiWPFControl.SwitchImage overCurrChargedStatus;
        
        #line default
        #line hidden
        
        
        #line 519 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ot;
        
        #line default
        #line hidden
        
        
        #line 522 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PiPiWPFControl.SwitchImage overTempStatus;
        
        #line default
        #line hidden
        
        
        #line 528 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ut;
        
        #line default
        #line hidden
        
        
        #line 531 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PiPiWPFControl.SwitchImage underTempStatus;
        
        #line default
        #line hidden
        
        
        #line 537 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock sc;
        
        #line default
        #line hidden
        
        
        #line 540 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PiPiWPFControl.SwitchImage shortCircuitStatus;
        
        #line default
        #line hidden
        
        
        #line 548 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PiPiWPFControl.ImageButton battSOCButton;
        
        #line default
        #line hidden
        
        
        #line 566 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.UniformGrid batterySocGrid;
        
        #line default
        #line hidden
        
        
        #line 573 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PiPiWPFControl.ImageButton userRecordButton;
        
        #line default
        #line hidden
        
        
        #line 580 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView deviceAttrListView;
        
        #line default
        #line hidden
        
        
        #line 659 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PiPiWPFControl.ImageButton firmwareButton;
        
        #line default
        #line hidden
        
        
        #line 667 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PiPiWPFControl.ImageButton saveLogButton;
        
        #line default
        #line hidden
        
        
        #line 673 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox saveLogText;
        
        #line default
        #line hidden
        
        
        #line 683 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PiPiWPFControl.ImageButton settingButton;
        
        #line default
        #line hidden
        
        
        #line 690 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal PiPiWPFControl.ImageButton closeButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GTEDiagnostic;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.statusValue = ((System.Windows.Controls.Primitives.StatusBarItem)(target));
            return;
            case 2:
            this.portStatusValue = ((System.Windows.Controls.Primitives.StatusBarItem)(target));
            return;
            case 3:
            this.baudRateStatusValue = ((System.Windows.Controls.Primitives.StatusBarItem)(target));
            return;
            case 4:
            this.versionValue = ((System.Windows.Controls.Primitives.StatusBarItem)(target));
            return;
            case 5:
            this.refreshTimeStatusSeparator = ((System.Windows.Controls.Separator)(target));
            return;
            case 6:
            this.refreshTimeStatusText = ((System.Windows.Controls.Primitives.StatusBarItem)(target));
            return;
            case 7:
            this.refreshTimeStatusValue = ((System.Windows.Controls.Primitives.StatusBarItem)(target));
            return;
            case 8:
            this.isAutoLogS = ((System.Windows.Controls.Separator)(target));
            return;
            case 9:
            this.isAutoLogText = ((System.Windows.Controls.Primitives.StatusBarItem)(target));
            return;
            case 10:
            this.isAutoLogValue = ((System.Windows.Controls.Primitives.StatusBarItem)(target));
            return;
            case 11:
            this.logoImage = ((System.Windows.Controls.Image)(target));
            
            #line 69 "..\..\MainWindow.xaml"
            this.logoImage.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.logoImage_MouseDown);
            
            #line default
            #line hidden
            return;
            case 12:
            this.infoTabControl = ((System.Windows.Controls.TabControl)(target));
            
            #line 72 "..\..\MainWindow.xaml"
            this.infoTabControl.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(this.infoTabControl_PreviewMouseDown);
            
            #line default
            #line hidden
            return;
            case 13:
            this.diagnosticButton = ((PiPiWPFControl.ImageButton)(target));
            return;
            case 14:
            this.sohProgressBar = ((System.Windows.Controls.ProgressBar)(target));
            return;
            case 15:
            this.socProgressBar = ((System.Windows.Controls.ProgressBar)(target));
            return;
            case 16:
            this.currentPath = ((System.Windows.Shapes.Path)(target));
            return;
            case 17:
            this.voltagePath = ((System.Windows.Shapes.Path)(target));
            return;
            case 18:
            this.tempProgressBar = ((System.Windows.Controls.ProgressBar)(target));
            return;
            case 19:
            this.sOHText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.sOCText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 21:
            this.currentText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 22:
            this.voltageText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 23:
            this.tempText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 24:
            this.deviceNameText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 25:
            this.manufactureDateText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 26:
            this.barcodeText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 27:
            this.designVoltageText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 28:
            this.designCapacityText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 29:
            this.remainCapacityText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 30:
            this.fCCText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 31:
            this.cycleCountedText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 32:
            this.rtcResetButton = ((System.Windows.Controls.Button)(target));
            
            #line 448 "..\..\MainWindow.xaml"
            this.rtcResetButton.Click += new System.Windows.RoutedEventHandler(this.rtcResetButton_Click);
            
            #line default
            #line hidden
            return;
            case 33:
            this.rtcCountedText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 34:
            this.uvd = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 35:
            this.underVoltStatus = ((PiPiWPFControl.SwitchImage)(target));
            return;
            case 36:
            this.ovc = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 37:
            this.overVoltStatus = ((PiPiWPFControl.SwitchImage)(target));
            return;
            case 38:
            this.ocd = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 39:
            this.overCurrDischargedStatus = ((PiPiWPFControl.SwitchImage)(target));
            return;
            case 40:
            this.occ = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 41:
            this.overCurrChargedStatus = ((PiPiWPFControl.SwitchImage)(target));
            return;
            case 42:
            this.ot = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 43:
            this.overTempStatus = ((PiPiWPFControl.SwitchImage)(target));
            return;
            case 44:
            this.ut = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 45:
            this.underTempStatus = ((PiPiWPFControl.SwitchImage)(target));
            return;
            case 46:
            this.sc = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 47:
            this.shortCircuitStatus = ((PiPiWPFControl.SwitchImage)(target));
            return;
            case 48:
            this.battSOCButton = ((PiPiWPFControl.ImageButton)(target));
            return;
            case 49:
            this.batterySocGrid = ((System.Windows.Controls.Primitives.UniformGrid)(target));
            return;
            case 50:
            this.userRecordButton = ((PiPiWPFControl.ImageButton)(target));
            return;
            case 51:
            this.deviceAttrListView = ((System.Windows.Controls.ListView)(target));
            return;
            case 52:
            this.firmwareButton = ((PiPiWPFControl.ImageButton)(target));
            return;
            case 53:
            this.saveLogButton = ((PiPiWPFControl.ImageButton)(target));
            return;
            case 54:
            this.saveLogText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 55:
            this.settingButton = ((PiPiWPFControl.ImageButton)(target));
            return;
            case 56:
            this.closeButton = ((PiPiWPFControl.ImageButton)(target));
            
            #line 692 "..\..\MainWindow.xaml"
            this.closeButton.MouseUp += new System.Windows.Input.MouseButtonEventHandler(this.CloseAP_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

