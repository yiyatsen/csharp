﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GTEDiagnostic
{
    /// <summary>
    /// CellProgressBar.xaml 的互動邏輯
    /// </summary>
    public partial class CellProgressBar : UserControl
    {
        public CellProgressBar()
        {
            InitializeComponent();
        }

        public string Title
        {
            get { return this.titleText.Text; }
            set { this.titleText.Text = "-" + value + "-"; }
        }

        public string VoltageText
        {
            get { return this.valueText.Text; }
            set { this.valueText.Text = value; }
        }

        public double VoltageValue
        {
            get { return this.progressBar.Value; }
            set { this.progressBar.Value = value; }
        }

        public double CanvasLeft
        {
            set { Canvas.SetLeft(this.progressBar, value); }
        }
        public double CanvasTop
        {
            set { Canvas.SetTop(this.progressBar, value); }
        }
        public double ProgressBarWidth
        {
            get { return this.progressBar.Width; }
            set { this.progressBar.Width = value; }
        }
        public double ProgressBarHeight
        {
            get { return this.progressBar.Height; }
            set { this.progressBar.Height = value; }
        }
    }
}
