﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PiPiWPFControl
{
    /// <summary>
    /// SwitchImage.xaml 的互動邏輯
    /// </summary>
    public partial class SwitchImage : UserControl
    {
        private bool mIsSwitchOn;

        public SwitchImage()
        {
            InitializeComponent();
        }

        public bool IsSwitchOn
        {
            get { return mIsSwitchOn; }
            set
            {
                mIsSwitchOn = value;
                if (mIsSwitchOn)
                {
                    SetValue(ImageProperty, ImageOn);
                }
                else
                {
                    SetValue(ImageProperty, ImageOff);
                }

            }
        }

        public ImageSource ImageOn
        {
            get;
            set;
        }

        public ImageSource ImageOff
        {
            get;
            set;
        }

        public ImageSource ImageDefault
        {
            get { return (ImageSource)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Image.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageProperty =
            DependencyProperty.Register("ImageDefault", typeof(ImageSource), typeof(SwitchImage), new UIPropertyMetadata(null));

        public double ImageWidth
        {
            get { return (double)GetValue(ImageWidthProperty); }
            set { SetValue(ImageWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ImageWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageWidthProperty =
            DependencyProperty.Register("ImageWidth", typeof(double), typeof(SwitchImage), new UIPropertyMetadata(16d));

        public double ImageHeight
        {
            get { return (double)GetValue(ImageHeightProperty); }
            set { SetValue(ImageHeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ImageHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageHeightProperty =
            DependencyProperty.Register("ImageHeight", typeof(double), typeof(SwitchImage), new UIPropertyMetadata(16d));

    }
}
