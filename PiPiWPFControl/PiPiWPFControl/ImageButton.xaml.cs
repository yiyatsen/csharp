﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PiPiWPFControl
{
    /// <summary>
    /// UserControl1.xaml 的互動邏輯
    /// </summary>
    public partial class ImageButton : UserControl
    {
        public ImageButton()
        {
            InitializeComponent();
        }

        public bool IsMouseHand
        {
            get;
            set;
        }

        public bool IsMouseStop
        {
            get;
            set;
        }

        public ImageSource Image
        {
            get { return (ImageSource)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Image.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageProperty =
            DependencyProperty.Register("Image", typeof(ImageSource), typeof(ImageButton), new UIPropertyMetadata(null));

        public double ImageWidth
        {
            get { return (double)GetValue(ImageWidthProperty); }
            set { SetValue(ImageWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ImageWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageWidthProperty =
            DependencyProperty.Register("ImageWidth", typeof(double), typeof(ImageButton), new UIPropertyMetadata(16d));

        public double ImageHeight
        {
            get { return (double)GetValue(ImageHeightProperty); }
            set { SetValue(ImageHeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ImageHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageHeightProperty =
            DependencyProperty.Register("ImageHeight", typeof(double), typeof(ImageButton), new UIPropertyMetadata(16d));


        public ImageSource ImageMouseEneter
        {
            get;
            set;
        }

        public ImageSource ImageMouseDown
        {
            get;
            set;
        }

        public ImageSource ImageDefault
        {
            get;
            set;
        }

        private void image_MouseEnter(object sender, MouseEventArgs e)
        {
            if (!IsMouseStop)
            {
                if (ImageMouseEneter != null)
                {
                    SetValue(ImageProperty, ImageMouseEneter);
                }
                if (IsMouseHand)
                {
                        Mouse.OverrideCursor = System.Windows.Input.Cursors.Hand;
                }
            }
            else
            {
                Mouse.OverrideCursor = System.Windows.Input.Cursors.No;
            }
        }

        private void image_MouseLeave(object sender, MouseEventArgs e)
        {
            if (ImageDefault != null)
            {
                SetValue(ImageProperty, ImageDefault);
            }
            if (Mouse.OverrideCursor == System.Windows.Input.Cursors.Hand)
            {
                Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!IsMouseStop)
            {
                if (ImageMouseDown != null)
                {
                    SetValue(ImageProperty, ImageMouseDown);
                }
            }
        }

        private void image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (ImageDefault != null)
            {
                SetValue(ImageProperty, ImageDefault);
            }
        }
    }
}
