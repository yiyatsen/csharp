{
  "Barcode": "1217230001",
  "PCGMTDateTime": "2017/08/18 06:46:04",
  "RTCDateTime": "2017/08/18 00:12:47",
  "RTCResetDateTime": "2017/07/11 01:00:55",
  "MaxVoltage": 4079,
  "MinVoltage": 4071,
  "VoltageDifference": 8,
  "CellsVoltage": [
    4076,
    4079,
    4077,
    4077,
    4074,
    4078,
    4077,
    4077,
    4077,
    4077,
    4076,
    4079,
    4078,
    4076,
    4073,
    4076,
    4072,
    4073,
    4072,
    4071
  ],
  "Attribute": [
    {
      "IsReadOnly": true,
      "Item": "EDV",
      "DefaultValue": "0",
      "RealValue": "65535",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "DC",
      "DefaultValue": "0",
      "RealValue": "34000",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "TapperCurrent",
      "DefaultValue": "0",
      "RealValue": "65535",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "FCC",
      "DefaultValue": "0",
      "RealValue": "35553",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "CycleCount",
      "DefaultValue": "0",
      "RealValue": "9",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "RC",
      "DefaultValue": ">0",
      "RealValue": "31642",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "DV",
      "DefaultValue": "0",
      "RealValue": "72000",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "DeviceName",
      "DefaultValue": "None",
      "RealValue": "CT7235LG",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SN",
      "DefaultValue": "65535>SN>0",
      "RealValue": "1",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "MCUVer",
      "DefaultValue": "None",
      "RealValue": "MPM_CT-72-35_M36-V1.00-Rev01-Beta-02",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "ManufactureDate",
      "DefaultValue": "None",
      "RealValue": "2017/06/08",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1REV",
      "DefaultValue": "None",
      "RealValue": "E01",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2REV",
      "DefaultValue": "None",
      "RealValue": "E01",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1FN",
      "DefaultValue": "G2E_20S",
      "RealValue": "G2E_20S",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2FN",
      "DefaultValue": "PANA-PF",
      "RealValue": "LG-M36 ",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1PN",
      "DefaultValue": "7238",
      "RealValue": "7235",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2PN",
      "DefaultValue": "",
      "RealValue": "",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "SCTH",
      "DefaultValue": "0",
      "RealValue": "583.3",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SCDelay",
      "DefaultValue": "0",
      "RealValue": "8",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OC",
      "DefaultValue": "0",
      "RealValue": "-75",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OCDTIIME",
      "DefaultValue": "0",
      "RealValue": "3024",
      "Result": false
    }
  ]
}