{
  "Barcode": "2216030101",
  "PCGMTDateTime": "2017/08/31 06:16:09",
  "RTCDateTime": "2017/00/28 04:59:48",
  "RTCResetDateTime": "2017/00/28 01:00:06",
  "MaxVoltage": 4055,
  "MinVoltage": 4046,
  "VoltageDifference": 9,
  "CellsVoltage": [
    4054,
    4055,
    4055,
    4054,
    4052,
    4051,
    4050,
    4051,
    4050,
    4051,
    4046,
    4052,
    4051,
    4051,
    4050,
    4050,
    4049,
    4049,
    4047,
    4051
  ],
  "Attribute": [
    {
      "IsReadOnly": true,
      "Item": "EDV",
      "DefaultValue": "65535",
      "RealValue": "65535",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "DC",
      "DefaultValue": "32000",
      "RealValue": "30000",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "TapperCurrent",
      "DefaultValue": "65535",
      "RealValue": "98369535",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "FCC",
      "DefaultValue": "32000",
      "RealValue": "32000",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "CycleCount",
      "DefaultValue": "0",
      "RealValue": "0",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "RC",
      "DefaultValue": ">0",
      "RealValue": "28799",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "DV",
      "DefaultValue": "72000",
      "RealValue": "72000",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "DeviceName",
      "DefaultValue": "CT7235BM",
      "RealValue": "GV7235BM",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SN",
      "DefaultValue": "65535>SN>0",
      "RealValue": "101",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "MCUVer",
      "DefaultValue": "MPM_CT-72-35-V1.00-Rev02",
      "RealValue": "MPM_GV-72-35-V1.00-Rev02-Beta-07",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "ManufactureDate",
      "DefaultValue": "2017/08/03",
      "RealValue": "2016/01/21",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1REV",
      "DefaultValue": "E01",
      "RealValue": "E01",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2REV",
      "DefaultValue": "E01",
      "RealValue": "E01",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1FN",
      "DefaultValue": "G2E_20S",
      "RealValue": "G2E_20S",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2FN",
      "DefaultValue": "PANA-BM",
      "RealValue": "PANA-BM",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1PN",
      "DefaultValue": "7235",
      "RealValue": "7235",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2PN",
      "DefaultValue": "",
      "RealValue": "",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "SCTH",
      "DefaultValue": "583.3",
      "RealValue": "700",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SCDelay",
      "DefaultValue": "8",
      "RealValue": "8",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "OC",
      "DefaultValue": "-75",
      "RealValue": "-110",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OCDTIIME",
      "DefaultValue": "3024",
      "RealValue": "7112",
      "Result": false
    }
  ]
}