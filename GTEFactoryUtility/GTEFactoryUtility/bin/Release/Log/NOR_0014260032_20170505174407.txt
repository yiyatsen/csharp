{
  "Barcode": "0014260032",
  "PCGMTDateTime": "2017/05/05 09:44:07",
  "RTCDateTime": "2017/05/05 17:09:02",
  "RTCResetDateTime": "2014/04/25 14:04:01",
  "MaxVoltage": 3588,
  "MinVoltage": 3581,
  "VoltageDifference": 7,
  "CellsVoltage": [
    3588,
    3588,
    3585,
    3585,
    3585,
    3584,
    3586,
    3581,
    3586,
    3586,
    3585,
    3585,
    3583,
    3583
  ],
  "Attribute": [
    {
      "IsReadOnly": true,
      "Item": "EDV",
      "DefaultValue": "0",
      "RealValue": "2736",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "DC",
      "DefaultValue": "0",
      "RealValue": "40000",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "TapperCurrent",
      "DefaultValue": "0",
      "RealValue": "1500",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "FCC",
      "DefaultValue": "0",
      "RealValue": "41202",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "CycleCount",
      "DefaultValue": "0",
      "RealValue": "57",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "RC",
      "DefaultValue": ">0",
      "RealValue": "20188",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "DV",
      "DefaultValue": "0",
      "RealValue": "48000",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "DeviceName",
      "DefaultValue": "None",
      "RealValue": "YS4840A",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SN",
      "DefaultValue": "65535>SN>0",
      "RealValue": "32",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "MCUVer",
      "DefaultValue": "None",
      "RealValue": "MPM2.02.6.42",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "ManufactureDate",
      "DefaultValue": "None",
      "RealValue": "2014/04/01",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1REV",
      "DefaultValue": "None",
      "RealValue": "B03",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2REV",
      "DefaultValue": "None",
      "RealValue": "B03",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1FN",
      "DefaultValue": "G2E_20S",
      "RealValue": "G2B14S",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2FN",
      "DefaultValue": "PANA-PF",
      "RealValue": "G2B14S",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1PN",
      "DefaultValue": "7238",
      "RealValue": "G2BL",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2PN",
      "DefaultValue": "",
      "RealValue": "G2BH",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SCTH",
      "DefaultValue": "0",
      "RealValue": "390.2",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SCDelay",
      "DefaultValue": "0",
      "RealValue": "464",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OC",
      "DefaultValue": "0",
      "RealValue": "-105.7",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OCDTIIME",
      "DefaultValue": "0",
      "RealValue": "10200",
      "Result": false
    }
  ]
}