{
  "Barcode": "2216010001",
  "PCGMTDateTime": "2016/09/07 03:03:23",
  "RTCDateTime": "2016/09/07 03:10:12",
  "RTCResetDateTime": "2016/08/10 05:51:38",
  "MaxVoltage": 3881,
  "MinVoltage": 3867,
  "VoltageDifference": 14,
  "CellsVoltage": [
    3875,
    3879,
    3879,
    3880,
    3876,
    3878,
    3876,
    3878,
    3876,
    3881,
    3867,
    3875,
    3874,
    3875,
    3874,
    3878,
    3876,
    3876,
    3876,
    3879
  ],
  "Attribute": [
    {
      "IsReadOnly": true,
      "Item": "EDV",
      "DefaultValue": "0",
      "RealValue": "65535",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "DC",
      "DefaultValue": "0",
      "RealValue": "53000",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "TapperCurrent",
      "DefaultValue": "0",
      "RealValue": "0",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "FCC",
      "DefaultValue": "0",
      "RealValue": "53000",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "CycleCount",
      "DefaultValue": "0",
      "RealValue": "0",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "RC",
      "DefaultValue": ">0",
      "RealValue": "32860",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "DV",
      "DefaultValue": "0",
      "RealValue": "72000",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "DeviceName",
      "DefaultValue": "None",
      "RealValue": "GV7253",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SN",
      "DefaultValue": "65535>SN>0",
      "RealValue": "1",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "MCUVer",
      "DefaultValue": "None",
      "RealValue": "GV-72-53-V1.00-Rev01A",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "ManufactureDate",
      "DefaultValue": "None",
      "RealValue": "2016/01/07",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1REV",
      "DefaultValue": "None",
      "RealValue": "E01",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2REV",
      "DefaultValue": "None",
      "RealValue": "E01",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1FN",
      "DefaultValue": "G2E_20S",
      "RealValue": "G2E_20S",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2FN",
      "DefaultValue": "PANA-PF",
      "RealValue": "SDI-22P",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1PN",
      "DefaultValue": "7238",
      "RealValue": "7238",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2PN",
      "DefaultValue": "",
      "RealValue": "",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "SCTH",
      "DefaultValue": "0",
      "RealValue": "700",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SCDelay",
      "DefaultValue": "0",
      "RealValue": "8",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OC",
      "DefaultValue": "0",
      "RealValue": "-110",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OCDTIIME",
      "DefaultValue": "0",
      "RealValue": "7112",
      "Result": false
    }
  ]
}