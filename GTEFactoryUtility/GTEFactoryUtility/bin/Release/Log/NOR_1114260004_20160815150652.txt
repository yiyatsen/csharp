{
  "Barcode": "1114260004",
  "PCGMTDateTime": "2016/08/15 07:06:52",
  "RTCDateTime": "2016/08/15 07:03:39",
  "RTCResetDateTime": "2016/05/11 08:58:42",
  "MaxVoltage": 3767,
  "MinVoltage": 3757,
  "VoltageDifference": 10,
  "CellsVoltage": [
    3761,
    3762,
    3761,
    3761,
    3760,
    3761,
    3761,
    3757,
    3766,
    3767,
    3766,
    3766,
    3766,
    3766
  ],
  "Attribute": [
    {
      "IsReadOnly": true,
      "Item": "EDV",
      "DefaultValue": "0",
      "RealValue": "2736",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "DC",
      "DefaultValue": "0",
      "RealValue": "40000",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "TapperCurrent",
      "DefaultValue": "0",
      "RealValue": "1500",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "FCC",
      "DefaultValue": "0",
      "RealValue": "39629",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "CycleCount",
      "DefaultValue": "0",
      "RealValue": "8",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "RC",
      "DefaultValue": ">0",
      "RealValue": "29067",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "DV",
      "DefaultValue": "0",
      "RealValue": "48000",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "DeviceName",
      "DefaultValue": "None",
      "RealValue": "4840AH1",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SN",
      "DefaultValue": "65535>SN>0",
      "RealValue": "4",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "MCUVer",
      "DefaultValue": "None",
      "RealValue": "MPM2.02.6.42",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "ManufactureDate",
      "DefaultValue": "None",
      "RealValue": "2014/06/26",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1REV",
      "DefaultValue": "None",
      "RealValue": "B03",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2REV",
      "DefaultValue": "None",
      "RealValue": "B03",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1FN",
      "DefaultValue": "G2E_20S",
      "RealValue": "G2B14S",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2FN",
      "DefaultValue": "PANA-PF",
      "RealValue": "G2B14S",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1PN",
      "DefaultValue": "7238",
      "RealValue": "G2BL",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2PN",
      "DefaultValue": "",
      "RealValue": "G2BH",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SCTH",
      "DefaultValue": "0",
      "RealValue": "390.2",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SCDelay",
      "DefaultValue": "0",
      "RealValue": "464",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OC",
      "DefaultValue": "0",
      "RealValue": "-97.6",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OCDTIIME",
      "DefaultValue": "0",
      "RealValue": "10200",
      "Result": false
    }
  ]
}