{
  "Barcode": "0014260032",
  "PCGMTDateTime": "2017/10/17 03:58:43",
  "RTCDateTime": "2017/09/16 03:55:25",
  "RTCResetDateTime": "2017/05/30 05:08:43",
  "MaxVoltage": 3662,
  "MinVoltage": 3654,
  "VoltageDifference": 8,
  "CellsVoltage": [
    3659,
    3662,
    3660,
    3659,
    3659,
    3658,
    3658,
    3654,
    3659,
    3659,
    3658,
    3659,
    3657,
    3658
  ],
  "Attribute": [
    {
      "IsReadOnly": true,
      "Item": "EDV",
      "DefaultValue": "0",
      "RealValue": "2736",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "DC",
      "DefaultValue": "0",
      "RealValue": "40000",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "TapperCurrent",
      "DefaultValue": "0",
      "RealValue": "0",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "FCC",
      "DefaultValue": "0",
      "RealValue": "40000",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "CycleCount",
      "DefaultValue": "0",
      "RealValue": "0",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "RC",
      "DefaultValue": ">0",
      "RealValue": "13600",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "DV",
      "DefaultValue": "0",
      "RealValue": "48000",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "DeviceName",
      "DefaultValue": "None",
      "RealValue": "YS4840A",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SN",
      "DefaultValue": "65535>SN>0",
      "RealValue": "32",
      "Result": true
    },
    {
      "IsReadOnly": true,
      "Item": "MCUVer",
      "DefaultValue": "None",
      "RealValue": "V02.02.6.45F",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "ManufactureDate",
      "DefaultValue": "None",
      "RealValue": "2014/04/01",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1REV",
      "DefaultValue": "None",
      "RealValue": "B03",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2REV",
      "DefaultValue": "None",
      "RealValue": "B03",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1FN",
      "DefaultValue": "None",
      "RealValue": "G2B14S",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2FN",
      "DefaultValue": "None",
      "RealValue": "G2B14S",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ1PN",
      "DefaultValue": "None",
      "RealValue": "G2BL",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OZ2PN",
      "DefaultValue": "None",
      "RealValue": "G2BH",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SCTH",
      "DefaultValue": "0",
      "RealValue": "390.2",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "SCDelay",
      "DefaultValue": "0",
      "RealValue": "464",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OC",
      "DefaultValue": "0",
      "RealValue": "-105.7",
      "Result": false
    },
    {
      "IsReadOnly": true,
      "Item": "OCDTIIME",
      "DefaultValue": "0",
      "RealValue": "10200",
      "Result": false
    }
  ]
}