﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEFactoryUtility.Parameter
{
    public class FactoryParameter
    {
        public const string APPSETTINGFILEPATH = "AppSetting.ap";
        public const int SCANPORTTIME = 5;
        public const int REFRESHVOLTAGETTIME = 3;
        public const string CMD_ENGMODE = "ENG";
        public const string CMD_OZMODE = "OZ";
    }
}
