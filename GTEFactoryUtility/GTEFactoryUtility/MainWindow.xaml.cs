﻿using GTE.Command;
using GTE.Encryption;
using GTE.Message;
using GTEBatteryLib;
using GTEFactoryConfig;
using GTEFactoryConfig.Config;
using GTEFactoryConfig.InfoModel;
using GTEFactoryConfig.Setting;
using GTEFactoryUtility.Parameter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEFactoryUtility
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        public static bool ISBETA = true;

        #region WIN 32
        [DllImport("user32.dll")]
        private extern static int SetWindowLong(IntPtr hwnd, int index, int value);
        [DllImport("user32.dll")]
        private extern static int GetWindowLong(IntPtr hwnd, int index);

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x00080000;

        void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            WindowInteropHelper wih = new WindowInteropHelper(this);
            int style = GetWindowLong(wih.Handle, GWL_STYLE);
            SetWindowLong(wih.Handle, GWL_STYLE, style & ~WS_SYSMENU);
        }
        #endregion

        #region Property
        private BatteryAPI mBattery { get; set; }

        private AppSetting mAppSetting; // 預設之連線設定及預設之電池屬性值
        private FactoryDeviceAdapter mDeviceAdapter;

        private string mAppPath; //視窗程式之路徑
        private bool mEnableAppClose; // 是否可以關閉視窗程式
        private int mModeCode = 2; // 0 for NOR, 1 For ENG, 2 for OZ, 3 of Bizcon, 4 of DEV, 5 of quickly save
        private int mSaveAction = 0;

        private DispatcherTimer mPortScanTimer; // 監聽Port狀態之定時器
        private DispatcherTimer mRefreshDateTimeTimer; // 更新時間狀態之定時器
        private DispatcherTimer mReceivedVoltageTimer; // 取得電池電壓之定時器

        public delegate void MessageHandler(object sender, CallbackInfo e);
        #endregion


        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();//引用stopwatch物件

        public MainWindow()
        {
            SourceInitialized += MainWindow_SourceInitialized;

            InitializeComponent();

            InitializeAppMode();
            InitializeAppSetting();
            InitializeScanPort();
            InitializeDateTime();

            InitializeDevice();
            InitializeUI();
            LoadLanguage();
        }

        private void LoadLanguage()
        {
            if (mModeCode != 3)
            {
                ResourceDictionary langRd = null;
                try
                {
                    if (mModeCode == 5)
                    {
                        langRd =
                        Application.LoadComponent(
                        new Uri(@"Cultures\" + "DefaultLanguage" + ".xaml", UriKind.Relative))
                        as ResourceDictionary;
                    }
                    else
                    {
                        langRd =
                        Application.LoadComponent(
                        new Uri(@"Cultures\" + "zh-TW" + ".xaml", UriKind.Relative))
                        as ResourceDictionary;
                    }
                }
                catch
                {
                }
                if (langRd != null)
                {
                    if (this.Resources.MergedDictionaries.Count > 0)
                    {
                        this.Resources.MergedDictionaries.Clear();
                    }
                    this.Resources.MergedDictionaries.Add(langRd);
                }
            }
        }

        /// <summary>
        /// 初始化UI數值
        /// </summary>
        private void InitializeUI()
        {
            SetBaudRateToComboBox();
            SetVoltageDifferenceLimitToTextBox();

            Version CurrentVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            string ver = CurrentVersion.ToString();
            if (ISBETA)
            {
                versionValue.Content = ver + " (Beta)";
            }
            else
            {
                versionValue.Content = ver;
            }
        }

        #region APP Setting
        /// <summary>
        /// 啟動關閉視窗之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseAP_Click(object sender, RoutedEventArgs e)
        {
            SaveAppSetting();

            mEnableAppClose = true;
            this.Close();
        }
        /// <summary>
        /// 視窗準備關閉之事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (!mEnableAppClose) // 決定可否關閉視窗之條件
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// 初始化視窗程式需要之設定檔
        /// </summary>
        private void InitializeAppSetting()
        {
            mAppPath = System.AppDomain.CurrentDomain.BaseDirectory;

            string appSettingFilePath = mAppPath + FactoryParameter.APPSETTINGFILEPATH;
            try
            {
                if (File.Exists(appSettingFilePath))
                {
                    using (StreamReader file = File.OpenText(appSettingFilePath))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        mAppSetting = (AppSetting)serializer.Deserialize(file, typeof(AppSetting));
                    }
                }
            }
            catch{ }

            if (mAppSetting == null)
            {
                mAppSetting = new AppSetting();

                string[] portList = System.IO.Ports.SerialPort.GetPortNames();
                if (portList != null && portList.Length > 0)
                {
                    mAppSetting.Port = portList[0];
                }

                mAppSetting.BaudRate = mAppSetting.BaudRateList[0];
                mAppSetting.VoltageDifferenceLimit = 50;
                mAppSetting.EnableRecordDeviceInfo = false;

                SaveAppSetting();
            }
        }
        /// <summary>
        /// 儲存AppSetting資訊至檔案(AppSetting.ap)內
        /// </summary>
        /// <returns></returns>
        private bool SaveAppSetting()
        {
            bool isSuccess = false;
            string appSettingFilePath = mAppPath + FactoryParameter.APPSETTINGFILEPATH;

            if (mAppSetting != null)
            {
                using (FileStream fs = File.Open(appSettingFilePath, FileMode.Create))
                using (StreamWriter sw = new StreamWriter(fs))
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    jw.Formatting = Formatting.Indented;

                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(jw, mAppSetting);
                    isSuccess = true;
                }
            }

            return isSuccess;
        }
        #endregion

        #region Engine Mode Setting
        /// <summary>
        /// 初始化是否為工程模式
        /// </summary>
        private void InitializeAppMode()
        {
            if (mModeCode != 5)
            {
                string[] arrArguments = System.Environment.GetCommandLineArgs();
                if (arrArguments != null)
                {
                    foreach (string arg in arrArguments)
                    {
                        if (arg.Equals(FactoryParameter.CMD_ENGMODE))
                        {
                            mModeCode = 1;
                            break;
                        }
                        else if (arg.Equals(FactoryParameter.CMD_OZMODE))
                        {
                            mModeCode = 2;
                            break;
                        }
                    }
                }
            }

            RefreshENGModeUI();
        }
        /// <summary>
        /// 更新模式UI
        /// </summary>
        private void RefreshENGModeUI()
        {
            if (mModeCode == 0)
            {
                barcodeTextBox.IsEnabled = false;
                barcodeButton.IsEnabled = false;
                dvLimitTextBox.IsReadOnly = true;
                saveVoltageDifferenceLimitValue.Visibility = System.Windows.Visibility.Hidden;
                saveDefaultValue.Visibility = System.Windows.Visibility.Hidden;
                writeToBattery.Visibility = System.Windows.Visibility.Hidden;

                modeValue.Content = "Normal Mode";
            }
            else if (mModeCode == 1)
            {
                modeValue.Content = "Engineer Mode";
            }
            else if (mModeCode == 2)
            {
                modeValue.Content = "OZ Mode";
                Page5.IsSelected = true;
            }
            else if (mModeCode == 3)
            {
                modeValue.Content = "Produce mode";
            }
            else if (mModeCode == 5)
            {
                winodwItem.Title = "GTE Diagnostic Lite";
                baudRateList.Visibility = System.Windows.Visibility.Collapsed;
                modeValue.Content = "Quickly Save";
            }
        }
        #endregion

        #region Scan Port
        /// <summary>
        /// 初始化ComPort監聽時間器
        /// </summary>
        private void InitializeScanPort()
        {
            SetSerialPortToComboBox(true);
            mPortScanTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(FactoryParameter.SCANPORTTIME) };
            mPortScanTimer.Tick += PortScanTimer_Tick;
            mPortScanTimer.Start();
        }
        /// <summary>
        /// 啟動監聽ComPort狀態之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PortScanTimer_Tick(object sender, EventArgs e)
        {
            SetSerialPortToComboBox(false);
        }
        /// <summary>
        /// 設定ComPort列表至ComboBox
        /// </summary>
        private void SetSerialPortToComboBox(bool isFirstTime)
        {
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();
            if (ports != null && ports.Length > 0)
            {
                bool isChanged = false;
                foreach (object data in portList.Items)
                {
                    if (!ports.Contains(data.ToString()))
                    {
                        isChanged = true;
                    }
                }

                if (isChanged || isFirstTime)
                {
                    string port = null;
                    int index = 0;
                    if (portList.SelectedItem != null)
                    {
                        port = portList.SelectedItem.ToString();
                    }

                    portList.Items.Clear();
                    for (int i = 0; i < ports.Length; i++)
                    {
                        if (isFirstTime)
                        {
                            if (ports[i].Equals(mAppSetting.Port))
                            {
                                index = i;
                            }
                        }
                        else
                        {
                            if (ports[i].Equals(port))
                            {
                                index = i;
                            }
                        }

                        portList.Items.Add(ports[i]);
                    }
                    portList.SelectedIndex = index;
                }
            }
        }
        #endregion

        #region Baud Rate
        /// <summary>
        /// 設定BaudRate列表至ComboBox
        /// </summary>
        private void SetBaudRateToComboBox()
        {
            for (int i = 0; i < mAppSetting.BaudRateList.Length; i++)
            {
                baudRateList.Items.Add(mAppSetting.BaudRateList[i]);
                if (mAppSetting.BaudRate == mAppSetting.BaudRateList[i])
                {
                    baudRateList.SelectedIndex = i;
                }
            }
        }
        #endregion

        #region Connect/Disconnect
        /// <summary>
        /// 與電池連線電池
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="port"></param>
        /// <param name="baudRate"></param>
        private void ConnectBattery(object sender, string port, int baudRate)
        {
            CallbackInfo callbackData = new CallbackInfo()
            {
                Type = CallbackType.ConnectDevice,
                Status = CallbackStatus.Fail,
                Data = null
            };

            if (port != null)
            {
                if (mModeCode == 5)
                {
                    for (int i =0; i < mAppSetting.BaudRateList.Length; i++)
                    {
                        bool isSuccess = mBattery.Connect(port, mAppSetting.BaudRateList[i]);
                        if (isSuccess)
                        {
                            callbackData.Status = CallbackStatus.Success;

                            SetDeviceAdapter();
                            break;
                        }
                    }
                }
                else
                {
                    bool isSuccess = mBattery.Connect(port, baudRate);
                    if (isSuccess)
                    {
                        callbackData.Status = CallbackStatus.Success;

                        SetDeviceAdapter();
                    }
                }
            }

            Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { sender, callbackData });
        }
        /// <summary>
        /// 與電池溝通之物件斷線
        /// </summary>
        private void DisconnectBattery()
        {
            infoTabControl.Visibility = System.Windows.Visibility.Hidden;
            SetVoltageSwitchButton(readVoltageButton, false);


            barcodeTextBox.Text = "";
            barcodeSetStatusLabel.Content = "---";
            connectButton.Content = connectButton.FindResource("Connect") as string;
            statusValue.Content = connectButton.FindResource("Disconnect") as string;
            portList.IsEnabled = true;
            baudRateList.IsEnabled = true;
            closeButton.Visibility = Visibility.Visible;

            mBattery.Disconnect();
        }
        /// <summary>
        /// 啟動電池連線之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            ToggleButton button = (sender as ToggleButton);
            if (button.IsChecked.Value)
            {
                if (portList.SelectedItem != null)
                {
                    mAppSetting.Port = portList.SelectedItem.ToString();
                }
                if (baudRateList.SelectedItem != null)
                {
                    mAppSetting.BaudRate = Convert.ToInt32(baudRateList.SelectedItem.ToString());
                }

                Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                button.IsEnabled = false;
                statusValue.Content = button.FindResource("Connecting") as string;

                if (mModeCode == 0)
                {
                    Page1.Visibility = System.Windows.Visibility.Visible;
                    Page2.Visibility = System.Windows.Visibility.Visible;
                    Page3.Visibility = System.Windows.Visibility.Visible;
                    Page4.Visibility = System.Windows.Visibility.Visible;
                    Page5.Visibility = System.Windows.Visibility.Collapsed;
                }
                else if (mModeCode == 1)
                {
                    Page1.Visibility = System.Windows.Visibility.Visible;
                    Page2.Visibility = System.Windows.Visibility.Collapsed;
                    Page3.Visibility = System.Windows.Visibility.Collapsed;
                    Page4.Visibility = System.Windows.Visibility.Collapsed;
                    Page5.Visibility = System.Windows.Visibility.Collapsed;
                }
                else if (mModeCode == 2)
                {
                    Page1.Visibility = System.Windows.Visibility.Collapsed;
                    Page2.Visibility = System.Windows.Visibility.Collapsed;
                    Page3.Visibility = System.Windows.Visibility.Collapsed;
                    Page4.Visibility = System.Windows.Visibility.Collapsed;
                    Page5.Visibility = System.Windows.Visibility.Visible;
                }
                else if (mModeCode == 3)
                {
                    Page1.Visibility = System.Windows.Visibility.Visible;
                    Page2.Visibility = System.Windows.Visibility.Collapsed;
                    Page3.Visibility = System.Windows.Visibility.Collapsed;
                    Page4.Visibility = System.Windows.Visibility.Collapsed;
                    Page5.Visibility = System.Windows.Visibility.Collapsed;
                }
                else if (mModeCode == 4)
                {
                    Page1.Visibility = System.Windows.Visibility.Visible;
                    Page2.Visibility = System.Windows.Visibility.Visible;
                    Page3.Visibility = System.Windows.Visibility.Visible;
                    Page4.Visibility = System.Windows.Visibility.Visible;
                    Page5.Visibility = System.Windows.Visibility.Visible;
                }

                closeButton.Visibility = Visibility.Hidden;

                sw.Reset();//碼表歸零
                sw.Start();//碼表開始計時
                ThreadPool.QueueUserWorkItem(o => ConnectBattery(sender, mAppSetting.Port, mAppSetting.BaudRate));
            }
            else
            {
                DisconnectBattery();
            }
        }
        #endregion

        #region Barcode
        /// <summary>
        /// 設定Battery Barcode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="barcodeText"></param>
        private void SetBarcodeToBattery(object sender, string barcodeText)
        {
            CallbackInfo callbackData = new CallbackInfo()
            {
                Type = CallbackType.SetDeviceBarcode,
                Status = CallbackStatus.Fail,
                Data = barcodeText
            };

            Regex regex = new Regex(@"^\d{10}$");
            bool isInt = regex.IsMatch(barcodeText);

            if (isInt) // 皆為數字且長度符合Barcode長度
            {
                int[] barCodeItem = new int[5];
                barCodeItem[0] = int.Parse(barcodeText.Substring(0, 1)); // 生產商代碼
                barCodeItem[1] = int.Parse(barcodeText.Substring(1, 1)); // 產品商代碼
                barCodeItem[2] = int.Parse(barcodeText.Substring(2, 2)); // 製造年份
                barCodeItem[3] = int.Parse(barcodeText.Substring(4, 2)); // 製造年週別
                barCodeItem[4] = int.Parse(barcodeText.Substring(6, 4)); // 序號

                bool isSuccess = true;
                mDeviceAdapter.SetBatteryEngMode(true);
                isSuccess = mBattery.SetMCUEEPCmd(BatteryEEPAttrCmd.ManufactureInfo, Convert.ToByte((barCodeItem[0] << 4) | barCodeItem[1])); // Set Manufacturer & ProductClass

                int year = 2000 + barCodeItem[2]; //barcode只有兩千年後兩碼
                isSuccess = mBattery.SetMCUEEPCmd(BatteryEEPAttrCmd.ManufactureDate, ISO8601WeekNumToThursday(year, barCodeItem[3])) && isSuccess;// Set Manufacture Date

                isSuccess = mBattery.SetMCUEEPCmd(BatteryEEPAttrCmd.ManufactureWeekOfYear, Convert.ToByte((barCodeItem[3]))) && isSuccess; // Set Manufacture Week Of Year

                isSuccess = mBattery.SetMCUEEPCmd(BatteryEEPAttrCmd.SerialNumber, barCodeItem[4]) && isSuccess; // Set Serial Number
                
                mDeviceAdapter.SetBatteryEngMode(false);
                if (isSuccess)
                {
                    callbackData.Status = CallbackStatus.Success;
                }
                else
                {                    
                    callbackData.Status = CallbackStatus.WriteToDeviceError;

                    object value = mBattery.SendBatteryCmd(BatteryCmd.CMD_BATTERY_NO_GET);
                    string sn = null;
                    if (value != null)
                    {
                        sn = value.ToString();
                    }
                    value = mBattery.SendBatteryCmd(BatteryCmd.CMD_MANUFACTURE_DATE_GET);
                    int[] data = value as int[];
                    if (sn != null && data != null)
                    {
                        int[] setData = ISO8601WeekNumToThursday(year, barCodeItem[3]);
                        if (sn.Equals(barCodeItem[4].ToString()) && data.Length == 3 && setData.Length == 3 && data[0] == setData[0] && data[1] == setData[1] && data[2] == setData[2])
                        {
                            callbackData.Status = CallbackStatus.Success;
                        }
                    }
                }
            }
            else
            {
                callbackData.Status = CallbackStatus.DataFormatError;
            }

            Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { sender, callbackData });
        }
        /// <summary>
        /// 轉換至ISO8601標準的週別日期時間
        /// </summary>
        /// <param name="year"></param>
        /// <param name="weekOfYear"></param>
        /// <returns></returns>
        public int[] ISO8601WeekNumToThursday(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1); //當年分第一天
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;//移動至星期三

            DateTime firstThursday = jan1.AddDays(daysOffset);//當周星期四時間
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            DateTime result = firstThursday.AddDays(weekNum * 7);
            int[] date = new int[3];
            date[0] = result.Year;
            date[1] = result.Month;
            date[2] = result.Day;
            return date;
        }
        /// <summary>
        /// 啟動寫入Barcode資訊之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetBarcode_Click(object sender, RoutedEventArgs e)
        {
            //判斷數值是否正確
            string barcodeText = barcodeTextBox.Text; //UI.text
            if (barcodeText != null && barcodeText != "")
            {
                Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                ((Button)sender).IsEnabled = false;
                ThreadPool.QueueUserWorkItem(o => SetBarcodeToBattery(sender, barcodeText));
            }
            else
            {
                // Warning Dialog
            }

            barcodeTextBox.Focus();
        }
        /// <summary>
        /// 只能輸入數字之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KeyDownBarcode_Click(object sender, KeyEventArgs e)
        {
            if (!((e.Key >= Key.D0 && e.Key <= Key.D9) ||
                (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)))
            {
                e.Handled = true;
            }
        }
        /// <summary>
        /// 全選TextBox之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FocusToBarCodeTextBox(object sender, EventArgs e)
        {
            TextBox tb = (sender as TextBox);
            if (tb != null)
            {
                tb.SelectAll();
            }
        }
        /// <summary>
        /// 防止無法全選TextBox之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectivelyIgnoreMouseButton(object sender, MouseButtonEventArgs e)
        {
            TextBox tb = (sender as TextBox);
            if (tb != null)
            {
                if (!tb.IsKeyboardFocusWithin)
                {
                    e.Handled = true;
                    tb.Focus();
                }
            }
        }
        /// <summary>
        /// 判斷Barcode TextBox是否正確
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BarcodeTextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            if (textBox.Text.Length > 10)
            {
                textBox.Text = textBox.Text.Substring(0, 10);
                textBox.SelectionStart = textBox.Text.Length;
            }
            else
            {
                Regex regex = new Regex(@"^\d{10}$");
                barcodeButton.IsEnabled = regex.IsMatch(textBox.Text);
            }
        }
        #endregion

        #region Battery Method
        /// <summary>
        /// 初始化電池物件
        /// </summary>
        private void InitializeDevice()
        {
            mBattery = new BatteryAPI();
        }

        private void SetDeviceAdapter()
        {
            mDeviceAdapter = null;
            string version = mBattery.GetVersion();
            Regex regex = new Regex(@"^MPM");
            if (regex.IsMatch(version))
            {
                mDeviceAdapter = new FactoryConfig_MPM(mBattery);
            }
            else
            {
                regex = new Regex(@"^V");
                if (regex.IsMatch(version))
                {
                    mDeviceAdapter = new FactoryConfig_MPM(mBattery);
                }
                else
                {
                    mDeviceAdapter = new FactoryConfig_MPS(mBattery);
                }
            }

            if (mDeviceAdapter != null)
            {
                string defaultAttrValue = mDeviceAdapter.SetDefaultAttrValue(mAppSetting.DefaultAttrValue);
                if (defaultAttrValue != null)
                {
                    mAppSetting.DefaultAttrValue = defaultAttrValue;
                    SaveAppSetting();
                }
            }
        }
        #region RTC Method
        /// <summary>
        /// 初始化時間顯示定時器
        /// </summary>
        private void InitializeDateTime()
        {
            mRefreshDateTimeTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(1) };
            mRefreshDateTimeTimer.Tick += RefreshDateTimeTimer_Tick;
            mRefreshDateTimeTimer.Start();
        }
        /// <summary>
        /// 啟動更新時間狀態之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshDateTimeTimer_Tick(object sender, EventArgs e)
        {
            systemTime.Text = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            gmtTime.Text = DateTime.UtcNow.ToString("yyyy/MM/dd HH:mm:ss");
        }
        /// <summary>
        /// 設定電池RTC時間
        /// </summary>
        /// <param name="sender"></param>
        private void SetDeviceRTCDateTime(object sender)
        {
            CallbackInfo callbackData = new CallbackInfo()
            {
                Type = CallbackType.SetDeviceRTCTime,
                Status = CallbackStatus.Fail,
                Data = null
            };

            DateTime dt = DateTime.UtcNow;

            bool isSuccess = true;
            mDeviceAdapter.SetDeviceRTCTime(dt);

            System.Threading.Thread.Sleep(500);

            List<int[]> dtList = new List<int[]>();

            int[] getNewDT = mDeviceAdapter.GetDeviceRTCTime();

            int[] resetDT = mDeviceAdapter.GetDeviceRTCResetTime();

            mDeviceAdapter.ResetMCU();
            if (dt != null && resetDT != null)
            {
                dtList.Add(getNewDT);
                dtList.Add(resetDT);
                try
                {
                    DateTime getCurrentTime = DateTime.Parse(string.Format("{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}", getNewDT[0], getNewDT[1], getNewDT[2], getNewDT[3], getNewDT[4], getNewDT[5]));

                    long btDT = getCurrentTime.Ticks;
                    long gmtMaxDT = dt.AddMinutes(1).Ticks;
                    long gmtMinDT = dt.AddMinutes(-1).Ticks;

                    if (btDT < gmtMaxDT && btDT > gmtMinDT)
                    {
                        callbackData.Status = CallbackStatus.Success;
                        callbackData.Data = dtList;
                    }
                    else
                    {
                        callbackData.Status = CallbackStatus.WriteToDeviceError;
                    }
                }
                catch
                {
                    callbackData.Status = CallbackStatus.WriteToDeviceError;
                }
            }

            Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { sender, callbackData });
        }
        /// <summary>
        /// 取得電池RTC時間
        /// </summary>
        private void GetDeviceRTCDateTime()
        {
            CallbackInfo callbackData = new CallbackInfo()
            {
                Type = CallbackType.GetDeviceRTCTime,
                Status = CallbackStatus.Fail,
                Data = null
            };

            List<int[]> dtList = new List<int[]>();

            int[] dt = mDeviceAdapter.GetDeviceRTCTime();

            int[] resetDT = mDeviceAdapter.GetDeviceRTCResetTime();

            if (dt != null && resetDT != null)
            {
                callbackData.Status = CallbackStatus.Success;

                dtList.Add(dt);
                dtList.Add(resetDT);
            }

            callbackData.Data = dtList;

            Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { null, callbackData });
        }
        /// <summary>
        /// 啟動設定電池RTC時間之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetDeviceRTCDateTime_Click(object sender, EventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
            ((Button)sender).IsEnabled = false;
            UpdateSetRTCLabel.Content = "";
            ThreadPool.QueueUserWorkItem(o => SetDeviceRTCDateTime(sender));
        }
        /// <summary>
        /// 手動更新RTC時間
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void refreshDeviceRTCTime_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
            rtcTime.Text = "";
            rtcResetTime.Text = "";
            ThreadPool.QueueUserWorkItem(o => GetDeviceRTCDateTime());
        }
        #endregion
        #region Voltage Method
        /// <summary>
        /// 設定壓差限制值列表至TextBox
        /// </summary>
        private void SetVoltageDifferenceLimitToTextBox()
        {
            dvLimitTextBox.Text = mAppSetting.VoltageDifferenceLimit.ToString();
        }
        /// <summary>
        /// 啟動設定壓差限制值之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetVoltageDifferenceLimitValue_Click(object sender, RoutedEventArgs e)
        {
            int number = 0;
            bool isSuccess = int.TryParse(dvLimitTextBox.Text, out number);

            if (isSuccess)
            {
                mAppSetting.VoltageDifferenceLimit = number;
                SaveAppSetting();
            }

            dvStatusLabel.Content = "";
        }
        /// <summary>
        /// 啟動開關讀取伏特資訊之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SwitchVoltageInfo_Click(object sender, RoutedEventArgs e)
        {
            ToggleButton button = (sender as ToggleButton);
            SetVoltageSwitchButton(button, button.IsChecked.Value);
        }
        /// <summary>
        /// 設定電壓之UI即啟動電壓更新定時器
        /// </summary>
        /// <param name="button"></param>
        /// <param name="isSwitchOn"></param>
        private void SetVoltageSwitchButton(ToggleButton button, bool isSwitchOn)
        {
            button.IsChecked = isSwitchOn;

            if (isSwitchOn)
            {
                button.Content = button.FindResource("StopReadVoltage") as string;

                if (mReceivedVoltageTimer == null)
                {
                    mReceivedVoltageTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(FactoryParameter.REFRESHVOLTAGETTIME) };
                    mReceivedVoltageTimer.Tick += ReceivedVoltageTimer_Tick;
                }
                mReceivedVoltageTimer.Start();
            }
            else
            {
                if (mReceivedVoltageTimer != null)
                {
                    mReceivedVoltageTimer.Stop();
                }
                button.Content = button.FindResource("ReadVoltage") as string;
                maxVoltageTextBox.Text = "";
                minVoltageTextBox.Text = "";
                dvStatusLabel.Content = "---";
            }
        }
        /// <summary>
        /// 啟動監聽電池電壓狀態之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReceivedVoltageTimer_Tick(object sender, EventArgs e)
        {
            int[] cellVolt = mDeviceAdapter.GetDeviceVoltage();
            if (cellVolt != null)
            {
                int maxVolt = 0;
                int minVolt = 0;

                for (int i = 0; i < cellVolt.Length; i++)
                {
                    if (i == 0)
                    {
                        maxVolt = cellVolt[i];
                        minVolt = cellVolt[i];
                    }
                    else
                    {
                        if (cellVolt[i] > maxVolt)
                        {
                            maxVolt = cellVolt[i];
                        }
                        if (cellVolt[i] < minVolt)
                        {
                            minVolt = cellVolt[i];
                        }
                    }
                }

                maxVoltageTextBox.Text = maxVolt.ToString();
                minVoltageTextBox.Text = minVolt.ToString();

                int vd = maxVolt - minVolt;

                if (vd > 0 && vd < mAppSetting.VoltageDifferenceLimit)
                {
                    if (mSaveAction == 2)
                    {
                        mSaveAction = 3;
                    }

                    dvStatusLabel.Content = "OK";
                }
                else
                {
                    if (mSaveAction > 0)
                    {
                        mSaveAction = 0;
                    }

                    dvStatusLabel.Content = "NG";
                }
                dvStatusLabel.Visibility = System.Windows.Visibility.Visible;
            }
        }
        #endregion
        /// <summary>
        /// 連線電池時取得電池資訊
        /// </summary>
        private void ConnectBatteryInfo()
        {
            if (mModeCode == 5)
            {
                GetDeviceInformation(null);
            }
            else
            {
                GetDeviceRTCDateTime();
            }
        }
        /// <summary>
        /// 啟動取得相關電池資訊之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetBatteryValue_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
            ((Button)sender).IsEnabled = false;
            ThreadPool.QueueUserWorkItem(o => GetDeviceInformation(sender));
        }
        /// <summary>
        /// 取得需要的電池資訊
        /// </summary>
        /// <param name="sender"></param>
        private void GetDeviceInformation(object sender)
        {
            CallbackInfo callbackData = new CallbackInfo()
            {
                Type = CallbackType.GetDeviceInformation,
                Status = CallbackStatus.Fail,
                Data = null
            };

            if (mModeCode == 5)
            {
                BatInfo ba = (BatInfo)mDeviceAdapter.GetBatteryNewAllInformation();
                if (ba != null)
                {
                    DateTime dt = DateTime.Now;
                    LogModel model = new LogModel();

                    model.path = mBattery.GetPort();
                    model.baudRate = mBattery.GetBaudRate();
                    model.batInfo = ba;
                    StringBuilder sb = new StringBuilder();
                    StringWriter sw = new StringWriter(sb);
                    using (JsonWriter jw = new JsonTextWriter(sw))
                    {
                        jw.Formatting = Formatting.Indented;

                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(jw, model);
                    }

                    const string LogSeparator = "===============================================================";
                    string result = string.Format(LogSeparator + Environment.NewLine + "Reocrd Time :      <<{0}>>" + Environment.NewLine + LogSeparator + Environment.NewLine
                        , dt.ToString("yyyy/MM/dd HH:mm:ss"));
                    result = result +
                        string.Format(LogSeparator + Environment.NewLine + "[Battery Pack Information]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                    result = result +
                        string.Format(("Device Name          :").PadRight(30) + "{0}" + Environment.NewLine
                        , ba.DEVICE_NAME);

                    result = result +
                        string.Format(("Serial Number        :").PadRight(30) + "{0}" + Environment.NewLine
                        , ba.batteryId);

                    result = result +
                        string.Format(("Manufacture Date :").PadRight(30) + "{0:0000}/{1:00}/{2:00}" + Environment.NewLine,
                        ba.MANUFACTURE_DATE[0], ba.MANUFACTURE_DATE[1], ba.MANUFACTURE_DATE[2]);

                    result = result +
                        string.Format(("Design Voltage :").PadRight(30) + "{0} mV" + Environment.NewLine,
                        ba.DESIGN_VOLTAGE);

                    result = result +
                        string.Format(("Design Capacity :").PadRight(30) + "{0} mAh" + Environment.NewLine
                        , ba.DESIGN_CAPACITY);

                    result = result +
                        string.Format(("Remain Capacity :").PadRight(30) + "{0} mAh" + Environment.NewLine
                        , ba.REMAIN_CAPACITY);

                    result = result +
                        string.Format(("Full Charge Capacity :").PadRight(30) + "{0} mAh" + Environment.NewLine
                        , ba.FULL_CHARGE_CAPACITY);

                    result = result +
                        string.Format(("Cycle Counted :").PadRight(30) + "{0}" + Environment.NewLine
                        , ba.CYCLE_COUNT);

                    result = result +
                        string.Format(LogSeparator + Environment.NewLine + "[State Of Health]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                    result = result +
                        string.Format(("Battery Health :").PadRight(30) + "{0} %" + Environment.NewLine
                        , ba.SOH);

                    result = result +
                        string.Format(LogSeparator + Environment.NewLine + "[Relative SOC]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                    result = result +
                        string.Format(("Relative SOC :").PadRight(30) + "{0} %" + Environment.NewLine
                        , ba.SOC);

                    result = result +
                        string.Format(LogSeparator + Environment.NewLine + "[Recent Current]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                    result = result +
                        string.Format(("Current :").PadRight(30) + "{0} mA" + Environment.NewLine
                        , ba.CURRENT);

                    result = result +
                        string.Format(LogSeparator + Environment.NewLine + "[Pack Voltage]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                    result = result +
                        string.Format(("Voltage :").PadRight(30) + "{0} mV" + Environment.NewLine
                        , ba.TOTAL_VOLT);

                    result = result +
                        string.Format(LogSeparator + Environment.NewLine + "[Pack Temperature]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                    result = result +
                        string.Format(("Battery Temperature :").PadRight(30) + "{0:0.00} ℃" + Environment.NewLine
                        , ba.TEMPERATURE);


                    if (ba.PROTECTION != null)
                    {
                        result = result +
                            string.Format(LogSeparator + Environment.NewLine + "[Usage Current Error Record]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                        if (ba.PROTECTION[1] > 0)
                        {
                            result = result +
                                string.Format(("Error : Under Voltage Discharged") + Environment.NewLine);
                        }
                        if (ba.PROTECTION[0] > 0)
                        {
                            result = result +
                                string.Format(("Error : Over Voltage Charged") + Environment.NewLine);
                        }
                        if (ba.PROTECTION[3] > 0)
                        {
                            result = result +
                                string.Format(("Error : Over Current Discharged") + Environment.NewLine);
                        }
                        if (ba.PROTECTION[2] > 0)
                        {
                            result = result +
                                string.Format(("Error : Over Current Charged") + Environment.NewLine);
                        }
                        if (ba.PROTECTION[4] > 0)
                        {
                            result = result +
                                string.Format(("Error : Over Temp.") + Environment.NewLine);
                        }
                        if (ba.PROTECTION[5] > 0)
                        {
                            result = result +
                                string.Format(("Error : Under Temp.") + Environment.NewLine);
                        }
                        if (ba.PROTECTION[6] > 0)
                        {
                            result = result +
                                string.Format(("Error : Short Circuit") + Environment.NewLine);
                        }
                    }

                    result = result +
                        string.Format(LogSeparator + Environment.NewLine + "[Cells Voltage]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                    if (ba.CELL_VOLT != null)
                    {
                        for (int i = 0; i < ba.CELL_VOLT.Length; i++)
                        {
                            result = result +
                                string.Format(string.Format("Cell[{0:00}] Volt. :", i + 1).PadRight(30) + "{0} mV" + Environment.NewLine
                                , ba.CELL_VOLT[i]);
                        }
                        result = result +
                            string.Format(string.Format("Cell Voltage Delta :").PadRight(30) + "{0} mV" + Environment.NewLine
                            , ba.DELTA_VOLT);
                    }
                    result = result +
                        string.Format(LogSeparator + Environment.NewLine + "[Pack Detail Info.]" + Environment.NewLine + LogSeparator + Environment.NewLine);

                    result = result +
                        string.Format(string.Format("MCU Firmware Ver :").PadRight(30) + "{0}" + Environment.NewLine
                        , ba.MCU_VERSION);

                    if (ba.LAST_CHARGE != null)
                    {
                        result = result +
                            string.Format(string.Format("Last Charging Time :").PadRight(30) + "{0:00}/{1:00}/{2:00} {3:00}:{4:00}:{5:00} {6}%" + Environment.NewLine
                            , ba.LAST_CHARGE[0], ba.LAST_CHARGE[1], ba.LAST_CHARGE[2], ba.LAST_CHARGE[3], ba.LAST_CHARGE[4], ba.LAST_CHARGE[5], ba.LAST_CHARGE[6]);
                    }
                    if (ba.LAST_DISCHARGE != null)
                    {
                        result = result +
                            string.Format(string.Format("Last Discharging Time :").PadRight(30) + "{0:00}/{1:00}/{2:00} {3:00}:{4:00}:{5:00} {6}%" + Environment.NewLine
                            , ba.LAST_DISCHARGE[0], ba.LAST_DISCHARGE[1], ba.LAST_DISCHARGE[2], ba.LAST_DISCHARGE[3], ba.LAST_DISCHARGE[4], ba.LAST_DISCHARGE[5], ba.LAST_DISCHARGE[6]);
                    }

                    string fileName = ba.batteryType + "_" + ba.batteryId + "_" + dt.ToString("yyyyMMddHHmmss");

                    result = result + LogSeparator
                        + Environment.NewLine;
                    result = result + Environment.NewLine
                        + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine
                        + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine;
                    result = result + LogSeparator + Environment.NewLine;

                    EncryptionAESNeil mEncryptionAlog = new EncryptionAESNeil();
                    mEncryptionAlog.SetEncryptionKey(fileName);
                    result = result + Convert.ToBase64String(mEncryptionAlog.EncryptionData(Encoding.UTF8.GetBytes(sb.ToString()))) + Environment.NewLine;
                    result = result + "!" + LogSeparator;

                    fileName = LOGPATH + fileName + ".txt";
                    bool isCreate = true;

                    if (!Directory.Exists(LOGPATH))
                    {
                        try
                        {
                            Directory.CreateDirectory(LOGPATH);
                        }
                        catch
                        {
                            isCreate = false;
                        }
                    }
                    if (isCreate)
                    {
                        using (StreamWriter outfile = new StreamWriter(fileName))
                        {
                            outfile.Write(result);
                        }

                        callbackData.Status = CallbackStatus.Success;
                    }
                }
            }
            else
            {
                bool isSuccess = mBattery.RefreshMCUEEP();
                isSuccess = isSuccess && mBattery.RefreshPBEEP();

                callbackData.Data = mDeviceAdapter.ConvertToListViewItem(mDeviceAdapter.GetDeviceAllInformation(), !(mModeCode == 1));
                if (callbackData.Data != null)
                {
                    callbackData.Status = CallbackStatus.Success;
                    if (mSaveAction == 3 || !(mModeCode == 1))
                    {
                        BatteryAttribute ba = new BatteryAttribute();
                        ba.Barcode = mDeviceAdapter.GetBarcode();
                        ba.Attribute = callbackData.Data;

                        DateTime ndt = DateTime.UtcNow;
                        ba.PCGMTDateTime = ndt.ToString("yyyy/MM/dd HH:mm:ss");

                        int[] resetDT = mDeviceAdapter.GetDeviceRTCResetTime();
                        int[] dt = mDeviceAdapter.GetDeviceRTCTime();

                        ba.RTCDateTime = string.Format("{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}", dt[0], dt[1], dt[2], dt[3], dt[4], dt[5]);
                        ba.RTCResetDateTime = string.Format("{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}", resetDT[0], resetDT[1], resetDT[2], resetDT[3], resetDT[4], resetDT[5]);

                        int[] cellVolt = mDeviceAdapter.GetDeviceVoltage();
                        ba.CellsVoltage = cellVolt;
                        if (cellVolt != null)
                        {
                            int maxVolt = 0;
                            int minVolt = 0;

                            for (int i = 0; i < cellVolt.Length; i++)
                            {
                                if (i == 0)
                                {
                                    maxVolt = cellVolt[i];
                                    minVolt = cellVolt[i];
                                }
                                else
                                {
                                    if (cellVolt[i] > maxVolt)
                                    {
                                        maxVolt = cellVolt[i];
                                    }
                                    if (cellVolt[i] < minVolt)
                                    {
                                        minVolt = cellVolt[i];
                                    }
                                }
                            }
                            ba.VoltageDifference = maxVolt - minVolt;
                            ba.MaxVoltage = maxVolt;
                            ba.MinVoltage = minVolt;
                        }
                        SaveLog(ba);
                        mSaveAction = 0;
                    }
                }
            }

            Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { sender, callbackData });
        }
        /// <summary>
        /// 設定電池屬性顯示之List View
        /// </summary>
        /// <param name="batteryAttribute"></param>
        private void SetDeviceAttrListView(object batteryAttribute)
        {
            List<DeviceInfoItem> items = batteryAttribute as List<DeviceInfoItem>;

            if (items == null)
            {
                items = new List<DeviceInfoItem>();
            }
            deviceAttrListView.ItemsSource = items;
            System.ComponentModel.ICollectionView view = CollectionViewSource.GetDefaultView(deviceAttrListView.ItemsSource);
            view.Refresh();
        }
        /// <summary>
        /// 啟動將電池預設值設定至預設檔案內之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetDefaultBatteryValueToDefaultAppSetting_Click(object sender, RoutedEventArgs e)
        {
            //第二欄位儲存至mAppSetting內

            Dictionary<string, string> tempData = new Dictionary<string, string>();
            for (int i = 0; i < deviceAttrListView.Items.Count; i++)
            {
                DeviceInfoItem item = (DeviceInfoItem)deviceAttrListView.Items[i];

                tempData.Add(item.Item, item.DefaultValue);
            }

            if (mDeviceAdapter != null)
            {
                string defaultAttrValue = mDeviceAdapter.ConvertDataToDefaultAttr(tempData);
                if (defaultAttrValue != null)
                {
                    mAppSetting.DefaultAttrValue = defaultAttrValue;
                    SaveAppSetting();
                }
            }
        }
        #endregion

        static string LOGPATH = System.AppDomain.CurrentDomain.BaseDirectory + @"Log\";
        private void SaveLog(BatteryAttribute ba)
        {
            if (ba != null)
            {
                bool isCreate = true;
                DateTime dt = DateTime.Now;

                if (!Directory.Exists(LOGPATH))
                {
                    try
                    {
                        Directory.CreateDirectory(LOGPATH);
                    }
                    catch
                    {
                        isCreate = false;
                    }
                }

                if (isCreate)
                {
                    string modeName = "NOR";
                    if (mModeCode == 1)
                    {
                        modeName = "ENG";
                    }

                    string fileName = LOGPATH + modeName + "_" + ba.Barcode + "_" + dt.ToString("yyyyMMddHHmmss") + ".txt";

                    using (FileStream fs = File.Open(fileName, FileMode.Create))
                    using (StreamWriter sw = new StreamWriter(fs))
                    using (JsonWriter jw = new JsonTextWriter(sw))
                    {
                        jw.Formatting = Formatting.Indented;

                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(jw, ba);
                    }
                }
                else
                {
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                        () =>
                        {
                            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                            if (!isCreate)
                            {
                                MessageBox.Show("Cannot save LOG");
                            }
                        }));
                }
            }
        }

        #region Callback Method
        /// <summary>
        /// 使用Thread回傳訊息之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MessageEvent(object sender, CallbackInfo e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;

            if (e != null)
            {
                switch (e.Type)
                {
                    case CallbackType.ConnectDevice:
                        {
                            ToggleButton button = (sender as ToggleButton);

                            if (e.Status != CallbackStatus.Success)
                            {
                                DisconnectBattery();
                                button.IsChecked = false;

                                MessageBox.Show("Check Port and Baud Rate", "Warning");
                                button.IsEnabled = true;
                            }
                            else
                            {
                                if (mModeCode == 5)
                                {
                                    Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                                    ThreadPool.QueueUserWorkItem(o => ConnectBatteryInfo()); // 取得電池資訊
                                }
                                else
                                {
                                    mSaveAction = 0;
                                    button.Content = button.FindResource("Disconnect") as string;
                                    portList.IsEnabled = false;
                                    baudRateList.IsEnabled = false;
                                    closeButton.Visibility = Visibility.Hidden;
                                    infoTabControl.Visibility = System.Windows.Visibility.Visible;

                                    SetDeviceAttrListView(null);

                                    rtcTime.Text = "";
                                    rtcResetTime.Text = "";
                                    statusValue.Content = (button.FindResource("ConnectTo") as string) + "[" + mBattery.GetPort() + "]";

                                    UpdateSetStatusLabel.Content = "---";
                                    UpdateSetStatusLabel.Foreground = new SolidColorBrush(Colors.Black);

                                    Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                                    ThreadPool.QueueUserWorkItem(o => ConnectBatteryInfo()); // 取得電池資訊
                                    button.IsEnabled = true;
                                }
                            }

                            break;
                        }
                    case CallbackType.SetDeviceBarcode:
                        {
                            if (e.Status == CallbackStatus.Success)
                            {
                                string productInfo = e.Data.ToString().Substring(0, 6);
                                //儲存制清單內
                                barcodeSetStatusLabel.Content = "OK";
                                barcodeSetStatusLabel.Foreground = new SolidColorBrush(Colors.Black);

                                if (mModeCode == 3 || mModeCode == 1)
                                {
                                    mSaveAction = 1;
                                    Page1.Visibility = System.Windows.Visibility.Visible;
                                    Page2.Visibility = System.Windows.Visibility.Visible;
                                    Page3.Visibility = System.Windows.Visibility.Collapsed;
                                    Page4.Visibility = System.Windows.Visibility.Collapsed;
                                }
                            }
                            else
                            {
                                // Warning Dialog
                                barcodeSetStatusLabel.Content = "NG";
                                barcodeSetStatusLabel.Foreground = new SolidColorBrush(Colors.Red);
                            }
                            barcodeSetStatusLabel.Visibility = System.Windows.Visibility.Visible;
                            ((Button)sender).IsEnabled = true;

                            break;
                        }
                    case CallbackType.GetDeviceInformation:
                        {

                            if (mModeCode == 5)
                            {
                                sw.Stop();//碼表停止
                                DisconnectBattery();
                                if (e.Status == CallbackStatus.Success)
                                {
                                    MessageBox.Show(string.Format("Total Time: {0:0.00}s", sw.Elapsed.TotalSeconds), "Success");
                                }
                                else
                                {
                                    MessageBox.Show("Cannot read Battery, please try again!", "Fail");
                                }
                                connectButton.IsChecked = false;
                                connectButton.IsEnabled = true;
                            }
                            else
                            {
                                if (e.Status == CallbackStatus.Success)
                                {
                                    SetDeviceAttrListView(e.Data);
                                }
                                else
                                {
                                    // Warning
                                    SetDeviceAttrListView(null);

                                    MessageBox.Show(e.Data.ToString());
                                }
                                if (sender != null)
                                {
                                    ((Button)sender).IsEnabled = true;
                                }
                            }
                            break;
                        }
                    case CallbackType.SetDeviceRTCTime:
                        {
                            if (e.Status == CallbackStatus.Success)
                            {
                                rtcTime.Text = "";
                                rtcResetTime.Text = "";
                                //ThreadPool.QueueUserWorkItem(o => GetDeviceRTCDateTime());

                                List<int[]> dtList = (List<int[]>)e.Data;
                                if (dtList.Count == 2)
                                {
                                    rtcTime.Text = string.Format("{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}", dtList[0][0], dtList[0][1], dtList[0][2], dtList[0][3], dtList[0][4], dtList[0][5]);
                                    rtcResetTime.Text = string.Format("{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}", dtList[1][0], dtList[1][1], dtList[1][2], dtList[1][3], dtList[1][4], dtList[1][5]);
                                }

                                if (mModeCode == 1)
                                {
                                        mSaveAction = 2;
                                        Page1.Visibility = System.Windows.Visibility.Visible;
                                        Page2.Visibility = System.Windows.Visibility.Visible;
                                        Page3.Visibility = System.Windows.Visibility.Visible;
                                        Page4.Visibility = System.Windows.Visibility.Visible;
                                }

                                UpdateSetRTCLabel.Content = "OK";
                                UpdateSetRTCLabel.Foreground = new SolidColorBrush(Colors.Black);
                            }
                            else
                            {
                                // TODO
                                UpdateSetRTCLabel.Content = "NG";
                                UpdateSetRTCLabel.Foreground = new SolidColorBrush(Colors.Red);
                            }
                            ((Button)sender).IsEnabled = true;
                            break;
                        }
                    case CallbackType.GetDeviceRTCTime:
                        {
                            if (e.Status == CallbackStatus.Success)
                            {
                                // Fixed by Neil 2016.08.05 
                                //int[] dt = (int[])e.Data;
                                //rtcTime.Text = string.Format("{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}", dt[0], dt[1], dt[2], dt[3], dt[4], dt[5]);
                                List<int[]> dtList = (List<int[]>)e.Data;
                                if (dtList.Count == 2)
                                {
                                    rtcTime.Text = string.Format("{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}", dtList[0][0], dtList[0][1], dtList[0][2], dtList[0][3], dtList[0][4], dtList[0][5]);
                                    rtcResetTime.Text = string.Format("{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}", dtList[1][0], dtList[1][1], dtList[1][2], dtList[1][3], dtList[1][4], dtList[1][5]);
                                }
                                ////
                            }
                            else
                            {
                                // TODO
                            }
                            break;
                        }
                    case CallbackType.DisconnectDeviceFromError:
                        {
                            DisconnectBattery();
                            MessageBox.Show(e.Data.ToString(), e.Type.ToString());
                            break;
                        }
                }
            }
        }
        #endregion

        private void UpdateFirmware_Click(object sender, RoutedEventArgs e)
        {
            UpdateSetStatusLabel.Content = "---";
            UpdateSetStatusLabel.Foreground = new SolidColorBrush(Colors.Black);

            try
            {
                List<PBAttributeInfo> dataList = null;
                //string appSettingFilePath = mAppPath + "update.json";
                //using (StreamReader file = File.OpenText(appSettingFilePath))
                //{
                //    JsonSerializer serializer = new JsonSerializer();
                //    dataList = (List<PBAttributeInfo>)serializer.Deserialize(file, typeof(List<PBAttributeInfo>));
                //}

                dataList = new List<PBAttributeInfo>()
                {
                    new PBAttributeInfo() { PBCmd = BatteryEEPAttrCmd.OVTH, PBIndex = 0, PBData = (long)4100 }
                };
                //using (FileStream fs = File.Open(appSettingFilePath, FileMode.Create))
                //using (StreamWriter sw = new StreamWriter(fs))
                //using (JsonWriter jw = new JsonTextWriter(sw))
                //{
                //    jw.Formatting = Formatting.Indented;

                //    JsonSerializer serializer = new JsonSerializer();
                //    serializer.Serialize(jw, dataList);
                //}

                bool isSuccess = mDeviceAdapter.SetPBData(dataList);

                if (isSuccess)
                {
                    UpdateSetStatusLabel.Content = "OK";
                    UpdateSetStatusLabel.Foreground = new SolidColorBrush(Colors.Black);
                }
                else
                {
                    UpdateSetStatusLabel.Content = "NG";
                    UpdateSetStatusLabel.Foreground = new SolidColorBrush(Colors.Red);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                UpdateSetStatusLabel.Content = "ERROR";
                UpdateSetStatusLabel.Foreground = new SolidColorBrush(Colors.Red);
            }
        }
    }
}
