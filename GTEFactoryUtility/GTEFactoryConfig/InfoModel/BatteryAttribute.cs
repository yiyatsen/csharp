﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEFactoryConfig.InfoModel
{
    public class BatteryAttribute
    {
        public string Barcode { get; set; }
        public string PCGMTDateTime { get; set; }
        public string RTCDateTime { get; set; }
        public string RTCResetDateTime { get; set; }
        public int MaxVoltage { get; set; }
        public int MinVoltage { get; set; }
        public int VoltageDifference { get; set; }
        public int[] CellsVoltage { get; set; }
        public object Attribute { get; set; }
    }

    public class StationInfo
    {
        public string stationId { get; set; }
        public string stationType { get; set; }
        public string stationMode { get; set; }
        public string commMode { get; set; }
        public bool disableLogin { get; set; }
        public bool isSupportSchedule { get; set; }
        public bool isRegister { get; set; }

        public StationInfo()
        {
            stationId = "non-Register";
            stationType = "diagnostic";
            stationMode = "QuicklySave";
            commMode = "1To1";
            disableLogin = false;
            isSupportSchedule = false;
            isRegister = false;
        }
    }

    public class AutoSetting
    {
        public bool isAutoConn { get; set; }
        public string action { get; set; }
        public int timeout { get; set; }
        public int retryTime { get; set; }
        public bool isDetectedCmd { get; set; }
        public bool isBatteryIn { get; set; }
        public bool isLogMode { get; set; }
        public bool isAutoLog { get; set; }
        public bool enableSchedule { get; set; }
        public bool disableRefreshEEP { get; set; }
        public bool disableRefreshEEPFlag { get; set; }

        public AutoSetting()
        {
            isAutoConn = false;
            action = "retry";
            timeout = 500;
            retryTime = 0;
            isDetectedCmd = true;
            isBatteryIn = true;
            isLogMode = true;
            isAutoLog = false;
            enableSchedule = false;
            disableRefreshEEP = false;
            disableRefreshEEPFlag = false;
        }
    }

    public class CmdSetting
    {
        public bool enableASCII { get; set; }
        public bool enableCipher { get; set; }

        public CmdSetting()
        {
            enableASCII = false;
            enableCipher = false;
        }
    }

    public class BatInfo
    {
        public string timestamp { get; set; }
        public string eepTimestamp { get; set; }
        public int[] MCU_EEP { get; set; }
        public Dictionary<string, int[]> PB_EEP { get; set; }
        public string RTC { get; set; }
        public string MCU_VERSION { get; set; }
        public string DEVICE_NAME { get; set; }
        public string batteryType { get; set; }
        public string batteryId { get; set; }
        public string BARCODE { get; set; }
        public int[] MANUFACTURE_DATE { get; set; }
        public int SOC { get; set; }
        public int SOH { get; set; }
        public int CURRENT { get; set; }
        public uint TOTAL_VOLT { get; set; }
        public int MAX_VOLT { get; set; }
        public int MIN_VOLT { get; set; }
        public int DELTA_VOLT { get; set; }
        public int CELL_NUM { get; set; }
        public int[] CELL_VOLT { get; set; }
        public double TEMPERATURE { get; set; }
        public double MAX_TEMPERATURE { get; set; }
        public double MIN_TEMPERATURE { get; set; }
        public double DELTA_TEMPERATURE { get; set; }
        public double[] CELL_TEMPERATURE { get; set; }
        public int[] PROTECTION { get; set; }
        public ulong DESIGN_VOLTAGE { get; set; }
        public ulong DESIGN_CAPACITY { get; set; }
        public ulong REMAIN_CAPACITY { get; set; }
        public ulong FULL_CHARGE_CAPACITY { get; set; }
        public ulong CYCLE_COUNT { get; set; }
        public int[] LAST_CHARGE { get; set; }
        public int[] LAST_DISCHARGE { get; set; }
        public int[][] LOG_CHARGE { get; set; }
        public int[] DISCHARGE_INTERVAL { get; set; }
        public int[] TEMPERATURE_INTERVAL { get; set; }
    }


    public class LogModel
    {
        public int slotId { get; set; }
        public string path { get; set; }
        public int baudRate { get; set; }
        public string commMode { get; set; }

        public StationInfo stationInfo { get; set; }
        public AutoSetting autoSetting { get; set; }
        public CmdSetting cmdSetting { get; set; }
        public BatInfo batInfo { get; set; }

        public LogModel()
        {
            slotId = 1;
            commMode = "1To1";
            stationInfo = new StationInfo();
            autoSetting = new AutoSetting();
            cmdSetting = new CmdSetting();
            batInfo = new BatInfo();
        }
    }
}
