﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEFactoryConfig.InfoModel
{
    public class DeviceInfoItem
    {
        public bool IsReadOnly { get; set; }
        public string Item { get; set; }
        public string DefaultValue { get; set; }
        public string RealValue { get; set; }
        public bool Result { get; set; }
    }
}
