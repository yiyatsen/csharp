﻿using GTE.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEFactoryConfig.InfoModel
{
    public class PBAttributeInfo
    {
        public BatteryEEPAttrCmd PBCmd { get; set; }
        public int PBIndex { get; set; }
        public object PBData { get; set; }
    }
}
