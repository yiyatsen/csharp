﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEFactoryConfig.Setting
{
    public class AppSetting
    {
        public string Port { get; set; }
        public int BaudRate { get; set; }
        public int[] BaudRateList { get; set; }
        public int VoltageDifferenceLimit { get; set; }

        public bool EnableRecordDeviceInfo { get; set; }

        public string DefaultAttrValue { get; set; }

        public string Lang { get; set; }

        public AppSetting()
        {
            BaudRateList = new int[] { 115200, 38400 };
        }
    }
}
