﻿using GTE.DeivceConfiguration;
using GTEBatteryLib;
using GTEFactoryConfig.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace GTEFactoryConfig.Config
{
    public abstract class FactoryDeviceAdapter : AppDeviceAdapter
    {
        protected const int DEFAULTYEAR = 2000;

        protected BatteryAPI mDevice { get; set; }

        protected object mDeviceConfig { get; set; }
        public object DeviceConfig { get { return mDeviceConfig; } }

        protected string[] mDeviceAttrNameList { get; set; }
        public string[] DeviceAttrNameList { get { return mDeviceAttrNameList; } }

        public FactoryDeviceAdapter(BatteryAPI device)
            : base()
        {
            mDevice = device;
            SetDeviceAttrList();
        }

        protected abstract void SetDeviceAttrList();

        public abstract string SetDefaultAttrValue(string json);

        public abstract string ConvertDataToDefaultAttr(Dictionary<string, string> data);

        public abstract object ConvertToListViewItem(object data, bool isReadOnly);

        protected abstract bool CheckValueEqual(PropertyInfo propertyInfo, object data);

        public abstract object GetDeviceAllInformation();

        public abstract object GetBatteryNewAllInformation();

        public abstract void SetDeviceRTCTime(DateTime dateTime);

        public abstract void SetBatteryEngMode(bool isEngMdoe);
        /// <summary>
        /// Get Barcode
        /// Add By Neil 2016.08.05
        /// </summary>
        /// <returns></returns>
        public abstract string GetBarcode();

        public abstract int[] GetDeviceRTCTime();
        /// <summary>
        /// Get RTC Reset Time
        /// Add By Neil 2017.07.20
        /// </summary>
        /// <returns></returns>
        public abstract bool ResetMCU();
        /// <summary>
        /// Get RTC Reset Time
        /// Add By Neil 2016.08.05
        /// </summary>
        /// <returns></returns>
        public abstract int[] GetDeviceRTCResetTime();

        public abstract int[] GetDeviceVoltage();

        public abstract bool SetPBData(List<PBAttributeInfo> pbAttributeInfoList);
    }
}
