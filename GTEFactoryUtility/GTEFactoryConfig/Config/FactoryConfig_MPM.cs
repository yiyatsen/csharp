﻿using GTE.Command;
using GTEFactoryConfig.InfoModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace GTEFactoryConfig.Config
{
    public class FactoryConfig_MPM : FactoryDeviceAdapter
    {
        /// <summary>
        /// MPM表示MCU類型
        /// 2表示加密版
        /// 1表示第一版
        /// </summary>
        public class FactoryConfgAttr_MPM
        {
            #region Property
            public long EDV { get; set; }
            public long DC { get; set; }
            public long TapperCurrent { get; set; }
            public long FCC { get; set; }
            public long CycleCount { get; set; }
            public string RC { get; set; }
            public long DV { get; set; }
            public string DeviceName { get; set; }
            public string SN { get; set; }
            public string MCUVer { get; set; }
            public string ManufactureDate { get; set; }
            public string OZ1REV { get; set; }
            public string OZ2REV { get; set; }
            public string OZ1FN { get; set; }
            public string OZ1PN { get; set; }
            public string OZ2FN { get; set; }
            public string OZ2PN { get; set; }
            public double SCTH { get; set; }
            public long SCDelay { get; set; }
            public double OC { get; set; }
            public long OCDTIIME { get; set; }

            public long MinRC { get; set; }
            public long MaxSN { get; set; }
            public long MinSN { get; set; }
            #endregion
        }

        public FactoryConfig_MPM(GTEBatteryLib.BatteryAPI device)
            : base(device)
        {

        }

        protected override void SetDeviceAttrList()
        {
            mDeviceAttrNameList = new string[]{ "EDV", "DC", "TapperCurrent", "FCC" 
                , "CycleCount", "RC", "DV", "DeviceName", "SN", "MCUVer", "ManufactureDate"
                , "OZ1REV", "OZ2REV", "OZ1FN", "OZ2FN", "OZ1PN", "OZ2PN", "SCTH", "SCDelay", "OC", "OCDTIIME"};
        }


        public override string SetDefaultAttrValue(string json)
        {
            string result = null; //如果有預設值不回傳

            try
            {
                using (var stringReader = new StringReader(json))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    mDeviceConfig = (FactoryConfgAttr_MPM)serializer.Deserialize(stringReader, typeof(FactoryConfgAttr_MPM));
                }
            }
            catch
            {
                FactoryConfgAttr_MPM config = new FactoryConfgAttr_MPM();

                config.DeviceName = "None";
                config.ManufactureDate = "None";
                config.MCUVer = "None";
                config.MaxSN = 65535;
                config.MinSN = 0;
                config.SN = string.Format("{0}>SN>{1}", config.MaxSN, config.MinSN);
                config.RC = string.Format(">{0}", config.MinRC);
                config.MinRC = 0;
                config.OZ1REV = "None";
                config.OZ2REV = "None";
                config.OZ1FN = "None";
                config.OZ2FN = "None";
                config.OZ1PN = "None";
                config.OZ2PN = "None";

                mDeviceConfig = config;

                result = ConvertDataToString();
            }

            return result;
        }

        public override string ConvertDataToDefaultAttr(Dictionary<string, string> data)
        {
            string result = null;

            if (mDeviceConfig != null && data != null)
            {
                foreach (string propertyName in data.Keys)
                {
                    PropertyInfo propertyInfo = mDeviceConfig.GetType().GetProperty(propertyName);

                    switch (propertyInfo.Name)
                    {
                        case "RC":
                            {
                                string[] dataItem = data[propertyName].Split('>');

                                long value = 0;
                                bool isSuccess = long.TryParse(dataItem[1], out value);
                                if (isSuccess)
                                {
                                    ((FactoryConfgAttr_MPM)mDeviceConfig).MinRC = value;
                                }
                                ((FactoryConfgAttr_MPM)mDeviceConfig).RC = data[propertyName];
                                break;
                            }
                        case "SN":
                            {
                                string[] dataItem = data[propertyName].Split('>');

                                long value = 0;
                                bool isSuccess = long.TryParse(dataItem[0], out value);
                                if (isSuccess)
                                {
                                    ((FactoryConfgAttr_MPM)mDeviceConfig).MaxSN = value;
                                }

                                value = 0;
                                isSuccess = long.TryParse(dataItem[2], out value);
                                if (isSuccess)
                                {
                                    ((FactoryConfgAttr_MPM)mDeviceConfig).MinSN = value;
                                }
                                ((FactoryConfgAttr_MPM)mDeviceConfig).SN = data[propertyName];
                                break;
                            }
                        default:
                            {
                                switch (propertyInfo.PropertyType.Name)
                                {
                                    case "Int64":
                                        {
                                            long value = 0;
                                            bool isSuccess = long.TryParse(data[propertyName], out value);
                                            if (isSuccess)
                                            {
                                                propertyInfo.SetValue(mDeviceConfig, value, null);
                                            }
                                            break;
                                        }
                                    case "String":
                                        {
                                            propertyInfo.SetValue(mDeviceConfig, data[propertyName], null);
                                            break;
                                        }
                                    case "Double":
                                        {
                                            double value = 0;
                                            bool isSuccess = double.TryParse(data[propertyName], out value);
                                            if (isSuccess)
                                            {
                                                propertyInfo.SetValue(mDeviceConfig, value, null);
                                            }

                                            propertyInfo.SetValue(mDeviceConfig, value, null);
                                            break;
                                        }
                                }

                                break;
                            }
                    }
                }
                result = ConvertDataToString();
            }
            return result;
        }

        private string ConvertDataToString()
        {
            string result = null;
            try
            {
                using (StringWriter sw = new StringWriter())
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    jw.Formatting = Formatting.Indented;

                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(jw, mDeviceConfig);
                    result = sw.ToString();
                }
            }
            catch { }
            return result;
        }

        public override object ConvertToListViewItem(object data, bool isReadOnly)
        {
            List<DeviceInfoItem> items = new List<DeviceInfoItem>();
            FactoryConfgAttr_MPM item = data as FactoryConfgAttr_MPM;
            if (item != null)
            {
                for (int i = 0; i < mDeviceAttrNameList.Length; i++)
                {
                    PropertyInfo propertyInfo = mDeviceConfig.GetType().GetProperty(mDeviceAttrNameList[i]);
                    bool isEqual = CheckValueEqual(propertyInfo, propertyInfo.GetValue(item, null));

                    items.Add(new DeviceInfoItem()
                    {
                        Item = mDeviceAttrNameList[i],
                        DefaultValue = propertyInfo.GetValue(mDeviceConfig, null).ToString(),
                        RealValue = propertyInfo.GetValue(item, null).ToString(),
                        IsReadOnly = isReadOnly,
                        Result = isEqual
                    });
                }
            }

            return items;
        }

        protected override bool CheckValueEqual(PropertyInfo propertyInfo, object data)
        {
            bool isSuccess = false;
            if (mDeviceConfig != null && propertyInfo != null && data != null)
            {
                switch (propertyInfo.Name)
                {
                    case "RC":
                        {
                            long value = 0;
                            isSuccess = long.TryParse(data.ToString(), out value);
                            if (isSuccess)
                            {
                                isSuccess = (value > ((FactoryConfgAttr_MPM)mDeviceConfig).MinRC);
                            }
                            break;
                        }
                    case "SN":
                        {
                            long value = 0;
                            isSuccess = long.TryParse(data.ToString(), out value);
                            if (isSuccess)
                            {
                                isSuccess = (((FactoryConfgAttr_MPM)mDeviceConfig).MaxSN > value) && (value > ((FactoryConfgAttr_MPM)mDeviceConfig).MinSN);
                            }
                            break;
                        }
                    default:
                        {
                            isSuccess = data.Equals(propertyInfo.GetValue(mDeviceConfig, null));

                            break;
                        }
                }
            }
            return isSuccess;
        }

        public override object GetDeviceAllInformation()
        {
            bool isSuccess = mDevice.RefreshMCUEEP();
            isSuccess = isSuccess && mDevice.RefreshPBEEP();

            if (isSuccess)
            {
                FactoryConfgAttr_MPM config = new FactoryConfgAttr_MPM();

                object value = IsLegalData(mDevice.GETMCUEEPData(BatteryEEPAttrCmd.EDV2Value));
                if (value != null)
                {
                    long numberValue = 0;
                    long.TryParse(value.ToString(), out numberValue);
                    config.EDV = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_DESIGN_CAPACITY_GET));
                if (value != null)
                {
                    long numberValue = 0;
                    long.TryParse(value.ToString(), out numberValue);
                    config.DC = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_TAPER_CURRENT_GET));
                if (value != null)
                {
                    long numberValue = 0;
                    long.TryParse(value.ToString(), out numberValue);
                    config.TapperCurrent = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_FULL_CC_GET));
                if (value != null)
                {
                    long numberValue = 0;
                    long.TryParse(value.ToString(), out numberValue);
                    config.FCC = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_CYCLE_COUNT_GET));
                if (value != null)
                {
                    long numberValue = 0;
                    long.TryParse(value.ToString(), out numberValue);
                    config.CycleCount = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_REMAIN_CAPACITY_GET));
                if (value != null)
                {
                    config.RC = value.ToString();
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_DESIGN_VOLTAGE_GET));
                if (value != null)
                {
                    long numberValue = 0;
                    long.TryParse(value.ToString(), out numberValue);
                    config.DV = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_DEVICE_NAME_GET));
                if (value != null)
                {
                    config.DeviceName = value.ToString();
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_BATTERY_NO_GET));
                if (value != null)
                {
                    config.SN = value.ToString();
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_VERSION_GET));
                if (value != null)
                {
                    config.MCUVer = value.ToString();
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_MANUFACTURE_DATE_GET));
                int[] data = value as int[];
                if (data != null)
                {
                    config.ManufactureDate = string.Format("{0:0000}/{1:00}/{2:00}", data[0], data[1], data[2]);
                }

                value = IsLegalData(mDevice.GetPBEEPData(BatteryEEPAttrCmd.Ver, 0));
                if (value != null)
                {
                    config.OZ1REV = value.ToString();
                }

                value = IsLegalData(mDevice.GetPBEEPData(BatteryEEPAttrCmd.FactoryName, 0));
                if (value != null)
                {
                    config.OZ1FN = value.ToString();
                }

                value = IsLegalData(mDevice.GetPBEEPData(BatteryEEPAttrCmd.ProjectName, 0));
                if (value != null)
                {
                    config.OZ1PN = value.ToString();
                }

                value = IsLegalData(mDevice.GetPBEEPData(BatteryEEPAttrCmd.SCTH, 0));
                if (value != null)
                {
                    double numberValue = 0;
                    double.TryParse(value.ToString(), out numberValue);
                    config.SCTH = numberValue;
                }

                value = IsLegalData(mDevice.GetPBEEPData(BatteryEEPAttrCmd.SCDelay, 0));
                if (value != null)
                {
                    long numberValue = 0;
                    long.TryParse(value.ToString(), out numberValue);
                    config.SCDelay = numberValue;
                }

                value = IsLegalData(mDevice.GetPBEEPData(BatteryEEPAttrCmd.OCTH, 0));
                if (value != null)
                {
                    double numberValue = 0;
                    double.TryParse(value.ToString(), out numberValue);
                    config.OC = numberValue;
                }

                value = IsLegalData(mDevice.GetPBEEPData(BatteryEEPAttrCmd.OCDelayTime, 0));
                if (value != null)
                {
                    long numberValue = 0;
                    long.TryParse(value.ToString(), out numberValue);
                    config.OCDTIIME = numberValue;
                }

                if (mDevice.GetPBNumber() > 1)
                {
                    value = IsLegalData(mDevice.GetPBEEPData(BatteryEEPAttrCmd.Ver, 1));
                    if (value != null)
                    {
                        config.OZ2REV = value.ToString();
                    }
                    else
                    {
                        config.OZ2REV = "None";
                    }

                    value = IsLegalData(mDevice.GetPBEEPData(BatteryEEPAttrCmd.FactoryName, 1));
                    if (value != null)
                    {
                        config.OZ2FN = value.ToString();
                    }

                    value = IsLegalData(mDevice.GetPBEEPData(BatteryEEPAttrCmd.ProjectName, 1));
                    if (value != null)
                    {
                        config.OZ2PN = value.ToString();
                    }

                    //value = IsLegalData(mDevice.GetPBEEPData(BatteryEEPAttrCmd.OVTH, 0));
                    //if (value != null)
                    //{
                    //    //config.OZ2PN = value.ToString();
                    //}

                    //value = IsLegalData(mDevice.GetPBEEPData(BatteryEEPAttrCmd.OVRelease, 0));
                    //if (value != null)
                    //{
                    //    //config.OZ2PN = value.ToString();
                    //}

                    //value = IsLegalData(mDevice.GetPBEEPData(BatteryEEPAttrCmd.OVTH, 1));
                    //if (value != null)
                    //{
                    //    //config.OZ2PN = value.ToString();
                    //}

                    //value = IsLegalData(mDevice.GetPBEEPData(BatteryEEPAttrCmd.OVRelease, 1));
                    //if (value != null)
                    //{
                    //    //config.OZ2PN = value.ToString();
                    //}
                }
                else
                {
                    config.OZ2REV = "None";
                    config.OZ2FN = "None";
                    config.OZ2PN = "None";
                }
                return config;
            }

            return null;
        }

        private int[] lookupSOH = new int[101] { 
            0,	1,	2,	3,	4,	5,	6,	7,	8,	9,
            10,	11,	12,	13,	14,	15,	16,	17,	18,	19,
            20,	21,	22,	23,	24,	25,	26,	27,	28,	29,
            30,	31,	32,	33,	34,	35,	36,	37,	38,	39,
            40,	41,	42,	43,	44,	45,	46,	47,	48,	49,
            50,	52,	54,	56,	58,	60,	62,	64,	66,	68,
            70,	71,	72,	73,	74,	75,	76,	77,	78,	79,
            80,	81,	82,	83,	84,	85,	86,	87,	88,	89,
            90,	90,	91,	91,	92,	92,	93,	93,	94,	94,
            95,	95,	97,	97,	98,	99,	100,	100,	100,	100, 100};

        public override object GetBatteryNewAllInformation()
        {
            BatInfo ba = new BatInfo();
            try
            {
                object value = mDevice.SendBatteryCmd(BatteryCmd.CMD_OZ1_EEPROM_GET);
                if (value != null)
                {
                    List<int> tempData = new List<int>();
                    ba.PB_EEP = new Dictionary<string, int[]>();
                    for (int i = 0; i < ((byte[])value).Length; i++)
                    {
                        tempData.Add(((byte[])value)[i]);
                    }
                    ba.PB_EEP.Add("1", tempData.ToArray());

                    value = mDevice.SendBatteryCmd(BatteryCmd.CMD_OZ2_EEPROM_GET);
                    if (value != null)
                    {
                        tempData.Clear();
                        for (int i = 0; i < ((byte[])value).Length; i++)
                        {
                            tempData.Add(((byte[])value)[i]);
                        }
                        ba.PB_EEP.Add("2", tempData.ToArray());
                    }
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_EEPROM_GET);
                if (value != null)
                {
                    int mcuIndex = 0;
                    int.TryParse(value.ToString(), out mcuIndex);

                    List<int> tempData = new List<int>();
                    for (int i = 1; i <= mcuIndex; i++)
                    {
                        value = mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_EEPROM_GET, (byte)i);
                        if (value != null)
                        {
                            for (int j = 0; j < ((byte[])value).Length; j++)
                            {
                                tempData.Add(((byte[])value)[j]);
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    ba.MCU_EEP = tempData.ToArray();


                    value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CRATE_INFO);
                    if (value != null)
                    {
                        tempData.Clear();
                        for (int i = 0; i < ((int[])value).Length; i++)
                        {
                            tempData.Add(((int[])value)[i]);
                        }
                        ba.DISCHARGE_INTERVAL = tempData.ToArray();
                    }
                    value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_TEMPERATURE_INTERVAL);
                    if (value != null)
                    {
                        tempData.Clear();
                        for (int i = 0; i < ((int[])value).Length; i++)
                        {
                            tempData.Add(((int[])value)[i]);
                        }
                        ba.TEMPERATURE_INTERVAL = tempData.ToArray();
                    }
                }
                else
                {
                    value = mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_EEPROM_H_GET);
                    if (value != null)
                    {
                        List<int> tempData = new List<int>();
                        for (int i = 0; i < ((byte[])value).Length; i++)
                        {
                            tempData.Add(((byte[])value)[i]);
                        }
                        value = mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_EEPROM_L_GET);
                        if (value != null)
                        {
                            for (int i = 0; i < ((byte[])value).Length; i++)
                            {
                                tempData.Add(((byte[])value)[i]);
                            }
                            ba.MCU_EEP = tempData.ToArray();
                        }
                    }

                    value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_00_22, 1);
                    if (value != null)
                    {
                        List<int[]> tempLog = new List<int[]>();
                        tempLog.AddRange((int[][])value);
                        value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_23_45, 1);
                        if (value != null)
                        {
                            tempLog.AddRange((int[][])value);
                            value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_CHARGE_LOG_46_49, 1);
                            if (value != null)
                            {
                                tempLog.AddRange((int[][])value);
                                ba.LOG_CHARGE = tempLog.ToArray();
                            }
                        }
                    }
                }

                if (ba.MCU_EEP != null)
                {
                    ba.eepTimestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.000Z");
                    int MANUFACTURE_DATE = ((ba.MCU_EEP[18] << 8) | ba.MCU_EEP[19]);

                    int tmpYear = ((MANUFACTURE_DATE >> 9) & 0x0000007F) + DEFAULTYEAR;
                    if (tmpYear > DateTime.Now.Year)
                    {
                        tmpYear = tmpYear - 20;
                    }

                    ba.MANUFACTURE_DATE = new int[] { tmpYear, (MANUFACTURE_DATE >> 5) & 0x0000000F, MANUFACTURE_DATE & 0x0000001F };

                    ba.batteryId = string.Format("{0:0}{1:0}{2:00}{3:00}{4:0000}",
                        ((ba.MCU_EEP[56] >> 4) & 0xF), (ba.MCU_EEP[56] & 0xF), (tmpYear - 2000), ba.MCU_EEP[57], ((ba.MCU_EEP[20] << 8) | ba.MCU_EEP[21]));
                    ba.BARCODE = ba.batteryId;

                    ba.CYCLE_COUNT = (ulong)((ba.MCU_EEP[12] << 8) | ba.MCU_EEP[13]);
                    ba.DESIGN_VOLTAGE = (ulong)(((ba.MCU_EEP[16] << 8) | ba.MCU_EEP[17]) * 1000);
                    ba.DESIGN_CAPACITY = (ulong)(((ba.MCU_EEP[2] << 24) | (ba.MCU_EEP[3] << 16) | (ba.MCU_EEP[4] << 8) | (ba.MCU_EEP[5])) / 3600);
                    ba.FULL_CHARGE_CAPACITY = (ulong)(((ba.MCU_EEP[8] << 24) | (ba.MCU_EEP[9] << 16) | (ba.MCU_EEP[10] << 8) | (ba.MCU_EEP[11])) / 3600);

                    if (ba.DESIGN_CAPACITY != 0)
                    {
                        int index = Convert.ToInt32(ba.FULL_CHARGE_CAPACITY * 100 / ba.DESIGN_CAPACITY);
                        if (index > 100)
                        {
                            ba.SOH = 100;
                        }
                        else if (index < 0)
                        {
                            ba.SOH = 0;
                        }
                        else
                        {
                            ba.SOH = lookupSOH[index];
                        }
                    }
                    else
                    {
                        ba.SOH = 0;
                    }
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_VERSION_GET));
                if (value != null) ba.MCU_VERSION = value.ToString();

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_DEVICE_NAME_GET));
                if (value != null)
                {
                    ba.DEVICE_NAME = value.ToString();
                    ba.batteryType = value.ToString();
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_RTC_REG_DUMP, 1);
                if (value != null)
                {
                    int[] tmpRTC = value as int[];
                    ba.RTC = string.Format("{0:0000}-{1:00}-{2:00}T{3:00}-{4:00}-{5:00}.000Z", tmpRTC[0], tmpRTC[1], tmpRTC[2], tmpRTC[3], tmpRTC[4], tmpRTC[5]);
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_RSOC_GET));
                if (value != null)
                {
                    int numberValue = 0;
                    int.TryParse(value.ToString(), out numberValue);
                    ba.SOC = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_CURRENT_GET));
                if (value != null)
                {
                    int numberValue = 0;
                    int.TryParse(value.ToString(), out numberValue);
                    ba.CURRENT = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_PROTECTION_GET));
                if (value != null)
                {
                    List<int> tempData = new List<int>();
                    for (int i = 0; i < ((byte[])value).Length; i++)
                    {
                        tempData.Add(((byte[])value)[i]);
                    }
                    ba.PROTECTION = tempData.ToArray();
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_REMAIN_CAPACITY_GET));
                if (value != null)
                {
                    ulong numberValue = 0;
                    ulong.TryParse(value.ToString(), out numberValue);
                    ba.REMAIN_CAPACITY = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_VOLTAGE_GET));
                if (value != null)
                {
                    ba.CELL_VOLT = value as int[];

                    if (ba.CELL_VOLT != null)
                    {
                        ba.CELL_NUM = ba.CELL_VOLT.Length;

                        ba.TOTAL_VOLT = 0;
                        ba.MAX_VOLT = 0;
                        ba.MIN_VOLT = 0;
                        bool isFirst = true;
                        foreach (int cellVoltage in ba.CELL_VOLT)
                        {
                            if (isFirst)
                            {
                                isFirst = false;
                                ba.MAX_VOLT = cellVoltage;
                                ba.MIN_VOLT = cellVoltage;
                            }
                            if (cellVoltage > ba.MAX_VOLT)
                            {
                                ba.MAX_VOLT = cellVoltage;
                            }

                            if (cellVoltage < ba.MIN_VOLT)
                            {
                                ba.MIN_VOLT = cellVoltage;
                            }
                            ba.TOTAL_VOLT = ba.TOTAL_VOLT + (uint)cellVoltage;
                        }
                        ba.DELTA_VOLT = ba.MAX_VOLT - ba.MIN_VOLT;
                    }
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_TEMP_GET));
                if (value != null)
                {
                    ba.CELL_TEMPERATURE = value as double[];
                    if (ba.CELL_TEMPERATURE != null)
                    {
                        double minTemp = 0;
                        double maxTemp = 0;
                        bool isFirst = true;
                        for (int i = 0; i < ba.CELL_TEMPERATURE.Length; i++)
                        {
                            ba.CELL_TEMPERATURE[i] = Math.Round(ba.CELL_TEMPERATURE[i], 2);
                            if ((i % 3) != 0) // inner not count
                            {
                                double cell = ba.CELL_TEMPERATURE[i];
                                if (isFirst)
                                {
                                    isFirst = false;
                                    minTemp = cell;
                                    maxTemp = cell;
                                }

                                if (cell > maxTemp)
                                {
                                    maxTemp = cell;
                                }

                                if (cell < minTemp)
                                {
                                    minTemp = cell;
                                }
                            }
                        }
                        ba.TEMPERATURE = maxTemp;
                        ba.MAX_TEMPERATURE = maxTemp;
                        ba.MIN_TEMPERATURE = minTemp;
                        if (ba.MIN_TEMPERATURE < 0 || ba.PROTECTION[5] > 0)
                        {
                            ba.TEMPERATURE = minTemp;
                        }
                    }
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_LAST_CHARGE_END, 1);
                if (value != null)
                {
                    ba.LAST_CHARGE = value as int[];
                }

                value = mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_LAST_DISCHARGE_END, 1);
                if (value != null)
                {
                    ba.LAST_DISCHARGE = value as int[];
                }

                ba.timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.000Z");
            }
            catch (Exception ex)
            {
                return null;
            }
            
            return ba;
        }

        public override void SetDeviceRTCTime(DateTime dateTime)
        {
            if (mDevice.IsConnect())
            {
                SetBatteryEngMode(true);
                IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_SEC, Convert.ToByte(dateTime.Second)));
                IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_MIN, Convert.ToByte(dateTime.Minute)));
                IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_HOUR, Convert.ToByte(dateTime.Hour)));
                IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_DAY, Convert.ToByte(dateTime.Day)));
                IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_MONTH, Convert.ToByte(dateTime.Month)));
                IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_YEAR, Convert.ToByte(dateTime.Year - DEFAULTYEAR)));
                SetBatteryEngMode(false);
            }
        }

        public override void SetBatteryEngMode(bool isEngMode)
        {
            if (isEngMode)
            {
                mDevice.SendBatteryCmd(BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET, Convert.ToByte(0x83));
            }
            else
            {
                mDevice.SendBatteryCmd(BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET, Convert.ToByte(0x03));
            }
        }

        public override string GetBarcode()
        {
            string barcode = null;
           object  value = mDevice.SendBatteryCmd(BatteryCmd.CMD_BARCODE_DATA_GET, 1);
            if (value != null)
            {
                barcode = value.ToString();
            }
            else
            {
                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_BATTERY_NO_GET));
                if (value != null)
                {
                    barcode = value.ToString();
                }
            }

            return barcode;
        }

        public override int[] GetDeviceRTCTime()
        {
            int[] dt = new int[6];
            object value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_RTC_REG_DUMP));
            if (value != null)
            {
                dt = (int[])value;
            }

            return dt;
        }

        public override bool ResetMCU()
        {
            object value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_RESET_BATTERY_MCU));
            return IsLegalData(value) != null;
        }

        public override int[] GetDeviceRTCResetTime()
        {
            int[] dt = new int[6];
            object value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_TIMESET));
            if (value != null)
            {
                dt = (int[])value;
            }

            return dt;
        }

        public override int[] GetDeviceVoltage()
        {
            object value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_VOLTAGE_GET));
            if (value != null)
            {
                return (int[])value;
            }
            return null;
        }

        public override bool SetPBData(List<PBAttributeInfo> pbAttributeInfoList)
        {
            bool isSuccess = true;


            SetBatteryEngMode(true);
            foreach (PBAttributeInfo data in pbAttributeInfoList)
            {
                isSuccess = isSuccess && mDevice.SetPBEEPData(data.PBCmd, data.PBIndex, data.PBData);
            }
            SetBatteryEngMode(false);

            if (isSuccess)
            {
                object ret = mDevice.SendBatteryCmd(BatteryCmd.CMD_RESET_PBPOWER);

                if (ret != null)
                {
                    isSuccess = (bool)ret;

                    if (isSuccess)
                    {
                        isSuccess = mDevice.RefreshPBEEP();

                        //System.Threading.Thread.Sleep(10);

                        foreach (PBAttributeInfo data in pbAttributeInfoList)
                        {
                            object value = mDevice.GetPBEEPData(data.PBCmd, data.PBIndex);
                            if (value != null && data.PBData != null)
                            {
                                Type type1 = value.GetType();
                                Type type2 = data.PBData.GetType();
                                if (type1.Name == type2.Name)
                                {
                                    switch (type1.Name)
                                    {
                                        case "Boolean":
                                        case "String":
                                        case "Int16":
                                        case "Int32":
                                        case "Int64":
                                        case "UInt16":
                                        case "UInt32":
                                        case "UInt64":
                                            {
                                                isSuccess = isSuccess && value.ToString().Equals(data.PBData.ToString());
                                                break;
                                            }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return isSuccess;
        }
    }
}
