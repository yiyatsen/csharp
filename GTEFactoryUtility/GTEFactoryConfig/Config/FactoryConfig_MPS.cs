﻿using GTE.Command;
using GTEFactoryConfig.InfoModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace GTEFactoryConfig.Config
{
    public class FactoryConfig_MPS : FactoryDeviceAdapter
    {
        /// <summary>
        /// MPS表示MCU類型
        /// 2表示加密版
        /// 1表示第一版
        /// </summary>
        public class FactoryConfgAttr_MPS
        {
            #region Property
            public long DC { get; set; }
            public long FCC { get; set; }
            public long CycleCount { get; set; }
            public string RC { get; set; }
            public long DV { get; set; }
            public string DeviceName { get; set; }
            public string SN { get; set; }
            public string MCUVer { get; set; }
            public string ManufactureDate { get; set; }

            public long MinRC { get; set; }
            public long MaxSN { get; set; }
            public long MinSN { get; set; }
            #endregion
        }



        public FactoryConfig_MPS(GTEBatteryLib.BatteryAPI device)
            : base(device)
        {

        }


        protected override void SetDeviceAttrList()
        {
            mDeviceAttrNameList = new string[]{ "DC", "FCC" 
                , "CycleCount", "RC", "DV", "DeviceName", "SN", "MCUVer", "ManufactureDate" };
        }

        public override string SetDefaultAttrValue(string json)
        {
            string result = null; //如果有預設值不回傳
            try
            {
                using (var stringReader = new System.IO.StringReader(json))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    mDeviceConfig = (FactoryConfgAttr_MPS)serializer.Deserialize(stringReader, typeof(FactoryConfgAttr_MPS));
                }
            }
            catch
            {
                FactoryConfgAttr_MPS config = new FactoryConfgAttr_MPS();

                config.DeviceName = "None";
                config.ManufactureDate = "None";
                config.MCUVer = "None";
                config.MaxSN = 65535;
                config.MinSN = 0;
                config.SN = string.Format("{0}>SN>{1}", config.MaxSN, config.MinSN);
                config.RC = string.Format(">{0}", config.MinRC);
                config.MinRC = 0;

                mDeviceConfig = config;

                result = ConvertDataToString();
            }

            return result;
        }

        public override string ConvertDataToDefaultAttr(Dictionary<string, string> data)
        {
            string result = null;

            if (mDeviceConfig != null && data != null)
            {
                foreach (string propertyName in data.Keys)
                {
                    PropertyInfo propertyInfo = mDeviceConfig.GetType().GetProperty(propertyName);

                    switch (propertyInfo.Name)
                    {
                        case "RC":
                            {
                                string[] dataItem = data[propertyName].Split('>');

                                long value = 0;
                                bool isSuccess = long.TryParse(dataItem[1], out value);
                                if (isSuccess)
                                {
                                    ((FactoryConfgAttr_MPS)mDeviceConfig).MinRC = value;
                                }
                                ((FactoryConfgAttr_MPS)mDeviceConfig).RC = data[propertyName];
                                break;
                            }
                        case "SN":
                            {
                                string[] dataItem = data[propertyName].Split('>');

                                long value = 0;
                                bool isSuccess = long.TryParse(dataItem[0], out value);
                                if (isSuccess)
                                {
                                    ((FactoryConfgAttr_MPS)mDeviceConfig).MaxSN = value;
                                }

                                value = 0;
                                isSuccess = long.TryParse(dataItem[2], out value);
                                if (isSuccess)
                                {
                                    ((FactoryConfgAttr_MPS)mDeviceConfig).MinSN = value;
                                }
                                ((FactoryConfgAttr_MPS)mDeviceConfig).SN = data[propertyName];
                                break;
                            }
                        default:
                            {
                                switch (propertyInfo.PropertyType.Name)
                                {
                                    case "Int64":
                                        {
                                            long value = 0;
                                            bool isSuccess = long.TryParse(data[propertyName], out value);
                                            if (isSuccess)
                                            {
                                                propertyInfo.SetValue(mDeviceConfig, value, null);
                                            }
                                            break;
                                        }
                                    case "String":
                                        {
                                            propertyInfo.SetValue(mDeviceConfig, data[propertyName], null);
                                            break;
                                        }
                                    case "Double":
                                        {
                                            double value = 0;
                                            bool isSuccess = double.TryParse(data[propertyName], out value);
                                            if (isSuccess)
                                            {
                                                propertyInfo.SetValue(mDeviceConfig, value, null);
                                            }

                                            propertyInfo.SetValue(mDeviceConfig, value, null);
                                            break;
                                        }
                                }

                                break;
                            }
                    }
                }
                result = ConvertDataToString();
            }
            return result;
        }

        private string ConvertDataToString()
        {
            string result = null;
            try
            {
                using (StringWriter sw = new StringWriter())
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    jw.Formatting = Formatting.Indented;

                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(jw, mDeviceConfig);
                    result = sw.ToString();
                }
            }
            catch { }
            return result;
        }

        public override object ConvertToListViewItem(object data, bool isReadOnly)
        {
            List<DeviceInfoItem> items = new List<DeviceInfoItem>();
            FactoryConfgAttr_MPS item = data as FactoryConfgAttr_MPS;
            if (item != null)
            {
                for (int i = 0; i < mDeviceAttrNameList.Length; i++)
                {
                    PropertyInfo propertyInfo = mDeviceConfig.GetType().GetProperty(mDeviceAttrNameList[i]);
                    bool isEqual = CheckValueEqual(propertyInfo, propertyInfo.GetValue(item, null));

                    items.Add(new DeviceInfoItem()
                    {
                        Item = mDeviceAttrNameList[i],
                        DefaultValue = propertyInfo.GetValue(mDeviceConfig, null).ToString(),
                        RealValue = propertyInfo.GetValue(item, null).ToString(),
                        IsReadOnly = isReadOnly,
                        Result = isEqual
                    });
                }
            }

            return items;
        }

        protected override bool CheckValueEqual(PropertyInfo propertyInfo, object data)
        {
            bool isSuccess = false;
            if (mDeviceConfig != null && propertyInfo != null && data != null)
            {
                switch (propertyInfo.Name)
                {
                    case "RC":
                        {
                            long value = 0;
                            isSuccess = long.TryParse(data.ToString(), out value);
                            if (isSuccess)
                            {
                                isSuccess = (value > ((FactoryConfgAttr_MPS)mDeviceConfig).MinRC);
                            }
                            break;
                        }
                    case "SN":
                        {
                            long value = 0;
                            isSuccess = long.TryParse(data.ToString(), out value);
                            if (isSuccess)
                            {
                                isSuccess = (((FactoryConfgAttr_MPS)mDeviceConfig).MaxSN > value) && (value > ((FactoryConfgAttr_MPS)mDeviceConfig).MinSN);
                            }
                            break;
                        }
                    default:
                        {
                            isSuccess = data.Equals(propertyInfo.GetValue(mDeviceConfig, null));

                            break;
                        }
                }
            }
            return isSuccess;
        }

        public override object GetDeviceAllInformation()
        {
            FactoryConfgAttr_MPS config = null;
            bool isSuccess = true;
            //isSuccess = mDevice.RefreshMCUEEP();
            //isSuccess = isSuccess && mDevice.RefreshPBEEP();

            if (isSuccess)
            {
                 config = new FactoryConfgAttr_MPS();

                //object value = IsLegalData(mDevice.GETMCUEEPData(BatteryEEPAttrCmd.EDV2Value));
                //if (value != null)
                //{
                //    long numberValue = 0;
                //    long.TryParse(value.ToString(), out numberValue);
                //    config.EDV = numberValue;
                //}

                object value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_DESIGN_CAPACITY_GET));
                if (value != null)
                {
                    long numberValue = 0;
                    long.TryParse(value.ToString(), out numberValue);
                    config.DC = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_FULL_CC_GET));
                if (value != null)
                {
                    long numberValue = 0;
                    long.TryParse(value.ToString(), out numberValue);
                    config.FCC = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_CYCLE_COUNT_GET));
                if (value != null)
                {
                    long numberValue = 0;
                    long.TryParse(value.ToString(), out numberValue);
                    config.CycleCount = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_REMAIN_CAPACITY_GET));
                if (value != null)
                {
                    config.RC = value.ToString();
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_DESIGN_VOLTAGE_GET));
                if (value != null)
                {
                    long numberValue = 0;
                    long.TryParse(value.ToString(), out numberValue);
                    config.DV = numberValue;
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_DEVICE_NAME_GET));
                if (value != null)
                {
                    config.DeviceName = value.ToString();
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_BATTERY_NO_GET));
                if (value != null)
                {
                    config.SN = value.ToString();
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_MCU_VERSION_GET));
                if (value != null)
                {
                    config.MCUVer = value.ToString();
                }

                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_MANUFACTURE_DATE_GET));
                int[] data = (int[])value;
                if (data != null)
                {
                    config.ManufactureDate = string.Format("{0:0000}/{1:00}/{2:00}", data[0], data[1], data[2]);
                }
            }

            return config;
        }


        public override object GetBatteryNewAllInformation()
        {
            return null;
        }

        public override void SetDeviceRTCTime(DateTime dateTime)
        {
            if (mDevice.IsConnect())
            {
                IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_SEC, Convert.ToByte(dateTime.Second)));
                IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_MIN, Convert.ToByte(dateTime.Minute)));
                IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_HOUR, Convert.ToByte(dateTime.Hour)));
                IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_DAY, Convert.ToByte(dateTime.Day)));
                IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_MONTH, Convert.ToByte(dateTime.Month)));
                IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_SET_YEAR, Convert.ToByte(dateTime.Year - DEFAULTYEAR)));
            }
        }

        public override void SetBatteryEngMode(bool isEngMdoe)
        {

        }

        public override string GetBarcode()
        {
            string barcode = null;
            object value = mDevice.SendBatteryCmd(BatteryCmd.CMD_BARCODE_DATA_GET, 1);
            if (value != null)
            {
                barcode = value.ToString();
            }
            else
            {
                value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_BATTERY_NO_GET));
                if (value != null)
                {
                    barcode = value.ToString();
                }
            }

            return barcode;
        }

        public override int[] GetDeviceRTCTime()
        {
            int[] dt = new int[6];
            object value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_TIMESET));
            if (value != null)
            {
                dt = (int[])value;
            }

            //object value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_SEC));
            //if (IsLegalData(value) != null)
            //{
            //    dt[5] = Convert.ToInt32(value);
            //}

            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_MIN));
            //if (IsLegalData(value) != null)
            //{
            //    dt[4] = Convert.ToInt32(value);
            //}

            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_HOURS));
            //if (IsLegalData(value) != null)
            //{
            //    dt[3] = Convert.ToInt32(value);
            //}

            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_DAY));
            //if (IsLegalData(value) != null)
            //{
            //    dt[2] = Convert.ToInt32(value);
            //}

            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_MONTH));
            //if (IsLegalData(value) != null)
            //{
            //    dt[1] = Convert.ToInt32(value);
            //}

            //value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_YEAR));
            //if (IsLegalData(value) != null)
            //{
            //    dt[0] = Convert.ToInt32(value);
            //}

            return dt;
        }

        public override bool ResetMCU()
        {
            object value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_RESET_BATTERY_MCU));
            return IsLegalData(value) != null;
        }

        public override int[] GetDeviceRTCResetTime()
        {
            int[] dt = new int[6];
            object value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_TIMESET));
            if (IsLegalData(value) != null)
            {
                dt = (int[])value;
            }

            return dt;
        }

        public override int[] GetDeviceVoltage()
        {
            int[] dt = new int[6];
            object value = IsLegalData(mDevice.SendBatteryCmd(BatteryCmd.CMD_GET_TIMESET));
            if (value != null)
            {
                dt = (int[])value;
            }

            return null;
        }

        public override bool SetPBData(List<PBAttributeInfo> pbAttributeInfoList)
        {
            return false;
        }
    }
}
