﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChargeDeviceUploadModel
{
    public enum UploadActionCode : int
    {
        DeviceLogin = 1,
        ChargeDeviceSlotRecord = 2,
        TransRecord = 3,
        ChargeTransRecord = 4,
        AbnormalRecord = 5
    }

    public class DeviceUploadFormat
    {
        public string SecurityKey { get; set; }
        public string TimeStamp { get; set; }
        public UploadActionCode ActionCode { get; set; }
        public string Message { get; set; }
    }
}
