﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace ChargeDeviceUploadModel
{
    [Table("ChargeDeviceRecord")]
    public class ChargeDeviceRecord
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ChargeDeviceRecordIndex { get; set; }
        [Required]
        public string StationId { get; set; }
        [Required]
        public int DeviceTypeId { get; set; }

        public long TimeStamp { get; set; }

        public virtual IList<SlotRecord> SlotRecords { get; set; }

        public ChargeDeviceRecord()
        {
            SlotRecords = new List<SlotRecord>();
        }
    }
}
