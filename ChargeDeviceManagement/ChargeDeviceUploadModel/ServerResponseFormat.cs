﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChargeDeviceUploadModel
{
    public enum ResponseActionCode : int
    {
        UploadSuccess = 0,
        UploadFail = 1,
        ErrorSecurityKey = 2,
        LoginSuccess = 99
    }

    public class ServerResponseFormat
    {
        public int ActionCode { get; set; }
        public string Message { get; set; }
    }
}
