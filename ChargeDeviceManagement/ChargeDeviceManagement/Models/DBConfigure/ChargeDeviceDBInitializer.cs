﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ChargeDeviceManagement.Models.DBConfigure
{
    public class ChargeDeviceDBInitializer : CreateDatabaseIfNotExists<ChargeDeviceDBContext>
    {
        protected override void Seed(ChargeDeviceDBContext context)
        {
            var data0 = new List<UserInfo>{
                new UserInfo{
                    Account = "gtenergy.admin",
                    Password = "1234",
                    Email = "gtenergy.developer@gmail.com",
                    Address = "新北市汐止區大同路二段175號11樓之1",
                    UserName = "系統管理者",
                    Phone = "0277080899",
                    UserRoleCode = Models.UserRoleCode.Administrator,
                    RegisterDateTime = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds
                },
                new UserInfo{
                    Account = "oodobike",
                    Password = "oodobike",
                    Email = "gtenergy.developer@gmail.com",
                    Address = "屏東縣恆春鎮萬里路27-8號",
                    UserName = "歐多賣交換系統",
                    Phone = "088869999",
                    UserRoleCode = Models.UserRoleCode.Cooperator,
                    RegisterDateTime = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds
                },
                new UserInfo{
                    Account = "YZU",
                    Password = "yzu",
                    Email = "service@skuromoto.tw",
                    Address = "桃園市中壢區遠東路135號",
                    UserName = "元智大學",
                    Phone = "0903100456",
                    UserRoleCode = Models.UserRoleCode.Cooperator,
                    RegisterDateTime = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds
                },
                new UserInfo{
                    Account = "gtenergy.developer",
                    Password = "1234",
                    Email = "gtenergy.developer@gmail.com",
                    Address = "新北市汐止區大同路二段175號11樓之1",
                    UserName = "高達能源開發者",
                    Phone = "0277080899",
                    UserRoleCode = Models.UserRoleCode.GTEnergyManager,
                    RegisterDateTime = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds
                }
            };
            data0.ForEach(d => context.UserInfo.Add(d));

            var data1 = new List<GroupInfo>{
                new GroupInfo{
                    GroupName = "歐多賣交換系統",
                    IsEnable = true,
                },
               new GroupInfo{
                    GroupName = "SKUROMOTO",
                    IsEnable = true,
                },
               new GroupInfo{
                    GroupName = "高達能源交換系統",
                    IsEnable = true,
                }
            };
            data1.ForEach(d => context.GroupInfo.Add(d));

            context.SaveChanges();

            data1 = new List<GroupInfo>{
                new GroupInfo{
                    GroupName = "高達能源測試交換系統",
                    ParentGroupId = 3,
                    IsEnable = true,
                }
            };
            data1.ForEach(d => context.GroupInfo.Add(d));

            var data3 = new List<UserGroupRelationship>{
                new UserGroupRelationship{
                    UserId = 2,
                    GroupId =1,
                    GroupRoleCode = Models.UserGroupRoleCode.Manager
                },
                new UserGroupRelationship{
                    UserId = 3,
                    GroupId =2,
                    GroupRoleCode = Models.UserGroupRoleCode.Manager
                },
                new UserGroupRelationship{
                    UserId = 4,
                    GroupId =1,
                    GroupRoleCode = Models.UserGroupRoleCode.Manager
                },
                new UserGroupRelationship{
                    UserId = 4,
                    GroupId =2,
                    GroupRoleCode = Models.UserGroupRoleCode.Manager
                }
            };
            data3.ForEach(d => context.UserGroupRelationship.Add(d));

            context.SaveChanges();

            var data4 = new List<CardInfo>{
                new CardInfo{
                    CardId = "ABCDE67890",
                    UserId = 4,
                    GroupId =1,
                    CardRoleCode = 0
                }
            };
            data4.ForEach(d => context.CardInfo.Add(d));

            var data5 = new List<ChargeDeviceType>{
                new ChargeDeviceType{
                    DeviceTypeName = "Kiosk"
                },
                new ChargeDeviceType{
                    DeviceTypeName = "Smart Charger"
                }
            };
            data5.ForEach(d => context.ChargeDeviceType.Add(d));

            var data6 = new List<BatteryDeviceType>{
                new BatteryDeviceType{
                    DeviceTypeName = "4816",
                    DesignCapacity = 17200
                },
                new BatteryDeviceType{
                    DeviceTypeName = "4840"
                }
            };
            data6.ForEach(d => context.BatteryDeviceType.Add(d));

            context.SaveChanges();

            var data7 = new List<ChargeDeviceInfo>{
                new ChargeDeviceInfo{
                    GroupId = 3,
                    DeviceTypeId = 1,
                    StationId = "0000000001",
                    RegisterDateTime = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds,
                }
            };
            data7.ForEach(d => context.ChargeDeviceInfo.Add(d));

            base.Seed(context);
        }
    }
}