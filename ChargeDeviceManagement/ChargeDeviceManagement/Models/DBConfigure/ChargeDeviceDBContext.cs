﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ChargeDeviceManagement.Models.DBConfigure
{
    public class ChargeDeviceDBContext : DbContext
    {
        public virtual DbSet<UserInfo> UserInfo { get; set; }
        public virtual DbSet<GroupInfo> GroupInfo { get; set; }

        public virtual DbSet<UserGroupRelationship> UserGroupRelationship { get; set; }
        public virtual DbSet<CardInfo> CardInfo { get; set; }

        public virtual DbSet<ChargeDeviceType> ChargeDeviceType { get; set; }
        public virtual DbSet<ChargeDeviceInfo> ChargeDeviceInfo { get; set; }

        public virtual DbSet<BatteryDeviceType> BatteryDeviceType { get; set; }
        public virtual DbSet<BatteryDeviceInfo> BatteryDeviceInfo { get; set; }

        //public virtual DbSet<ChargeDeviceRecord> ChargeDeviceRecord { get; set; }
        public virtual DbSet<SlotRecord> SlotRecord { get; set; }
        public virtual DbSet<SlotBatteryRecord> SlotBatteryRecord { get; set; }

        public virtual DbSet<TransRecord> TransRecord { get; set; }
        public virtual DbSet<TransDeviceRecord> TransDeviceRecord { get; set; }

        public virtual DbSet<ChargeTransRecord> ChargeTransRecord { get; set; }
        public virtual DbSet<ChargeTransDeviceRecord> ChargeTransDeviceRecord { get; set; }

        public virtual DbSet<AbnormalRecord> AbnormalRecord { get; set; }
        public virtual DbSet<AbnormalDeviceRecord> AbnormalDeviceRecord { get; set; }

        public ChargeDeviceDBContext()
            : base("name=ChargeDeviceDBContext")
        {
            //this.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserInfoMapping());
            modelBuilder.Configurations.Add(new GroupInfoMapping());

            modelBuilder.Configurations.Add(new UserGroupRelationshipMapping());
            modelBuilder.Configurations.Add(new CardInfoMapping());

            modelBuilder.Configurations.Add(new ChargeDeviceTypeMapping());
            modelBuilder.Configurations.Add(new ChargeDeviceInfoMapping());

            modelBuilder.Configurations.Add(new BatteryDeviceTypeMapping());
            modelBuilder.Configurations.Add(new BatteryDeviceInfoMapping());

            //modelBuilder.Configurations.Add(new ChargeDeviceRecordMapping());
            modelBuilder.Configurations.Add(new SlotRecordMapping());
            modelBuilder.Configurations.Add(new SlotBatteryRecordMapping());

            modelBuilder.Configurations.Add(new TransRecordMapping());
            modelBuilder.Configurations.Add(new TransDeviceRecordMapping());

            modelBuilder.Configurations.Add(new ChargeTransRecordMapping());
            modelBuilder.Configurations.Add(new ChargeTransDeviceRecordMapping());

            modelBuilder.Configurations.Add(new AbnormalRecordMapping());
            modelBuilder.Configurations.Add(new AbnormalDeviceRecordMapping());
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}