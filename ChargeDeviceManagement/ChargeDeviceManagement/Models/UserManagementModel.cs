﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ChargeDeviceManagement.Models
{
    public class RegisterCard
    {
        public string Account { get; set; }
        public string Password { get; set; }
        public string CardId { get; set; }
        public string UserId { get; set; }
    }

    /// <summary>
    /// Defined User Role
    /// </summary>
    public enum UserRoleCode : int
    {
        Remove = -2,
        Ban = -1,
        None = 0,
        NonActivity = 1, // for 認證
        User = 2,  // 會員 (可以"租?"借或是買"賣?")
        Cooperator = 4, // 營運商
        GTEnergyManager = 8,
        Administrator = 16 // => Only one, System administrator for GoTech
    }

    public enum UserGroupRoleCode : int
    {
        Remove = -2,
        Ban = -1,
        NonActivity = 0,
        User = 1,
        Monitor = 2,
        Manager = 3
    }

    public enum CardRoleCode : int
    {
        Remove = -2,
        Ban = -1,
        NonActivity = 0,
        Normal = 1,
        Manager = 2
    }

    [Table("GroupInfo")]
    public class GroupInfo
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GroupId { get; set; }
        [Required]
        public string GroupName { get; set; }

        public int? ParentGroupId { get; set; }
        [JsonIgnore]
        public GroupInfo ParentGroupInfo { get; set; }

        public string Description { get; set; }

        public bool IsEnable { get; set; } // Did the user can be use
        public bool IsRemove { get; set; } // Did the user be removed 

        [JsonIgnore]
        public virtual IList<GroupInfo> ChildGroupInfos { get; set; }
        [JsonIgnore]
        public virtual IList<CardInfo> CardInfos { get; set; }
        [JsonIgnore]
        public virtual IList<UserGroupRelationship> UserGroupRelationships { get; set; }

        public GroupInfo()
        {
            CardInfos = new List<CardInfo>();
            ChildGroupInfos = new List<GroupInfo>();
            UserGroupRelationships = new List<UserGroupRelationship>();
        }
    }

    public class UserGroupRelationship
    {
        [ForeignKey("GroupInfo"), Column(Order = 0), Key]
        public int GroupId { get; set; }
        [JsonIgnore]
        public virtual GroupInfo GroupInfo { get; set; }
        [ForeignKey("UserInfo"), Column(Order = 1), Key]
        public int UserId { get; set; }
        [JsonIgnore]
        public virtual UserInfo UserInfo { get; set; }

        public string Description { get; set; }

        [DefaultValue(UserGroupRoleCode.User)]
        public UserGroupRoleCode GroupRoleCode { get; set; }

        public bool IsRemove { get; set; } // Did the user be removed 
    }

    [Table("UserInfo")]
    public class UserInfo
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        [Required]
        public string Account { get; set; } // = Email
        [Required, EmailAddress]
        public string Email { get; set; } // = Email
        [Required]
        public string Password { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Address { get; set; }
        [DefaultValue(UserRoleCode.NonActivity)]
        public UserRoleCode UserRoleCode { get; set; }

        public string Description { get; set; }

        public long RegisterDateTime { get; set; }

        public bool IsRemove { get; set; } // Did the user be removed 

        [JsonIgnore]
        public virtual IList<CardInfo> CardInfos { get; set; }
        [JsonIgnore]
        public virtual IList<UserGroupRelationship> UserGroupRelationships { get; set; }

        public UserInfo()
        {
            CardInfos = new List<CardInfo>();
            UserGroupRelationships = new List<UserGroupRelationship>();

        }
    }

    public class CardInfo
    {
        [Key, Column(Order = 0)]
        public string CardId { get; set; }
        [ForeignKey("UserInfo"), Column(Order = 1)]
        public int UserId { get; set; }
        [JsonIgnore]
        public virtual UserInfo UserInfo { get; set; }
        [ForeignKey("GroupInfo"), Column(Order = 2)]
        public int GroupId { get; set; }
        [JsonIgnore]
        public virtual GroupInfo GroupInfo { get; set; }
        [DefaultValue(CardRoleCode.NonActivity)]
        public CardRoleCode CardRoleCode { get; set; } // -1: ban, 0: non-Active, 1: normal, 2: manager

        public string Description { get; set; }
        public bool IsRemove { get; set; }
    }
}