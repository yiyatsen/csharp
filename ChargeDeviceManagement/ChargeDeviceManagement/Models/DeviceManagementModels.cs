﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ChargeDeviceManagement.Models
{
    public enum TransStatusCode : int
    {
        Success = 0,
        Fail =1,
        FailByNoCard = 2,
        FailByNoBattery = 3,
        FailByCheck = 4,
        FailByDrawOut = 5
    }

    public enum UploadActionCode : int
    {
        DeviceLogin = 1,
        ChargeDeviceSlotRecord = 2,
        TransRecord = 3,
        ChargeTransRecord = 4,
        AbnormalRecord = 5,
        RegisterDevice = 999
    }

    public class DeviceUploadFormat
    {
        public string SecurityKey { get; set; }
        public string TimeStamp { get; set; }
        public UploadActionCode ActionCode { get; set; }
        public string Message { get; set; }
    }

    public enum ResponseActionCode : int
    {
        UploadSuccess = 0,
        UploadFail = 1,
        ErrorSecurityKey = 2,
        LoginSuccess = 99,
        RegisterFail = 998,
        RegisterSuccess = 999
    }

    public enum DeviceOwnerType : int
    {
        None = 0,
        User = 1,
        Group =2
    }

    public class ServerResponseFormat
    {
        public ResponseActionCode ActionCode { get; set; }
        public string Message { get; set; }
    }

    public class LoginFormat
    {
        public string StationId { get; set; }
        public int DeviceTypeId { get; set; }
    }

    public class LoginResponseFormat
    {
        public string SecurityKey { get; set; }
        public int UpdateCode { get; set; }
        
    }

    [Table("ChargeDeviceType")]
    public class ChargeDeviceType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DeviceTypeId { get; set; }
        [Required]
        public string DeviceTypeName { get; set; }

        public string Description { get; set; }
        [JsonIgnore]
        public virtual IList<ChargeDeviceInfo> ChargeDeviceInfos { get; set; }

        public ChargeDeviceType()
        {
            ChargeDeviceInfos = new List<ChargeDeviceInfo>();
        }
    }

    public enum DeviceStatusCode
    {
        Remove = -2,
        Ban = -1,
        NonActivity = 0,
        Useable = 1
    }

    [Table("ChargeDeviceInfo")]
    public class ChargeDeviceInfo
    {
        [Key, Column(Order = 0)]
        public string StationId { get; set; }
        [ForeignKey("ChargeDeviceType"), Column(Order = 1), Key]
        public int DeviceTypeId { get; set; } //carrier
        public virtual ChargeDeviceType ChargeDeviceType { get; set; }
        
        // Charge Device Information ...
        public DeviceOwnerType DeviceOwnerType { get; set; }
        public int OwnerId { get; set; }

        public string DeviceMac { get; set; }
        public string LastOnlineIP { get; set; }
        public long LastOnlineTime { get; set; }
        public bool IsOnline { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

        public int SlotNumber { get; set; }

        public DeviceStatusCode DeviceStatusCode { get; set; }

        // Device Register Info
        public long RegisterDateTime { get; set; }
        public long ManufacturedDateTime { get; set; }
        public string OSVersion { get; set; }
        public string APVersion { get; set; }

        public string Description { get; set; }
    }

    [Table("BatteryDeviceType")]
    public class BatteryDeviceType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DeviceTypeId { get; set; }
        [Required]
        public string DeviceTypeName { get; set; }

        // type information
        public int DesignVoltage { get; set; }
        public int DesignCapacity { get; set; }

        public int DesignCellsNumber { get; set; }

        public string Description { get; set; }

        [JsonIgnore]
        public virtual IList<BatteryDeviceInfo> BatteryDeviceInfos { get; set; }

        public BatteryDeviceType()
        {
            BatteryDeviceInfos = new List<BatteryDeviceInfo>();
        }
    }

    [Table("BatteryDeviceInfo")]
    public class BatteryDeviceInfo
    {
        [Key, Column(Order = 0)]
        public string BatteryId { get; set; }
        [Key, Column(Order = 1)]
        public string ManufactureDate { get; set; }
        [ForeignKey("BatteryDeviceType"), Column(Order = 2), Key]
        public int DeviceTypeId { get; set; }
        public virtual BatteryDeviceType BatteryDeviceType { get; set; }

        // Device Register Info
        public DeviceOwnerType DeviceOwnerType { get; set; }
        public int OwnerId { get; set; }

        public DeviceStatusCode DeviceStatusCode { get; set; }

        public long RegisterDateTime { get; set; }
        public long ManufacturedDateTime { get; set; }
        public long LastOnlineTime { get; set; }
        public string APVersion { get; set; }
        public string Description { get; set; }
    }

    [Table("SlotRecord")]
    public class SlotRecord
    {
        [Key, Column(Order = 0)]
        public long TimeStamp { get; set; }
        [Key, Column(Order = 1)]
        public int SlotId { get; set; }
        [Key, Column(Order = 2)]
        public string StationId { get; set; }
        [Key, Column(Order = 3)]
        public int DeviceTypeId { get; set; }

        public long SlotRecordId { get; set; }

        public int SlotStatusCode { get; set; }

        public long? SlotBatteryRecordIndex { get; set; }

        public long SlotBatteryRecordId { get; set; }
        public virtual SlotBatteryRecord SlotBatteryRecord { get; set; }

        public string Description { get; set; }
    }

    [Table("SlotBatteryRecord")]
    public class SlotBatteryRecord
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SlotBatteryRecordIndex { get; set; }

        [ForeignKey("SlotRecord"), Column(Order = 1)]
        public long TimeStamp { get; set; }
        [ForeignKey("SlotRecord"), Column(Order = 2)]
        public int SlotId { get; set; }
        [ForeignKey("SlotRecord"), Column(Order = 3)]
        public string StationId { get; set; }
        [ForeignKey("SlotRecord"), Column(Order = 4)]
        public int DeviceTypeId { get; set; }

        [JsonIgnore]
        public virtual SlotRecord SlotRecord { get; set; }

        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }

        public long SlotBatteryRecordId { get; set; }

        public int SOC { get; set; }
        public double Temperature { get; set; }
        public int Current { get; set; }
        public int Voltage { get; set; }
        public int VMax { get; set; }
        public int VMin { get; set; }
        public int Protection { get; set; }

        public string Description { get; set; }
    }

    [Table("TransRecord")]
    public class TransRecord
    {
        [Key, Column(Order = 0)]
        public long TransTimeStamp { get; set; }
        [Key, Column(Order = 1)]
        public string StationId { get; set; }
        [Key, Column(Order = 2)]
        public int DeviceTypeId { get; set; }
        [Key, Column(Order = 3)]
        public long TransRecordId { get; set; }

        public string UserId { get; set; }
        public string CardId { get; set; }
        public int TransStatusCode { get; set; }

        public int OldPoint { get; set; }
        public int Point { get; set; }
        public long? InsertedTransDeviceIndex { get; set; }
        public long InsertedTransDeviceId { get; set; }
        public virtual TransDeviceRecord InsertedTransDevice { get; set; }
        public long? DrawOutTransDeviceIndex { get; set; }
        public long DrawOutTransDeviceId { get; set; }
        public virtual TransDeviceRecord DrawOutTransDevice { get; set; }

        public string Description { get; set; }
    }
    [Table("TransDeviceRecord")]
    public class TransDeviceRecord
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long TransDeviceRecordIndex { get; set; }
        public long TransDeviceRecordId { get; set; }

        [ForeignKey("TransRecord"), Column(Order = 1)]
        public long TransTimeStamp { get; set; }
        [ForeignKey("TransRecord"), Column(Order = 2)]
        public string StationId { get; set; }
        [ForeignKey("TransRecord"), Column(Order = 3)]
        public int DeviceTypeId { get; set; }
        [ForeignKey("TransRecord"), Column(Order = 4)]
        public long TransRecordId { get; set; }
        [JsonIgnore]
        public virtual TransRecord TransRecord { get; set; }

        public int SlotId { get; set; }
        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public string OperatorId { get; set; }

        public int SOC { get; set; }
        public string CellsVoltage { get; set; }
        public int Protection { get; set; }

        public string Description { get; set; }
    }

    [Table("ChargeTransRecord")]
    public class ChargeTransRecord
    {
        [Key, Column(Order = 0)]
        public long ChargeTransTimeStamp { get; set; }
        [Key, Column(Order = 1)]
        public int SlotId { get; set; }
        [Key, Column(Order = 2)]
        public string StationId { get; set; }
        [Key, Column(Order = 3)]
        public int DeviceTypeId { get; set; }

        public int ChargeTransStatusCode { get; set; }

        public long ChargeTransRecordId { get; set; }

        public long? StartChargeTransDeviceRecordIndex { get; set; }
        public virtual ChargeTransDeviceRecord StartChargeTransDeviceRecord { get; set; }
        public long? EndChargeTransDeviceRecordIndex { get; set; }
        public virtual ChargeTransDeviceRecord EndChargeTransDeviceRecord { get; set; }

        public string Description { get; set; }
    }

    [Table("ChargeTransDeviceRecord")]
    public class ChargeTransDeviceRecord
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ChargeTransDeviceRecordIndex { get; set; }
        public long ChargeTransDeviceRecordId { get; set; }

        [ForeignKey("ChargeTransRecord"), Column(Order = 1)]
        public long ChargeTransTimeStamp { get; set; }
        [ForeignKey("ChargeTransRecord"), Column(Order = 2)]
        public int SlotId { get; set; }
        [ForeignKey("ChargeTransRecord"), Column(Order = 3)]
        public string StationId { get; set; }
        [ForeignKey("ChargeTransRecord"), Column(Order = 4)]
        public int DeviceTypeId { get; set; }
        [JsonIgnore]
        public virtual ChargeTransRecord ChargeTransRecord { get; set; }

        public long ChargeTimeStamp { get; set; }
        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }

        public bool IsSlave { get; set; }

        public int SOC { get; set; }
        public string CellsVoltage { get; set; }
        public string CellsTemperature { get; set; }
        public long BatteryLife { get; set; }
        public long CycleCount { get; set; }

        public int Protection { get; set; }

        public string Description { get; set; }
    }
        
    [Table("AbnormalRecord")]
    public class AbnormalRecord
    {
        [Key, Column(Order = 0)]
        public long AbnormalTimeStamp { get; set; }        
        [Key, Column(Order = 1)]
        public int SlotId { get; set; }
        [Key, Column(Order = 2)]
        public string StationId { get; set; }
        [Key, Column(Order = 3)]
        public int DeviceTypeId { get; set; }

        public long AbnormalRecordId { get; set; }
        public int AbnormalStatusCode { get; set; }

        public long? AbnormalDeviceRecordIndex { get; set; }
        public virtual AbnormalDeviceRecord AbnormalDeviceRecord { get; set; }
    }
        
    [Table("AbnormalDeviceRecord")]
    public class AbnormalDeviceRecord
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long AbnormalDeviceRecordIndex { get; set; }
        public long AbnormalDeviceRecordId { get; set; }

        [ForeignKey("AbnormalRecord"), Column(Order = 1)]
        public long AbnormalTimeStamp { get; set; }
        [ForeignKey("AbnormalRecord"), Column(Order = 2)]
        public int SlotId { get; set; }
        [ForeignKey("AbnormalRecord"), Column(Order = 3)]
        public string StationId { get; set; }
        [ForeignKey("AbnormalRecord"), Column(Order = 4)]
        public int DeviceTypeId { get; set; }
        [JsonIgnore]
        public virtual AbnormalRecord AbnormalRecord { get; set; }

        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }

        public int SOC { get; set; }
        public string CellsVoltage { get; set; }
        public string CellsTemperature { get; set; }
        public long BatteryLife { get; set; }
        public long CycleCount { get; set; }

        public int Protection { get; set; }
    }
}