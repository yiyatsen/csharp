﻿using ChargeDeviceManagement.Models;
using ChargeDeviceManagement.Models.DBConfigure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChargeDeviceManagement.Controllers
{
    public class DeviceUploadController : ApiController
    {
        ChargeDeviceDBContext db = new ChargeDeviceDBContext();

        [HttpPost]
        public ServerResponseFormat UploadData(DeviceUploadFormat value)
        {
            ServerResponseFormat resultFormat = new ServerResponseFormat();
            resultFormat.ActionCode = ResponseActionCode.UploadFail;
            string message = value.Message;

            try
            {
                switch (value.ActionCode)
                {
                    case UploadActionCode.RegisterDevice:
                        {
                            // UserAccount
                            // password
                            // macAddress

                            break;
                        }
                    case UploadActionCode.DeviceLogin: // Device Online
                        {
                            LoginFormat data = null;
                            using (TextReader file = new StringReader(message))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                data = (LoginFormat)serializer.Deserialize(file, typeof(LoginFormat));
                            }

                            if (data != null)
                            {
                                LoginResponseFormat lrf = new LoginResponseFormat();
                                lrf.SecurityKey = data.StationId + "_" + data.DeviceTypeId;

                                resultFormat.ActionCode = ResponseActionCode.LoginSuccess;
                                resultFormat.Message = Newtonsoft.Json.JsonConvert.SerializeObject(lrf);
                            }

                            break;
                        }
                    case UploadActionCode.ChargeDeviceSlotRecord: // Upload All Slot Record
                        {
                            List<SlotRecord> data = null;
                            using (TextReader file = new StringReader(message))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                data = (List<SlotRecord>)serializer.Deserialize(file, typeof(List<SlotRecord>));
                            }

                            if (data != null)
                            {
                                foreach (SlotRecord sr in data)
                                {
                                    sr.StationId = "0000000001";
                                    sr.DeviceTypeId = 1;

                                    var slotInfo = (from a in db.SlotRecord
                                                    where a.StationId.Equals(sr.StationId) && a.DeviceTypeId.Equals(sr.DeviceTypeId) && a.TimeStamp.Equals(sr.TimeStamp) && a.SlotId.Equals(sr.SlotId)
                                                    select a).SingleOrDefault();

                                    if (slotInfo == null)
                                    {
                                        SlotBatteryRecord sbr = sr.SlotBatteryRecord;
                                        sr.SlotBatteryRecord = null;

                                        db.SlotRecord.Add(sr);
                                        db.SaveChanges();

                                        if (sbr != null)
                                        {
                                            sbr.StationId = sr.StationId;
                                            sbr.DeviceTypeId = sr.DeviceTypeId;
                                            sbr.SlotId = sr.SlotId;
                                            sbr.TimeStamp = sr.TimeStamp;

                                            db.SlotBatteryRecord.Add(sbr);
                                            sr.SlotBatteryRecord = sbr;
                                            db.Entry(sr).State = System.Data.Entity.EntityState.Modified;
                                            db.SaveChanges();
                                        }
                                    }
                                }

                                resultFormat.ActionCode = ResponseActionCode.UploadSuccess;
                                resultFormat.Message = "Success";
                            }

                            break;
                        }
                    case UploadActionCode.TransRecord: // Upload exchange Trans Record
                        {
                            List<TransRecord> data = null;
                            using (TextReader file = new StringReader(message))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                data = (List<TransRecord>)serializer.Deserialize(file, typeof(List<TransRecord>));
                            }

                            if (data != null)
                            {
                                foreach (TransRecord tr in data)
                                {
                                    tr.StationId = "0000000001";
                                    tr.DeviceTypeId = 1;

                                    var transInfo = (from a in db.TransRecord
                                                     where a.TransTimeStamp.Equals(tr.TransTimeStamp) && a.StationId.Equals(tr.StationId) 
                                                     && a.DeviceTypeId.Equals(tr.DeviceTypeId) && a.TransRecordId.Equals(tr.TransRecordId)
                                                     select a).SingleOrDefault();
                                    if (transInfo == null)
                                    {
                                        TransDeviceRecord itdr = tr.InsertedTransDevice;
                                        tr.InsertedTransDevice = null;
                                        TransDeviceRecord dtdr = tr.DrawOutTransDevice;
                                        tr.DrawOutTransDevice = null;

                                        db.TransRecord.Add(tr);
                                        db.SaveChanges();

                                        if (itdr != null || dtdr != null)
                                        {
                                            if (itdr != null)
                                            {
                                                itdr.TransTimeStamp = tr.TransTimeStamp;
                                                itdr.StationId = tr.StationId;
                                                itdr.DeviceTypeId = tr.DeviceTypeId;
                                                itdr.TransRecordId = tr.TransRecordId;

                                                db.TransDeviceRecord.Add(itdr);
                                                tr.InsertedTransDevice = itdr;
                                            }

                                            if (dtdr != null)
                                            {
                                                dtdr.TransTimeStamp = tr.TransTimeStamp;
                                                dtdr.StationId = tr.StationId;
                                                dtdr.DeviceTypeId = tr.DeviceTypeId;
                                                dtdr.TransRecordId = tr.TransRecordId;

                                                db.TransDeviceRecord.Add(dtdr);
                                                tr.DrawOutTransDevice = dtdr;
                                            }

                                            db.Entry(tr).State = System.Data.Entity.EntityState.Modified;
                                            db.SaveChanges();
                                        }
                                    }
                                }

                                resultFormat.ActionCode = ResponseActionCode.UploadSuccess;
                                resultFormat.Message = "Success";
                            }
                            break;
                        }
                    case UploadActionCode.ChargeTransRecord: // Upload Charge Trans Record
                        {
                            List<ChargeTransRecord> data = null;
                            using (TextReader file = new StringReader(message))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                data = (List<ChargeTransRecord>)serializer.Deserialize(file, typeof(List<ChargeTransRecord>));
                            }

                            if (data != null)
                            {
                                foreach (ChargeTransRecord ctr in data)
                                {
                                    ctr.StationId = "0000000001";
                                    ctr.DeviceTypeId = 1;

                                    var chargeTransInfo = (from a in db.ChargeTransRecord
                                                           where a.ChargeTransTimeStamp.Equals(ctr.ChargeTransTimeStamp) && a.StationId.Equals(ctr.StationId)
                                                     && a.DeviceTypeId.Equals(ctr.DeviceTypeId) && a.SlotId.Equals(ctr.SlotId)
                                                     select a).SingleOrDefault();
                                    if (chargeTransInfo == null)
                                    {
                                        ChargeTransDeviceRecord sctdr = ctr.StartChargeTransDeviceRecord;
                                        ctr.StartChargeTransDeviceRecord = null;
                                        ChargeTransDeviceRecord ectdr = ctr.EndChargeTransDeviceRecord;
                                        ctr.EndChargeTransDeviceRecord = null;

                                        db.ChargeTransRecord.Add(ctr);
                                        db.SaveChanges();

                                        if (sctdr != null || ectdr != null)
                                        {
                                            if (sctdr != null)
                                            {
                                                sctdr.ChargeTransTimeStamp = ctr.ChargeTransTimeStamp;
                                                sctdr.StationId = ctr.StationId;
                                                sctdr.DeviceTypeId = ctr.DeviceTypeId;
                                                sctdr.SlotId = ctr.SlotId;

                                                db.ChargeTransDeviceRecord.Add(sctdr);
                                                ctr.StartChargeTransDeviceRecord = sctdr;
                                            }

                                            if (ectdr != null)
                                            {
                                                ectdr.ChargeTransTimeStamp = ctr.ChargeTransTimeStamp;
                                                ectdr.StationId = ctr.StationId;
                                                ectdr.DeviceTypeId = ctr.DeviceTypeId;
                                                ectdr.SlotId = ctr.SlotId;

                                                db.ChargeTransDeviceRecord.Add(ectdr);
                                                ctr.EndChargeTransDeviceRecord = ectdr;
                                            }

                                            db.Entry(ctr).State = System.Data.Entity.EntityState.Modified;
                                            db.SaveChanges();
                                        }
                                    }
                                }

                                resultFormat.ActionCode = ResponseActionCode.UploadSuccess;
                                resultFormat.Message = "Success";
                            }
                            break;
                        }
                    case UploadActionCode.AbnormalRecord: // Upload abnormal Record
                        {
                            List<AbnormalRecord> data = null;
                            using (TextReader file = new StringReader(message))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                data = (List<AbnormalRecord>)serializer.Deserialize(file, typeof(List<AbnormalRecord>));
                            }

                            if (data != null)
                            {
                                foreach (AbnormalRecord ar in data)
                                {
                                    ar.StationId = "0000000001";
                                    ar.DeviceTypeId = 1;

                                    var abnormalInfo = (from a in db.AbnormalRecord
                                                        where a.AbnormalTimeStamp.Equals(ar.AbnormalTimeStamp) && a.StationId.Equals(ar.StationId)
                                                           && a.DeviceTypeId.Equals(ar.DeviceTypeId) && a.SlotId.Equals(ar.SlotId)
                                                           select a).SingleOrDefault();
                                    if (abnormalInfo == null)
                                    {
                                        AbnormalDeviceRecord adr = ar.AbnormalDeviceRecord;
                                        ar.AbnormalDeviceRecord = null;

                                        db.AbnormalRecord.Add(ar);
                                        db.SaveChanges();

                                        if (adr != null)
                                        {
                                            adr.AbnormalTimeStamp = ar.AbnormalTimeStamp;
                                            adr.StationId = ar.StationId;
                                            adr.DeviceTypeId = ar.DeviceTypeId;
                                            adr.SlotId = ar.SlotId;

                                            db.AbnormalDeviceRecord.Add(adr);
                                            ar.AbnormalDeviceRecord = adr;
                                            db.Entry(ar).State = System.Data.Entity.EntityState.Modified;
                                            db.SaveChanges();
                                        }
                                    }
                                }

                                resultFormat.ActionCode = ResponseActionCode.UploadSuccess;
                                resultFormat.Message = "Success";
                            }
                            break;
                        }
                }
            }
            catch(Exception ex)
            {
                resultFormat.ActionCode = ResponseActionCode.UploadFail;
                resultFormat.Message = "Error Message";
            }

            return resultFormat;
        }
    }
}
