﻿// Provide api Service
app.service('apiService', function ($http, apiConfigProvider) {
    var self = this;

    self.refreshCaptchaUrl = function () {
        return apiConfigProvider.PATHREFRESHCAPTCHA + '?' + new Date().getTime();
    };

    self.requestFormat = function(rqMethod, rqURL, rqData) {
        this.method = rqMethod;
        this.url = rqURL;
        if (rqData != null) {
            this.data = rqData;
        }
        this.timeout = apiConfigProvider.CONNECTTIMEOUT;
    }

});