﻿angular.module('gteAPP')
  .directive('sidebar', ['$location', function () {
      return {
          templateUrl: 'Page/MainSidebar',
          restrict: 'E',
          replace: true,
          scope: {
          },
          controller: function ($scope, userInfoService) {
              $scope.selectedMenu = 'dashboard';
              $scope.collapseVar = 0;
              $scope.multiCollapseVar = 0;

              restUserStatusInfo();

              function restUserStatusInfo() {
                  $scope.userRoleCode = userInfoService.getUserRoleCode();
              }

              $scope.check = function (x) {

                  if (x == $scope.collapseVar)
                      $scope.collapseVar = 0;
                  else
                      $scope.collapseVar = x;
              };

              $scope.multiCheck = function (y) {

                  if (y == $scope.multiCollapseVar)
                      $scope.multiCollapseVar = 0;
                  else
                      $scope.multiCollapseVar = y;
              };
          }
      };
  }]);