﻿angular.module('gteAPP')
	.directive('dashboardcharger',function () {
	    return {
	        restrict: 'E',
	        replace: true,
	        templateUrl: 'Page/DashboardCharger',
	        controller: function ($scope, $http, $uibModal, apiConfigProvider, userInfoService, apiService) {

	            var currentChargerInfo;
	            $scope.cChargerModeList = [{ "Name": "Transaction Time", "Code": 1 }, { "Name": "Power Consumption", "Code": 2 }, { "Name": "SOC of Last Half Hour", "Code": 3 }];
	            $scope.cTimeFormatList = [{ "Name": "Day", "Code": 1 }, { "Name": "Month", "Code": 2 }, { "Name": "Year", "Code": 3 }];
	            $scope.chargerModeFormat = $scope.cChargerModeList[0];
	            $scope.chargerTimeFormat = $scope.cTimeFormatList[0];
	            $scope.chargerList = [];
	            $scope.displaycomboboxbyMode = true;
	            $scope.chargerbar = {
	                labels: [],
	                datas: [],
                    series: []
	            };

	            refreshChargerInfo();

	            $scope.refreshChargerInfo = refreshChargerInfo; // 取得ChargerList
	            function refreshChargerInfo() {
	                var reqParam = new requestInfoByAccount();
	                var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHAPI + '/AccountApi/GetUserChargeDeviceInfo', reqParam);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            if (data) {
                                
                                $scope.chargerList = data;
                                
                                if ($scope.chargerList && $scope.chargerList.length > 0) {
                                    resetChargerStatisticsInfo($scope.chargerList[0]);
                                }
                            } else {
                                $scope.chargerList = [];
                            }

                            $scope.chargerTableCollection = [].concat($scope.chargerList);
                        })
                        .error(function (data, status, headers, config) {
                            $scope.chargerList = [];
                        });
	            }

	            $scope.refreshChargerSlotInfo = function (chargeInfo) {

	                var modalInstance = $uibModal.open({
	                    templateUrl: 'displaySlotInfo.html',
	                    resolve: {
	                        chargeInfo: function () {
	                            return chargeInfo;
	                        }
	                    },
	                    controller: function ($scope, $modalInstance, chargeInfo) {
	                        $scope.titleName = chargeInfo.Name;
	                        initData();

	                        function initData() {
	                            var reqParam = new requestChargerSlotInfoByAccount(chargeInfo.DeviceId, chargeInfo.DeviceTypeId);
	                            var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHAPI + '/AccountApi/GetUserChargerSlotInfoByCharger', reqParam);

	                            $http(req)
                                    .success(function (data, status, headers, config) {
                                        $scope.chargerSlots = data;
                                    })
                                    .error(function (data, status, headers, config) {
                                        $scope.chargerSlots = null;
                                    });
	                        }

	                        $scope.cancel = function () {
	                            $modalInstance.dismiss('cancel');
	                        };

	                        $scope.slotStatus = function (stationStatus) {
	                            if (stationStatus != null) {
	                                if (stationStatus.TimeStamp) {
	                                    $scope.stationUpdateTime = '(' + js_yyyy_mm_dd_hh_mm_ss(stationStatus.TimeStamp) + ')';
	                                } else {
	                                    $scope.stationUpdateTime = '(None)';
	                                }
	                                if (stationStatus.SlotStatusCode < 0) {
	                                    return "Shut down";
	                                } else {
	                                    return "Normal";
	                                }
	                            }
	                            return "Unknow";
	                        };

	                        $scope.slotSOC = function (batteryRecord) {
	                            if (batteryRecord == null) {
	                                return "-";
	                            }
	                            return batteryRecord.SOC;
	                        };
	                        $scope.slotTemperature = function (batteryRecord) {
	                            if (batteryRecord == null) {
	                                return "-";
	                            }
	                            return batteryRecord.Temperature;
	                        };
	                        $scope.slotCurrent = function (batteryRecord) {
	                            if (batteryRecord == null) {
	                                return "-";
	                            }
	                            return batteryRecord.Current;
	                        };
	                        $scope.slotVoltage = function (batteryRecord) {
	                            if (batteryRecord == null) {
	                                return "-";
	                            }
	                            return batteryRecord.Voltage;
	                        };
	                        $scope.slotVMax = function (batteryRecord) {
	                            if (batteryRecord == null) {
	                                return "-";
	                            }
	                            return batteryRecord.VMax;
	                        };
	                        $scope.slotVMin = function (batteryRecord) {
	                            if (batteryRecord == null) {
	                                return "-";
	                            }
	                            return batteryRecord.VMin;
	                        };

	                        function js_yyyy_mm_dd_hh_mm_ss (dateTime) {
	                            now = new Date(dateTime);
	                            year = "" + now.getFullYear();
	                            month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
	                            day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
	                            hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
	                            minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
	                            second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
	                            return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
	                        }
	                    }
	                });
	            };

	            $scope.resetChargerStatisticsInfo = resetChargerStatisticsInfo;
	            function resetChargerStatisticsInfo(chargerInfo) {
	                currentChargerInfo = chargerInfo;
	                refreshChargerBarInfo();
	            }

	            $scope.refreshChargerBarInfo = refreshChargerBarInfo;
	            function refreshChargerBarInfo() {
	                if ($scope.chargerModeFormat && $scope.chargerTimeFormat && currentChargerInfo) {
	                    $scope.chargerName = currentChargerInfo.Name;

	                    var reqChartInfo = new requestChartInfo($scope.chargerTimeFormat.Code, currentChargerInfo);

	                    $scope.displaycomboboxbyMode = true;
	                    var path = apiConfigProvider.PATHAPI + '/DashboardApi/GetTransactionTimeInfo';
	                    if ($scope.chargerModeFormat.Code === 2) {
	                        path = apiConfigProvider.PATHAPI + '/DashboardApi/GetChargeQuantityInfo';
	                    } else if ($scope.chargerModeFormat.Code === 3) {
	                        path = apiConfigProvider.PATHAPI + '/DashboardApi/GetHalfHourSOCByChargeDevice';
	                        $scope.displaycomboboxbyMode = false;
	                    }
	                    var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, path, reqChartInfo);

	                    $http(req)
                            .success(function (data, status, headers, config) {
                                $scope.chargerbar = {
                                    labels: [],
                                    datas: [],
                                    series: []
                                };
                                if (data && data.length > 0) {

                                    if ($scope.chargerModeFormat.Code !== 3) {
                                        $scope.chargerbar.datas.push(new Array());

                                        data.forEach(function (entry) {
                                            $scope.chargerbar.labels.push(entry.labels);
                                            // Fixed by Neil 2016.06.24  Temp mAh to kw
                                            if ($scope.chargerModeFormat.Code === 2) {
                                                $scope.chargerbar.datas[0].push(Math.ceil(entry.datas * 48 / 1000));
                                            } else {
                                                $scope.chargerbar.datas[0].push(entry.datas);
                                            }
                                        });
                                    } else {

                                        data.forEach(function (entry) {
                                            if (!($scope.chargerbar.labels.indexOf(entry.labels) > -1)) {
                                                $scope.chargerbar.labels.push(entry.labels);
                                            }

                                            var seriesIndex = $scope.chargerbar.series.indexOf(entry.series);
                                            if (!(seriesIndex > -1)) {
                                                $scope.chargerbar.series.push(entry.series);
                                                $scope.chargerbar.datas.push(new Array());
                                                seriesIndex = $scope.chargerbar.series.length - 1;
                                            }

                                            $scope.chargerbar.datas[seriesIndex].push(entry.datas);
                                        });
                                    }

                                } else {
                                    $scope.chargerbar.labels.push('No Data');
                                    $scope.chargerbar.series.push('No Data');
                                    $scope.chargerbar.datas.push(new Array());
                                    $scope.chargerbar.datas[0].push(0);
                                }
                            })
                            .error(function (data, status, headers, config) {
                                $scope.chargerbar = {
                                    labels: [],
                                    datas: [],
                                    series: []
                                };
                                $scope.chargerbar.labels.push('No Data');
                                $scope.chargerbar.series.push('No Data');
                                $scope.chargerbar.datas.push(new Array());
                                $scope.chargerbar.datas[0].push(0);
                            });
	                } else {
	                    $scope.chargerName = null;

	                    $scope.chargerbar = {
	                        labels: [],
	                        datas: [],
	                        series: []
	                    };
	                }
	            };

	            $scope.chargerTypeByCode = function (deviceTypeId) {
	                var deviceType = userInfoService.getObjectByCode(userInfoService.chargerDeviceTypeList(), deviceTypeId);
	                if (deviceType == null) {
	                    return "Unknow";
	                } else {
	                    return deviceType.Name;
	                }
	            };

	            function requestInfoByAccount() {
	                this.Account = userInfoService.getAccount();
	                this.Password = userInfoService.getPassword();
	            }

	            function requestChargerSlotInfoByAccount(deviceId, deviceTypeId) {
	                this.Account = userInfoService.getAccount();
	                this.Password = userInfoService.getPassword();

	                this.DeviceId = deviceId;
	                //this.StationManufactureDate = stationManufactureDate;
	                this.DeviceTypeId = deviceTypeId;
	            }

	            function requestChartInfo(timeformat, chargerInfo) {
	                this.SecurityKey = userInfoService.getSecurityKey();
	                this.TimeFormat = timeformat;

	                this.DeviceId = chargerInfo.DeviceId;
	                //this.StationManufactureDate = stationManufactureDate;
	                this.DeviceTypeId = chargerInfo.DeviceTypeId;
	            }
	        }
	    };
	});