﻿angular.module('gteAPP')
	.directive('administratoruser', function () {
	    return {
	        restrict: 'E',
	        replace: true,
	        templateUrl: 'Page/AdministratorUser',
	        controller: function ($scope, $uibModal, $http, apiConfigProvider, userInfoService, apiService) {

	            var editIndex = -1;
	            $("#userMessageAlert").hide();
	            $scope.setUserRole = setUserRole;
	            $scope.getUserInfoList = getUserInfoList;
	            $scope.editUserInfo = editUserInfo;
	            $scope.editGroupRelationship = editGroupRelationship;
	            $scope.userRoleList = userInfoService.userRoleList();
	            getUserInfoList();

	            function setUserRole(userRoleCode) {
	                return userInfoService.getObjectByCode($scope.userRoleList, userRoleCode).Name;
	            }

	            function getUserInfoList() {
	                var req = new apiService.requestFormat(apiConfigProvider.METHODGET, apiConfigProvider.PATHAPI + '/AdministratorInfo/GetUserInfos', null);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            if (data) {
                                $scope.userInfoList = data;
                            } else {
                                $scope.userInfoList = [];
                            }

                            $scope.userInfoTableCollection = [].concat($scope.userInfoList);
                        })
                        .error(function (data, status, headers, config) {
                            $scope.userInfoList = [];
                            $scope.userInfoTableCollection = [].concat($scope.userInfoList);
                        });
	            }

	            function setUserInfo(userInfoModel, mode) {

	                userInfoModel.UserRoleCode = userInfoModel.UserRole.Code;

	                var apiString = apiConfigProvider.PATHAPI + '/AdministratorInfo/CreateUserInfo';
	                if (mode == 0) {
	                    apiString = apiConfigProvider.PATHAPI + '/AdministratorInfo/EditUserInfo';
	                }
	                var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiString, userInfoModel);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            if (data > 0) {
                                if (editIndex === -1) {
                                    $scope.userInfoTableCollection.push(userInfoModel); //加到清單內
                                } else {
                                    $scope.userInfoTableCollection[editIndex] = userInfoModel; //更新清單內容
                                }
                                showAlert(true, 'Update Success');
                            } else {
                                showAlert(false, 'Update Fail');
                            }
                        })
                        .error(function (data, status, headers, config) {
                            //錯誤訊息
                            showAlert(false, 'Connect Fail');
                        });
	            }

	            function setUserGroupRelationship(reqParam) {
	                var apiString = apiConfigProvider.PATHAPI + '/AdministratorInfo/SetUserGroupRelationship';

	                var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiString, reqParam);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            //更新成功訊息
                            if (data == 1) {
                                showAlert(true, 'Update Success');
                            } else {
                                showAlert(false, 'Update Fail');
                            }

                        })
                        .error(function (data, status, headers, config) {
                            //錯誤訊息
                            showAlert(false, 'Connect Fail');
                        });
	            }

	            function showAlert(responseStatus, responseMessage) {
	                $scope.responseStatus = responseStatus;
	                $scope.responseMessage = responseMessage;
	                $("#userMessageAlert").alert();
	                $("#userMessageAlert").fadeTo(2000, 500).slideUp(500, function () {
	                    $("#userMessageAlert").hide();
	                });
	            }

	            function editUserInfo(data, title) {
	                if (data) {
	                    editIndex = $scope.userInfoTableCollection.indexOf(data);
	                   
	                } else {
	                    editIndex = -1;
	                }

	                var modalInstance = $uibModal.open({
	                    templateUrl: 'editUserModalContent.html',
	                    resolve: {
	                        userInfoData: function () {
	                            return data;
	                        },
	                        title: function () {
	                            return title;
	                        }
	                    },
	                    controller: function ($scope, $modalInstance, userInfoData, title, userInfoService) {

	                        $scope.editTitle = 'Add';
	                        if (title == 0) {
	                            $scope.editTitle = 'Edit';
	                        }

	                        $scope.userRoleCode = userInfoService.getUserRoleCode();
	                        $scope.userRoleList = userInfoService.userRoleList();
	                        $scope.userInfoModel = new editUserInfoModel(userInfoData);


	                        $scope.save = function () {
	                            $modalInstance.close($scope.userInfoModel);
	                        };

	                        $scope.cancel = function () {
	                            $modalInstance.dismiss('cancel');
	                        };

	                        function editUserInfoModel(data) {
	                            this.SecurityKey = userInfoService.getSecurityKey();
	                            if (data) {
	                                this.UserId = data.UserId;
	                                this.Account = data.Account;
	                                this.Password = data.Password;
	                                this.UserName = data.UserName;
	                                this.Email = data.Email;
	                                this.Phone = data.Phone;
	                                this.Address = data.Address;
	                                this.UserRoleCode = data.UserRoleCode;
	                                this.UserRole = userInfoService.getObjectByCode($scope.userRoleList, data.UserRoleCode);
	                            } else {
	                                this.UserId = null;
	                                this.Account = null;
	                                this.Password = null;
	                                this.UserName = null;
	                                this.Email = null;
	                                this.Phone = null;
	                                this.Address = null;
	                                this.UserRoleCode = 0;
	                                this.UserRole = $scope.userRoleList[0];
	                            }
	                        }
	                    }
	                });

	                modalInstance.result.then(function (data) {
	                    setUserInfo(data, title);
	                });
	            }

	            function editGroupRelationship(data) {

	                var modalInstance = $uibModal.open({
	                    templateUrl: 'userGroupRelationshipModalContent.html',
	                    resolve: {
	                        userInfoData: function () {
	                            return data;
	                        }
	                    },
	                    controller: function ($scope, $modalInstance, userInfoData, userInfoService) {

	                        $scope.save = function () {
	                            var groupRelationship = [];
	                            $scope.groupRelationshipTableCollection.forEach(function (entry) {
	                                if (entry.DataChange) {
	                                    entry.RelationRoleCode = entry.RelationRole.Code;
	                                    groupRelationship.push(entry);
	                                }
	                            });
	                            var reqParam = new reqsGroupRelationship(userInfoData.UserId, groupRelationship);
	                            $modalInstance.close(reqParam);
	                        };

	                        $scope.cancel = function () {
	                            $modalInstance.dismiss('cancel');
	                        };

	                        $scope.UserName = userInfoData.UserName;
	                        $scope.setRelationRole = setRelationRole;
	                        $scope.isChangeGroupRelation = isChangeGroupRelation;
	                        $scope.relationRoleList = userInfoService.relationRoleList();
	                        $scope.groupRelationship = [];
	                        getGroupRelationship();

	                        function getGroupRelationship() {
	                            var reqParam = new reqsUserId(userInfoData.UserId);
	                            var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHAPI + '/AdministratorInfo/GetUserGroupRelationship', reqParam);

	                            $http(req)
                                    .success(function (data, status, headers, config) {
                                        if (data) {
                                            $scope.groupRelationship = data;
                                        } else {
                                            $scope.groupRelationship = [];
                                        }

                                        $scope.groupRelationshipTableCollection = [].concat($scope.groupRelationship);
                                    })
                                    .error(function (data, status, headers, config) {
                                        $scope.groupRelationship = [];
                                        $scope.groupRelationshipTableCollection = [].concat($scope.groupRelationship);
                                    });
	                        }

	                        function setRelationRole(code) {
	                            return userInfoService.getObjectByCode(userInfoService.relationRoleList(), code);
	                        }

	                        function isChangeGroupRelation(data) {
	                            if (data.RelationRole.Code == data.RelationRoleCode && data.Enable == data.EnableInit) {
	                                data.DataChange = false;
	                            }
	                            else {
	                                data.DataChange = true;
	                            }
	                        }

	                        function reqsUserId(UserId) {
	                            this.SecurityKey = userInfoService.getSecurityKey();
	                            this.UserId = UserId;
	                        }

	                        function reqsGroupRelationship(UserId, groupRelationship) {
	                            this.SecurityKey = userInfoService.getSecurityKey();
	                            this.UserId = UserId;
	                            this.GroupRelationInfos = groupRelationship;
	                        }

	                    }
	                });

	                modalInstance.result.then(function (data) {
	                    setUserGroupRelationship(data);
	                });
	            }
	        }
	    };
	});