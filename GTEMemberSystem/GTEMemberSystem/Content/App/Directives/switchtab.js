﻿angular.module('gteAPP')
	.directive('switchtab', function () {
	    return {
	        link: function (scope, element, attrs) {
	            element.click(function (e) {
	                e.preventDefault();
	                $(element).tab('show');
	            });
	        }
	    };
	});