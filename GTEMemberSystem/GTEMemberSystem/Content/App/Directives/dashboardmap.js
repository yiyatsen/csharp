﻿angular.module('gteAPP')
	.directive('dashboardmap', function () {
	    return {
	        restrict: 'E',
	        replace: true,
	        templateUrl: 'Page/DashboardMap',
	        controller: function ($scope, $http, NgMap, apiConfigProvider, userInfoService, apiService) {

	            var param = {
	                DEFAULTLOCATION: [25.062562, 121.64665],
	                USERLOCATION: 'UserLocation',
	                LATITUDE: 'Latitude',
	                LONGITUDE: 'Longitude',
	                CLUSTERMARKER: 'ClusterMarker',
	                MARKERS: 'Markers',
	                ICON: 'Icon',
	                ICONPATH: 'IconPath',
	                ICONWIDTH: 'IconWidth',
	                ICONHEIGHT: 'IconHeight',
	                CHARGER: "Charger"
	            };

	            var googleMap;
	            var mapInfowindow;
	            var markerList;
	            var watchUserLocationFlagId;


	            NgMap.getMap().then(function (map) {
	                // Set Map
	                googleMap = map;
	                // Set Infowindow
	                mapInfowindow = new google.maps.InfoWindow();
	                // Hide infowindow when click map
	                google.maps.event.addListener(googleMap, 'click', function () {
	                    if (mapInfowindow) {
	                        mapInfowindow.close();
	                    }
	                });


	                markerList = userInfoService.getIconInfo();
	                if (markerList) {
	                    for (item in markerList) {
	                        markerList[item][param.ICON] =
                                new google.maps.MarkerImage(
                                markerList[item][param.ICONPATH],
                                null,
                                null,
                                null,
                                new google.maps.Size(markerList[item][param.ICONWIDTH], markerList[item][param.ICONHEIGHT]));

	                        if (item === param.USERLOCATION) {
	                            // User Location Markers
	                            markerList[item][param.MARKERS] = new google.maps.Marker({
	                                map: googleMap
	                            });
	                            markerList[item][param.MARKERS].setIcon(markerList[item][param.ICON]);
	                        }
	                        else {
	                            markerList[item][param.CLUSTERMARKER]
                                    = new MarkerClusterer(googleMap, []);
	                        }
	                    }
	                }


	                // init UserLocation
	                $scope.goUserLocation();
	                // init Marker
	                $scope.refreshChargerMarker();

	                //console.log(map.getCenter());
	                //console.log('markers', map.markers);
	                //console.log('shapes', map.shapes);
	            });

	            // Refresh user location
	            $scope.goUserLocation = function (isAutoRun) {

	                if (watchUserLocationFlagId) {
	                    navigator.geolocation.clearWatch(watchUserLocationFlagId);
	                    watchUserLocationFlagId = null;
	                }

	                var options = { enableHighAccuracy: true, timeout: 5000, maximumAge: 0, desiredAccuracy: 0, frequency: 1000 };

	                watchUserLocationFlagId = navigator.geolocation.watchPosition(
                        function success(position) {
                            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                            markerList[param.USERLOCATION][param.MARKERS].setPosition(pos);

                            if (!isAutoRun) {
                                isAutoRun = 'trigger';
                                // Move to position
                                googleMap.setCenter(pos);
                            }
                        },
                        function error(err) {
                            if (!isAutoRun) {
                                var pos = new google.maps.LatLng(param.DEFAULTLOCATION[0], param.DEFAULTLOCATION[1]);
                                googleMap.setCenter(pos);
                            }
                        },
                        options);
	            };

	            // Refresh Charger Marker
	            $scope.refreshChargerMarker = function () {

	                var reqCurrentStationInfo = new requestCurrentStationInfo();
	                var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHAPI + '/AccountApi/GetCurrentChargerInfo', reqCurrentStationInfo);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            if (data) {
                                clearClusterMarkers(markerList[param.CHARGER]);

                                var markers = [];
                                for (item in data) {
                                    var pos = new google.maps.LatLng(data[item][param.LATITUDE], data[item][param.LONGITUDE]);

                                    var marker = new google.maps.Marker({
                                        map: googleMap,
                                        position: pos,
                                        icon: markerList[param.CHARGER][param.ICON]
                                    });

                                    google.maps.event.addListener(
                                      marker,
                                      'click',
                                      (function (marker, scope) {
                                          return function () {
                                              googleMap.panTo(marker.getPosition());

                                              // content of infowindow must be a file
                                              mapInfowindow.setContent('<h4>' + marker.getPosition().A + ',' + marker.getPosition().F + '</h4>');
                                              mapInfowindow.open(googleMap, marker);
                                          };//return fn()
                                      })(marker, $scope)
                                    );//addListener

                                    markers.push(marker);
                                }

                                addClusterMarkers(markerList[param.CHARGER], markers);
                            }
                        })
                        .error(function (data, status, headers, config) {

                        });

	            };

	            function requestCurrentStationInfo() {
	                this.Account = userInfoService.getAccount();
	                this.Password = userInfoService.getPassword();
	            }

	            function clearClusterMarkers(item) {
	                item[param.CLUSTERMARKER].clearMarkers();
	            }

	            function addClusterMarkers(item, data) {
	                item[param.CLUSTERMARKER].addMarkers(data, false);
	            }
	        }
	    };
	});