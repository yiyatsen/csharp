﻿angular.module('gteAPP')
	.directive('administratorbattery', function () {
	    return {
	        restrict: 'E',
	        replace: true,
	        templateUrl: 'Page/AdministratorBattery',
	        controller: function ($scope, $uibModal, $http, apiConfigProvider, userInfoService, apiService) {

	            var editIndex = -1;
	            $("#batteryMessageAlert").hide();
	            $scope.setBatteryDeviceType = setBatteryDeviceType;
	            $scope.setDeviceStatus = setDeviceStatus;
	            $scope.getBatteryInfoList = getBatteryInfoList;
	            $scope.editBatteryInfo = editBatteryInfo;
	            $scope.batteryDeviceTypeList = userInfoService.batteryDeviceTypeList();
	            $scope.deviceStatusList = userInfoService.statusList();
	            $scope.editBatteryGroupRelationship = editBatteryGroupRelationship;
	            $scope.getBatteryRecord = getBatteryRecord;
	            getBatteryInfoList();

	            function setBatteryDeviceType(batteryDeviceTypeCode) {
	                return userInfoService.getObjectByCode($scope.batteryDeviceTypeList, batteryDeviceTypeCode).Name;
	            }

	            function setDeviceStatus(chargerStatusCode) {
	                return userInfoService.getObjectByCode($scope.deviceStatusList, chargerStatusCode).Name;
	            }

	            function getBatteryInfoList() {
	                var req = new apiService.requestFormat(apiConfigProvider.METHODGET, apiConfigProvider.PATHAPI + '/AdministratorInfo/GetBatteryInfos', null);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            if (data) {
                                $scope.batteryInfoList = data;                            
                            } else {
                                $scope.batteryInfoList = [];
                            }
                            $scope.batteryInfoTableCollection = [].concat($scope.batteryInfoList);
                        })
                        .error(function (data, status, headers, config) {
                            $scope.batteryInfoList = [];
                            $scope.batteryInfoTableCollection = [].concat($scope.batteryInfoList);
                        });
	            }

	            function setBatteryInfo(batteryInfoModel, mode) {

	                batteryInfoModel.DeviceTypeId = batteryInfoModel.DeviceType.Code;
	                batteryInfoModel.StatusCode = batteryInfoModel.Status.Code;
	                //batteryInfoModel.DeviceOwnerTypeCode = batteryInfoModel.DeviceOwnerType.Code;

	                var apiString = apiConfigProvider.PATHAPI + '/AdministratorInfo/CreateBatteryInfo';
	                if (mode == 0) {
	                    apiString = apiConfigProvider.PATHAPI + '/AdministratorInfo/EditBatteryInfo';
	                }

	                var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiString, batteryInfoModel);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            if (data > 0) {
                                if (editIndex === -1) {
                                    $scope.batteryInfoTableCollection.push(batteryInfoModel); //加到清單內
                                } else {
                                    $scope.batteryInfoTableCollection[editIndex] = batteryInfoModel; //更新清單內容
                                }
                                showAlert(true, 'Update Success');
                            } else {
                                showAlert(false, 'Update Fail');
                            }
                        })
                        .error(function (data, status, headers, config) {
                            showAlert(false, 'Connect Fail');
                        });
	            }

	            function setBatteryGroupRelationship(reqParam) {
	                var apiString = apiConfigProvider.PATHAPI + '/AdministratorInfo/SetBatteryGroupRelationship';

	                var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiString, reqParam);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            //更新成功訊息
                            if (data == 1) {
                                showAlert(true, 'Update Success');
                            } else {
                                showAlert(false, 'Update Fail');
                            }

                        })
                        .error(function (data, status, headers, config) {
                            //錯誤訊息
                            showAlert(false, 'Connect Fail');
                        });
	            }

	            function showAlert(responseStatus, responseMessage) {
	                $scope.responseStatus = responseStatus;
	                $scope.responseMessage = responseMessage;
	                $("#batteryMessageAlert").alert();
	                $("#batteryMessageAlert").fadeTo(2000, 500).slideUp(500, function () {
	                    $("#batteryMessageAlert").hide();
	                });
	            }

	            function editBatteryInfo(data, title) {
	                if (data) {
	                    editIndex = $scope.batteryInfoTableCollection.indexOf(data);
	                } else {
	                    editIndex = -1;
	                }

	                var modalInstance = $uibModal.open({
	                    templateUrl: 'editBatteryModalContent.html',
	                    resolve: {
	                        batteryInfoData: function () {
	                            return data;
	                        },
	                        title: function () {
	                            return title;
	                        }
	                    },
	                    controller: function ($scope, $modalInstance, batteryInfoData, title, userInfoService) {

	                        $scope.editTitle = 'Add';
	                        if (title == 0) {
	                            $scope.editTitle = 'Edit';
	                        }

	                        $scope.userRoleCode = userInfoService.getUserRoleCode();
	                        $scope.batteryDeviceTypeList = userInfoService.batteryDeviceTypeList();
	                        $scope.ownerTypeList = userInfoService.ownerTypeList();
	                        $scope.deviceStatusList = userInfoService.statusList();
	                        $scope.batteryInfoModel = new editBatteryInfoModel(batteryInfoData);


	                        $scope.save = function () {
	                            $modalInstance.close($scope.batteryInfoModel);
	                        };

	                        $scope.cancel = function () {
	                            $modalInstance.dismiss('cancel');
	                        };

	                        function editBatteryInfoModel(data) {
	                            this.SecurityKey = userInfoService.getSecurityKey();
	                            if (data) {
	                                this.BatteryId = data.BatteryId;
	                                this.ManufactureDate = data.ManufactureDate;
	                                this.DeviceTypeId = data.DeviceTypeId;
	                                this.RegisterDateTime = data.RegisterDateTime;
	                                this.LastUpdateTime = data.LastUpdateTime;
	                                this.APVersion = data.APVersion;
	                                this.Description = data.Description;
	                                this.OwnerId = data.OwnerId;
	                                this.StatusCode = data.StatusCode;

	                                this.DeviceType = userInfoService.getObjectByCode($scope.batteryDeviceTypeList, data.DeviceTypeId);
	                                this.Status = userInfoService.getObjectByCode($scope.deviceStatusList, data.StatusCode);

	                            } else {
	                                this.BatteryId = null;
	                                this.ManufactureDate = null;
	                                this.DeviceTypeId = 1;
	                                this.OwnerId = null;
	                                this.DeviceStatusCode = 0;
	                                this.DeviceType = null;
	                                this.DeviceOwnerType = null;
	                                this.DeviceStatus = null;

	                                this.BatteryId = null;
	                                this.ManufactureDate = null;
	                                this.DeviceTypeId = 0;
	                                this.RegisterDateTime = null;
	                                this.LastUpdateTime = null;
	                                this.APVersion = null;
	                                this.Description = null;
	                                this.OwnerId = null;
	                                this.DeviceOwnerTypeCode = 0;
	                                this.StatusCode = 0;

	                                this.DeviceType = userInfoService.getObjectByCode($scope.batteryDeviceTypeList, 0);
	                                this.Status = userInfoService.getObjectByCode($scope.deviceStatusList, 0);
	                            }

	                        }
	                    }
	                });

	                modalInstance.result.then(function (data) {
	                    setBatteryInfo(data, title);
	                });
	            }

	            function editBatteryGroupRelationship(data) {

	                var modalInstance = $uibModal.open({
	                    templateUrl: 'batteryGroupRelationshipModalContent.html',
	                    resolve: {
	                        batteryInfoData: function () {
	                            return data;
	                        }
	                    },
	                    controller: function ($scope, $modalInstance, batteryInfoData, userInfoService) {

	                        $scope.save = function () {
	                            var groupRelationship = [];
	                            $scope.groupRelationshipTableCollection.forEach(function (entry) {
	                                if (entry.DataChange) {
	                                    groupRelationship.push(entry);
	                                }
	                            });
	                            var reqParam = new reqsGroupRelationship(groupRelationship);
	                            $modalInstance.close(reqParam);
	                        };

	                        $scope.cancel = function () {
	                            $modalInstance.dismiss('cancel');
	                        };
	                        $scope.batteryName = batteryInfoData.Name;
	                        $scope.isChangeGroupRelation = isChangeGroupRelation;
	                        $scope.groupRelationship = [];
	                        getGroupRelationship();

	                        function getGroupRelationship() {
	                            var reqParam = new reqsData();
	                            var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHAPI + '/AdministratorInfo/GetBatteryGroupRelationship', reqParam);

	                            $http(req)
                                    .success(function (data, status, headers, config) {
                                        if (data) {
                                            $scope.groupRelationship = data;
                                        } else {
                                            $scope.groupRelationship = [];
                                        }

                                        $scope.groupRelationshipTableCollection = [].concat($scope.groupRelationship);
                                    })
                                    .error(function (data, status, headers, config) {
                                        $scope.groupRelationship = [];
                                        $scope.groupRelationshipTableCollection = [].concat($scope.groupRelationship);
                                    });
	                        }

	                        function isChangeGroupRelation(data) {
	                            if (data.Enable == data.EnableInit) {
	                                data.DataChange = false;
	                            }
	                            else {
	                                data.DataChange = true;
	                            }
	                        }

	                        function reqsData() {
	                            this.SecurityKey = userInfoService.getSecurityKey();
	                            this.OwnerId = batteryInfoData.OwnerId;
	                            this.BatteryId = batteryInfoData.BatteryId;
	                            this.ManufactureDate = batteryInfoData.ManufactureDate;
	                            this.DeviceTypeId = batteryInfoData.DeviceTypeId;
	                        }

	                        function reqsGroupRelationship(groupRelationship) {
	                            this.SecurityKey = userInfoService.getSecurityKey();
	                            this.OwnerId = batteryInfoData.OwnerId;
	                            this.BatteryId = batteryInfoData.BatteryId;
	                            this.ManufactureDate = batteryInfoData.ManufactureDate;
	                            this.DeviceTypeId = batteryInfoData.DeviceTypeId;
	                            this.GroupRelationInfos = groupRelationship;
	                        }

	                    }
	                });

	                modalInstance.result.then(function (data) {
	                    setBatteryGroupRelationship(data);
	                });
	            }

	            function getBatteryRecord(data) {

	                var modalInstance = $uibModal.open({
	                    templateUrl: 'batteryRecordModalContent.html',
	                    resolve: {
	                        batteryInfoData: function () {
	                            return data;
	                        }
	                    },
	                    controller: function ($scope, $modalInstance, batteryInfoData, userInfoService) {


	                        $scope.batteryId = batteryInfoData.BatteryId;
	                        $scope.setChargerDeviceType = setChargerDeviceType;
	                        $scope.getBatteryDetailRecord = getBatteryDetailRecord;
	                        $scope.batteryRecordList = [];
	                        getBatteryRecord();

	                        $scope.confirm = function () {
	                            $modalInstance.dismiss('cancel');
	                        };

	                        function setChargerDeviceType(chargerDeviceTypeCode) {
	                            return userInfoService.getObjectByCode(userInfoService.chargerDeviceTypeList(), chargerDeviceTypeCode).Name;
	                        }

	                        function getBatteryRecord() {
	                            var reqParam = new reqsBattery();
	                            var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHAPI + '/AdministratorInfo/GetBatteryChargedRecord', reqParam);

	                            $http(req)
                                    .success(function (data, status, headers, config) {
                                        if (data) {
                                            $scope.batteryRecordList = data;
                                            if ($scope.batteryRecordList.length > 0) {
                                                getBatteryDetailRecord($scope.batteryRecordList[0]);
                                            }
                                        } else {
                                            $scope.batteryRecordList = [];
                                        }

                                        $scope.batteryRecordTableCollection = [].concat($scope.batteryRecordList);
                                    })
                                    .error(function (data, status, headers, config) {
                                        $scope.batteryRecordList = [];
                                        $scope.batteryRecordTableCollection = [].concat($scope.batteryRecordList);
                                    });
	                        }

	                        function getBatteryDetailRecord(data) {

	                            $scope.selectedRecordTime = data.ChargeTransTimeStamp;
	                            $scope.batteryDetailList = [];
	                            if (data.StartChargeTransDeviceRecord) {
	                                data.StartChargeTransDeviceRecord.Status = 'Start';

	                                if (data.StartChargeTransDeviceRecord.CellsVoltage && (data.StartChargeTransDeviceRecord.CellsVoltage.length % 4 == 0)) {
	                                    var max = data.StartChargeTransDeviceRecord.CellsVoltage.length / 4;
	                                    for (var i = 0; i < max; i++) {
	                                        var j = i * 4;
	                                        var test = "0x" + data.StartChargeTransDeviceRecord.CellsVoltage.substring(j, j+4);
	                                        var temp = parseInt("0x" + data.StartChargeTransDeviceRecord.CellsVoltage.substring(j, j+4));

	                                        if (i === 0) {
	                                            data.StartChargeTransDeviceRecord.VMax = temp;
	                                            data.StartChargeTransDeviceRecord.VMin = temp;
	                                        }

	                                        if (data.StartChargeTransDeviceRecord.VMax && data.StartChargeTransDeviceRecord.VMax < temp) {
	                                            data.StartChargeTransDeviceRecord.VMax = temp;
	                                        }
	                                        if (data.StartChargeTransDeviceRecord.VMin && data.StartChargeTransDeviceRecord.VMin > temp) {
	                                            data.StartChargeTransDeviceRecord.VMin = temp;
	                                        }
	                                    }
	                                }

	                                $scope.batteryDetailList.push(data.StartChargeTransDeviceRecord);
	                            }
	                            if (data.EndChargeTransDeviceRecord) {
	                                data.EndChargeTransDeviceRecord.Status = 'End';

	                                if (data.EndChargeTransDeviceRecord.CellsVoltage && (data.EndChargeTransDeviceRecord.CellsVoltage.length % 4 == 0)) {
	                                    var max = data.EndChargeTransDeviceRecord.CellsVoltage.length / 4;
	                                    for (var i = 0; i < max; i++) {
	                                        var j = i * 4;
	                                        var temp = parseInt("0x" + data.EndChargeTransDeviceRecord.CellsVoltage.substring(j, j+4));

	                                        if (i === 0) {
	                                            data.EndChargeTransDeviceRecord.VMax = temp;
	                                            data.EndChargeTransDeviceRecord.VMin = temp;
	                                        }

	                                        if (data.EndChargeTransDeviceRecord.VMax && data.EndChargeTransDeviceRecord.VMax < temp) {
	                                            data.EndChargeTransDeviceRecord.VMax = temp;
	                                        }

	                                        if (data.EndChargeTransDeviceRecord.VMin && data.EndChargeTransDeviceRecord.VMin > temp) {
	                                            data.EndChargeTransDeviceRecord.VMin = temp;
	                                        }
	                                    }
	                                }

	                                $scope.batteryDetailList.push(data.EndChargeTransDeviceRecord);
	                            }
	                        }

	                        function reqsBattery() {
	                            this.SecurityKey = userInfoService.getSecurityKey();
	                            this.BatteryId = batteryInfoData.BatteryId;
	                            this.ManufactureDate = batteryInfoData.ManufactureDate;
	                        }
	                    }
	                });

	                //modalInstance.result.then(function (data) {
	                //    setBatteryGroupRelationship(data);
	                //});
	            }
	        }
	    };
	});