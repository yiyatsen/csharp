﻿angular.module('gteAPP').controller('UserProfileCtrl', function ($scope, $http, userInfoService, apiService, apiConfigProvider) {

    $("#userProfileAlert").hide();
    $scope.userProfileResponseMessage = '';
    $scope.setUserProfile = setUserProfile;
    getUserProfile();

    function getUserProfile() {
        var req = new apiService.requestFormat(apiConfigProvider.METHODGET, apiConfigProvider.PATHAPI + '/AdministratorInfo/GetUserProfile', null);

        $http(req)
            .success(function (data, status, headers, config) {
                if (data) {
                    $scope.userProfileModel = data;
                } else {
                    $scope.userProfileModel = null;
                }
            })
            .error(function (data, status, headers, config) {
                $scope.userProfileModel = null;
            });
    }

    function setUserProfile() {
        $scope.userProfileModel.SecurityKey = userInfoService.getSecurityKey();

        var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHAPI + '/AdministratorInfo/SetUserProfile', $scope.userProfileModel);

        $http(req)
            .success(function (data, status, headers, config) {
                if (data != null) {

                    $scope.userProfileResponseMessage = 'Fail';
                    if (data == 1) {
                        $scope.userProfileResponseMessage = 'Success';
                    }

                    $("#userProfileAlert").alert();
                    $("#userProfileAlert").fadeTo(2000, 500).slideUp(500, function () {
                        $("#userProfileAlert").hide();
                    });
                }
            })
            .error(function (data, status, headers, config) {
                $scope.userProfileResponseMessage = 'Fail';

                $("#userProfileAlert").alert();
                $("#userProfileAlert").fadeTo(2000, 500).slideUp(500, function () {
                    $("#userProfileAlert").hide();
                });
            });
    }
});