'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('gteAPP')
	.directive('timeline',function() {
    return {
        templateUrl: 'Content/dist/scripts/directives/timeline/timeline.html',
        restrict: 'E',
        replace: true,
    }
  });
