'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('gteAPP')
	.directive('notifications',function(){
		return {
		    templateUrl: 'Content/dist/scripts/directives/notifications/notifications.html',
        restrict: 'E',
        replace: true,
    	}
	});


