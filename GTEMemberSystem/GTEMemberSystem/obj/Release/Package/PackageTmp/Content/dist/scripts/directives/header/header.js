'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('gteAPP')
	.directive('header',function(){
		return {
		    templateUrl: 'Content/dist/scripts/directives/header/header.html',
        restrict: 'E',
        replace: true,
    	}
	});


