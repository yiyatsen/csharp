﻿angular.module('gteAPP').controller('MainCtrl', function ($scope, $state, userInfoService, apiService) {

    $scope.logout = function () {
        userInfoService.logout(callbackForLogout);
    };

    function callbackForLogout(loginStatusCode) {
        if (loginStatusCode !== null) {
            switch (loginStatusCode) {
                case 0:
                    userInfoService.resetUserInfo();
                    userInfoService.initConnect();

                    break;
                case 1:
                    $state.go('Login');
                    break;
                default:
                    $state.go('Login');
                    break;
            }
        }
    }
});
