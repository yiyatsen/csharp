﻿angular.module('gteAPP')
	.directive('administratorcharger', function () {
	    return {
	        restrict: 'E',
	        replace: true,
	        templateUrl: 'Page/AdministratorCharger',
	        controller: function ($scope, $uibModal, $http, apiConfigProvider, userInfoService, apiService) {

	            var editIndex = -1;
	            $("#chargerMessageAlert").hide();
	            $scope.setChargerDeviceType = setChargerDeviceType;
	            $scope.setBatteryDeviceType = setBatteryDeviceType;
	            $scope.setDeviceStatus = setDeviceStatus;
	            $scope.getChargerInfoList = getChargerInfoList;
	            $scope.editChargerInfo = editChargerInfo;
	            $scope.chargerDeviceTypeList = userInfoService.chargerDeviceTypeList();
	            $scope.batteryDeviceTypeList = userInfoService.batteryDeviceTypeList();
	            $scope.deviceStatusList = userInfoService.statusList();
	            $scope.editChargerGroupRelationship = editChargerGroupRelationship;
	            getChargerInfoList();

	            function setChargerDeviceType(chargerDeviceTypeCode) {
	                return userInfoService.getObjectByCode($scope.chargerDeviceTypeList, chargerDeviceTypeCode).Name;
	            }

	            function setBatteryDeviceType(batteryDeviceTypeCode) {
	                return userInfoService.getObjectByCode($scope.batteryDeviceTypeList, batteryDeviceTypeCode).Name;
	            }

	            function setDeviceStatus(chargerStatusCode) {
	                return userInfoService.getObjectByCode($scope.deviceStatusList, chargerStatusCode).Name;
	            }

	            function getChargerInfoList() {
	                var req = new apiService.requestFormat(apiConfigProvider.METHODGET, apiConfigProvider.PATHAPI + '/AdministratorInfo/GetChargerInfos', null);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            if (data) {
                                $scope.chargerInfoList = data;
                                
                            } else {
                                $scope.chargerInfoList = [];
                            }
                            $scope.chargerInfoTableCollection = [].concat($scope.chargerInfoList);
                        })
                        .error(function (data, status, headers, config) {
                            $scope.chargerInfoList = [];
                            $scope.chargerInfoTableCollection = [].concat($scope.chargerInfoList);
                        });
	            }

	            function setChargerInfo(chargerInfoModel, mode) {

	                chargerInfoModel.DeviceTypeId = chargerInfoModel.DeviceType.Code;
	                chargerInfoModel.BatteryTypeId = chargerInfoModel.BatteryType.Code;
	                chargerInfoModel.StatusCode = chargerInfoModel.Status.Code;

	                var apiString = apiConfigProvider.PATHAPI + '/AdministratorInfo/CreateChargerInfo';
	                if (mode == 0) {
	                    apiString = apiConfigProvider.PATHAPI + '/AdministratorInfo/EditChargerInfo';
	                }

	                var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiString, chargerInfoModel);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            if (data > 0) {
                                if (editIndex === -1) {
                                    $scope.chargerInfoTableCollection.push(chargerInfoModel); //加到清單內
                                } else {
                                    $scope.chargerInfoTableCollection[editIndex] = chargerInfoModel; //更新清單內容
                                }
                                showAlert(true, 'Update Success');
                            } else {
                                showAlert(false, 'Update Fail');
                            }
                        })
                        .error(function (data, status, headers, config) {
                            showAlert(false, 'Connect Fail');
                        });
	            }
	            function setChargerGroupRelationship(reqParam) {
	                var apiString = apiConfigProvider.PATHAPI + '/AdministratorInfo/SetChargerGroupRelationship';

	                var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiString, reqParam);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            //更新成功訊息
                            if (data == 1) {
                                showAlert(true, 'Update Success');
                            } else {
                                showAlert(false, 'Update Fail');
                            }

                        })
                        .error(function (data, status, headers, config) {
                            //錯誤訊息
                            showAlert(false, 'Connect Fail');
                        });
	            }

	            function showAlert(responseStatus, responseMessage) {
	                $scope.responseStatus = responseStatus;
	                $scope.responseMessage = responseMessage;
	                $("#chargerMessageAlert").alert();
	                $("#chargerMessageAlert").fadeTo(2000, 500).slideUp(500, function () {
	                    $("#chargerMessageAlert").hide();
	                });
	            }

	            function editChargerInfo(data, title) {
	                if (data) {
	                    editIndex = $scope.chargerInfoTableCollection.indexOf(data);
	                } else {
	                    editIndex = -1;
	                }

	                var modalInstance = $uibModal.open({
	                    templateUrl: 'editChargerModalContent.html',
	                    resolve: {
	                        chargerInfoData: function () {
	                            return data;
	                        },
	                        title: function () {
	                            return title;
	                        }
	                    },
	                    controller: function ($scope, $modalInstance, chargerInfoData, title, userInfoService) {

	                        $scope.editTitle = 'Add';
	                        if (title == 0) {
	                            $scope.editTitle = 'Edit';
	                        }

	                        $scope.userRoleCode = userInfoService.getUserRoleCode();
	                        $scope.chargerDeviceTypeList = userInfoService.chargerDeviceTypeList();
	                        $scope.batteryDeviceTypeList = userInfoService.batteryDeviceTypeList();
	                        $scope.deviceStatusList = userInfoService.statusList();
	                        $scope.chargerInfoModel = new editChargerInfoModel(chargerInfoData);

	                        $scope.save = function () {
	                            $modalInstance.close($scope.chargerInfoModel);
	                        };

	                        $scope.cancel = function () {
	                            $modalInstance.dismiss('cancel');
	                        };

	                        function editChargerInfoModel(data) {
	                            this.SecurityKey = userInfoService.getSecurityKey();
	                            if (data) {
	                                this.DeviceId = data.DeviceId;
	                                //this.StationManufactureDate = data.StationManufactureDate;
	                                this.DeviceTypeId = data.DeviceTypeId;
	                                this.Name = data.Name;
	                                this.Password = data.Password;
	                                this.DeviceMac = data.DeviceMac;
	                                this.LastOnlineIP = data.LastOnlineIP;
	                                this.IsOnline = data.IsOnline;
	                                this.Latitude = data.Latitude;
	                                this.Longitude = data.Longitude;
	                                this.Address = data.Address;
	                                this.SlotNumber = data.SlotNumber;
	                                this.LastOnlineTime = data.LastOnlineTime;
	                                this.RegisterDateTime = data.RegisterDateTime;
	                                this.OSVersion = data.OSVersion;
	                                this.APVersion = data.APVersion;
	                                this.Description = data.Description;

	                                this.BatteryTypeId = data.BatteryTypeId;
	                                this.StatusCode = data.StatusCode;
	                                this.OwnerId = data.OwnerId;

	                                this.DeviceType = userInfoService.getObjectByCode($scope.chargerDeviceTypeList, data.DeviceTypeId);
	                                this.BatteryType = userInfoService.getObjectByCode($scope.batteryDeviceTypeList, data.BatteryTypeId);
	                                this.Status = userInfoService.getObjectByCode($scope.deviceStatusList, data.StatusCode);
	                            } else {

	                                this.DeviceId = null;
	                                //this.StationManufactureDate = null;
	                                this.DeviceTypeId = 1;
	                                this.Name = null;
	                                this.Password = null;
	                                this.DeviceMac = null;
	                                this.LastOnlineIP = null;
	                                this.IsOnline = null;
	                                this.Latitude = null;
	                                this.Longitude = null;
	                                this.Longitude = null;
	                                this.Address = null;
	                                this.SlotNumber = null;
	                                this.LastOnlineTime = null;
	                                this.RegisterDateTime = null;
	                                this.OSVersion = null;
	                                this.APVersion = null;
	                                this.Description = null;

	                                this.BatteryTypeId = 0;
	                                this.StatusCode = 0;
	                                this.OwnerId = 0;

	                                this.DeviceType = userInfoService.getObjectByCode($scope.chargerDeviceTypeList, 1);
	                                this.BatteryType = userInfoService.getObjectByCode($scope.batteryDeviceTypeList, 0);
	                                this.Status = userInfoService.getObjectByCode($scope.deviceStatusList, 0);
	                            }
	                        }
	                    }
	                });

	                modalInstance.result.then(function (data) {
	                    setChargerInfo(data, title);
	                });
	            }

	            function editChargerGroupRelationship(data) {

	                var modalInstance = $uibModal.open({
	                    templateUrl: 'chargerGroupRelationshipModalContent.html',
	                    resolve: {
	                        chargerInfoData: function () {
	                            return data;
	                        }
	                    },
	                    controller: function ($scope, $modalInstance, chargerInfoData, userInfoService) {

	                        $scope.save = function () {
	                            var groupRelationship = [];
	                            $scope.groupRelationshipTableCollection.forEach(function (entry) {
	                                if (entry.DataChange) {
	                                    groupRelationship.push(entry);
	                                }
	                            });
	                            var reqParam = new reqsGroupRelationship(groupRelationship);
	                            $modalInstance.close(reqParam);
	                        };

	                        $scope.cancel = function () {
	                            $modalInstance.dismiss('cancel');
	                        };
	                        $scope.chargerName = chargerInfoData.Name;
	                        $scope.isChangeGroupRelation = isChangeGroupRelation;
	                        $scope.groupRelationship = [];
	                        getGroupRelationship();

	                        function getGroupRelationship() {
	                            var reqParam = new reqsData();
	                            var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHAPI + '/AdministratorInfo/GetChargerGroupRelationship', reqParam);

	                            $http(req)
                                    .success(function (data, status, headers, config) {
                                        if (data) {
                                            $scope.groupRelationship = data;
                                        } else {
                                            $scope.groupRelationship = [];
                                        }

                                        $scope.groupRelationshipTableCollection = [].concat($scope.groupRelationship);
                                    })
                                    .error(function (data, status, headers, config) {
                                        $scope.groupRelationship = [];
                                        $scope.groupRelationshipTableCollection = [].concat($scope.groupRelationship);
                                    });
	                        }

	                        function isChangeGroupRelation(data) {
	                            if (data.Enable == data.EnableInit) {
	                                data.DataChange = false;
	                            }
	                            else {
	                                data.DataChange = true;
	                            }
	                        }

	                        function reqsData() {
	                            this.SecurityKey = userInfoService.getSecurityKey();
	                            this.OwnerId = chargerInfoData.OwnerId;
	                            this.DeviceId = chargerInfoData.DeviceId;
	                            //this.StationManufactureDate = chargerInfoData.StationManufactureDate;
	                            this.DeviceTypeId = chargerInfoData.DeviceTypeId;
	                        }

	                        function reqsGroupRelationship(groupRelationship) {
	                            this.SecurityKey = userInfoService.getSecurityKey();
	                            this.OwnerId = chargerInfoData.OwnerId;
	                            this.DeviceId = chargerInfoData.DeviceId;
	                            //this.StationManufactureDate = chargerInfoData.StationManufactureDate;
	                            this.DeviceTypeId = chargerInfoData.DeviceTypeId;
	                            this.GroupRelationInfos = groupRelationship;
	                        }

	                    }
	                });

	                modalInstance.result.then(function (data) {
	                    setChargerGroupRelationship(data);
	                });
	            }
	        }
	    };
	});