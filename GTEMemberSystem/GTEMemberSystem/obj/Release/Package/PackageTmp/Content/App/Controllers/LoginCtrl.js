﻿
angular.module('gteAPP').controller('LoginCtrl', function ($scope, $state, userInfoService, apiService) {

    // Initialize
    CheckUserInfo();
    $scope.userLoginModel = {
        account: '',
        password: '',
        captcha: ''
    };
    $scope.captchaUrl = apiService.refreshCaptchaUrl();

    // Login function
    $scope.login = function (data) {
        userInfoService.login(data, callbackForLogin);
    };

    // Refresh Captcha
    $scope.refreshCaptchaUrl = function () {
        $scope.captchaUrl = apiService.refreshCaptchaUrl();
    };

    function callbackForLogin(loginStatusCode) {

        if (loginStatusCode !== null) {
            switch (loginStatusCode) {
                case 0:
                    userInfoService.resetUserInfo();
                    userInfoService.initConnect();

                    $scope.userLoginModel.account = '';
                    $scope.userLoginModel.password = '';
                    $scope.userLoginModel.captcha = '';
                    break;
                case 1:
                    CheckUserInfo();
                    break;
                default:
                    $scope.userLoginModel.account = '';
                    $scope.userLoginModel.password = '';
                    $scope.userLoginModel.captcha = '';
                    $scope.refreshCaptchaUrl();
                    break;
            }
        }

    }

    function CheckUserInfo() {
        if (userInfoService.isLogin() === true && userInfoService.getUserRoleCode() > 0) {

            userInfoService.refreshListInfo();
            var checkListInterval = setInterval(function () {
                if (userInfoService.refreshListDone()) {
                    clearInterval(checkListInterval);
                    $state.go('GTE.Dashboard');
                }
            }, 300);
        }
    }
});