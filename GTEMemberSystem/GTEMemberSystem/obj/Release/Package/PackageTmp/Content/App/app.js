﻿/// <reference path="D:\GTEDeveloper\WorkSpace\PCTool\GTEMemberSystem\GTEMemberSystem\Scripts/ng-map.min.js" />
'use strict';

var app = angular.module('gteAPP', ['oc.lazyLoad', 'ui.router', 'ui.bootstrap', 'smart-table']);

// Set APP Constant
app.constant('version', '0.0.1');
app.constant('apiConfigProvider', {
    METHODGET: 'GET',
    METHODPOST: 'POST',
    CONNECTTIMEOUT: 3000,
    PATHTableInfo: '/api/Account/GetTableInfo',
    PATHAPI: '/api',
    PATHLOGIN: '/api/Account/Login',
    PATHLOGOUT: '/api/Account/Logout',
    PATHREFRESHCAPTCHA: '/api/Account/RefreshCaptcha'
});


app.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {

    $ocLazyLoadProvider.config({
        debug: false,
        events: true,
    });

    $urlRouterProvider.otherwise('/Login');

    var baseTab = {
        name: 'GTE',
        url: "/GTE",
        abstract: true,
        templateUrl: "Page/Main",
        controller: 'MainCtrl',
        resolve: {
            loadMyFiles: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'gteAPP',
                    files: [
                    'Content/App/Controllers/MainCtrl.js',
                    'Content/App/Directives/header.js',
                    'Content/App/Directives/header-notification.js',
                    'Content/App/Directives/sidebar.js'
                    ]
                })
            }
        }
    };

    var dashboardTab = {
        name: 'GTE.Dashboard',
        url: '/Dashboard',
        templateUrl: 'Page/Dashboard',
        controller: 'DashboardCtrl',
        resolve: {
            loadMyFiles: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                                    name:'chart.js',
                                    files:[
                                      'Content/bower_components/angular-chart.js/dist/angular-chart.min.js',
                                      'Content/bower_components/angular-chart.js/dist/angular-chart.css'
                                    ]
                                }),
                    $ocLazyLoad.load({
                    name: 'gteAPP',
                    files: [
                        'Content/App/Controllers/DashboardCtrl.js',
                        'Content/App/Directives/dashboardsummary.js',
                        'Content/App/Directives/dashboardcharger.js'                  
                    ]
                })
            }
        }
    };

    var administratorTab = {
        name: 'GTE.Administrator',
        url: '/Administrator',
        templateUrl: 'Page/Administrator',
        controller: 'AdministratorCtrl',
        resolve: {
            loadMyFiles: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'gteAPP',
                    files: [
                    'Content/App/Controllers/AdministratorCtrl.js',
                    'Content/App/Directives/switchtab.js',
                    'Content/App/Directives/administratorgroup.js',
                    'Content/App/Directives/administratoruser.js',
                    'Content/App/Directives/administratorcharger.js',
                    'Content/App/Directives/administratorbattery.js'
                    ]
                })
            }
        }
    };

    var userProfileTab = {
        name: 'GTE.UserProfile',
        url: '/UserProfile',
        templateUrl: 'Page/UserProfile',
        controller: 'UserProfileCtrl',
        resolve: {
            loadMyFiles: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'gteAPP',
                    files: [
                    'Content/App/Controllers/UserProfileCtrl.js'
                    ]
                })
            }
        }
    };

    var loginTab = {
        name: 'Login',
        url: "/Login",
        templateUrl: 'Page/Login',
        controller: 'LoginCtrl',
        resolve: {
            loadMyFiles: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'gteAPP',
                    files: [
                    'Content/App/Controllers/LoginCtrl.js'
                    ]
                })
            }
        }
    };

    $stateProvider
        .state(baseTab)
        .state(loginTab)
        .state(dashboardTab)
        .state(administratorTab)
        .state(userProfileTab);
}]);

// Initialize app run
app.run(function ($rootScope, $state, userInfoService) {

    $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {

        if (toState.name !== 'Login') {
            if (userInfoService.getUserRoleCode() <= 1) {
                e.preventDefault();

                $state.go('Login');
            }

        } else {
            if (userInfoService.getUserRoleCode() > 1) {
                e.preventDefault();

                $state.go('GTE.Dashboard');
            }
        }
    });
});