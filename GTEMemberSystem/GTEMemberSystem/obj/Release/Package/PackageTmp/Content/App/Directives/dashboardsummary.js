﻿angular.module('gteAPP')
	.directive('dashboardsummary', function () {
	    return {
	        restrict: 'E',
	        replace: true,
	        templateUrl: 'Page/DashboardSummary',
	        controller: function ($scope, $uibModal, $http, apiConfigProvider, userInfoService, apiService) {

	            $scope.TimeFormatList = [{ "Name": "Day", "Code": 1 }, { "Name": "Month", "Code": 2 }, { "Name": "Year", "Code": 3 }];
	            $scope.ChargerStyleFormatList = [{ "Name": "All", "Code": 1 }, { "Name": "Charge Device", "Code": 2 }, { "Name": "Battery", "Code": 3 }];
	            $scope.sChargerModeList = [{ "Name": "Transaction Time", "Code": 1 }, { "Name": "Power Consumption", "Code": 2 }];
	            $scope.sChargerModeFormat = $scope.sChargerModeList[0];
	            $scope.sChargerTimeFormat = $scope.TimeFormatList[0];
	            $scope.sChargerStyleFormat = $scope.ChargerStyleFormatList[0];

	            $scope.sChargerbar = {
	                labels: [],
	                datas: [],
	                series: []
	            };
	            sChargerMode();

	            
	            $scope.sBatteryModeList = [{ "Name": "SOH", "Code": 1 }, { "Name": "Volt. Diff.", "Code": 2 }]; // Fixed by Neil 2016.04.18
	            $scope.sBatteryModeFormat = $scope.sBatteryModeList[0];
	            $scope.sBatteryTypeList = userInfoService.batteryDeviceTypeList();
	            $scope.sBatteryTypeFormat = $scope.sBatteryTypeList[0];
	            $scope.sBatteryPie = {
	                labels: [],
	                datas: []
	            };
	            sBatteryMode();

	            $scope.sChargerMode = sChargerMode;  //切換模式
	            function sChargerMode() {
	                if ($scope.sChargerModeFormat && $scope.sChargerStyleFormat) {
	                    var reqParam = new reqsChargerInfo($scope.sChargerTimeFormat.Code, $scope.sChargerStyleFormat.Code);

	                    var path = apiConfigProvider.PATHAPI + '/DashboardApi/GetTransactionTimeInfo';
	                    if ($scope.sChargerModeFormat.Code === 2) {
	                        path = apiConfigProvider.PATHAPI + '/DashboardApi/GetChargeQuantityInfo';
	                    }

	                    if ($scope.sChargerStyleFormat.Code == 2) {
	                        path = path + 'ByChargeDevice'
	                    } else if ($scope.sChargerStyleFormat.Code == 3) {
	                        path = path + 'ByBattery'
	                    }

	                    var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, path, reqParam);

	                    $http(req)
                            .success(function (data, status, headers, config) {
                                $scope.sChargerbar = {
                                    labels: [],
                                    datas: [],
                                    series: []
                                };
                                if (data && data.length > 0) {

                                    if ($scope.sChargerStyleFormat.Code === 1) {
                                        $scope.sChargerbar.series = ["All"];
                                        $scope.sChargerbar.datas.push(new Array());

                                        data.forEach(function (entry) {
                                            $scope.sChargerbar.labels.push(entry.labels);
                                            $scope.sChargerbar.datas[0].push(entry.datas);
                                        });
                                    } else {

                                        data.forEach(function (entry) {
                                            if (!($scope.sChargerbar.labels.indexOf(entry.labels) > -1)) {
                                                $scope.sChargerbar.labels.push(entry.labels);
                                            }

                                            var seriesIndex = $scope.sChargerbar.series.indexOf(entry.series);
                                            if (!(seriesIndex > -1)) {
                                                $scope.sChargerbar.series.push(entry.series);
                                                $scope.sChargerbar.datas.push(new Array());
                                                seriesIndex = $scope.sChargerbar.series.length - 1;
                                            }

                                            $scope.sChargerbar.datas[seriesIndex].push(entry.datas);
                                        });
                                    }

                                } else {
                                    $scope.sChargerbar.labels.push('No Data');
                                    $scope.sChargerbar.series.push('No Data');
                                    $scope.sChargerbar.datas.push(new Array());
                                    $scope.sChargerbar.datas[0].push(0);
                                }
                            })
                            .error(function (data, status, headers, config) {
                                $scope.sChargerbar = {
                                    labels: [],
                                    datas: [],
                                    series: []
                                };
                                $scope.sChargerbar.labels.push('No Data');
                                $scope.sChargerbar.series.push('No Data');
                                $scope.sChargerbar.datas.push(new Array());
                                $scope.sChargerbar.datas[0].push(0);
                            });

	                } else {
	                    // 
	                }
	            }

	            var sBatteryPieAPI;
	            $scope.sBatteryMode = sBatteryMode;  //切換模式
	            function sBatteryMode() {
	                if ($scope.sBatteryModeFormat) {
	                    var reqParam = new reqsBatteryInfo($scope.sBatteryTypeFormat.Code);

	                    var path = apiConfigProvider.PATHAPI + '/DashboardApi/GetSOHByUserBattery';
	                    // Fixed by Neil 2016.04.18
	                    if ($scope.sBatteryModeFormat.Code === 2) {
	                        path = apiConfigProvider.PATHAPI + '/DashboardApi/GetCellVoltDiffByUserBattery';
	                    }
                        ////
	                    var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, path, reqParam);

	                    $http(req)
                            .success(function (data, status, headers, config) {
                                $scope.sBatteryPie.labels = [];
                                $scope.sBatteryPie.datas = [];

                                if (data && data.length > 0) {

                                    data.forEach(function (entry) {
                                        $scope.sBatteryPie.labels.push(entry.labels);
                                        $scope.sBatteryPie.datas.push(entry.datas);
                                    });
                                } else {
                                    $scope.sBatteryPie.labels.push('No Data');
                                    $scope.sBatteryPie.datas.push(0);
                                }
                            })
                            .error(function (data, status, headers, config) {
                                $scope.sBatteryPie.labels = [];
                                $scope.sBatteryPie.datas = [];

                                $scope.sBatteryPie.labels.push('No Data');
                                $scope.sBatteryPie.push(0);
                            });
	                }
	            }

	            function reqsChargerInfo(timeformat, styleFormat) {
	                this.SecurityKey = userInfoService.getSecurityKey();
	                this.TimeFormat = timeformat;
	                this.StyleFormat = styleFormat;
	            }

	            function reqsBatteryInfo(typeformat, label) {
	                this.SecurityKey = userInfoService.getSecurityKey();
	                this.BatteryTypeCode = typeformat;
	                this.BatteryStyleLabel = label;
	            }

	            $scope.sbatterydetailclick = sBatteryDetailClick;
	            function sBatteryDetailClick(points, evt) {
	                if (points.length === 1) {
	                    
	                    var reqParam = new reqsBatteryInfo($scope.sBatteryTypeFormat.Code, points[0].label);
	                    var path = apiConfigProvider.PATHAPI + '/DashboardApi/GetSOHByUserBattery';
	                    // Fixed by Neil 2016.04.18
	                    if ($scope.sBatteryModeFormat.Code === 2) {
	                        path = apiConfigProvider.PATHAPI + '/DashboardApi/GetCellVoltDiffByUserBattery';
	                    }
	                    ////
	                    var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, path, reqParam);

	                    showBatteryCurrentDetail(req);
	                }
	            };

	            function showBatteryCurrentDetail(reqObject) {

	                var modalInstance = $uibModal.open({
	                    templateUrl: 'batteryCurrentDetail.html',
	                    resolve: {
	                        reqObject: function () {
	                            return reqObject;
	                        }
	                    },
	                    controller: function ($scope, $modalInstance, reqObject, userInfoService) {

	                        $scope.batteryDeviceTypeList = userInfoService.batteryDeviceTypeList();
	                        $scope.setBatteryDeviceType = setBatteryDeviceType;
	                        $scope.batteryRecordList = [];
	                        getBatteryDetail();

	                        $scope.confirm = function () {
	                            $modalInstance.dismiss('cancel');
	                        };

	                        function setBatteryDeviceType(batteryDeviceTypeCode) {
	                            return userInfoService.getObjectByCode($scope.batteryDeviceTypeList, batteryDeviceTypeCode).Name;
	                        }

	                        function getBatteryDetail() {

	                            $http(reqObject)
                                    .success(function (data, status, headers, config) {
                                        if (data) {
                                            $scope.batteryRecordList = data;

                                        } else {
                                            $scope.batteryRecordList = [];
                                        }

                                        $scope.batteryRecordTableCollection = [].concat($scope.batteryRecordList);
                                    })
                                    .error(function (data, status, headers, config) {
                                        $scope.batteryRecordList = [];
                                        $scope.batteryRecordTableCollection = [].concat($scope.batteryRecordList);
                                    });
	                        }
	                    }
	                });

	                //modalInstance.result.then(function (data) {
	                //    setBatteryGroupRelationship(data);
	                //});
	            }

	            //$scope.batteryTypeList = userInfoService.batteryDeviceTypeList();
	            //$scope.batteryStatusFormat = $scope.batteryTypeList[0];

	            //$scope.batterystatuspie = {
	            //    labels: [],
	            //    data: []
	            //};

	            //$scope.refreshBatteryStatusInfo = function () {
	            //    var reqChartInfo = new requestChartInfo($scope.batteryStatusFormat.Code);
	            //    var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHAPI + '/DashboardApi/GetBatteryStatusPieData', reqChartInfo);

	            //    $http(req)
                //        .success(function (data, status, headers, config) {
                //            if (data) {
                //                var xName = [];
                //                var dataValus = [];
                //                data.forEach(function (entry) {
                //                    xName.push(entry.Name);
                //                    dataValus.push(entry.Value);
                //                });
                //                $scope.batterystatuspie.labels = xName;
                //                $scope.batterystatuspie.data = [];
                //                $scope.batterystatuspie.data.push(dataValus);
                //            }
                //        })
                //        .error(function (data, status, headers, config) {

                //        });
	            //};

	            //function requestChartInfo(dataformat) {
	            //    this.SecurityKey = userInfoService.getAccount();
	            //    this.DataFormat = dataformat;
	            //}
	        }
	    };
	});