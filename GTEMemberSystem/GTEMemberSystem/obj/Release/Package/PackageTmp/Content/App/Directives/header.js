﻿angular.module('gteAPP')
	.directive('header', function () {
	    return {
	        templateUrl: 'Page/MainHeader',
	        restrict: 'E',
	        replace: true,
	    };
	});