﻿angular.module('gteAPP')
	.directive('headerNotification', function () {
	    return {
	        templateUrl: 'Page/MainHeaderNotification',
	        restrict: 'E',
	        replace: true,
	    };
	});