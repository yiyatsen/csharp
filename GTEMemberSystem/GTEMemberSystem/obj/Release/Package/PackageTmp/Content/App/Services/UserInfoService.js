﻿app.service('userInfoService', function ($http, $q, apiConfigProvider) {

    var self = this;
    var isConnection = false; // Only one request for account api

    // User Information Object
    var userInfo = {
        isLogin: false,
        securityKey: null,
        account: null,
        password: null,
        userName: null,
        userRoleCode: 0,
        userRoleName: null
    };
    var googleMap;
    var mapIconList;

    var timeFormatList = null;
    var userRoleList = null;
    var statusList = null;
    var ownerTypeList = null;
    var relationRoleList = null;
    var chargerDeviceTypeList = null;
    var batteryDeviceTypeList = null;

    var refreshListNumber = 7;

    self.getObjectByCode = function (arr, value) {

        var result = arr.filter(function (o) { return o.Code == value; });

        return result ? result[0] : null; // or undefined
    }

    self.setMapIconInfo = function (iconList) {
        mapIconList = iconList;
    };
    self.getMapIconInfo = function () {
        return mapIconList;
    };

    self.setMap = function (map) {
        googleMap = map;
    };
    self.getMap = function () {
        return googleMap;
    };

    self.timeFormatList = function () {
        return timeFormatList;
    };

    self.userRoleList = function () {
        return userRoleList;
    };

    self.statusList = function () {
        return statusList;
    };

    self.ownerTypeList = function () {
        return ownerTypeList;
    };

    self.relationRoleList = function () {
        return relationRoleList;
    };

    self.chargerDeviceTypeList = function () {
        return chargerDeviceTypeList;
    };

    self.batteryDeviceTypeList = function () {
        return batteryDeviceTypeList;
    };

    self.isLogin = function () {
        return userInfo.isLogin;
    };

    self.getAccount = function () {
        return userInfo.account;
    };

    self.getPassword = function () {
        return userInfo.password;
    };

    self.getUserName = function () {
        return userInfo.userName;
    };

    self.getUserRoleCode = function () {
        return userInfo.userRoleCode;
    };

    self.getUserRoleName = function () {
        return userInfo.userRoleName;
    };

    self.getSecurityKey = function () {
        return userInfo.securityKey;
    };

    self.resetUserInfo = function () {
        userInfo.isLogin = false;
        userInfo.account = null;
        userInfo.securityKey = null;
        userInfo.userName = null;
        userInfo.userRoleCode = 0;
        userInfo.userRoleName = null;
    };

    self.refreshListInfo = function () {
        getTimeFormatList();
        getUserRoleList();
        getStatusList();
        getDeviceOwnerTypeList();
        getRelationRoleList();
        getChargerTypeList();
        getBatteryTypeList();
    };

    self.refreshListDone = function () {
        return refreshListNumber < 1;
    };

    self.initConnect == function () {
        isConnection = false;
    };

    self.login = function (value, callbackFunction) {
        if (!userInfo.isLogin && !isConnection && value !== null) {
            switchConnectionState();
            if (value.account === '' || value.password === '' || value.captcha === '') {
                if (callbackFunction) {
                    callbackFunction(2);
                }
                switchConnectionState();
            } else {
                userInfo.account = value.account;
                userInfo.password = value.password;

                var req = new requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHLOGIN, value);

                $http(req)
                    .success(function (data, status, headers, config) {

                        if (data.LoginStatusCode === 1) {
                            userInfo.isLogin = true;
                        } else {
                            userInfo.isLogin = false;
                        }

                        userInfo.account = data.Account;
                        userInfo.securityKey = data.SecurityKey;
                        userInfo.userName = data.UserName;
                        userInfo.userRoleCode = data.UserRoleCode;
                        userInfo.userRoleName = data.UserRoleName;

                        switchConnectionState();

                        if (callbackFunction) {
                            callbackFunction(data.LoginStatusCode);
                        }
                    })
                    .error(function (data, status, headers, config) {

                        self.resetUserInfo();

                        switchConnectionState();

                        if (callbackFunction) {
                            callbackFunction(-1);
                        }
                    });
            }

        } else {
            if (callbackFunction) {
                callbackFunction(0);
            }
        }
    };

    self.logout = function (callbackFunction) {
        if (userInfo.isLogin && !isConnection) {
            switchConnectionState();

            var req = new requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHLOGOUT, userInfo);

            $http(req)
                .success(function (data, status, headers, config) {

                    self.resetUserInfo();

                    switchConnectionState();

                    if (callbackFunction) {
                        callbackFunction(1);
                    }
                })
                .error(function (data, status, headers, config) {
                    switchConnectionState();
                    if (callbackFunction) {
                        callbackFunction(-1);
                    }
                });
        } else {
            if (callbackFunction) {
                callbackFunction(0);
            }
        }
    };

    function requestFormat(rqMethod, rqURL, rqData) {
        this.method = rqMethod;
        this.url = rqURL;
        if (rqData != null) {
            this.data = rqData;
        }
        this.timeout = apiConfigProvider.CONNECTTIMEOUT;
    }

    function switchConnectionState() {
        isConnection = !isConnection;
    }

    function getTimeFormatList() {
        var req = new requestFormat(apiConfigProvider.METHODGET, apiConfigProvider.PATHAPI + '/CodeApi/GetTimeFormatList', null);

        $http(req)
            .success(function (data, status, headers, config) {
                if (data) {
                    timeFormatList = data;
                }
                refreshListNumber = refreshListNumber - 1;
            })
            .error(function (data, status, headers, config) {
                timeFormatList = null;
                refreshListNumber = refreshListNumber - 1;
            });
    }

    function getUserRoleList() {
        var req = new requestFormat(apiConfigProvider.METHODGET, apiConfigProvider.PATHAPI + '/CodeApi/GetUserRoleList', null);

        $http(req)
            .success(function (data, status, headers, config) {
                if (data) {
                    userRoleList = data;
                }
                refreshListNumber = refreshListNumber - 1;
            })
            .error(function (data, status, headers, config) {
                userRoleList = null;
                refreshListNumber = refreshListNumber - 1;
            });
    }

    function getStatusList() {
        var req = new requestFormat(apiConfigProvider.METHODGET, apiConfigProvider.PATHAPI + '/CodeApi/getStatusList', null);

        $http(req)
            .success(function (data, status, headers, config) {
                if (data) {
                    statusList = data;
                }
                refreshListNumber = refreshListNumber - 1;
            })
            .error(function (data, status, headers, config) {
                statusList = null;
                refreshListNumber = refreshListNumber - 1;
            });
    }

    function getDeviceOwnerTypeList() {
        var req = new requestFormat(apiConfigProvider.METHODGET, apiConfigProvider.PATHAPI + '/CodeApi/GetDeviceOwnerTypeList', null);

        $http(req)
            .success(function (data, status, headers, config) {
                if (data) {
                    ownerTypeList = data;
                }
                refreshListNumber = refreshListNumber - 1;
            })
            .error(function (data, status, headers, config) {
                ownerTypeList = null;
                refreshListNumber = refreshListNumber - 1;
            });
    }

    function getRelationRoleList() {
        var req = new requestFormat(apiConfigProvider.METHODGET, apiConfigProvider.PATHAPI + '/CodeApi/GetRelationRoleList', null);

        $http(req)
            .success(function (data, status, headers, config) {
                if (data) {
                    relationRoleList = data;
                }
                refreshListNumber = refreshListNumber - 1;
            })
            .error(function (data, status, headers, config) {
                relationRoleList = null;
                refreshListNumber = refreshListNumber - 1;
            });
    }

    function getChargerTypeList() {
        var req = new requestFormat(apiConfigProvider.METHODGET, apiConfigProvider.PATHAPI + '/CodeApi/GetChargeDeviceTypeList', null);

        $http(req)
            .success(function (data, status, headers, config) {
                if (data) {
                    chargerDeviceTypeList = data;
                }
                refreshListNumber = refreshListNumber - 1;
            })
            .error(function (data, status, headers, config) {
                chargerDeviceTypeList = null;
                refreshListNumber = refreshListNumber - 1;
            });
    }

    function getBatteryTypeList() {
        var req = new requestFormat(apiConfigProvider.METHODGET, apiConfigProvider.PATHAPI + '/CodeApi/GetBatteryTypeList', null);

        $http(req)
            .success(function (data, status, headers, config) {
                if (data) {
                    batteryDeviceTypeList = data;
                }
                refreshListNumber = refreshListNumber - 1;
            })
            .error(function (data, status, headers, config) {
                batteryDeviceTypeList = null;
                refreshListNumber = refreshListNumber - 1;
            });
    }
});


