﻿angular.module('gteAPP')
	.directive('administratorgroup', function () {
	    return {
	        restrict: 'E',
	        replace: true,
	        templateUrl: 'Page/AdministratorGroup',
	        controller: function ($scope, $uibModal, $http, apiConfigProvider, userInfoService, apiService) {

	            // GroupInfo
	            var editIndex = -1;

	            $scope.setChargerDeviceType = setChargerDeviceType;
	            $scope.setGroupStatus = setGroupStatus;
	            $scope.setRelationRole = setRelationRole;
	            $scope.getGroupInfoList = getGroupInfoList;
	            $scope.editGroupInfo = editGroupInfo;
	            $scope.groupStatusList = userInfoService.statusList();
	            $scope.relationRoleList = userInfoService.relationRoleList();
	            getGroupInfoList();

	            $scope.relateModeList = [{ "Name": "User", "Code": 1 }, { "Name": "Card", "Code": 2 }, { "Name": "Charge Device", "Code": 3 }, { "Name": "Battery", "Code": 4 }];
	            $scope.relateModeFormat = $scope.relateModeList[0];
	            $scope.currentGroup;

	            function setChargerDeviceType(chargerDeviceTypeCode) {
	                return userInfoService.getObjectByCode($scope.chargerDeviceTypeList, chargerDeviceTypeCode).Name;
	            }

	            function setGroupStatus(groupStatusCode) {              
	                return userInfoService.getObjectByCode($scope.groupStatusList, groupStatusCode).Name;
	            }

	            function setRelationRole(code) {
	                return userInfoService.getObjectByCode($scope.relationRoleList, code).Name;
	            }

	            function getGroupInfoList() {
	                var req = new apiService.requestFormat(apiConfigProvider.METHODGET, apiConfigProvider.PATHAPI + '/AdministratorInfo/GetGroupInfos', null);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            if (data) {
                                $scope.groupInfoList = data;
                                $scope.groupInfoTableCollection = [].concat($scope.groupInfoList);
                                if ($scope.groupInfoList && $scope.groupInfoList.length > 0) {
                                    if (!$scope.currentGroup || $scope.groupInfoList[0].GroupId !== $scope.currentGroup.GroupId) {
                                        $scope.currentGroup = $scope.groupInfoList[0];
                                        refreshGroupRelationInfo();
                                    }
                                }
                            } else {
                                $scope.groupInfoList = [];
                            }
                            $scope.groupInfoTableCollection = [].concat($scope.groupInfoList);
                        })
                        .error(function (data, status, headers, config) {
                            $scope.groupInfoList = [];
                            $scope.groupInfoTableCollection = [].concat($scope.groupInfoList);
                        });
	            }

	            $scope.clickGroupRelationInfo = clickGroupRelationInfo;
	            function clickGroupRelationInfo(x) {
	                $scope.currentGroup = x;
	                refreshGroupRelationInfo();
	            }
	            $scope.refreshGroupRelationInfo = refreshGroupRelationInfo;
	            function refreshGroupRelationInfo() {
	                if ($scope.relateModeFormat.Code === 1) {
	                    getGroupUserList();
	                } else if ($scope.relateModeFormat.Code === 2) {
	                    getGroupCardList();
	                } else if ($scope.relateModeFormat.Code === 3) {
	                    getGroupChargerList();
	                } else if ($scope.relateModeFormat.Code === 4) {
	                    getGroupBatteryList();
	                }
	            }

	            function getGroupUserList() {
	                var param = new requestGroupRelationInfo();
	                var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHAPI + '/AdministratorInfo/GetGroupUserInfosByGroupInfo', param);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            if (data) {
                                $scope.groupUserList = data;
                                
                            } else {
                                $scope.groupUserList = [];
                            }
                            $scope.groupUserTableCollection = [].concat($scope.groupUserList);
                        })
                        .error(function (data, status, headers, config) {
                            $scope.groupUserList = [];
                            $scope.groupUserTableCollection = [].concat($scope.groupUserList);
                        });
	            }

	            function getGroupCardList() {
	                var param = new requestGroupRelationInfo();
	                var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHAPI + '/AdministratorInfo/GetGroupCardInfosByGroupInfo', param);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            if (data) {
                                $scope.groupCardList = data;
                            } else {
                                $scope.groupCardList = [];
                            }
                            $scope.groupCardTableCollection = [].concat($scope.groupCardList);
                        })
                        .error(function (data, status, headers, config) {
                            $scope.groupCardList = [];
                            $scope.groupCardTableCollection = [].concat($scope.groupCardList);
                        });
	            }

	            function getGroupChargerList() {
	                var param = new requestGroupRelationInfo();
	                var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHAPI + '/AdministratorInfo/GetGroupChargerInfosByGroupInfo', param);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            if (data) {
                                $scope.groupChargerList = data;
                            } else {
                                $scope.groupChargerList = [];
                            }
                            $scope.groupChargerTableCollection = [].concat($scope.groupChargerList);
                        })
                        .error(function (data, status, headers, config) {
                            $scope.groupChargerList = [];
                            $scope.groupChargerTableCollection = [].concat($scope.groupChargerList);
                        });
	            }

	            function getGroupBatteryList() {
	                var param = new requestGroupRelationInfo();
	                var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiConfigProvider.PATHAPI + '/AdministratorInfo/GetGroupBatteryInfosByGroupInfo', param);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            if (data) {
                                $scope.groupBatteryList = data;
                            } else {
                                $scope.groupBatteryList = [];
                            }
                            $scope.groupBatteryTableCollection = [].concat($scope.groupBatteryList);
                        })
                        .error(function (data, status, headers, config) {
                            $scope.groupBatteryList = [];
                            $scope.groupBatteryTableCollection = [].concat($scope.groupBatteryList);
                        });
	            }

	            function setGroupInfo(groupInfoModel, mode) {

	                groupInfoModel.StatusCode = groupInfoModel.Status.Code;

	                var apiString = apiConfigProvider.PATHAPI + '/AdministratorInfo/CreateGroupInfo';
	                if (mode == 0) {
	                    apiString = apiConfigProvider.PATHAPI + '/AdministratorInfo/EditGroupInfo';
	                }
	                var req = new apiService.requestFormat(apiConfigProvider.METHODPOST, apiString, groupInfoModel);

	                $http(req)
                        .success(function (data, status, headers, config) {
                            if (data > 0) {
                                if (editIndex === -1) {
                                    $scope.groupInfoList.push(groupInfoModel); //加到清單內
                                } else {
                                    $scope.groupInfoList[editIndex] = groupInfoModel; //更新清單內容
                                }
                                showAlert(true, 'Update Success');
                            } else {
                                showAlert(false, 'Update Fail');
                            }
                        })
                        .error(function (data, status, headers, config) {
                            //錯誤訊息
                            showAlert(false, 'Connect Fail');
                        });
	            }

	            function editGroupInfo(data, title) {
	                if (data) {
	                    editIndex = $scope.groupInfoList.indexOf(data);

	                } else {
	                    editIndex = -1;
	                }

	                var modalInstance = $uibModal.open({
	                    templateUrl: 'editGroupModalContent.html',
	                    resolve: {
	                        groupInfoData: function () {
	                            return data;
	                        },
	                        title: function () {
	                            return title;
	                        }
	                    },
	                    controller: function ($scope, $modalInstance, groupInfoData, title, userInfoService) {
	                        $scope.editTitle = 'Add';
	                        if (title == 0) {
	                            $scope.editTitle = 'Edit';
	                        }

	                        $scope.userRoleCode = userInfoService.getUserRoleCode();
	                        $scope.groupStatusList = userInfoService.statusList();
	                        $scope.userRelationRoleList = userInfoService.relationRoleList();
	                        
	                        $scope.groupInfoModel = new editGroupInfoModel(groupInfoData);


	                        $scope.save = function () {
	                            $modalInstance.close($scope.groupInfoModel);
	                        };

	                        $scope.cancel = function () {
	                            $modalInstance.dismiss('cancel');
	                        };

	                        function editGroupInfoModel(data) {
	                            this.SecurityKey = userInfoService.getSecurityKey();
	                            if (data) {
	                                this.GroupId = data.GroupId;
	                                this.GroupName = data.GroupName;
	                                this.ParentGroupId = data.ParentGroupId;
	                                this.Description = data.Description;
	                                this.StatusCode = data.StatusCode;
	                                this.RelationRoleCode = data.RelationRoleCode;

	                                this.Status = userInfoService.getObjectByCode($scope.groupStatusList, data.StatusCode);
	                                this.RelationRole = userInfoService.getObjectByCode($scope.userRelationRoleList, data.RelationRoleCode);
	                            } else {
	                                this.GroupId = null;
	                                this.GroupName = null;
	                                this.ParentGroupId = null;
	                                this.Description = null;
	                                this.StatusCode = 0;
	                                this.RelationRoleCode = 0;

	                                this.Status = userInfoService.getObjectByCode($scope.groupStatusList, 0);
	                                this.RelationRole = userInfoService.getObjectByCode($scope.userRelationRoleList, 0);
	                            }

	                        }
	                    }
	                });

	                modalInstance.result.then(function (data) {
	                    setGroupInfo(data, title);
	                });
	            }

	            function requestGroupRelationInfo() {
	                this.SecurityKey = userInfoService.getSecurityKey();
	                this.GroupId = $scope.currentGroup.GroupId;
	                this.RelationRoleCode = $scope.currentGroup.RelationRoleCode;
	            }
	        }
	    };
	});