﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GTEMemberSystem.Models.DBConfigure
{
    public class GTEDMSDBInitializer : CreateDatabaseIfNotExists<GTEDMSDBContext>
    {
        protected override void Seed(GTEDMSDBContext context)
        {
            var data0 = new List<UserInfo>{
                new UserInfo{
                    Account = "admin",
                    Password = "admin",
                    Email = "gtenergy.developer@gmail.com",
                    Address = "新北市汐止區大同路二段175號11樓之1",
                    UserName = "系統管理者",
                    Phone = "0277080899",
                    UserRoleCode = Models.UserRoleCode.Administrator,
                    RegisterDateTime = new DateTime(2012, 1, 1),
                    RefId = 0
                },
                new UserInfo{
                    Account = "gte.service",
                    Password = "gte.service",
                    Email = "gtenergy.developer@gmail.com",
                    Address = "新北市汐止區大同路二段175號11樓之1",
                    UserName = "高達能源客服人員",
                    Phone = "0277080899",
                    UserRoleCode = Models.UserRoleCode.Manager,
                    RegisterDateTime = new DateTime(2012, 1, 1),
                    RefId = 1
                },
                new UserInfo{
                    Account = "oodobike",
                    Password = "oodobike",
                    Email = "oodo.bike06@msa.hinet.net",
                    Address = "台南市安平區安平路658號",
                    UserName = "歐多賣",
                    Phone = "062202718",
                    UserRoleCode = Models.UserRoleCode.Cooperator,
                    RegisterDateTime = new DateTime(2012, 1, 1),
                    RefId = 1
                }
            };
            data0.ForEach(d => context.UserInfo.Add(d));
            context.SaveChanges();

            var data1 = new List<GroupInfo>{
                new GroupInfo{
                    GroupName = "高達能源交換系統",
                    OwnerId = 1,
                    StatusCode = StatusCode.Useable,
                    RegisterDateTime = new DateTime(2012, 1, 1)
                }
            };
            data1.ForEach(d => context.GroupInfo.Add(d));

            context.SaveChanges();

            data1 = new List<GroupInfo>{
                new GroupInfo{
                    GroupName = "高達測試系統",
                    ParentGroupId = 1,
                    OwnerId = 1,
                    StatusCode = StatusCode.Useable,
                    RegisterDateTime = new DateTime(2012, 1, 1)
                },
                new GroupInfo{
                    GroupName = "台南歐多賣系統",
                    OwnerId = 3,
                    StatusCode = StatusCode.Useable,
                    RegisterDateTime = new DateTime(2012, 1, 1)
                }
            };
            data1.ForEach(d => context.GroupInfo.Add(d));

            context.SaveChanges();
            //var data3 = new List<UserGroupRelationship>{
            //    new UserGroupRelationship{
            //        UserId = 2,
            //        GroupId =1,
            //        RelationRoleCode = Models.RelationRoleCode.Manager
            //    },
            //    new UserGroupRelationship{
            //        UserId = 3,
            //        GroupId =2,
            //        RelationRoleCode = Models.RelationRoleCode.Manager
            //    },
            //    new UserGroupRelationship{
            //        UserId = 4,
            //        GroupId =1,
            //        RelationRoleCode = Models.RelationRoleCode.Manager
            //    },
            //    new UserGroupRelationship{
            //        UserId = 4,
            //        GroupId =2,
            //        RelationRoleCode = Models.RelationRoleCode.Manager
            //    }
            //};
            //data3.ForEach(d => context.UserGroupRelationship.Add(d));

            var data3 = new List<GroupUserInfo>{
                new GroupUserInfo{
                    UserId = 2,
                    GroupId = 1,
                    RelationRoleCode = Models.RelationRoleCode.Manager
                },
                new GroupUserInfo{
                    UserId = 2,
                    GroupId = 2,
                    RelationRoleCode = Models.RelationRoleCode.Manager
                },
                new GroupUserInfo{
                    UserId = 2,
                    GroupId = 3,
                    RelationRoleCode = Models.RelationRoleCode.Manager
                },
                new GroupUserInfo{
                    UserId = 3,
                    GroupId = 3,
                    RelationRoleCode = Models.RelationRoleCode.Manager
                }
            };
            data3.ForEach(d => context.GroupUserInfo.Add(d));

            context.SaveChanges();

            //var data4 = new List<GroupCardInfo>{
            //    new GroupCardInfo{
            //        CardId = "ABCDE67890",
            //        UserId = 4,
            //        GroupId =1,
            //        StatusCode = Models.StatusCode.Useable,
            //        DeviceOwnerTypeCode = Models.DeviceOwnerTypeCode.Administrator,
            //        RegisterDateTime = DateTime.UtcNow
            //    },
            //    new GroupCardInfo{
            //        CardId = "ABCDE67891",
            //        UserId = 4,
            //        GroupId =1,
            //        StatusCode = Models.StatusCode.Useable,
            //        DeviceOwnerTypeCode = Models.DeviceOwnerTypeCode.Group,
            //        RegisterDateTime = DateTime.UtcNow
            //    },                
            //    new GroupCardInfo{
            //        CardId = "ABCDE67892",
            //        UserId = 4,
            //        GroupId =1,
            //        StatusCode = Models.StatusCode.Useable,
            //        DeviceOwnerTypeCode = Models.DeviceOwnerTypeCode.User,
            //        RegisterDateTime = DateTime.UtcNow
            //    }
            //};
            //data4.ForEach(d => context.GroupCardInfo.Add(d));

            var data5 = new List<BatteryType>{
                new BatteryType{
                    DeviceTypeName = "4816",
                    DesignVoltage = 48000,
                    DesignCapacity = 17200,
                    DesignCellsNumber = 14
                },
                new BatteryType{
                    DeviceTypeName = "4840",
                    DesignVoltage = 48000,
                    DesignCapacity = 40000,
                    DesignCellsNumber = 14
                }
            };
            data5.ForEach(d => context.BatteryType.Add(d));

            context.SaveChanges();

            var data7 = new List<ChargeDeviceType>{
                new ChargeDeviceType{
                    DeviceTypeName = "EXN Station"
                },
                new ChargeDeviceType{
                    DeviceTypeName = "Smart Charger"
                }
            };
            data7.ForEach(d => context.ChargeDeviceType.Add(d));

            context.SaveChanges();

            var data8 = new List<ChargeDeviceInfo>{
                new ChargeDeviceInfo{
                    DeviceTypeId = 1,
                    DeviceId = "1212010001",
                    Password = "1234",
                    Name = "高達測試交換站",
                    Address = "新北市汐止區大同路二段175號11樓之1",
                    StatusCode = Models.StatusCode.Useable,
                    BatteryTypeId = 1,
                    Longitude = 120.167421,
                    Latitude = 22.999318,
                    OwnerId = 1,
                    RegisterDateTime = new DateTime(2012, 1, 1),
                    LastLoginTime = new DateTime(2012, 1, 1)
                },                             
                new ChargeDeviceInfo{
                    DeviceTypeId = 1,
                    DeviceId = "1212010008",
                    Password = "oq168WvQ",
                    Name = "台南安平4816交換站",
                    Address = "台南市安平區安平路658號",
                    StatusCode = Models.StatusCode.Useable,
                    BatteryTypeId = 1,
                    Longitude = 120.167421,
                    Latitude = 22.999318,
                    OwnerId = 3,
                    RegisterDateTime = new DateTime(2012, 9, 30),
                    LastLoginTime = new DateTime(2012, 9, 30)
                }
                ////,                             
                //new ChargerInfo{
                //    StationManufactureDate = "2012/01/01",
                //    DeviceTypeId = 1,
                //    StationId = "0000000004",
                //    Password = "th6EcSpV",
                //    Name = "悠活4816交換站",
                //    Longitude = 120.7052600,
                //    Latitude = 21.9977290,
                //    StatusCode = Models.StatusCode.Useable,
                //    BatteryTypeId = 1,                    
                //    OwnerId = 2,
                //    RegisterDateTime = new DateTime(2012, 9, 30),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2012/01/01",
                //    DeviceTypeId = 1,
                //    StationId = "0000000006",
                //    Password = "WDsHja3E",
                //    Name = "南灣4816交換站",
                //    BatteryTypeId = 1,
                //    Longitude = 120.7629570,
                //    Latitude = 21.9599090,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 2,
                //    RegisterDateTime = new DateTime(2012, 9, 30),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2012/01/01",
                //    DeviceTypeId = 1,
                //    StationId = "0000000007",
                //    Password = "nOl9Uo2U",
                //    Name = "南灣4840交換站",
                //    BatteryTypeId = 2,
                //    Longitude = 120.7629570,
                //    Latitude = 21.9599090,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 2,
                //    RegisterDateTime = new DateTime(2012, 9, 30),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2012/01/01",
                //    DeviceTypeId = 1,
                //    StationId = "0000000009",
                //    Password = "aeqetGay",
                //    Name = "恆春轉運站4840交換站",
                //    BatteryTypeId = 2,
                //    Longitude = 120.7453800,
                //    Latitude = 22.0025720,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 2,
                //    RegisterDateTime = new DateTime(2012, 9, 30),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2013/11/21",
                //    DeviceTypeId = 2,
                //    StationId = "0000000188",
                //    Password = "UfwriKiY",
                //    Name = "元智大學188",
                //    Longitude = 120.7453800,
                //    Latitude = 22.0025720,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 1, 2),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2013/11/22",
                //    DeviceTypeId = 2,
                //    StationId = "0000000197",
                //    Password = "Dq0DoaHA",
                //    Name = "元智大學197",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 1, 2),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2013/11/25",
                //    DeviceTypeId = 2,
                //    StationId = "0000000200",
                //    Password = "rl39P7ZC",
                //    Name = "元智大學200",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 1, 2),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2013/11/25",
                //    DeviceTypeId = 2,
                //    StationId = "0000000202",
                //    Password = "J0xMV75o",
                //    Name = "元智大學202",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 1, 2),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2013/11/26",
                //    DeviceTypeId = 2,
                //    StationId = "0000000207",
                //    Password = "7JyoWqCp",
                //    Name = "元智大學207",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 1, 2),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2013/11/29",
                //    DeviceTypeId = 2,
                //    StationId = "0000000208",
                //    Password = "CipCYNZe",
                //    Name = "元智大學208",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 1, 2),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2013/12/02",
                //    DeviceTypeId = 2,
                //    StationId = "0000000211",
                //    Password = "tZQ9p1Ag",
                //    Name = "元智大學211",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 1, 2),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2014/04/10",
                //    DeviceTypeId = 2,
                //    StationId = "0000000216",
                //    Password = "rq7BtDZr",
                //    Name = "元智大學216",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 9, 1),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2014/04/10",
                //    DeviceTypeId = 2,
                //    StationId = "0000000217",
                //    Password = "Y8sanH8b",
                //    Name = "元智大學217",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 9, 1),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2014/04/10",
                //    DeviceTypeId = 2,
                //    StationId = "0000000218",
                //    Password = "ZC7MEdpP",
                //    Name = "元智大學218",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 9, 1),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2014/07/17",
                //    DeviceTypeId = 2,
                //    StationId = "0000000225",
                //    Password = "y4PYfNW4",
                //    Name = "元智大學225",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 9, 1),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2014/07/18",
                //    DeviceTypeId = 2,
                //    StationId = "0000000227",
                //    Password = "FWByCnUl",
                //    Name = "元智大學227",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 9, 1),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2014/07/18",
                //    DeviceTypeId = 2,
                //    StationId = "0000000229",
                //    Password = "jsAZW5Gc",
                //    Name = "元智大學229",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 9, 1),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2014/07/18",
                //    DeviceTypeId = 2,
                //    StationId = "0000000231",
                //    Password = "OYjXjfCV",
                //    Name = "元智大學231",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 9, 1),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2014/07/18",
                //    DeviceTypeId = 2,
                //    StationId = "0000000232",
                //    Password = "bi4a893J",
                //    Name = "元智大學232",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 9, 1),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2014/07/18",
                //    DeviceTypeId = 2,
                //    StationId = "0000000233",
                //    Password = "NnYBrv5v",
                //    Name = "元智大學233",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 9, 1),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2014/08/22",
                //    DeviceTypeId = 2,
                //    StationId = "0000000243",
                //    Password = "JRWKlmbG",
                //    Name = "元智大學243",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 9, 1),
                //    LastOnlineTime= DateTime.UtcNow
                //},                             
                //new ChargerInfo{
                //    StationManufactureDate = "2014/08/25",
                //    DeviceTypeId = 2,
                //    StationId = "0000000244",
                //    Password = "dIedo35p",
                //    Name = "元智大學244",
                //    Longitude = 121.2664937,
                //    Latitude = 24.9699006,
                //    StatusCode = Models.StatusCode.Useable,
                //    OwnerId = 3,
                //    RegisterDateTime = new DateTime(2014, 9, 1),
                //    LastOnlineTime= DateTime.UtcNow
                //}
            };
            data8.ForEach(d => context.ChargeDeviceInfo.Add(d));

            context.SaveChanges();

            var data9 = new List<GroupChargeDeviceInfo>{                             
                new GroupChargeDeviceInfo{
                    DeviceTypeId = 1,
                    DeviceId = "1212010008",
                    GroupId = 3
                } 
                //new GroupChargerInfo{
                //    StationManufactureDate = "2012/01/01",
                //    DeviceTypeId = 1,
                //    StationId = "0000000004",
                //    GroupId = 1
                //},                         
                //new GroupChargerInfo{
                //    StationManufactureDate = "2012/01/01",
                //    DeviceTypeId = 1,
                //    StationId = "0000000006",
                //    GroupId = 1
                //},                          
                //new GroupChargerInfo{
                //    StationManufactureDate = "2012/01/01",
                //    DeviceTypeId = 1,
                //    StationId = "0000000007",
                //    GroupId = 1
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2012/01/01",
                //    DeviceTypeId = 1,
                //    StationId = "0000000009",
                //    GroupId = 1
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2013/11/21",
                //    DeviceTypeId = 2,
                //    StationId = "0000000188",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2013/11/22",
                //    DeviceTypeId = 2,
                //    StationId = "0000000197",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2013/11/25",
                //    DeviceTypeId = 2,
                //    StationId = "0000000200",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2013/11/25",
                //    DeviceTypeId = 2,
                //    StationId = "0000000202",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2013/11/26",
                //    DeviceTypeId = 2,
                //    StationId = "0000000207",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2013/11/29",
                //    DeviceTypeId = 2,
                //    StationId = "0000000208",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2013/12/02",
                //    DeviceTypeId = 2,
                //    StationId = "0000000211",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2014/04/10",
                //    DeviceTypeId = 2,
                //    StationId = "0000000216",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2014/04/10",
                //    DeviceTypeId = 2,
                //    StationId = "0000000217",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2014/04/10",
                //    DeviceTypeId = 2,
                //    StationId = "0000000218",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2014/07/17",
                //    DeviceTypeId = 2,
                //    StationId = "0000000225",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2014/07/18",
                //    DeviceTypeId = 2,
                //    StationId = "0000000227",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2014/07/18",
                //    DeviceTypeId = 2,
                //    StationId = "0000000229",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2014/07/18",
                //    DeviceTypeId = 2,
                //    StationId = "0000000231",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2014/07/18",
                //    DeviceTypeId = 2,
                //    StationId = "0000000232",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2014/07/18",
                //    DeviceTypeId = 2,
                //    StationId = "0000000233",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2014/08/22",
                //    DeviceTypeId = 2,
                //    StationId = "0000000243",
                //    GroupId = 2
                //},                             
                //new GroupChargerInfo{
                //    StationManufactureDate = "2014/08/25",
                //    DeviceTypeId = 2,
                //    StationId = "0000000244",
                //    GroupId = 2
                //}
            };
            data9.ForEach(d => context.GroupChargeDeviceInfo.Add(d));

            context.SaveChanges();

            base.Seed(context);
        }
    }
}