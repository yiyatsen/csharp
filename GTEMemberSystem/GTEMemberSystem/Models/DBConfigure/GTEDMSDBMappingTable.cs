﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace GTEMemberSystem.Models.DBConfigure
{
    public class UserInfoMapping : EntityTypeConfiguration<UserInfo>
    {
        public UserInfoMapping()
        {
            this.ToTable("UserInfo");
            this.HasKey(x => x.UserId);
            this.Property(x => x.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.Account)
                .IsRequired();
            this.Property(x => x.Password)
                .IsRequired();
            this.Property(x => x.UserName)
                .IsRequired();
            this.Property(x => x.Phone)
                .IsRequired();
            this.Property(x => x.Address)
                .IsRequired();
        }
    }

    public class GroupInfoMapping : EntityTypeConfiguration<GroupInfo>
    {
        public GroupInfoMapping()
        {
            this.ToTable("GroupInfo");
            this.HasKey(x => x.GroupId);
            this.Property(x => x.GroupId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.GroupName)
                .IsRequired();
            this.HasOptional(x => x.ParentGroupInfo)
                .WithMany(x => x.ChildGroupInfos)
                .HasForeignKey(x => x.ParentGroupId);
        }
    }

    public class GroupUserInfoMapping : EntityTypeConfiguration<GroupUserInfo>
    {
        public GroupUserInfoMapping()
        {
            this.ToTable("GroupUserInfo");
            //this.HasKey(x => new { x.UserId, x.GroupId });
            this.HasRequired(x => x.UserInfo)
                .WithMany(x => x.GroupUserInfos)
                .HasForeignKey(x => x.UserId);
            this.HasRequired(x => x.GroupInfo)
                .WithMany(x => x.GroupUserInfos)
                .HasForeignKey(x => x.GroupId);
        }
    }

    public class GroupChargeDeviceInfoMapping : EntityTypeConfiguration<GroupChargeDeviceInfo>
    {
        public GroupChargeDeviceInfoMapping()
        {
            this.ToTable("GroupChargeDeviceInfo");
            //this.HasKey(x => new { x.UserId, x.GroupId });
            this.HasRequired(x => x.ChargeDeviceInfo)
                .WithMany(x => x.GroupChargeDeviceInfos)
                .HasForeignKey(x => new { x.DeviceId, x.DeviceTypeId });
            this.HasRequired(x => x.GroupInfo)
                .WithMany(x => x.GroupChargeDeviceInfos)
                .HasForeignKey(x => x.GroupId);
        }
    }

    public class GroupBatteryInfoMapping : EntityTypeConfiguration<GroupBatteryInfo>
    {
        public GroupBatteryInfoMapping()
        {
            this.ToTable("GroupBatteryInfo");
            //this.HasKey(x => new { x.UserId, x.GroupId });
            this.HasRequired(x => x.BatteryInfo)
                .WithMany(x => x.GroupBatteryInfos)
                .HasForeignKey(x => new { x.BatteryId, x.ManufactureDate, x.DeviceTypeId });
            this.HasRequired(x => x.GroupInfo)
                .WithMany(x => x.GroupBatteryInfos)
                .HasForeignKey(x => x.GroupId);
        }
    }

    //public class UserGroupRelationshipMapping : EntityTypeConfiguration<UserGroupRelationship>
    //{
    //    public UserGroupRelationshipMapping()
    //    {
    //        this.ToTable("UserGroupRelationship");
    //        //this.HasKey(x => new { x.UserId, x.GroupId });
    //        this.HasRequired(x => x.UserInfo)
    //            .WithMany(x => x.UserGroupRelationships)
    //            .HasForeignKey(x => x.UserId);
    //        this.HasRequired(x => x.GroupInfo)
    //            .WithMany(x => x.UserGroupRelationships)
    //            .HasForeignKey(x => x.GroupId);
    //    }
    //}

    public class GroupCardInfoMapping : EntityTypeConfiguration<GroupCardInfo>
    {
        public GroupCardInfoMapping()
        {
            this.ToTable("GroupCardInfo");
            this.HasKey(x => x.CardId);
            this.HasRequired(x => x.UserInfo)
                .WithMany(x => x.GroupCardInfos)
                .HasForeignKey(x => x.UserId);
            this.HasRequired(x => x.GroupInfo)
                .WithMany(x => x.GroupCardInfos)
                .HasForeignKey(x => x.GroupId);
        }
    }


    public class ChargeDeviceTypeMapping : EntityTypeConfiguration<ChargeDeviceType>
    {
        public ChargeDeviceTypeMapping()
        {
            this.ToTable("ChargeDeviceType");
            this.HasKey(x => x.DeviceTypeId);
            this.Property(x => x.DeviceTypeId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.DeviceTypeName)
                .IsRequired();
        }
    }

    public class ChargeDeviceInfoMapping : EntityTypeConfiguration<ChargeDeviceInfo>
    {
        public ChargeDeviceInfoMapping()
        {
            this.ToTable("ChargeDeviceInfo");
            this.HasKey(x => new { x.DeviceId, x.DeviceTypeId });
            this.HasRequired(x => x.ChargeDeviceType)
                .WithMany(x => x.ChargeDeviceInfos)
                .HasForeignKey(x => x.DeviceTypeId);
            this.Property(x => x.Password)
                .IsRequired();
            this.Property(x => x.Name)
                .IsRequired();
        }
    }

    public class BatteryTypeMapping : EntityTypeConfiguration<BatteryType>
    {
        public BatteryTypeMapping()
        {
            this.ToTable("BatteryType");
            this.HasKey(x => x.DeviceTypeId);
            this.Property(x => x.DeviceTypeId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.DeviceTypeName)
                .IsRequired();
        }
    }



    public class BatteryInfoMapping : EntityTypeConfiguration<BatteryInfo>
    {
        public BatteryInfoMapping()
        {
            this.Map(x => x.MapInheritedProperties());
            this.ToTable("BatteryInfo");
            this.HasKey(x => new { x.BatteryId, x.ManufactureDate, x.DeviceTypeId });
            //this.Property(x => x.DeviceTypeId);
        }
    }

    public class SlotRecordMapping : EntityTypeConfiguration<SlotRecord>
    {
        public SlotRecordMapping()
        {
            this.ToTable("SlotRecord");
            this.HasKey(x => new { x.TimeStamp, x.SlotId, x.DeviceId, x.DeviceTypeId });
            this.HasOptional(x => x.SlotBatteryRecord)
                .WithMany()
                .HasForeignKey(x => x.SlotBatteryRecordIndex);
        }
    }

    public class SlotBatteryRecordMapping : EntityTypeConfiguration<SlotBatteryRecord>
    {
        public SlotBatteryRecordMapping()
        {
            this.ToTable("SlotBatteryRecord");
            this.HasKey(x => x.SlotBatteryRecordIndex);
            this.Property(x => x.SlotBatteryRecordIndex)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.HasOptional(x => x.SlotRecord)
                .WithMany()
                .HasForeignKey(x => new { x.TimeStamp, x.SlotId, x.DeviceId, x.DeviceTypeId });
        }
    }

    public class TransRecordMapping : EntityTypeConfiguration<TransRecord>
    {
        public TransRecordMapping()
        {
            this.ToTable("TransRecord");
            this.HasKey(x => new { x.TransTimeStamp, x.DeviceId, x.DeviceTypeId, x.TransRecordId });
            this.HasOptional(x => x.InsertedTransDevice)
                .WithMany()
                .HasForeignKey(x => x.InsertedTransDeviceIndex);
            this.HasOptional(x => x.DrawOutTransDevice)
                .WithMany()
                .HasForeignKey(x => x.DrawOutTransDeviceIndex);
        }
    }

    public class TransDeviceRecordMapping : EntityTypeConfiguration<TransDeviceRecord>
    {
        public TransDeviceRecordMapping()
        {
            this.ToTable("TransDeviceRecord");
            this.HasKey(x => x.TransDeviceRecordIndex);
            this.Property(x => x.TransDeviceRecordIndex)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.HasOptional(x => x.TransRecord)
                .WithMany()
                .HasForeignKey(x => new { x.TransTimeStamp, x.DeviceId, x.DeviceTypeId, x.TransRecordId });
        }
    }

    public class ChargeTransRecordMapping : EntityTypeConfiguration<ChargeTransRecord>
    {
        public ChargeTransRecordMapping()
        {
            this.ToTable("ChargeTransRecord");
            this.HasKey(x => new { x.ChargeTransTimeStamp, x.SlotId, x.DeviceId, x.DeviceTypeId });
            this.HasOptional(x => x.StartChargeTransDeviceRecord)
                .WithMany()
                .HasForeignKey(x => x.StartChargeTransDeviceRecordIndex);
            this.HasOptional(x => x.EndChargeTransDeviceRecord)
                .WithMany()
                .HasForeignKey(x => x.EndChargeTransDeviceRecordIndex);
        }
    }

    public class ChargeTransDeviceRecordMapping : EntityTypeConfiguration<ChargeTransDeviceRecord>
    {
        public ChargeTransDeviceRecordMapping()
        {
            this.ToTable("ChargeTransDeviceRecord");
            this.HasKey(x => x.ChargeTransDeviceRecordIndex);
            this.Property(x => x.ChargeTransDeviceRecordIndex)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.HasOptional(x => x.ChargeTransRecord)
                .WithMany()
                .HasForeignKey(x => new { x.ChargeTransTimeStamp, x.SlotId, x.DeviceId, x.DeviceTypeId });
        }
    }

    public class AbnormalRecordMapping : EntityTypeConfiguration<AbnormalRecord>
    {
        public AbnormalRecordMapping()
        {
            this.ToTable("AbnormalRecord");
            this.HasKey(x => new { x.AbnormalTimeStamp, x.SlotId, x.DeviceId, x.DeviceTypeId });
            this.HasOptional(x => x.AbnormalDeviceRecord)
                .WithMany()
                .HasForeignKey(x => x.AbnormalDeviceRecordIndex);
        }
    }

    public class AbnormalDeviceRecordMapping : EntityTypeConfiguration<AbnormalDeviceRecord>
    {
        public AbnormalDeviceRecordMapping()
        {
            this.ToTable("AbnormalDeviceRecord");
            this.HasKey(x => x.AbnormalDeviceRecordIndex);
            this.Property(x => x.AbnormalDeviceRecordIndex)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.HasOptional(x => x.AbnormalRecord)
                .WithMany()
                .HasForeignKey(x => new { x.AbnormalTimeStamp, x.SlotId, x.DeviceId, x.DeviceTypeId });
        }
    }

}