﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GTEMemberSystem.Models.DBConfigure
{
    public class GTEDMSDBContext : DbContext
    {
        public virtual DbSet<UserInfo> UserInfo { get; set; }
        public virtual DbSet<GroupInfo> GroupInfo { get; set; }

        //public virtual DbSet<UserGroupRelationship> UserGroupRelationship { get; set; }
        public virtual DbSet<GroupCardInfo> GroupCardInfo { get; set; }

        public virtual DbSet<GroupUserInfo> GroupUserInfo { get; set; }
        public virtual DbSet<GroupChargeDeviceInfo> GroupChargeDeviceInfo { get; set; }
        public virtual DbSet<GroupBatteryInfo> GroupBatteryInfo { get; set; }

        public virtual DbSet<ChargeDeviceType> ChargeDeviceType { get; set; }
        public virtual DbSet<ChargeDeviceInfo> ChargeDeviceInfo { get; set; }

        public virtual DbSet<BatteryType> BatteryType { get; set; }
        public virtual DbSet<BatteryInfo> BatteryInfo { get; set; }

        public virtual DbSet<SlotRecord> SlotRecord { get; set; }
        public virtual DbSet<SlotBatteryRecord> SlotBatteryRecord { get; set; }

        public virtual DbSet<TransRecord> TransRecord { get; set; }
        public virtual DbSet<TransDeviceRecord> TransDeviceRecord { get; set; }

        public virtual DbSet<ChargeTransRecord> ChargeTransRecord { get; set; }
        public virtual DbSet<ChargeTransDeviceRecord> ChargeTransDeviceRecord { get; set; }

        public virtual DbSet<AbnormalRecord> AbnormalRecord { get; set; }
        public virtual DbSet<AbnormalDeviceRecord> AbnormalDeviceRecord { get; set; }

        public GTEDMSDBContext()
            : base("name=GTEEXNDBContext")
        {
            //this.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserInfoMapping());
            modelBuilder.Configurations.Add(new GroupInfoMapping());

            //modelBuilder.Configurations.Add(new UserGroupRelationshipMapping());
            modelBuilder.Configurations.Add(new GroupUserInfoMapping());
            modelBuilder.Configurations.Add(new GroupChargeDeviceInfoMapping());
            modelBuilder.Configurations.Add(new GroupBatteryInfoMapping());

            modelBuilder.Configurations.Add(new GroupCardInfoMapping());

            modelBuilder.Configurations.Add(new ChargeDeviceTypeMapping());
            modelBuilder.Configurations.Add(new ChargeDeviceInfoMapping());

            modelBuilder.Configurations.Add(new BatteryTypeMapping());
            modelBuilder.Configurations.Add(new BatteryInfoMapping());

            modelBuilder.Configurations.Add(new SlotRecordMapping());
            modelBuilder.Configurations.Add(new SlotBatteryRecordMapping());

            modelBuilder.Configurations.Add(new TransRecordMapping());
            modelBuilder.Configurations.Add(new TransDeviceRecordMapping());

            modelBuilder.Configurations.Add(new ChargeTransRecordMapping());
            modelBuilder.Configurations.Add(new ChargeTransDeviceRecordMapping());

            modelBuilder.Configurations.Add(new AbnormalRecordMapping());
            modelBuilder.Configurations.Add(new AbnormalDeviceRecordMapping());
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}