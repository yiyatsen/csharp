﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GTEMemberSystem.Models
{
    public enum UploadActionCode : int
    {
        DeviceLogin = 1,
        ChargeDeviceSlotRecord = 2,
        TransRecord = 3,
        ChargeTransRecord = 4,
        AbnormalRecord = 5,
        RegisterDevice = 999
    }

    public class DeviceUploadFormat
    {
        //public string StationId { get; set; }
        //public string StationManufactureDate { get; set; }
        //public string DeviceTypeId { get; set; }
        public string DeviceId { get; set; }
        public int DeviceTypeId { get; set; }
        public string TimeStamp { get; set; }
        public UploadActionCode ActionCode { get; set; }
        public string Message { get; set; }
    }

    public class RegisterDeviceFormat
    {
        public string DeviceId { get; set; }
        public string Password { get; set; }
        public int DeviceTypeId { get; set; }
        public string UUID { get; set; }
    }

    public enum ResponseActionCode : int
    {
        UploadSuccess = 0,
        UploadFail = 1,
        ErrorSecurityKey = 2,
        LoginSuccess = 99,
        RegisterFail = 998,
        RegisterSuccess = 999
    }

    public class ServerResponseFormat
    {
        public ResponseActionCode ActionCode { get; set; }
        public string Message { get; set; }
    }

    public class LoginFormat
    {
        public string Password { get; set; }
        //public int DeviceTypeId { get; set; }
    }

    public class LoginResponseFormat
    {
        public string SecurityKey { get; set; }
        public int UpdateCode { get; set; }

    }

    [Table("ChargeDeviceType")]
    public class ChargeDeviceType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DeviceTypeId { get; set; }
        [Required]
        public string DeviceTypeName { get; set; }

        public string Description { get; set; }
        [JsonIgnore]
        public virtual IList<ChargeDeviceInfo> ChargeDeviceInfos { get; set; }

        public ChargeDeviceType()
        {
            ChargeDeviceInfos = new List<ChargeDeviceInfo>();
        }
    }

    [Table("ChargeDeviceInfo")]
    public class ChargeDeviceInfo
    {
        //[Key, Column(Order = 0)]
        //public string StationId { get; set; }
        //[Key, Column(Order = 1)]
        //public string StationManufactureDate { get; set; }
        //[ForeignKey("ChargerType"), Column(Order = 2), Key]
        //public int DeviceTypeId { get; set; }
        //public virtual ChargerType ChargerType { get; set; }
        [Key, Column(Order = 0)]
        public string DeviceId { get; set; }
        [ForeignKey("ChargeDeviceType"), Column(Order = 2), Key]
        public int DeviceTypeId { get; set; }
        public virtual ChargeDeviceType ChargeDeviceType { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Password { get; set; }

        // More Information ...
        public string UUID { get; set; }
        public int BatteryTypeId { get; set; }

        public int OwnerId { get; set; }

        public string LastOnlineIP { get; set; }
        public DateTime LastLoginTime { get; set; }
        public string SecurityKey { get; set; }     
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string OSVersion { get; set; }
        public string APVersion { get; set; }

        public string Address { get; set; }
        public int SlotNumber { get; set; }

        public StatusCode StatusCode { get; set; }

        // Device Register Info
        public DateTime RegisterDateTime { get; set; }

        public string Description { get; set; }
        
        [JsonIgnore]
        public virtual IList<GroupChargeDeviceInfo> GroupChargeDeviceInfos { get; set; }

        public ChargeDeviceInfo()
        {
            GroupChargeDeviceInfos = new List<GroupChargeDeviceInfo>();
        }
    }

    [Table("BatteryType")]
    public class BatteryType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DeviceTypeId { get; set; }
        [Required]
        public string DeviceTypeName { get; set; }

        // type information
        public int DesignVoltage { get; set; }
        public int DesignCapacity { get; set; }

        public int DesignCellsNumber { get; set; }

        public string Description { get; set; }
    }

    [Table("BatteryInfo")]
    public class BatteryInfo
    {
        [Key, Column(Order = 0)]
        public string BatteryId { get; set; }
        [Key, Column(Order = 1)]
        public string ManufactureDate { get; set; }
        [Key, Column(Order = 2)]//[Required]
        public int DeviceTypeId { get; set; }

        // Device Register Info
        //public DeviceOwnerTypeCode DeviceOwnerTypeCode { get; set; }
        public int OwnerId { get; set; }

        public StatusCode StatusCode { get; set; }

        public DateTime RegisterDateTime { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public int SOH { get; set; }
        // Fixed by Neil 2016.04.15
        public int CellVoltDiff { get; set; }
        //
        public int SOC { get; set; }
        public string APVersion { get; set; }
        public string Description { get; set; }

        [JsonIgnore]
        public virtual IList<GroupBatteryInfo> GroupBatteryInfos { get; set; }

        public BatteryInfo()
        {
            GroupBatteryInfos = new List<GroupBatteryInfo>();
        }
    }


    public enum SlotStatusCode
    {
        NonDetectedGate = -1,
        NonBattery = 0,
        DetectedBattery = 1
    }

    public enum TransActionCode : int
    {
        InsertedBattery,
        DrawoutBattery
    }

    public enum TransStatusCode : int
    {
        KioskTransSuccess = 0,
        SmartChargerInsertedSuccess = 1,
        Fail = 2,
        VerifyErrorByKiosk = 3,
        NoCardFail = 4,
        NoInsertedBatteryFail = 5,
        DrawOutBatteryFail = 6,
        VerifyErrorBySmartCharger = 7,
        SmartChargerDrawout = 8
    }

    public enum ChargeTransStatusCode : int
    {
        Charge = 0,
        Finish = 1,
        AbnormalEnd = 2
    }

    public enum AbnormalStatusCode
    {
        NonDetectedBatteryInfo = 0,
        AbnormalBatteryCharge = 1,
        SlotFull = 2,
        SlotEmpty = 3
    }

    [Table("SlotRecord")]
    public class SlotRecord
    {
        [Key, Column(Order = 0)]
        public DateTime TimeStamp { get; set; }
        [Key, Column(Order = 1)]
        public int SlotId { get; set; }
        //[Key, Column(Order = 2)]
        //public string StationId { get; set; }
        //[Key, Column(Order = 3)]
        //public string StationManufactureDate { get; set; }
        //[Key, Column(Order = 4)]
        //public int DeviceTypeId { get; set; }
        [Key, Column(Order = 2)]
        public string DeviceId { get; set; }
        [Key, Column(Order = 3)]
        public int DeviceTypeId { get; set; }

        public long SlotRecordId { get; set; }

        public SlotStatusCode SlotStatusCode { get; set; }

        public long? SlotBatteryRecordIndex { get; set; }

        public long SlotBatteryRecordId { get; set; }
        public virtual SlotBatteryRecord SlotBatteryRecord { get; set; }

        public string Description { get; set; }
    }

    [Table("SlotBatteryRecord")]
    public class SlotBatteryRecord
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SlotBatteryRecordIndex { get; set; }

        [ForeignKey("SlotRecord"), Column(Order = 1)]
        public DateTime TimeStamp { get; set; }
        [ForeignKey("SlotRecord"), Column(Order = 2)]
        public int SlotId { get; set; }
        //[ForeignKey("SlotRecord"), Column(Order = 3)]
        //public string StationId { get; set; }
        //[ForeignKey("SlotRecord"), Column(Order = 4)]
        //public string StationManufactureDate { get; set; }
        //[ForeignKey("SlotRecord"), Column(Order = 5)]
        //public int DeviceTypeId { get; set; }
        [ForeignKey("SlotRecord"), Column(Order = 3)]
        public string DeviceId { get; set; }
        [ForeignKey("SlotRecord"), Column(Order = 4)]
        public int DeviceTypeId { get; set; }

        [JsonIgnore]
        public virtual SlotRecord SlotRecord { get; set; }

        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public int BatteryTypeId { get; set; }

        public long SlotBatteryRecordId { get; set; }

        public int SOC { get; set; }
        public long RC { get; set; }
        public double Temperature { get; set; }
        public int Current { get; set; }
        public long Voltage { get; set; }
        public int VMax { get; set; }
        public int VMin { get; set; }
        public int Protection { get; set; }

        public string Description { get; set; }
    }

    [Table("TransRecord")]
    public class TransRecord
    {
        [Key, Column(Order = 0)]
        public DateTime TransTimeStamp { get; set; }
        //[Key, Column(Order = 1)]
        //public string StationId { get; set; }
        //[Key, Column(Order = 2)]
        //public string StationManufactureDate { get; set; }
        //[Key, Column(Order = 3)]
        //public int DeviceTypeId { get; set; }
        //[Key, Column(Order = 4)]
        //public long TransRecordId { get; set; }
        [Key, Column(Order = 1)]
        public string DeviceId { get; set; }
        [Key, Column(Order = 2)]
        public int DeviceTypeId { get; set; }
        [Key, Column(Order = 3)]
        public long TransRecordId { get; set; }

        public string UserId { get; set; }
        public string CardId { get; set; }
        public TransStatusCode TransStatusCode { get; set; }

        public int OldPoint { get; set; }
        public int Point { get; set; }
        public long? InsertedTransDeviceIndex { get; set; }
        public long InsertedTransDeviceId { get; set; }
        public virtual TransDeviceRecord InsertedTransDevice { get; set; }
        public long? DrawOutTransDeviceIndex { get; set; }
        public long DrawOutTransDeviceId { get; set; }
        public virtual TransDeviceRecord DrawOutTransDevice { get; set; }

        public string Description { get; set; }
    }
    [Table("TransDeviceRecord")]
    public class TransDeviceRecord
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long TransDeviceRecordIndex { get; set; }
        public long TransDeviceRecordId { get; set; }

        [ForeignKey("TransRecord"), Column(Order = 1)]
        public DateTime TransTimeStamp { get; set; }
        //[ForeignKey("TransRecord"), Column(Order = 2)]
        //public string StationId { get; set; }
        //[ForeignKey("TransRecord"), Column(Order = 3)]
        //public string StationManufactureDate { get; set; }
        //[ForeignKey("TransRecord"), Column(Order = 4)]
        //public int DeviceTypeId { get; set; }
        //[ForeignKey("TransRecord"), Column(Order = 5)]
        //public long TransRecordId { get; set; }
        [ForeignKey("TransRecord"), Column(Order = 2)]
        public string DeviceId { get; set; }
        [ForeignKey("TransRecord"), Column(Order = 3)]
        public int DeviceTypeId { get; set; }
        [ForeignKey("TransRecord"), Column(Order = 4)]
        public long TransRecordId { get; set; }

        [JsonIgnore]
        public virtual TransRecord TransRecord { get; set; }

        //public TransActionCode TransActionCode { get; set; }

        public DateTime TransDeviceTime { get; set; }
        public int SlotId { get; set; }
        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public int BatteryTypeId { get; set; }

        public string OID { get; set; } // Group Id
        public string UID { get; set; } // User Id
        public string CID { get; set; } // Card Id

        public int SOC { get; set; }
        public long CycleCount { get; set; }
        public string CellsVoltage { get; set; }
        public int Protection { get; set; }

        public string Description { get; set; }
    }

    [Table("ChargeTransRecord")]
    public class ChargeTransRecord
    {
        [Key, Column(Order = 0)]
        public DateTime ChargeTransTimeStamp { get; set; }
        [Key, Column(Order = 1)]
        public int SlotId { get; set; }
        //[Key, Column(Order = 2)]
        //public string StationId { get; set; }
        //[Key, Column(Order = 3)]
        //public string StationManufactureDate { get; set; }
        //[Key, Column(Order = 4)]
        //public int DeviceTypeId { get; set; }
        [Key, Column(Order = 2)]
        public string DeviceId { get; set; }
        [Key, Column(Order = 3)]
        public int DeviceTypeId { get; set; }

        public ChargeTransStatusCode ChargeTransStatusCode { get; set; }

        public long ChargeTransRecordId { get; set; }

        public int BatteryTypeId { get; set; }

        public long ChargedCapacity { get; set; }

        public long TotalTime { get; set; }

        public long? StartChargeTransDeviceRecordIndex { get; set; }
        public virtual ChargeTransDeviceRecord StartChargeTransDeviceRecord { get; set; }
        public long? EndChargeTransDeviceRecordIndex { get; set; }
        public virtual ChargeTransDeviceRecord EndChargeTransDeviceRecord { get; set; }

        public string Description { get; set; }
    }

    [Table("ChargeTransDeviceRecord")]
    public class ChargeTransDeviceRecord
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ChargeTransDeviceRecordIndex { get; set; }
        public long ChargeTransDeviceRecordId { get; set; }

        [ForeignKey("ChargeTransRecord"), Column(Order = 1)]
        public DateTime ChargeTransTimeStamp { get; set; }
        [ForeignKey("ChargeTransRecord"), Column(Order = 2)]
        public int SlotId { get; set; }
        //[ForeignKey("ChargeTransRecord"), Column(Order = 3)]
        //public string StationId { get; set; }
        //[ForeignKey("ChargeTransRecord"), Column(Order = 4)]
        //public string StationManufactureDate { get; set; }
        //[ForeignKey("ChargeTransRecord"), Column(Order = 5)]
        //public int DeviceTypeId { get; set; }
        [ForeignKey("ChargeTransRecord"), Column(Order = 3)]
        public string DeviceId { get; set; }
        [ForeignKey("ChargeTransRecord"), Column(Order = 4)]
        public int DeviceTypeId { get; set; }

        [JsonIgnore]
        public virtual ChargeTransRecord ChargeTransRecord { get; set; }

        public DateTime ChargeTimeStamp { get; set; }
        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public int BatteryTypeId { get; set; }

        public int SOC { get; set; }
        public long FCC { get; set; }
        public long RC { get; set; }
        public string Impedance { get; set; }
        public long Voltage { get; set; }
        public string CellsVoltage { get; set; }
        public string CellsTemperature { get; set; }
        public long CycleCount { get; set; }
        public int Protection { get; set; }

        public string Description { get; set; }
        // Fixed by Neil 2016.04.15
        public int SOH { get; set; }
        public int CellVoltDiff { get; set; }
        //
    }

    [Table("AbnormalRecord")]
    public class AbnormalRecord
    {
        [Key, Column(Order = 0)]
        public DateTime AbnormalTimeStamp { get; set; }
        [Key, Column(Order = 1)]
        public int SlotId { get; set; }
        //[Key, Column(Order = 2)]
        //public string StationId { get; set; }
        //[Key, Column(Order = 3)]
        //public string StationManufactureDate { get; set; }
        //[Key, Column(Order = 4)]
        //public int DeviceTypeId { get; set; }
        [Key, Column(Order = 2)]
        public string DeviceId { get; set; }
        [Key, Column(Order = 3)]
        public int DeviceTypeId { get; set; }

        public long AbnormalRecordId { get; set; }
        public AbnormalStatusCode AbnormalStatusCode { get; set; }

        public long? AbnormalDeviceRecordIndex { get; set; }
        public virtual AbnormalDeviceRecord AbnormalDeviceRecord { get; set; }

        public int IsNotification { get; set; }
    }

    [Table("AbnormalDeviceRecord")]
    public class AbnormalDeviceRecord
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long AbnormalDeviceRecordIndex { get; set; }
        public long AbnormalDeviceRecordId { get; set; }

        [ForeignKey("AbnormalRecord"), Column(Order = 1)]
        public DateTime AbnormalTimeStamp { get; set; }
        [ForeignKey("AbnormalRecord"), Column(Order = 2)]
        public int SlotId { get; set; }
        //[ForeignKey("AbnormalRecord"), Column(Order = 3)]
        //public string StationId { get; set; }
        //[ForeignKey("AbnormalRecord"), Column(Order = 4)]
        //public string StationManufactureDate { get; set; }
        //[ForeignKey("AbnormalRecord"), Column(Order = 5)]
        //public int DeviceTypeId { get; set; }
        [ForeignKey("AbnormalRecord"), Column(Order = 3)]
        public string DeviceId { get; set; }
        [ForeignKey("AbnormalRecord"), Column(Order = 4)]
        public int DeviceTypeId { get; set; }
        [JsonIgnore]
        public virtual AbnormalRecord AbnormalRecord { get; set; }

        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public int BatteryTypeId { get; set; }

        public int SOC { get; set; }
        public long RC { get; set; }
        public long FCC { get; set; }
        public string Impedance { get; set; }
        public string CellsVoltage { get; set; }
        public string CellsTemperature { get; set; }
        public long CycleCount { get; set; }

        public int Protection { get; set; }
    }

}