﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GTEMemberSystem.Models
{
    /// <summary>
    /// Defined User Role
    /// </summary>
    public enum UserRoleCode : int
    {
        Remove = -2,
        Ban = -1,
        NonActivity = 0,
        User = 1,
        Manager = 2,
        Cooperator = 4, // 營運商
        Administrator = 16 // => Only one, System administrator for GoTech
    }

    public enum RelationRoleCode : int
    {
        Remove = -2,
        Ban = -1,
        NonActivity = 0,
        User = 1,
        Manager = 2
    }

    public enum StatusCode
    {
        Remove = -2,
        Ban = -1,
        NonActivity = 0,
        Useable = 1
    }

    public enum DeviceOwnerTypeCode : int
    {
        Administrator = 0,
        Group = 1,
        User = 2
    }

    [Table("GroupChargeDeviceInfo")]
    public class GroupChargeDeviceInfo
    {
        [ForeignKey("GroupInfo"), Column(Order = 0), Key]
        public int GroupId { get; set; }
        [JsonIgnore]
        public virtual GroupInfo GroupInfo { get; set; }

        [ForeignKey("ChargeDeviceInfo"), Column(Order = 1), Key]
        public string DeviceId { get; set; }
        [ForeignKey("ChargeDeviceInfo"), Column(Order = 2), Key]
        public int DeviceTypeId { get; set; } 
        [JsonIgnore]
        public virtual ChargeDeviceInfo ChargeDeviceInfo { get; set; }

        public string Description { get; set; }

        [DefaultValue(StatusCode.Useable)]
        public StatusCode StatusCode { get; set; }
    }

    [Table("GroupBatteryInfo")]
    public class GroupBatteryInfo
    {
        [ForeignKey("GroupInfo"), Column(Order = 0), Key]
        public int GroupId { get; set; }
        [JsonIgnore]
        public virtual GroupInfo GroupInfo { get; set; }

        [ForeignKey("BatteryInfo"), Column(Order = 1), Key]
        public string BatteryId { get; set; }
        [ForeignKey("BatteryInfo"), Column(Order = 2), Key]
        public string ManufactureDate { get; set; }
        [ForeignKey("BatteryInfo"), Column(Order = 3), Key]
        public int DeviceTypeId { get; set; }
        [JsonIgnore]
        public virtual BatteryInfo BatteryInfo { get; set; }

        public string Description { get; set; }

        [DefaultValue(StatusCode.Useable)]
        public StatusCode StatusCode { get; set; }
    }

    [Table("GroupUserInfo")]
    public class GroupUserInfo
    {
        [ForeignKey("GroupInfo"), Column(Order = 0), Key]
        public int GroupId { get; set; }
        [JsonIgnore]
        public virtual GroupInfo GroupInfo { get; set; }
        [ForeignKey("UserInfo"), Column(Order = 1), Key]
        public int UserId { get; set; }
        [JsonIgnore]
        public virtual UserInfo UserInfo { get; set; }

        public string Description { get; set; }

        [DefaultValue(RelationRoleCode.User)]
        public RelationRoleCode RelationRoleCode { get; set; }
    }

    [Table("GroupInfo")]
    public class GroupInfo
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GroupId { get; set; }
        [Required]
        public string GroupName { get; set; }

        public int? ParentGroupId { get; set; }
        [JsonIgnore]
        public GroupInfo ParentGroupInfo { get; set; }

        public DateTime RegisterDateTime { get; set; }
        public string Description { get; set; }

        public int OwnerId { get; set; }

        public StatusCode StatusCode { get; set; }

        [JsonIgnore]
        public virtual IList<GroupInfo> ChildGroupInfos { get; set; }
        [JsonIgnore]
        public virtual IList<GroupCardInfo> GroupCardInfos { get; set; }
        [JsonIgnore]
        public virtual IList<GroupChargeDeviceInfo> GroupChargeDeviceInfos { get; set; }
        [JsonIgnore]
        public virtual IList<GroupBatteryInfo> GroupBatteryInfos { get; set; }
        [JsonIgnore]
        public virtual IList<GroupUserInfo> GroupUserInfos { get; set; }
        //[JsonIgnore]
        //public virtual IList<UserGroupRelationship> UserGroupRelationships { get; set; }

        public GroupInfo()
        {
            ChildGroupInfos = new List<GroupInfo>();
            GroupCardInfos = new List<GroupCardInfo>();
            GroupChargeDeviceInfos = new List<GroupChargeDeviceInfo>();
            GroupBatteryInfos = new List<GroupBatteryInfo>();
            GroupUserInfos = new List<GroupUserInfo>();
            //UserGroupRelationships = new List<UserGroupRelationship>();
        }
    }

    [Table("UserInfo")]
    public class UserInfo
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        [Required]
        public string Account { get; set; } // = Email
        [Required, EmailAddress]
        public string Email { get; set; } // = Email
        [Required]
        public string Password { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Address { get; set; }
        [DefaultValue(UserRoleCode.NonActivity)]
        public UserRoleCode UserRoleCode { get; set; }

        public int DefaultGroup { get; set; }
        public string Description { get; set; }

        public DateTime RegisterDateTime { get; set; }

        public int RefId { get; set; }

        [JsonIgnore]
        public virtual IList<GroupCardInfo> GroupCardInfos { get; set; }
        [JsonIgnore]
        public virtual IList<GroupUserInfo> GroupUserInfos { get; set; }

        public UserInfo()
        {
            GroupCardInfos = new List<GroupCardInfo>();
            GroupUserInfos = new List<GroupUserInfo>();
        }
    }

    [Table("GroupCardInfo")]
    public class GroupCardInfo
    {
        [Key, Column(Order = 0)]
        public string CardId { get; set; }
        [ForeignKey("UserInfo"), Column(Order = 1)]
        public int UserId { get; set; }
        [JsonIgnore]
        public virtual UserInfo UserInfo { get; set; }
        [ForeignKey("GroupInfo"), Column(Order = 2)]
        public int GroupId { get; set; }
        [JsonIgnore]
        public virtual GroupInfo GroupInfo { get; set; }
        [DefaultValue(StatusCode.NonActivity)]
        public StatusCode StatusCode { get; set; } // -1: ban, 0: non-Active, 1: normal, 2: manager
        [DefaultValue(DeviceOwnerTypeCode.Administrator)]
        public DeviceOwnerTypeCode DeviceOwnerTypeCode { get; set; }

        public DateTime RegisterDateTime { get; set; }
        public string Description { get; set; }
    }

    [Table("MarkerInfo")]
    public class MarkerInfo
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MarkerId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string IconPath { get; set; }
        [Required, DefaultValue(48)]
        public int IconWidth { get; set; }
        [Required, DefaultValue(48)]
        public int IconHeight { get; set; }
    }
}