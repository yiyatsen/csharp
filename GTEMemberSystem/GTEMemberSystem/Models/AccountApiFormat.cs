﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GTEMemberSystem.Models
{
    public enum LoginStatusCode
    {
        None = 0,
        Success = 1,
        ModelValidFail = 2,
        AccountPasswordFail = 3,
        CaptchaFail = 4
    }

    public enum TimeFormat : int
    {
        Day = 1,
        Month = 2,
        Year = 3
    }

    public enum StyleFormat : int
    {
        All = 1,
        ChargerClass = 2,
        BatteryClass = 3
    }

    public class UserLoginInfo
    {
        [Required]
        public string account { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public string captcha { get; set; }
    }

    public class CodeFormat
    {
        public string Name { get; set; }
        public int Code { get; set; }
    }

    public class CurrentChargeDeviceInfo
    {
        public string DeviceId { get; set; }
        //public string StationManufactureDate { get; set; }
        public int DeviceTypeId { get; set; } //carrier

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

        public int SlotNumber { get; set; }

        public StatusCode StatusCode { get; set; }

        // Device Register Info
        public string OSVersion { get; set; }
        public string APVersion { get; set; }

        public string Description { get; set; }
    }

    public class CurrentChargeDeviceSlotInfo
    {
        public DateTime TimeStamp { get; set; }
        public int SlotId { get; set; }
        public string DeviceId { get; set; }
        //public string StationManufactureDate { get; set; }
        public int DeviceTypeId { get; set; }

        public SlotStatusCode SlotStatusCode { get; set; }

        public virtual SlotBatteryRecord SlotBatteryRecord { get; set; }

        public string Description { get; set; }
    }

    public class AccountRequest
    {
        public string Account { get; set; }
        public string Password { get; set; }
    }

    public class SlotInfoRequestByChargeDevice : AccountRequest
    {
        public string DeviceId { get; set; }
        //public string StationManufactureDate { get; set; }
        public int DeviceTypeId { get; set; }
    }


    public class WebUserRequest 
    {
        public string SecurityKey { get; set; }
    }

    public class BatteryTypeRequest : WebUserRequest
    {
        public int BatteryTypeCode { get; set; }
        public string BatteryStyleLabel { get; set; }
    }

    public class TimeRequest : WebUserRequest
    {
        public TimeFormat TimeFormat { get; set; }
    }

    public class TimeStyleRequest : TimeRequest
    {
        public StyleFormat StyleFormat { get; set; }
    }

    public class ChartData
    {
        public string labels { get; set; }
        public int datas { get; set; }
        public string series { get; set; }
    }

    public class ChargeDeviceTimeRequest : TimeRequest
    {
        public string DeviceId { get; set; }
        //public string StationManufactureDate { get; set; }
        public int DeviceTypeId { get; set; }
    }

    public class ChargeDeviceBatteryTimeRequest : TimeRequest
    {
        public string DeviceId { get; set; }
        //public string StationManufactureDate { get; set; }
        public int DeviceTypeId { get; set; }

        public string BatteryId { get; set; }
        public string BatteryManufactureDate { get; set; }
    }

    public class DataRequest : WebUserRequest
    {
        public int DataFormat { get; set; }
    }

    public class AccountInfo : WebUserRequest
    {
        public string Account { get; set; }
        public string UserName { get; set; }
        [DefaultValue(UserRoleCode.NonActivity)]
        public UserRoleCode UserRoleCode { get; set; }
        public string UserRoleName { get; set; }

        public LoginStatusCode LoginStatusCode { get; set; }
    }

    public class UserProfile : WebUserRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
    }

    public class UserInfoModel : WebUserRequest
    {
        public int UserId { get; set; }

        public string Account { get; set; } // = Email
        public string Email { get; set; } // = Email
        public string Password { get; set; }
        public string UserName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public UserRoleCode UserRoleCode { get; set; }
        public string Description { get; set; }  
        public DateTime RegisterDateTime { get; set; }
    }

    public class UserGroupRelationInfoModel : WebUserRequest
    {
        public int UserId { get; set; }

        public List<GroupRelationInfo> GroupRelationInfos { get; set; } 
    }

    public class ChargeDeviceGroupRelationInfoModel : UserGroupRelationInfoModel
    {
        public string DeviceId { get; set; }
        //public string StationManufactureDate { get; set; }
        public int DeviceTypeId { get; set; }
    }

    public class BatteryGroupRelationInfoModel : UserGroupRelationInfoModel
    {
        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public int DeviceTypeId { get; set; }
    }

    public class GroupRelationInfo : WebUserRequest
    {
        public bool Enable { get; set; }

        public int GroupId { get; set; }

        public RelationRoleCode RelationRoleCode { get; set; }
    }

    public class GroupInfoModel : WebUserRequest
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }

        public int? ParentGroupId { get; set; }

        public DateTime RegisterDateTime { get; set; }
        public string Description { get; set; }

        public StatusCode StatusCode { get; set; }

        public RelationRoleCode RelationRoleCode { get; set; }
    }

    public class GroupCardInfoModel : WebUserRequest
    {
        public string CardId { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public DateTime RegisterDateTime { get; set; }
        public string Description { get; set; }
        public StatusCode StatusCode { get; set; }
        public DeviceOwnerTypeCode DeviceOwnerTypeCode { get; set; }
    }

    public class ChargeDeviceInfoModel : WebUserRequest
    {
        //public string StationId { get; set; }
        //public string StationManufactureDate { get; set; }
        public string DeviceId { get; set; }
        public int DeviceTypeId { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string DeviceMac { get; set; }
        public string LastOnlineIP { get; set; }
        public bool IsOnline { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Address { get; set; }
        public int SlotNumber { get; set; }

        // Device Register Info
        public DateTime LastOnlineTime { get; set; }
        public DateTime RegisterDateTime { get; set; }
        public string OSVersion { get; set; }
        public string APVersion { get; set; }
        public string Description { get; set; }
        public int BatteryTypeId { get; set; }
        public StatusCode StatusCode { get; set; }
        public int OwnerId { get; set; }
    }

    public class BatteryInfoModel : WebUserRequest
    {
        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public int DeviceTypeId { get; set; }


        public DateTime RegisterDateTime { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public string APVersion { get; set; }
        public string Description { get; set; }

        public StatusCode StatusCode { get; set; }
        public int OwnerId { get; set; }
    }

    public class BatteryCurrentInfoModel : WebUserRequest
    {
        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public int DeviceTypeId { get; set; }

        public DateTime LastUpdateTime { get; set; }
        public int SOC { get; set; }
        public int SOH { get; set; }
        public int CellVoltDiff { get; set; }
    }
    //public class BatteryDetailModel : BatteryInfoModel
    //{
    //    public int StartIndex { get; set; }
    //    public int EndIndex { get; set; }
    //}
}