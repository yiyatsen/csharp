﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GTEMemberSystem.Controllers
{
    public class PageController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult UserProfile()
        {
            return View();
        }

        public ActionResult Main()
        {
            return View();
        }

        public ActionResult MainHeader()
        {
            return View();
        }

        public ActionResult MainHeaderNotification()
        {
            return View();
        }

        public ActionResult MainSidebar()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        public ActionResult DashboardSummary()
        {
            return View();
        }

        public ActionResult DashboardCharger()
        {
            return View();
        }

        public ActionResult DashboardBattery()
        {
            return View();
        }

        public ActionResult AdministratorCharger()
        {
            return View();
        }

        public ActionResult AdministratorBattery()
        {
            return View();
        }

        public ActionResult Administrator()
        {
            return View();
        }

        public ActionResult AdministratorGroup()
        {
            return View();
        }

        public ActionResult AdministratorUser()
        {
            return View();
        }
    }
}
