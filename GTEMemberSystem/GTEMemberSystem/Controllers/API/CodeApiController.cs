﻿using GTEMemberSystem.Models;
using GTEMemberSystem.Models.DBConfigure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GTEMemberSystem.Controllers.API
{
    public class CodeApiController : ApiController
    {
        GTEDMSDBContext db = new GTEDMSDBContext();

        [HttpGet]
        public IList<MarkerInfo> GetMapIconList()
        {
            List<MarkerInfo> list = new List<MarkerInfo>();

            list.Add(new MarkerInfo()
            {
                Name = "UserLocation",
                IconPath = "/Content/image/Here.png",
                IconWidth = 48,
                IconHeight = 48
            });

            list.Add(new MarkerInfo()
            {
                Name = "Charger",
                IconPath = "/Content/image/EXN.png",
                IconWidth = 64,
                IconHeight = 64
            });

            return list;
        }

        [HttpGet]
        public List<CodeFormat> GetTimeFormatList()
        {
            List<CodeFormat> result = new List<CodeFormat>();

            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                foreach (TimeFormat data in Enum.GetValues(typeof(TimeFormat)).Cast<TimeFormat>())
                {
                    result.Add(new CodeFormat()
                    {
                        Name = data.ToString(),
                        Code = (int)data
                    });
                }
            }

            return result;
        }


        [HttpGet]
        public List<CodeFormat> GetUserRoleList()
        {
            List<CodeFormat> result = new List<CodeFormat>();

            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                foreach (UserRoleCode data in Enum.GetValues(typeof(UserRoleCode)).Cast<UserRoleCode>())
                {
                    if (userInfo.UserRoleCode != UserRoleCode.Administrator)
                    {
                        if (data != UserRoleCode.Administrator)
                        {
                            result.Add(new CodeFormat()
                            {
                                Name = data.ToString(),
                                Code = (int)data
                            });
                        }
                    }
                    else
                    {
                        result.Add(new CodeFormat()
                        {
                            Name = data.ToString(),
                            Code = (int)data
                        });
                    }
                }
            }

            return result;
        }

        [HttpGet]
        public List<CodeFormat> GetStatusList()
        {
            List<CodeFormat> result = new List<CodeFormat>();

            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                foreach (StatusCode data in Enum.GetValues(typeof(StatusCode)).Cast<StatusCode>())
                {
                    result.Add(new CodeFormat(){
                        Name = data.ToString(),
                        Code = (int)data
                    });
                }
            }

            return result;
        }

        [HttpGet]
        public List<CodeFormat> GetDeviceOwnerTypeList()
        {
            List<CodeFormat> result = new List<CodeFormat>();

            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                foreach (DeviceOwnerTypeCode data in Enum.GetValues(typeof(DeviceOwnerTypeCode)).Cast<DeviceOwnerTypeCode>())
                {
                    result.Add(new CodeFormat()
                    {
                        Name = data.ToString(),
                        Code = (int)data
                    });
                }
            }

            return result;
        }

        [HttpGet]
        public List<CodeFormat> GetRelationRoleList()
        {
            List<CodeFormat> result = new List<CodeFormat>();

            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                foreach (RelationRoleCode data in Enum.GetValues(typeof(RelationRoleCode)).Cast<RelationRoleCode>())
                {
                    result.Add(new CodeFormat()
                    {
                        Name = data.ToString(),
                        Code = (int)data
                    });
                }
            }

            return result;
        }

        [HttpGet]
        public List<CodeFormat> GetChargeDeviceTypeList()
        {
            return (from ct in db.ChargeDeviceType
             select new CodeFormat()
             {
                 Name = ct.DeviceTypeName,
                 Code = ct.DeviceTypeId
             }).ToList();
        }

        [HttpGet]
        public List<CodeFormat> GetBatteryTypeList()
        {
            List<CodeFormat> result =(from ct in db.BatteryType
             select new CodeFormat()
             {
                 Name = ct.DeviceTypeName,
                 Code = ct.DeviceTypeId
             }).ToList();

            result.Insert(0, (new CodeFormat()
             {
                 Name = "All",
                 Code = 0
             }));
            return result;
        }
    }
}
