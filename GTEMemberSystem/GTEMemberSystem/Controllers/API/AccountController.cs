﻿using GTEMemberSystem.Models;
using GTEMemberSystem.Models.DBConfigure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GTEMemberSystem.Controllers.API
{
    public class AccountController : ApiController
    {
        GTEDMSDBContext db = new GTEDMSDBContext();

        [HttpPost]
        public AccountInfo Login(UserLoginInfo userLoginInfo)
        {
            AccountInfo value = new AccountInfo();

            if (this.ModelState.IsValid)
            {
                var session = System.Web.HttpContext.Current.Session;
                session["UserInfo"] = null;
                session["SecurityKey"] = null;
                if (session["Captcha"] != null && userLoginInfo.captcha.Equals(session["Captcha"].ToString()))
                {
                    /// 1. 帳密正確且尚未登入紀錄 -> 成功登入
                    /// 2. 帳密正確已經有登入紀錄 -> 此帳號已經登入
                    /// 3. 帳密不正確 -> 帳密輸入錯誤
                    var account = (from a in db.UserInfo
                                   where a.Account.Equals(userLoginInfo.account) && a.Password.Equals(userLoginInfo.password)
                                   select a).SingleOrDefault();

                    if (account != null)
                    {
                        value.Account = account.Account;
                        value.UserName = account.UserName;
                        value.SecurityKey = System.Guid.NewGuid().ToString();
                        value.UserRoleCode = account.UserRoleCode;
                        value.UserRoleName = account.UserRoleCode.ToString();
                        value.LoginStatusCode = LoginStatusCode.Success;

                        session["UserInfo"] = account;
                        session["SecurityKey"] = value.SecurityKey;
                        session["Captcha"] = null; // Rmove Session Data
                    }
                    else
                    {
                        value.Account = null;
                        value.UserName = null;
                        value.SecurityKey = null;
                        value.UserRoleCode = UserRoleCode.NonActivity;
                        value.UserRoleName = UserRoleCode.NonActivity.ToString();
                        value.LoginStatusCode = LoginStatusCode.AccountPasswordFail;
                    }
                }
                else
                {
                    value.Account = null;
                    value.UserName = null;
                    value.SecurityKey = null;
                    value.UserRoleCode = UserRoleCode.NonActivity;
                    value.UserRoleName = UserRoleCode.NonActivity.ToString();
                    value.LoginStatusCode = LoginStatusCode.CaptchaFail;
                }
            }
            else
            {
                value.Account = null;
                value.UserName = null;
                value.SecurityKey = null;
                value.UserRoleCode = UserRoleCode.NonActivity;
                value.UserRoleName = UserRoleCode.NonActivity.ToString();
                value.LoginStatusCode = LoginStatusCode.ModelValidFail;
            }

            return value;
        }

        [HttpPost]
        public AccountInfo Logout(AccountInfo value)
        {                       
            var session = System.Web.HttpContext.Current.Session;
            session["UserInfo"] = null;
            session["SecurityKey"] = null;

            value.Account = null;
            value.UserName = null;
            value.SecurityKey = null;
            value.UserRoleCode = UserRoleCode.NonActivity;
            value.UserRoleName = UserRoleCode.NonActivity.ToString();
            value.LoginStatusCode = LoginStatusCode.Success;

            return value;
        }

        [HttpGet]
        public HttpResponseMessage RefreshCaptcha()
        {
            var rand = new Random((int)DateTime.Now.Ticks);
            //generate new question 
            int a = rand.Next(10, 99);
            int b = rand.Next(0, 9);
            var captcha = string.Format("{0} + {1} = ?", a, b);

            var session = System.Web.HttpContext.Current.Session;
            session["Captcha"] = (a + b).ToString();

            byte[] fileData = null;

            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(130, 30))
            using (var gfx = Graphics.FromImage((Image)bmp))
            {
                gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));

                //add question 
                gfx.DrawString(captcha, new Font("Tahoma", 15), Brushes.Gray, 2, 3);

                //render as Jpeg 
                bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
                fileData = mem.GetBuffer();
            }

            HttpResponseMessage Response = new HttpResponseMessage(HttpStatusCode.OK);

            if (fileData == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            //S3:Set Response contents and MediaTypeHeaderValue
            Response.Content = new ByteArrayContent(fileData);
            Response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/jpeg");

            return Response;
        }
    }
}
