﻿using GTEMemberSystem.Models;
using GTEMemberSystem.Models.DBConfigure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GTEMemberSystem.Controllers.API
{
    public class DashboardApiController : ApiController
    {
        GTEDMSDBContext db = new GTEDMSDBContext();

        [HttpPost]
        public object GetTransactionTimeInfo(ChargeDeviceTimeRequest request)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                var query = from tr in db.TransRecord
                            where tr.TransStatusCode <= TransStatusCode.SmartChargerInsertedSuccess
                            select new { TimeStamp = tr.TransTimeStamp };
                if (userInfo.UserRoleCode != UserRoleCode.Administrator)
                {
                    var query2 = (from bb in db.GroupChargeDeviceInfo
                                  join aa in
                                      (from gui in db.GroupUserInfo
                                       where gui.UserId == userInfo.UserId && gui.RelationRoleCode == RelationRoleCode.Manager
                                       select new { GroupId = gui.GroupId }) on bb.GroupId equals aa.GroupId
                                  select new { DeviceId = bb.DeviceId, DeviceTypeId = bb.DeviceTypeId });

                    if (request.DeviceId != null && request.DeviceTypeId > 0)
                    {
                        query = from tr in db.TransRecord
                                join q in query2 on new { tr.DeviceId, tr.DeviceTypeId } equals new { q.DeviceId, q.DeviceTypeId }
                                where tr.TransStatusCode <= TransStatusCode.SmartChargerInsertedSuccess && (tr.DeviceId == request.DeviceId && tr.DeviceTypeId == request.DeviceTypeId)
                                select new { TimeStamp = tr.TransTimeStamp };
                    }
                    else
                    {
                        query = from tr in db.TransRecord
                                join q in query2 on new { tr.DeviceId, tr.DeviceTypeId } equals new { q.DeviceId, q.DeviceTypeId }
                                where tr.TransStatusCode <= TransStatusCode.SmartChargerInsertedSuccess
                                select new { TimeStamp = tr.TransTimeStamp };
                    }
                }
                else
                {
                    if (request.DeviceId != null && request.DeviceTypeId > 0)
                    {
                        query = from tr in db.TransRecord
                                where tr.TransStatusCode <= TransStatusCode.SmartChargerInsertedSuccess && (tr.DeviceId == request.DeviceId && tr.DeviceTypeId == request.DeviceTypeId)
                                select new { TimeStamp = tr.TransTimeStamp };
                    }
                }


                var now = DateTime.UtcNow;

                switch (request.TimeFormat)
                {
                    case TimeFormat.Year:
                        {
                            var dt = Enumerable.Range(-2, 3)
                                .Select(x => new
                                {
                                    year = now.AddYears(x).Year
                                });

                            return dt.GroupJoin(query,
                                    m => new { year = m.year },
                                    revision => new
                                    {
                                        year = revision.TimeStamp.Year
                                    },
                                    (p, g) => new
                                    {
                                        labels = p.year.ToString(),
                                        datas = g.Count(),
                                        series = "All"
                                    }).ToList();
                        }
                    case TimeFormat.Month:
                        {
                            var dt = Enumerable.Range(-12, 13)
                                .Select(x => new
                                {
                                    year = now.AddMonths(x).Year,
                                    month = now.AddMonths(x).Month
                                });

                            return dt.GroupJoin(query,
                                            m => new { month = m.month, year = m.year },
                                            revision => new
                                            {
                                                month = revision.TimeStamp.Month,
                                                year = revision.TimeStamp.Year
                                            },
                                            (p, g) => new
                                            {
                                                labels = p.year.ToString() + '/' + p.month.ToString(),
                                                datas = g.Count(),
                                                series = "All"
                                            }).ToList();
                        }
                    case TimeFormat.Day:
                        {
                            var dt = Enumerable.Range(-20, 21)
                                .Select(x => new
                                {
                                    year = now.AddDays(x).Year,
                                    month = now.AddDays(x).Month,
                                    day = now.AddDays(x).Day
                                });

                            return dt.GroupJoin(query,
                                            m => new { day = m.day, month = m.month, year = m.year },
                                            revision => new
                                            {
                                                day = revision.TimeStamp.Day,
                                                month = revision.TimeStamp.Month,
                                                year = revision.TimeStamp.Year
                                            },
                                            (p, g) => new
                                            {
                                                labels = p.year.ToString() + '/' + p.month.ToString() + '/' + p.day.ToString(),
                                                datas = g.Count(),
                                                series = "All"
                                            }).ToList();
                        }
                }
            }
            return null;
        }

        [HttpPost]
        public object GetTransactionTimeInfoByChargeDevice(TimeRequest request)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                var query = (from q in
                            (
                             from tr in db.TransRecord
                             join ct in db.ChargeDeviceType on tr.DeviceTypeId equals ct.DeviceTypeId
                             //join bt in db.BatteryType on tr.SlotBatteryRecord.DeviceTypeId equals bt.DeviceTypeId
                             where tr.TransStatusCode <= TransStatusCode.SmartChargerInsertedSuccess
                             select new { TimeStamp = tr.TransTimeStamp, DeviceTypeName = ct.DeviceTypeName })
                        group q by new { TimeStamp = q.TimeStamp, DeviceTypeName = q.DeviceTypeName } into g
                        select new
                        {
                            TimeStamp = g.Select(item => item.TimeStamp).Distinct().FirstOrDefault(),
                            DeviceTypeName = g.Select(item => item.DeviceTypeName).Distinct().FirstOrDefault(),
                            Count = g.Select(item => item.DeviceTypeName).Count()
                        });

                if (userInfo.UserRoleCode != UserRoleCode.Administrator)
                {
                    var query2 = (from bb in db.GroupChargeDeviceInfo
                                  join aa in
                                      (from gui in db.GroupUserInfo
                                       where gui.UserId == userInfo.UserId && gui.RelationRoleCode == RelationRoleCode.Manager
                                       select new { GroupId = gui.GroupId }) on bb.GroupId equals aa.GroupId
                                  select new { DeviceId = bb.DeviceId, DeviceTypeId = bb.DeviceTypeId });

                    query = (from q in
                                 (
                                  from tr in db.TransRecord
                                  join q in query2 on new { tr.DeviceId, tr.DeviceTypeId } equals new { q.DeviceId, q.DeviceTypeId }
                                  join ct in db.ChargeDeviceType on tr.DeviceTypeId equals ct.DeviceTypeId
                                  //join bt in db.BatteryType on tr.SlotBatteryRecord.DeviceTypeId equals bt.DeviceTypeId
                                  where tr.TransStatusCode <= TransStatusCode.SmartChargerInsertedSuccess
                                  select new { TimeStamp = tr.TransTimeStamp, DeviceTypeName = ct.DeviceTypeName })
                             group q by new { TimeStamp = q.TimeStamp, DeviceTypeName = q.DeviceTypeName } into g
                             select new
                             {
                                 TimeStamp = g.Select(item => item.TimeStamp).Distinct().FirstOrDefault(),
                                 DeviceTypeName = g.Select(item => item.DeviceTypeName).Distinct().FirstOrDefault(),
                                 Count = g.Select(item => item.DeviceTypeName).Count()
                             });
                }

                var now = DateTime.UtcNow;

                switch (request.TimeFormat)
                {
                    case TimeFormat.Year:
                        {
                            var dt = from x in Enumerable.Range(-2, 3)
                                     from ct in db.ChargeDeviceType
                                     select new
                                     {
                                         Year = now.AddYears(x).Year,
                                         DeviceTypeName = ct.DeviceTypeName
                                     };

                            return (from q3 in
                                        (from q1 in dt.DefaultIfEmpty()
                                         join q2 in query on new { Year = q1.Year, DeviceTypeName = q1.DeviceTypeName } equals new { Year = q2.TimeStamp.Year, DeviceTypeName = q2.DeviceTypeName } into ps
                                         from q2 in ps.DefaultIfEmpty()
                                         select new { Year = q1.Year, DeviceTypeName = q1.DeviceTypeName, Sum = q2 == null ? 0 : 1 })
                                    group q3 by new { Year = q3.Year, DeviceTypeName = q3.DeviceTypeName } into g
                                    select new { labels = g.Select(x => x.Year).FirstOrDefault().ToString(), datas = g.Sum(x => x.Sum), series = g.Select(x => x.DeviceTypeName).Distinct().FirstOrDefault() }); 

                        }
                    case TimeFormat.Month:
                        {
                            var dt = from x in Enumerable.Range(-12, 13)
                                     from ct in db.ChargeDeviceType
                                     select new
                                     {
                                         Year = now.AddMonths(x).Year,
                                         Month = now.AddMonths(x).Month,
                                         DeviceTypeName = ct.DeviceTypeName
                                     };

                            return (from q3 in
                                        (from q1 in dt.DefaultIfEmpty()
                                         join q2 in query on new { Year = q1.Year, Month = q1.Month, DeviceTypeName = q1.DeviceTypeName } equals new { Year = q2.TimeStamp.Year, Month = q2.TimeStamp.Month, DeviceTypeName = q2.DeviceTypeName } into ps
                                         from q2 in ps.DefaultIfEmpty()
                                         select new { Year = q1.Year, Month = q1.Month, DeviceTypeName = q1.DeviceTypeName, Sum = q2 == null ? 0 : 1 })
                                    group q3 by new { Year = q3.Year, Month = q3.Month, DeviceTypeName = q3.DeviceTypeName } into g
                                    select new { labels = g.Select(x => x.Year).FirstOrDefault().ToString() + '/' + g.Select(x => x.Month).FirstOrDefault().ToString(), datas = g.Sum(x => x.Sum), series = g.Select(x => x.DeviceTypeName).Distinct().FirstOrDefault() }); 

                        }
                    case TimeFormat.Day:
                        {
                            var dt = from x in Enumerable.Range(-20, 21)
                                     from ct in db.ChargeDeviceType
                                     select new
                                     {
                                         Year = now.AddDays(x).Year,
                                         Month = now.AddDays(x).Month,
                                         Day = now.AddDays(x).Day, 
                                         DeviceTypeName = ct.DeviceTypeName
                                     };

                            return (from q3 in
                                        (from q1 in dt.DefaultIfEmpty()
                                         join q2 in query on new { Year = q1.Year, Month = q1.Month, Day = q1.Day, DeviceTypeName = q1.DeviceTypeName } equals new { Year = q2.TimeStamp.Year, Month = q2.TimeStamp.Month, Day = q2.TimeStamp.Day, DeviceTypeName = q2.DeviceTypeName } into ps
                                         from q2 in ps.DefaultIfEmpty()
                                         select new { Year = q1.Year, Month = q1.Month, Day = q1.Day, DeviceTypeName = q1.DeviceTypeName, Sum = q2 == null ? 0 : 1 })
                                    group q3 by new { Year = q3.Year, Month = q3.Month, Day = q3.Day, DeviceTypeName = q3.DeviceTypeName } into g
                                    select new { labels = g.Select(x => x.Year).FirstOrDefault().ToString() + '/' + g.Select(x => x.Month).FirstOrDefault().ToString() + '/' + g.Select(x => x.Day).FirstOrDefault().ToString(), datas = g.Sum(x => x.Sum), series = g.Select(x => x.DeviceTypeName).Distinct().FirstOrDefault() }); 

                        }
                }

            }


            return null;
        }

        [HttpPost]
        public object GetTransactionTimeInfoByBattery(TimeRequest request)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                var query = (from q in
                                 (
                                  from tr in db.TransRecord
                                  //join ct in db.ChargerType on tr.DeviceTypeId equals ct.DeviceTypeId
                                  join bt in db.BatteryType on tr.InsertedTransDevice.DeviceTypeId equals bt.DeviceTypeId
                                  where tr.InsertedTransDeviceIndex > 0 && tr.TransStatusCode <= TransStatusCode.SmartChargerInsertedSuccess
                                  select new { TimeStamp = tr.TransTimeStamp, DeviceTypeName = bt.DeviceTypeName })
                             group q by new { TimeStamp = q.TimeStamp, DeviceTypeName = q.DeviceTypeName } into g
                             select new
                             {
                                 TimeStamp = g.Select(item => item.TimeStamp).Distinct().FirstOrDefault(),
                                 DeviceTypeName = g.Select(item => item.DeviceTypeName).Distinct().FirstOrDefault(),
                                 Count = g.Select(item => item.DeviceTypeName).Count()
                             });

                if (userInfo.UserRoleCode != UserRoleCode.Administrator)
                {
                    var query2 = (from bb in db.GroupChargeDeviceInfo
                                  join aa in
                                      (from gui in db.GroupUserInfo
                                       where gui.UserId == userInfo.UserId && gui.RelationRoleCode == RelationRoleCode.Manager
                                       select new { GroupId = gui.GroupId }) on bb.GroupId equals aa.GroupId
                                  select new { DeviceId = bb.DeviceId, DeviceTypeId = bb.DeviceTypeId });

                    query = (from q in
                                 (
                                  from tr in db.TransRecord
                                  join q in query2 on new { tr.DeviceId, tr.DeviceTypeId } equals new { q.DeviceId, q.DeviceTypeId }
                                  join bt in db.BatteryType on tr.InsertedTransDevice.DeviceTypeId equals bt.DeviceTypeId
                                  where tr.InsertedTransDeviceIndex > 0 && tr.TransStatusCode <= TransStatusCode.SmartChargerInsertedSuccess
                                  select new { TimeStamp = tr.TransTimeStamp, DeviceTypeName = bt.DeviceTypeName })
                             group q by new { TimeStamp = q.TimeStamp, DeviceTypeName = q.DeviceTypeName } into g
                             select new
                             {
                                 TimeStamp = g.Select(item => item.TimeStamp).Distinct().FirstOrDefault(),
                                 DeviceTypeName = g.Select(item => item.DeviceTypeName).Distinct().FirstOrDefault(),
                                 Count = g.Select(item => item.DeviceTypeName).Count()
                             });
                }

                var now = DateTime.UtcNow;

                switch (request.TimeFormat)
                {
                    case TimeFormat.Year:
                        {
                            var dt = from x in Enumerable.Range(-2, 3)
                                     from ct in db.BatteryType
                                     select new
                                     {
                                         Year = now.AddYears(x).Year,
                                         DeviceTypeName = ct.DeviceTypeName
                                     };

                            return (from q3 in
                                        (from q1 in dt.DefaultIfEmpty()
                                         join q2 in query on new { Year = q1.Year, DeviceTypeName = q1.DeviceTypeName } equals new { Year = q2.TimeStamp.Year, DeviceTypeName = q2.DeviceTypeName } into ps
                                         from q2 in ps.DefaultIfEmpty()
                                         select new { Year = q1.Year, DeviceTypeName = q1.DeviceTypeName, Sum = q2 == null ? 0 : 1 })
                                    group q3 by new { Year = q3.Year, DeviceTypeName = q3.DeviceTypeName } into g
                                    select new { labels = g.Select(x => x.Year).FirstOrDefault().ToString(), datas = g.Sum(x => x.Sum), series = g.Select(x => x.DeviceTypeName).Distinct().FirstOrDefault() }); 
                        }
                    case TimeFormat.Month:
                        {
                            var dt = from x in Enumerable.Range(-12, 13)
                                     from ct in db.BatteryType
                                     select new
                                     {
                                         Year = now.AddMonths(x).Year,
                                         Month = now.AddMonths(x).Month,
                                         DeviceTypeName = ct.DeviceTypeName
                                     };

                            return (from q3 in
                                        (from q1 in dt.DefaultIfEmpty()
                                         join q2 in query on new { Year = q1.Year, Month = q1.Month, DeviceTypeName = q1.DeviceTypeName } equals new { Year = q2.TimeStamp.Year, Month = q2.TimeStamp.Month, DeviceTypeName = q2.DeviceTypeName } into ps
                                         from q2 in ps.DefaultIfEmpty()
                                         select new { Year = q1.Year, Month = q1.Month, DeviceTypeName = q1.DeviceTypeName, Sum = q2 == null ? 0 : 1 })
                                    group q3 by new { Year = q3.Year, Month = q3.Month, DeviceTypeName = q3.DeviceTypeName } into g
                                    select new { labels = g.Select(x => x.Year).FirstOrDefault().ToString() + '/' + g.Select(x => x.Month).FirstOrDefault().ToString(), datas = g.Sum(x => x.Sum), series = g.Select(x => x.DeviceTypeName).Distinct().FirstOrDefault() }); 

                        }
                    case TimeFormat.Day:
                        {
                            var dt = from x in Enumerable.Range(-20, 21)
                                     from ct in db.BatteryType
                                     select new
                                     {
                                         Year = now.AddDays(x).Year,
                                         Month = now.AddDays(x).Month,
                                         Day = now.AddDays(x).Day,
                                         DeviceTypeName = ct.DeviceTypeName
                                     };

                            return (from q3 in
                                        (from q1 in dt.DefaultIfEmpty()
                                         join q2 in query on new { Year = q1.Year, Month = q1.Month, Day = q1.Day, DeviceTypeName = q1.DeviceTypeName } equals new { Year = q2.TimeStamp.Year, Month = q2.TimeStamp.Month, Day = q2.TimeStamp.Day, DeviceTypeName = q2.DeviceTypeName } into ps
                                         from q2 in ps.DefaultIfEmpty()
                                         select new { Year = q1.Year, Month = q1.Month, Day = q1.Day, DeviceTypeName = q1.DeviceTypeName, Sum = q2 == null ? 0 : 1 })
                                    group q3 by new { Year = q3.Year, Month = q3.Month, Day = q3.Day, DeviceTypeName = q3.DeviceTypeName } into g
                                    select new { labels = g.Select(x => x.Year).FirstOrDefault().ToString() + '/' + g.Select(x => x.Month).FirstOrDefault().ToString() + '/' + g.Select(x => x.Day).FirstOrDefault().ToString(), datas = g.Sum(x => x.Sum), series = g.Select(x => x.DeviceTypeName).Distinct().FirstOrDefault() }); 

                        }
                }

            }


            return null;
        }

        [HttpPost]
        public object GetChargeQuantityInfo(ChargeDeviceTimeRequest request)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                var query = from ctr in db.ChargeTransRecord
                         where ctr.ChargedCapacity > 0
                            select new { TimeStamp = ctr.ChargeTransTimeStamp, ChargedCapacity = ctr.ChargedCapacity };

                if (userInfo.UserRoleCode != UserRoleCode.Administrator)
                {
                    var query2 = (from bb in db.GroupChargeDeviceInfo
                                  join aa in
                                      (from gui in db.GroupUserInfo
                                       where gui.UserId == userInfo.UserId && gui.RelationRoleCode == RelationRoleCode.Manager
                                       select new { GroupId = gui.GroupId }) on bb.GroupId equals aa.GroupId
                                  select new { DeviceId = bb.DeviceId, DeviceTypeId = bb.DeviceTypeId });

                    if (request.DeviceId != null && request.DeviceTypeId > 0)
                    {
                        query = from ctr in db.ChargeTransRecord
                                join q in query2 on new { ctr.DeviceId, ctr.DeviceTypeId } equals new { q.DeviceId, q.DeviceTypeId }
                                where ctr.ChargedCapacity > 0 && (ctr.DeviceId == request.DeviceId && ctr.DeviceTypeId == request.DeviceTypeId)
                                select new { TimeStamp = ctr.ChargeTransTimeStamp, ChargedCapacity = ctr.ChargedCapacity };
                    }
                    else
                    {
                        query = from ctr in db.ChargeTransRecord
                                join q in query2 on new { ctr.DeviceId, ctr.DeviceTypeId } equals new { q.DeviceId, q.DeviceTypeId }
                                where ctr.ChargedCapacity > 0
                                select new { TimeStamp = ctr.ChargeTransTimeStamp, ChargedCapacity = ctr.ChargedCapacity };
                    }
                }
                else
                {
                    if (request.DeviceId != null && request.DeviceTypeId > 0)
                    {
                        query = from ctr in db.ChargeTransRecord
                                where ctr.ChargedCapacity > 0 && (ctr.DeviceId == request.DeviceId && ctr.DeviceTypeId == request.DeviceTypeId)
                                select new { TimeStamp = ctr.ChargeTransTimeStamp, ChargedCapacity = ctr.ChargedCapacity };
                    }
                }

                var now = DateTime.UtcNow;

                switch (request.TimeFormat)
                {
                    case TimeFormat.Year:
                        {
                            var dt = Enumerable.Range(-2, 3)
                                .Select(x => new
                                {
                                    year = now.AddYears(x).Year
                                });

                            return dt.GroupJoin(query,
                                    m => new { year = m.year },
                                    revision => new
                                    {
                                        year = revision.TimeStamp.Year
                                    },
                                    (p, g) => new
                                    {
                                        labels = p.year.ToString(),
                                        datas = g.Sum(x => x.ChargedCapacity),
                                        series = "All"
                                    }).ToList();
                        }
                    case TimeFormat.Month:
                        {
                            var dt = Enumerable.Range(-12, 13)
                                .Select(x => new
                                {
                                    year = now.AddMonths(x).Year,
                                    month = now.AddMonths(x).Month
                                });

                            return dt.GroupJoin(query,
                                            m => new { month = m.month, year = m.year },
                                            revision => new
                                            {
                                                month = revision.TimeStamp.Month,
                                                year = revision.TimeStamp.Year
                                            },
                                            (p, g) => new
                                            {
                                                labels = p.year.ToString() + '/' + p.month.ToString(),
                                                datas = g.Sum(x => x.ChargedCapacity),
                                                series = "All"
                                            }).ToList();
                        }
                    case TimeFormat.Day:
                        {
                            var dt = Enumerable.Range(-20, 21)
                                .Select(x => new
                                {
                                    year = now.AddDays(x).Year,
                                    month = now.AddDays(x).Month,
                                    day = now.AddDays(x).Day
                                });

                            return dt.GroupJoin(query,
                                            m => new { day = m.day, month = m.month, year = m.year },
                                            revision => new
                                            {
                                                day = revision.TimeStamp.Day,
                                                month = revision.TimeStamp.Month,
                                                year = revision.TimeStamp.Year
                                            },
                                            (p, g) => new
                                            {
                                                labels = p.year.ToString() + '/' + p.month.ToString() + '/' + p.day.ToString(),
                                                datas = g.Sum(x => x.ChargedCapacity),
                                                series = "All"
                                            }).ToList();
                        }
                }
            }
            return null;
        }

        [HttpPost]
        public object GetChargeQuantityInfoByChargeDevice(TimeRequest request)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                var query = (from q in
                             (from ctr in db.ChargeTransRecord
                              join ct in db.ChargeDeviceType on ctr.DeviceTypeId equals ct.DeviceTypeId
                            where ctr.ChargedCapacity > 0
                            select new { TimeStamp = ctr.ChargeTransTimeStamp, DeviceTypeName = ct.DeviceTypeName, ChargedCapacity = ctr.ChargedCapacity })
                            group q by new { TimeStamp = q.TimeStamp, DeviceTypeName = q.DeviceTypeName } into g
                             select new
                             {
                                 TimeStamp = g.Select(item => item.TimeStamp).Distinct().FirstOrDefault(),
                                 DeviceTypeName = g.Select(item => item.DeviceTypeName).Distinct().FirstOrDefault(),
                                 ChargedCapacity = g.Sum(item => item.ChargedCapacity)
                             });

                if (userInfo.UserRoleCode != UserRoleCode.Administrator)
                {
                    var query2 = (from bb in db.GroupChargeDeviceInfo
                                  join aa in
                                      (from gui in db.GroupUserInfo
                                       where gui.UserId == userInfo.UserId && gui.RelationRoleCode == RelationRoleCode.Manager
                                       select new { GroupId = gui.GroupId }) on bb.GroupId equals aa.GroupId
                                  select new { DeviceId = bb.DeviceId, DeviceTypeId = bb.DeviceTypeId });

                    query = (from q in
                                 (from ctr in db.ChargeTransRecord
                                  join q in query2 on new { ctr.DeviceId, ctr.DeviceTypeId } equals new { q.DeviceId, q.DeviceTypeId }
                                  join ct in db.ChargeDeviceType on ctr.DeviceTypeId equals ct.DeviceTypeId
                                  where ctr.ChargedCapacity > 0
                                  select new { TimeStamp = ctr.ChargeTransTimeStamp, DeviceTypeName = ct.DeviceTypeName, ChargedCapacity = ctr.ChargedCapacity })
                             group q by new { TimeStamp = q.TimeStamp, DeviceTypeName = q.DeviceTypeName } into g
                             select new
                             {
                                 TimeStamp = g.Select(item => item.TimeStamp).Distinct().FirstOrDefault(),
                                 DeviceTypeName = g.Select(item => item.DeviceTypeName).Distinct().FirstOrDefault(),
                                 ChargedCapacity = g.Sum(item => item.ChargedCapacity)
                             });
                }

                var now = DateTime.UtcNow;

                switch (request.TimeFormat)
                {
                    case TimeFormat.Year:
                        {
                            var dt = from x in Enumerable.Range(-2, 3)
                                     from ct in db.ChargeDeviceType
                                     select new
                                     {
                                         Year = now.AddYears(x).Year,
                                         DeviceTypeName = ct.DeviceTypeName
                                     };

                            return (from q3 in
                                        (from q1 in dt.DefaultIfEmpty()
                                         join q2 in query on new { Year = q1.Year, DeviceTypeName = q1.DeviceTypeName } equals new { Year = q2.TimeStamp.Year, DeviceTypeName = q2.DeviceTypeName } into ps
                                         from q2 in ps.DefaultIfEmpty()
                                         select new { Year = q1.Year, DeviceTypeName = q1.DeviceTypeName, ChargedCapacity = q2 == null ? 0 : q2.ChargedCapacity })
                                    group q3 by new { Year = q3.Year, DeviceTypeName = q3.DeviceTypeName } into g
                                    select new { labels = g.Select(x => x.Year).FirstOrDefault().ToString(), datas = g.Sum(x => x.ChargedCapacity), series = g.Select(x => x.DeviceTypeName).Distinct().FirstOrDefault() }); 

                        }
                    case TimeFormat.Month:
                        {
                            var dt = from x in Enumerable.Range(-12, 13)
                                     from ct in db.ChargeDeviceType
                                     select new
                                     {
                                         Year = now.AddMonths(x).Year,
                                         Month = now.AddMonths(x).Month,
                                         DeviceTypeName = ct.DeviceTypeName
                                     };
                            return (from q3 in
                                        (from q1 in dt.DefaultIfEmpty()
                                         join q2 in query on new { Year = q1.Year, Month = q1.Month, DeviceTypeName = q1.DeviceTypeName } equals new { Year = q2.TimeStamp.Year, Month = q2.TimeStamp.Month, DeviceTypeName = q2.DeviceTypeName } into ps
                                         from q2 in ps.DefaultIfEmpty()
                                         select new { Year = q1.Year, Month = q1.Month, DeviceTypeName = q1.DeviceTypeName, ChargedCapacity = q2 == null ? 0 : q2.ChargedCapacity })
                                    group q3 by new { Year = q3.Year, Month = q3.Month, DeviceTypeName = q3.DeviceTypeName } into g
                                    select new { labels = g.Select(x => x.Year).FirstOrDefault().ToString() + '/' + g.Select(x => x.Month).FirstOrDefault().ToString(), datas = g.Sum(x => x.ChargedCapacity), series = g.Select(x => x.DeviceTypeName).Distinct().FirstOrDefault() }); 

                        }
                    case TimeFormat.Day:
                        {
                            var dt = (from x in Enumerable.Range(-20, 21)
                                     from ct in db.ChargeDeviceType
                                     select new
                                     {
                                         Year = now.AddDays(x).Year,
                                         Month = now.AddDays(x).Month,
                                         Day = now.AddDays(x).Day,
                                         DeviceTypeName = ct.DeviceTypeName
                                     });

                            return (from q3 in
                                                (from q1 in dt.DefaultIfEmpty()
                                                 join q2 in query on new { Year = q1.Year, Month = q1.Month, Day = q1.Day, DeviceTypeName = q1.DeviceTypeName } equals new { Year = q2.TimeStamp.Year, Month = q2.TimeStamp.Month, Day = q2.TimeStamp.Day, DeviceTypeName = q2.DeviceTypeName } into ps
                                                 from q2 in ps.DefaultIfEmpty()
                                                 select new { Year = q1.Year, Month = q1.Month, Day = q1.Day, DeviceTypeName = q1.DeviceTypeName, ChargedCapacity = q2 == null ? 0 : q2.ChargedCapacity })
                                        group q3 by new { Year = q3.Year, Month = q3.Month, Day = q3.Day, DeviceTypeName = q3.DeviceTypeName } into g
                                    select new { labels = g.Select(x => x.Year).FirstOrDefault().ToString() + '/' + g.Select(x => x.Month).FirstOrDefault().ToString() + '/' + g.Select(x => x.Day).FirstOrDefault().ToString(), datas = g.Sum(x => x.ChargedCapacity), series = g.Select(x => x.DeviceTypeName).Distinct().FirstOrDefault() }); 
                        }
                }
            }
            return null;
        }

        [HttpPost]
        public object GetChargeQuantityInfoByBattery(TimeRequest request)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                var query = (from q in
                                 (from ctr in db.ChargeTransRecord
                                  join bt in db.BatteryType on ctr.BatteryTypeId equals bt.DeviceTypeId
                                  where ctr.ChargedCapacity > 0
                                  select new { TimeStamp = ctr.ChargeTransTimeStamp, DeviceTypeName = bt.DeviceTypeName, ChargedCapacity = ctr.ChargedCapacity })
                             group q by new { TimeStamp = q.TimeStamp, DeviceTypeName = q.DeviceTypeName } into g
                             select new
                             {
                                 TimeStamp = g.Select(item => item.TimeStamp).Distinct().FirstOrDefault(),
                                 DeviceTypeName = g.Select(item => item.DeviceTypeName).Distinct().FirstOrDefault(),
                                 ChargedCapacity = g.Sum(item => item.ChargedCapacity)
                             });

                if (userInfo.UserRoleCode != UserRoleCode.Administrator)
                {
                    var query2 = (from bb in db.GroupChargeDeviceInfo
                                  join aa in
                                      (from gui in db.GroupUserInfo
                                       where gui.UserId == userInfo.UserId && gui.RelationRoleCode == RelationRoleCode.Manager
                                       select new { GroupId = gui.GroupId }) on bb.GroupId equals aa.GroupId
                                  select new { DeviceId = bb.DeviceId, DeviceTypeId = bb.DeviceTypeId });

                    query = (from q in
                                 (from ctr in db.ChargeTransRecord
                                  join q in query2 on new { ctr.DeviceId, ctr.DeviceTypeId } equals new { q.DeviceId, q.DeviceTypeId }
                                  join bt in db.BatteryType on ctr.BatteryTypeId equals bt.DeviceTypeId
                                  where ctr.ChargedCapacity > 0
                                  select new { TimeStamp = ctr.ChargeTransTimeStamp, DeviceTypeName = bt.DeviceTypeName, ChargedCapacity = ctr.ChargedCapacity })
                             group q by new { TimeStamp = q.TimeStamp, DeviceTypeName = q.DeviceTypeName } into g
                             select new
                             {
                                 TimeStamp = g.Select(item => item.TimeStamp).Distinct().FirstOrDefault(),
                                 DeviceTypeName = g.Select(item => item.DeviceTypeName).Distinct().FirstOrDefault(),
                                 ChargedCapacity = g.Sum(item => item.ChargedCapacity)
                             });
                }

                var now = DateTime.UtcNow;

                switch (request.TimeFormat)
                {
                    case TimeFormat.Year:
                        {
                            var dt = from x in Enumerable.Range(-2, 3)
                                     from ct in db.BatteryType
                                     select new
                                     {
                                         Year = now.AddYears(x).Year,
                                         DeviceTypeName = ct.DeviceTypeName
                                     };

                            return (from q3 in
                                        (from q1 in dt.DefaultIfEmpty()
                                         join q2 in query on new { Year = q1.Year, DeviceTypeName = q1.DeviceTypeName } equals new { Year = q2.TimeStamp.Year, DeviceTypeName = q2.DeviceTypeName } into ps
                                         from q2 in ps.DefaultIfEmpty()
                                         select new { Year = q1.Year, DeviceTypeName = q1.DeviceTypeName, ChargedCapacity = q2 == null ? 0 : q2.ChargedCapacity })
                                    group q3 by new { Year = q3.Year, DeviceTypeName = q3.DeviceTypeName } into g
                                    select new { labels = g.Select(x => x.Year).FirstOrDefault().ToString(), datas = g.Sum(x => x.ChargedCapacity), series = g.Select(x => x.DeviceTypeName).Distinct().FirstOrDefault() }); 

                        }
                    case TimeFormat.Month:
                        {
                            var dt = from x in Enumerable.Range(-12, 13)
                                     from ct in db.BatteryType
                                     select new
                                     {
                                         Year = now.AddMonths(x).Year,
                                         Month = now.AddMonths(x).Month,
                                         DeviceTypeName = ct.DeviceTypeName
                                     };

                            return (from q3 in
                                        (from q1 in dt.DefaultIfEmpty()
                                         join q2 in query on new { Year = q1.Year, Month = q1.Month, DeviceTypeName = q1.DeviceTypeName } equals new { Year = q2.TimeStamp.Year, Month = q2.TimeStamp.Month, DeviceTypeName = q2.DeviceTypeName } into ps
                                         from q2 in ps.DefaultIfEmpty()
                                         select new { Year = q1.Year, Month = q1.Month, DeviceTypeName = q1.DeviceTypeName, ChargedCapacity = q2 == null ? 0 : q2.ChargedCapacity })
                                    group q3 by new { Year = q3.Year, Month = q3.Month, DeviceTypeName = q3.DeviceTypeName } into g
                                    select new { labels = g.Select(x => x.Year).FirstOrDefault().ToString() + '/' + g.Select(x => x.Month).FirstOrDefault().ToString(), datas = g.Sum(x => x.ChargedCapacity), series = g.Select(x => x.DeviceTypeName).Distinct().FirstOrDefault() }); 

                        }
                    case TimeFormat.Day:
                        {
                            var dt = from x in Enumerable.Range(-20, 21)
                                     from ct in db.BatteryType
                                     select new
                                     {
                                         Year = now.AddDays(x).Year,
                                         Month = now.AddDays(x).Month,
                                         Day = now.AddDays(x).Day,
                                         DeviceTypeName = ct.DeviceTypeName
                                     };

                            return (from q3 in
                                        (from q1 in dt.DefaultIfEmpty()
                                         join q2 in query on new { Year = q1.Year, Month = q1.Month, Day = q1.Day, DeviceTypeName = q1.DeviceTypeName } equals new { Year = q2.TimeStamp.Year, Month = q2.TimeStamp.Month, Day = q2.TimeStamp.Day, DeviceTypeName = q2.DeviceTypeName } into ps
                                         from q2 in ps.DefaultIfEmpty()
                                         select new { Year = q1.Year, Month = q1.Month, Day = q1.Day, DeviceTypeName = q1.DeviceTypeName, ChargedCapacity = q2 == null ? 0 : q2.ChargedCapacity })
                                    group q3 by new { Year = q3.Year, Month = q3.Month, Day = q3.Day, DeviceTypeName = q3.DeviceTypeName } into g
                                    select new { labels = g.Select(x => x.Year).FirstOrDefault().ToString() + '/' + g.Select(x => x.Month).FirstOrDefault().ToString() + '/' + g.Select(x => x.Day).FirstOrDefault().ToString(), datas = g.Sum(x => x.ChargedCapacity), series = g.Select(x => x.DeviceTypeName).Distinct().FirstOrDefault() }); 
                        }
                }
            }
            return null;
        }

        [HttpPost]
        public object GetHalfHourSOCByChargeDevice(ChargeDeviceTimeRequest req)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                var now = DateTime.UtcNow;
                var past = now.AddMinutes(-30);

               return (from sr in db.SlotRecord
                            join srd in db.SlotBatteryRecord on sr.SlotBatteryRecordIndex equals srd.SlotBatteryRecordIndex
                       where (sr.DeviceId == req.DeviceId && sr.DeviceTypeId == req.DeviceTypeId)
                            && (sr.TimeStamp >= past && sr.TimeStamp <= now)
                            orderby sr.TimeStamp ascending
                            select new { labels = sr.TimeStamp, series = sr.SlotId, datas = srd.SOC });
            }
            return null;
        }

        [HttpPost]
        public object GetChargeCurrentStatusByChargerBattery(ChargeDeviceBatteryTimeRequest req)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {


            }
            return null;
        }

        [HttpPost]
        public object GetCurrentChargerSlotInfo(ChargeDeviceTimeRequest req)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                var dt = DateTime.UtcNow;

                return (from ci in db.ChargeDeviceInfo
                        join sr in db.SlotRecord on new { ci.DeviceId, ci.DeviceTypeId } equals new { sr.DeviceId, sr.DeviceTypeId }
                        where (ci.DeviceId == req.DeviceId && ci.DeviceTypeId == req.DeviceTypeId)
                        orderby sr.TimeStamp descending
                        select new
                        {
                            Time = sr.TimeStamp,
                            SlotId = sr.SlotId,
                            BatteryRecord =  sr.SlotBatteryRecord
                        }).Take(30).OrderBy(x => x.Time);
            }

            return null;
        }

        [HttpPost]
        public object GetSOHByUserBattery(BatteryTypeRequest req)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                var query1 = (from q1 in db.BatteryInfo
                       select q1);
                if (req.BatteryTypeCode != 0)
                {
                    query1 = (from q1 in db.BatteryInfo
                              where q1.DeviceTypeId == req.BatteryTypeCode
                              select q1);
                }
                if (userInfo.UserRoleCode != UserRoleCode.Administrator)
                {

                    var query2 = (from bb in db.GroupBatteryInfo
                                  join aa in
                                      (from gui in db.GroupUserInfo
                                       where gui.UserId == userInfo.UserId && gui.RelationRoleCode == RelationRoleCode.Manager
                                       select new { GroupId = gui.GroupId }) on bb.GroupId equals aa.GroupId
                                  select new { BatteryId = bb.BatteryId, ManufactureDate = bb.ManufactureDate, DeviceTypeId = bb.DeviceTypeId });

                    if (req.BatteryTypeCode != 0)
                    {
                        query1 = (from q1 in db.BatteryInfo
                                  join q2 in query2 on new { q1.BatteryId, q1.ManufactureDate, q1.DeviceTypeId } equals new { q2.BatteryId, q2.ManufactureDate, q2.DeviceTypeId }
                                  where q1.DeviceTypeId == req.BatteryTypeCode
                                  select q1);
                    }
                    else
                    {
                        query1 = (from q1 in db.BatteryInfo
                                  join q2 in query2 on new { q1.BatteryId, q1.ManufactureDate, q1.DeviceTypeId } equals new { q2.BatteryId, q2.ManufactureDate, q2.DeviceTypeId }
                                  select q1);
                    }
                }

                var dt = DateTime.UtcNow.AddDays(-30);

                if (req.BatteryStyleLabel == null)
                {
                    return (from q1 in query1
                            where q1.LastUpdateTime > dt && q1.SOH == 100
                            group q1 by q1.SOH into g
                            select new { labels = "100%", datas = g.Count() }).Concat
                            (from q1 in query1
                             where q1.LastUpdateTime > dt && q1.SOH > 95 && q1.SOH < 100
                             group q1 by q1.SOH > 95 into g
                             select new { labels = "96-99%", datas = g.Count() }).Concat(
                              from q1 in query1
                              where q1.LastUpdateTime > dt && q1.SOH > 90 && q1.SOH <= 95
                              group q1 by q1.SOH > 90 into g
                              select new { labels = "91-95%", datas = g.Count() }).Concat(
                              from q1 in query1
                              where q1.LastUpdateTime > dt && q1.SOH > 80 && q1.SOH <= 90
                              group q1 by q1.SOH > 80 into g
                              select new { labels = "81-90%", datas = g.Count() }).Concat(
                              from q1 in query1
                              where q1.LastUpdateTime > dt && q1.SOH > 70 && q1.SOH <= 80
                              group q1 by q1.SOH > 70 into g
                              select new { labels = "71-80%", datas = g.Count() }).Concat(
                              from q1 in query1
                              where q1.LastUpdateTime > dt && q1.SOH >= 0 && q1.SOH <= 70
                              group q1 by q1.SOH >= 0 into g
                              select new { labels = "0-70%", datas = g.Count() }).Concat(
                              from q1 in query1
                              where q1.LastUpdateTime <= dt
                              group q1 by q1.SOH >= 0 into g
                              select new { labels = "Non-Update", datas = g.Count() }).ToList();
                }
                else
                {
                    switch (req.BatteryStyleLabel)
                    {
                        case "100%":
                            {
                                return (from q1 in query1
                                        where q1.LastUpdateTime > dt && q1.SOH == 100
                                        select new BatteryCurrentInfoModel()
                                        {
                                            BatteryId = q1.BatteryId,
                                            ManufactureDate = q1.ManufactureDate,
                                            DeviceTypeId = q1.DeviceTypeId,
                                            SOC = q1.SOC,
                                            LastUpdateTime = q1.LastUpdateTime,
                                            SOH = q1.SOH,
                                            CellVoltDiff = q1.CellVoltDiff
                                        }).ToList();
                            }
                        case "96-99%":
                            {
                                return (from q1 in query1
                                        where q1.LastUpdateTime > dt && q1.SOH > 95 && q1.SOH < 100
                                        select new BatteryCurrentInfoModel()
                                        {
                                            BatteryId = q1.BatteryId,
                                            ManufactureDate = q1.ManufactureDate,
                                            DeviceTypeId = q1.DeviceTypeId,
                                            SOC = q1.SOC,
                                            LastUpdateTime = q1.LastUpdateTime,
                                            SOH = q1.SOH,
                                            CellVoltDiff = q1.CellVoltDiff
                                        }).ToList();
                            }
                        case "91-95%":
                            {
                                return (from q1 in query1
                                        where q1.LastUpdateTime > dt && q1.SOH > 90 && q1.SOH <= 95
                                        select new BatteryCurrentInfoModel()
                                        {
                                            BatteryId = q1.BatteryId,
                                            ManufactureDate = q1.ManufactureDate,
                                            DeviceTypeId = q1.DeviceTypeId,
                                            SOC = q1.SOC,
                                            LastUpdateTime = q1.LastUpdateTime,
                                            SOH = q1.SOH,
                                            CellVoltDiff = q1.CellVoltDiff
                                        }).ToList();
                            }
                        case "81-90%":
                            {
                                return (from q1 in query1
                                        where q1.LastUpdateTime > dt && q1.SOH > 80 && q1.SOH <= 90
                                        select new BatteryCurrentInfoModel()
                                        {
                                            BatteryId = q1.BatteryId,
                                            ManufactureDate = q1.ManufactureDate,
                                            DeviceTypeId = q1.DeviceTypeId,
                                            SOC = q1.SOC,
                                            LastUpdateTime = q1.LastUpdateTime,
                                            SOH = q1.SOH,
                                            CellVoltDiff = q1.CellVoltDiff
                                        }).ToList();
                            }
                        case "71-80%":
                            {
                                return (from q1 in query1
                                        where q1.LastUpdateTime > dt && q1.SOH > 70 && q1.SOH <= 80
                                        select new BatteryCurrentInfoModel()
                                        {
                                            BatteryId = q1.BatteryId,
                                            ManufactureDate = q1.ManufactureDate,
                                            DeviceTypeId = q1.DeviceTypeId,
                                            SOC = q1.SOC,
                                            LastUpdateTime = q1.LastUpdateTime,
                                            SOH = q1.SOH,
                                            CellVoltDiff = q1.CellVoltDiff
                                        }).ToList();
                            }
                        case "0-70%":
                            {
                                return (from q1 in query1
                                        where q1.LastUpdateTime > dt && q1.SOH >= 0 && q1.SOH <= 70
                                        select new BatteryCurrentInfoModel()
                                        {
                                            BatteryId = q1.BatteryId,
                                            ManufactureDate = q1.ManufactureDate,
                                            DeviceTypeId = q1.DeviceTypeId,
                                            SOC = q1.SOC,
                                            LastUpdateTime = q1.LastUpdateTime,
                                            SOH = q1.SOH,
                                            CellVoltDiff = q1.CellVoltDiff
                                        }).ToList();
                            }
                        case "Non-Update":
                            {
                                return (from q1 in query1
                                        where q1.LastUpdateTime <= dt
                                        select new BatteryCurrentInfoModel()
                                        {
                                            BatteryId = q1.BatteryId,
                                            ManufactureDate = q1.ManufactureDate,
                                            DeviceTypeId = q1.DeviceTypeId,
                                            SOC = q1.SOC,
                                            LastUpdateTime = q1.LastUpdateTime,
                                            SOH = q1.SOH,
                                            CellVoltDiff = q1.CellVoltDiff
                                        }).ToList();
                            }
                    }
                }

            }

            return null;
        }

        /// <summary>
        ///  Add By Neil 2016.04.18 
        ///  Get Diff. of Cell Volt. by user
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public object GetCellVoltDiffByUserBattery(BatteryTypeRequest req)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                var query1 = (from q1 in db.BatteryInfo
                              select q1);
                if (req.BatteryTypeCode != 0)
                {
                    query1 = (from q1 in db.BatteryInfo
                              where q1.DeviceTypeId == req.BatteryTypeCode
                              select q1);
                }
                if (userInfo.UserRoleCode != UserRoleCode.Administrator)
                {

                    var query2 = (from bb in db.GroupBatteryInfo
                                  join aa in
                                      (from gui in db.GroupUserInfo
                                       where gui.UserId == userInfo.UserId && gui.RelationRoleCode == RelationRoleCode.Manager
                                       select new { GroupId = gui.GroupId }) on bb.GroupId equals aa.GroupId
                                  select new { BatteryId = bb.BatteryId, ManufactureDate = bb.ManufactureDate, DeviceTypeId = bb.DeviceTypeId });

                    if (req.BatteryTypeCode != 0)
                    {
                        query1 = (from q1 in db.BatteryInfo
                                  join q2 in query2 on new { q1.BatteryId, q1.ManufactureDate, q1.DeviceTypeId } equals new { q2.BatteryId, q2.ManufactureDate, q2.DeviceTypeId }
                                  where q1.DeviceTypeId == req.BatteryTypeCode
                                  select q1);
                    }
                    else
                    {
                        query1 = (from q1 in db.BatteryInfo
                                  join q2 in query2 on new { q1.BatteryId, q1.ManufactureDate, q1.DeviceTypeId } equals new { q2.BatteryId, q2.ManufactureDate, q2.DeviceTypeId }
                                  select q1);
                    }
                }

                var dt = DateTime.UtcNow.AddDays(-30);

                if (req.BatteryStyleLabel == null)
                {
                    return (from q1 in query1
                            where q1.LastUpdateTime > dt && q1.CellVoltDiff > 200
                            group q1 by q1.CellVoltDiff into g
                            select new { labels = ">200", datas = g.Count() }).Concat
                                (from q1 in query1
                                 where q1.LastUpdateTime > dt && q1.CellVoltDiff > 100 && q1.CellVoltDiff <= 200
                                 group q1 by q1.CellVoltDiff > 100 into g
                                 select new { labels = "101-200", datas = g.Count() }).Concat(
                                  from q1 in query1
                                  where q1.LastUpdateTime > dt && q1.CellVoltDiff > 50 && q1.CellVoltDiff <= 100
                                  group q1 by q1.CellVoltDiff > 50 into g
                                  select new { labels = "51-100", datas = g.Count() }).Concat(
                                  from q1 in query1
                                  where q1.LastUpdateTime > dt && q1.CellVoltDiff > 10 && q1.CellVoltDiff <= 50
                                  group q1 by q1.CellVoltDiff > 10 into g
                                  select new { labels = "11-50", datas = g.Count() }).Concat(
                                  from q1 in query1
                                  where q1.LastUpdateTime > dt && q1.CellVoltDiff >= 0 && q1.CellVoltDiff <= 10
                                  group q1 by q1.CellVoltDiff > 0 into g
                                  select new { labels = "0-10", datas = g.Count() }).Concat(
                                  from q1 in query1
                                  where q1.LastUpdateTime <= dt
                                  group q1 by q1.CellVoltDiff >= 0 into g
                                  select new { labels = "Non-Update", datas = g.Count() }).ToList();
                }
                else
                {
                    switch (req.BatteryStyleLabel)
                    {
                        case ">200":
                            {
                                return (from q1 in query1
                                        where q1.LastUpdateTime > dt && q1.CellVoltDiff > 200
                                        select new BatteryCurrentInfoModel()
                                        {
                                            BatteryId = q1.BatteryId,
                                            ManufactureDate = q1.ManufactureDate,
                                            DeviceTypeId = q1.DeviceTypeId,
                                            SOC = q1.SOC,
                                            LastUpdateTime = q1.LastUpdateTime,
                                            SOH = q1.SOH,
                                            CellVoltDiff = q1.CellVoltDiff
                                        }).ToList();
                            }
                        case "101-200":
                            {
                                return (from q1 in query1
                                        where q1.LastUpdateTime > dt && q1.CellVoltDiff > 100 && q1.CellVoltDiff <= 200
                                        select new BatteryCurrentInfoModel()
                                        {
                                            BatteryId = q1.BatteryId,
                                            ManufactureDate = q1.ManufactureDate,
                                            DeviceTypeId = q1.DeviceTypeId,
                                            SOC = q1.SOC,
                                            LastUpdateTime = q1.LastUpdateTime,
                                            SOH = q1.SOH,
                                            CellVoltDiff = q1.CellVoltDiff
                                        }).ToList();
                            }
                        case "51-100":
                            {
                                return (from q1 in query1
                                        where q1.LastUpdateTime > dt && q1.CellVoltDiff > 50 && q1.CellVoltDiff <= 100
                                        select new BatteryCurrentInfoModel()
                                        {
                                            BatteryId = q1.BatteryId,
                                            ManufactureDate = q1.ManufactureDate,
                                            DeviceTypeId = q1.DeviceTypeId,
                                            SOC = q1.SOC,
                                            LastUpdateTime = q1.LastUpdateTime,
                                            SOH = q1.SOH,
                                            CellVoltDiff = q1.CellVoltDiff
                                        }).ToList();
                            }
                        case "11-50":
                            {
                                return (from q1 in query1
                                        where q1.LastUpdateTime > dt && q1.CellVoltDiff > 10 && q1.CellVoltDiff <= 50
                                        select new BatteryCurrentInfoModel()
                                        {
                                            BatteryId = q1.BatteryId,
                                            ManufactureDate = q1.ManufactureDate,
                                            DeviceTypeId = q1.DeviceTypeId,
                                            SOC = q1.SOC,
                                            LastUpdateTime = q1.LastUpdateTime,
                                            SOH = q1.SOH,
                                            CellVoltDiff = q1.CellVoltDiff
                                        }).ToList();
                            }
                        case "0-10":
                            {
                                return (from q1 in query1
                                        where q1.LastUpdateTime > dt && q1.CellVoltDiff >= 0 && q1.CellVoltDiff <= 10
                                        select new BatteryCurrentInfoModel()
                                        {
                                            BatteryId = q1.BatteryId,
                                            ManufactureDate = q1.ManufactureDate,
                                            DeviceTypeId = q1.DeviceTypeId,
                                            SOC = q1.SOC,
                                            LastUpdateTime = q1.LastUpdateTime,
                                            SOH = q1.SOH,
                                            CellVoltDiff = q1.CellVoltDiff
                                        }).ToList();
                            }
                        case "Non-Update":
                            {
                                return (from q1 in query1
                                        where q1.LastUpdateTime <= dt
                                        select new BatteryCurrentInfoModel()
                                        {
                                            BatteryId = q1.BatteryId,
                                            ManufactureDate = q1.ManufactureDate,
                                            DeviceTypeId = q1.DeviceTypeId,
                                            SOC = q1.SOC,
                                            LastUpdateTime = q1.LastUpdateTime,
                                            SOH = q1.SOH,
                                            CellVoltDiff = q1.CellVoltDiff
                                        }).ToList();
                            }
                    }
                }


            }

            return null;
        }


    }
}
