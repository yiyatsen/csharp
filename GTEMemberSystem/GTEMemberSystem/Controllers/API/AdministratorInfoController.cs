﻿using GTEMemberSystem.Models;
using GTEMemberSystem.Models.DBConfigure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GTEMemberSystem.Controllers.API
{
    public class AdministratorInfoController : ApiController
    {
        GTEDMSDBContext db = new GTEDMSDBContext();

        [HttpGet]
        public UserProfile GetUserProfile()
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                return (from ui in db.UserInfo
                        where ui.UserId == userInfo.UserId
                        select new UserProfile()
                        {
                            Address = ui.Address,
                            Email = ui.Email,
                            Password = ui.Password,
                            Phone = ui.Phone,
                            UserName = ui.UserName
                        }).FirstOrDefault();
            }

            return null;
        }

        [HttpPost]
        public int SetUserProfile(UserProfile model)
        {
            int result = 0; // 比對錯誤
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            if (userInfo != null && model != null && model.SecurityKey != null && model.SecurityKey.Equals(securityKey))
            {
                UserInfo ui = db.UserInfo.Find(userInfo.UserId);

                if (ui != null)
                {
                    ui.UserName = model.UserName;
                    ui.Password = model.Password;
                    ui.Phone = model.Phone;
                    ui.Address = model.Address;
                    ui.Email = model.Email;

                    db.Entry(ui).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    result = 1; //編輯成功

                }
            }
            return result;
        }

        [HttpGet]
        public UserInfoModel GetUserInfo()
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                UserInfoModel uim = new UserInfoModel()
                {
                    Account = userInfo.Account,
                    Address = userInfo.Address,
                    Email = userInfo.Email,
                    Password = userInfo.Password,
                    Phone = userInfo.Phone,
                    UserId = userInfo.UserId,
                    UserName = userInfo.UserName,
                    UserRoleCode = userInfo.UserRoleCode,
                    Description = userInfo.Description,
                    RegisterDateTime = userInfo.RegisterDateTime
                };

                return uim;
            }

            return null;
        }

        [HttpGet]
        public List<UserInfoModel> GetUserInfos()
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    return (from ui in db.UserInfo
                            select new UserInfoModel()
                            {
                                Account = ui.Account,
                                Address = ui.Address,
                                Email = ui.Email,
                                Password = ui.Password,
                                Phone = ui.Phone,
                                UserId = ui.UserId,
                                UserName = ui.UserName,
                                UserRoleCode = ui.UserRoleCode,
                                Description = ui.Description,
                                RegisterDateTime = ui.RegisterDateTime
                            }).ToList();
                }
            }

            return null;
        }

        [HttpPost]
        public object GetUserGroupRelationship(UserInfoModel req)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null && req != null)
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    return (from gi in db.GroupInfo
                            join q2 in
                                (from gui in db.GroupUserInfo
                                 where gui.UserId == req.UserId
                                 select gui) on gi.GroupId equals q2.GroupId into ps
                            from q2 in ps.DefaultIfEmpty()
                            select new { EnableInit = q2 == null ? false : true, Enable = q2 == null ? false : true, GroupId = gi.GroupId, GroupName = gi.GroupName, RelationRoleCode = q2 == null ? RelationRoleCode.NonActivity : q2.RelationRoleCode }).ToList();
                }
                else if (userInfo.UserId == req.UserId)
                {
                    return (from gi in db.GroupInfo
                            join q2 in
                                (from gui in db.GroupUserInfo
                                 where gui.UserId == req.UserId
                                 select gui) on gi.GroupId equals q2.GroupId into ps
                            from q2 in ps.DefaultIfEmpty()
                            select new { EnableInit = q2 == null ? false : true, Enable = q2 == null ? false : true, GroupId = gi.GroupId, GroupName = gi.GroupName, RelationRoleCode = q2 == null ? RelationRoleCode.NonActivity : q2.RelationRoleCode }).ToList();
                }
            }

            return null;
        }

        [HttpPost]
        public int SetUserGroupRelationship(UserGroupRelationInfoModel req)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null && req != null && req.GroupRelationInfos != null)
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator || userInfo.UserId == req.UserId)
                {
                    foreach (GroupRelationInfo gri in req.GroupRelationInfos)
                    {
                        GroupUserInfo gui = db.GroupUserInfo.Find(gri.GroupId, req.UserId);
                        if (gui == null)
                        {
                            gui = new GroupUserInfo()
                            {
                                GroupId = gri.GroupId,
                                UserId = req.UserId,
                                RelationRoleCode = gri.RelationRoleCode
                            };

                            db.GroupUserInfo.Add(gui);
                        }
                        else
                        {
                            if (!gri.Enable)
                            {
                                db.GroupUserInfo.Remove(gui);
                            }
                            else
                            {
                                gui.RelationRoleCode = gri.RelationRoleCode;
                                                    db.Entry(gui).State = System.Data.Entity.EntityState.Modified;
                    
                            }
                        }
                    }
                    db.SaveChanges();
                    return 1;
                }
            }

            return 0;
        }

        [HttpPost]
        public object GetChargerGroupRelationship(ChargeDeviceInfoModel req)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null && req != null)
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    return (from gi in db.GroupInfo
                            join q2 in
                                (from gci in db.GroupChargeDeviceInfo
                                 where gci.DeviceId == req.DeviceId && gci.DeviceTypeId == req.DeviceTypeId
                                 select gci) on gi.GroupId equals q2.GroupId into ps
                            from q2 in ps.DefaultIfEmpty()
                            select new { EnableInit = q2 == null ? false : true, Enable = q2 == null ? false : true, GroupId = gi.GroupId, GroupName = gi.GroupName }).ToList();
                }
                else if (userInfo.UserId == req.OwnerId)
                {
                    return (from gi in (from gi2 in db.GroupInfo
                                                    join gui in db.GroupUserInfo on gi2.GroupId equals gui.GroupId
                                                    where gui.UserId == req.OwnerId
                                                    select gi2)
                            join q2 in
                                (from gci in db.GroupChargeDeviceInfo
                                 where gci.DeviceId == req.DeviceId && gci.DeviceTypeId == req.DeviceTypeId
                                 select gci) on gi.GroupId equals q2.GroupId into ps
                            from q2 in ps.DefaultIfEmpty()
                            select new { EnableInit = q2 == null ? false : true, Enable = q2 == null ? false : true, GroupId = gi.GroupId, GroupName = gi.GroupName }).ToList();
                }
            }

            return null;
        }

        [HttpPost]
        public int SetChargerGroupRelationship(ChargeDeviceGroupRelationInfoModel req)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null && req != null && req.GroupRelationInfos != null && req.DeviceId != null)
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator || userInfo.UserId == req.UserId)
                {
                    foreach (GroupRelationInfo gri in req.GroupRelationInfos)
                    {
                        GroupChargeDeviceInfo gci = db.GroupChargeDeviceInfo.Find(gri.GroupId, req.DeviceId, req.DeviceTypeId);
                        if (gci == null)
                        {
                            gci = new GroupChargeDeviceInfo()
                            {
                                GroupId = gri.GroupId,
                                DeviceId = req.DeviceId,
                                //StationManufactureDate = req.StationManufactureDate,
                                DeviceTypeId = req.DeviceTypeId
                            };

                            db.GroupChargeDeviceInfo.Add(gci);
                        }
                        else
                        {
                            if (!gri.Enable)
                            {
                                db.GroupChargeDeviceInfo.Remove(gci);
                            }
                        }
                    }
                    db.SaveChanges();
                    return 1;
                }
            }

            return 0;
        }

        [HttpPost]
        public object GetBatteryGroupRelationship(BatteryInfoModel req)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null && req != null)
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    return (from gi in db.GroupInfo
                            join q2 in
                                (from gbi in db.GroupBatteryInfo
                                 where gbi.BatteryId == req.BatteryId && gbi.ManufactureDate == req.ManufactureDate && gbi.DeviceTypeId == req.DeviceTypeId
                                 select gbi) on gi.GroupId equals q2.GroupId into ps
                            from q2 in ps.DefaultIfEmpty()
                            select new { EnableInit = q2 == null ? false : true, Enable = q2 == null ? false : true, GroupId = gi.GroupId, GroupName = gi.GroupName }).ToList();
                }
                else if (userInfo.UserId == req.OwnerId)
                {
                    return (from gi in
                                (from gi2 in db.GroupInfo
                                 join gui in db.GroupUserInfo on gi2.GroupId equals gui.GroupId
                                 where gui.UserId == req.OwnerId
                                 select gi2)
                            join q2 in
                                (from gbi in db.GroupBatteryInfo
                                 where gbi.BatteryId == req.BatteryId && gbi.ManufactureDate == req.ManufactureDate && gbi.DeviceTypeId == req.DeviceTypeId
                                 select gbi) on gi.GroupId equals q2.GroupId into ps
                            from q2 in ps.DefaultIfEmpty()
                            select new { EnableInit = q2 == null ? false : true, Enable = q2 == null ? false : true, GroupId = gi.GroupId, GroupName = gi.GroupName }).ToList();
                }
            }

            return null;
        }

        [HttpPost]
        public int SetBatteryGroupRelationship(BatteryGroupRelationInfoModel req)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null && req != null && req.GroupRelationInfos != null)
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator || userInfo.UserId == req.UserId)
                {
                    foreach (GroupRelationInfo gri in req.GroupRelationInfos)
                    {
                        GroupBatteryInfo gbi = db.GroupBatteryInfo.Find(gri.GroupId, req.BatteryId, req.ManufactureDate, req.DeviceTypeId);
                        if (gbi == null)
                        {
                            if (gri.Enable)
                            {
                                gbi = new GroupBatteryInfo()
                                {
                                    GroupId = gri.GroupId,
                                    BatteryId = req.BatteryId,
                                    ManufactureDate = req.ManufactureDate,
                                    DeviceTypeId = req.DeviceTypeId,
                                    StatusCode = StatusCode.Useable
                                };

                                db.GroupBatteryInfo.Add(gbi);
                            }
                        }
                        else
                        {
                            if (!gri.Enable)
                            {
                                db.GroupBatteryInfo.Remove(gbi);
                            }
                        }
                    }
                    db.SaveChanges();
                    return 1;
                }
            }

            return 0;
        }

        [HttpPost]
        public int CreateUserInfo(UserInfoModel userInfoModel)
        {
            int result = 0; // 比對錯誤
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            UserInfo ui = db.UserInfo.Find(userInfoModel.UserId);

            if (userInfo != null && ui == null && userInfoModel.Account != null && userInfoModel.UserName != null && userInfoModel.Password != null
                && userInfoModel.Phone != null && userInfoModel.Address != null && userInfoModel.Email != null)
            {
                ui = new UserInfo()
                {
                    Account = userInfoModel.Account,
                    UserName = userInfoModel.UserName,
                    Password = userInfoModel.Password,
                    Phone = userInfoModel.Phone,
                    Address = userInfoModel.Address,
                    Email = userInfoModel.Email,
                    Description = userInfoModel.Description,
                    UserRoleCode = userInfoModel.UserRoleCode,
                    RegisterDateTime = DateTime.UtcNow,
                    RefId = userInfo.UserId
                };

                db.UserInfo.Add(ui);

                db.SaveChanges();
                result = 1; //新增成功
            }
            else
            {
                result = -1; // 新增失敗
            }

            return result;
        }

        [HttpPost]
        public int EditUserInfo(UserInfoModel userInfoModel)
        {
            int result = 0; // 比對錯誤
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            if (userInfoModel != null)
            {
                UserInfo ui = db.UserInfo.Find(userInfoModel.UserId);
                if (ui != null && (ui.Account.Equals(userInfoModel.Account) || (userInfo != null && userInfo.UserRoleCode == UserRoleCode.Administrator)))
                {
                    if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                    {
                        ui.Account = userInfoModel.Account;
                        ui.UserName = userInfoModel.UserName;
                        ui.Password = userInfoModel.Password;
                        ui.Phone = userInfoModel.Phone;
                        ui.Address = userInfoModel.Address;
                        ui.Email = userInfoModel.Email;
                        ui.Description = userInfoModel.Description;
                        ui.UserRoleCode = userInfoModel.UserRoleCode;
                    }
                    else if (userInfoModel.Account == ui.Account)
                    {
                        ui.UserName = userInfoModel.UserName;
                        ui.Password = userInfoModel.Password;
                        ui.Phone = userInfoModel.Phone;
                        ui.Address = userInfoModel.Address;
                        ui.Email = userInfoModel.Email;
                        ui.Description = userInfoModel.Description;
                        ui.UserRoleCode = userInfoModel.UserRoleCode;
                    }

                    db.Entry(ui).State = System.Data.Entity.EntityState.Modified;

                    db.SaveChanges();
                    result = 1; // 編輯成功
                }
                else
                {
                    result = -1; // 編輯失敗
                }
            }

            return result;
        }

        [HttpGet]
        public List<ChargeDeviceInfoModel> GetChargerInfos()
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    return (from cdi in db.ChargeDeviceInfo
                            select new ChargeDeviceInfoModel()
                            {
                                DeviceId = cdi.DeviceId,
                                //StationManufactureDate = cdi.StationManufactureDate,
                                DeviceTypeId = cdi.DeviceTypeId,
                                BatteryTypeId = cdi.BatteryTypeId,

                                Password = cdi.Password,
                                Name = cdi.Name,
                                Address = cdi.Address,
                                StatusCode = cdi.StatusCode,
                                Latitude = cdi.Latitude,
                                Longitude = cdi.Longitude,
                                APVersion = cdi.APVersion,
                                OSVersion = cdi.OSVersion,
                                SlotNumber = cdi.SlotNumber,
                                OwnerId = cdi.OwnerId
                            }).ToList();
                }
                else
                {
                    return (from cdi in db.ChargeDeviceInfo
                            where cdi.OwnerId == userInfo.UserId
                            select new ChargeDeviceInfoModel()
                            {
                                DeviceId = cdi.DeviceId,
                                //StationManufactureDate = cdi.StationManufactureDate,
                                DeviceTypeId = cdi.DeviceTypeId,
                                BatteryTypeId = cdi.BatteryTypeId,

                                Password = cdi.Password,
                                Name = cdi.Name,
                                Address = cdi.Address,
                                StatusCode = cdi.StatusCode,
                                Latitude = cdi.Latitude,
                                Longitude = cdi.Longitude,
                                APVersion = cdi.APVersion,
                                OSVersion = cdi.OSVersion,
                                SlotNumber = cdi.SlotNumber,
                                OwnerId = cdi.OwnerId
                            }).ToList();
                }
            }

            return null;
        }

        [HttpPost]
        public int CreateChargerInfo(ChargeDeviceInfoModel chargerInfoModel)
        {
            int result = 0; // 比對錯誤
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            if (userInfo != null && chargerInfoModel != null && chargerInfoModel.SecurityKey != null && chargerInfoModel.SecurityKey.Equals(securityKey))
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    ChargeDeviceInfo ci = db.ChargeDeviceInfo.Find(chargerInfoModel.DeviceId, chargerInfoModel.DeviceTypeId);

                    if (ci == null)
                    {

                        ci = new ChargeDeviceInfo()
                        {
                            //StationManufactureDate = chargerInfoModel.StationManufactureDate,
                            DeviceTypeId = chargerInfoModel.DeviceTypeId,
                            DeviceId = chargerInfoModel.DeviceId,
                            Password = chargerInfoModel.Password,
                            Name = chargerInfoModel.Name,
                            Address = chargerInfoModel.Address,
                            StatusCode = chargerInfoModel.StatusCode,
                            BatteryTypeId = chargerInfoModel.BatteryTypeId,                        
                            Longitude = chargerInfoModel.Longitude,
                            Latitude = chargerInfoModel.Latitude,
                            
                            SlotNumber = chargerInfoModel.SlotNumber,
                            OwnerId = userInfo.UserId,
                            RegisterDateTime = DateTime.UtcNow,
                            LastLoginTime = new DateTime(2012, 9, 30)
                        };

                        db.ChargeDeviceInfo.Add(ci);

                        db.SaveChanges();
                        result = 1; //新增成功
                    }
                    else
                    {
                        result = -1; // 新增失敗
                    }
                }
            }

            return result;
        }

        [HttpPost]
        public int EditChargerInfo(ChargeDeviceInfoModel chargerInfoModel)
        {
            int result = 0; // 比對錯誤
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            if (userInfo != null && chargerInfoModel != null && chargerInfoModel.SecurityKey != null && chargerInfoModel.SecurityKey.Equals(securityKey))
            {
                if (!((userInfo.UserRoleCode == UserRoleCode.Administrator) || (userInfo.UserId == chargerInfoModel.OwnerId)))
                {
                    result = -1; // 編輯失敗
                }
                else
                {
                    ChargeDeviceInfo ci = db.ChargeDeviceInfo.Find(chargerInfoModel.DeviceId, chargerInfoModel.DeviceTypeId);

                    if (ci != null)
                    {
                        if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                        {
                            ci.DeviceId = chargerInfoModel.DeviceId;
                            //ci.StationManufactureDate = chargerInfoModel.StationManufactureDate;
                            ci.DeviceTypeId = chargerInfoModel.DeviceTypeId;
                            ci.BatteryTypeId = chargerInfoModel.BatteryTypeId;

                            ci.Name = chargerInfoModel.Name;
                            ci.Password = chargerInfoModel.Password;
                            ci.Latitude = chargerInfoModel.Latitude;
                            ci.Longitude = chargerInfoModel.Longitude;
                            ci.Address = chargerInfoModel.Address;
                            ci.StatusCode = chargerInfoModel.StatusCode;

                            ci.SlotNumber = chargerInfoModel.SlotNumber;
                            ci.Description = chargerInfoModel.Description;

                            ci.OwnerId = chargerInfoModel.OwnerId;
                        }
                        else if (userInfo.UserId == chargerInfoModel.OwnerId)
                        {
                            ci.Name = chargerInfoModel.Name;
                            ci.Password = chargerInfoModel.Password;
                            ci.Latitude = chargerInfoModel.Latitude;
                            ci.Longitude = chargerInfoModel.Longitude;
                            ci.Address = chargerInfoModel.Address;
                            ci.StatusCode = chargerInfoModel.StatusCode;
                        }

                        db.Entry(ci).State = System.Data.Entity.EntityState.Modified;

                        db.SaveChanges();
                        result = 1; // 編輯成功
                    }
                    else
                    {
                        result = -1; // 編輯失敗
                    }
                }
            }
            return result;
        }

        [HttpGet]
        public List<BatteryInfoModel> GetBatteryInfos()
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    return (from bi in db.BatteryInfo
                            select new BatteryInfoModel()
                            {
                                BatteryId = bi.BatteryId,
                                ManufactureDate = bi.ManufactureDate,
                                DeviceTypeId = bi.DeviceTypeId,
                                StatusCode = bi.StatusCode,
                                LastUpdateTime = bi.LastUpdateTime,
                                RegisterDateTime = bi.RegisterDateTime,
                                APVersion = bi.APVersion,
                                Description = bi.Description,
                                OwnerId = bi.OwnerId
                            }).ToList();
                }
                else
                {
                    return (from bi in db.BatteryInfo
                            where bi.OwnerId == userInfo.UserId
                            select new BatteryInfoModel()
                            {
                                BatteryId = bi.BatteryId,
                                ManufactureDate = bi.ManufactureDate,
                                DeviceTypeId = bi.DeviceTypeId,
                                StatusCode = bi.StatusCode,
                                LastUpdateTime = bi.LastUpdateTime,
                                RegisterDateTime = bi.RegisterDateTime,
                                APVersion = bi.APVersion,
                                Description = bi.Description,
                                OwnerId = bi.OwnerId
                            }).ToList();
                }
            }

            return null;
        }

        [HttpPost]
        public int CreateBatteryInfo(BatteryInfoModel batteryInfoModel)
        {
            int result = 0; // 比對錯誤
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            if (userInfo != null && batteryInfoModel != null && batteryInfoModel.SecurityKey != null && batteryInfoModel.SecurityKey.Equals(securityKey))
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    BatteryInfo bi = db.BatteryInfo.Find(batteryInfoModel.BatteryId, batteryInfoModel.ManufactureDate, batteryInfoModel.DeviceTypeId);
                    BatteryType bt = db.BatteryType.Find(batteryInfoModel.DeviceTypeId);
                    if (bi == null && bt != null)
                    {
                        bi = new BatteryInfo()
                        {
                            BatteryId = batteryInfoModel.BatteryId,
                            ManufactureDate = batteryInfoModel.ManufactureDate,
                            DeviceTypeId = batteryInfoModel.DeviceTypeId,

                            StatusCode = batteryInfoModel.StatusCode,
                            OwnerId = batteryInfoModel.OwnerId,
                            RegisterDateTime = DateTime.UtcNow
                        };

                        db.BatteryInfo.Add(bi);

                        db.SaveChanges();
                        result = 1; //新增成功
                    }
                    else
                    {
                        result = -1; // 新增失敗
                    }
                }
            }

            return result;
        }

        [HttpPost]
        public int EditBatteryInfo(BatteryInfoModel batteryInfoModel)
        {
            int result = 0; // 比對錯誤
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            if (userInfo != null && batteryInfoModel != null && batteryInfoModel.SecurityKey != null && batteryInfoModel.SecurityKey.Equals(securityKey))
            {
                if (!((userInfo.UserRoleCode == UserRoleCode.Administrator) || (userInfo.UserId == batteryInfoModel.OwnerId)))
                {
                    result = -1; // 編輯失敗
                }
                else
                {
                    BatteryInfo bi = db.BatteryInfo.Find(batteryInfoModel.BatteryId, batteryInfoModel.ManufactureDate, batteryInfoModel.DeviceTypeId);

                    BatteryType bt = db.BatteryType.Find(batteryInfoModel.DeviceTypeId);
                    if (bi != null && bt != null)
                    {
                        if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                        {
                            //bi.BatteryId = batteryInfoModel.BatteryId;
                            //bi.ManufactureDate = batteryInfoModel.ManufactureDate;
                            //bi.DeviceTypeId = batteryInfoModel.DeviceTypeId;

                            bi.StatusCode = batteryInfoModel.StatusCode;
                            bi.OwnerId = batteryInfoModel.OwnerId;
                        }

                        db.Entry(bi).State = System.Data.Entity.EntityState.Modified;

                        db.SaveChanges();
                        result = 1; // 編輯成功
                    }
                    else
                    {
                        result = -1; // 編輯失敗
                    }
                }
            }
            return result;
        }

        [HttpGet]
        public List<GroupInfoModel> GetGroupInfos()
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;

            if (userInfo != null)
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    return (from gi in db.GroupInfo
                            select new GroupInfoModel()
                            {
                                GroupId = gi.GroupId,
                                GroupName = gi.GroupName,
                                StatusCode = gi.StatusCode,
                                RelationRoleCode = Models.RelationRoleCode.Manager
                            }).ToList();

                }
                else
                {
                    return (from gi in db.GroupInfo
                            join ugr in db.GroupUserInfo on gi.GroupId equals ugr.GroupId
                            where ugr.UserId == userInfo.UserId && ugr.RelationRoleCode == RelationRoleCode.Manager
                            select new GroupInfoModel()
                            {
                                GroupId = gi.GroupId,
                                GroupName = gi.GroupName,
                                ParentGroupId = gi.ParentGroupId,
                                StatusCode = gi.StatusCode,
                                Description = gi.Description,
                                RegisterDateTime = gi.RegisterDateTime,
                                RelationRoleCode = ugr.RelationRoleCode
                            }).ToList();
                }

            }

            return null;
        }

        [HttpPost]
        public int CreateGroupInfo(GroupInfoModel groupInfoModel)
        {
            int result = 0; // 比對錯誤
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            if (userInfo != null && groupInfoModel != null && groupInfoModel.SecurityKey != null && groupInfoModel.SecurityKey.Equals(securityKey))
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    GroupInfo gi = db.GroupInfo.Find(groupInfoModel.GroupId);

                    if (gi == null)
                    {
                        gi = new GroupInfo()
                        {
                            GroupName = groupInfoModel.GroupName,
                            Description = groupInfoModel.Description,

                            StatusCode = groupInfoModel.StatusCode,
                            ParentGroupId = groupInfoModel.ParentGroupId,
                            RegisterDateTime = DateTime.UtcNow
                        };

                        db.GroupInfo.Add(gi);

                        db.SaveChanges();
                        result = 1; //新增成功
                    }
                    else
                    {
                        result = -1; // 新增失敗
                    }
                }
            }

            return result;
        }

        [HttpPost]
        public int EditGroupInfo(GroupInfoModel groupInfoModel)
        {
            int result = 0; // 比對錯誤
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            if (userInfo != null && groupInfoModel != null && groupInfoModel.SecurityKey != null && groupInfoModel.SecurityKey.Equals(securityKey))
            {
                if (!((userInfo.UserRoleCode == UserRoleCode.Administrator) || (groupInfoModel.RelationRoleCode == RelationRoleCode.Manager)))
                {
                    result = -1; // 編輯失敗
                }
                else
                {
                    GroupInfo gi = db.GroupInfo.Find(groupInfoModel.GroupId);

                    if (gi == null)
                    {
                        if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                        {
                            gi.GroupName = groupInfoModel.GroupName;
                            gi.Description = groupInfoModel.Description;

                            gi.StatusCode = groupInfoModel.StatusCode;
                            gi.ParentGroupId = groupInfoModel.ParentGroupId;
                        }
                        else if (groupInfoModel.RelationRoleCode == RelationRoleCode.Manager)
                        {
                            gi.GroupName = groupInfoModel.GroupName;
                            gi.Description = groupInfoModel.Description;
                        }

                        db.Entry(gi).State = System.Data.Entity.EntityState.Modified;

                        db.SaveChanges();
                        result = 1; // 編輯成功
                    }
                    else
                    {
                        result = -1; // 編輯失敗
                    }
                }
            }
            return result;
        }

        [HttpPost]
        public object GetGroupUserInfosByGroupInfo(GroupRelationInfo groupRelationInfo)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            if (userInfo != null && groupRelationInfo != null && groupRelationInfo.SecurityKey != null && groupRelationInfo.SecurityKey.Equals(securityKey))
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    return (from gui in db.GroupUserInfo
                            where gui.GroupId == groupRelationInfo.GroupId
                            select new
                            {
                                GroupId = gui.GroupId,
                                UserId = gui.UserId,
                                UserName = gui.UserInfo.UserName,
                                RelationRoleCode = gui.RelationRoleCode
                            }).ToList();
                }
                else
                {
                    if (groupRelationInfo.RelationRoleCode == RelationRoleCode.Manager)
                    {
                        return (from gui in db.GroupUserInfo
                                where gui.GroupId == groupRelationInfo.GroupId
                                select new
                                {
                                    GroupId = gui.GroupId,
                                    UserId = gui.UserId,
                                    UserName = gui.UserInfo.UserName,
                                    RelationRoleCode = gui.RelationRoleCode
                                }).ToList();
                    }
                }
            }

            return null;
        }

        [HttpPost]
        public object GetGroupCardInfosByGroupInfo(GroupRelationInfo groupRelationInfo)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            if (userInfo != null && groupRelationInfo != null && groupRelationInfo.SecurityKey != null && groupRelationInfo.SecurityKey.Equals(securityKey))
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    return (from gci in db.GroupCardInfo
                            where gci.GroupId == groupRelationInfo.GroupId
                            select new
                            {
                                GroupId = gci.GroupId,
                                UserId = gci.UserId,
                                UserName = gci.UserInfo.UserName,
                                CardId = gci.CardId
                            }).ToList();
                }
                else
                {
                    if (groupRelationInfo.RelationRoleCode == RelationRoleCode.Manager)
                    {
                        return (from gci in db.GroupCardInfo
                                where gci.GroupId == groupRelationInfo.GroupId
                                select new
                                {
                                    GroupId = gci.GroupId,
                                    UserId = gci.UserId,
                                    UserName = gci.UserInfo.UserName,
                                    CardId = gci.CardId
                                }).ToList();
                    }
                }
            }

            return null;
        }

        [HttpPost]
        public object GetGroupChargerInfosByGroupInfo(GroupRelationInfo groupRelationInfo)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            if (userInfo != null && groupRelationInfo != null && groupRelationInfo.SecurityKey != null && groupRelationInfo.SecurityKey.Equals(securityKey))
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    return (from gci in db.GroupChargeDeviceInfo
                            where gci.GroupId == groupRelationInfo.GroupId
                            select new
                            {
                                GroupId = gci.GroupId,
                                Name = gci.ChargeDeviceInfo.Name,
                                DeviceId = gci.DeviceId,
                                //StationManufactureDate = gci.StationManufactureDate,
                                DeviceTypeId = gci.DeviceTypeId
                            }).ToList();
                }
                else
                {
                    if (groupRelationInfo.RelationRoleCode == RelationRoleCode.Manager)
                    {
                        return (from gci in db.GroupChargeDeviceInfo
                                where gci.GroupId == groupRelationInfo.GroupId
                                select new
                                {
                                    GroupId = gci.GroupId,
                                    Name = gci.ChargeDeviceInfo.Name,
                                    DeviceId = gci.DeviceId,
                                    //StationManufactureDate = gci.StationManufactureDate,
                                    DeviceTypeId = gci.DeviceTypeId
                                }).ToList();
                    }
                }
            }

            return null;
        }

        [HttpPost]
        public object GetGroupBatteryInfosByGroupInfo(GroupInfoModel groupInfoModel)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            if (userInfo != null && groupInfoModel != null && groupInfoModel.SecurityKey != null && groupInfoModel.SecurityKey.Equals(securityKey))
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator || groupInfoModel.RelationRoleCode == RelationRoleCode.Manager)
                {
                    return (from bi in db.BatteryInfo
                            join gbi in db.GroupBatteryInfo on new { bi.BatteryId, bi.ManufactureDate, bi.DeviceTypeId } equals new { gbi.BatteryId, gbi.ManufactureDate, gbi.DeviceTypeId }
                            where gbi.GroupId == groupInfoModel.GroupId
                            select new BatteryInfoModel()
                            {
                                BatteryId = bi.BatteryId,
                                ManufactureDate = bi.ManufactureDate,
                                DeviceTypeId = bi.DeviceTypeId,
                                StatusCode = bi.StatusCode,
                                LastUpdateTime = bi.LastUpdateTime,
                                RegisterDateTime = bi.RegisterDateTime,
                                APVersion = bi.APVersion,
                                Description = bi.Description
                            }).ToList();
                }
            }

            return null;
        }

        [HttpPost]
        public int SetGroupCardInfo(GroupCardInfoModel model)
        {
            int result = 0; // 比對錯誤
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            if (userInfo != null && model != null && model.SecurityKey != null && model.SecurityKey.Equals(securityKey))
            {
                GroupCardInfo ci = db.GroupCardInfo.Find(model.CardId);

                if (ci != null)
                {
                    ci.UserId = model.UserId;
                    ci.GroupId = model.GroupId;

                    ci.StatusCode = model.StatusCode;
                    ci.DeviceOwnerTypeCode = model.DeviceOwnerTypeCode;

                    db.Entry(ci).State = System.Data.Entity.EntityState.Modified;

                    db.SaveChanges();
                    result = 2; // 新增成功
                }
                else
                {
                    ci = new GroupCardInfo();
                    ci.CardId = model.CardId;
                    ci.UserId = model.UserId;
                    ci.GroupId = model.GroupId;
                    ci.StatusCode = model.StatusCode;
                    ci.DeviceOwnerTypeCode = model.DeviceOwnerTypeCode;
                    ci.RegisterDateTime = DateTime.UtcNow;

                    db.GroupCardInfo.Add(ci);

                    db.SaveChanges();
                    result = 1; // 新增成功
                }
            }

            return result;
        }

        [HttpPost]
        public object GetBatteryChargedRecord(BatteryInfoModel req)
        {
            var session = System.Web.HttpContext.Current.Session;
            UserInfo userInfo = session["UserInfo"] as UserInfo;
            string securityKey = session["SecurityKey"] as string;

            if (userInfo != null && req != null && req.SecurityKey != null && req.SecurityKey.Equals(securityKey))
            {
                return (from ctr in db.ChargeTransRecord
                        join ctdr in db.ChargeTransDeviceRecord on ctr.StartChargeTransDeviceRecordIndex equals ctdr.ChargeTransDeviceRecordIndex
                        where ctdr.BatteryId == req.BatteryId && ctdr.ManufactureDate == req.ManufactureDate
                        orderby ctr.ChargeTransTimeStamp descending
                        select ctr).ToList();
            }

            return null;
        }
    }
}
