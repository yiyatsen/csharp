﻿using GTEMemberSystem.Models;
using GTEMemberSystem.Models.DBConfigure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GTEMemberSystem.Controllers.API
{
    public class DeviceUploadController : ApiController
    {
        GTEDMSDBContext db = new GTEDMSDBContext();

        //[HttpPost]
        //public ServerResponseFormat UploadOldData(DeviceUploadFormat value)
        //{
        //    ServerResponseFormat resultFormat = new ServerResponseFormat();
        //    resultFormat.ActionCode = ResponseActionCode.UploadFail;
        //    string message = value.Message;

        //    //string deviceId = "121201" + value.StationId;
        //    // "12 + value.StationManufactureDate +  value.StationId";

        //    var device = (from a in db.ChargeDeviceInfo
        //                  where a.DeviceId.Equals(value.DeviceId) && a.DeviceTypeId.Equals(value.DeviceTypeId)
        //                  select a).SingleOrDefault();

        //    //解密資料

        //    if (device != null)
        //    {
        //        try
        //        {
        //            switch (value.ActionCode)
        //            {
        //                case UploadActionCode.ChargeTransRecord: // Upload Charge Trans Record
        //                    {
        //                        List<ChargeTransRecord> data = null;
        //                        using (TextReader file = new StringReader(message))
        //                        {
        //                            JsonSerializer serializer = new JsonSerializer();
        //                            data = (List<ChargeTransRecord>)serializer.Deserialize(file, typeof(List<ChargeTransRecord>));
        //                        }

        //                        if (data != null)
        //                        {
        //                            foreach (ChargeTransRecord ctr in data)
        //                            {
        //                                ctr.DeviceId = device.DeviceId;
        //                                //ctr.StationManufactureDate = device.StationManufactureDate;
        //                                ctr.DeviceTypeId = device.DeviceTypeId;

        //                                var chargeTransInfo = (from a in db.ChargeTransRecord
        //                                                       where a.ChargeTransTimeStamp.Equals(ctr.ChargeTransTimeStamp) && a.DeviceId.Equals(ctr.DeviceId)
        //                                                        //&& a.StationManufactureDate.Equals(ctr.StationManufactureDate)
        //                                                       && a.DeviceTypeId.Equals(ctr.DeviceTypeId) && a.SlotId.Equals(ctr.SlotId)
        //                                                       select a).SingleOrDefault();
        //                                if (chargeTransInfo == null)
        //                                {
        //                                    ChargeTransDeviceRecord sctdr = ctr.StartChargeTransDeviceRecord;
        //                                    ctr.StartChargeTransDeviceRecord = null;
        //                                    ChargeTransDeviceRecord ectdr = ctr.EndChargeTransDeviceRecord;
        //                                    ctr.EndChargeTransDeviceRecord = null;

        //                                    db.ChargeTransRecord.Add(ctr);
        //                                    db.SaveChanges();

        //                                    if (sctdr != null || ectdr != null)
        //                                    {
        //                                        bool isUpdate = false;
        //                                        BatteryType bt = null;

        //                                        if (ectdr != null)
        //                                        {
        //                                            ectdr.ChargeTransTimeStamp = ctr.ChargeTransTimeStamp;
        //                                            ectdr.DeviceId = ctr.DeviceId;
        //                                            //ectdr.StationManufactureDate = ctr.StationManufactureDate;
        //                                            ectdr.DeviceTypeId = ctr.DeviceTypeId;
        //                                            ectdr.SlotId = ctr.SlotId;

        //                                            db.ChargeTransDeviceRecord.Add(ectdr);
        //                                            ctr.EndChargeTransDeviceRecord = ectdr;

        //                                            BatteryInfo bi = db.BatteryInfo.Find(ectdr.BatteryId, ectdr.ManufactureDate);
        //                                            if (bi == null)
        //                                            {
        //                                                bt = db.BatteryType.Find(ctr.BatteryTypeId);
        //                                                bi = new BatteryInfo();
        //                                                if (bt == null)
        //                                                {
        //                                                    bi.BatteryId = ectdr.BatteryId;
        //                                                    bi.ManufactureDate = ectdr.ManufactureDate;
        //                                                    bi.DeviceTypeId = ctr.BatteryTypeId;
        //                                                    bi.OwnerId = 1;
        //                                                    bi.RegisterDateTime = DateTime.UtcNow;
        //                                                    bi.StatusCode = Models.StatusCode.Useable;
        //                                                    bi.LastUpdateTime = ectdr.ChargeTransTimeStamp;
        //                                                }
        //                                                else
        //                                                {
        //                                                    bi.BatteryId = ectdr.BatteryId;
        //                                                    bi.ManufactureDate = ectdr.ManufactureDate;
        //                                                    bi.DeviceTypeId = ctr.BatteryTypeId;
        //                                                    bi.OwnerId = 1;
        //                                                    bi.RegisterDateTime = DateTime.UtcNow;
        //                                                    bi.StatusCode = Models.StatusCode.Useable;
        //                                                    bi.LastUpdateTime = ectdr.ChargeTransTimeStamp;
        //                                                    bi.SOC = ectdr.SOC;
        //                                                    bi.SOH = Convert.ToInt32(ectdr.FCC * 100 / Convert.ToInt64(bt.DesignCapacity));
        //                                                    if (bi.SOH > 100)
        //                                                    {
        //                                                        bi.SOH = 100;
        //                                                    }
        //                                                }

        //                                                db.BatteryInfo.Add(bi);
        //                                                db.SaveChanges();
        //                                                isUpdate = true;
        //                                            }
        //                                            else
        //                                            {
        //                                                bt = db.BatteryType.Find(bi.DeviceTypeId);
        //                                                if (bt != null)
        //                                                {
        //                                                    ctr.BatteryTypeId = bt.DeviceTypeId;

        //                                                    if (bi.LastUpdateTime < ectdr.ChargeTransTimeStamp && bt.DesignCapacity > 0)
        //                                                    {
        //                                                        bi.LastUpdateTime = ectdr.ChargeTransTimeStamp;
        //                                                        bi.SOC = ectdr.SOC;
        //                                                        bi.SOH = Convert.ToInt32(ectdr.FCC * 100 / Convert.ToInt64(bt.DesignCapacity));
        //                                                        if (bi.SOH > 100)
        //                                                        {
        //                                                            bi.SOH = 100;
        //                                                        }
        //                                                        db.Entry(bi).State = System.Data.Entity.EntityState.Modified;
        //                                                        isUpdate = true;
        //                                                    }
        //                                                }
        //                                            }
        //                                        }

        //                                        if (sctdr != null)
        //                                        {
        //                                            sctdr.ChargeTransTimeStamp = ctr.ChargeTransTimeStamp;
        //                                            sctdr.DeviceId = ctr.DeviceId;
        //                                            //sctdr.StationManufactureDate = ctr.StationManufactureDate;
        //                                            sctdr.DeviceTypeId = ctr.DeviceTypeId;
        //                                            sctdr.SlotId = ctr.SlotId;

        //                                            db.ChargeTransDeviceRecord.Add(sctdr);
        //                                            ctr.StartChargeTransDeviceRecord = sctdr;

        //                                            if (!isUpdate)
        //                                            {
        //                                                BatteryInfo bi = db.BatteryInfo.Find(sctdr.BatteryId, sctdr.ManufactureDate);
        //                                                if (bi == null)
        //                                                {
        //                                                    bt = db.BatteryType.Find(ctr.BatteryTypeId);
        //                                                    bi = new BatteryInfo();
        //                                                    if (bt == null)
        //                                                    {                                                               
        //                                                        bi.BatteryId = sctdr.BatteryId;
        //                                                        bi.ManufactureDate = sctdr.ManufactureDate;
        //                                                        bi.DeviceTypeId = ctr.BatteryTypeId;
        //                                                        bi.OwnerId = 1;
        //                                                        bi.RegisterDateTime = DateTime.UtcNow;
        //                                                        bi.StatusCode = Models.StatusCode.Useable;
        //                                                        bi.LastUpdateTime = sctdr.ChargeTransTimeStamp;
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        bi.BatteryId = sctdr.BatteryId;
        //                                                        bi.ManufactureDate = sctdr.ManufactureDate;
        //                                                        bi.DeviceTypeId = ctr.BatteryTypeId;
        //                                                        bi.OwnerId = 1;
        //                                                        bi.RegisterDateTime = DateTime.UtcNow;
        //                                                        bi.StatusCode = Models.StatusCode.Useable;
        //                                                        bi.LastUpdateTime = sctdr.ChargeTransTimeStamp;
        //                                                        bi.SOC = sctdr.SOC;
        //                                                        bi.SOH = Convert.ToInt32(sctdr.FCC * 100 / Convert.ToInt64(bt.DesignCapacity));
        //                                                        if (bi.SOH > 100)
        //                                                        {
        //                                                            bi.SOH = 100;
        //                                                        }
        //                                                    }

        //                                                    db.BatteryInfo.Add(bi);
        //                                                    db.SaveChanges();
        //                                                    isUpdate = true;
        //                                                }
        //                                                else
        //                                                {
        //                                                    bt = db.BatteryType.Find(bi.DeviceTypeId);
        //                                                    if (bt != null)
        //                                                    {
        //                                                        ctr.BatteryTypeId = bt.DeviceTypeId;

        //                                                        if (bi.LastUpdateTime < sctdr.ChargeTransTimeStamp && bt.DesignCapacity > 0)
        //                                                        {
        //                                                            bi.LastUpdateTime = sctdr.ChargeTransTimeStamp;
        //                                                            bi.SOC = sctdr.SOC;
        //                                                            bi.SOH = Convert.ToInt32(sctdr.FCC * 100 / Convert.ToInt64(bt.DesignCapacity));
        //                                                            if (bi.SOH > 100)
        //                                                            {
        //                                                                bi.SOH = 100;
        //                                                            }
        //                                                            db.Entry(bi).State = System.Data.Entity.EntityState.Modified;
        //                                                            isUpdate = true;
        //                                                        }
        //                                                    }
        //                                                }
        //                                            }
        //                                        }

        //                                        db.Entry(ctr).State = System.Data.Entity.EntityState.Modified;
        //                                        db.SaveChanges();
        //                                    }
        //                                }
        //                            }

        //                            resultFormat.ActionCode = ResponseActionCode.UploadSuccess;
        //                            resultFormat.Message = "Success";
        //                        }
        //                        break;

        //                    }

        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            resultFormat.ActionCode = ResponseActionCode.UploadFail;
        //            resultFormat.Message = "Error Message";
        //        }
        //    }

        //    return resultFormat;
        //}

        [HttpPost]
        public ServerResponseFormat UploadData(DeviceUploadFormat value)
        {
            ServerResponseFormat resultFormat = new ServerResponseFormat();
            resultFormat.ActionCode = ResponseActionCode.UploadFail;
            string message = value.Message;

            string deviceId = value.DeviceId;
            if (value.DeviceId != null)
            {
                deviceId = value.DeviceId.Replace("\"","");
            }

            var device = (from a in db.ChargeDeviceInfo
                          where a.DeviceId.Equals(deviceId) && a.DeviceTypeId.Equals(value.DeviceTypeId) //&& a.StationManufactureDate.Equals(value.StationManufactureDate)
                          select a).SingleOrDefault();

            //解密資料

            if (device != null)
            {
                try
                {
                    switch (value.ActionCode)
                    {
                        case UploadActionCode.DeviceLogin: // Device Online
                            {
                                LoginFormat data = null;
                                using (TextReader file = new StringReader(message))
                                {
                                    JsonSerializer serializer = new JsonSerializer();
                                    data = (LoginFormat)serializer.Deserialize(file, typeof(LoginFormat));
                                }

                                if (data != null && device.Password.Equals(data.Password))
                                {
                                    device.LastLoginTime = DateTime.UtcNow;
                                    device.SecurityKey = data.Password;

                                    db.Entry(device).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();

                                    LoginResponseFormat lrf = new LoginResponseFormat();
                                    lrf.SecurityKey = device.SecurityKey;

                                    resultFormat.ActionCode = ResponseActionCode.LoginSuccess;
                                    resultFormat.Message = Newtonsoft.Json.JsonConvert.SerializeObject(lrf);
                                }

                                break;
                            }
                        case UploadActionCode.ChargeDeviceSlotRecord: // Upload All Slot Record
                            {
                                List<SlotRecord> data = null;
                                using (TextReader file = new StringReader(message))
                                {
                                    JsonSerializer serializer = new JsonSerializer();
                                    data = (List<SlotRecord>)serializer.Deserialize(file, typeof(List<SlotRecord>));
                                }

                                if (data != null)
                                {
                                    foreach (SlotRecord sr in data)
                                    {
                                        //sr.StationId = device.StationId;
                                        //sr.StationManufactureDate = device.StationManufactureDate;
                                        sr.DeviceId = device.DeviceId;
                                        sr.DeviceTypeId = device.DeviceTypeId;

                                        var slotInfo = (from a in db.SlotRecord
                                                        where a.DeviceId.Equals(sr.DeviceId) && a.DeviceTypeId.Equals(sr.DeviceTypeId) && a.TimeStamp.Equals(sr.TimeStamp) && a.SlotId.Equals(sr.SlotId)
                                                        select a).SingleOrDefault();

                                        if (slotInfo == null)
                                        {
                                            SlotBatteryRecord sbr = sr.SlotBatteryRecord;
                                            sr.SlotBatteryRecord = null;

                                            db.SlotRecord.Add(sr);
                                            db.SaveChanges();

                                            if (sbr != null)
                                            {
                                                //sbr.StationId = sr.StationId;
                                                //sbr.StationManufactureDate = sr.StationManufactureDate;
                                                sbr.DeviceId = sr.DeviceId;
                                                sbr.DeviceTypeId = sr.DeviceTypeId;
                                                sbr.SlotId = sr.SlotId;
                                                sbr.TimeStamp = sr.TimeStamp;

                                                db.SlotBatteryRecord.Add(sbr);
                                                sr.SlotBatteryRecord = sbr;
                                                db.Entry(sr).State = System.Data.Entity.EntityState.Modified;
                                                db.SaveChanges();
                                            }
                                        }
                                    }

                                    resultFormat.ActionCode = ResponseActionCode.UploadSuccess;
                                    resultFormat.Message = "Success";
                                }

                                break;
                            }
                        case UploadActionCode.TransRecord: // Upload exchange Trans Record
                            {
                                List<TransRecord> data = null;
                                using (TextReader file = new StringReader(message))
                                {
                                    JsonSerializer serializer = new JsonSerializer();
                                    data = (List<TransRecord>)serializer.Deserialize(file, typeof(List<TransRecord>));
                                }

                                if (data != null)
                                {
                                    foreach (TransRecord tr in data)
                                    {
                                        //tr.StationId = device.StationId;
                                        //tr.StationManufactureDate = device.StationManufactureDate;
                                        tr.DeviceId = device.DeviceId;
                                        tr.DeviceTypeId = device.DeviceTypeId;

                                        var transInfo = (from a in db.TransRecord
                                                         where a.TransTimeStamp.Equals(tr.TransTimeStamp) && a.DeviceId.Equals(tr.DeviceId)
                                                         //&& a.StationManufactureDate.Equals(tr.StationManufactureDate)
                                                         && a.DeviceTypeId.Equals(tr.DeviceTypeId) && a.TransRecordId.Equals(tr.TransRecordId)
                                                         select a).SingleOrDefault();
                                        if (transInfo == null)
                                        {
                                            TransDeviceRecord itdr = tr.InsertedTransDevice;
                                            tr.InsertedTransDevice = null;
                                            TransDeviceRecord dtdr = tr.DrawOutTransDevice;
                                            tr.DrawOutTransDevice = null;

                                            db.TransRecord.Add(tr);
                                            db.SaveChanges();

                                            if (itdr != null || dtdr != null)
                                            {
                                                if (itdr != null)
                                                {
                                                    itdr.TransTimeStamp = tr.TransTimeStamp;
                                                    itdr.DeviceId = tr.DeviceId;
                                                    //itdr.StationManufactureDate = tr.StationManufactureDate;
                                                    itdr.DeviceTypeId = tr.DeviceTypeId;
                                                    itdr.TransRecordId = tr.TransRecordId;

                                                    db.TransDeviceRecord.Add(itdr);
                                                    tr.InsertedTransDevice = itdr;


                                                    BatteryInfo bi = db.BatteryInfo.Find(itdr.BatteryId, itdr.ManufactureDate, itdr.BatteryTypeId);
                                                    
                                                    if (bi == null)
                                                    {
                                                        BatteryType bt = db.BatteryType.Find(itdr.BatteryTypeId);
                                                        if (bt != null)
                                                        {
                                                            bi = new BatteryInfo();
                                                            bi.BatteryId = itdr.BatteryId;
                                                            bi.ManufactureDate = itdr.ManufactureDate;
                                                            bi.DeviceTypeId = itdr.BatteryTypeId;
                                                            bi.OwnerId = 1;
                                                            bi.RegisterDateTime = new DateTime(2012, 1, 1);
                                                            bi.StatusCode = Models.StatusCode.Useable;
                                                            bi.LastUpdateTime = new DateTime(2012, 1, 1);
                                                            bi.SOC = itdr.SOC;
                                                            bi.SOH = 100;
                                                            db.BatteryInfo.Add(bi);
                                                            db.SaveChanges();
                                                        }
                                                    }
                                                }

                                                if (dtdr != null)
                                                {
                                                    dtdr.TransTimeStamp = tr.TransTimeStamp;
                                                    dtdr.DeviceId = tr.DeviceId;
                                                    //dtdr.StationManufactureDate = tr.StationManufactureDate;
                                                    dtdr.DeviceTypeId = tr.DeviceTypeId;
                                                    dtdr.TransRecordId = tr.TransRecordId;

                                                    db.TransDeviceRecord.Add(dtdr);
                                                    tr.DrawOutTransDevice = dtdr;

                                                    BatteryInfo bi = db.BatteryInfo.Find(dtdr.BatteryId, dtdr.ManufactureDate, dtdr.BatteryTypeId);

                                                    if (bi == null)
                                                    {
                                                        BatteryType bt = db.BatteryType.Find(dtdr.BatteryTypeId);
                                                        if (bt != null)
                                                        {
                                                            bi = new BatteryInfo();
                                                            bi.BatteryId = dtdr.BatteryId;
                                                            bi.ManufactureDate = dtdr.ManufactureDate;
                                                            bi.DeviceTypeId = dtdr.BatteryTypeId;
                                                            bi.OwnerId = 1;
                                                            bi.RegisterDateTime = new DateTime(2012, 1, 1);
                                                            bi.StatusCode = Models.StatusCode.Useable;
                                                            bi.LastUpdateTime = new DateTime(2012, 1, 1);
                                                            bi.SOC = dtdr.SOC;
                                                            bi.SOH = 100;
                                                            db.BatteryInfo.Add(bi);
                                                            db.SaveChanges();
                                                        }
                                                    }
                                                }

                                                db.Entry(tr).State = System.Data.Entity.EntityState.Modified;
                                                db.SaveChanges();
                                            }
                                        }
                                    }

                                    resultFormat.ActionCode = ResponseActionCode.UploadSuccess;
                                    resultFormat.Message = "Success";
                                }
                                break;
                            }
                        case UploadActionCode.ChargeTransRecord: // Upload Charge Trans Record
                            {
                                List<ChargeTransRecord> data = null;
                                using (TextReader file = new StringReader(message))
                                {
                                    JsonSerializer serializer = new JsonSerializer();
                                    data = (List<ChargeTransRecord>)serializer.Deserialize(file, typeof(List<ChargeTransRecord>));
                                }

                                if (data != null)
                                {
                                    foreach (ChargeTransRecord ctr in data)
                                    {
                                        ctr.DeviceId = device.DeviceId;
                                        //ctr.StationManufactureDate = device.StationManufactureDate;
                                        ctr.DeviceTypeId = device.DeviceTypeId;

                                        var chargeTransInfo = (from a in db.ChargeTransRecord
                                                               where a.ChargeTransTimeStamp.Equals(ctr.ChargeTransTimeStamp) && a.DeviceId.Equals(ctr.DeviceId)
                                                                //&& a.StationManufactureDate.Equals(ctr.StationManufactureDate)
                                                               && a.DeviceTypeId.Equals(ctr.DeviceTypeId) && a.SlotId.Equals(ctr.SlotId)
                                                               select a).SingleOrDefault();
                                        if (chargeTransInfo == null)
                                        {
                                            ChargeTransDeviceRecord sctdr = ctr.StartChargeTransDeviceRecord;
                                            ctr.StartChargeTransDeviceRecord = null;
                                            ChargeTransDeviceRecord ectdr = ctr.EndChargeTransDeviceRecord;
                                            ctr.EndChargeTransDeviceRecord = null;

                                            db.ChargeTransRecord.Add(ctr);
                                            db.SaveChanges();

                                            if (sctdr != null || ectdr != null)
                                            {
                                                bool isUpdate = false;
                                                BatteryType bt = null;

                                                if (ectdr != null)
                                                {
                                                    ectdr.ChargeTransTimeStamp = ctr.ChargeTransTimeStamp;
                                                    ectdr.DeviceId = ctr.DeviceId;
                                                    //ectdr.StationManufactureDate = ctr.StationManufactureDate;
                                                    ectdr.DeviceTypeId = ctr.DeviceTypeId;
                                                    ectdr.SlotId = ctr.SlotId;

                                                    db.ChargeTransDeviceRecord.Add(ectdr);
                                                    ctr.EndChargeTransDeviceRecord = ectdr;

                                                    BatteryInfo bi = db.BatteryInfo.Find(ectdr.BatteryId, ectdr.ManufactureDate, ectdr.BatteryTypeId);

                                                    if (bi != null)
                                                    {
                                                        bt = db.BatteryType.Find(ectdr.BatteryTypeId);
                                                        if (bt != null)
                                                        {
                                                            if (bi.LastUpdateTime < ectdr.ChargeTransTimeStamp && bt.DesignCapacity > 0)
                                                            {
                                                                bi.LastUpdateTime = ectdr.ChargeTransTimeStamp;
                                                                bi.SOC = ectdr.SOC;
                                                                // Fixed by Neil 2016.04.15
                                                                bi.CellVoltDiff = ectdr.CellVoltDiff;
                                                                if (ectdr.SOH > 0)
                                                                {
                                                                    bi.SOH = ectdr.SOH;
                                                                }
                                                                //bi.SOH = Convert.ToInt32(ectdr.FCC * 100 / Convert.ToInt64(bt.DesignCapacity));
                                                                ////
                                                                if (bi.SOH > 100)
                                                                {
                                                                    bi.SOH = 100;
                                                                }                                                               
                                                                db.Entry(bi).State = System.Data.Entity.EntityState.Modified;
                                                                isUpdate = true;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        bt = db.BatteryType.Find(ctr.BatteryTypeId);
                                                        bi = new BatteryInfo();
                                                        if (bt != null)
                                                        {
                                                            bi.BatteryId = ectdr.BatteryId;
                                                            bi.ManufactureDate = ectdr.ManufactureDate;
                                                            bi.DeviceTypeId = ctr.BatteryTypeId;
                                                            bi.OwnerId = 1;
                                                            bi.RegisterDateTime = new DateTime(2012, 1, 1);
                                                            bi.StatusCode = Models.StatusCode.Useable;
                                                            bi.LastUpdateTime = ectdr.ChargeTransTimeStamp;
                                                            bi.SOC = ectdr.SOC;
                                                            // Fixed by Neil 2016.04.15
                                                            bi.CellVoltDiff = ectdr.CellVoltDiff;
                                                            if (ectdr.SOH > 0)
                                                            {
                                                                bi.SOH = ectdr.SOH;
                                                            }
                                                            //bi.SOH = Convert.ToInt32(ectdr.FCC * 100 / Convert.ToInt64(bt.DesignCapacity));
                                                            ////
                                                            if (bi.SOH > 100)
                                                            {
                                                                bi.SOH = 100;
                                                            }
                                                        }

                                                        db.BatteryInfo.Add(bi);
                                                        db.SaveChanges();
                                                        isUpdate = true;
                                                    }
                                                }

                                                if (sctdr != null)
                                                {
                                                    sctdr.ChargeTransTimeStamp = ctr.ChargeTransTimeStamp;
                                                    sctdr.DeviceId = ctr.DeviceId;
                                                    //sctdr.StationManufactureDate = ctr.StationManufactureDate;
                                                    sctdr.DeviceTypeId = ctr.DeviceTypeId;
                                                    sctdr.SlotId = ctr.SlotId;

                                                    db.ChargeTransDeviceRecord.Add(sctdr);
                                                    ctr.StartChargeTransDeviceRecord = sctdr;
                                   
                                                    if (!isUpdate)
                                                    {
                                                        BatteryInfo bi = db.BatteryInfo.Find(sctdr.BatteryId, sctdr.ManufactureDate, sctdr.BatteryTypeId);
                                                        if (bi != null)
                                                        {
                                                            bt = db.BatteryType.Find(sctdr.BatteryTypeId);
                                                            if (bt != null)
                                                            {
                                                                if (bi.LastUpdateTime < sctdr.ChargeTransTimeStamp && bt.DesignCapacity > 0)
                                                                {
                                                                    bi.LastUpdateTime = sctdr.ChargeTransTimeStamp;
                                                                    bi.SOC = sctdr.SOC;
                                                                    // Fixed by Neil 2016.04.15
                                                                    bi.CellVoltDiff = sctdr.CellVoltDiff;
                                                                    if (sctdr.SOH > 0)
                                                                    {
                                                                        bi.SOH = sctdr.SOH;
                                                                    }
                                                                    //bi.SOH = Convert.ToInt32(sctdr.FCC * 100 / Convert.ToInt64(bt.DesignCapacity));
                                                                    ////                                                                   
                                                                    if (bi.SOH > 100)
                                                                    {
                                                                        bi.SOH = 100;
                                                                    }
                                                                    db.Entry(bi).State = System.Data.Entity.EntityState.Modified;
                                                                    isUpdate = true;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            bt = db.BatteryType.Find(ctr.BatteryTypeId);
                                                            bi = new BatteryInfo();
                                                            if (bt != null)
                                                            {
                                                                bi.BatteryId = sctdr.BatteryId;
                                                                bi.ManufactureDate = sctdr.ManufactureDate;
                                                                bi.DeviceTypeId = ctr.BatteryTypeId;
                                                                bi.OwnerId = 1;
                                                                bi.RegisterDateTime = new DateTime(2012, 1, 1);
                                                                bi.StatusCode = Models.StatusCode.Useable;
                                                                bi.LastUpdateTime = sctdr.ChargeTransTimeStamp;
                                                                bi.SOC = sctdr.SOC;
                                                                // Fixed by Neil 2016.04.15
                                                                bi.CellVoltDiff = sctdr.CellVoltDiff;
                                                                if (sctdr.SOH > 0)
                                                                {
                                                                    bi.SOH = sctdr.SOH;
                                                                }
                                                                //bi.SOH = Convert.ToInt32(sctdr.FCC * 100 / Convert.ToInt64(bt.DesignCapacity));
                                                                ////
                                                                if (bi.SOH > 100)
                                                                {
                                                                    bi.SOH = 100;
                                                                }
                                                            }

                                                            db.BatteryInfo.Add(bi);
                                                            db.SaveChanges();
                                                            isUpdate = true;
                                                        }
                                                    }
                                                }

                                                db.Entry(ctr).State = System.Data.Entity.EntityState.Modified;
                                                db.SaveChanges();
                                            }
                                        }
                                    }

                                    resultFormat.ActionCode = ResponseActionCode.UploadSuccess;
                                    resultFormat.Message = "Success";
                                }
                                break;
                            }
                        case UploadActionCode.AbnormalRecord: // Upload abnormal Record
                            {
                                //

                                List<AbnormalRecord> data = null;
                                using (TextReader file = new StringReader(message))
                                {
                                    JsonSerializer serializer = new JsonSerializer();
                                    data = (List<AbnormalRecord>)serializer.Deserialize(file, typeof(List<AbnormalRecord>));
                                }

                                if (data != null)
                                {
                                    foreach (AbnormalRecord ar in data)
                                    {
                                        ar.DeviceId = device.DeviceId;
                                        //ar.StationManufactureDate = device.StationManufactureDate;
                                        ar.DeviceTypeId = device.DeviceTypeId;

                                        var abnormalInfo = (from a in db.AbnormalRecord
                                                            where a.AbnormalTimeStamp.Equals(ar.AbnormalTimeStamp) && a.DeviceId.Equals(ar.DeviceId)
                                                            //&& a.StationManufactureDate.Equals(ar.StationManufactureDate)
                                                            && a.DeviceTypeId.Equals(ar.DeviceTypeId) && a.SlotId.Equals(ar.SlotId)
                                                            select a).SingleOrDefault();
                                        if (abnormalInfo == null)
                                        {
                                            AbnormalDeviceRecord adr = ar.AbnormalDeviceRecord;
                                            ar.AbnormalDeviceRecord = null;

                                            db.AbnormalRecord.Add(ar);
                                            db.SaveChanges();

                                            if (adr != null)
                                            {
                                                adr.AbnormalTimeStamp = ar.AbnormalTimeStamp;
                                                adr.DeviceId = ar.DeviceId;
                                                //adr.StationManufactureDate = ar.StationManufactureDate;
                                                adr.DeviceTypeId = ar.DeviceTypeId;
                                                adr.SlotId = ar.SlotId;

                                                db.AbnormalDeviceRecord.Add(adr);
                                                ar.AbnormalDeviceRecord = adr;
                                                db.Entry(ar).State = System.Data.Entity.EntityState.Modified;
                                                db.SaveChanges();
                                            }
                                        }
                                    }

                                    resultFormat.ActionCode = ResponseActionCode.UploadSuccess;
                                    resultFormat.Message = "Success";
                                }
                                break;
                            }
                    }
                }
                catch (Exception ex)
                {
                    resultFormat.ActionCode = ResponseActionCode.UploadFail;
                    resultFormat.Message = "Error Message";
                }
            }

            return resultFormat;
        }

        [HttpPost]
        public ServerResponseFormat RegisterDevice(RegisterDeviceFormat value)
        {
            ServerResponseFormat resultFormat = new ServerResponseFormat();
            resultFormat.ActionCode = ResponseActionCode.RegisterFail;

            var device = (from a in db.ChargeDeviceInfo
                          where a.DeviceId.Equals(value.DeviceId) && a.DeviceTypeId.Equals(value.DeviceTypeId)
                          select a).SingleOrDefault();

            if (value != null && device != null && device.Password.Equals(value.Password))
            {
                if (device.UUID == null)
                {
                    resultFormat.ActionCode = ResponseActionCode.RegisterSuccess;
                    
                    device.UUID = value.UUID;

                    db.Entry(device).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    if (device.UUID.Equals(value.UUID))
                    {
                        resultFormat.ActionCode = ResponseActionCode.RegisterSuccess;
                    }
                    else
                    {
                        resultFormat.Message = "Illegal Station";
                    }
                }

            }
            else
            {
                resultFormat.Message = "Information Error";
            }


            return resultFormat;
        }

        //[HttpGet]
        //public int GenerationSlotRecord()
        //{
        //    int i = 0;

        //    string stationId = "0000000001";
        //    string stationManufactureDate = "2015/01/01";
        //    int deviceTypeId = 1;

        //    List<SlotRecord> data = new List<SlotRecord>();
        //    DateTime dt = DateTime.UtcNow.AddHours(-5);
        //    data.Add(new SlotRecord()
        //    {
        //        TimeStamp = dt,
        //        StationId = stationId,
        //        StationManufactureDate = stationManufactureDate,
        //        DeviceTypeId = deviceTypeId,
        //        SlotStatusCode = Models.SlotStatusCode.DetectedBattery,
        //        SlotId = 1,
        //        SlotBatteryRecord = new SlotBatteryRecord(){
        //            StationId = stationId,
        //            StationManufactureDate = stationManufactureDate,
        //            DeviceTypeId = deviceTypeId,
        //            SlotId = 1,

        //            BatteryId =  "0000000001",
        //            ManufactureDate = "2015/01/01",
        //            SOC = 3,
        //            Temperature = 29.0,
        //            VMax = 4000,
        //            VMin = 3999,
        //            Voltage = 50000,
        //            Protection = 0,
        //            TimeStamp = dt
        //        }
        //    });

        //    data.Add(new SlotRecord()
        //    {
        //        TimeStamp = dt,
        //        StationId = stationId,
        //        StationManufactureDate = stationManufactureDate,
        //        DeviceTypeId = deviceTypeId,
        //        SlotStatusCode = Models.SlotStatusCode.NonBattery,
        //        SlotId = 2,
        //    });


        //    foreach (SlotRecord sr in data)
        //    {
        //        sr.StationId = stationId;
        //        sr.StationManufactureDate = stationManufactureDate;
        //        sr.DeviceTypeId = deviceTypeId;

        //        var slotInfo = (from a in db.SlotRecord
        //                        where a.StationId.Equals(sr.StationId) && a.StationManufactureDate.Equals(sr.StationManufactureDate) && a.DeviceTypeId.Equals(sr.DeviceTypeId) && a.TimeStamp.Equals(sr.TimeStamp) && a.SlotId.Equals(sr.SlotId)
        //                        select a).SingleOrDefault();

        //        if (slotInfo == null)
        //        {
        //            SlotBatteryRecord sbr = sr.SlotBatteryRecord;
        //            sr.SlotBatteryRecord = null;

        //            db.SlotRecord.Add(sr);
        //            db.SaveChanges();

        //            if (sbr != null)
        //            {
        //                sbr.StationId = sr.StationId;
        //                sbr.StationManufactureDate = sr.StationManufactureDate;
        //                sbr.DeviceTypeId = sr.DeviceTypeId;
        //                sbr.SlotId = sr.SlotId;
        //                sbr.TimeStamp = sr.TimeStamp;

        //                db.SlotBatteryRecord.Add(sbr);
        //                sr.SlotBatteryRecord = sbr;
        //                db.Entry(sr).State = System.Data.Entity.EntityState.Modified;
        //                db.SaveChanges();
        //            }
        //        }
        //    }

        //    return i;
        //}

        //[HttpGet]
        //public int GenerationTransRecord()
        //{
        //    int i = 0;

        //    string stationId = "0000000001";
        //    string stationManufactureDate = "2015/01/01";
        //    int deviceTypeId = 1;

        //    List<TransRecord> data = new List<TransRecord>();
        //    DateTime dt = DateTime.UtcNow.AddHours(-5);
        //    data.Add(new TransRecord()
        //    {
        //        TransTimeStamp = dt,
        //        StationId = stationId,
        //        StationManufactureDate = stationManufactureDate,
        //        DeviceTypeId = deviceTypeId,
        //        TransStatusCode = Models.TransStatusCode.KioskTransSuccess,

        //        InsertedTransDevice = new TransDeviceRecord()
        //        {
        //            TransTimeStamp = dt,
        //            StationId = stationId,
        //            StationManufactureDate = stationManufactureDate,
        //            DeviceTypeId = deviceTypeId,
        //            SlotId = 1,
        //            BatteryId = "0000000001",
        //            ManufactureDate = "2015/01/01",
        //            SOC = 3,
        //            CellsVoltage = "0D850D7F0D830D810D860D870D860D5B0C650C750C5A0CA20CB50C9F",
        //            Protection = 0
        //        },
        //        DrawOutTransDevice = new TransDeviceRecord()
        //        {
        //            TransTimeStamp = dt,
        //            StationId = stationId,
        //            StationManufactureDate = stationManufactureDate,
        //            DeviceTypeId = deviceTypeId,
        //            SlotId = 2,
        //            BatteryId = "0000000002",
        //            ManufactureDate = "2015/01/01",
        //            SOC = 97,
        //            CellsVoltage = "1013101410131013101310191017100A0FF90FF90FF90FF70FF50FF5",
        //            Protection = 0
        //        }
        //    });

        //    dt = DateTime.UtcNow;
        //    data.Add(new TransRecord()
        //    {
        //        TransTimeStamp = dt,
        //        StationId = stationId,
        //        StationManufactureDate = stationManufactureDate,
        //        DeviceTypeId = deviceTypeId,
        //        TransStatusCode = Models.TransStatusCode.KioskTransSuccess,

        //        InsertedTransDevice = new TransDeviceRecord()
        //        {
        //            TransTimeStamp = dt,
        //            StationId = stationId,
        //            StationManufactureDate = stationManufactureDate,
        //            DeviceTypeId = deviceTypeId,
        //            SlotId = 1,
        //            BatteryId = "0000000001",
        //            ManufactureDate = "2015/01/01",
        //            SOC = 3,
        //            CellsVoltage = "0D850D7F0D830D810D860D870D860D5B0C650C750C5A0CA20CB50C9F",
        //            Protection = 0
        //        },
        //        DrawOutTransDevice = new TransDeviceRecord()
        //        {
        //            TransTimeStamp = dt,
        //            StationId = stationId,
        //            StationManufactureDate = stationManufactureDate,
        //            DeviceTypeId = deviceTypeId,
        //            SlotId = 2,
        //            BatteryId = "0000000002",
        //            ManufactureDate = "2015/01/01",
        //            SOC = 97,
        //            CellsVoltage = "1013101410131013101310191017100A0FF90FF90FF90FF70FF50FF5",
        //            Protection = 0
        //        }
        //    });


        //    foreach (TransRecord tr in data)
        //    {
        //        tr.StationId = stationId;
        //        tr.StationManufactureDate = stationManufactureDate;
        //        tr.DeviceTypeId = deviceTypeId;

        //        var transInfo = (from a in db.TransRecord
        //                         where a.TransTimeStamp.Equals(tr.TransTimeStamp) && a.StationId.Equals(tr.StationId)
        //                         && a.StationManufactureDate.Equals(tr.StationManufactureDate)
        //                         && a.DeviceTypeId.Equals(tr.DeviceTypeId) && a.TransRecordId.Equals(tr.TransRecordId)
        //                         select a).SingleOrDefault();
        //        if (transInfo == null)
        //        {
        //            TransDeviceRecord itdr = tr.InsertedTransDevice;
        //            tr.InsertedTransDevice = null;
        //            TransDeviceRecord dtdr = tr.DrawOutTransDevice;
        //            tr.DrawOutTransDevice = null;

        //            db.TransRecord.Add(tr);
        //            db.SaveChanges();

        //            if (itdr != null || dtdr != null)
        //            {
        //                if (itdr != null)
        //                {
        //                    itdr.TransTimeStamp = tr.TransTimeStamp;
        //                    itdr.StationId = tr.StationId;
        //                    itdr.StationManufactureDate = tr.StationManufactureDate;
        //                    itdr.DeviceTypeId = tr.DeviceTypeId;
        //                    itdr.TransRecordId = tr.TransRecordId;

        //                    db.TransDeviceRecord.Add(itdr);
        //                    tr.InsertedTransDevice = itdr;
        //                }

        //                if (dtdr != null)
        //                {
        //                    dtdr.TransTimeStamp = tr.TransTimeStamp;
        //                    dtdr.StationId = tr.StationId;
        //                    dtdr.StationManufactureDate = tr.StationManufactureDate;
        //                    dtdr.DeviceTypeId = tr.DeviceTypeId;
        //                    dtdr.TransRecordId = tr.TransRecordId;

        //                    db.TransDeviceRecord.Add(dtdr);
        //                    tr.DrawOutTransDevice = dtdr;
        //                }

        //                db.Entry(tr).State = System.Data.Entity.EntityState.Modified;
        //                db.SaveChanges();
        //            }
        //        }
        //    }

        //    return i;
        //}
    }
}
