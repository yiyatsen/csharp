﻿using GTEMemberSystem.Models;
using GTEMemberSystem.Models.DBConfigure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GTEMemberSystem.Controllers.API
{
    public class AccountApiController : ApiController
    {
        GTEDMSDBContext db = new GTEDMSDBContext();

        [HttpPost]
        public object GetUserChargeDeviceInfo(AccountRequest request)
        {
            var userInfo = (from a in db.UserInfo
                            where a.Account.Equals(request.Account) && a.Password.Equals(request.Password)
                            select a).SingleOrDefault();

            if (userInfo != null)
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    return (from cdi in db.ChargeDeviceInfo
                            where cdi.StatusCode != StatusCode.Remove
                            select new
                            {
                                DeviceId = cdi.DeviceId,
                                //StationManufactureDate = cdi.StationManufactureDate,
                                DeviceTypeId = cdi.DeviceTypeId,

                                Name = cdi.Name,
                                Address = cdi.Address,
                                StatusCode = cdi.StatusCode,
                                Latitude = cdi.Latitude,
                                Longitude = cdi.Longitude,
                                APVersion = cdi.APVersion,
                                OSVersion = cdi.OSVersion,
                                SlotNumber = cdi.SlotNumber,
                                Description = cdi.Description
                            }).ToList();
                }
                else
                {
                    return (from cdi in db.ChargeDeviceInfo
                            join gci in db.GroupChargeDeviceInfo on new { cdi.DeviceId, cdi.DeviceTypeId } equals new { gci.DeviceId, gci.DeviceTypeId }
                            join gui in db.GroupUserInfo on gci.GroupId equals gui.GroupId
                            where (gui.UserId == userInfo.UserId && gui.RelationRoleCode == RelationRoleCode.Manager) || (cdi.OwnerId == userInfo.UserId)
                            select new
                            {
                                DeviceId = cdi.DeviceId,
                                //StationManufactureDate = cdi.StationManufactureDate,
                                DeviceTypeId = cdi.DeviceTypeId,

                                Name = cdi.Name,
                                Address = cdi.Address,
                                StatusCode = cdi.StatusCode,
                                Latitude = cdi.Latitude,
                                Longitude = cdi.Longitude,
                                APVersion = cdi.APVersion,
                                OSVersion = cdi.OSVersion,
                                SlotNumber = cdi.SlotNumber,
                                Description = cdi.Description
                            }).Distinct().ToList();
                }
            }

            return null;
        }

        [HttpPost]
        public List<CurrentChargeDeviceSlotInfo> GetUserChargerSlotInfo(AccountRequest request)
        {
            var userInfo = (from a in db.UserInfo
                            where a.Account.Equals(request.Account) && a.Password.Equals(request.Password)
                            select a).SingleOrDefault();

            if (userInfo != null)
            {
                if (userInfo.UserRoleCode == UserRoleCode.Administrator)
                {
                    var data = (from sr in db.SlotRecord
                                group sr by
                                new { StationId = sr.DeviceId, DeviceTypeId = sr.DeviceTypeId, TimeStamp = sr.TimeStamp } into srg
                                orderby srg.Key.TimeStamp descending
                                let res = srg.FirstOrDefault()
                                where res != null
                                select new CurrentChargeDeviceSlotInfo()
                                {
                                    TimeStamp = res.TimeStamp,
                                    DeviceId = res.DeviceId,
                                    //StationManufactureDate = res.StationManufactureDate,
                                    DeviceTypeId = res.DeviceTypeId,

                                    SlotStatusCode = res.SlotStatusCode,
                                    SlotId = res.SlotId,
                                    SlotBatteryRecord = res.SlotBatteryRecord,
                                    Description = res.Description
                                }).Distinct();

                    return data.ToList();
                }
                else
                {
                    var data = (from cdi in db.ChargeDeviceInfo
                                join gi in db.GroupInfo on cdi.OwnerId equals gi.GroupId
                                join ugr in db.GroupUserInfo on gi.GroupId equals ugr.GroupId
                                join sr in db.SlotRecord on new { cdi.DeviceId, cdi.DeviceTypeId } equals new { sr.DeviceId, sr.DeviceTypeId }
                                //where (sr.StationId == cdi.StationId && sr.StationManufactureDate == cdi.StationManufactureDate && sr.DeviceTypeId == cdi.DeviceTypeId) && ((ugr.UserId == userInfo.UserId && cdi.DeviceOwnerTypeCode == DeviceOwnerTypeCode.Group) || (cdi.DeviceOwnerTypeCode == DeviceOwnerTypeCode.User && cdi.OwnerId == userInfo.UserId))
                                group sr by
                                new { StationId = sr.DeviceId, DeviceTypeId = sr.DeviceTypeId, TimeStamp = sr.TimeStamp } into srg
                                orderby srg.Key.TimeStamp descending
                                let res = srg.FirstOrDefault()
                                where res != null
                                select new CurrentChargeDeviceSlotInfo()
                                {
                                    TimeStamp = res.TimeStamp,
                                    DeviceId = res.DeviceId,
                                    //StationManufactureDate = res.StationManufactureDate,
                                    DeviceTypeId = res.DeviceTypeId,

                                    SlotStatusCode = res.SlotStatusCode,
                                    SlotId = res.SlotId,
                                    SlotBatteryRecord = res.SlotBatteryRecord,
                                    Description = res.Description
                                }).Distinct();

                    return data.ToList();
                }
            }
            return null;
        }

        [HttpPost]
        public List<CurrentChargeDeviceSlotInfo> GetUserChargerSlotInfoByCharger(SlotInfoRequestByChargeDevice request)
        {
            var userInfo = (from a in db.UserInfo
                            where a.Account.Equals(request.Account) && a.Password.Equals(request.Password)
                            select a).SingleOrDefault();

            if (userInfo != null && request != null && request.DeviceId != null)
            {
                CurrentChargeDeviceSlotInfo currentStationSlotInfo = (from sr in db.SlotRecord
                                                                      where request.DeviceId.Equals(sr.DeviceId) && sr.DeviceTypeId == request.DeviceTypeId
                                                                 orderby sr.TimeStamp descending
                                                                      select new CurrentChargeDeviceSlotInfo()
                                                                 {
                                                                     TimeStamp = sr.TimeStamp
                                                                 }).FirstOrDefault();
                if (currentStationSlotInfo != null)
                {
                    var data = (from sr in db.SlotRecord
                                where request.DeviceId.Equals(sr.DeviceId) && sr.DeviceTypeId == request.DeviceTypeId
                                && sr.TimeStamp == currentStationSlotInfo.TimeStamp
                                select new CurrentChargeDeviceSlotInfo()
                                {
                                    TimeStamp = sr.TimeStamp,
                                    DeviceId = sr.DeviceId,
                                    //StationManufactureDate = sr.StationManufactureDate,
                                    DeviceTypeId = sr.DeviceTypeId,

                                    SlotStatusCode = sr.SlotStatusCode,
                                    SlotId = sr.SlotId,
                                    SlotBatteryRecord = sr.SlotBatteryRecord,
                                    Description = sr.Description
                                });
                    if (data != null)
                    {
                        return data.ToList();
                    }
                }
            }

            return null;
        }
    }
}
