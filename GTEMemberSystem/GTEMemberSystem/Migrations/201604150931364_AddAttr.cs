namespace GTEMemberSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAttr : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BatteryInfo", "CellVoltDiff", c => c.Int(nullable: false));
            AddColumn("dbo.ChargeTransDeviceRecord", "SOH", c => c.Int(nullable: false));
            AddColumn("dbo.ChargeTransDeviceRecord", "CellVoltDiff", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChargeTransDeviceRecord", "CellVoltDiff");
            DropColumn("dbo.ChargeTransDeviceRecord", "SOH");
            DropColumn("dbo.BatteryInfo", "CellVoltDiff");
        }
    }
}
