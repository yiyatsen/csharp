namespace GTEMemberSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AbnormalDeviceRecord",
                c => new
                    {
                        AbnormalDeviceRecordIndex = c.Long(nullable: false, identity: true),
                        AbnormalTimeStamp = c.DateTime(nullable: false),
                        SlotId = c.Int(nullable: false),
                        DeviceId = c.String(maxLength: 128),
                        DeviceTypeId = c.Int(nullable: false),
                        AbnormalDeviceRecordId = c.Long(nullable: false),
                        BatteryId = c.String(),
                        ManufactureDate = c.String(),
                        BatteryTypeId = c.Int(nullable: false),
                        SOC = c.Int(nullable: false),
                        RC = c.Long(nullable: false),
                        FCC = c.Long(nullable: false),
                        Impedance = c.String(),
                        CellsVoltage = c.String(),
                        CellsTemperature = c.String(),
                        CycleCount = c.Long(nullable: false),
                        Protection = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AbnormalDeviceRecordIndex)
                .ForeignKey("dbo.AbnormalRecord", t => new { t.AbnormalTimeStamp, t.SlotId, t.DeviceId, t.DeviceTypeId })
                .Index(t => new { t.AbnormalTimeStamp, t.SlotId, t.DeviceId, t.DeviceTypeId });
            
            CreateTable(
                "dbo.AbnormalRecord",
                c => new
                    {
                        AbnormalTimeStamp = c.DateTime(nullable: false),
                        SlotId = c.Int(nullable: false),
                        DeviceId = c.String(nullable: false, maxLength: 128),
                        DeviceTypeId = c.Int(nullable: false),
                        AbnormalRecordId = c.Long(nullable: false),
                        AbnormalStatusCode = c.Int(nullable: false),
                        AbnormalDeviceRecordIndex = c.Long(),
                        IsNotification = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AbnormalTimeStamp, t.SlotId, t.DeviceId, t.DeviceTypeId })
                .ForeignKey("dbo.AbnormalDeviceRecord", t => t.AbnormalDeviceRecordIndex)
                .Index(t => t.AbnormalDeviceRecordIndex);
            
            CreateTable(
                "dbo.BatteryInfo",
                c => new
                    {
                        BatteryId = c.String(nullable: false, maxLength: 128),
                        ManufactureDate = c.String(nullable: false, maxLength: 128),
                        DeviceTypeId = c.Int(nullable: false),
                        OwnerId = c.Int(nullable: false),
                        StatusCode = c.Int(nullable: false),
                        RegisterDateTime = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(nullable: false),
                        SOH = c.Int(nullable: false),
                        SOC = c.Int(nullable: false),
                        APVersion = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => new { t.BatteryId, t.ManufactureDate, t.DeviceTypeId });
            
            CreateTable(
                "dbo.GroupBatteryInfo",
                c => new
                    {
                        GroupId = c.Int(nullable: false),
                        BatteryId = c.String(nullable: false, maxLength: 128),
                        ManufactureDate = c.String(nullable: false, maxLength: 128),
                        DeviceTypeId = c.Int(nullable: false),
                        Description = c.String(),
                        StatusCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GroupId, t.BatteryId, t.ManufactureDate, t.DeviceTypeId })
                .ForeignKey("dbo.BatteryInfo", t => new { t.BatteryId, t.ManufactureDate, t.DeviceTypeId }, cascadeDelete: true)
                .ForeignKey("dbo.GroupInfo", t => t.GroupId, cascadeDelete: true)
                .Index(t => t.GroupId)
                .Index(t => new { t.BatteryId, t.ManufactureDate, t.DeviceTypeId });
            
            CreateTable(
                "dbo.GroupInfo",
                c => new
                    {
                        GroupId = c.Int(nullable: false, identity: true),
                        GroupName = c.String(nullable: false),
                        ParentGroupId = c.Int(),
                        RegisterDateTime = c.DateTime(nullable: false),
                        Description = c.String(),
                        OwnerId = c.Int(nullable: false),
                        StatusCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GroupId)
                .ForeignKey("dbo.GroupInfo", t => t.ParentGroupId)
                .Index(t => t.ParentGroupId);
            
            CreateTable(
                "dbo.GroupCardInfo",
                c => new
                    {
                        CardId = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                        GroupId = c.Int(nullable: false),
                        StatusCode = c.Int(nullable: false),
                        DeviceOwnerTypeCode = c.Int(nullable: false),
                        RegisterDateTime = c.DateTime(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.CardId)
                .ForeignKey("dbo.GroupInfo", t => t.GroupId, cascadeDelete: true)
                .ForeignKey("dbo.UserInfo", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.GroupId);
            
            CreateTable(
                "dbo.UserInfo",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Account = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        UserName = c.String(nullable: false),
                        Phone = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        UserRoleCode = c.Int(nullable: false),
                        DefaultGroup = c.Int(nullable: false),
                        Description = c.String(),
                        RegisterDateTime = c.DateTime(nullable: false),
                        RefId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.GroupUserInfo",
                c => new
                    {
                        GroupId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Description = c.String(),
                        RelationRoleCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GroupId, t.UserId })
                .ForeignKey("dbo.GroupInfo", t => t.GroupId, cascadeDelete: true)
                .ForeignKey("dbo.UserInfo", t => t.UserId, cascadeDelete: true)
                .Index(t => t.GroupId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.GroupChargeDeviceInfo",
                c => new
                    {
                        GroupId = c.Int(nullable: false),
                        DeviceId = c.String(nullable: false, maxLength: 128),
                        DeviceTypeId = c.Int(nullable: false),
                        Description = c.String(),
                        StatusCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GroupId, t.DeviceId, t.DeviceTypeId })
                .ForeignKey("dbo.ChargeDeviceInfo", t => new { t.DeviceId, t.DeviceTypeId }, cascadeDelete: true)
                .ForeignKey("dbo.GroupInfo", t => t.GroupId, cascadeDelete: true)
                .Index(t => t.GroupId)
                .Index(t => new { t.DeviceId, t.DeviceTypeId });
            
            CreateTable(
                "dbo.ChargeDeviceInfo",
                c => new
                    {
                        DeviceId = c.String(nullable: false, maxLength: 128),
                        DeviceTypeId = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        UUID = c.String(),
                        BatteryTypeId = c.Int(nullable: false),
                        OwnerId = c.Int(nullable: false),
                        LastOnlineIP = c.String(),
                        LastLoginTime = c.DateTime(nullable: false),
                        SecurityKey = c.String(),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        OSVersion = c.String(),
                        APVersion = c.String(),
                        Address = c.String(),
                        SlotNumber = c.Int(nullable: false),
                        StatusCode = c.Int(nullable: false),
                        RegisterDateTime = c.DateTime(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => new { t.DeviceId, t.DeviceTypeId })
                .ForeignKey("dbo.ChargeDeviceType", t => t.DeviceTypeId, cascadeDelete: true)
                .Index(t => t.DeviceTypeId);
            
            CreateTable(
                "dbo.ChargeDeviceType",
                c => new
                    {
                        DeviceTypeId = c.Int(nullable: false, identity: true),
                        DeviceTypeName = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.DeviceTypeId);
            
            CreateTable(
                "dbo.BatteryType",
                c => new
                    {
                        DeviceTypeId = c.Int(nullable: false, identity: true),
                        DeviceTypeName = c.String(nullable: false),
                        DesignVoltage = c.Int(nullable: false),
                        DesignCapacity = c.Int(nullable: false),
                        DesignCellsNumber = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.DeviceTypeId);
            
            CreateTable(
                "dbo.ChargeTransDeviceRecord",
                c => new
                    {
                        ChargeTransDeviceRecordIndex = c.Long(nullable: false, identity: true),
                        ChargeTransTimeStamp = c.DateTime(nullable: false),
                        SlotId = c.Int(nullable: false),
                        DeviceId = c.String(maxLength: 128),
                        DeviceTypeId = c.Int(nullable: false),
                        ChargeTransDeviceRecordId = c.Long(nullable: false),
                        ChargeTimeStamp = c.DateTime(nullable: false),
                        BatteryId = c.String(),
                        ManufactureDate = c.String(),
                        BatteryTypeId = c.Int(nullable: false),
                        SOC = c.Int(nullable: false),
                        FCC = c.Long(nullable: false),
                        RC = c.Long(nullable: false),
                        Impedance = c.String(),
                        Voltage = c.Long(nullable: false),
                        CellsVoltage = c.String(),
                        CellsTemperature = c.String(),
                        CycleCount = c.Long(nullable: false),
                        Protection = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ChargeTransDeviceRecordIndex)
                .ForeignKey("dbo.ChargeTransRecord", t => new { t.ChargeTransTimeStamp, t.SlotId, t.DeviceId, t.DeviceTypeId })
                .Index(t => new { t.ChargeTransTimeStamp, t.SlotId, t.DeviceId, t.DeviceTypeId });
            
            CreateTable(
                "dbo.ChargeTransRecord",
                c => new
                    {
                        ChargeTransTimeStamp = c.DateTime(nullable: false),
                        SlotId = c.Int(nullable: false),
                        DeviceId = c.String(nullable: false, maxLength: 128),
                        DeviceTypeId = c.Int(nullable: false),
                        ChargeTransStatusCode = c.Int(nullable: false),
                        ChargeTransRecordId = c.Long(nullable: false),
                        BatteryTypeId = c.Int(nullable: false),
                        ChargedCapacity = c.Long(nullable: false),
                        TotalTime = c.Long(nullable: false),
                        StartChargeTransDeviceRecordIndex = c.Long(),
                        EndChargeTransDeviceRecordIndex = c.Long(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => new { t.ChargeTransTimeStamp, t.SlotId, t.DeviceId, t.DeviceTypeId })
                .ForeignKey("dbo.ChargeTransDeviceRecord", t => t.EndChargeTransDeviceRecordIndex)
                .ForeignKey("dbo.ChargeTransDeviceRecord", t => t.StartChargeTransDeviceRecordIndex)
                .Index(t => t.StartChargeTransDeviceRecordIndex)
                .Index(t => t.EndChargeTransDeviceRecordIndex);
            
            CreateTable(
                "dbo.SlotBatteryRecord",
                c => new
                    {
                        SlotBatteryRecordIndex = c.Long(nullable: false, identity: true),
                        TimeStamp = c.DateTime(nullable: false),
                        SlotId = c.Int(nullable: false),
                        DeviceId = c.String(maxLength: 128),
                        DeviceTypeId = c.Int(nullable: false),
                        BatteryId = c.String(),
                        ManufactureDate = c.String(),
                        BatteryTypeId = c.Int(nullable: false),
                        SlotBatteryRecordId = c.Long(nullable: false),
                        SOC = c.Int(nullable: false),
                        RC = c.Long(nullable: false),
                        Temperature = c.Double(nullable: false),
                        Current = c.Int(nullable: false),
                        Voltage = c.Long(nullable: false),
                        VMax = c.Int(nullable: false),
                        VMin = c.Int(nullable: false),
                        Protection = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.SlotBatteryRecordIndex)
                .ForeignKey("dbo.SlotRecord", t => new { t.TimeStamp, t.SlotId, t.DeviceId, t.DeviceTypeId })
                .Index(t => new { t.TimeStamp, t.SlotId, t.DeviceId, t.DeviceTypeId });
            
            CreateTable(
                "dbo.SlotRecord",
                c => new
                    {
                        TimeStamp = c.DateTime(nullable: false),
                        SlotId = c.Int(nullable: false),
                        DeviceId = c.String(nullable: false, maxLength: 128),
                        DeviceTypeId = c.Int(nullable: false),
                        SlotRecordId = c.Long(nullable: false),
                        SlotStatusCode = c.Int(nullable: false),
                        SlotBatteryRecordIndex = c.Long(),
                        SlotBatteryRecordId = c.Long(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => new { t.TimeStamp, t.SlotId, t.DeviceId, t.DeviceTypeId })
                .ForeignKey("dbo.SlotBatteryRecord", t => t.SlotBatteryRecordIndex)
                .Index(t => t.SlotBatteryRecordIndex);
            
            CreateTable(
                "dbo.TransDeviceRecord",
                c => new
                    {
                        TransDeviceRecordIndex = c.Long(nullable: false, identity: true),
                        TransTimeStamp = c.DateTime(nullable: false),
                        DeviceId = c.String(maxLength: 128),
                        DeviceTypeId = c.Int(nullable: false),
                        TransRecordId = c.Long(nullable: false),
                        TransDeviceRecordId = c.Long(nullable: false),
                        TransDeviceTime = c.DateTime(nullable: false),
                        SlotId = c.Int(nullable: false),
                        BatteryId = c.String(),
                        ManufactureDate = c.String(),
                        BatteryTypeId = c.Int(nullable: false),
                        OID = c.String(),
                        UID = c.String(),
                        CID = c.String(),
                        SOC = c.Int(nullable: false),
                        CycleCount = c.Long(nullable: false),
                        CellsVoltage = c.String(),
                        Protection = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.TransDeviceRecordIndex)
                .ForeignKey("dbo.TransRecord", t => new { t.TransTimeStamp, t.DeviceId, t.DeviceTypeId, t.TransRecordId })
                .Index(t => new { t.TransTimeStamp, t.DeviceId, t.DeviceTypeId, t.TransRecordId });
            
            CreateTable(
                "dbo.TransRecord",
                c => new
                    {
                        TransTimeStamp = c.DateTime(nullable: false),
                        DeviceId = c.String(nullable: false, maxLength: 128),
                        DeviceTypeId = c.Int(nullable: false),
                        TransRecordId = c.Long(nullable: false),
                        UserId = c.String(),
                        CardId = c.String(),
                        TransStatusCode = c.Int(nullable: false),
                        OldPoint = c.Int(nullable: false),
                        Point = c.Int(nullable: false),
                        InsertedTransDeviceIndex = c.Long(),
                        InsertedTransDeviceId = c.Long(nullable: false),
                        DrawOutTransDeviceIndex = c.Long(),
                        DrawOutTransDeviceId = c.Long(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => new { t.TransTimeStamp, t.DeviceId, t.DeviceTypeId, t.TransRecordId })
                .ForeignKey("dbo.TransDeviceRecord", t => t.DrawOutTransDeviceIndex)
                .ForeignKey("dbo.TransDeviceRecord", t => t.InsertedTransDeviceIndex)
                .Index(t => t.InsertedTransDeviceIndex)
                .Index(t => t.DrawOutTransDeviceIndex);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TransDeviceRecord", new[] { "TransTimeStamp", "DeviceId", "DeviceTypeId", "TransRecordId" }, "dbo.TransRecord");
            DropForeignKey("dbo.TransRecord", "InsertedTransDeviceIndex", "dbo.TransDeviceRecord");
            DropForeignKey("dbo.TransRecord", "DrawOutTransDeviceIndex", "dbo.TransDeviceRecord");
            DropForeignKey("dbo.SlotBatteryRecord", new[] { "TimeStamp", "SlotId", "DeviceId", "DeviceTypeId" }, "dbo.SlotRecord");
            DropForeignKey("dbo.SlotRecord", "SlotBatteryRecordIndex", "dbo.SlotBatteryRecord");
            DropForeignKey("dbo.ChargeTransDeviceRecord", new[] { "ChargeTransTimeStamp", "SlotId", "DeviceId", "DeviceTypeId" }, "dbo.ChargeTransRecord");
            DropForeignKey("dbo.ChargeTransRecord", "StartChargeTransDeviceRecordIndex", "dbo.ChargeTransDeviceRecord");
            DropForeignKey("dbo.ChargeTransRecord", "EndChargeTransDeviceRecordIndex", "dbo.ChargeTransDeviceRecord");
            DropForeignKey("dbo.GroupBatteryInfo", "GroupId", "dbo.GroupInfo");
            DropForeignKey("dbo.GroupInfo", "ParentGroupId", "dbo.GroupInfo");
            DropForeignKey("dbo.GroupChargeDeviceInfo", "GroupId", "dbo.GroupInfo");
            DropForeignKey("dbo.GroupChargeDeviceInfo", new[] { "DeviceId", "DeviceTypeId" }, "dbo.ChargeDeviceInfo");
            DropForeignKey("dbo.ChargeDeviceInfo", "DeviceTypeId", "dbo.ChargeDeviceType");
            DropForeignKey("dbo.GroupCardInfo", "UserId", "dbo.UserInfo");
            DropForeignKey("dbo.GroupUserInfo", "UserId", "dbo.UserInfo");
            DropForeignKey("dbo.GroupUserInfo", "GroupId", "dbo.GroupInfo");
            DropForeignKey("dbo.GroupCardInfo", "GroupId", "dbo.GroupInfo");
            DropForeignKey("dbo.GroupBatteryInfo", new[] { "BatteryId", "ManufactureDate", "DeviceTypeId" }, "dbo.BatteryInfo");
            DropForeignKey("dbo.AbnormalDeviceRecord", new[] { "AbnormalTimeStamp", "SlotId", "DeviceId", "DeviceTypeId" }, "dbo.AbnormalRecord");
            DropForeignKey("dbo.AbnormalRecord", "AbnormalDeviceRecordIndex", "dbo.AbnormalDeviceRecord");
            DropIndex("dbo.TransRecord", new[] { "DrawOutTransDeviceIndex" });
            DropIndex("dbo.TransRecord", new[] { "InsertedTransDeviceIndex" });
            DropIndex("dbo.TransDeviceRecord", new[] { "TransTimeStamp", "DeviceId", "DeviceTypeId", "TransRecordId" });
            DropIndex("dbo.SlotRecord", new[] { "SlotBatteryRecordIndex" });
            DropIndex("dbo.SlotBatteryRecord", new[] { "TimeStamp", "SlotId", "DeviceId", "DeviceTypeId" });
            DropIndex("dbo.ChargeTransRecord", new[] { "EndChargeTransDeviceRecordIndex" });
            DropIndex("dbo.ChargeTransRecord", new[] { "StartChargeTransDeviceRecordIndex" });
            DropIndex("dbo.ChargeTransDeviceRecord", new[] { "ChargeTransTimeStamp", "SlotId", "DeviceId", "DeviceTypeId" });
            DropIndex("dbo.ChargeDeviceInfo", new[] { "DeviceTypeId" });
            DropIndex("dbo.GroupChargeDeviceInfo", new[] { "DeviceId", "DeviceTypeId" });
            DropIndex("dbo.GroupChargeDeviceInfo", new[] { "GroupId" });
            DropIndex("dbo.GroupUserInfo", new[] { "UserId" });
            DropIndex("dbo.GroupUserInfo", new[] { "GroupId" });
            DropIndex("dbo.GroupCardInfo", new[] { "GroupId" });
            DropIndex("dbo.GroupCardInfo", new[] { "UserId" });
            DropIndex("dbo.GroupInfo", new[] { "ParentGroupId" });
            DropIndex("dbo.GroupBatteryInfo", new[] { "BatteryId", "ManufactureDate", "DeviceTypeId" });
            DropIndex("dbo.GroupBatteryInfo", new[] { "GroupId" });
            DropIndex("dbo.AbnormalRecord", new[] { "AbnormalDeviceRecordIndex" });
            DropIndex("dbo.AbnormalDeviceRecord", new[] { "AbnormalTimeStamp", "SlotId", "DeviceId", "DeviceTypeId" });
            DropTable("dbo.TransRecord");
            DropTable("dbo.TransDeviceRecord");
            DropTable("dbo.SlotRecord");
            DropTable("dbo.SlotBatteryRecord");
            DropTable("dbo.ChargeTransRecord");
            DropTable("dbo.ChargeTransDeviceRecord");
            DropTable("dbo.BatteryType");
            DropTable("dbo.ChargeDeviceType");
            DropTable("dbo.ChargeDeviceInfo");
            DropTable("dbo.GroupChargeDeviceInfo");
            DropTable("dbo.GroupUserInfo");
            DropTable("dbo.UserInfo");
            DropTable("dbo.GroupCardInfo");
            DropTable("dbo.GroupInfo");
            DropTable("dbo.GroupBatteryInfo");
            DropTable("dbo.BatteryInfo");
            DropTable("dbo.AbnormalRecord");
            DropTable("dbo.AbnormalDeviceRecord");
        }
    }
}
