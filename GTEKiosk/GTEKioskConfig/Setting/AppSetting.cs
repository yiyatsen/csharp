﻿using GTEKioskConfig.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.Setting
{
    public class AppSetting
    {
        public string ScreenOrientation { get; set; }
        public string[] ScreenOrientationList { get; set; }

        public string Port { get; set; }
        public int BaudRate { get; set; }
        public int[] BaudRateList { get; set; }

        public string BatteryTypeVersion { get; set; }
        public string[] BatteryTypeVersionList { get; set; }

        public int StartAddress { get; set; }
        public int EndAddress { get; set; }

        public List<ChargeSlotItem> ChargeSlotList { get; set; }

        public string RFIDPort { get; set; }

        public int AdWaitingTime { get; set; }
        public int AdUpdateTime { get; set; }

        public bool IsMute { get; set; }

        public string Lang { get; set; }
        public string[] LangList { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public string ServerHost { get; set; }
        public string ServerPort { get; set; }

        //// TODO
        //public string WebServer { get; set; }
        //public int UpdateTimeForWebServer { get; set; }
        // TODO

        public AppSetting()
        {
            ScreenOrientationList = new string[] { "Landscape", "Portrait" };
            ScreenOrientation = ScreenOrientationList[0];
            BaudRateList = new int[] { 115200, 38400 };
            BaudRate = BaudRateList[0];
            BatteryTypeVersionList = new string[] { "4840", "4840V", "4816" };
            BatteryTypeVersion = BatteryTypeVersionList[0];
            LangList = new string[] { "zh-TW", "en-US" };
            Lang = LangList[0];
            ChargeSlotList = new List<ChargeSlotItem>();

            StartAddress = 0;
            EndAddress = 29;

            AdWaitingTime = 90;
            AdUpdateTime = 5;

            Latitude = 25.0392;
            Longitude = 121.525;

            //ServerHost = "210.61.66.247";
            //ServerPort = "80";
            ServerHost = "localhost";
            ServerPort = "50086";
        }
    }
}
