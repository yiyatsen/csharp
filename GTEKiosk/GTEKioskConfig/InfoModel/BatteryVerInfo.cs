﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class BatteryVerInfo
    {
        public string DeviceName { get; set; }
        public string MCUVer { get; set; }
        public string BatteryId { get; set; }
        public int[] ManufactureDate { get; set; }
    }
}
