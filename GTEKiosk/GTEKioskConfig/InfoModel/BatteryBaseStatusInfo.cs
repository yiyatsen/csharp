﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class BatteryBaseStatusInfo
    {
        public int SOC { get; set; }
        public byte[] Protection { get; set; }
        public int[] CellsVoltage { get; set; }
    }
}
