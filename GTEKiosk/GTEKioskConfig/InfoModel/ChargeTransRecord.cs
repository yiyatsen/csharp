﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class ChargeTransRecord
    {
        public long ChargeTransRecordId { get; set; }
        public string ChargeTransTimeStamp { get; set; }
        public int SlotId { get; set; }
        public long StartChargeTransDeviceRecordId { get; set; }
        public ChargeTransDeviceRecord StartChargeTransDeviceRecord { get; set; }
        public long EndChargeTransDeviceRecordId { get; set; }
        public ChargeTransDeviceRecord EndChargeTransDeviceRecord { get; set; }

        public long ChargedCapacity { get; set; }
        public long TotalTime { get; set; }

        public int BatteryTypeId { get; set; }

        public int ChargeTransStatusCode { get; set; }
        public bool IsUpdate { get; set; }
    }
}
