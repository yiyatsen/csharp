﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class RFIDCardInfo : CardInfo
    {
        public long Point { get; set; }
        public string IssueDateTime { get; set; }
        public string TranDateTime { get; set; }
    }
}
