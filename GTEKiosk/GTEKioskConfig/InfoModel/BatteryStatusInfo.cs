﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class BatteryStatusInfo
    {
        public int SOC { get; set; }
        public int[] CellsVoltage { get; set; }
        public byte[] Protection { get; set; }
        public double[] Temperature { get; set; }
        public int Current { get; set; }
        public long FCC { get; set; }
        public long RC { get; set; }
        public long CycleCount { get; set; }
        public long DC { get; set; } // Added by Neil 2016.04.15
    }
}
