﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public enum SlotAbnormalStatus
    {
        NonBatteryInfo = 0,
        NonChargeFull = 1,
        FullBattery = 2,
        AllSlotShutDown = 3
    }

    public class AbnormalRecord
    {
        public long AbnormalRecordId { get; set; }

        public int SlotId { get; set; }
        public int AbnormalStatusCode { get; set; }

        public string AbnormalTimeStamp { get; set; }

        public long AbnormalDeviceRecordId { get; set; }
        public AbnormalDeviceRecord AbnormalDeviceRecord { get; set; }

        public bool IsUpdate { get; set; }
    }

    public class AbnormalDeviceRecord
    {
        public long AbnormalDeviceRecordId { get; set; }

        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public int BatteryTypeId { get; set; }

        public int SOC { get; set; }
        public long RC { get; set; }
        public long FCC { get; set; }
        public string Impedance { get; set; }
        public string CellsVoltage { get; set; }
        public string CellsTemperature { get; set; }
        public long CycleCount { get; set; }

        public int Protection { get; set; }
    }
}
