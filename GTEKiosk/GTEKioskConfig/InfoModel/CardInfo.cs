﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class CardInfo
    {
        public string CardId { get; set; }
        public string UserId { get; set; }
    }
}
