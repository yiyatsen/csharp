﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public enum UploadActionCode : int
    {
        DeviceLogin = 1,
        ChargeDeviceSlotRecord = 2,
        TransRecord = 3,
        ChargeTransRecord = 4,
        AbnormalRecord = 5,
        RegisterDevice = 999
    }

    public class LoginFormat
    {
        public string Password { get; set; }
    }

    public class LoginResponseFormat
    {
        public string SecurityKey { get; set; }
        public int UpdateCode { get; set; }
    }
}
