﻿using GTE.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class ModuleItem
    {
        public KioskModuleType Type { get; set; }

        public int Address { get; set; }
    }
}
