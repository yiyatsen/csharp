﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class BatteryTransIdInfo
    {
        public string OperatorId { get; set; }
        public string UserId { get; set; }
        public string BatteryId { get; set; }
        public int[] ManufactureDate { get; set; }
        public string CardId { get; set; }
    }
}
