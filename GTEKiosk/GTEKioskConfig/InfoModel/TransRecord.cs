﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class TransRecord
    {
        public long TransRecordId { get; set; }
        public string TransTimeStamp { get; set; }
        public string UserId { get; set; }
        public string CardId { get; set; }
        public int TransStatusCode { get; set; }

        public int OldPoint { get; set; }
        public int Point { get; set; }
        public long InsertedTransDeviceId { get; set; }
        public TransDeviceRecord InsertedTransDevice { get; set; }
        public long DrawOutTransDeviceId { get; set; }
        public TransDeviceRecord DrawOutTransDevice { get; set; }

        public bool IsUpdate { get; set; }

        public TransRecord()
        {
            TransStatusCode = -1;
        }
    }
}
