﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class SlotBatteryRecord
    {
        public long SlotBatteryRecordId { get; set; }
        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public int BatteryTypeId { get; set; }

        public int SOC { get; set; }
        public long RC { get; set; }
        public double Temperature { get; set; }
        public int Current { get; set; }
        public long Voltage { get; set; }
        public int VMax { get; set; }
        public int VMin { get; set; }
        public int Protection { get; set; }
    }
}
