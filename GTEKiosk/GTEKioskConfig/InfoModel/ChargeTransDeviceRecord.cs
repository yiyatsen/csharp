﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class ChargeTransDeviceRecord
    {
        public long ChargeTransDeviceRecordId { get; set; }

        public string ChargeTimeStamp { get; set; }
        
        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public int BatteryTypeId { get; set; }

        public int SOC { get; set; }
        public long FCC { get; set; }
        public long RC { get; set; }
        public string Impedance { get; set; }
        public long Voltage { get; set; }
        // Fixed by Neil 2016.04.15
        //public long VMax { get; set; }
        //public long VMin { get; set; }
        public int SOH { get; set; }
        public int CellVoltDiff { get; set; }
        //
        public string CellsVoltage { get; set; }
        public string CellsTemperature { get; set; }
        public long CycleCount { get; set; }
        public int Protection { get; set; }
    }
}
