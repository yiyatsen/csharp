﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class ChargeSlotItem
    {
        public int SlotId { get; set; }
        public int GateAddress { get; set; }
        public int BatteryAddress { get; set; }
        public bool SlotStatus { get; set; }
    }
}
