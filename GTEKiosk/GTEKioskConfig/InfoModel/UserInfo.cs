﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public enum UerRole
    {
        None = 0,
        User = 1,
        Manager = 2,
        Admin = 16
    }

    public class UserInfo
    {
        public string UserId { get; set; }
        public int UserRole { get; set; }
    }
}
