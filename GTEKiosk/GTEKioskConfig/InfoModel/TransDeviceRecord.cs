﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class TransDeviceRecord
    {
        public long TransDeviceRecordId { get; set; }

        public int SlotId { get; set; }
        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public int BatteryTypeId { get; set; }

        public string TransDeviceTime { get; set; }

        public string OID { get; set; }
        public string UID { get; set; }
        public string CID { get; set; }

        public int SOC { get; set; }
        public long CycleCount { get; set; }
        public string CellsVoltage { get; set; }
        public int Protection { get; set; }
    }
}
