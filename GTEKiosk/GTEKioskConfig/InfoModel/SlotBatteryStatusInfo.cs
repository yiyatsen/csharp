﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class SlotBatteryStatusInfo
    {
        public bool isShow { get; set; }
        public string SlotId { get; set; }
        public bool IsExistBattery { get; set; }
        public int EnableAlarmCode { get; set; }
        public string SOC { get; set; }
        public string Current { get; set; }
        public string Voltage { get; set; }
        public string VMax { get; set; }
        public string VMin { get; set; }
        public string VDiff { get; set; }
    }
}
