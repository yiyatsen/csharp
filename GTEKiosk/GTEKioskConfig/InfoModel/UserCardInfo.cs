﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class UserCardInfo : CardInfo
    {
        public int UserRole { get; set; }
        public int StatusCode { get; set; } // 0: normal , 1: black, 2: white
    }
}
