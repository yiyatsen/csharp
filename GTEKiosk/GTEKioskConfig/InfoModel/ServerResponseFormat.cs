﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class ServerResponseFormat
    {
        public int ActionCode { get; set; }
        public string Message { get; set; }
    }
}
