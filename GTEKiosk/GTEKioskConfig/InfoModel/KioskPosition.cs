﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class KioskPosition
    {
        public string ip { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}
