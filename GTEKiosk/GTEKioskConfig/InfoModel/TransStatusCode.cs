﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public enum TransStatusCode : int
    {
        KioskTransSuccess = 0,
        SmartChargerInsertedSuccess = 1,
        Fail = 2,
        VerifyErrorByKiosk = 3,
        NoCardFail = 4,
        NoInsertedBatteryFail = 5,
        DrawOutBatteryFail = 6,
        VerifyErrorBySmartCharger = 7,
        SmartChargerDrawout = 8
    }

}
