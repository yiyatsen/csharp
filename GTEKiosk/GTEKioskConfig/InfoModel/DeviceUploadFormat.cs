﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class DeviceUploadFormat
    {
        public string DeviceId { get; set; }
        //public string StationManufactureDate { get; set; }
        public string DeviceTypeId { get; set; }
        public string TimeStamp { get; set; }
        public int ActionCode { get; set; }
        public string Message { get; set; }
    }

    public class DeviceFormat
    {
        public string SerialCode { get; set; }
        public string TimeStamp { get; set; }
        public int ActionCode { get; set; }
        public string Message { get; set; }
    }
}
