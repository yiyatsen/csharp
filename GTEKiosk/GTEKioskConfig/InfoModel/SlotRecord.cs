﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class SlotRecord
    {
        public long SlotRecordId { get; set; }
        public string TimeStamp { get; set; }
        public int SlotId { get; set; }
        public int SlotStatusCode { get; set; } //  -1:None Gate, 0:Gate,None Battery, 1:Gate, Battery

        public bool IsUpdate { get; set; }

        public long SlotBatteryRecordId { get; set; }
        public SlotBatteryRecord SlotBatteryRecord { get; set; }
    }
}
