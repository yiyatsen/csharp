﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class ConfigInfo
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
