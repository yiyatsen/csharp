﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskConfig.InfoModel
{
    public class BatteryInfo
    {
        public string BatteryId { get; set; }
        public string ManufactureDate { get; set; }
        public int StatusCode { get; set; } // 0: normal , 1: black, 2: white
    }
}
