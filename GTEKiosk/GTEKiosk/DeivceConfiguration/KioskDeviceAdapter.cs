﻿using GTE.Command;
using GTE.DeivceConfiguration;
using GTE.Encapsulation;
using GTEKioskConfig.InfoModel;
using GTEKioskLib;
using GTEKioskLib.Command;
using GTEKioskLib.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKiosk.DeivceConfiguration
{
    public class KioskDeviceAdapter : AppDeviceAdapter
    {
        private List<ChargeSlotItem> mSlotList { get; set; }

        private Dictionary<int, GateStatusInfo> mAddressGateStatusList = new Dictionary<int, GateStatusInfo>();

        private KioskAPI mDevice { get; set; }

        public KioskDeviceAdapter(KioskAPI device)
            : base()
        {
            mDevice = device;
        }

        public bool SetConnectModule(string port, int baudRate)
        {
            if (port != null && !port.Equals("") && baudRate > 0)
            {
                return mDevice.Connect(port, baudRate);
            }
            return false;
        }

        public void SetKioskAPIConfig(KioskBatteryType type, GTE.Encapsulation.EncapsulationType version)
        {
            mDevice.SetBatteryConfig(type);
            mDevice.SetEncapsulationType(version);
        }

        public void SetSlotItemList(List<ChargeSlotItem> slotList)
        {
            mSlotList = slotList;
        }

        public ChargeSlotItem GetSlotItemById(int slotId)
        {
            try
            {
                return (from list in mSlotList
                        where list.SlotId == slotId
                        select list).FirstOrDefault();
            }
            catch
            {
            }

            return null;
        }

        public List<ChargeSlotItem> GetSlotItemList()
        {
            return mSlotList;
        }

        private void SetGateStatusByAddress(int address, GateStatusInfo gateStatusInfo)
        {
            if (mAddressGateStatusList.ContainsKey(address))
            {
                mAddressGateStatusList[address] = gateStatusInfo;
            }
            else
            {
                mAddressGateStatusList.Add(address, gateStatusInfo);
            }
        }

        private void RemoveGateStatusByAddress(int address)
        {
            mAddressGateStatusList.Remove(address);
        }

        public GateStatusInfo GetGateStatusByAddress(int address)
        {
            GateStatusInfo gsi = null;
            if (mAddressGateStatusList.ContainsKey(address))
            {
                gsi = mAddressGateStatusList[address];
            }
            else
            {
                gsi = (GateStatusInfo)mDevice.SendGateCmdByAddress(KioskGateStatusCmd.CurrentStatus, address, null);
                SetGateStatusByAddress(address, gsi);
            }

            return gsi;
        }

        public GateStatusInfo GetGateStatusBySlotId(int slotId)
        {
            GateStatusInfo gsi = null;
            ChargeSlotItem chargeSlotItem = GetSlotItemById(slotId);

            if (chargeSlotItem != null)
            {
                gsi = (GateStatusInfo)mDevice.SendGateCmdByAddress(KioskGateStatusCmd.CurrentStatus, chargeSlotItem.GateAddress, null);
                SetGateStatusByAddress(chargeSlotItem.GateAddress, gsi);
            }

            return gsi;
        }

        public GateStatusInfo SetGateStatusByAddress(int address, KioskGateStatusCmd gateStatusCmd)
        {
            GateStatusInfo gsi = GetGateStatusByAddress(address);

            if (gsi != null)
            {
                object isSuccess = mDevice.SendGateCmdByAddress(gateStatusCmd, address, gsi);
                if (isSuccess != null)
                {
                    switch (gateStatusCmd)
                    {
                        case KioskGateStatusCmd.BlueLightOn:
                        case KioskGateStatusCmd.BlueLightOff:
                            {
                                gsi.IsBlueLightOn = !gsi.IsBlueLightOn;
                                break;
                            }
                        case KioskGateStatusCmd.GreenLightOff:
                        case KioskGateStatusCmd.GreenLightOn:
                            {
                                gsi.IsGreenLightOn = !gsi.IsGreenLightOn;
                                break;
                            }
                        case KioskGateStatusCmd.FrontUnLockOff:
                        case KioskGateStatusCmd.FrontUnLockOn:
                            {
                                gsi.IsFrontDoorUnLock = !gsi.IsFrontDoorUnLock;
                                break;
                            }
                        case KioskGateStatusCmd.BackUnLockOff:
                        case KioskGateStatusCmd.BackUnLockOn:
                            {
                                gsi.IsBackDoorUnLock = !gsi.IsBackDoorUnLock;
                                break;
                            }
                        case KioskGateStatusCmd.OnCharge:
                        case KioskGateStatusCmd.OffCharge:
                            {
                                gsi.IsOnCharge = !gsi.IsOnCharge;
                                break;
                            }
                        case KioskGateStatusCmd.TurnOffAllStatus:
                            {
                                gsi.IsBlueLightOn = false;
                                gsi.IsGreenLightOn = false;
                                gsi.IsFrontDoorUnLock = false;
                                gsi.IsBackDoorUnLock = false;
                                gsi.IsOnCharge = false;
                                break;
                            }
                        case KioskGateStatusCmd.TurnOnAllStatus:
                            {
                                gsi.IsBlueLightOn = true;
                                gsi.IsGreenLightOn = true;
                                gsi.IsFrontDoorUnLock = true;
                                gsi.IsBackDoorUnLock = true;
                                gsi.IsOnCharge = true;
                                break;
                            }
                    }

                    SetGateStatusByAddress(address, gsi);
                }
            }
            return gsi;
        }

        public GateStatusInfo SetGateStatusBySlotId(int slotId, KioskGateStatusCmd gateStatusCmd)
        {
            GateStatusInfo gsi = null;
            ChargeSlotItem chargeSlotItem = GetSlotItemById(slotId);
            if (chargeSlotItem != null)
            {
                gsi = SetGateStatusByAddress(chargeSlotItem.GateAddress, gateStatusCmd);
            }
           
            return gsi;
        }

        public int GetBatterySOCBySlotId(int slotId)
        {
            int soc = 0;
            ChargeSlotItem chargeSlotItem = GetSlotItemById(slotId);
            
            if (chargeSlotItem != null)
            {
                object value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_RSOC_GET, chargeSlotItem.BatteryAddress));
                if (value != null)
                {
                    int numberValue = 0;
                    int.TryParse(value.ToString(), out numberValue);
                    soc = numberValue;
                }
            }

            return soc;
        }

        public BatteryTransIdInfo GetBatteryTransIdBySlotId(int slotId)
        {
            BatteryTransIdInfo batteryIdInfo = null;
            ChargeSlotItem chargeSlotItem = GetSlotItemById(slotId);

            if (chargeSlotItem != null)
            {
                batteryIdInfo = new BatteryTransIdInfo();
                object value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_BATTERY_NO_GET, chargeSlotItem.BatteryAddress));
                if (value != null)
                {
                    batteryIdInfo.BatteryId = value.ToString();
                }

                value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_MANUFACTURE_DATE_GET, chargeSlotItem.BatteryAddress));
                if (value != null)
                {
                    batteryIdInfo.ManufactureDate = value as int[];
                }

                value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_CID_GET, chargeSlotItem.BatteryAddress));
                if (value != null)
                {
                    batteryIdInfo.CardId = value.ToString();
                }
                value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_UID_GET, chargeSlotItem.BatteryAddress));
                if (value != null)
                {
                    batteryIdInfo.UserId = value.ToString();
                }

                if (batteryIdInfo.BatteryId == null)
                {
                    batteryIdInfo = null;
                }
            }

            return batteryIdInfo;
        }

        public bool SetBatteryTransIdBySlotId(int slotId, BatteryTransIdInfo batteryIdInfo)
        {
            bool isSuccess = false;
            ChargeSlotItem chargeSlotItem = GetSlotItemById(slotId);
            if (chargeSlotItem != null && batteryIdInfo != null && batteryIdInfo.CardId != null && batteryIdInfo.UserId != null)
            {
                object value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_CID_SET, 0, batteryIdInfo.CardId, chargeSlotItem.BatteryAddress));
                if (value != null)
                {
                    isSuccess = true;
                }
                value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_UID_SET, 0, batteryIdInfo.UserId, chargeSlotItem.BatteryAddress));
                if (value != null)
                {
                    isSuccess = isSuccess && true;
                }
            }

            return isSuccess;
        }

        public BatteryBaseStatusInfo GetBatteryBaseStatusByAddress(int address)
        {
            BatteryBaseStatusInfo statusInfo = new BatteryBaseStatusInfo();

            object value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_RSOC_GET, address));
            if (value != null)
            {
                int numberValue = 0;
                int.TryParse(value.ToString(), out numberValue);
                statusInfo.SOC = numberValue;
            }

            value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_VOLTAGE_GET, address));
            if (value != null)
            {
                statusInfo.CellsVoltage = value as int[];
            }

            value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_PROTECTION_GET, address));
            if (value != null)
            {
                statusInfo.Protection = value as byte[];
            }

            if (statusInfo.Protection == null || statusInfo.CellsVoltage == null)
            {
                statusInfo = null;
            }

            return statusInfo;
        }

        public BatteryBaseStatusInfo GetBatteryBaseStatusBySlotId(int slotId)
        {
            BatteryBaseStatusInfo statusInfo = null;
            ChargeSlotItem chargeSlotItem = GetSlotItemById(slotId);
            if (chargeSlotItem != null)
            {
                statusInfo = GetBatteryBaseStatusByAddress(chargeSlotItem.BatteryAddress);
            }

            return statusInfo;
        }

        public BatteryStatusInfo GetBatteryStatusByAddress(int address)
        {
            BatteryStatusInfo statusInfo = new BatteryStatusInfo();

            object value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_RSOC_GET, address));
            if (value != null)
            {
                int numberValue = 0;
                int.TryParse(value.ToString(), out numberValue);
                statusInfo.SOC = numberValue;
            }

            value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_CURRENT_GET, address));
            if (value != null)
            {
                int numberValue = 0;
                int.TryParse(value.ToString(), out numberValue);
                statusInfo.Current = numberValue;
            }

            value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_VOLTAGE_GET, address));
            if (value != null)
            {
                statusInfo.CellsVoltage = value as int[];
            }

            value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_TEMP_GET, address));
            if (value != null)
            {
                statusInfo.Temperature = value as double[];
            }

            value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_PROTECTION_GET, address));
            if (value != null)
            {
                statusInfo.Protection = value as byte[];
            }

            value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_FULL_CC_GET, address));
            if (value != null)
            {
                ulong numberValue = 0;
                ulong.TryParse(value.ToString(), out numberValue);
                statusInfo.FCC = Convert.ToInt64(numberValue);
            }

            value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_CYCLE_COUNT_GET, address));
            if (value != null)
            {
                ulong numberValue = 0;
                ulong.TryParse(value.ToString(), out numberValue);
                statusInfo.CycleCount = Convert.ToInt64(numberValue);
            }

            value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_REMAIN_CAPACITY_GET, address));
            if (value != null)
            {
                ulong numberValue = 0;
                ulong.TryParse(value.ToString(), out numberValue);
                statusInfo.RC = Convert.ToInt64(numberValue);
            }

            value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_DESIGN_CAPACITY_GET, address));
            if (value != null)
            {
                ulong numberValue = 0;
                ulong.TryParse(value.ToString(), out numberValue);
                statusInfo.DC = Convert.ToInt64(numberValue);
            }

            if (statusInfo.Protection == null || statusInfo.CellsVoltage == null)
            {
                statusInfo = null;
            }

            return statusInfo;
        }

        public BatteryStatusInfo GetBatteryStatusBySlotId(int slotId)
        {
            BatteryStatusInfo statusInfo = null;
            ChargeSlotItem chargeSlotItem = GetSlotItemById(slotId);
            if (chargeSlotItem != null)
            {
                statusInfo = GetBatteryStatusByAddress(chargeSlotItem.BatteryAddress);
            }

            return statusInfo;
        }

        public BatteryVerInfo GetBatteryVerInfoByAddress(int address)
        {
            BatteryVerInfo verInfo = new BatteryVerInfo();

            object value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_DEVICE_NAME_GET, address));
            if (value != null)
            {
                verInfo.DeviceName = value.ToString();
            }

            value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_MCU_VERSION_GET, address));
            if (value != null)
            {
                verInfo.MCUVer = value.ToString();
            }

            value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_BATTERY_NO_GET, address));
            if (value != null)
            {
                verInfo.BatteryId = value.ToString();
            }

            value = IsLegalData(mDevice.SendBatteryCmdByAddress(BatteryCmd.CMD_MANUFACTURE_DATE_GET, address));
            if (value != null)
            {
                verInfo.ManufactureDate = value as int[];
            }

            if (verInfo.ManufactureDate == null || verInfo.BatteryId == null)
            {
                verInfo = null;
            }

            return verInfo;
        }

        public BatteryVerInfo GetBatteryVerInfoBySlotId(int slotId)
        {
            BatteryVerInfo verInfo = null;
            ChargeSlotItem chargeSlotItem = GetSlotItemById(slotId);
            if (chargeSlotItem != null)
            {
                verInfo = GetBatteryVerInfoByAddress(chargeSlotItem.BatteryAddress);
            }

            return verInfo;
        }
    }
}
