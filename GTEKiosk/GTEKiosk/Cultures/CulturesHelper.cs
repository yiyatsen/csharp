﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Xml;

namespace GTEKiosk.Cultures
{
    public class CulturesHelper
    {
        private const string _resourcePrefix = "Language";

        private const string _culturesFolder = "Cultures";

        private bool _isFoundInstalledCultures = false;

        private XmlDataProvider _xmlDataProvider;

        private List<CultureInfo> _supportedCultures = new List<CultureInfo>();

        public List<CultureInfo> SupportedCultures
        {
            get
            {
                return _supportedCultures;
            }
        }

        public string CurrentCultureInfoName()
        {
            if (Properties.Settings.Default.DefaultCulture != null)
            {
                return Properties.Settings.Default.DefaultCulture.Name;
            }
            return null;
        }

        public CulturesHelper()
        {
            if (!_isFoundInstalledCultures)
            {

                CultureInfo cultureInfo = new CultureInfo("");

                List<string> files = Directory.GetFiles(string.Format("{0}{1}", System.AppDomain.CurrentDomain.BaseDirectory, _culturesFolder)).Where(s => s.Contains(_resourcePrefix) && s.ToLower().EndsWith("xml")).ToList();

                foreach (string file in files)
                {
                    try
                    {
                        string cultureName = file.Substring(file.IndexOf(".") + 1).Replace(".xml", "");

                        cultureInfo = CultureInfo.GetCultureInfo(cultureName);


                        if (cultureInfo != null)
                        {
                            _supportedCultures.Add(cultureInfo);

                        }
                    }
                    catch (ArgumentException)
                    {
                    }

                }

                if (_supportedCultures.Count > 0 && Properties.Settings.Default.DefaultCulture != null)
                {
                    ChangeCulture(Properties.Settings.Default.DefaultCulture);
                }

                _isFoundInstalledCultures = true;
            }
        }

        public XmlDataProvider LanguageProvider
        {
            get
            {
                if (_xmlDataProvider == null)
                {
                    _xmlDataProvider = (XmlDataProvider)App.Current.FindResource("xmlLanguageProvider");
                }

                return _xmlDataProvider;
            }
        }

        private CultureInfo GetCultureInfo(string name)
        {
            try
            {
                return (from list in SupportedCultures
                        where list.Name == name
                        select list).FirstOrDefault();
            }
            catch { }

            return null;
        }

        public void ChangeCulture(string cultureName)
        {
            if (cultureName != null)
            {
                CultureInfo cultureInfo = GetCultureInfo(cultureName);

                ChangeCulture(cultureInfo);
            }
        }

        public void ChangeCulture(CultureInfo culture)
        {
            if (_supportedCultures.Contains(culture))
            {
                string LoadedFileName = string.Format("{0}{1}\\{2}.{3}.xml", System.AppDomain.CurrentDomain.BaseDirectory, _culturesFolder, _resourcePrefix, culture.Name);

                FileStream fileStream = new FileStream(@LoadedFileName, FileMode.Open);

                LanguageProvider.Document = new XmlDocument();

                LanguageProvider.Document.Load(fileStream);
                
                fileStream.Close();

                Properties.Settings.Default.DefaultCulture = culture;
                Properties.Settings.Default.Save();

                LanguageProvider.Refresh();
            }
        }

    }
}
