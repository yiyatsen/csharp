﻿using GTEKiosk.Mgr;
using GTEKiosk.Parameter;
using GTEKioskConfig.InfoModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GTEKiosk
{
    /// <summary>
    /// RegisterWindow.xaml 的互動邏輯
    /// </summary>
    public partial class RegisterWindow : Window
    {
        public delegate void CallbackMessage(bool isSuccessResponse, string result, RegisterDeviceFormat message);

        public static event CallbackMessage ServerCallbackMessage;


        public RegisterWindow()
        {
            InitializeComponent();

            ServerCallbackMessage += RegisterCallback;
        }

        private void confirmButton_Click(object sender, RoutedEventArgs e)
        {
            ConfigInfo deviceTypeId = MainWindow.KioskMgr.GetConfigInfoByName(KioskParameter.DEVICETYPEID);

            if (deviceTypeId != null && deviceTypeId.Value != null)
            {
                RegisterDeviceFormat registerStationFormat = new RegisterDeviceFormat()
                {
                    DeviceId = deviceIdText.Text,
                    Password = pwdText.Password,
                    DeviceTypeId = deviceTypeId.Value,
                    UUID = MainWindow.UUID
                };

                RegisterPost("http://" + MainWindow.AppSettingFile.ServerHost + ":" + MainWindow.AppSettingFile.ServerPort + KioskParameter.REGISTERAPI, registerStationFormat);
            }
        }

        private void exitButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.CloseApp();
        }

        public static async void RegisterPost(string url, RegisterDeviceFormat message)
        {
            bool isSuccessResponse = false;
            string result = null;
            try
            {
                //// ... Use HttpClient.
                using (HttpClient client = new HttpClient())
                using (HttpResponseMessage response = await client.PostAsync(url
                    , new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(message).ToString(),
                            Encoding.UTF8, "application/json")))
                using (HttpContent content = response.Content)
                {
                    result = await content.ReadAsStringAsync();
                    isSuccessResponse = true;
                }
            }
            catch
            {
                isSuccessResponse = false;
            }

            if (ServerCallbackMessage != null)
            {
                ServerCallbackMessage(isSuccessResponse, result, message);
            }
        }

        private void RegisterCallback(bool isSuccessResponse, string result, RegisterDeviceFormat message)
        {
            if (isSuccessResponse)
            {
                ServerResponseFormat responseData = null;
                using (TextReader file = new StringReader(result))
                {
                    try
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        responseData = (ServerResponseFormat)serializer.Deserialize(file, typeof(ServerResponseFormat));
                    }
                    catch
                    {

                    }
                }

                if (responseData != null)
                {
                    if (responseData.ActionCode == 999)
                    {
                        MainWindow.KioskMgr.SetConfigInfoValue(KioskParameter.REGISTERTIME, DateTime.UtcNow.ToString("yyyyMMddHHmmss"));
                        MainWindow.KioskMgr.SetConfigInfoValue(KioskParameter.REGISTERDEVICE, MainWindow.UUID);
                        MainWindow.KioskMgr.SetConfigInfoValue(KioskParameter.DEVICEID, message.DeviceId);
                        //MainWindow.KioskMgr.SetConfigInfoValue(KioskParameter.STATIONMANUFACTUREDATE, responseData.Message);
                        MainWindow.KioskMgr.SetConfigInfoValue(KioskParameter.PASSWORD, message.Password);
                        MainWindow.KioskMgr.SaveConfigInfoToDB();

                        MessageBox.Show("Please restart Application", "Success");
                        MainWindow.CloseApp();
                    }
                    else
                    {
                        MessageBox.Show(responseData.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Error Message");
                }
            }
            else
            {
                MessageBox.Show("Cannot found Server");
            }


        }

        public class RegisterDeviceFormat
        {
            public string DeviceId { get; set; }
            public string Password { get; set; }
            public string DeviceTypeId { get; set; }
            public string UUID { get; set; }
        }
    }
}
