﻿using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsPresentation;
using GTEKioskConfig.InfoModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace GTEKiosk
{
    /// <summary>
    /// MapWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MapWindow : Window
    {
        public MapWindow()
        {
            InitializeComponent();

            try
            {
                System.Net.IPHostEntry e = System.Net.Dns.GetHostEntry("www.google.com");
            }
            catch
            {
                mapControl.Manager.Mode = AccessMode.CacheOnly;
            }

            mapControl.MapProvider = GMapProviders.GoogleMap;
            mapControl.MinZoom = 12; //Minimal Shrink + Grow
            mapControl.MaxZoom = 17; //Maximal Shrink + Grow
            mapControl.Zoom = 15; //Current Shrink + Grow
            mapControl.ShowCenter = false; //The block of wood display centre cross burns
            mapControl.DragButton = MouseButton.Left; //The key drags left dragging a map
            mapControl.Position = new PointLatLng(MainWindow.AppSettingFile.Latitude, MainWindow.AppSettingFile.Longitude); //Map centre positionNanjing

            MoveToKioskPosition();

            PointLatLng point = new PointLatLng(MainWindow.AppSettingFile.Latitude, MainWindow.AppSettingFile.Longitude);
            GMapMarker marker = new GMapMarker(point);
            System.Windows.Controls.Image image = new System.Windows.Controls.Image();
            image.Source = new BitmapImage(new Uri(@"res/image/Here.png", UriKind.RelativeOrAbsolute));
            marker.Shape = image;
            mapControl.Markers.Add(marker);

            MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.Map.ToString()));
        }

        private void MoveToKioskPosition()
        {
            mapControl.Position = new PointLatLng(MainWindow.AppSettingFile.Latitude, MainWindow.AppSettingFile.Longitude);
        }

        //private void mapControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    Point clickPoint = e.GetPosition(mapControl);
        //    PointLatLng point = mapControl.FromLocalToLatLng((int)clickPoint.X, (int)clickPoint.Y);
        //    GMapMarker marker = new GMapMarker(point);
        //    mapControl.Markers.Add(marker);
        //}

        //private void mapControl_OnMapZoomChanged()
        //{
        //}

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.BackToHome(false);
        }

        private void zoomInButton_Click(object sender, RoutedEventArgs e)
        {
            mapControl.Zoom = mapControl.Zoom + 1;
        }

        private void zoomOutButton_Click(object sender, RoutedEventArgs e)
        {
            mapControl.Zoom = mapControl.Zoom - 1;
        }
    }
}