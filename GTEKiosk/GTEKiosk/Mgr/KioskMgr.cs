﻿using GTEKiosk.DeivceConfiguration;
using GTEKiosk.Parameter;
using GTEKiosk.Trans;
using GTEKioskConfig.InfoModel;
using GTEKioskLib.Command;
using GTEKioskLib.InfoModel;
using GTEKioskLib.RFID;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace GTEKiosk.Mgr
{
    public class KioskMgr
    {
        public static int ResetAbnormalDeviceTime = 15;
        private static int CheckCurrentTime = 3;

        public bool IsKeepRunThread { get; set; }
        public bool IsStopScanSlotInfo = false;
        private bool mIsWaitingAutoScan { get; set; }

        public bool IsRefreshRFID = false;

        private bool IsCheckOnline = false;

        private string mDeviceType { get; set; }

        private DBMgr mDBMgr { get; set; }
        private RFIDAPI mRFIDApi { get; set; }
        private KioskDeviceAdapter mDeviceAdapter { get; set; }

        private TransCheck mTransCheck { get; set; }

        private ChargeSlotItem EmptySlotItem { get; set; }
        private ChargeSlotItem UseableSlotItem { get; set; }
        private List<ChargeSlotItem> UseableSlotItemList = new List<ChargeSlotItem>(); // Added By Neil 2016.07.19
        private CardInfo mCardInfo { get; set; }

        private long mUpdateSlotStatusTimer { get; set; }
        private TransRecord mTransRecord = new TransRecord();
        private List<ChargeTransRecord> mCurrentChargeTransList { get; set; }
        private List<AbnormalRecord> mAbnormalRecordList = new List<AbnormalRecord>();

        private Dictionary<int, int> mCheckCurrentList = new Dictionary<int, int>(); //確認充電狀態次數

        private List<UserInfo> mUserInfoList = new List<UserInfo>();

        private bool mIsUpdateConfigInfoToDB = false;
        private List<ConfigInfo> mConfigInfoList = new List<ConfigInfo>();
        private List<ConfigInfo> mUpdateConfigInfoList = new List<ConfigInfo>();

        private Dictionary<int, int> mCurrentDetectedErrorTime = new Dictionary<int, int>();

        private string mSlotStatusRecordMessage { get; set; }
        private string mTransRecordMessage { get; set; }
        private string mChargeTransRecordMessage { get; set; }
        private string mAbnormalRecordMessage { get; set; }

        public KioskMgr()
        {
            mDBMgr = new DBMgr();
        }

        public void SetDeviceType(string deviceType)
        {
            mDeviceType = deviceType;
        }

        public void SetDeviceAdapter(KioskDeviceAdapter deviceAdapter)
        {
            mDeviceAdapter = deviceAdapter;
        }

        public void SetRFIDAPI(RFIDAPI rfidAPI)
        {
            mRFIDApi = rfidAPI;
        }

        /// <summary>
        /// 線上模式
        /// 線下模式(資料庫比對)
        /// </summary>
        /// <param name="index"></param>
        public void SetTransCheckMode(int index)
        {
            switch (index)
            {
                default:
                    {
                        mTransCheck = new TransCheckBase();
                        break;
                    }
            }
        }


        #region ConfigInfo
        public void ResetConfigInfo()
        {
            mConfigInfoList.Clear();
            //if (mDBMgr.OpenDB())
            //{
                List<object> tempUserInfoList = mDBMgr.Read(string.Format("SELECT * FROM {0}", typeof(ConfigInfo).Name), typeof(ConfigInfo));
                //mDBMgr.CloseDB();

                foreach (object data in tempUserInfoList)
                {
                    mConfigInfoList.Add((ConfigInfo)data);
                }
            //}
        }

        public ConfigInfo GetConfigInfoByName(string name)
        {
            try
            {
                return (from list in mConfigInfoList
                        where list.Name.Equals(name)
                        select list).FirstOrDefault();
            }
            catch { }

            return null;
        }

        public void SetConfigInfoValue(string name, string value)
        {
            ConfigInfo configInfo = GetConfigInfoByName(name);
            if (configInfo != null)
            {
                configInfo.Value = value;
            }
            else
            {
                configInfo = new ConfigInfo()
                {
                    Name = name,
                    Value = value
                };
                mConfigInfoList.Add(configInfo);
            }
        }

        public void SaveConfigInfoToDB()
        {
            foreach (ConfigInfo ci in mConfigInfoList)
            {
                mDBMgr.Update(string.Format(
                    "UPDATE  {0} SET Value='{1}' WHERE Name='{2}';"
                    , typeof(ConfigInfo).Name, ci.Value, ci.Name));
            }
        }
        #endregion

        #region ChargeTrans
        public void InitCurrentChargeTransList()
        {
            if (mCurrentChargeTransList == null)
            {
                mCurrentChargeTransList = new List<ChargeTransRecord>();

                //if (mDBMgr.OpenDB())
                //{
                    List<object> tempData = mDBMgr.Read(string.Format("SELECT * FROM {0} WHERE ChargeTransStatusCode = {1}", typeof(ChargeTransRecord).Name, 0), typeof(ChargeTransRecord));
                    if (tempData != null && tempData.Count > 0)
                    {
                        foreach (object data in tempData)
                        {
                            ChargeTransRecord chargeTransRecord = data as ChargeTransRecord;
                            if (chargeTransRecord != null)
                            {
                                List<object> tempChargeData = mDBMgr.Read(string.Format("SELECT * FROM {0} WHERE ChargeTransDeviceRecordId = {1}"
                                    , typeof(ChargeTransDeviceRecord).Name, chargeTransRecord.StartChargeTransDeviceRecordId), typeof(ChargeTransDeviceRecord));
                                if (tempChargeData != null && tempChargeData.Count > 0)
                                {
                                    chargeTransRecord.StartChargeTransDeviceRecord = tempChargeData[0] as ChargeTransDeviceRecord;
                                }
                                mCurrentChargeTransList.Add(chargeTransRecord);
                            }
                        }
                    }
                //    mDBMgr.CloseDB();
                //}
            }
        }

        public ChargeTransRecord GetChargeTransById(int slotId)
        {
            try
            {
                if (mCurrentChargeTransList != null)
                {
                    return (from list in mCurrentChargeTransList
                            where list.SlotId == slotId
                            select list).FirstOrDefault();
                }
            }
            catch { }

            return null;
        }
        #endregion

        #region AbnormalDate
        private bool CheckAbnormalDevice(int slotId, string batteryId, string manufactureDate)
        {
            try
            {
                foreach (AbnormalRecord slot in mAbnormalRecordList)
                {
                    if (slot.SlotId == slotId && slot.AbnormalDeviceRecord == null && batteryId == null && manufactureDate == null)
                    {
                        return true;
                    }
                    else if (slot.SlotId == slotId && slot.AbnormalDeviceRecord != null && batteryId != null && batteryId.Equals(slot.AbnormalDeviceRecord.BatteryId) && manufactureDate != null && manufactureDate.Equals(slot.AbnormalDeviceRecord.ManufactureDate))
                    {
                        return true;
                    }
                    
                }
            }
            catch { }

            return false;
        }
        private void ResetAbnormalDeviceData()
        {
            List<AbnormalRecord> tempDelARList = new List<AbnormalRecord>(); // 大於十分鐘刪除

            long timeStamp = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
            foreach (AbnormalRecord ar in mAbnormalRecordList)
            {
                if (DateTime.UtcNow > DateTime.Parse(ar.AbnormalTimeStamp).AddMinutes(ResetAbnormalDeviceTime))
                {
                    tempDelARList.Add(ar);
                }
            }

            foreach (AbnormalRecord ar in tempDelARList)
            {
                mAbnormalRecordList.Remove(ar);
            }
        }
        public void ResetAbnormalInformation()
        {
            mAbnormalRecordList.Clear();
        }
        #endregion

        #region UserInfo
        public void ResetUserInfo()
        {
            //if (mDBMgr.OpenDB())
            //{
                List<object> tempUserInfoList = mDBMgr.Read(string.Format("SELECT * FROM {0}", typeof(UserInfo).Name), typeof(UserInfo));
                //mDBMgr.CloseDB();

                mUserInfoList.Clear();
                foreach (object data in tempUserInfoList)
                {
                    mUserInfoList.Add((UserInfo)data);
                }
            //}
        }

        public UserInfo CheckUserInfo(string userId)
        {
            try
            {
                return (from list in mUserInfoList
                        where list.UserId.Equals(userId)
                        select list).FirstOrDefault();
            }
            catch { }

            return null;
        }
        #endregion

        #region RFID
        public bool CreateCard(string userId)
        {
            bool isSuccess = false;
            if (mRFIDApi.IsConnect() && userId != null && userId.Length == 10)
            {
                isSuccess = mRFIDApi.OpenCard(userId, DateTime.UtcNow);
            }

            return isSuccess;
        }


        public RFIDCardInfo ScanRFIDCard()
        {
            RFIDCardInfo cardInfo = null;
            if (mRFIDApi.IsConnect())
            {
                IsRefreshRFID = true;

                while (IsRefreshRFID)
                {
                    Thread.Sleep(50);

                    cardInfo = mRFIDApi.Refresh_Card();
                    if (cardInfo != null)
                    {
                        IsRefreshRFID = false;
                    }
                }
            }

            return cardInfo;
        }

        public bool DetectedRFIDCard()
        {
            int failTime = 0;
            bool isCardDetected = true;
            RFIDCardInfo cardInfo = null;
            if (mRFIDApi.IsConnect())
            {
                IsRefreshRFID = true;

                while (isCardDetected && IsRefreshRFID)
                {
                    Thread.Sleep(50);

                    cardInfo = mRFIDApi.Refresh_Card();
                    if (cardInfo == null)
                    {
                        failTime++;
                        if (failTime > 3)
                        {
                            isCardDetected = false;
                        }
                    }
                }
            }

            return isCardDetected;
        }

        public RFIDCardInfo IncrementRFIDCard(int addPoint)
        {
            RFIDCardInfo card = ScanRFIDCard();

            if (card != null)
            {
                mRFIDApi.Increment(addPoint);
                card = ScanRFIDCard();
            }

            return card;
        }
        #endregion

        public void ScanWaiting()
        {
            IsStopScanSlotInfo = true;
            while (mIsWaitingAutoScan)
            {
                Thread.Sleep(50);
            }
        }

        public void AutoScanSlotInfo()
        {
            bool isAutoScan = true;
            while (true)
            {
                isAutoScan = true;
                while (IsStopScanSlotInfo)
                {
                    mIsWaitingAutoScan = false;
                    Thread.Sleep(50);
                    isAutoScan = false;
                }

                if (isAutoScan)
                {
                    mIsWaitingAutoScan = true;
                    ScanSlotInfo(0);
                    mIsWaitingAutoScan = false;
                }
  
                Thread.Sleep(KioskParameter.UPLOADWAITINGTIME * 1000); //60秒
            }
        }

        /// <summary>
        /// 管理流程
        /// </summary>
        public bool ScanSlotInfo(int scanStatusCode)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();//引用stopwatch物件
            sw.Reset();//碼表歸零
            sw.Start();//碼表開始計時

            bool isUpdateFinish = true; //是否有正確更新完成
            string refreshTimeStamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");

            List<SlotRecord> slotRecordList = new List<SlotRecord>();
            List<ChargeTransRecord> chargeTransRecordList = new List<ChargeTransRecord>();
            List<AbnormalRecord> abnormalRecordList = new List<AbnormalRecord>();

            bool isExistEmptyBattery = false;
            int slotNormalSatusNumber = mDeviceAdapter.GetSlotItemList().Count;
            foreach (ChargeSlotItem item in mDeviceAdapter.GetSlotItemList())
            {
                if (scanStatusCode == 0 && IsStopScanSlotInfo)
                {
                    isUpdateFinish = false;// 有其他事件需要啟動需要停止更新資料
                    break;
                }

                SlotRecord tempSR = new SlotRecord();
                slotRecordList.Add(tempSR);
                tempSR.TimeStamp = refreshTimeStamp;
                tempSR.SlotId = item.SlotId;

                try
                {
                    GateStatusInfo gsi = mDeviceAdapter.GetGateStatusBySlotId(item.SlotId);

                    if (gsi == null) // gate cannot found
                    {
                        slotNormalSatusNumber = slotNormalSatusNumber - 1;
                        tempSR.SlotStatusCode = -1;

                        if (UseableSlotItem != null && UseableSlotItem.SlotId == item.SlotId)
                        {
                            UseableSlotItem = null;
                        }

                        if (EmptySlotItem != null && EmptySlotItem.SlotId == item.SlotId)
                        {
                            EmptySlotItem = null;
                        }
                    }
                    else
                    {
                        ChargeTransRecord ctr = GetChargeTransById(item.SlotId);
                        BatteryVerInfo bvi = mDeviceAdapter.GetBatteryVerInfoBySlotId(item.SlotId);

                        for (int i = 0; i < 3; i++)
                        {
                            if (bvi == null)
                            {
                                Thread.Sleep(300);
                                bvi = mDeviceAdapter.GetBatteryVerInfoBySlotId(item.SlotId);
                            }
                            else
                            {
                                break;
                            }
                        }

                        if (bvi != null)
                        {
                            bool isCheckedBattery = false;
                            BatteryStatusInfo bsi = mDeviceAdapter.GetBatteryStatusBySlotId(item.SlotId);
                            if (!(bsi != null && bsi.CellsVoltage != null && bsi.CellsVoltage.Count() >= 10))
                            {
                                bsi = mDeviceAdapter.GetBatteryStatusBySlotId(item.SlotId);
                            }

                            if (ctr != null)
                            {
                                if (ctr.StartChargeTransDeviceRecord != null && bvi.BatteryId != null && bvi.BatteryId.Equals(ctr.StartChargeTransDeviceRecord.BatteryId) &&
                                    ctr.StartChargeTransDeviceRecord.ManufactureDate != null && ctr.StartChargeTransDeviceRecord.ManufactureDate.Equals(mDBMgr.ConvertManufactureDate(bvi.ManufactureDate)))
                                {
                                    isCheckedBattery = true;
                                    if (gsi.IsOnCharge)
                                    {
                                        if (bsi.Current < 500)
                                        {
                                            int checkTime = 3;
                                            for (int i = 0; i < checkTime; i++)
                                            {
                                                bsi = mDeviceAdapter.GetBatteryStatusBySlotId(item.SlotId);
                                                if (bsi != null && bsi.Current < 500)
                                                {
                                                    checkTime--;
                                                    Thread.Sleep(200);
                                                }
                                                else
                                                {
                                                    break;
                                                }
                                            }

                                            if (bsi.Current < 500) //確認電流真的小於500
                                            {
                                                if (!mCheckCurrentList.ContainsKey(item.SlotId))
                                                {
                                                    mCheckCurrentList.Add(item.SlotId, CheckCurrentTime);
                                                }
                                                else
                                                {
                                                    if (mCheckCurrentList[item.SlotId] < 1)
                                                    {
                                                        mCheckCurrentList.Remove(item.SlotId);

                                                        // 1.關閉充電器
                                                        mDeviceAdapter.SetGateStatusBySlotId(item.SlotId, KioskGateStatusCmd.TurnOffAllStatus);
                                                        string timeStamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");

                                                        int protection = mDBMgr.ConvertProtection(bsi.Protection);
                                                        string manufactureDate = mDBMgr.ConvertManufactureDate(bvi.ManufactureDate);
                                                        string cellsVolt = mDBMgr.ConvertCellsVoltage(bsi.CellsVoltage);
                                                        string temperature = mDBMgr.ConvertBatteryTemp(bsi.Temperature);

                                                        // 正常充電結束記錄
                                                        ctr.EndChargeTransDeviceRecord = new ChargeTransDeviceRecord()
                                                        {
                                                            ChargeTimeStamp = timeStamp,
                                                            BatteryId = bvi.BatteryId,
                                                            ManufactureDate = manufactureDate,
                                                            BatteryTypeId = MainWindow.BATTERYTYPEID,
                                                            SOC = bsi.SOC,
                                                            FCC = bsi.FCC,
                                                            RC = bsi.RC,
                                                            Voltage = mDBMgr.SumCellsVoltage(bsi.CellsVoltage),
                                                            CellsVoltage = cellsVolt,
                                                            CellsTemperature = temperature,

                                                            CycleCount = bsi.CycleCount,
                                                            Protection = protection
                                                        };

                                                        // Fixed by Neil 2016.04.15
                                                        if (bsi.DC > 0)
                                                        {
                                                            ctr.EndChargeTransDeviceRecord.SOH = Convert.ToInt32(bsi.FCC * 100 / bsi.DC);

                                                            if (ctr.EndChargeTransDeviceRecord.SOH > 100)
                                                            {
                                                                ctr.EndChargeTransDeviceRecord.SOH = 100;
                                                            }
                                                        }

                                                        SetVoltageData(ctr.EndChargeTransDeviceRecord, bsi.CellsVoltage);
                                                        //

                                                        if (ctr.EndChargeTransDeviceRecord != null && ctr.StartChargeTransDeviceRecord != null)
                                                        {
                                                            ctr.ChargedCapacity = ctr.EndChargeTransDeviceRecord.RC - ctr.StartChargeTransDeviceRecord.RC;
                                                            DateTime endTime = DateTime.Parse(ctr.EndChargeTransDeviceRecord.ChargeTimeStamp);
                                                            DateTime startTime = DateTime.Parse(ctr.StartChargeTransDeviceRecord.ChargeTimeStamp);
                                                            TimeSpan difference = endTime - startTime;
                                                            ctr.TotalTime = Convert.ToInt64(difference.TotalMinutes);
                                                        }

                                                        ctr.ChargeTransStatusCode = 1;
                                                        chargeTransRecordList.Add(ctr);
                                                        mCurrentChargeTransList.Remove(ctr); //刪除目前充電的狀態

                                                        if (!(bsi.SOC > 90)) //異常狀態 無法充電超過90%
                                                        {
                                                            AbnormalDeviceRecord adr = new AbnormalDeviceRecord()
                                                            {
                                                                BatteryId = bvi.BatteryId,
                                                                ManufactureDate = manufactureDate,
                                                                BatteryTypeId = MainWindow.BATTERYTYPEID,
                                                                FCC = bsi.FCC,
                                                                CycleCount = bsi.CycleCount,
                                                                SOC = bsi.SOC,
                                                                RC = bsi.RC,
                                                                CellsVoltage = cellsVolt,
                                                                CellsTemperature = temperature,
                                                                Protection = protection
                                                            };

                                                            AbnormalRecord ar = new AbnormalRecord()
                                                            {
                                                                SlotId = item.SlotId,
                                                                AbnormalStatusCode = 1, //異常代碼  無法充電超過90%
                                                                AbnormalTimeStamp = timeStamp,
                                                                AbnormalDeviceRecord = adr
                                                            };

                                                            abnormalRecordList.Add(ar);
                                                            mAbnormalRecordList.Add(ar);
                                                        }
                                                        else if (bsi.SOC <= 95 && bsi.SOC >= 90)
                                                        {
                                                            // 已經老化電池
                                                        }
                                                    }
                                                    else
                                                    {
                                                        mCheckCurrentList[item.SlotId] = mCheckCurrentList[item.SlotId] - 1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // 有充電紀錄並且為同一顆電池 -> 繼續充電
                                        mDeviceAdapter.SetGateStatusBySlotId(item.SlotId, KioskGateStatusCmd.OnCharge);
                                    }
                                }
                                else
                                {
                                    // 有充電紀錄電池不會同一顆 -> 刪除充電紀錄並且記錄至DB
                                    ctr.ChargeTransStatusCode = 2;
                                    chargeTransRecordList.Add(ctr);
                                    mCurrentChargeTransList.Remove(ctr);
                                }
                            }

                            if (!isCheckedBattery)
                            {
                                // 先判斷是否為異常電池
                                string manufactureDate = mDBMgr.ConvertManufactureDate(bvi.ManufactureDate);
                                if (!CheckAbnormalDevice(item.SlotId, bvi.BatteryId, manufactureDate))
                                {
                                    int protection = mDBMgr.ConvertProtection(bsi.Protection);
                                    if (!(bsi.SOC > 95) )
                                    {
                                        // 不為異常電池 開啟充電器
                                        mDeviceAdapter.SetGateStatusBySlotId(item.SlotId, KioskGateStatusCmd.OnCharge);

                                        // 開始充電記錄
                                        ChargeTransDeviceRecord ctdr = new ChargeTransDeviceRecord()
                                        {
                                            ChargeTimeStamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"),
                                            BatteryId = bvi.BatteryId,
                                            ManufactureDate = manufactureDate,
                                            BatteryTypeId = MainWindow.BATTERYTYPEID,
                                            SOC = bsi.SOC,
                                            FCC = bsi.FCC,
                                            RC = bsi.RC,
                                            Voltage = mDBMgr.SumCellsVoltage(bsi.CellsVoltage),
                                            CellsVoltage = mDBMgr.ConvertCellsVoltage(bsi.CellsVoltage),
                                            CellsTemperature = mDBMgr.ConvertBatteryTemp(bsi.Temperature),

                                            CycleCount = bsi.CycleCount,
                                            Protection = protection
                                        };

                                        // Fixed by Neil 2016.04.15
                                        if (bsi.DC > 0)
                                        {
                                            ctdr.SOH = Convert.ToInt32(bsi.FCC * 100 / bsi.DC);
                                            if (ctdr.SOH > 100)
                                            {
                                                ctdr.SOH = 100;
                                            }
                                        }

                                        SetVoltageData(ctdr, bsi.CellsVoltage);
                                        //

                                        ctr = new ChargeTransRecord()
                                        {
                                            ChargeTransTimeStamp = refreshTimeStamp,
                                            SlotId = item.SlotId,
                                            StartChargeTransDeviceRecord = ctdr,
                                            EndChargeTransDeviceRecordId = 0,
                                            BatteryTypeId = MainWindow.BATTERYTYPEID,
                                            IsUpdate = false
                                        };

                                        ctr.ChargeTransStatusCode = 0;
                                        chargeTransRecordList.Add(ctr);
                                        mCurrentChargeTransList.Add(ctr);
                                    }
                                    else if (!(protection < 1))// || bsi.SOC > 100)
                                    {
                                        // 有保護狀態
                                        string timeStamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                                        string cellsVolt = mDBMgr.ConvertCellsVoltage(bsi.CellsVoltage);
                                        string temperature = mDBMgr.ConvertBatteryTemp(bsi.Temperature);

                                        AbnormalDeviceRecord adr = new AbnormalDeviceRecord()
                                        {
                                            BatteryId = bvi.BatteryId,
                                            ManufactureDate = manufactureDate,
                                            BatteryTypeId = MainWindow.BATTERYTYPEID,
                                            FCC = bsi.FCC,
                                            CycleCount = bsi.CycleCount,
                                            SOC = bsi.SOC,
                                            RC = bsi.RC,
                                            CellsVoltage = cellsVolt,
                                            CellsTemperature = temperature,
                                            Protection = protection
                                        };

                                        AbnormalRecord ar = new AbnormalRecord()
                                        {
                                            SlotId = item.SlotId,
                                            AbnormalStatusCode = 1, //異常代碼  無法充電超過90%
                                            AbnormalTimeStamp = timeStamp,
                                            AbnormalDeviceRecord = adr
                                        };

                                        abnormalRecordList.Add(ar);
                                        mAbnormalRecordList.Add(ar);
                                    }
                                    else if (bsi.SOC <= 100 && bsi.SOC > 90)
                                    {
                                        UseableSlotItem = item; //可使用電池之電池櫃
                                    }
                                }
                            }

                            tempSR.SlotStatusCode = 1;
                            SlotBatteryRecord tempSBR = new SlotBatteryRecord()
                            {
                                SOC = bsi.SOC,
                                RC = bsi.RC,
                                Current = bsi.Current,
                                BatteryId = bvi.BatteryId,
                                ManufactureDate = mDBMgr.ConvertManufactureDate(bvi.ManufactureDate),
                                BatteryTypeId = MainWindow.BATTERYTYPEID,
                                Protection = mDBMgr.ConvertProtection(bsi.Protection)
                            };
                            SetVoltageData(tempSBR, bsi.CellsVoltage);
                            SetTempData(tempSBR, bsi.Temperature);
                            tempSR.SlotBatteryRecord = tempSBR;
                        }
                        else
                        {
                            if (ctr != null)
                            {
                                // 有充電紀錄無電池 -> 刪除充電紀錄並且記錄至DB
                                ctr.ChargeTransStatusCode = 2;
                                chargeTransRecordList.Add(ctr);
                                mCurrentChargeTransList.Remove(ctr);
                            }

                            if (gsi.IsBatteryInserted)// && !gsi.IsFrontDoorClose)
                            {
                                // 電池櫃存在無法辨識之電池
                                if (!CheckAbnormalDevice(item.SlotId, null, null))
                                {
                                    // 異常狀態
                                    // 無法讀取資料狀態
                                    AbnormalRecord ar = new AbnormalRecord()
                                    {
                                        SlotId = item.SlotId,
                                        AbnormalStatusCode = 0,
                                        AbnormalTimeStamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss")
                                    };

                                    abnormalRecordList.Add(ar);
                                    mAbnormalRecordList.Add(ar);
                                }

                                //通知介面有異常狀態
                            }
                            else
                            {
                                isExistEmptyBattery = true;
                                EmptySlotItem = item; //真正的空電池櫃
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }

                // Fixed by Neil 2016.07.18
                if (UseableSlotItem != null)
                {
                    var slotItem = UseableSlotItemList.FirstOrDefault(s => s.SlotId == item.SlotId);
                    if (slotItem != null)
                    {
                        if (UseableSlotItem.SlotId != item.SlotId)
                        {
                            UseableSlotItemList.Remove(item);
                        }
                    }
                    else
                    {
                        if (UseableSlotItem.SlotId == item.SlotId)
                        {
                            UseableSlotItemList.Add(item);
                        }
                    }
                }
                //
            }

            if (!isExistEmptyBattery)
            {
                int abnormalStatusCode = 3; //電池櫃全空
                if (slotNormalSatusNumber > 1)
                {
                    if (EmptySlotItem == null)
                    {
                        List<object> tempTransRecordList = mDBMgr.Read(string.Format(
                             "SELECT tr.* FROM {0} as tr ORDER BY tr.TransTimeStamp DESC"
                             , typeof(TransRecord).Name), typeof(TransRecord));
                        if (tempTransRecordList != null && tempTransRecordList.Count > 0)
                        {
                            TransRecord tr = tempTransRecordList[0] as TransRecord;
                            if (tr != null)
                            {
                                List<object> tempTransDeviceRecordList = mDBMgr.Read(string.Format(
                                     "SELECT tdr.* FROM {0} as tdr WHERE tdr.TransDeviceRecordId = {1}"
                                     , typeof(TransDeviceRecord).Name, tr.InsertedTransDeviceId), typeof(TransDeviceRecord));
                                if (tempTransDeviceRecordList != null && tempTransDeviceRecordList.Count > 0)
                                {
                                    TransDeviceRecord tdr = tempTransDeviceRecordList[0] as TransDeviceRecord;
                                    EmptySlotItem = mDeviceAdapter.GetSlotItemById(tdr.SlotId);
                                }
                            }
                        }
                    }

                    abnormalStatusCode = 2; // 電池櫃全滿
                }

                // 電池櫃存在無法辨識之電池
                if (!CheckAbnormalDevice(-1, null, null))
                {
                    // 異常狀態
                    // 無法讀取資料狀態
                    AbnormalRecord ar = new AbnormalRecord()
                    {
                        SlotId = -1,
                        AbnormalStatusCode = abnormalStatusCode,
                        AbnormalTimeStamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss")
                    };

                    abnormalRecordList.Add(ar);
                    mAbnormalRecordList.Add(ar);
                }
            }

            //if (mDBMgr.OpenDB()) // update DB
            //{
                TransRecordToDB(mTransRecord);
                ChargeTransRecordToDB(chargeTransRecordList);
                AbnormalRecordToDB(abnormalRecordList);

                if (isUpdateFinish)
                {
                    // 超過一分鐘的資料才紀錄
                    if (mUpdateSlotStatusTimer == 0 || mUpdateSlotStatusTimer < ((long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds + (long)(KioskParameter.UPLOADWAITINGTIME * 1000)))
                    {
                        SlotStatusRecordToDB(slotRecordList);
                        mUpdateSlotStatusTimer = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
                    }

                    ResetAbnormalDeviceData();
                }

                UploadData();
                
            //    mDBMgr.CloseDB();
            //}
            sw.Stop();//碼表停止

            return isUpdateFinish;
        }

        public int GetEmptySlotId()
        {
            return EmptySlotItem.SlotId;
        }

        public int GetUsableSlotId()
        {
            return UseableSlotItem.SlotId;
        }

        /// <summary>
        /// 掃瞄電池是否插入
        /// </summary>
        /// <returns></returns>
        public bool ScanBatteryInserted()
        {
            ChargeSlotItem slotItem = EmptySlotItem;

            bool isSuccessInserted = false;

            if (slotItem != null)
            {
                DateTime initDt = DateTime.Now;
                DateTime dt = DateTime.Now;

                int switchTime = 1000;
                bool enableLight = true;
                mDeviceAdapter.SetGateStatusBySlotId(slotItem.SlotId, KioskGateStatusCmd.GreenLightOn);
                IsKeepRunThread = true;

                mDeviceAdapter.SetGateStatusBySlotId(slotItem.SlotId, KioskGateStatusCmd.FrontUnLockOn);
                while (IsKeepRunThread)
                {
                    Thread.Sleep(50);
                    GateStatusInfo gsi = mDeviceAdapter.GetGateStatusBySlotId(slotItem.SlotId);

                    if (!gsi.IsFrontDoorClose && gsi.IsBatteryInserted)
                    {
                        isSuccessInserted = true;
                        IsKeepRunThread = false;
                    }
                    else
                    {
                        // Fixed by Neil 2016.04.15
                        //if (mDeviceAdapter.GetBatteryVerInfoBySlotId(slotItem.SlotId) != null)
                        //{
                        //    isSuccessInserted = true;
                        //    IsKeepRunThread = false;
                        //}
                    }

                    if (DateTime.Now > initDt.AddSeconds(KioskParameter.TRANSLASTWAITINGTIME))
                    {
                        mDeviceAdapter.SetGateStatusBySlotId(slotItem.SlotId, KioskGateStatusCmd.FrontUnLockOff);
                        switchTime = 500;
                        initDt = DateTime.Now;
                    }

                    // 閃燈
                    if (DateTime.Now > dt.AddMilliseconds(switchTime))
                    {
                        if (enableLight)
                        {
                            mDeviceAdapter.SetGateStatusBySlotId(slotItem.SlotId, KioskGateStatusCmd.GreenLightOff);
                        }
                        else
                        {
                            mDeviceAdapter.SetGateStatusBySlotId(slotItem.SlotId, KioskGateStatusCmd.GreenLightOn);
                        }
                        enableLight = !enableLight;
                        dt = DateTime.Now;
                    }
                }
            }

            mDeviceAdapter.SetGateStatusBySlotId(slotItem.SlotId, GTEKioskLib.Command.KioskGateStatusCmd.TurnOffAllStatus);


            return isSuccessInserted;
        }

        public bool ScanDrawOutBattery(TransAction transAction)
        {
            int slotId = 0;
            switch (transAction)
            {
                case TransAction.DrawOutNewBattery:
                    {
                        slotId = UseableSlotItem.SlotId;
                        break;
                    }
                case TransAction.DrawOutOriginalBatteryCheckBatteryAndCardError:
                case TransAction.DrawOutOriginalBattery:
                    {
                        slotId = EmptySlotItem.SlotId;
                        break;
                    }
            }

            return ScanDrawOutDevice(slotId);
        }

        #region Record Data To DB
        private void SlotStatusRecordToDB(List<SlotRecord> slotRecordList)
        {
            foreach (SlotRecord sr in slotRecordList)
            {
                                                    string batteryIdText = "null";
                    string manufactureDateText = "null";

                    if (sr.SlotBatteryRecord != null)
                    {
                        if (sr.SlotBatteryRecord.BatteryId != null)
                        {
                            batteryIdText = "'" + sr.SlotBatteryRecord.BatteryId + "'";
                        }

                        if (sr.SlotBatteryRecord.ManufactureDate != null)
                        {
                            manufactureDateText = "'" + sr.SlotBatteryRecord.ManufactureDate + "'";
                        }

                        sr.SlotBatteryRecord.SlotBatteryRecordId = mDBMgr.Insert(string.Format(
                            "INSERT INTO {0} (SlotBatteryRecordId,BatteryId, ManufactureDate,BatteryTypeId, SOC, RC, Temperature, Current, Voltage, VMax, VMin, Protection) VALUES (null,{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11});"
                            , typeof(SlotBatteryRecord).Name, batteryIdText, manufactureDateText, sr.SlotBatteryRecord.BatteryTypeId, sr.SlotBatteryRecord.SOC, sr.SlotBatteryRecord.RC
                            , sr.SlotBatteryRecord.Temperature, sr.SlotBatteryRecord.Current, sr.SlotBatteryRecord.Voltage
                            , sr.SlotBatteryRecord.VMax, sr.SlotBatteryRecord.VMin, sr.SlotBatteryRecord.Protection));

                        //sr.SlotBatteryRecord.SlotBatteryRecordId = mDBMgr.GetLastInsertId();
                        sr.SlotBatteryRecordId = sr.SlotBatteryRecord.SlotBatteryRecordId;
                    }

               sr.SlotRecordId = mDBMgr.Insert(string.Format(
                    "INSERT INTO {0} (SlotRecordId,TimeStamp,SlotId,SlotStatusCode,SlotBatteryRecordId,IsUpdate) VALUES (null, '{1}',{2},{3},{4},{5});"
                    , typeof(SlotRecord).Name, sr.TimeStamp, sr.SlotId, sr.SlotStatusCode, sr.SlotBatteryRecordId,0));

                //sr.SlotRecordId = mDBMgr.GetLastInsertId();
            }
        }
        private void TransRecordToDB(TransRecord transRecord)
        {
            if (transRecord.TransStatusCode > -1)
            {
                if (transRecord.InsertedTransDevice != null)
                {
                    string batteryId = "null";
                    if (transRecord.InsertedTransDevice.BatteryId != null)
                    {
                        batteryId = "'" + transRecord.InsertedTransDevice.BatteryId + "'";
                    }
                    string manufactureDate = "null";
                    if (transRecord.InsertedTransDevice.ManufactureDate != null)
                    {
                        manufactureDate = "'" + transRecord.InsertedTransDevice.ManufactureDate + "'";
                    }
                    string oId = "null";
                    if (transRecord.DrawOutTransDevice.OID != null)
                    {
                        oId = "'" + transRecord.DrawOutTransDevice.OID + "'";
                    }
                    string uId = "null";
                    if (transRecord.DrawOutTransDevice.UID != null)
                    {
                        uId = "'" + transRecord.DrawOutTransDevice.UID + "'";
                    }
                    string cId = "null";
                    if (transRecord.DrawOutTransDevice.CID != null)
                    {
                        cId = "'" + transRecord.DrawOutTransDevice.CID + "'";
                    }
                    string cellsVoltage = "null";
                    if (transRecord.InsertedTransDevice.CellsVoltage != null)
                    {
                        cellsVoltage = "'" + transRecord.InsertedTransDevice.CellsVoltage + "'";
                    }

                    transRecord.InsertedTransDevice.TransDeviceRecordId = mDBMgr.Insert(string.Format(
                        "INSERT INTO {0} (TransDeviceRecordId,SlotId,BatteryId,ManufactureDate,BatteryTypeId,TransDeviceTime,OID,UID,CID,SOC,CycleCount,CellsVoltage,Protection) VALUES (null, {1},{2},{3},{4},'{5}',{6},{7},{8},{9},{10},{11},{12});"
                        , typeof(TransDeviceRecord).Name, transRecord.InsertedTransDevice.SlotId, batteryId, manufactureDate, transRecord.InsertedTransDevice.BatteryTypeId, transRecord.InsertedTransDevice.TransDeviceTime
                        , oId, uId, cId, transRecord.InsertedTransDevice.SOC, transRecord.InsertedTransDevice.CycleCount, cellsVoltage, transRecord.InsertedTransDevice.Protection));
                    //紀錄
                    //transRecord.InsertedTransDevice.TransDeviceRecordId = mDBMgr.GetLastInsertId();
                    transRecord.InsertedTransDeviceId = transRecord.InsertedTransDevice.TransDeviceRecordId;
                }

                if (transRecord.DrawOutTransDevice != null)
                {
                    string batteryId = "null";
                    if (transRecord.DrawOutTransDevice.BatteryId != null)
                    {
                        batteryId = "'" + transRecord.DrawOutTransDevice.BatteryId + "'";
                    }
                    string manufactureDate = "null";
                    if (transRecord.DrawOutTransDevice.ManufactureDate != null)
                    {
                        manufactureDate = "'" + transRecord.DrawOutTransDevice.ManufactureDate + "'";
                    }
                    string oId = "null";
                    if (transRecord.DrawOutTransDevice.OID != null)
                    {
                        oId = "'" + transRecord.DrawOutTransDevice.OID + "'";
                    }
                    string uId = "null";
                    if (transRecord.DrawOutTransDevice.UID != null)
                    {
                        uId = "'" + transRecord.DrawOutTransDevice.UID + "'";
                    }
                    string cId = "null";
                    if (transRecord.DrawOutTransDevice.CID != null)
                    {
                        cId = "'" + transRecord.DrawOutTransDevice.CID + "'";
                    }
                    string cellsVoltage = "null";
                    if (transRecord.DrawOutTransDevice.CellsVoltage != null)
                    {
                        cellsVoltage = "'" + transRecord.DrawOutTransDevice.CellsVoltage + "'";
                    }

                    transRecord.DrawOutTransDevice.TransDeviceRecordId = mDBMgr.Insert(string.Format(
                        "INSERT INTO {0} (TransDeviceRecordId,SlotId,BatteryId,ManufactureDate,BatteryTypeId,TransDeviceTime,OID,UID,CID,SOC,CycleCount,CellsVoltage,Protection) VALUES (null, {1},{2},{3},{4},'{5}',{6},{7},{8},{9},{10},{11},{12});"
                        , typeof(TransDeviceRecord).Name, transRecord.DrawOutTransDevice.SlotId, batteryId, manufactureDate, transRecord.DrawOutTransDevice.BatteryTypeId, transRecord.DrawOutTransDevice.TransDeviceTime
                        , oId, uId, cId, transRecord.DrawOutTransDevice.SOC, transRecord.DrawOutTransDevice.CycleCount, cellsVoltage, transRecord.DrawOutTransDevice.Protection));

                    //紀錄
                    //transRecord.DrawOutTransDevice.TransDeviceRecordId = mDBMgr.GetLastInsertId();
                    transRecord.DrawOutTransDeviceId = transRecord.DrawOutTransDevice.TransDeviceRecordId;
                }

                string userId = "null";
                if (transRecord.UserId != null)
                {
                    userId = "'" + transRecord.UserId + "'";
                }
                string cardId = "null";
                if (transRecord.CardId != null)
                {
                    cardId = "'" + transRecord.CardId + "'";
                }

                mDBMgr.Update(string.Format(
                    "INSERT INTO {0} (TransRecordId, TransTimeStamp,UserId,CardId,TransStatusCode,OldPoint,Point,InsertedTransDeviceId,DrawOutTransDeviceId,IsUpdate) VALUES (null, '{1}',{2},{3},{4},{5},{6},{7},{8},{9});"
                    , typeof(TransRecord).Name, transRecord.TransTimeStamp, userId, cardId
                    , transRecord.TransStatusCode, transRecord.OldPoint, transRecord.Point
                    , transRecord.InsertedTransDeviceId, transRecord.DrawOutTransDeviceId, 0));
            }
        }
        private void ChargeTransRecordToDB(List<ChargeTransRecord> chargeTransRecordList)
        {
            foreach (ChargeTransRecord ctr in chargeTransRecordList)
            {
                string batteryIdText = "null";
                string manufactureDateText = "null";
                string cellsVoltageText = "null";
                string temperatureText = "null";
                string impedanceText = "null";

                if (ctr.ChargeTransStatusCode == 0)
                {
                    if (ctr.StartChargeTransDeviceRecord != null)
                    {
                        if (ctr.StartChargeTransDeviceRecord.BatteryId != null)
                        {
                            batteryIdText = "'" + ctr.StartChargeTransDeviceRecord.BatteryId + "'";
                        }
                        if (ctr.StartChargeTransDeviceRecord.ManufactureDate != null)
                        {
                            manufactureDateText = "'" + ctr.StartChargeTransDeviceRecord.ManufactureDate + "'";
                        }
                        if (ctr.StartChargeTransDeviceRecord.CellsVoltage != null)
                        {
                            cellsVoltageText = "'" + ctr.StartChargeTransDeviceRecord.CellsVoltage + "'";
                        }
                        if (ctr.StartChargeTransDeviceRecord.CellsTemperature != null)
                        {
                            temperatureText = "'" + ctr.StartChargeTransDeviceRecord.CellsTemperature + "'";
                        }
                        if (ctr.StartChargeTransDeviceRecord.Impedance != null)
                        {
                            impedanceText = "'" + ctr.StartChargeTransDeviceRecord.Impedance + "'";
                        }

                        ctr.StartChargeTransDeviceRecord.ChargeTransDeviceRecordId = mDBMgr.Insert(string.Format(
                         "INSERT INTO {0} (ChargeTransDeviceRecordId,ChargeTimeStamp,BatteryId,ManufactureDate,BatteryTypeId,SOC,Voltage,CellsVoltage,CellsTemperature,FCC,RC,Impedance,CycleCount,Protection,SOH,CellVoltDiff) VALUES (null,'{1}',{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15});"
                         , typeof(ChargeTransDeviceRecord).Name, ctr.StartChargeTransDeviceRecord.ChargeTimeStamp
                         , batteryIdText, manufactureDateText, ctr.StartChargeTransDeviceRecord.BatteryTypeId, ctr.StartChargeTransDeviceRecord.SOC, ctr.StartChargeTransDeviceRecord.Voltage
                         , cellsVoltageText, temperatureText, ctr.StartChargeTransDeviceRecord.FCC, ctr.StartChargeTransDeviceRecord.RC, impedanceText
                         , ctr.StartChargeTransDeviceRecord.CycleCount, ctr.StartChargeTransDeviceRecord.Protection, ctr.StartChargeTransDeviceRecord.SOH, ctr.StartChargeTransDeviceRecord.CellVoltDiff));

                        //ctr.StartChargeTransDeviceRecord.ChargeTransDeviceRecordId = mDBMgr.GetLastInsertId();
                        ctr.StartChargeTransDeviceRecordId = ctr.StartChargeTransDeviceRecord.ChargeTransDeviceRecordId;

                        ctr.ChargeTransRecordId = mDBMgr.Insert(string.Format(
                            "INSERT INTO {0} (ChargeTransRecordId,SlotId,ChargeTransTimeStamp,BatteryTypeId,StartChargeTransDeviceRecordId,EndChargeTransDeviceRecordId, ChargedCapacity, TotalTime, ChargeTransStatusCode, IsUpdate) VALUES (null,{1},'{2}',{3},{4},{5},{6},{7},{8},{9});"
                            , typeof(ChargeTransRecord).Name, ctr.SlotId, ctr.ChargeTransTimeStamp, ctr.BatteryTypeId, ctr.StartChargeTransDeviceRecordId, ctr.EndChargeTransDeviceRecordId, ctr.ChargedCapacity, ctr.TotalTime, ctr.ChargeTransStatusCode, 0));

                        //ctr.ChargeTransRecordId = mDBMgr.GetLastInsertId();
                    }
                }
                else
                {
                    if (ctr.ChargeTransStatusCode == 1)
                    {
                        if (ctr.EndChargeTransDeviceRecord != null)
                        {
                            if (ctr.EndChargeTransDeviceRecord.BatteryId != null)
                            {
                                batteryIdText = "'" + ctr.EndChargeTransDeviceRecord.BatteryId + "'";
                            }
                            if (ctr.EndChargeTransDeviceRecord.ManufactureDate != null)
                            {
                                manufactureDateText = "'" + ctr.EndChargeTransDeviceRecord.ManufactureDate + "'";
                            }
                            if (ctr.EndChargeTransDeviceRecord.CellsVoltage != null)
                            {
                                cellsVoltageText = "'" + ctr.EndChargeTransDeviceRecord.CellsVoltage + "'";
                            }
                            if (ctr.EndChargeTransDeviceRecord.CellsTemperature != null)
                            {
                                temperatureText = "'" + ctr.EndChargeTransDeviceRecord.CellsTemperature + "'";
                            }
                            if (ctr.EndChargeTransDeviceRecord.Impedance != null)
                            {
                                impedanceText = "'" + ctr.EndChargeTransDeviceRecord.Impedance + "'";
                            }

                            ctr.EndChargeTransDeviceRecord.ChargeTransDeviceRecordId = mDBMgr.Insert(string.Format(
                             "INSERT INTO {0} (ChargeTransDeviceRecordId,ChargeTimeStamp,BatteryId,ManufactureDate,BatteryTypeId,SOC,Voltage,CellsVoltage,CellsTemperature,FCC,RC,Impedance,CycleCount,Protection,SOH,CellVoltDiff) VALUES (null,'{1}',{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15});"
                             , typeof(ChargeTransDeviceRecord).Name, ctr.EndChargeTransDeviceRecord.ChargeTimeStamp
                             , batteryIdText, manufactureDateText, ctr.EndChargeTransDeviceRecord.BatteryTypeId, ctr.EndChargeTransDeviceRecord.SOC, ctr.EndChargeTransDeviceRecord.Voltage
                             , cellsVoltageText, temperatureText, ctr.EndChargeTransDeviceRecord.FCC, ctr.EndChargeTransDeviceRecord.RC, impedanceText
                             , ctr.EndChargeTransDeviceRecord.CycleCount, ctr.EndChargeTransDeviceRecord.Protection, ctr.EndChargeTransDeviceRecord.SOH, ctr.EndChargeTransDeviceRecord.CellVoltDiff));

                            //ctr.EndChargeTransDeviceRecord.ChargeTransDeviceRecordId = mDBMgr.GetLastInsertId();
                            ctr.EndChargeTransDeviceRecordId = ctr.EndChargeTransDeviceRecord.ChargeTransDeviceRecordId;
                        }
                    }

                    mDBMgr.Update(string.Format(
                        "UPDATE  {0} SET EndChargeTransDeviceRecordId={1}, ChargedCapacity={2}, TotalTime={3}, ChargeTransStatusCode={4} WHERE ChargeTransRecordId={5};"
                        , typeof(ChargeTransRecord).Name, ctr.EndChargeTransDeviceRecordId, ctr.ChargedCapacity, ctr.TotalTime
                        , ctr.ChargeTransStatusCode, ctr.ChargeTransRecordId));
                }
            }
        }
        private void AbnormalRecordToDB(List<AbnormalRecord> abnormalRecordList)
        {
            foreach (AbnormalRecord ar in abnormalRecordList)
            {
                string batteryIdText = "null";
                string manufactureDateText = "null";
                string cellsVoltageText = "null";
                string temperatureText = "null";
                string impedanceText = "null";

                if (ar.AbnormalDeviceRecord != null)
                {
                    if (ar.AbnormalDeviceRecord.BatteryId != null)
                    {
                        batteryIdText = "'" + ar.AbnormalDeviceRecord.BatteryId + "'";
                    }
                    if (ar.AbnormalDeviceRecord.ManufactureDate != null)
                    {
                        manufactureDateText = "'" + ar.AbnormalDeviceRecord.ManufactureDate + "'";
                    }
                    if (ar.AbnormalDeviceRecord.CellsVoltage != null)
                    {
                        cellsVoltageText = "'" + ar.AbnormalDeviceRecord.CellsVoltage + "'";
                    }
                    if (ar.AbnormalDeviceRecord.CellsTemperature != null)
                    {
                        temperatureText = "'" + ar.AbnormalDeviceRecord.CellsTemperature + "'";
                    }
                    if (ar.AbnormalDeviceRecord.Impedance != null)
                    {
                        impedanceText = "'" + ar.AbnormalDeviceRecord.Impedance + "'";
                    }

                    ar.AbnormalDeviceRecord.AbnormalDeviceRecordId = mDBMgr.Insert(string.Format(
                         "INSERT INTO {0} (AbnormalDeviceRecordId,BatteryId,ManufactureDate,BatteryTypeId,SOC,CellsVoltage,CellsTemperature,FCC,RC,Impedance,CycleCount,Protection) VALUES (null,{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11});"
                         , typeof(AbnormalDeviceRecord).Name, batteryIdText, manufactureDateText, ar.AbnormalDeviceRecord.BatteryTypeId
                         , ar.AbnormalDeviceRecord.SOC, cellsVoltageText, temperatureText, ar.AbnormalDeviceRecord.FCC, ar.AbnormalDeviceRecord.RC, impedanceText
                         , ar.AbnormalDeviceRecord.CycleCount, ar.AbnormalDeviceRecord.Protection));

                    //ar.AbnormalDeviceRecord.AbnormalDeviceRecordId = mDBMgr.GetLastInsertId();
                    ar.AbnormalDeviceRecordId = ar.AbnormalDeviceRecord.AbnormalDeviceRecordId;
                }

                ar.AbnormalRecordId = mDBMgr.Insert(string.Format(
                    "INSERT INTO {0} (AbnormalRecordId,SlotId,AbnormalStatusCode,AbnormalTimeStamp, AbnormalDeviceRecordId, IsUpdate) VALUES (null,{1},{2},'{3}',{4},{5});"
                    , typeof(AbnormalRecord).Name, ar.SlotId, ar.AbnormalStatusCode, ar.AbnormalTimeStamp
                    , ar.AbnormalDeviceRecordId, 0));

                //ar.AbnormalRecordId = mDBMgr.GetLastInsertId();
            }
        }
        #endregion
        #region Upload Data
        public void InitOnlineSetting()
        {
            WebConnector.ServerCallbackMessage += WebConnector_ServerCallbackMessage;
        }
        public void AutoUploadData()
        {
            while (true)
            {
                //UploadData();

                Thread.Sleep(KioskParameter.UPLOADWAITINGTIME * 1000); //60秒
            }
        }
        private void UploadMessage(string timeStamp, int actionCode, string message, bool isCheckOnlineStatus, bool isCipher)
        {
            if (isCheckOnlineStatus)
            {
                ConfigInfo deviceId = GetConfigInfoByName(KioskParameter.DEVICEID);
                //ConfigInfo stationManufactureDate = GetConfigInfoByName(KioskParameter.STATIONMANUFACTUREDATE);
                ConfigInfo deviceTypeId = GetConfigInfoByName(KioskParameter.DEVICETYPEID);

                if (isCipher)
                {
                    ConfigInfo securtiyKey = GetConfigInfoByName(KioskParameter.SECURITYKEY);
                    if (securtiyKey != null && securtiyKey.Value != "")
                    {
                        message = message;
                    }                    
                }

                if (deviceId != null && deviceTypeId != null)
                {
                    DeviceUploadFormat deviceMessageFormat = new DeviceUploadFormat()
                    {
                        DeviceId = deviceId.Value,
                        //StationManufactureDate = stationManufactureDate.Value,
                        DeviceTypeId = deviceTypeId.Value,
                        TimeStamp = timeStamp,
                        ActionCode = actionCode,
                        Message = message
                    };
                    WebConnector.ServerPost("http://" + MainWindow.AppSettingFile.ServerHost + ":" + MainWindow.AppSettingFile.ServerPort + KioskParameter.UPLOADAPI, deviceMessageFormat);
                }
            }
        }

        public void LoginServer()
        {
            LoginFormat lf = new LoginFormat();

            ConfigInfo password = GetConfigInfoByName(KioskParameter.PASSWORD);

            if (password != null)
            {
                lf.Password = password.Value;

                string timeStamp = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                UploadMessage(timeStamp, (int)UploadActionCode.DeviceLogin, Newtonsoft.Json.JsonConvert.SerializeObject(lf), true, false);
            }
        }
        public bool CheckKioskActive()
        {
            LoginServer();
            int waitingTime = 0;
            IsCheckOnline = false;
            bool isActive = false;

            ConfigInfo datetime = GetConfigInfoByName(KioskParameter.ACTIVESDAY);
            if (datetime != null && datetime.Value != null && !datetime.Value.Equals(""))
            {
                DateTime dt = DateTime.ParseExact(datetime.Value, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture);

                isActive = (DateTime.UtcNow < dt.AddDays(7));
            }

            if (!isActive)
            {
                while (!IsCheckOnline)
                {
                    waitingTime++;
                    Thread.Sleep(1000);

                    if (waitingTime > 5)
                    {
                        break;
                    }
                }

                datetime = GetConfigInfoByName(KioskParameter.ACTIVESDAY);
                if (datetime != null && datetime.Value != null && !datetime.Value.Equals(""))
                {
                    DateTime dt = DateTime.ParseExact(datetime.Value, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture);

                    isActive = (DateTime.UtcNow < dt.AddDays(7));
                }
            }

            return isActive;
        }

        private void WebConnector_ServerCallbackMessage(bool isSuccessResponse, string result, DeviceUploadFormat message)
        {
            if (isSuccessResponse)
            {
                ServerResponseFormat responseData = null;
                using (TextReader file = new StringReader(result))
                {
                    try
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        responseData = (ServerResponseFormat)serializer.Deserialize(file, typeof(ServerResponseFormat));
                    }
                    catch
                    {
                        // output
                    }
                }

                if (responseData != null)
                {
                    switch (responseData.ActionCode)
                    {
                        case 0:
                            {
                                switch (message.ActionCode)
                                {
                                    case 2:
                                        {
                                            mSlotStatusRecordMessage = message.Message;
                                            break;
                                        }
                                    case 3:
                                        {
                                            mTransRecordMessage = message.Message;

                                            break;
                                        }
                                    case 4:
                                        {
                                            mChargeTransRecordMessage = message.Message;

                                            break;
                                        }
                                    case 5:
                                        {
                                            mAbnormalRecordMessage = message.Message;

                                            break;
                                        }
                                    default:
                                        {
                                            break;
                                        }
                                }

                                break;
                            }
                        case 99:
                            {
                                if (message.ActionCode == 1)
                                {
                                    using (TextReader file = new StringReader(responseData.Message))
                                    {
                                        JsonSerializer serializer = new JsonSerializer();
                                        LoginResponseFormat lrf = (LoginResponseFormat)serializer.Deserialize(file, typeof(LoginResponseFormat));

                                        SetConfigInfoValue(KioskParameter.SECURITYKEY, lrf.SecurityKey);
                                        SetConfigInfoValue(KioskParameter.ACTIVESDAY, DateTime.UtcNow.ToString("yyyyMMddHHmmss"));
                                        SaveConfigInfoToDB();
                                        mIsUpdateConfigInfoToDB = true;
                                        IsCheckOnline = true;
                                    }
                                }

                                break;
                            }
                    }
                }
            }
        }

        private void UploadData()
        {
            string timeStamp = DateTime.UtcNow.ToString("yyyyMMddHHmmss");

            //if (mDBMgr.OpenDB())
            //{
            if (mIsUpdateConfigInfoToDB)
            {
                SaveConfigInfoToDB();
                mIsUpdateConfigInfoToDB = false;
            }

            UpdateSlotStatusDataToDB(mDBMgr, mSlotStatusRecordMessage);
            UpdateTransDataToDB(mDBMgr, mTransRecordMessage);
            UpdateDeviceChargeTransDataToDB(mDBMgr, mChargeTransRecordMessage);
            UpdateAbnormalDeviceDataToDB(mDBMgr, mAbnormalRecordMessage);

            if (IsCheckOnline)
            {
                List<object> tempData = GetUploadSlotStatusData(mDBMgr);
                if (tempData != null && tempData.Count > 0)
                {
                    UploadMessage(timeStamp, (int)UploadActionCode.ChargeDeviceSlotRecord, Newtonsoft.Json.JsonConvert.SerializeObject(tempData), IsCheckOnline, true);
                }
                tempData = GetUploadTransData(mDBMgr);
                if (tempData != null && tempData.Count > 0)
                {
                    UploadMessage(timeStamp, (int)UploadActionCode.TransRecord, Newtonsoft.Json.JsonConvert.SerializeObject(tempData), IsCheckOnline, true);
                }
                tempData = GetUploadDeviceChargeTransData(mDBMgr);
                if (tempData != null && tempData.Count > 0)
                {
                    UploadMessage(timeStamp, (int)UploadActionCode.ChargeTransRecord, Newtonsoft.Json.JsonConvert.SerializeObject(tempData), IsCheckOnline, true);
                }
                tempData = GetUploadAbnormalDeviceData(mDBMgr);
                if (tempData != null && tempData.Count > 0)
                {
                    UploadMessage(timeStamp, (int)UploadActionCode.AbnormalRecord, Newtonsoft.Json.JsonConvert.SerializeObject(tempData), IsCheckOnline, true);
                }
            }
            //    mDBMgr.CloseDB();
            //}
        }
        private List<object> GetUploadSlotStatusData(DBMgr dbMgr)
        {
            List<object> tempData = dbMgr.Read(string.Format("SELECT * FROM {0} WHERE IsUpdate = {1} LIMIT 0,200", typeof(SlotRecord).Name, 0), typeof(SlotRecord));
            if (tempData != null)
            {
                foreach (SlotRecord sr in tempData)
                {
                    if (sr.SlotBatteryRecordId > 0)
                    {
                        List<object> deviceData = dbMgr.Read(string.Format("SELECT * FROM {0} WHERE SlotBatteryRecordId = {1}", typeof(SlotBatteryRecord).Name, sr.SlotBatteryRecordId), typeof(SlotBatteryRecord));
                        if (deviceData != null && deviceData.Count > 0)
                        {
                            sr.SlotBatteryRecord = deviceData[0] as SlotBatteryRecord;
                        }
                    }
                }
            }

            return tempData;
        }
        private List<object> GetUploadTransData(DBMgr dbMgr)
        {
            List<object> tempData = dbMgr.Read(string.Format("SELECT * FROM {0} WHERE IsUpdate = {1} LIMIT 0,200", typeof(TransRecord).Name, 0), typeof(TransRecord));
            if (tempData != null)
            {
                foreach (TransRecord tr in tempData)
                {
                    if (tr.InsertedTransDeviceId != 0 && tr.DrawOutTransDeviceId != 0)
                    {
                        List<object> deviceData = dbMgr.Read(string.Format("SELECT * FROM {0} WHERE TransDeviceRecordId = {1} OR TransDeviceRecordId = {2}", typeof(TransDeviceRecord).Name, tr.InsertedTransDeviceId, tr.DrawOutTransDeviceId), typeof(TransDeviceRecord));
                        if (deviceData != null)
                        {
                            foreach (TransDeviceRecord tdr in deviceData)
                            {
                                if (tdr.TransDeviceRecordId == tr.InsertedTransDeviceId)
                                {
                                    tr.InsertedTransDevice = tdr;
                                }
                                if (tdr.TransDeviceRecordId == tr.DrawOutTransDeviceId)
                                {
                                    tr.DrawOutTransDevice = tdr;
                                }
                            }
                        }
                    }
                }
            }

            return tempData;
        }
        private List<object> GetUploadDeviceChargeTransData(DBMgr dbMgr)
        {
            List<object> tempData = dbMgr.Read(string.Format("SELECT * FROM {0} WHERE ChargeTransStatusCode > {1} AND IsUpdate = {2} LIMIT 0,200", typeof(ChargeTransRecord).Name, 0, 0), typeof(ChargeTransRecord));
            if (tempData != null)
            {
                foreach (ChargeTransRecord ctr in tempData)
                {
                    if (ctr.StartChargeTransDeviceRecordId != 0 && ctr.EndChargeTransDeviceRecordId != 0)
                    {
                        List<object> deviceData = dbMgr.Read(string.Format("SELECT * FROM {0} WHERE ChargeTransDeviceRecordId = {1} OR ChargeTransDeviceRecordId = {2}", typeof(ChargeTransDeviceRecord).Name, ctr.StartChargeTransDeviceRecordId, ctr.EndChargeTransDeviceRecordId), typeof(ChargeTransDeviceRecord));
                        if (deviceData != null)
                        {
                            foreach (ChargeTransDeviceRecord ctdr in deviceData)
                            {
                                if (ctdr.ChargeTransDeviceRecordId == ctr.StartChargeTransDeviceRecordId)
                                {
                                    ctr.StartChargeTransDeviceRecord = ctdr;
                                }
                                if (ctdr.ChargeTransDeviceRecordId == ctr.EndChargeTransDeviceRecordId)
                                {
                                    ctr.EndChargeTransDeviceRecord = ctdr;
                                }
                            }
                        }
                    }
                }
            }

            return tempData;
        }
        private List<object> GetUploadAbnormalDeviceData(DBMgr dbMgr)
        {
            List<object> tempData = dbMgr.Read(string.Format("SELECT * FROM {0} WHERE IsUpdate = {1} LIMIT 0,200", typeof(AbnormalRecord).Name, 0), typeof(AbnormalRecord));
            if (tempData != null)
            {
                foreach (AbnormalRecord ar in tempData)
                {
                    if (ar.AbnormalDeviceRecordId != 0)
                    {
                        List<object> deviceData = dbMgr.Read(string.Format("SELECT * FROM {0} WHERE AbnormalDeviceRecordId = {1}", typeof(AbnormalDeviceRecord).Name, ar.AbnormalDeviceRecordId), typeof(AbnormalDeviceRecord));
                        if (deviceData != null)
                        {
                            foreach (AbnormalDeviceRecord adr in deviceData)
                            {
                                if (adr.AbnormalDeviceRecordId == ar.AbnormalDeviceRecordId)
                                {
                                    ar.AbnormalDeviceRecord = adr;
                                }
                            }
                        }
                    }
                }
            }

            return tempData;
        }

        private void UpdateSlotStatusDataToDB(DBMgr dbMgr, string data)
        {
            if (data != null)
            {
                using (TextReader file = new StringReader(data))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    List<SlotRecord> uploadSlotData = (List<SlotRecord>)serializer.Deserialize(file, typeof(List<SlotRecord>));

                    foreach (SlotRecord sr in uploadSlotData)
                    {
                        dbMgr.Update(string.Format(
                            "UPDATE  {0} SET IsUpdate={1} WHERE SlotRecordId={2};"
                            , typeof(SlotRecord).Name, 1, sr.SlotRecordId));
                    }
                }
            }
        }
        private void UpdateTransDataToDB(DBMgr dbMgr, string data)
        {
            if (data != null)
            {
                using (TextReader file = new StringReader(data))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    List<TransRecord> uploadTransData = (List<TransRecord>)serializer.Deserialize(file, typeof(List<TransRecord>));

                    foreach (TransRecord tr in uploadTransData)
                    {
                        dbMgr.Update(string.Format(
                            "UPDATE  {0} SET IsUpdate={1} WHERE TransRecordId={2};"
                            , typeof(TransRecord).Name, 1, tr.TransRecordId));
                    }
                }
            }
        }
        private void UpdateDeviceChargeTransDataToDB(DBMgr dbMgr, string data)
        {
            if (data != null)
            {
                using (TextReader file = new StringReader(data))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    List<ChargeTransRecord> uploadChargeTransRecord = (List<ChargeTransRecord>)serializer.Deserialize(file, typeof(List<ChargeTransRecord>));

                    foreach (ChargeTransRecord ctr in uploadChargeTransRecord)
                    {
                        dbMgr.Update(string.Format(
                            "UPDATE  {0} SET IsUpdate={1} WHERE ChargeTransRecordId={2};"
                            , typeof(ChargeTransRecord).Name, 1, ctr.ChargeTransRecordId));
                    }
                }
            }
        }
        private void UpdateAbnormalDeviceDataToDB(DBMgr dbMgr, string data)
        {
            if (data != null)
            {
                using (TextReader file = new StringReader(data))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    List<AbnormalRecord> uploadAbnormalRecord = (List<AbnormalRecord>)serializer.Deserialize(file, typeof(List<AbnormalRecord>));

                    foreach (AbnormalRecord ar in uploadAbnormalRecord)
                    {
                        dbMgr.Update(string.Format(
                            "UPDATE  {0} SET IsUpdate={1} WHERE AbnormalRecordId={2};"
                            , typeof(AbnormalRecord).Name, 1, ar.AbnormalRecordId));
                    }
                }
            }
        }
        #endregion

        private void SetVoltageData(SlotBatteryRecord slotBatteryRecord, int[] cellsVoltage)
        {
            if (cellsVoltage != null)
            {
                for (int i = 0; i < cellsVoltage.Length; i++)
                {
                    slotBatteryRecord.Voltage = slotBatteryRecord.Voltage + cellsVoltage[i];
                    if (i == 0)
                    {
                        slotBatteryRecord.VMin = cellsVoltage[i];
                        slotBatteryRecord.VMax = cellsVoltage[i];
                    }
                    else
                    {
                        if (cellsVoltage[i] < slotBatteryRecord.VMin)
                        {
                            slotBatteryRecord.VMin = cellsVoltage[i];
                        }
                        if (cellsVoltage[i] > slotBatteryRecord.VMax)
                        {
                            slotBatteryRecord.VMax = cellsVoltage[i];
                        }
                    }
                }
            }
        }

        private void SetVoltageData(ChargeTransDeviceRecord chargeTransDeviceRecord, int[] cellsVoltage)
        {
            if (cellsVoltage != null)
            {
                int vmax = 0;
                int vmin = 0;
                for (int i = 0; i < cellsVoltage.Length; i++)
                {
                    chargeTransDeviceRecord.Voltage = chargeTransDeviceRecord.Voltage + cellsVoltage[i];
                    if (i == 0)
                    {
                        vmax = cellsVoltage[i];
                        vmin = cellsVoltage[i];
                    }
                    else
                    {
                        if (cellsVoltage[i] < vmin)
                        {
                            vmin = cellsVoltage[i];
                        }
                        if (cellsVoltage[i] > vmax)
                        {
                            vmax = cellsVoltage[i];
                        }
                    }
                }

                chargeTransDeviceRecord.CellVoltDiff = vmax - vmin;
            }
        }

        private void SetTempData(SlotBatteryRecord slotBatteryRecord, double[] cellsTemp)
        {
            if (cellsTemp != null)
            {
                for (int i = 0; i < cellsTemp.Length; i++)
                {
                    if (i == 0)
                    {
                        slotBatteryRecord.Temperature = cellsTemp[i];
                    }
                    else
                    {
                        if (cellsTemp[i] > slotBatteryRecord.Temperature)
                        {
                            slotBatteryRecord.Temperature = cellsTemp[i];
                        }
                    }
                }
            }
        }

        public bool EnableTrans()
        {
            // Fixed by Neil 2016.07.19
            //return (EmptySlotItem != null && UseableSlotItem != null);
            return (EmptySlotItem != null && UseableSlotItemList.Count > 0);
        }

        public void EndTrans(TransStatusCode transStatusCode)
        {
            IsRefreshRFID = false; //結束偵測RFID

            mTransRecord.TransTimeStamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
            mTransRecord.TransStatusCode = (int)transStatusCode;
            if (mCardInfo != null)
            {
                mTransRecord.UserId = mCardInfo.UserId;
                mTransRecord.CardId = mCardInfo.CardId;
            }

            // Fixed by Neil 2016.07.19
            //EmptySlotItem = null;
            //UseableSlotItem = null;
            //ScanSlotInfo(2); //重新更新資料
            if (transStatusCode == TransStatusCode.KioskTransSuccess)
            {
                EmptySlotItem = UseableSlotItem;
                if(UseableSlotItemList.Count > 0){
                    UseableSlotItem = UseableSlotItemList.FirstOrDefault();
                    UseableSlotItemList.Remove(UseableSlotItem);
                }
                else
                {
                    UseableSlotItem = null;
                }
            }
            //

            mTransRecord = new TransRecord();//重新產生新的交易紀錄提供下次使用
            //mTransRecord.TransStatusCode = -1;
            mCardInfo = null;
        }

        /// <summary>
        /// 確認卡片資訊是否正確
        /// </summary>
        /// <param name="cardInfo"></param>
        /// <returns></returns>
        public bool CheckCardInfo(CardInfo cardInfo)
        {
            bool isSuccess = false;
            mCardInfo = cardInfo;

            if (cardInfo != null)
            {
                //if (mDBMgr.OpenDB())
                //{
                    List<object> tempData = mDBMgr.Read(string.Format("SELECT * FROM CardInfo WHERE CardId = '{0}' AND UserId = '{1}'", cardInfo.CardId, cardInfo.UserId), typeof(CardInfo));
                    //mDBMgr.CloseDB();
                    if (tempData.Count > 0)
                    {
                        isSuccess = true;
                    }
                //}
            }

            return isSuccess;
        }

        /// <summary>
        /// 確認電池和卡片資訊是否相符
        /// </summary>
        /// <returns>Status Code</returns>
        public int CheckTransIdInfo()
        {
            int statusCode = -1;

            BatteryTransIdInfo emptyBTII = null;
            BatteryTransIdInfo useableBTII = null;

            bool isDetectedDevice = false;
            for (int i = 0; i < 7; i++)// 讀取的次數
            {
                emptyBTII = mDeviceAdapter.GetBatteryTransIdBySlotId(EmptySlotItem.SlotId);
                useableBTII = mDeviceAdapter.GetBatteryTransIdBySlotId(UseableSlotItem.SlotId);

                isDetectedDevice = (emptyBTII != null) && (useableBTII != null);

                if (isDetectedDevice)
                {
                    break;
                }
                else
                {
                    Thread.Sleep(1000); //調整讀取電池時間 有時候讀取很久
                }
            }

            if (isDetectedDevice)
            {
                if (mTransCheck != null)
                {
                    statusCode = mTransCheck.CheckTransIdInfo(emptyBTII, mCardInfo);
                    if (statusCode == 0)
                    {
                        if (mTransCheck.IsCopyIdToDevice)
                        {
                            if (!(SetTransIdInfo(UseableSlotItem.SlotId, emptyBTII)))
                            {
                                statusCode = -3;
                            }
                        }
                        else
                        {
                            statusCode = 0;
                        }
                    }

                    // 交易紀錄
                    BatteryStatusInfo emptySlotBSI = mDeviceAdapter.GetBatteryStatusBySlotId(EmptySlotItem.SlotId);
                    BatteryStatusInfo useableSlotBSI = mDeviceAdapter.GetBatteryStatusBySlotId(UseableSlotItem.SlotId);

                    mTransRecord.InsertedTransDevice = ConvertToTransDeviceRecord(EmptySlotItem.SlotId, emptyBTII, emptySlotBSI);
                    mTransRecord.DrawOutTransDevice = ConvertToTransDeviceRecord(UseableSlotItem.SlotId, useableBTII, useableSlotBSI);
                }
            }

            return statusCode;
        }

        private TransDeviceRecord ConvertToTransDeviceRecord(int slotId, BatteryTransIdInfo btii, BatteryStatusInfo bsi)
        {
            TransDeviceRecord tdr = null;
            if (bsi != null)
            {
                string transDeviceTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                tdr = new TransDeviceRecord()
                {
                    SlotId = slotId,
                    BatteryId = btii.BatteryId,
                    ManufactureDate = mDBMgr.ConvertManufactureDate(btii.ManufactureDate),
                    BatteryTypeId = MainWindow.BATTERYTYPEID,
                    TransDeviceTime = transDeviceTime,
                    OID = btii.OperatorId,
                    UID = btii.UserId,
                    CID = btii.CardId,
                    SOC = bsi.SOC,
                    CycleCount = bsi.CycleCount,
                    Protection = mDBMgr.ConvertProtection(bsi.Protection),
                    CellsVoltage = mDBMgr.ConvertCellsVoltage(bsi.CellsVoltage)
                };
            }

            return tdr;
        }

        private bool SetTransIdInfo(int slotId, BatteryTransIdInfo batteryTransIdInfo)
        {
            bool isSuccess = false;

            for (int i = 0; i < 3; i++)
            {
                if (mDeviceAdapter.SetBatteryTransIdBySlotId(slotId, batteryTransIdInfo))
                {
                    isSuccess = true;
                    break;
                }
            }

            return isSuccess;
        }

        public bool ScanDrawOutDevice(int slotId)
        {
            ChargeSlotItem slotItem = mDeviceAdapter.GetSlotItemById(slotId);

            bool isSuccessDrawOut = false;
            if (slotItem != null)
            {
                DateTime initDt = DateTime.Now;
                DateTime dt = DateTime.Now;

                int switchTime = 1000;
                bool enableLight = true;
                mDeviceAdapter.SetGateStatusBySlotId(slotItem.SlotId, KioskGateStatusCmd.BlueLightOn);
                IsKeepRunThread = true;

                if (mDeviceType != null && mDeviceType.Contains("4816"))
                {
                    mDeviceAdapter.SetGateStatusBySlotId(slotItem.SlotId, KioskGateStatusCmd.BackUnLockOn);
                }
                else
                {
                    mDeviceAdapter.SetGateStatusBySlotId(slotItem.SlotId, KioskGateStatusCmd.FrontUnLockOn);
                }

                while (IsKeepRunThread)
                {
                    Thread.Sleep(50);
                    GateStatusInfo gsi = mDeviceAdapter.GetGateStatusBySlotId(slotItem.SlotId);

                    if (!gsi.IsBatteryInserted)
                    {
                        if (gsi.IsFrontDoorClose)
                        {
                            isSuccessDrawOut = true;
                            IsKeepRunThread = false;
                        }
                        else
                        {
                            if (mDeviceAdapter.GetBatteryVerInfoBySlotId(slotItem.SlotId) == null)
                            {
                                isSuccessDrawOut = true;
                                IsKeepRunThread = false;
                            }
                        }
                    }


                    if (DateTime.Now > initDt.AddSeconds(KioskParameter.TRANSLASTWAITINGTIME))
                    {
                        if (mDeviceType != null && mDeviceType.Contains("4816"))
                        {
                            mDeviceAdapter.SetGateStatusBySlotId(slotItem.SlotId, KioskGateStatusCmd.BackUnLockOff);
                        }
                        else
                        {
                            mDeviceAdapter.SetGateStatusBySlotId(slotItem.SlotId, KioskGateStatusCmd.FrontUnLockOff);
                        }

                        switchTime = 500;
                        initDt = DateTime.Now;
                    }

                    // 閃燈
                    if (DateTime.Now > dt.AddMilliseconds(switchTime))
                    {
                        if (enableLight)
                        {
                            mDeviceAdapter.SetGateStatusBySlotId(slotItem.SlotId, KioskGateStatusCmd.BlueLightOff);
                        }
                        else
                        {
                            mDeviceAdapter.SetGateStatusBySlotId(slotItem.SlotId, KioskGateStatusCmd.BlueLightOn);
                        }
                        enableLight = !enableLight;
                        dt = DateTime.Now;
                    }
                }
            }

            mDeviceAdapter.SetGateStatusBySlotId(slotItem.SlotId, GTEKioskLib.Command.KioskGateStatusCmd.TurnOffAllStatus);

            return isSuccessDrawOut;
        }

        public void TurnOffAllGateStatus()
        {
            foreach (ChargeSlotItem item in mDeviceAdapter.GetSlotItemList())
            {
                mDeviceAdapter.SetGateStatusBySlotId(item.SlotId, KioskGateStatusCmd.TurnOffAllStatus);
            }
        }

        public void ScanAllSlotDevice()
        {
            foreach (ChargeSlotItem item in mDeviceAdapter.GetSlotItemList())
            {
                BatteryVerInfo bvi = mDeviceAdapter.GetBatteryVerInfoBySlotId(item.SlotId);
                BatteryStatusInfo bsi = mDeviceAdapter.GetBatteryStatusBySlotId(item.SlotId);

                for (int i = 0; i < 3; i++)
                {
                    if (bvi == null || bsi == null)
                    {
                        Thread.Sleep(400);

                        bvi = mDeviceAdapter.GetBatteryVerInfoBySlotId(item.SlotId);
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        public bool IsExistAbnormalDevice()
        {
            return (mAbnormalRecordList.Count > 0);
        }
    }
}
