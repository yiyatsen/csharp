﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace GTEKiosk.Mgr
{
    public enum AudioList
    {
        MainMenu,
        CardDetected,
        NoCard,
        NoBattery,
        QueryCash,
        Map,
        DrawoutOldBattery,
        NoDetectedBattery,
        CheckBatteryAndCardError,
        DrawoutNewBattery,
        InsertedBattery,
        CheckBattery,
        TransCancel,
        TransFinish
    }

    public class MediaMgr
    {
        private string mAppPath = System.AppDomain.CurrentDomain.BaseDirectory;
        private MediaPlayer mMediaPlayer = new MediaPlayer();

        public void PlayMedia(string file)
        {
                mMediaPlayer.Stop();
                mMediaPlayer.Open(new Uri(mAppPath + file));
                mMediaPlayer.Play();
        }

        public void StopMedia()
        {
            mMediaPlayer.Stop();
        }

        public string GetLangAudioPath(string lang, string file)
        {
            return string.Format("res\\Audio\\{0}\\{1}.wav", lang, file);
        }
    }
}
