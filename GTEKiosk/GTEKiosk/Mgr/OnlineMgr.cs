﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace GTEKiosk.Mgr
{
    public class OnlineMgr
    {
        private bool mIsOnlineMode { get; set; }
        private bool mIsConnected { get; set; }
        private string mServerURL { get; set; }
        public IHubProxy mHubProxy { get; set; }
        public HubConnection mConnection { get; set; }

        public OnlineMgr()
        {
            mIsOnlineMode = false;
        }

        public void SetOnlineMode(bool isOnlineMode)
        {
            mIsOnlineMode = isOnlineMode;
        }

        public void SetServerURL(string serverURL)
        {
            mServerURL = serverURL;
        }

        private async void ConnectAsync()
        {
            Disconnect();

            mConnection = new HubConnection(mServerURL);
            mConnection.Closed += Connection_Closed;
            mHubProxy = mConnection.CreateHubProxy("MyHub");
            //Handle incoming event from server: use Invoke to write to console from SignalR's thread
            //mHubProxy.On<string, string>("AddMessage", (name, message) =>
            //    //this.Dispatcher.Invoke(() =>
            //    //    RichTextBoxConsole.AppendText(String.Format("{0}: {1}\r", name, message))
            //    //)
            //);

            try
            {
                mIsConnected = true;
                await mConnection.Start();
            }
            catch (HttpRequestException)
            {
                //StatusText.Content = "Unable to connect to server: Start server before connecting clients.";
                //No connection: Don't enable Send button or show chat UI
                return;
            }
        }

        /// <summary>
        /// If the server is stopped, the connection will time out after 30 seconds (default), and the 
        /// Closed event will fire.
        /// </summary>
        private void Connection_Closed()
        {
            //Hide chat UI; show login UI
            //var dispatcher = Application.Current.Dispatcher;
            //dispatcher.Invoke(() => ChatPanel.Visibility = Visibility.Collapsed);
            //dispatcher.Invoke(() => ButtonSend.IsEnabled = false);
            //dispatcher.Invoke(() => StatusText.Content = "You have been disconnected.");
            //dispatcher.Invoke(() => SignInPanel.Visibility = Visibility.Visible);
        }

        public void Disconnect()
        {
            mIsConnected = false;
            if (mConnection != null)
            {
                mConnection.Stop();
                mConnection.Dispose();
            }
        }

        public void SendData(string statio, string data)
        {
            //mHubProxy.Invoke("Send", UserName, TextBoxMessage.Text);
        }


    }
}
