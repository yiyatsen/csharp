﻿using GTEKioskConfig.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace GTEKiosk.Mgr
{
    public class WebConnector
    {
        public delegate void CallbackMessage(bool isSuccessResponse, string result, DeviceUploadFormat message);

        public static event CallbackMessage ServerCallbackMessage;

        public static event CallbackMessage WebResourceCallbackMessage;


        //public static async void ServerGet(string url)
        //{
        //    string result = null;
        //    try
        //    {
        //        using (HttpClient client = new HttpClient())
        //        using (HttpResponseMessage response = await client.GetAsync(url))
        //        using (HttpContent content = response.Content)
        //        {
        //            result = await content.ReadAsStringAsync();
        //        }
        //    }
        //    catch
        //    {
        //        DeviceUploadFormat message = new DeviceUploadFormat()
        //        { 
        //        Message = "ServerShutDown"
        //        };
        //        result = Newtonsoft.Json.JsonConvert.SerializeObject(message).ToString();
        //    }

        //    if (ServerCallbackMessage != null)
        //    {
        //        ServerCallbackMessage(result);
        //    }
        //}

        public static async void ServerPost(string url, DeviceUploadFormat message)
        {
            bool isSuccessResponse = false;
            string result = null;
            try
            {
                //// ... Use HttpClient.
                using (HttpClient client = new HttpClient())
                using (HttpResponseMessage response = await client.PostAsync(url
                    , new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(message).ToString(),
                            Encoding.UTF8, "application/json")))
                using (HttpContent content = response.Content)
                {
                    result = await content.ReadAsStringAsync();
                    isSuccessResponse = true;
                }
            }
            catch
            {
                isSuccessResponse = false;
            }

            if (ServerCallbackMessage != null)
            {
                ServerCallbackMessage(isSuccessResponse, result, message);
            }
        }

        //public static async void WebGet(string url)
        //{
        //    try
        //    {
        //        using (HttpClient client = new HttpClient())
        //        using (HttpResponseMessage response = await client.GetAsync(url))
        //        using (HttpContent content = response.Content)
        //        {
        //            string result = await content.ReadAsStringAsync();

        //            if (WebResourceCallbackMessage != null)
        //            {
        //                WebResourceCallbackMessage(result);
        //            }
        //        }
        //    }
        //    catch { }
        //}
    }
}
