﻿using GTEKiosk.Parameter;
using GTEKioskConfig.InfoModel;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace GTEKiosk.Mgr
{
    public class DBMgr
    {
        private bool isConnectToDB = false;

        private SQLiteConnection conn { get; set; }
        private SQLiteCommand sqlite_cmd { get; set; }

        private string mDBFile = System.AppDomain.CurrentDomain.BaseDirectory + KioskParameter.GOTECHDATABASE;

        public DBMgr()
        {
            if (!File.Exists(mDBFile))
            {
                SQLiteConnection.CreateFile(mDBFile);

                if (InitDB())
                {
                    Update(@"CREATE TABLE IF NOT EXISTS ConfigInfo (Name TEXT PRIMARY KEY, Value TEXT)");

                    Update(@"CREATE TABLE IF NOT EXISTS UserInfo (UserId TEXT PRIMARY KEY, UserRole INTEGER)");

                    Update(@"CREATE TABLE IF NOT EXISTS CardInfo (CardId TEXT PRIMARY KEY, UserId TEXT)");

                    Update(@"CREATE TABLE IF NOT EXISTS BatteryInfo (BatteryId TEXT NOT NULL, ManufactureDate TEXT NOT NULL, "
                        + "StatusCode INTEGER, PRIMARY KEY ( BatteryId, ManufactureDate))");

                    Update(@"CREATE TABLE IF NOT EXISTS SlotRecord "
                        + "(SlotRecordId INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + "TimeStamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL , SlotId INTEGER, SlotStatusCode INTEGER, SlotBatteryRecordId INTEGER, IsUpdate INTEGER)");

                    Update(@"CREATE TABLE IF NOT EXISTS SlotBatteryRecord "
                        + "(SlotBatteryRecordId INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + "BatteryId TEXT, ManufactureDate TEXT, BatteryTypeId INTEGER, SOC INTEGER, RC INTEGER, "
                        + "Temperature FLOAT, Current INTEGER, Voltage INTEGER, VMax INTEGER, VMin INTEGER, Protection INTEGER)");

                    Update(@"CREATE TABLE IF NOT EXISTS TransRecord "
                        + "(TransRecordId INTEGER PRIMARY KEY AUTOINCREMENT, TransTimeStamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL, UserId TEXT, CardId TEXT, OldPoint INTEGER, Point INTEGER, "
                        + "TransStatusCode INTEGER, InsertedTransDeviceId INTEGER, DrawOutTransDeviceId INTEGER, IsUpdate INTEGER)");

                    Update(@"CREATE TABLE IF NOT EXISTS TransDeviceRecord "
                        + "(TransDeviceRecordId INTEGER PRIMARY KEY AUTOINCREMENT, SlotId INTEGER, BatteryId TEXT, "
                        + "ManufactureDate TEXT, BatteryTypeId INTEGER, TransDeviceTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL, "
                        + "OID TEXT, UID TEXT, CID TEXT, SOC INTEGER, CycleCount INTEGER, CellsVoltage TEXT, Protection INTEGER)");

                    Update(@"CREATE TABLE IF NOT EXISTS ChargeTransRecord "
                        + "(ChargeTransRecordId INTEGER PRIMARY KEY AUTOINCREMENT,ChargeTransTimeStamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL, BatteryTypeId INTEGER, SlotId INTEGER, "
                        + "StartChargeTransDeviceRecordId INTEGER, EndChargeTransDeviceRecordId INTEGER, ChargeTransStatusCode INTEGER, ChargedCapacity INTEGER, TotalTime INTEGER, IsUpdate INTEGER)");

                    Update(@"CREATE TABLE IF NOT EXISTS ChargeTransDeviceRecord "
                        + "(ChargeTransDeviceRecordId INTEGER PRIMARY KEY AUTOINCREMENT, ChargeTimeStamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL, "
                        + "BatteryId TEXT, ManufactureDate TEXT, BatteryTypeId INTEGER, SOC INTEGER, CellsVoltage TEXT, Voltage INTEGER, CellsTemperature TEXT, "
                        + "FCC INTEGER, RC INTEGER, Impedance TEXT, CycleCount INTEGER, Protection INTEGER, SOH INTEGER, CellVoltDiff INTEGER)");

                    Update(@"CREATE TABLE IF NOT EXISTS AbnormalRecord "
                        + "(AbnormalRecordId INTEGER PRIMARY KEY AUTOINCREMENT, SlotId INTEGER, AbnormalStatusCode INTEGER, "
                        + "AbnormalTimeStamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL, AbnormalDeviceRecordId INTEGER, IsUpdate INTEGER)");

                    Update(@"CREATE TABLE IF NOT EXISTS AbnormalDeviceRecord "
                        + "(AbnormalDeviceRecordId INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + "BatteryId TEXT, ManufactureDate TEXT, BatteryTypeId INTEGER, SOC INTEGER, CellsVoltage TEXT, CellsTemperature TEXT, "
                        + "FCC INTEGER, RC INTEGER, Impedance TEXT, CycleCount INTEGER, Protection INTEGER)");

                    UserInfo userInfo = new UserInfo(){
                        UserId = "0000000001",
                        UserRole = 16
                    };

                    CardInfo cardInfo = new CardInfo()
                    {
                        UserId = "0000000001",
                        CardId = "3C8483D6"
                    };

                    Update(string.Format("INSERT INTO {0} VALUES ('{1}',{2});", typeof(UserInfo).Name, userInfo.UserId, userInfo.UserRole));
                    Update(string.Format("INSERT INTO {0} VALUES ('{1}','{2}');", typeof(CardInfo).Name, cardInfo.CardId, cardInfo.UserId));

                    userInfo = new UserInfo()
                    {
                        UserId = "0000000003",
                        UserRole = 2
                    };

                    cardInfo = new CardInfo()
                    {
                        UserId = "0000000003",
                        CardId = "12345678"
                    };

                    Update(string.Format("INSERT INTO {0} VALUES ('{1}',{2});", typeof(UserInfo).Name, userInfo.UserId, userInfo.UserRole));
                    Update(string.Format("INSERT INTO {0} VALUES ('{1}','{2}');", typeof(CardInfo).Name, cardInfo.CardId, cardInfo.UserId));


                    cardInfo = new CardInfo()
                    {
                        UserId = "0000000003",
                        CardId = "87654321"
                    };
                    Update(string.Format("INSERT INTO {0} VALUES ('{1}','{2}');", typeof(CardInfo).Name, cardInfo.CardId, cardInfo.UserId));


                    List<ConfigInfo> configInfoList = new List<ConfigInfo>();
                    configInfoList.Add(new ConfigInfo()
                    {
                        Name = KioskParameter.REGISTERTIME,
                        Value = ""//DateTime.UtcNow.ToString("yyyyMMddHHmmss")
                    });
                    configInfoList.Add(new ConfigInfo()
                    {
                        Name = KioskParameter.REGISTERDEVICE,
                        Value = ""//KioskParameter.SUCCESS
                    });

                    configInfoList.Add(new ConfigInfo()
                    {
                        Name = KioskParameter.DEVICETYPEID,
                        Value = "1"
                    });
                    configInfoList.Add(new ConfigInfo()
                    {
                        Name = KioskParameter.DEVICEID,
                        Value = ""//"1212010001"
                    });
                    //configInfoList.Add(new ConfigInfo()
                    //{
                    //    Name = KioskParameter.STATIONMANUFACTUREDATE,
                    //    Value = ""//"2015/01/01"
                    //});
                    configInfoList.Add(new ConfigInfo()
                    {
                        Name = KioskParameter.PASSWORD,
                        Value = ""//"test123"
                    });
                    configInfoList.Add(new ConfigInfo()
                    {
                        Name = KioskParameter.SECURITYKEY,
                        Value = ""//"test123"
                    });
                    //configInfoList.Add(new ConfigInfo()
                    //{
                    //    Name = KioskParameter.GROUPID,
                    //    Value = "03"
                    //});
                    configInfoList.Add(new ConfigInfo()
                    {
                        Name = KioskParameter.ACTIVESDAY,
                        Value = ""//DateTime.UtcNow.ToString("yyyyMMddHHmmss")
                    });

                    configInfoList.ForEach(d => Update(string.Format("INSERT INTO {0} VALUES ('{1}','{2}');", typeof(ConfigInfo).Name, d.Name, d.Value)));


                    //BatteryInfo test = new BatteryInfo()
                    //{
                    //    BatteryId = "0000000001",
                    //    ManufactureDate = "2013/01/01",
                    //    StatusCode = 1
                    //};
                    //Update(string.Format("INSERT INTO {0} VALUES ('{1}','{2}',{3});", typeof(BatteryInfo).Name, test.BatteryId, test.ManufactureDate, test.StatusCode));
                    //CloseDB();
                }
            }
        }

        private bool InitDB()
        {
            bool isSuccess = false;
            conn = new SQLiteConnection("Data source=" + mDBFile);

            try
            {
                conn.Open();
                conn.ChangePassword(KioskParameter.DATABASEPASSWORD);

                sqlite_cmd = conn.CreateCommand();//create command
                conn.Close();
                isSuccess = true;
            }
            catch
            {
                CloseDB();
            }

            return isSuccess;
        }

        public bool OpenDB()
        {
            conn = new SQLiteConnection("Data source=" + mDBFile);

            try
            {
                isConnectToDB = true;

                conn.SetPassword(KioskParameter.DATABASEPASSWORD);
                conn.Open();

                sqlite_cmd = conn.CreateCommand();//create command

            }
            catch(Exception ex)
            {
                CloseDB();
            }

            return isConnectToDB;
        }

        public void CloseDB()
        {
            isConnectToDB = false;
            if(conn != null)
            {
                try
                {
                    conn.Close();
                }
                catch
                {
                }
            }
        }

        public long GetLastInsertId()
        {
            long result = 0;
            try
            {
                sqlite_cmd.CommandText = @"SELECT last_insert_rowid()";
                result = (long)sqlite_cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Get ID DB Error : " + ex.Message);
            }
            return result;
        }

        public long Insert(string text)
        {
            long result = 0;

            int sqlErrorCode = 0;
            if (OpenDB())
            {
                try
                {
                    sqlite_cmd.CommandText = text;
                    sqlite_cmd.ExecuteNonQuery();//using behind every write cmd

                    sqlite_cmd.CommandText = @"SELECT last_insert_rowid()";
                    result = (long)sqlite_cmd.ExecuteScalar();
                }
                catch (SQLiteException ex)
                {
                    sqlErrorCode = ex.ErrorCode;
                    Debug.WriteLine("Update DB Error : " + ex.Message);
                }
                CloseDB();
            }
            return result;
        }

        public bool Update(string text)
        {
            bool isSuccess = false;
            int sqlErrorCode = 0;
            if (OpenDB())
            {
                try
                {
                    sqlite_cmd.CommandText = text;
                    sqlite_cmd.ExecuteNonQuery();//using behind every write cmd

                    isSuccess = true;
                }
                catch (SQLiteException ex)
                {
                    sqlErrorCode = ex.ErrorCode;
                    Debug.WriteLine("Update DB Error : " + ex.Message);
                }
                CloseDB();
            }
            return isSuccess;
        }

        public List<object> Read(string text, Type type)
        {
            List<object> temp = new List<object>();
            if (OpenDB())
            {
                try
                {
                    sqlite_cmd.CommandText = text;
                    SQLiteDataReader reader = sqlite_cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        object instance = Activator.CreateInstance(type);
                        foreach (PropertyInfo info in type.GetProperties())
                        {
                            switch (info.PropertyType.Name)
                            {
                                case "String":
                                    {
                                        if (!reader[info.Name].GetType().Equals(typeof(DBNull)))
                                        {
                                            if(reader[info.Name].GetType().Equals(typeof(DateTime))){
                                                DateTime dt = (DateTime)reader[info.Name];
                                                info.SetValue(instance, dt.ToString("yyyy-MM-dd HH:mm:ss"), null);
                                            }
                                            else
                                            {
                                                info.SetValue(instance, reader[info.Name], null);
                                            }
                                        }
                                        break;
                                    }
                                case "Int32":
                                    {
                                        info.SetValue(instance, Convert.ToInt32(reader[info.Name]), null);
                                        break;
                                    }
                                case "Int64":
                                    {
                                        info.SetValue(instance, Convert.ToInt64(reader[info.Name]), null);
                                        break;
                                    }
                                case "UInt64":
                                    {
                                        info.SetValue(instance, Convert.ToUInt64(reader[info.Name]), null);
                                        break;
                                    }
                                case "Double":
                                    {
                                        info.SetValue(instance, Convert.ToDouble(reader[info.Name]), null);
                                        break;
                                    }
                                case "Boolean":
                                    {
                                        info.SetValue(instance, Convert.ToBoolean(reader[info.Name]), null);
                                        break;
                                    }
                                default:
                                    {
                                        break;
                                    }
                            }
                        }
                        temp.Add(instance);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Read DB Error : " + ex.Message);
                }
                CloseDB();
            }
            return temp;
        }

        public int ConvertProtection(byte[] protection)
        {
            int number = 0;
            if (protection != null && protection.Length > 5)
            {
                number = protection[0] | (protection[1] << 1) | (protection[2] << 2) | (protection[3] << 3) | (protection[4] << 4) | (protection[5] << 5) | (protection[6] << 6);
            }

            return number;
        }

        public string ConvertManufactureDate(int[] manufactureDate)
        {
            if (manufactureDate != null && manufactureDate.Length >= 3)
            {
                return string.Format("{0:0000}/{1:00}/{2:00}", manufactureDate[0], manufactureDate[1], manufactureDate[2]);
            }
            return null;
        }

        public int SumCellsVoltage(int[] cellsVoltage)
        {
            int voltage = 0;
            if (cellsVoltage != null)
            {
                foreach (int volt in cellsVoltage)
                {
                    voltage = voltage + volt;
                }
            }
            return voltage;
        }

        public string ConvertCellsVoltage(int[] cellsVoltage)
        {
            string result = null;
            if (cellsVoltage != null)
            {
                result = "";
                foreach (int volt in cellsVoltage)
                {
                    result = result + string.Format("{0:X2}{1:X2}", volt / 256, volt % 256);
                }
            }
            return result;
        }

        public string ConvertBatteryTemp(double[] temps)
        {
            string result = null;
            if (temps != null)
            {
                result = "";
                foreach (double temp in temps)
                {
                    int num = Convert.ToInt32(temp * 100);
                    result = result + string.Format("{0:X2}{1:X2}", num / 256, num % 256);
                }
            }
            return result;
        }
    }
}
