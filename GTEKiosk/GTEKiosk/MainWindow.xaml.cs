﻿using GTE.Encryption;
using GTEKiosk.DeivceConfiguration;
using GTEKiosk.Mgr;
using GTEKiosk.Parameter;
using GTEKiosk.Trans;
using GTEKioskConfig.InfoModel;
using GTEKioskConfig.Setting;
using GTEKioskLib;
using GTEKioskLib.Command;
using GTEKioskLib.InfoModel;
using GTEKioskLib.RFID;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEKiosk
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Property
        public static bool IsDebugMode = false;
        public static bool IsOnlineMode = true;
        public static bool IsENGMode = false;
        public static bool IsDiagnosticENGMode = false;
        public static string UUID = null;

        public static int BATTERYTYPEID = 1; // 0: ALL 1: 4816 2: 4840

        private static AppSetting mAppSetting { get; set; }
        public static AppSetting AppSettingFile { get { return mAppSetting; } }

        private static KioskAPI mKiosk = KioskAPI.Kiosk;
        private static RFIDAPI mRFIDApi = new RFIDAPI();

        private static KioskDeviceAdapter mDeviceAdapter = new KioskDeviceAdapter(KioskAPI.Kiosk);
        public static KioskDeviceAdapter DeviceAdapter { get { return mDeviceAdapter; } }

        private static KioskMgr mKioskMgr = new KioskMgr();
        public static KioskMgr KioskMgr { get { return mKioskMgr; } }

        private static MainWindow mMainWindow;

        private static Window mCurrentWindow { get; set; }
        private static Grid mMainGrid { get; set; }

        private static Thread mRFIDThread;
        private static Thread mUpdateSlotThread;
        private static DispatcherTimer mAdWindowTimer = new DispatcherTimer();
        private static int mWaitingSpanTimer = 0;
        private static Thread mUploadDataThread { get; set; }

        private static MediaMgr mMediaMgr = new MediaMgr();
        public static MediaMgr MediaMgr { get { return mMediaMgr; } }

        public static GTEKiosk.Cultures.CulturesHelper CulturesHelper = new GTEKiosk.Cultures.CulturesHelper();
        #endregion

        public MainWindow()
        {
            InitializeComponent();

            //InitializeMonitor();
            InitializeUUID();
            InitializeAppMode();
            InitializeAppSetting();

            InitializeMgr();
            InitializeAdWindowTimer();

            mMainWindow = this;

           Loaded += Initialize_Loaded;
        }

        #region APP Configure
        /// <summary>
        /// 設定畫面顯示位置
        /// </summary>
        private void InitializeMonitor()
        {
            int screen = 0;
            //if (Screen.AllScreens.Length > 1)
            //{
            //    screen = Screen.AllScreens.Length - 1;
            //}
            Screen s2 = Screen.AllScreens[screen];
            System.Drawing.Rectangle r2 = s2.WorkingArea;
            this.Top = r2.Top;
            this.Left = r2.Left;
            this.WindowState = WindowState.Maximized;
        }

        /// <summary>
        /// 視窗準備關閉之事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (mCurrentWindow != null)
            {
                mCurrentWindow.Close();
            }
            StopRFIDScaner();
            mRFIDApi.Disconnect();
            StopUploader();
            StopSlotUpdater();
            mKiosk.Disconnect();
        }

        private void Initialize_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeMonitor();

            mMainGrid = mainGrid;
            KioskMgr.ResetConfigInfo();
            KioskMgr.ResetUserInfo();

            ConfigInfo RegisterDevice = KioskMgr.GetConfigInfoByName(KioskParameter.REGISTERDEVICE);

            if (RegisterDevice != null && RegisterDevice.Value != null && RegisterDevice.Value.Equals(UUID))
            {
                if (!IsENGMode)
                {
                    ThreadPool.QueueUserWorkItem(o => CheckKioskOnlineActive());
                }

                ChangeLang();

                ResetDeviceConfig(); // Step1 初始化Battery Config
                ConnectKioskModule(); // Step2 連線Kiosk API
                ConnectRFIDAPI(); // Step3 連線RFID裝置

                Thread.Sleep(300);

                KioskMgr.ScanAllSlotDevice();

                StartSlotUpdater();

                GoSystemRun(SystemRunMode.EnterApp);
            }
            else
            {
                // Register UUID
                if (MainWindow.IsOnlineMode)
                {
                    ShowWindow(new RegisterWindow());
                }
                else
                {
                    System.Windows.MessageBox.Show("Illegal Application");
                    CloseApp();
                }
            }
        }

        private void CheckKioskOnlineActive()
        {
            if (MainWindow.IsOnlineMode)
            {
                KioskMgr.InitOnlineSetting();

                bool isActive = KioskMgr.CheckKioskActive();

                if (!isActive)
                {
                    System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                        () =>
                        {
                            System.Windows.MessageBox.Show("Non-active Device");
                            CloseApp();
                        }));
                }
                else
                {
                    StartUploader();
                }
            }
        }

        private void Window_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (mCurrentWindow as AdWindow != null)
            {
                BackToHome(false);
            }
            else if (mCurrentWindow as DiagnosticWindow != null)
            {
                DiagnosticWindow dw = mCurrentWindow as DiagnosticWindow;
                dw.RestGoTimer();
            }
            else
            {
                mWaitingSpanTimer = 0;
            }
        }
        /// <summary>
        /// 初始化是否為管理者模式
        /// </summary>
        private void InitializeAppMode()
        {
            string[] arrArguments = System.Environment.GetCommandLineArgs();
            if (arrArguments != null)
            {
                foreach (string arg in arrArguments)
                {
                    if (arg.Equals(KioskParameter.CMD_ENGMODE))
                    {
                        IsENGMode = true;
                        break;
                    }
                }
            }
        }

        private void InitializeUUID()
        {
            //ManagementClass mc = new ManagementClass("win32_processor");
            //ManagementObjectCollection moc = mc.GetInstances();

            //foreach (ManagementObject mo in moc)
            //{
            //    UUID = mo.Properties["processorID"].Value.ToString();
            //    break;
            //}
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface adapter in nics)
            {
                if (UUID == null)// only return MAC Address from first card  
                {
                    UUID = adapter.GetPhysicalAddress().ToString();
                    break;
                }
            }
        }
        /// <summary>
        /// 初始化視窗程式需要之設定檔
        /// </summary>
        private void InitializeAppSetting()
        {
            string appPath = System.AppDomain.CurrentDomain.BaseDirectory;
            string appSettingFilePath = appPath + KioskParameter.APPSETTINGFILEFILE;
            try
            {
                if (File.Exists(appSettingFilePath))
                {
                    using (StreamReader file = File.OpenText(appSettingFilePath))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        mAppSetting = (AppSetting)serializer.Deserialize(file, typeof(AppSetting));
                    }
                }
            }
            catch { }
            if (mAppSetting == null)
            {
                mAppSetting = new AppSetting();

                string[] portList = System.IO.Ports.SerialPort.GetPortNames();
                if (portList != null && portList.Length > 0)
                {
                    mAppSetting.Port = portList[0];
                    mAppSetting.RFIDPort = portList[0];
                }

                mAppSetting.ScreenOrientation = mAppSetting.ScreenOrientationList[0];

                mAppSetting.BaudRate = 115200;

                mAppSetting.StartAddress = 0;
                mAppSetting.EndAddress = 29;

                mAppSetting.BatteryTypeVersion = AppSettingFile.BatteryTypeVersionList[0];

                mAppSetting.AdWaitingTime = 180;
                mAppSetting.AdUpdateTime = 5;

                mAppSetting.Lang = mAppSetting.LangList[0];

                SaveAppSetting();
            }
        }
        /// <summary>
        /// 儲存AppSetting資訊至檔案(AppSetting.ap)內
        /// </summary>
        /// <returns></returns>
        public static bool SaveAppSetting()
        {
            bool isSuccess = false;

            string appSettingFilePath = System.AppDomain.CurrentDomain.BaseDirectory + KioskParameter.APPSETTINGFILEFILE;

            if (AppSettingFile != null)
            {
                try
                {
                    using (FileStream fs = File.Open(appSettingFilePath, FileMode.Create))
                    using (StreamWriter sw = new StreamWriter(fs))
                    using (JsonWriter jw = new JsonTextWriter(sw))
                    {
                        jw.Formatting = Formatting.Indented;

                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(jw, AppSettingFile);
                        isSuccess = true;
                    }
                }
                catch { }
            }

            return isSuccess;
        }


        public static void ChangeLang()
        {
            if (mAppSetting.Lang != null)
            {
                CulturesHelper.ChangeCulture(mAppSetting.Lang);
            }
        }

        #endregion

        #region Kiosk Method
        /// <summary>
        /// 初始化交換站管理物件
        /// </summary>
        private void InitializeMgr()
        {
            KioskMgr.SetDeviceAdapter(mDeviceAdapter);
            KioskMgr.SetRFIDAPI(mRFIDApi);
            KioskMgr.InitCurrentChargeTransList();
        }

        public static bool ConnectRFIDAPI()
        {
            if (AppSettingFile != null)
            {
                mRFIDApi.Disconnect();
                return mRFIDApi.Connect(AppSettingFile.RFIDPort);
            }

            return false;
        }

        /// <summary>
        /// 初始化交換站API模組
        /// </summary>
        public static bool ConnectKioskModule()
        {
            if (AppSettingFile != null)
            {
                return mDeviceAdapter.SetConnectModule(AppSettingFile.Port, AppSettingFile.BaudRate);
            }

            return false;
        }

        public static void ResetDeviceConfig()
        {
            if (AppSettingFile != null)
            {
                KioskBatteryType type = KioskBatteryType.MPM;
                GTE.Encapsulation.EncapsulationType version = GTE.Encapsulation.EncapsulationType.MPVersion;
                if (AppSettingFile.BatteryTypeVersion != null)
                {
                    KioskMgr.SetDeviceType(AppSettingFile.BatteryTypeVersion);
                    MainWindow.BATTERYTYPEID = 2; //4840
                    if (AppSettingFile.BatteryTypeVersion.Contains("4816"))
                    {
                        MainWindow.BATTERYTYPEID = 1; //4816
                        type = KioskBatteryType.MPS;
                    }

                    if (AppSettingFile.BatteryTypeVersion.Contains("V"))
                    {
                        version = GTE.Encapsulation.EncapsulationType.VVersion;
                    }
                }
                                
                mDeviceAdapter.SetKioskAPIConfig(type, version);
                mDeviceAdapter.SetSlotItemList(AppSettingFile.ChargeSlotList);

                KioskMgr.SetTransCheckMode(0);
            }
        }
        #endregion

        #region RFID
        public static void ResetRFIDScraner()
        {
            StopRFIDScaner();

            mRFIDThread = new Thread(new ThreadStart(DetectedRFID));
            mRFIDThread.Start();
        }

        private static void StopRFIDScaner()
        {
            if (mRFIDThread != null)
            {
                mRFIDThread.Abort();
                mRFIDThread = null;
            }
        }

        private static void DetectedRFID()
        {
            KioskMgr.IsRefreshRFID = true;

            while (KioskMgr.IsRefreshRFID)
            {
                CardInfo cardInfo = KioskMgr.ScanRFIDCard();

                if (cardInfo != null)
                {
                    bool isMgr = false;
                    UserInfo userInfo = KioskMgr.CheckUserInfo(cardInfo.UserId);
                    if (userInfo != null)
                    {
                        IsDiagnosticENGMode = false;
                        if (userInfo.UserRole > 1)
                        {
                            isMgr = true;

                            if (userInfo.UserRole > 4)
                            {
                                IsDiagnosticENGMode = true;
                            }
                        }

                        System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                            () =>
                            {
                                BackToHome(isMgr);
                            }));
                    }
                    else
                    {
                        KioskMgr.IsRefreshRFID = true;
                    }
                }
            }
        }
        #endregion

        #region Kiosk Manager Method

        public void StartUploader()
        {
            StopUploader();
            mUploadDataThread = new Thread(new ThreadStart(KioskMgr.AutoUploadData));
            mUploadDataThread.Start();
        }

        private void StopUploader()
        {
            if (mUploadDataThread != null)
            {
                mUploadDataThread.Abort();
                mUploadDataThread = null;
            }
        }

        public void StartSlotUpdater()
        {
            StopSlotUpdater();

            KioskMgr.IsStopScanSlotInfo = true;

            mUpdateSlotThread = new Thread(new ThreadStart(KioskMgr.AutoScanSlotInfo));
            mUpdateSlotThread.Start();
        }

        private void StopSlotUpdater()
        {
            if (mUpdateSlotThread != null)
            {
                mUpdateSlotThread.Abort();
                mUpdateSlotThread = null;
            }
        }
        #endregion

        #region Play Media
        public static void PlayMedia(string file)
        {
            if (!mAppSetting.IsMute)
            {
                mMediaMgr.PlayMedia(file);
            }
        }
        #endregion

        #region Switch Window Method
        public static void CloseApp()
        {
            mMainWindow.Close();
        }
        public static void BackToHome(bool isManager)
        {
            mWaitingSpanTimer = 0;
            KioskMgr.IsRefreshRFID = false; //停止RFID自動掃描
            KioskMgr.IsStopScanSlotInfo = false; //更新slot資訊
            mAdWindowTimer.Start();

            if (KioskParameter.PORTRAIT.Equals(mAppSetting.ScreenOrientation))
            {
                ShowWindow(new HomeWindowPortrait());
                HomeWindowPortrait window = mCurrentWindow as HomeWindowPortrait;
                if (window != null)
                {
                    window.RefreshUI(isManager);
                }
            }
            else
            {
                ShowWindow(new HomeWindow());
                HomeWindow window = mCurrentWindow as HomeWindow;
                if (window != null)
                {
                    window.RefreshUI(isManager);
                }
            }

            // 請選擇螢幕上的操作選項
            //PlayMedia(MediaMgr.GetLangAudioPath(CulturesHelper.CurrentCultureInfoName(), AudioList.MainMenu.ToString()));
        }

        public static void GoSystemRun(SystemRunMode mode)
        {
            mWaitingSpanTimer = 0;
            mAdWindowTimer.Stop(); //停止進入廣告頁面
            KioskMgr.IsRefreshRFID = false; //停止RFID自動掃描

            KioskMgr.ScanWaiting(); //停止更新slot資訊

            if (!IsDebugMode)
            {
                Mouse.OverrideCursor = System.Windows.Input.Cursors.None;
            }

            if (KioskParameter.PORTRAIT.Equals(mAppSetting.ScreenOrientation))
            {
                ShowWindow(new SystemRunWindowPortrait());
                SystemRunWindowPortrait window = mCurrentWindow as SystemRunWindowPortrait;
                if (window != null)
                {
                    window.Start(mode);
                }
            }
            else
            {
                ShowWindow(new SystemRunWindow());
                SystemRunWindow window = mCurrentWindow as SystemRunWindow;
                if (window != null)
                {
                    window.Start(mode);
                }
            }
        }

        public static void GoDiagnostic(bool isManager)
        {
            mWaitingSpanTimer = 0;
            mAdWindowTimer.Stop(); //停止進入廣告頁面

            KioskMgr.IsRefreshRFID = false; //停止RFID自動掃描

            KioskMgr.ScanWaiting(); //停止更新slot資訊

            ShowWindow(new DiagnosticWindow());
            DiagnosticWindow sw = mCurrentWindow as DiagnosticWindow;
            if (sw == null)
            {
                BackToHome(false);
            }
            else
            {
                sw.Start(isManager);
            }
        }

        public static void GoMap()
        {
            mWaitingSpanTimer = 0;
            KioskMgr.IsRefreshRFID = false; //停止RFID自動掃描
            KioskMgr.IsStopScanSlotInfo = false; //更新slot資訊
            mAdWindowTimer.Start();

            ShowWindow(new MapWindow());
            MapWindow mw = mCurrentWindow as MapWindow;
        }

        public static void GoMember()
        {
            mWaitingSpanTimer = 0;
            KioskMgr.IsRefreshRFID = false; //停止RFID自動掃描
            KioskMgr.IsStopScanSlotInfo = false; //更新slot資訊
            mAdWindowTimer.Start();

            if (KioskParameter.PORTRAIT.Equals(mAppSetting.ScreenOrientation))
            {
                ShowWindow(new MemberWindowPortrait());
                MemberWindowPortrait window = mCurrentWindow as MemberWindowPortrait;
            }
            else
            {
                ShowWindow(new MemberWindow());
                MemberWindow window = mCurrentWindow as MemberWindow;
            }
        }

        public static void GoCash()
        {
            mWaitingSpanTimer = 0;
            KioskMgr.IsRefreshRFID = false; //停止RFID自動掃描
            KioskMgr.IsStopScanSlotInfo = false; //更新slot資訊
            mAdWindowTimer.Start();

            if (KioskParameter.PORTRAIT.Equals(mAppSetting.ScreenOrientation))
            {
                ShowWindow(new CashWindow());
                CashWindow window = mCurrentWindow as CashWindow;
                if (window != null)
                {
                    window.StartTrans(CashAction.DetectedCard);
                }
            }
            else
            {
                ShowWindow(new CashWindow());
                CashWindow window = mCurrentWindow as CashWindow;
                if (window != null)
                {
                    window.StartTrans(CashAction.DetectedCard);
                }
            }
        }

        public static void TransDevice(TransAction transAction)
        {
            mAdWindowTimer.Stop(); //停止進入廣告頁面

            KioskMgr.IsRefreshRFID = false; //停止RFID自動掃描
            KioskMgr.ScanWaiting(); //停止更新slot資訊

            if (KioskParameter.PORTRAIT.Equals(mAppSetting.ScreenOrientation))
            {
                TransDeviceWindowPortrait td = mCurrentWindow as TransDeviceWindowPortrait;
                if (td == null)
                {
                    ShowWindow(new TransDeviceWindowPortrait());
                }
                td = mCurrentWindow as TransDeviceWindowPortrait;
                if (td == null)
                {
                    BackToHome(false);
                }
                else
                {
                    td.StartTrans(transAction);
                }
            }
            else
            {
                TransDevice td = mCurrentWindow as TransDevice;
                if (td == null)
                {
                    ShowWindow(new TransDevice());
                }
                td = mCurrentWindow as TransDevice;
                if (td == null)
                {
                    BackToHome(false);
                }
                else
                {
                    td.StartTrans(transAction);
                }
            }
        }

        private static void ShowWindow(Window window)
        {
            if (mCurrentWindow != null)
            {
                mCurrentWindow.Close();
            }

            mCurrentWindow = window;

            mMainGrid.Children.Clear();
            if (mCurrentWindow != null)
            {
                object content = mCurrentWindow.Content;

                mCurrentWindow.Content = null;

                mMainGrid.Children.Add(content as UIElement);
            }
        }
        #endregion

        #region Ad Window Method
        private void InitializeAdWindowTimer()
        {
            mAdWindowTimer.Interval = TimeSpan.FromMilliseconds(1000);
            mAdWindowTimer.Tick += adWindowTimer_Tick;
        }

        private void adWindowTimer_Tick(object sender, EventArgs e)
        {
            mWaitingSpanTimer++;
            if (mWaitingSpanTimer > mAppSetting.AdWaitingTime)
            {
                GoAdWindow();
            }
        }

        public static void GoAdWindow()
        {
            ShowWindow(new AdWindow());

            ResetRFIDScraner();
            mAdWindowTimer.Stop();
        }
        #endregion
    }
}
