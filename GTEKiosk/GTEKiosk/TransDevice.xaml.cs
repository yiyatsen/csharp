﻿using GTEKiosk.DeivceConfiguration;
using GTEKiosk.Parameter;
using GTEKiosk.Trans;
using GTEKioskConfig.InfoModel;
using GTEKioskLib.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEKiosk
{
    /// <summary>
    /// TransDevice.xaml 的互動邏輯
    /// </summary>
    public partial class TransDevice : Window
    {
        private DispatcherTimer mCountDownTimer { get; set; }
        private TransAction mCurrentAction { get; set; }
        private int mCountDownTime { get; set; }
        private bool mIsEndTrans { get; set; }
        private bool mIsDetectedCardError = false;

        public TransDevice()
        {
            InitializeComponent();

            mCountDownTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(1) };
            mCountDownTimer.Tick += CountDownTimer_Tick;
        }

        public void StartTrans(TransAction transAction)
        {
            mCurrentAction = transAction;
            countdownGrid.Visibility = System.Windows.Visibility.Visible;
            sandGlassImage.Visibility = System.Windows.Visibility.Visible;
            cancelTransButton.Visibility = System.Windows.Visibility.Visible;

            switch (transAction)
            {
                case TransAction.DetectedCard:
                    {
                        actionImage.Source = new BitmapImage(new Uri(@"res/image/DetectRFID.png", UriKind.Relative));
                        contentTextBox.Text = MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/PleasePlaceTheInductionKeyring").InnerText;

                        MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.CardDetected.ToString()));

                        mCountDownTimer.Stop();
                        mCountDownTime = 0;

                        countdownText.Text = KioskParameter.TRANSWAITINGTIME.ToString();

                        ThreadPool.QueueUserWorkItem(o => ScanRFIDCard());
                        mCountDownTimer.Start();
                        break;
                    }
                case TransAction.InsertedBattery:
                    {
                        int slotId = MainWindow.KioskMgr.GetEmptySlotId();

                        actionImage.Source = new BitmapImage(new Uri(@"res/image/InsertedBattery.png", UriKind.Relative));
                        contentTextBox.Text = MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/InsertedBattery").InnerText;

                        MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.InsertedBattery.ToString()));

                        mCountDownTimer.Stop();
                        mCountDownTime = 0;
                        countdownText.Text = KioskParameter.TRANSWAITINGTIME.ToString();

                        ThreadPool.QueueUserWorkItem(o => DetectedRFIDCard());

                        ThreadPool.QueueUserWorkItem(o => ScanBatteryInserted());
                        mCountDownTimer.Start();

                        break;
                    }
                case TransAction.CheckTransInfo:
                    {
                        actionImage.Source = new BitmapImage(new Uri(@"res/image/CheckBatteryInfo.png", UriKind.Relative));
                        contentTextBox.Text = MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/CheckTransInfo").InnerText;

                        MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.CheckBattery.ToString()));

                        mCountDownTimer.Stop();
                        mCountDownTime = 0;
                        countdownText.Text = "";

                        countdownGrid.Visibility = System.Windows.Visibility.Hidden;
                        sandGlassImage.Visibility = System.Windows.Visibility.Hidden;
                        cancelTransButton.Visibility = System.Windows.Visibility.Hidden;

                        ThreadPool.QueueUserWorkItem(o => CheckTransInfo());
                        //mCountDownTimer.Start();

                        break;
                    }
                case TransAction.DrawOutOriginalBattery:
                    {
                        int slotId = MainWindow.KioskMgr.GetEmptySlotId();

                        actionImage.Source = new BitmapImage(new Uri(@"res/image/DrawOutEmptyBattery.png", UriKind.Relative));
                        contentTextBox.Text = MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/PleaseTakeOutTheOriginalBattery").InnerText
                            + Environment.NewLine
                            + MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/DrawOutOldBattery").InnerText;

                        MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.DrawoutOldBattery.ToString()));

                        mCountDownTimer.Stop();
                        mCountDownTime = 0;
                        countdownText.Text = KioskParameter.TRANSWAITINGTIME.ToString();

                        cancelTransButton.Visibility = System.Windows.Visibility.Hidden;

                        ThreadPool.QueueUserWorkItem(o => ScanDrawOutBattery(transAction));
                        mCountDownTimer.Start();

                        break;
                    }
                case TransAction.DrawOutOriginalBatteryCheckBatteryAndCardError:
                    {
                        int slotId = MainWindow.KioskMgr.GetEmptySlotId();

                        actionImage.Source = new BitmapImage(new Uri(@"res/image/DrawOutEmptyBattery.png", UriKind.Relative));
                        contentTextBox.Text = MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/PleaseTakeOutTheOriginalBattery").InnerText
                            + Environment.NewLine
                            + MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/CheckBatteryAndCardError").InnerText;

                        MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.CheckBatteryAndCardError.ToString()));

                        mCountDownTimer.Stop();
                        mCountDownTime = 0;
                        countdownText.Text = KioskParameter.TRANSWAITINGTIME.ToString();

                        cancelTransButton.Visibility = System.Windows.Visibility.Hidden;

                        ThreadPool.QueueUserWorkItem(o => ScanDrawOutBattery(transAction));
                        mCountDownTimer.Start();

                        break;
                    }
                case TransAction.DrawOutNewBattery:
                    {
                        int slotId = MainWindow.KioskMgr.GetUsableSlotId();

                        actionImage.Source = new BitmapImage(new Uri(@"res/image/DrawOutFineBattery.png", UriKind.Relative));
                        contentTextBox.Text = MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/DrawOutNewBattery").InnerText;

                        MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.DrawoutNewBattery.ToString()));

                        mCountDownTimer.Stop();
                        mCountDownTime = 0;
                        countdownText.Text = KioskParameter.TRANSWAITINGTIME.ToString();

                        cancelTransButton.Visibility = System.Windows.Visibility.Hidden;

                        ThreadPool.QueueUserWorkItem(o => ScanDrawOutBattery(transAction));
                        mCountDownTimer.Start();

                        break;
                    }
                case TransAction.CancelTrans:
                case TransAction.CancelTransByCheckBatteryAndCardError:
                case TransAction.CancelTransByNonDrawOutNewBattery:
                    {
                        actionImage.Source = new BitmapImage(new Uri(@"res/image/TransCancel.png", UriKind.Relative));
                        contentTextBox.Text = MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/CancelTrans").InnerText;

                        MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.TransCancel.ToString()));

                        mCountDownTimer.Stop();
                        mCountDownTime = 0;
                        mIsEndTrans = false;

                        countdownGrid.Visibility = System.Windows.Visibility.Hidden;
                        sandGlassImage.Visibility = System.Windows.Visibility.Hidden;
                        cancelTransButton.Visibility = System.Windows.Visibility.Hidden;

                        TransStatusCode transStatusCode = TransStatusCode.Fail;
                        if (transAction == TransAction.CancelTransByCheckBatteryAndCardError)
                        {
                            transStatusCode = TransStatusCode.VerifyErrorByKiosk;
                        }
                        else if(transAction == TransAction.CancelTransByNonDrawOutNewBattery)
                        {
                            transStatusCode = TransStatusCode.DrawOutBatteryFail;
                        }

                        ThreadPool.QueueUserWorkItem(o => TransDone(transStatusCode));
                        mCountDownTimer.Start();

                        break;
                    }
                case TransAction.CancelTransByNoCard:
                    {
                        actionImage.Source = new BitmapImage(new Uri(@"res/image/TransCancel.png", UriKind.Relative));
                        contentTextBox.Text = MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/CancelTrans").InnerText
                            + Environment.NewLine
                            + MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/YourCardCannotBeIdentify").InnerText;

                        MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.NoCard.ToString()));

                        mCountDownTimer.Stop();
                        mCountDownTime = 0;
                        mIsEndTrans = false;

                        countdownGrid.Visibility = System.Windows.Visibility.Hidden;
                        sandGlassImage.Visibility = System.Windows.Visibility.Hidden;
                        cancelTransButton.Visibility = System.Windows.Visibility.Hidden;

                        ThreadPool.QueueUserWorkItem(o => TransDone(TransStatusCode.NoCardFail));
                        mCountDownTimer.Start();

                        break;
                    }
                case TransAction.CancelTransByNoDetectedBattery:
                    {
                        actionImage.Source = new BitmapImage(new Uri(@"res/image/TransCancel.png", UriKind.Relative));
                        contentTextBox.Text = MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/CancelTrans").InnerText
                            + Environment.NewLine
                            + MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/NoDetectedBattery").InnerText;

                        MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.NoDetectedBattery.ToString()));

                        mCountDownTimer.Stop();
                        mCountDownTime = 0;
                        mIsEndTrans = false;

                        countdownGrid.Visibility = System.Windows.Visibility.Hidden;
                        sandGlassImage.Visibility = System.Windows.Visibility.Hidden;
                        cancelTransButton.Visibility = System.Windows.Visibility.Hidden;

                        ThreadPool.QueueUserWorkItem(o => TransDone(TransStatusCode.NoInsertedBatteryFail));
                        mCountDownTimer.Start();

                        break;
                    }
                case TransAction.FinishTrans:
                    {
                        actionImage.Source = new BitmapImage(new Uri(@"res/image/TransFinish.png", UriKind.Relative));
                        contentTextBox.Text = MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/FinishTrans").InnerText;

                        MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.TransFinish.ToString()));

                        mCountDownTimer.Stop();
                        mCountDownTime = 0;
                        mIsEndTrans = false;


                        countdownGrid.Visibility = System.Windows.Visibility.Hidden;
                        sandGlassImage.Visibility = System.Windows.Visibility.Hidden;
                        cancelTransButton.Visibility = System.Windows.Visibility.Hidden;

                        ThreadPool.QueueUserWorkItem(o => TransDone(TransStatusCode.KioskTransSuccess));
                        mCountDownTimer.Start();

                        break;
                    }
                default:
                    {
                        //MainWindow.BackToHome(false);
                        MainWindow.GoAdWindow();
                        break;
                    }
            }
        }

        private void TransDone(TransStatusCode transStatusCode)
        {
            MainWindow.KioskMgr.EndTrans(transStatusCode);
            mIsEndTrans = true;
        }

        private void ScanRFIDCard()
        {
            CardInfo cardInfo = MainWindow.KioskMgr.ScanRFIDCard();

            bool isSuccess = MainWindow.KioskMgr.CheckCardInfo(cardInfo);

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    if (isSuccess)
                    {
                        StartTrans(TransAction.InsertedBattery);
                    }
                    else
                    {
                        StartTrans(TransAction.CancelTransByNoCard);
                    }
                }));
        }

        private void DetectedRFIDCard()
        {
            bool isExistCard = MainWindow.KioskMgr.DetectedRFIDCard();

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    if (!isExistCard)
                    {
                        mIsDetectedCardError = true;
                        switch (mCurrentAction)
                        {
                            case TransAction.CheckTransInfo:
                                {
                                    StartTrans(TransAction.CancelTransByNoCard);
                                    break;
                                }
                            default:
                                {
                                    StartTrans(TransAction.CancelTransByNoCard);
                                    break;
                                }
                        }                   
                    }
                }));
        }

        private void ScanBatteryInserted()
        {
            bool isSuccess = MainWindow.KioskMgr.ScanBatteryInserted();

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    if (!mIsDetectedCardError)
                    {
                        if (isSuccess)
                        {
                            StartTrans(TransAction.CheckTransInfo);
                        }
                        else
                        {
                            StartTrans(TransAction.CancelTransByNoDetectedBattery);
                        }
                    }
                }));
        }

        private void CheckTransInfo()
        {
            //確認卡片資訊和電池資訊是否吻合並且依據設定將電池資訊寫入新電池中
            int statusCode = MainWindow.KioskMgr.CheckTransIdInfo();

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    if (!mIsDetectedCardError)
                    {
                        MainWindow.KioskMgr.IsRefreshRFID = false; //結束偵測RFID
                        switch (statusCode)
                        {
                            case 0:
                                {
                                    StartTrans(TransAction.DrawOutNewBattery);
                                    break;
                                }
                            default:
                                {
                                    StartTrans(TransAction.DrawOutOriginalBatteryCheckBatteryAndCardError);
                                    break;
                                }
                        }
                    }
                }));
        }

        private void ScanDrawOutBattery(TransAction transAction)
        {
            bool isSuccess = MainWindow.KioskMgr.ScanDrawOutBattery(transAction);

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    switch (transAction)
                    {
                        case TransAction.DrawOutNewBattery:
                            {
                                if (isSuccess)
                                {
                                    StartTrans(TransAction.FinishTrans);
                                }
                                else
                                {
                                    StartTrans(TransAction.DrawOutOriginalBattery);
                                }

                                break;
                            }
                        case TransAction.DrawOutOriginalBattery:
                            {
                                StartTrans(TransAction.CancelTransByNonDrawOutNewBattery);

                                break;
                            }
                        case TransAction.DrawOutOriginalBatteryCheckBatteryAndCardError:
                            {
                                StartTrans(TransAction.CancelTransByCheckBatteryAndCardError);

                                break;
                            }
                        default:
                            {
                                //MainWindow.BackToHome(false);
                                MainWindow.GoAdWindow();
                                break;
                            }
                    }
                }));
        }

        private void CountDownTimer_Tick(object sender, EventArgs e)
        {
            mCountDownTime++;
            switch (mCurrentAction)
            {
                case TransAction.DetectedCard:
                    {
                        if (mCountDownTime > KioskParameter.TRANSWAITINGTIME)
                        {
                            mCountDownTime = 0;
                            mCountDownTimer.Stop();
                            MainWindow.KioskMgr.IsRefreshRFID = false;
                        }
                        else
                        {
                            countdownText.Text = (KioskParameter.TRANSWAITINGTIME - mCountDownTime).ToString();
                        }

                        break;
                    }
                case TransAction.InsertedBattery:
                    {
                        if (mCountDownTime > KioskParameter.TRANSWAITINGTIME)
                        {
                            mCountDownTime = 0;
                            mCountDownTimer.Stop();
                            MainWindow.KioskMgr.IsKeepRunThread = false;
                        }
                        else
                        {
                            countdownText.Text = (KioskParameter.TRANSWAITINGTIME - mCountDownTime).ToString();
                        }

                        break;
                    }
                case TransAction.DrawOutOriginalBatteryCheckBatteryAndCardError:
                case TransAction.DrawOutOriginalBattery:
                case TransAction.DrawOutNewBattery:
                    {
                        if (mCountDownTime > KioskParameter.TRANSWAITINGTIME)
                        {
                            mCountDownTime = 0;
                            mCountDownTimer.Stop();
                            MainWindow.KioskMgr.IsKeepRunThread = false;
                        }
                        else
                        {
                            countdownText.Text = (KioskParameter.TRANSWAITINGTIME - mCountDownTime).ToString();
                        }

                        break;
                    }
                case TransAction.CancelTrans:
                case TransAction.CancelTransByNoCard:
                case TransAction.CancelTransByNoDetectedBattery:
                case TransAction.CancelTransByCheckBatteryAndCardError:
                case TransAction.CancelTransByNonDrawOutNewBattery:
                case TransAction.FinishTrans:
                    {
                        if (mCountDownTime > 3 && mIsEndTrans)
                        {
                            mCountDownTime = 0;
                            mCountDownTimer.Stop();
                            //MainWindow.BackToHome(false);
                            MainWindow.GoAdWindow();
                        }
                        break;
                    }
                default:
                    {
                        mCountDownTime = 0;
                        mCountDownTimer.Stop();
                        //MainWindow.BackToHome(false);
                        MainWindow.GoAdWindow();
                        break;
                    }
            }
        }

        private void cancelTransButton_Click(object sender, RoutedEventArgs e)
        {
            switch (mCurrentAction)
            {
                case TransAction.DetectedCard:
                    {
                        mCountDownTime = 0;
                        mCountDownTimer.Stop();
                        MainWindow.KioskMgr.IsRefreshRFID = false;

                        break;
                    }
                case TransAction.InsertedBattery:
                    {
                        mCountDownTime = 0;
                        mCountDownTimer.Stop();
                        MainWindow.KioskMgr.IsKeepRunThread = false;

                        break;
                    }
                case TransAction.DrawOutOriginalBatteryCheckBatteryAndCardError:
                case TransAction.DrawOutOriginalBattery:
                case TransAction.DrawOutNewBattery:
                    {
                        mCountDownTime = 0;
                        mCountDownTimer.Stop();
                        MainWindow.KioskMgr.IsKeepRunThread = false;

                        break;
                    }
                default:
                    {
                        mCountDownTime = 0;
                        mCountDownTimer.Stop();
                        //MainWindow.BackToHome(false);
                        MainWindow.GoAdWindow();
                        break;
                    }
            }
        }
    }
}
