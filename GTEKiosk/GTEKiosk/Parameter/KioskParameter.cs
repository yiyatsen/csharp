﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKiosk.Parameter
{
    public class KioskParameter
    {
        public const int KIOSKTYPEID = 1;

        public const int UPLOADWAITINGTIME = 60;
        public const int DETECTRFIDTIME = 10;
        public const int AUTOGOKIOSKTIME = 180;
        public const int AUTOGOTIME = 15;
        public const int SCANPORTTIME = 5;
        public const string APPSETTINGFILEFILE = @"AppSetting.ap";

        public const string GOTECHDATABASE = @"GotechDatabase.db";

        public const string DATABASEPASSWORD = "gtenergy.developer";

        public const string CMD_ENGMODE = "ENG";

        public const string LANDSCAPE = "Landscape";

        public const string PORTRAIT = "Portrait";

        public const string REGISTERTIME = "RegisterTime";
        public const string REGISTERDEVICE = "RegisterDevice";
        public const string SUCCESS = "Success";
        public const string FAIL = "Fail";
        public const string DEVICEID = "DeviceId";
        //public const string STATIONMANUFACTUREDATE = "StationManufactureDate";
        public const string PASSWORD = "Password";
        public const string DEVICETYPEID = "DeviceTypeId";
        public const string GROUPID = "GroupId";

        public const string ACTIVESDAY = "ActiveDay";
        public const string SECURITYKEY = "SecurityKey";
        

        public const string UPLOADAPI = @"/api/DeviceUpload/UploadData";
        public const string REGISTERAPI = @"/api/DeviceUpload/RegisterDevice";

        public const int TRANSWAITINGTIME = 15;
        public const int TRANSLASTWAITINGTIME = 10;

        public const bool IsDiagnosticMode = true;
    }
}
