﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKiosk.Parameter
{
    public enum SystemRunMode
    {
        EnterApp,
        UpdateApp,
        UpdateData
    }
}
