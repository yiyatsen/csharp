﻿using GTE.Module;
using GTEKiosk.Parameter;
using GTEKioskConfig.InfoModel;
using GTEKioskConfig.Setting;
using GTEKioskLib;
using GTEKioskLib.Command;
using GTEKioskLib.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEKiosk
{
    /// <summary>
    /// DiagnosticWindow.xaml 的互動邏輯
    /// </summary>
    public partial class DiagnosticWindow : Window
    {
        private static AppSetting mAppSetting = MainWindow.AppSettingFile;

        private DispatcherTimer mPortScanTimer; // 監聽Port狀態之定時器

        private int mGoTimer = KioskParameter.AUTOGOTIME;
        private DispatcherTimer mAutoGoTimer; // 監聽Port狀態之定時器

        private int mDetectRFIDTimer = KioskParameter.DETECTRFIDTIME;
        private DispatcherTimer mRFIDDetectTimer; // 監聽Port狀態之定時器

        private List<ModuleItem> mGateModuleItemList = new List<ModuleItem>();
        private List<ModuleItem> mBatteryModuleItemList = new List<ModuleItem>();

        private ChargeSlotItem mCurrentChargeSlotItem { get; set; }

        private Thread mSlotUpdateThread;
        private bool mEnableSlotUpdate = false;
        private int mGateAction = 0;
        private bool mIsFirstEnable = false;
        private DateTime mActionDateTime { get; set; }
        private int mActionTime = 0;
        private bool mIsSlotLight = false;
        private bool mIsInitStart = true;

        public DiagnosticWindow()
        {
            InitializeComponent();
        }

        public void Start(bool isMgr)
        {
            SetMode(isMgr);

            InitializeUI();
            InitializeScanPort();

            ResetSlotUpdateScraner();
            InitializeDetectRFID();
            InitializeAutoGo();

            Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
            ThreadPool.QueueUserWorkItem(o => InitStart());        
        }

        private void InitStart()
        {
            UpdateSlotBatteryInfo();
            RefreshSlotModule(false);
        }

        private void SetMode(bool isMgr)
        {
            if (isMgr)
            {
                addSlotButton.Visibility = System.Windows.Visibility.Visible;
                deleteSlotButton.Visibility = System.Windows.Visibility.Visible;

                createButton.Visibility = System.Windows.Visibility.Visible;

                userIdText.IsReadOnly = false;

                startAddressTextBox.IsReadOnly = false;
                endAddressTextBox.IsReadOnly = false;

                slotIdName.IsReadOnly = false;
                gateAddressList.IsEnabled = true;
                batteryAddressList.IsEnabled = true;
            }
            else
            {
                addSlotButton.Visibility = System.Windows.Visibility.Hidden;
                deleteSlotButton.Visibility = System.Windows.Visibility.Hidden;

                startAddressTextBox.IsReadOnly = true;
                endAddressTextBox.IsReadOnly = true;

                slotIdName.IsReadOnly = true;
                gateAddressList.IsEnabled = false;
                batteryAddressList.IsEnabled = false;
            }
        }

        private void InitializeUI()
        {
            SetScreenOrientationeToComboBox();
            SetSerialPortToComboBox(true);
            SetRFIDPortToComboBox(true);
            SetBaudRateToComboBox();
            SetLanguageToComboBox();
            SetGeolocation();
            SetServerAddress();

            startAddressTextBox.Text = mAppSetting.StartAddress.ToString();
            endAddressTextBox.Text = mAppSetting.EndAddress.ToString();

            isMuteButton.IsChecked = mAppSetting.IsMute;

            SetBatteryTypeToComboBox();
        }

        /// <summary>
        /// 視窗準備關閉之事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            mAutoGoTimer.Stop();
            mPortScanTimer.Stop();
            StopSlotUpdateScaner();
            CloseRFIDDetected();
        }

        #region Module Method
        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            slotDetailInfo.Visibility = System.Windows.Visibility.Hidden;
            mEnableSlotUpdate = false;

            refreshModuleButton.IsEnabled = false;
            SaveModuleStatus(false);
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            mCurrentChargeSlotItem = new ChargeSlotItem();
            ShowChargeSlot(mCurrentChargeSlotItem);

            mEnableSlotUpdate = false;
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            if (moduleInfoListView.SelectedItem != null)
            {
                mCurrentChargeSlotItem = (ChargeSlotItem)moduleInfoListView.SelectedItem;
                ShowChargeSlot(mCurrentChargeSlotItem);

                mEnableSlotUpdate = true;
            }
            else
            {

            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (moduleInfoListView.SelectedItem != null)
            {
                if (mCurrentChargeSlotItem != null && mCurrentChargeSlotItem.Equals((ChargeSlotItem)moduleInfoListView.SelectedItem))
                {
                    mEnableSlotUpdate = false;
                    mCurrentChargeSlotItem = null;
                    ShowChargeSlot(mCurrentChargeSlotItem);
                }

                mAppSetting.ChargeSlotList.Remove((ChargeSlotItem)moduleInfoListView.SelectedItem);
                RefreshSlotModuleViewUI();
            }
            else
            {
                // No Slot
            }
        }

        private void refreshAddress_Button(object sender, RoutedEventArgs e)
        {
            if (mCurrentChargeSlotItem != null 
                && slotIdName.Text != null && !slotIdName.Text.Equals("") 
                && gateAddressList.SelectedItem != null && batteryAddressList.SelectedItem != null)
            {
                mCurrentChargeSlotItem.SlotId = int.Parse(slotIdName.Text);
                mCurrentChargeSlotItem.GateAddress = (int)gateAddressList.SelectedItem;
                mCurrentChargeSlotItem.BatteryAddress = (int)batteryAddressList.SelectedItem;

                if (!mAppSetting.ChargeSlotList.Contains(mCurrentChargeSlotItem))
                {
                    Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;

                    refreshAddressButton.IsEnabled = false;
                    mAppSetting.ChargeSlotList.Add(mCurrentChargeSlotItem);
                    SaveModuleStatus(true);
                }
            }
        }

        private void SaveModuleStatus(bool updateAddress)
        {
            refreshModuleButton.IsEnabled = false;
            if (CheckAddress())
            {
                mAppSetting.Port = portList.SelectedItem.ToString();
                mAppSetting.BaudRate = (int)baudRateList.SelectedItem;
                mAppSetting.BatteryTypeVersion = batteryTypeList.SelectedItem.ToString();
                
                MainWindow.SaveAppSetting();
                MainWindow.ResetDeviceConfig(); // Step1 初始化Battery Config
                MainWindow.ConnectKioskModule(); // Step2 連線Kiosk API

                Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                ThreadPool.QueueUserWorkItem(o => RefreshSlotModule(updateAddress));
            }
            else
            {
                MessageBox.Show("Illegal address", "Parameter Error");
                refreshModuleButton.IsEnabled = true;
            }
        }

        private void RefreshSlotModule(bool updateAddress)
        {
            RefreshSlotInformation();
            RefreshModuleInformation();

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {               
                    if (updateAddress)
                    {
                        ShowChargeSlot(mCurrentChargeSlotItem);
                        mEnableSlotUpdate = true;
                        refreshAddressButton.IsEnabled = true;
                    }                

                    RefreshSlotModuleViewUI();
                    Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                    refreshModuleButton.IsEnabled = true;

                    if (mIsInitStart)
                    {
                        mIsInitStart = !mIsInitStart;
                        mAutoGoTimer.Start();
                    }
                }));
        }

        private void RefreshSlotInformation()
        {
            foreach (ChargeSlotItem chargeSlot in mAppSetting.ChargeSlotList)
            {
                KioskModuleType gateType = CheckModuleType(chargeSlot.GateAddress);
                KioskModuleType batteryType = CheckModuleType(chargeSlot.BatteryAddress);
                if (gateType == KioskModuleType.Gate && batteryType == KioskModuleType.Battery)
                {
                    chargeSlot.SlotStatus = true;
                }else{
                    chargeSlot.SlotStatus = false;
                }
            }
        }

        private KioskModuleType CheckModuleType(int address)
        {
            KioskModuleType type = KioskModuleType.None;

            object value = KioskAPI.Kiosk.SendModuleCmdByAddress(KioskModuleType.Gate,
                GTE.Command.DCONCmd.ReadModuleName, address, null);

            if (value != null)
            {
                type = KioskModuleType.Gate;
                if (value.ToString().Contains("7521"))
                {
                    type = KioskModuleType.Battery;
                }
            }

            return type;
        }

        private void RefreshSlotModuleViewUI()
        {
            moduleInfoListView.ItemsSource = mAppSetting.ChargeSlotList;
            System.ComponentModel.ICollectionView view = CollectionViewSource.GetDefaultView(moduleInfoListView.ItemsSource);
            view.Refresh();
        }

        private void RefreshModuleInformation()
        {
            mGateModuleItemList.Clear();
            mBatteryModuleItemList.Clear();

            for (int i = mAppSetting.StartAddress; i <= mAppSetting.EndAddress; i++)
            {
                object value = KioskAPI.Kiosk.SendModuleCmdByAddress(KioskModuleType.Gate,
                    GTE.Command.DCONCmd.ReadModuleName, i, null);

                if (value != null)
                {
                    if (value.ToString().Contains("7521"))
                    {
                        mBatteryModuleItemList.Add(new ModuleItem() { Address = i, Type = KioskModuleType.Battery });
                    }
                    else
                    {
                        mGateModuleItemList.Add(new ModuleItem() { Address = i, Type = KioskModuleType.Gate });
                    }
                }
            }
        }

        private void ShowChargeSlot(ChargeSlotItem chargeSlotItem)
        {
            if (chargeSlotItem != null)
            {
                slotDetailInfo.Visibility = System.Windows.Visibility.Visible;

                slotIdName.Text = chargeSlotItem.SlotId.ToString();

                SetGateModuleToComboBox();
                SetBatteryModuleToComboBox();
            }
            else
            {
                slotDetailInfo.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void openDoor_Button(object sender, RoutedEventArgs e)
        {
            mEnableSlotUpdate = false;

            refreshModuleButton.IsEnabled = false;
            addSlotButton.IsEnabled = false;
            deleteSlotButton.IsEnabled = false;
            editSlotButton.IsEnabled = false;
            refreshAddressButton.IsEnabled = false;

            openDoorButton.IsEnabled = false;
            UnlockDoorButton.IsEnabled = false;

            mGateAction = 1;
            mIsFirstEnable = true;

            mEnableSlotUpdate = true;
        }

        private void unlockDoor_Button(object sender, RoutedEventArgs e)
        {
            mEnableSlotUpdate = false;

            refreshModuleButton.IsEnabled = false;
            addSlotButton.IsEnabled = false;
            deleteSlotButton.IsEnabled = false;
            editSlotButton.IsEnabled = false;
            refreshAddressButton.IsEnabled = false;

            openDoorButton.IsEnabled = false;
            UnlockDoorButton.IsEnabled = false;

            mGateAction = 2;
            mIsFirstEnable = true;

            mEnableSlotUpdate = true;
        }

        private void cancelAction_Button(object sender, RoutedEventArgs e)
        {
            mGateAction = -1;
            mIsFirstEnable = false;

            countdownText.Text = "";

            refreshModuleButton.IsEnabled = true;
            addSlotButton.IsEnabled = true;
            deleteSlotButton.IsEnabled = true;
            editSlotButton.IsEnabled = true;
            refreshAddressButton.IsEnabled = true;

            openDoorButton.IsEnabled = true;
            UnlockDoorButton.IsEnabled = true;
        }

        public void ResetSlotUpdateScraner()
        {
            StopSlotUpdateScaner();

            mSlotUpdateThread = new Thread(new ThreadStart(SlotUpdater));
            mSlotUpdateThread.Start();
        }

        private void StopSlotUpdateScaner()
        {
            if (mSlotUpdateThread != null)
            {
                mSlotUpdateThread.Abort();
                mSlotUpdateThread = null;
            }
        }

        private void SlotUpdater()
        {
            while (true)
            {
                while (!mEnableSlotUpdate)
                {
                    Thread.Sleep(50);
                }

                bool isCancel = false;
                if (mGateAction > 0)
                {
                    if (mIsFirstEnable)
                    {
                        mIsFirstEnable = !mIsFirstEnable;

                        mActionDateTime = DateTime.Now;
                        mActionTime = 8;

                        if (!MainWindow.AppSettingFile.BatteryTypeVersion.Contains("4816"))
                        {
                            MainWindow.DeviceAdapter.SetGateStatusByAddress(mCurrentChargeSlotItem.GateAddress, KioskGateStatusCmd.FrontUnLockOn);
                        }
                        else
                        {
                            if (mGateAction == 1)
                            {
                                MainWindow.DeviceAdapter.SetGateStatusByAddress(mCurrentChargeSlotItem.GateAddress, KioskGateStatusCmd.FrontUnLockOn);
                            }
                            else if (mGateAction == 2)
                            {
                                MainWindow.DeviceAdapter.SetGateStatusByAddress(mCurrentChargeSlotItem.GateAddress, KioskGateStatusCmd.BackUnLockOn);
                            }
                        }

                        MainWindow.DeviceAdapter.SetGateStatusByAddress(mCurrentChargeSlotItem.GateAddress, KioskGateStatusCmd.GreenLightOn);
                        mIsSlotLight = true;
                    }
                    else
                    {
                        if (mActionTime > 0)
                        {
                            if (DateTime.Now > mActionDateTime.AddSeconds(1))
                            {
                                mActionDateTime = DateTime.Now;
                                mIsSlotLight = !mIsSlotLight;

                                if (mIsSlotLight)
                                {
                                    MainWindow.DeviceAdapter.SetGateStatusByAddress(mCurrentChargeSlotItem.GateAddress, KioskGateStatusCmd.GreenLightOn);
                                }
                                else
                                {
                                    MainWindow.DeviceAdapter.SetGateStatusByAddress(mCurrentChargeSlotItem.GateAddress, KioskGateStatusCmd.GreenLightOff);
                                }

                                mActionTime--;
                            }
                        }
                        else
                        {
                            isCancel = true;
                        }
                    }
                }
                else if (mGateAction < 0)
                {
                    mGateAction = 0;
                    MainWindow.DeviceAdapter.SetGateStatusByAddress(mCurrentChargeSlotItem.GateAddress, KioskGateStatusCmd.TurnOffAllStatus);
                }

                GateStatusInfo gss = MainWindow.DeviceAdapter.GetGateStatusBySlotId(mCurrentChargeSlotItem.SlotId);
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                    () =>
                    {
                        if (isCancel)
                        {
                            cancelAction_Button(null, null);
                        }
                                                    
                        countdownText.Text = mActionTime.ToString();

                        if (gss != null)
                        {
                            SetTextBackgroundColor(blueLight, gss.IsBlueLightOn);
                            SetTextBackgroundColor(greenLight, gss.IsGreenLightOn);
                            SetTextBackgroundColor(onCharge, gss.IsOnCharge);
                            SetTextBackgroundColor(frontLock, !gss.IsFrontDoorUnLock);
                            SetTextBackgroundColor(backLock, !gss.IsBackDoorUnLock);
                            SetTextBackgroundColor(insertBattery, gss.IsBatteryInserted);
                            SetTextBackgroundColor(frontDoor, !gss.IsFrontDoorClose);
                            SetTextBackgroundColor(backDoor, !gss.IsBatterySensoredLock);
                            SetTextBackgroundColor(fanDetecetd, gss.IsFanRun);
                        }

                    }));
                Thread.Sleep(200);
            }
        }

        private void SetTextBackgroundColor(TextBox textbox, bool isEnable)
        {
            if (isEnable)
            {
                textbox.Background = Brushes.Green;
            }
            else
            {
                textbox.Background = Brushes.White;
            }
        }

        #region Gate Module
        /// <summary>
        /// 設定Gate Module列表至ComboBox
        /// </summary>
        private void SetGateModuleToComboBox()
        {
            gateAddressList.Items.Clear();
            for (int i = 0; i < mGateModuleItemList.Count; i++)
            {
                gateAddressList.Items.Add(mGateModuleItemList[i].Address);

                if (mCurrentChargeSlotItem != null && mCurrentChargeSlotItem.GateAddress == mGateModuleItemList[i].Address)
                {
                    gateAddressList.SelectedIndex = i;
                }
            }
        }
        #endregion
        #region Battery Module
        /// <summary>
        /// 設定Battery Module列表至ComboBox
        /// </summary>
        private void SetBatteryModuleToComboBox()
        {
            batteryAddressList.Items.Clear();
            for (int i = 0; i < mBatteryModuleItemList.Count; i++)
            {
                batteryAddressList.Items.Add(mBatteryModuleItemList[i].Address);

                if (mCurrentChargeSlotItem != null && mCurrentChargeSlotItem.BatteryAddress == mBatteryModuleItemList[i].Address)
                {
                    batteryAddressList.SelectedIndex = i;
                }
            }
        }
        #endregion
        private void saveAddressButton_Click(object sender, RoutedEventArgs e)
        {
            if (mCurrentChargeSlotItem != null && batteryAddressList.SelectedItem != null && gateAddressList.SelectedItem != null)
            {
                mCurrentChargeSlotItem.BatteryAddress = (int)batteryAddressList.SelectedItem;
                mCurrentChargeSlotItem.GateAddress = (int)gateAddressList.SelectedItem;
            }
            else
            {

            }
        }
        #endregion

        #region Language
        /// <summary>
        /// 設定Language列表至ComboBox
        /// </summary>
        private void SetLanguageToComboBox()
        {
            string[] list = mAppSetting.LangList;
            langList.Items.Clear();
            for (int i = 0; i < list.Length; i++)
            {
                langList.Items.Add(list[i]);
                if (list[i].Equals(mAppSetting.Lang))
                {
                    langList.SelectedIndex = i;
                }
            }
        }
        #endregion

        #region Battery Type
        /// <summary>
        /// 設定Battery Type列表至ComboBox
        /// </summary>
        private void SetBatteryTypeToComboBox()
        {
            string[] list = mAppSetting.BatteryTypeVersionList;
            batteryTypeList.Items.Clear();
            for (int i = 0; i < list.Length; i++)
            {
                batteryTypeList.Items.Add(list[i]);
                if (list[i].Equals(mAppSetting.BatteryTypeVersion))
                {
                    batteryTypeList.SelectedIndex = i;
                }
            }
        }
        #endregion

        #region Scan Port
        /// <summary>
        /// 初始化ComPort監聽時間器
        /// </summary>
        private void InitializeScanPort()
        {
            mPortScanTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(KioskParameter.SCANPORTTIME) };
            mPortScanTimer.Tick += PortScanTimer_Tick;
            mPortScanTimer.Start();
        }
        /// <summary>
        /// 啟動監聽ComPort狀態之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PortScanTimer_Tick(object sender, EventArgs e)
        {
            SetSerialPortToComboBox(false);
            SetRFIDPortToComboBox(false);
        }
        /// <summary>
        /// 設定Port列表至ComboBox
        /// </summary>
        private void SetSerialPortToComboBox(bool isFirstTime)
        {
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();
            if (ports != null && ports.Length > 0)
            {
                bool isChanged = false;
                foreach (object data in portList.Items)
                {
                    if (!ports.Contains(data.ToString()))
                    {
                        isChanged = true;
                    }
                }

                if (isChanged || isFirstTime)
                {
                    string port = null;
                    int index = 0;
                    if (portList.SelectedItem != null)
                    {
                        port = portList.SelectedItem.ToString();
                    }

                    portList.Items.Clear();
                    for (int i = 0; i < ports.Length; i++)
                    {
                        if (isFirstTime)
                        {
                            if (ports[i].Equals(mAppSetting.Port))
                            {
                                index = i;
                            }
                        }
                        else
                        {
                            if (ports[i].Equals(port))
                            {
                                index = i;
                            }
                        }

                        portList.Items.Add(ports[i]);
                    }
                    portList.SelectedIndex = index;
                }
            }
        }
        /// <summary>
        /// 設定Port列表至ComboBox
        /// </summary>
        private void SetRFIDPortToComboBox(bool isFirstTime)
        {
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();
            if (ports != null && ports.Length > 0)
            {
                bool isChanged = false;
                foreach (object data in portList.Items)
                {
                    if (!ports.Contains(data.ToString()))
                    {
                        isChanged = true;
                    }
                }

                if (isChanged || isFirstTime)
                {
                    string port = null;
                    int index = 0;
                    if (rfidportList.SelectedItem != null)
                    {
                        port = rfidportList.SelectedItem.ToString();
                    }

                    rfidportList.Items.Clear();
                    for (int i = 0; i < ports.Length; i++)
                    {
                        if (isFirstTime)
                        {
                            if (ports[i].Equals(mAppSetting.RFIDPort))
                            {
                                index = i;
                            }
                        }
                        else
                        {
                            if (ports[i].Equals(port))
                            {
                                index = i;
                            }
                        }

                        rfidportList.Items.Add(ports[i]);
                    }
                    rfidportList.SelectedIndex = index;
                }
            }
        }
        #endregion

        #region Baud Rate
        /// <summary>
        /// 設定BaudRate列表至ComboBox
        /// </summary>
        private void SetBaudRateToComboBox()
        {
            for (int i = 0; i < mAppSetting.BaudRateList.Length; i++)
            {
                baudRateList.Items.Add(mAppSetting.BaudRateList[i]);
                if (mAppSetting.BaudRate == mAppSetting.BaudRateList[i])
                {
                    baudRateList.SelectedIndex = i;
                }
            }
        }
        #endregion

        #region ScreenOrientation
        /// <summary>
        /// 設定ScreenOrientation列表至ComboBox
        /// </summary>
        private void SetScreenOrientationeToComboBox()
        {
            for (int i = 0; i < mAppSetting.ScreenOrientationList.Length; i++)
            {
                sorientationList.Items.Add(mAppSetting.ScreenOrientationList[i]);
                if (mAppSetting.ScreenOrientation != null && mAppSetting.ScreenOrientation.Equals(mAppSetting.ScreenOrientationList[i]))
                {
                    sorientationList.SelectedIndex = i;
                }
            }
        }
        #endregion

        private void BattButton_Click(object sender, RoutedEventArgs e)
        {
            infoTabControl.SelectedIndex = 0;
            mEnableSlotUpdate = false;
        }

        private void SettingButton_Click(object sender, RoutedEventArgs e)
        {
            infoTabControl.SelectedIndex = 1;
            if (mCurrentChargeSlotItem != null)
            {
                mEnableSlotUpdate = true;
            }
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            if (!mIsInitStart)
            {
                MainWindow.KioskMgr.ResetAbnormalInformation();
                MainWindow.GoSystemRun(SystemRunMode.UpdateApp);
            }
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                mAppSetting.ScreenOrientation = sorientationList.SelectedItem.ToString();


                mAppSetting.IsMute = isMuteButton.IsChecked.Value;

                mAppSetting.Lang = langList.SelectedItem.ToString();

                mAppSetting.Latitude = Convert.ToDouble(latiText);
                mAppSetting.Longitude = Convert.ToDouble(longText);

                mAppSetting.ServerHost = hostText.Text;
                mAppSetting.ServerPort = portText.Text;
            }
            catch
            {

            }

            MainWindow.SaveAppSetting();
            MainWindow.ChangeLang();
        }

        private bool CheckAddress()
        {
            int address = 0;
            bool isInt = int.TryParse(startAddressTextBox.Text, out address);
            isInt = isInt && (address >= 0);
            if (isInt)
            {
                mAppSetting.StartAddress = address;
            }
            isInt = isInt && int.TryParse(endAddressTextBox.Text, out address);
            isInt = isInt && (address > 0);
            if (isInt)
            {
                mAppSetting.EndAddress = address;
            }

            if (isInt)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 只能輸入數字之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KeyDownAddress_Click(object sender, KeyEventArgs e)
        {
            if (!((e.Key >= Key.D0 && e.Key <= Key.D9) ||
                (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)))
            {
                e.Handled = true;
            }
        }

        private void UpdateSlotBatteryInfo()
        {
            List<SlotBatteryStatusInfo> tempSlotBatteryStatusInfoList = new List<SlotBatteryStatusInfo>();

            foreach (ChargeSlotItem chargeSlot in mAppSetting.ChargeSlotList)
            {
                SlotBatteryStatusInfo item = new SlotBatteryStatusInfo();
                item.SlotId = chargeSlot.SlotId.ToString();
                BatteryStatusInfo bbsi = MainWindow.DeviceAdapter.GetBatteryStatusBySlotId(chargeSlot.SlotId);

                GateStatusInfo gsi = MainWindow.DeviceAdapter.GetGateStatusBySlotId(chargeSlot.SlotId);
                if (gsi != null && gsi.IsBatteryInserted && !gsi.IsFrontDoorClose)
                {
                    item.IsExistBattery = true;
                }
                if (bbsi == null && item.IsExistBattery)
                {
                    Thread.Sleep(500);
                    bbsi = MainWindow.DeviceAdapter.GetBatteryStatusBySlotId(chargeSlot.SlotId);
                }

                if (bbsi != null)
                {
                    item.EnableAlarmCode = 1;

                    if (bbsi.FCC == 0)
                    {
                        item.EnableAlarmCode = 2;
                    }
                    else if (100 < (bbsi.RC * 100 / bbsi.FCC))
                    {
                        item.EnableAlarmCode = 2;
                    }
                    for(int i=0;i<7;i++){
                        if (bbsi.Protection[i] > 0)
                        {
                            item.EnableAlarmCode = 3 + i;
                            break;
                        }
                    }

                    item.SOC = bbsi.SOC.ToString();
                    item.Current = bbsi.Current.ToString();

                    int totalVolt = 0;
                    int minVolt = 0;
                    int maxVolt = 0;
                    if (bbsi.CellsVoltage != null)
                    {
                        for (int i = 0; i < bbsi.CellsVoltage.Length; i++)
                        {
                            totalVolt = totalVolt + bbsi.CellsVoltage[i];
                            if (i == 0)
                            {
                                minVolt = bbsi.CellsVoltage[i];
                                maxVolt = bbsi.CellsVoltage[i];
                            }
                            else
                            {
                                if (bbsi.CellsVoltage[i] < minVolt)
                                {
                                    minVolt = bbsi.CellsVoltage[i];
                                }
                                if (bbsi.CellsVoltage[i] > maxVolt)
                                {
                                    maxVolt = bbsi.CellsVoltage[i];
                                }
                            }
                        }
                        item.Voltage = totalVolt.ToString();
                        item.VMax = maxVolt.ToString();
                        item.VMin = minVolt.ToString();
                        item.VDiff = (maxVolt - minVolt).ToString();
                    }
                }

                item.isShow = true;
                tempSlotBatteryStatusInfoList.Add(item);
            }

            if (mAppSetting.ChargeSlotList.Count < 6)
            {
                int hideNumber = 6 - mAppSetting.ChargeSlotList.Count;
                for (int i = 0; i < hideNumber; i++)
                {
                    SlotBatteryStatusInfo item = new SlotBatteryStatusInfo();
                    tempSlotBatteryStatusInfoList.Add(item);
                }
            }

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    batterySocGrid.Children.Clear();

                    foreach (SlotBatteryStatusInfo slotBatteryStatusInfo in tempSlotBatteryStatusInfoList)
                    {
                        SlotBatteryStatus item = new SlotBatteryStatus();
                        item.Title = slotBatteryStatusInfo.SlotId;
                        if (slotBatteryStatusInfo.isShow)
                        {
                            if (slotBatteryStatusInfo.IsExistBattery)
                            {
                                switch (slotBatteryStatusInfo.EnableAlarmCode)
                                {
                                    case 1:
                                        {
                                            item.StatusBackground = Brushes.Green;
                                            item.StatusText = "Good";
                                            break;
                                        }
                                    case 2:
                                        {
                                            item.StatusBackground = Brushes.GreenYellow;
                                            item.StatusText = "SOC Error";
                                            break;
                                        }
                                    default:
                                        {
                                            if ((slotBatteryStatusInfo.EnableAlarmCode - 3) < 0)
                                            {
                                                item.StatusBackground = Brushes.Red;
                                                item.StatusText = "Error";
                                            }
                                            else
                                            {
                                                item.StatusBackground = Brushes.Gray;
                                                item.StatusText = string.Format("Protection - {0}", slotBatteryStatusInfo.EnableAlarmCode - 3);
                                            }

                                            break;
                                        }
                                }

                                item.SOCText = slotBatteryStatusInfo.SOC;
                                item.CurrentText = slotBatteryStatusInfo.Current;
                                item.VoltageText = slotBatteryStatusInfo.Voltage;
                                item.VmaxText = slotBatteryStatusInfo.VMax;
                                item.VminText = slotBatteryStatusInfo.VMin;
                                item.VdiffText = slotBatteryStatusInfo.VDiff;
                            }
                            else
                            {
                                item.StatusBackground = Brushes.Blue;
                            }
                        }
                        else
                        {
                            item.Visibility = System.Windows.Visibility.Hidden;
                        }

                        batterySocGrid.Children.Add(item);
                    }
                    Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                }));
        }

        private void updateSlotBattery_Button(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
            ThreadPool.QueueUserWorkItem(o => UpdateSlotBatteryInfo());
        }

        private void isMuteButton_Click(object sender, RoutedEventArgs e)
        {
            if (isMuteButton.IsChecked.Value)
            {
                isMuteButton.Content = "Enable";
            }
            else
            {
                isMuteButton.Content = "Disable";
            }
        }

        /// <summary>
        /// 初始化AutoGo時間器
        /// </summary>
        private void InitializeAutoGo()
        {
            mAutoGoTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(1) };
            mAutoGoTimer.Tick += AutoGoTimer_Tick;
        }
        /// <summary>
        /// 啟動監聽AutoGo狀態之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoGoTimer_Tick(object sender, EventArgs e)
        {
            if (mGoTimer < 1)
            {
                HomeButton_Click(null, null);
            }
            mGoTimer--;
            countDownValue.Content = mGoTimer.ToString();
        }
        private void autoGoButton_Click(object sender, RoutedEventArgs e)
        {
            if (!autoGoButton.IsChecked.Value)
            {
                autoGoButton.Content = "YES";
                mGoTimer = KioskParameter.AUTOGOTIME;
                countDownValue.Content = mGoTimer.ToString();
            }
            else
            {
                autoGoButton.Content = "NO";
                mGoTimer = KioskParameter.AUTOGOKIOSKTIME;
                countDownValue.Content = mGoTimer.ToString();
            }
        }

        public void RestGoTimer()
        {
            if ("NO".Contains(autoGoButton.Content.ToString()))
            {
                mGoTimer = KioskParameter.AUTOGOKIOSKTIME;
            }
        }

        private void rfidSaveButton_Click(object sender, RoutedEventArgs e)
        {
            mAppSetting.RFIDPort = rfidportList.SelectedItem.ToString();
            MainWindow.SaveAppSetting();
            MainWindow.ConnectRFIDAPI();
        }

        private void rfidQuery_Click(object sender, RoutedEventArgs e)
        {
            Button bt = sender as Button;
            bt.IsEnabled = false;

            cardIdText.Text = "";
            userIdText.Text = "";
            pointText.Text = "";
            ThreadPool.QueueUserWorkItem(o => ScanRFIDCard());            
        }


        private void createCard_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.KioskMgr.CreateCard(userIdText.Text);
        }

        private void ScanRFIDCard()
        {
            mDetectRFIDTimer = KioskParameter.DETECTRFIDTIME;
            mRFIDDetectTimer.Start();
            RFIDCardInfo card = MainWindow.KioskMgr.ScanRFIDCard();

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    CloseRFIDDetected();
                    if (card != null)
                    {
                        cardIdText.Text = card.CardId;
                        userIdText.Text = card.UserId;
                        pointText.Text = card.Point.ToString();
                    }
                    queryButton.IsEnabled = true;
                }
            ));

        }
        /// <summary>
        /// 初始化RFID Timer時間器
        /// </summary>
        private void InitializeDetectRFID()
        {
            mRFIDDetectTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(1) };
            mRFIDDetectTimer.Tick += DetectRFIDTimer_Tick;
        }
        /// <summary>
        /// 啟動監聽RFID Timer狀態之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DetectRFIDTimer_Tick(object sender, EventArgs e)
        {
            if (mDetectRFIDTimer < 1)
            {
                CloseRFIDDetected();
            }
            mDetectRFIDTimer--;
            rfidCountdownText.Text = mDetectRFIDTimer.ToString();
        }

        private void CloseRFIDDetected()
        {
            MainWindow.KioskMgr.IsRefreshRFID = false;
            mRFIDDetectTimer.Stop();
        }
        /// <summary>
        /// 設定Geolocation
        /// </summary>
        private void SetGeolocation()
        {
            latiText.Text = mAppSetting.Latitude.ToString();
            longText.Text = mAppSetting.Longitude.ToString();
        }

        private void SetServerAddress()
        {
            hostText.Text = mAppSetting.ServerHost;
            portText.Text = mAppSetting.ServerPort;
        }

        private void Shutdown(object sender, EventArgs e)
        {
            foreach (ChargeSlotItem chargeSlot in mAppSetting.ChargeSlotList)
            {
                MainWindow.DeviceAdapter.SetGateStatusBySlotId(chargeSlot.SlotId, GTEKioskLib.Command.KioskGateStatusCmd.TurnOffAllStatus);
            }

            System.Diagnostics.Process.Start("shutdown", "/s /t 0");
        }
    }
}
