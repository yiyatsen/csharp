﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEKiosk
{
    /// <summary>
    /// AdWindow.xaml 的互動邏輯
    /// </summary>
    public partial class AdWindow : Window
    {
        readonly IEnumerable<BitmapImage> _images;
        IEnumerator<BitmapImage> _imageEnum;

        private DispatcherTimer mAdTimer = new DispatcherTimer();
        private bool mIsFirstTime = true;

        public AdWindow()
        {
            InitializeComponent();

            // 需要考慮同時更新線上資訊之機制
            string exe = System.AppDomain.CurrentDomain.BaseDirectory + @"res\Ad\" + MainWindow.AppSettingFile.ScreenOrientation + @"\Project\";

            _images =
                from file in System.IO.Directory.GetFiles(exe, "*.png")
                orderby file
                let uri = new Uri(file, UriKind.Absolute)
                select new BitmapImage(uri);

            StartAd();
        }

        private void StartAd()
        {
            mAdTimer.Tick += AdTimer_Tick;
            mAdTimer.Start();
        }


        private void AdTimer_Tick(object sender, EventArgs e)
        {
            if (mIsFirstTime)
            {
                mIsFirstTime = false;
                mAdTimer.Interval = TimeSpan.FromMilliseconds(MainWindow.AppSettingFile.AdUpdateTime * 1000);
            }

            if (_imageEnum == null || !_imageEnum.MoveNext())
            {
                _imageEnum = _images.GetEnumerator();
                _imageEnum.MoveNext();
            }

            this.image.Source = _imageEnum.Current;
        }
    }
}
