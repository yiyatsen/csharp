﻿using GTEKioskConfig.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKiosk.Trans
{
    public abstract class TransCheck
    {
        private bool mIsCopyIdToDevice { get; set; }
        public bool IsCopyIdToDevice { get { return mIsCopyIdToDevice; } }


        public abstract bool CheckCardInfo(CardInfo cardInfo);
        /// <summary>
        /// 需要實作交易確認之方式
        /// 身份辨識演算法(線上比對, 資料庫比對, 電池卡片比對)
        /// /////////////////////////
        /// </summary>
        /// <param name="batteryTransIdInfo"></param>
        /// <param name="cardInfo"></param>
        /// <returns></returns>
        public abstract int CheckTransIdInfo(BatteryTransIdInfo batteryTransIdInfo, CardInfo cardInfo);
    }
}
