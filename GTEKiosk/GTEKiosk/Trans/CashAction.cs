﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTEKiosk.Trans
{
    public enum CashAction
    {
        None,
        DetectedCard,
        NoCardCancelTrans,
        QueryCash
    }
}
