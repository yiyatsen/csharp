﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKiosk.Trans
{
    public enum TransAction
    {
        None,
        DetectedCard,
        InsertedBattery,
        CheckTransInfo,
        DrawOutOriginalBattery,
        DrawOutOriginalBatteryCheckBatteryAndCardError,
        DrawOutNewBattery,
        CancelTransByNoCard,
        CancelTransByNoDetectedBattery,
        CancelTransByCheckBatteryAndCardError,
        CancelTransByNonDrawOutNewBattery,
        CancelTrans,
        FinishTrans
    }
}
