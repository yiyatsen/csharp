﻿using GTE.Device;
using GTE.Encryption;
using GTEKioskConfig.InfoModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace GTEKioskLib.RFID
{
    public class RFIDAPI
    {
        [DllImport("680api.dll", CharSet = CharSet.Ansi)]
        public static extern int E680_Request_CardSN(StringBuilder serial);
        [DllImport("680api.dll")]
        public static extern int E680_Close_ComPort();
        [DllImport("680api.dll", CharSet = CharSet.Ansi)]
        public static extern int E680_Open_ComPort(int port);
        [DllImport("680api.dll", CharSet = CharSet.Ansi)]
        public static extern int E680_loadkey_by_string(byte keyid, StringBuilder keyvalue);
        [DllImport("680api.dll", CharSet = CharSet.Ansi)]
        public static extern int E680_block_read_by_string(byte block, byte keytype, byte keyid, StringBuilder data);
        [DllImport("680api.dll", CharSet = CharSet.Ansi)]
        public static extern int E680_block_write_by_string(byte block, byte keytype, byte keyid, StringBuilder data);
        [DllImport("680api.dll", CharSet = CharSet.Ansi)]
        public static extern int E680_initvalue(byte block, byte keytype, byte keyid, long value);
        [DllImport("680api.dll", CharSet = CharSet.Ansi)]
        public static extern int E680_readvalue(byte block, byte keytype, byte keyid, ref long value);
        [DllImport("680api.dll", CharSet = CharSet.Ansi)]
        public static extern int E680_increment(byte block, byte keytype, byte keyid, uint value);
        [DllImport("680api.dll", CharSet = CharSet.Ansi)]
        public static extern int E680_decrement(byte block, byte keytype, byte keyid, uint value);

        private bool mIsConnected = false;
        private EncryptionEWT680 mCryptography { get; set; }
        private string keyA;
        private string keyB;
        private string Writekey;
        private int write_card_block = 27;
        private int write_card_dis_block = 11;
        private int write_dis_length_block = 31;
        private int userid_block = 26;
        private int issuedatetime_block = 25;
        private int point_block = 24;
        private int dis_block1 = 8;
        private int dis_block2 = 9;
        private int dis_block3 = 10;
        private int dis_length_block1 = 28;
        private int dis_length_block2 = 29;
        private int TranDatetime_block = 30;

        public RFIDAPI()
		{
            mCryptography = new EncryptionEWT680();
		}

        public bool Connect(string port)
        {
            int portNumber = 0;
            if (port != null && !port.Equals(""))
            {
                int.TryParse(port.Replace("COM",""), out portNumber);
            }

            mIsConnected = (E680_Open_ComPort(portNumber) == 1);
            if (mIsConnected)
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("FFFFFFFFFFFF");
                int a = E680_loadkey_by_string(0, stringBuilder);
            }
            return mIsConnected;
        }

        public bool Disconnect()
        {
            if (E680_Close_ComPort() == 1)
            {
                mIsConnected = false;
                return true;
            }
            return false ;
        }

        public bool IsConnect()
        {
            return mIsConnected;
        }
        /// <summary>
        /// 加值點數
        /// </summary>
        /// <param name="addPoint"></param>
        /// <returns></returns>
        public bool Increment(int addPoint)
        {
            bool result = false;
            if (E680_increment(byte.Parse(this.point_block.ToString()), 0, 1, Convert.ToUInt32(addPoint)) == 1)
            {
                result = true;
            }
            return result;
        }
        /// <summary>
        /// 扣點數
        /// </summary>
        /// <param name="substractPoint"></param>
        /// <returns></returns>
        public bool Decrement(int substractPoint)
        {
            bool result = false;
            if (E680_decrement(byte.Parse(this.point_block.ToString()), 0, 1, Convert.ToUInt32(substractPoint)) == 1)
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        /// 產生新卡
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="issuedatetime"></param>
        /// <param name="point"></param>
        /// <param name="displayname"></param>
        /// <returns></returns>
        public bool OpenCard(string userId, DateTime issuedatetime)
        {
            bool result = false;
            string value = TransUserID(userId);
            string value2 = this.tranissuedatetime(issuedatetime);

            //string text = "";
            //string value3 = TransDisplayName(displayname, ref text);
            //text = this.tran_len_displayname(text);
            //if (E680_block_write_by_string(byte.Parse(this.write_card_block.ToString()), 0, 0, new StringBuilder(256).Append(this.Writekey)) == 1 
            //    && E680_block_write_by_string(byte.Parse(this.write_card_dis_block.ToString()), 0, 0, new StringBuilder(256).Append(this.Writekey)) == 1 
            //    && E680_block_write_by_string(byte.Parse(this.write_dis_length_block.ToString()), 0, 0, new StringBuilder(256).Append(this.Writekey)) == 1 
            //    && E680_block_write_by_string(byte.Parse(this.userid_block.ToString()), 0, 1, new StringBuilder(256).Append(value)) == 1 
            //    && E680_block_write_by_string(byte.Parse(this.issuedatetime_block.ToString()), 0, 1, new StringBuilder(256).Append(value2)) == 1 
            //    && E680_initvalue(byte.Parse(this.point_block.ToString()), 0, 1, long.Parse(point.ToString())) == 1 
            //    && E680_block_write_by_string(byte.Parse(this.dis_block1.ToString()), 0, 1, new StringBuilder(256).Append(value3)) == 1 
            //    && E680_block_write_by_string(byte.Parse(this.dis_length_block1.ToString()), 0, 1, new StringBuilder(256).Append(text)) == 1 
            //    && E680_block_write_by_string(byte.Parse(this.TranDatetime_block.ToString()), 0, 1, new StringBuilder(256).Append(value2)) == 1)
            //{
            //    result = true;
            //}

            if (E680_block_write_by_string(byte.Parse(this.userid_block.ToString()), 0, 1, new StringBuilder(256).Append(value)) == 1
                && E680_block_write_by_string(byte.Parse(this.issuedatetime_block.ToString()), 0, 1, new StringBuilder(256).Append(value2)) == 1)
            {
                result = true;
            }

            return result;
        }

        public RFIDCardInfo Refresh_Card()
        {
            StringBuilder stringBuilder = new StringBuilder(256);
            //this.is_Card = false;
            //this.mCard_Infor.CardSID = "";
            if (E680_Request_CardSN(stringBuilder) == 1 && stringBuilder.ToString().Length > 0)
            {
                string cardId =  stringBuilder.ToString().Trim();
                string text = mCryptography.EncryptionBytesDataToString(mCryptography.EncryptionData(mCryptography.EncryptionStringDataToBytes(cardId)));

                this.keyA = text.Substring(0, 12);
                this.keyB = text.Substring(13, 12);
                this.Writekey = this.keyA + "FF078069" + this.keyB;
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder2.Append(this.keyA);
                int num = E680_loadkey_by_string(1, stringBuilder2);
                stringBuilder2 = new StringBuilder(256);
                num = E680_block_read_by_string(byte.Parse(this.userid_block.ToString()), 0, 1, stringBuilder2);

                // 確認是否有取得
                if (num == 1)
                {
                    string issueDateTime = null;
                    string tranDateTime = null;

                    string userId = GetUserID(stringBuilder2.ToString());

                    long point = 0L;
                    E680_readvalue(byte.Parse(this.point_block.ToString()), 0, 1, ref point);


                    stringBuilder2 = new StringBuilder(256);
                    if (E680_block_read_by_string(byte.Parse(this.issuedatetime_block.ToString()), 0, 1, stringBuilder2) == 1)
                    {
                        issueDateTime = this.Get_issuedatetime(stringBuilder2.ToString()).ToString();
                    }
                    stringBuilder2 = new StringBuilder(256);
                    if (E680_block_read_by_string(byte.Parse(this.TranDatetime_block.ToString()), 0, 1, stringBuilder2) == 1)
                    {
                        tranDateTime = this.Get_issuedatetime(stringBuilder2.ToString()).ToString();
                    }

                    return new RFIDCardInfo()
                    {
                        CardId = cardId,
                        UserId = userId,
                        Point = point,
                        IssueDateTime = issueDateTime,
                        TranDateTime = tranDateTime
                    };
                }
            }
            return null;
        }

        private string GetUserID(string cardUserId)
        {
            cardUserId = cardUserId.Substring(0, 20);
            string text = "";
            for (int i = 0; i < cardUserId.Length; i += 2)
            {
                if (text.Length == 0)
                {
                    text = cardUserId.Substring(i, 2);
                }
                else
                {
                    text = text + " " + cardUserId.Substring(i, 2);
                }
            }
            string[] array = text.Split(new char[]
			{
				' '
			});
            string text2 = "";
            string[] array2 = array;
            for (int j = 0; j < array2.Length; j++)
            {
                string value = array2[j];
                int utf = Convert.ToInt32(value, 16);
                string str = char.ConvertFromUtf32(utf);
                text2 += str;
            }
            return text2;
        }

        private string tran_len_displayname(string len_displayname)
        {
            int num = 32 - len_displayname.Length;
            string text = "";
            for (int i = 0; i < num; i++)
            {
                text += "F";
            }
            return len_displayname + text;
        }

        private string tranissuedatetime(DateTime issuedatetime)
        {
            string text = issuedatetime.ToString("yyyyMMddHHmmss");
            char[] array = text.ToCharArray();
            string str = "";
            char[] array2 = array;
            for (int i = 0; i < array2.Length; i++)
            {
                char value = array2[i];
                int num = Convert.ToInt32(value);
                str += string.Format("{0:X}", num);
            }
            return str + "FF";
        }

        private string TransDisplayName(string displayname, ref string displayname_len)
        {
            string text = displayname;
            char[] array = text.ToCharArray();
            displayname = "";
            displayname_len = "";
            char[] array2 = array;
            for (int i = 0; i < array2.Length; i++)
            {
                char value = array2[i];
                int num = Convert.ToInt32(value);
                displayname += string.Format("{0:X}", num);
                displayname_len += string.Format("{0:X}", Convert.ToInt32(string.Format("{0:X}", num).Length.ToString().ToCharArray()[0]));
            }
            return displayname;
        }

        private DateTime Get_issuedatetime(string card_issuedatetime)
        {
            card_issuedatetime = card_issuedatetime.Substring(0, 28);
            string text = "";
            for (int i = 0; i < card_issuedatetime.Length; i += 2)
            {
                if (text.Length == 0)
                {
                    text = card_issuedatetime.Substring(i, 2);
                }
                else
                {
                    text = text + " " + card_issuedatetime.Substring(i, 2);
                }
            }
            string[] array = text.Split(new char[]
			{
				' '
			});
            string text2 = "";
            string[] array2 = array;
            for (int j = 0; j < array2.Length; j++)
            {
                string value = array2[j];
                int utf = Convert.ToInt32(value, 16);
                string str = char.ConvertFromUtf32(utf);
                text2 += str;
            }
            return DateTime.Parse(string.Concat(new string[]
			{
				text2.Substring(0, 4),
				"-",
				text2.Substring(4, 2),
				"-",
				text2.Substring(6, 2),
				" ",
				text2.Substring(8, 2),
				":",
				text2.Substring(10, 2),
				":",
				text2.Substring(12, 2)
			}));
        }

        private string TransUserID(string userID)
        {
            char[] array = userID.ToCharArray();
            string str = "";
            char[] array2 = array;
            for (int i = 0; i < array2.Length; i++)
            {
                char value = array2[i];
                int num = Convert.ToInt32(value);
                str += string.Format("{0:X}", num);
            }
            return str + "FFFFFFFFFFFF";
        }
    }
}
