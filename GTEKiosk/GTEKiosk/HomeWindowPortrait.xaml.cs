﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GTEKiosk
{
    /// <summary>
    /// HomeWindowPortrait.xaml 的互動邏輯
    /// </summary>
    public partial class HomeWindowPortrait : Window
    {
        public HomeWindowPortrait()
        {
            InitializeComponent();
        }

        public void RefreshUI(bool isShow)
        {
            if (isShow)
            {
                diagnosticButton.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                diagnosticButton.Visibility = System.Windows.Visibility.Hidden;
            }

            if (MainWindow.KioskMgr.IsExistAbnormalDevice())
            {
                errorBatteryImage.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                errorBatteryImage.Visibility = System.Windows.Visibility.Hidden;
            }

            if (MainWindow.KioskMgr.EnableTrans())
            {
                ImageBrush ib = new ImageBrush();

                ib.ImageSource = new BitmapImage(new Uri(@"res/image/Portrait/ExnBattery.png", UriKind.Relative));

                exnImage.Background = ib;
            }
            else
            {
                ImageBrush ib = new ImageBrush();

                ib.ImageSource = new BitmapImage(new Uri(@"res/image/Portrait/nonExnBattery.png", UriKind.Relative));

                exnImage.Background = ib;
            }
        }

        private void searchMapButton_Click(object sender, RoutedEventArgs e)
        {
            Button bt = sender as Button;
            bt.IsEnabled = false;

            MainWindow.GoMap();

            bt.IsEnabled = true;
        }

        private void exChangeButton_Click(object sender, RoutedEventArgs e)
        {
            Button bt = sender as Button;
            bt.IsEnabled = false;

            if (MainWindow.KioskMgr.EnableTrans())
            {
                MainWindow.TransDevice(GTEKiosk.Trans.TransAction.DetectedCard);
            }
            else
            {
                MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.NoBattery.ToString()));
            }

            bt.IsEnabled = true;
        }

        private void diagnosticButton_Click(object sender, RoutedEventArgs e)
        {
            Button bt = sender as Button;
            bt.IsEnabled = false;

            MainWindow.GoDiagnostic(MainWindow.IsDiagnosticENGMode);

            //bt.IsEnabled = true;
        }

        private void langButton_Click(object sender, RoutedEventArgs e)
        {
            Button bt = sender as Button;

            if (bt != null && bt.Tag != null)
            {
                MainWindow.CulturesHelper.ChangeCulture(bt.Tag.ToString());
                MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.MainMenu.ToString()));
            }
        }

        private void memberButton_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow.GoMember();

            MainWindow.GoCash();
        }
    }
}
