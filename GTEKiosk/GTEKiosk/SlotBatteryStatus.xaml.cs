﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GTEKiosk
{
    /// <summary>
    /// SlotBatteryStatus.xaml 的互動邏輯
    /// </summary>
    public partial class SlotBatteryStatus : UserControl
    {
        public SlotBatteryStatus()
        {
            InitializeComponent();
        }

        public string Title
        {
            get { return this.titleText.Text; }
            set { this.titleText.Text = "-" + value + "-"; }
        }

        public System.Windows.Media.Brush StatusBackground
        {
            get { return this.Status.Background; }
            set { this.Status.Background = value; }
        }

        public string StatusText
        {
            get { return this.Status.Text; }
            set { this.Status.Text = value; }
        }

        public string SOCText
        {
            get { return this.SOC.Text; }
            set { this.SOC.Text = value; }
        }

        public string VmaxText
        {
            get { return this.Vmax.Text; }
            set { this.Vmax.Text = value; }
        }

        public string VminText
        {
            get { return this.Vmin.Text; }
            set { this.Vmin.Text = value; }
        }

        public string VdiffText
        {
            get { return this.Vdiff.Text; }
            set { this.Vdiff.Text = value; }
        }

        public string VoltageText
        {
            get { return this.Voltage.Text; }
            set { this.Voltage.Text = value; }
        }

        public string CurrentText
        {
            get { return this.Current.Text; }
            set { this.Current.Text = value; }
        }
    }
}
