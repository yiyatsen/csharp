﻿using GTEKiosk.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEKiosk
{
    /// <summary>
    /// SystemRunWindow.xaml 的互動邏輯
    /// </summary>
    public partial class SystemRunWindow : Window
    {
        public SystemRunWindow()
        {
            InitializeComponent();
        }

        public void Start(SystemRunMode mode)
        {
            switch (mode)
            {
                case SystemRunMode.EnterApp:
                    {
                        ThreadPool.QueueUserWorkItem(o => EnterApp());

                        break;
                    }
                case SystemRunMode.UpdateApp:
                    {
                        ThreadPool.QueueUserWorkItem(o => UpdateApp());

                        break;
                    }
                default:
                    {
                        MainWindow.BackToHome(false);
                        break;
                    }
            }
        }

        private void UpdateApp()
        {
            // database資料
            MainWindow.KioskMgr.ScanSlotInfo(1);
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    //MainWindow.BackToHome(false);
                    MainWindow.KioskMgr.IsStopScanSlotInfo = false;
                    MainWindow.GoAdWindow();
                }));
        }

        private void EnterApp()
        {
            MainWindow.KioskMgr.TurnOffAllGateStatus();

            //MainWindow.KioskMgr.InitCurrentChargeTransList();
            // database資料
            //MainWindow.KioskMgr.ScanSlotInfo(false);

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                    () =>
                    {
                        MainWindow.GoDiagnostic(MainWindow.IsENGMode);
                    }));
        }
    }
}
