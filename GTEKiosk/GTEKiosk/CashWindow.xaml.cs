﻿using GTEKiosk.Parameter;
using GTEKiosk.Trans;
using GTEKioskConfig.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEKiosk
{
    /// <summary>
    /// CashWindow.xaml 的互動邏輯
    /// </summary>
    public partial class CashWindow : Window
    {
        private DispatcherTimer mCountDownTimer { get; set; }
        private CashAction mCurrentAction { get; set; }
        private int mCountDownTime { get; set; }
        private bool mIsEndTrans { get; set; }

        private long mCardPoint { get; set; }

        public CashWindow()
        {
            InitializeComponent();
            
            mCountDownTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(1) };
            mCountDownTimer.Tick += CountDownTimer_Tick;
        }


        public void StartTrans(CashAction transAction)
        {
            mCurrentAction = transAction;
            countdownGrid.Visibility = System.Windows.Visibility.Visible;
            sandGlassImage.Visibility = System.Windows.Visibility.Visible;
            cancelTransButton.Visibility = System.Windows.Visibility.Visible;

            switch (transAction)
            {
                case CashAction.DetectedCard:
                    {
                        actionImage.Source = new BitmapImage(new Uri(@"res/image/DetectRFID.png", UriKind.Relative));
                        contentTextBox.Text = MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/PleasePlaceTheInductionKeyring").InnerText;

                        MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.CardDetected.ToString()));

                        mCountDownTimer.Stop();
                        mCountDownTime = 0;

                        countdownText.Text = KioskParameter.TRANSWAITINGTIME.ToString();

                        ThreadPool.QueueUserWorkItem(o => ScanRFIDCard());
                        mCountDownTimer.Start();

                        break;
                    }
                case CashAction.NoCardCancelTrans:
                    {
                        actionImage.Source = new BitmapImage(new Uri(@"res/image/ErrorCard.png", UriKind.Relative));
                        contentTextBox.Text = MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/YourCardCannotBeIdentify").InnerText;

                        MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.NoCard.ToString()));

                        mCountDownTimer.Stop();
                        mCountDownTime = 0;

                        countdownGrid.Visibility = System.Windows.Visibility.Hidden;
                        sandGlassImage.Visibility = System.Windows.Visibility.Hidden;
                        cancelTransButton.Visibility = System.Windows.Visibility.Hidden;

                        mCountDownTimer.Start();
                        break;
                    }
                case CashAction.QueryCash:
                    {
                        actionImage.Source = new BitmapImage(new Uri(@"res/image/CMoney.png", UriKind.Relative));
                        contentTextBox.Text = string.Format("{0} {1}", mCardPoint
                            , MainWindow.CulturesHelper.LanguageProvider.Document.SelectSingleNode("LangSettings/Point").InnerText);

                        MainWindow.PlayMedia(MainWindow.MediaMgr.GetLangAudioPath(MainWindow.CulturesHelper.CurrentCultureInfoName(), GTEKiosk.Mgr.AudioList.QueryCash.ToString()));

                        mCountDownTimer.Stop();
                        mCountDownTime = 0;

                        countdownGrid.Visibility = System.Windows.Visibility.Hidden;
                        sandGlassImage.Visibility = System.Windows.Visibility.Hidden;

                        mCountDownTimer.Start();
                        break;
                    }
            }
        }

        private void ScanRFIDCard()
        {
            RFIDCardInfo cardInfo = MainWindow.KioskMgr.ScanRFIDCard();

            CashAction action = CashAction.NoCardCancelTrans;
            if (cardInfo != null)
            {
                action = CashAction.QueryCash;
                mCardPoint = cardInfo.Point;
            }

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    StartTrans(action);
                }));
        }

        private void CountDownTimer_Tick(object sender, EventArgs e)
        {
            mCountDownTime++;
            switch (mCurrentAction)
            {
                case CashAction.DetectedCard:
                    {
                        if (mCountDownTime > KioskParameter.TRANSWAITINGTIME)
                        {
                            mCountDownTime = 0;
                            mCountDownTimer.Stop();
                            MainWindow.KioskMgr.IsRefreshRFID = false;
                        }
                        else
                        {
                            countdownText.Text = (KioskParameter.TRANSWAITINGTIME - mCountDownTime).ToString();
                        }

                        break;
                    }
                case CashAction.NoCardCancelTrans:
                    {
                        if (mCountDownTime > 5)
                        {
                            mCountDownTime = 0;
                            mCountDownTimer.Stop();
                            MainWindow.BackToHome(false);
                        }

                        break;
                    }
                case CashAction.QueryCash:
                    {
                        if (mCountDownTime > KioskParameter.TRANSLASTWAITINGTIME)
                        {
                            mCountDownTime = 0;
                            mCountDownTimer.Stop();
                            MainWindow.BackToHome(false);
                        }
                        else
                        {
                            countdownText.Text = (KioskParameter.TRANSLASTWAITINGTIME - mCountDownTime).ToString();
                        }
                        break;
                    }

            }

        }

        private void cancelTransButton_Click(object sender, RoutedEventArgs e)
        {
            switch (mCurrentAction)
            {
                case CashAction.DetectedCard:
                    {
                        mCountDownTime = 0;
                        mCountDownTimer.Stop();
                        MainWindow.KioskMgr.IsRefreshRFID = false;

                        break;
                    }
                default:
                    {
                        mCountDownTime = 0;
                        mCountDownTimer.Stop();
                        MainWindow.BackToHome(false);
                        break;
                    }
            }
        }
    }
}
