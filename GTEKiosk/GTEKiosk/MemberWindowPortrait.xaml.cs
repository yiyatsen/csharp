﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTEKiosk
{
    /// <summary>
    /// MemberWindowPortrait.xaml 的互動邏輯
    /// </summary>
    public partial class MemberWindowPortrait : Window
    {
        public MemberWindowPortrait()
        {
            InitializeComponent();

        }

        private void homeButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.BackToHome(false);
        }

        private void registerButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void cashButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.GoCash();
        }
    }
}
