﻿using GTE.Command;
using GTEBatteryLib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTETransfer
{
    public class AppSetting
    {
        public string[] BatteryTypeList { get; set; }

        public AppSetting()
        {
            BatteryTypeList = new string[] { "7253", "7238" };
        }
    }

    public class BatteryAtter
    {
        #region Property
        public string DeviceName { get; set; }
        public ulong CycleCount { get; set; }
        public string SN { get; set; }
        public string MCUVer { get; set; }
        public string ManufactureDate { get; set; }
        public string OZ2PN { get; set; }
        #endregion
    }
    public class CallbackInfo
    {
        public int Step { get; set; }
        public bool Status { get; set; }
        public object Data { get; set; }
    }
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        #region WIN 32
        [DllImport("user32.dll")]
        private extern static int SetWindowLong(IntPtr hwnd, int index, int value);
        [DllImport("user32.dll")]
        private extern static int GetWindowLong(IntPtr hwnd, int index);

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x00080000;

        void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            WindowInteropHelper wih = new WindowInteropHelper(this);
            int style = GetWindowLong(wih.Handle, GWL_STYLE);
            SetWindowLong(wih.Handle, GWL_STYLE, style & ~WS_SYSMENU);
        }
        #endregion

        private BatteryAPI mBattery { get; set; }
        public delegate void MessageHandler(object sender, CallbackInfo e);
        private DispatcherTimer mPortScanTimer; // 監聽Port狀態之定時器
        private bool mEnableAppClose; // 是否可以關閉視窗程式

        private BatteryAtter mResource { get; set; }
        private BatteryAtter mTarget { get; set; }

        private string mAppPath; //視窗程式之路徑
        private AppSetting mAppSetting; // 預設之連線設定及預設之電池屬性值

        private int mClickBarTime = 0;

        public MainWindow()
        {
            SourceInitialized += MainWindow_SourceInitialized;

            InitializeComponent();
            InitializeAppSetting();
            InitializeScanPort();
            mBattery = new BatteryAPI();
        }

        /// <summary>
        /// 初始化視窗程式需要之設定檔
        /// </summary>
        private void InitializeAppSetting()
        {
            mAppPath = System.AppDomain.CurrentDomain.BaseDirectory;

            string appSettingFilePath = mAppPath + "AppSetting.ap";
            try
            {
                if (File.Exists(appSettingFilePath))
                {
                    using (StreamReader file = File.OpenText(appSettingFilePath))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        mAppSetting = (AppSetting)serializer.Deserialize(file, typeof(AppSetting));
                    }
                }

                if (mAppSetting == null)
                {
                    mAppSetting = new AppSetting();

                    using (FileStream fs = File.Open(appSettingFilePath, FileMode.Create))
                    using (StreamWriter sw = new StreamWriter(fs))
                    using (JsonWriter jw = new JsonTextWriter(sw))
                    {
                        jw.Formatting = Formatting.Indented;

                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(jw, mAppSetting);
                    }
                }
            }
            catch { }


        }
        /// <summary>
        /// 初始化ComPort監聽時間器
        /// </summary>
        private void InitializeScanPort()
        {
            SetSerialPortToComboBox(rportList, true);
            SetSerialPortToComboBox(tportList, true);
            mPortScanTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(5) };
            mPortScanTimer.Tick += PortScanTimer_Tick;
            mPortScanTimer.Start();
        }
        /// <summary>
        /// 啟動監聽ComPort狀態之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PortScanTimer_Tick(object sender, EventArgs e)
        {
            SetSerialPortToComboBox(rportList, false);
            SetSerialPortToComboBox(tportList, false);
        }
        /// <summary>
        /// 設定ComPort列表至ComboBox
        /// </summary>
        private void SetSerialPortToComboBox(ComboBox portList, bool isFirstTime)
        {
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();
            if (ports != null && ports.Length > 0)
            {
                bool isChanged = false;
                foreach (object data in portList.Items)
                {
                    if (!ports.Contains(data.ToString()))
                    {
                        isChanged = true;
                    }
                }

                if (isChanged || isFirstTime)
                {
                    string port = null;
                    int index = 0;
                    if (portList.SelectedItem != null)
                    {
                        port = portList.SelectedItem.ToString();
                    }

                    portList.Items.Clear();
                    for (int i = 0; i < ports.Length; i++)
                    {
                        if (ports[i].Equals(port))
                        {
                            index = i;
                        }

                        portList.Items.Add(ports[i]);
                    }
                    portList.SelectedIndex = index;
                }
            }
        }

        private void transfer_Click(object sender, RoutedEventArgs e)
        {
            transferStateTitle.Content = "Transfer Battery";
            transferStateValue.Content = "Ready";

            if (mode != 2)
            {
                transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                if ((rportList.SelectedIndex == -1) || (tportList.SelectedIndex == -1))
                {
                    MessageBox.Show("Please select Source Port and Target Port!", "Alert");
                    return;
                }
                if (rportList.SelectedIndex == tportList.SelectedIndex)
                {
                    MessageBox.Show("Target Port must be not equal to Source Port!", "Alert");
                    return;
                }

                transferStateTitle.Content = "Check Source Battery";
                transferStateValue.Content = "---";

                mResource = new BatteryAtter();
                mTarget = new BatteryAtter();
                Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                rportList.IsEnabled = false;
                tportList.IsEnabled = false;
                transferButton.IsEnabled = false;
                closeButton.IsEnabled = false;
                barcodeText.IsEnabled = false;
                codeTextBox.IsEnabled = false;
                CallbackInfo callbackData = new CallbackInfo()
                {
                    Step = 1,
                    Status = false,
                    Data = mResource
                };
                string port = rportList.SelectedValue.ToString();
                ThreadPool.QueueUserWorkItem(o => CheckBattery(sender, port, callbackData));
            }
            else
            {
                if (isBardcodeSuccess)
                {
                    transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                    if ((tportList.SelectedIndex == -1))
                    {
                        MessageBox.Show("Please select Target Port!", "Alert");
                        return;
                    }

                    transferStateTitle.Content = "Check Target Battery";
                    transferStateValue.Content = "---";

                    mTarget = new BatteryAtter();
                    Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                    rportList.IsEnabled = false;
                    tportList.IsEnabled = false;
                    transferButton.IsEnabled = false;
                    closeButton.IsEnabled = false;
                    barcodeText.IsEnabled = false;
                    codeTextBox.IsEnabled = false;
                    CallbackInfo callbackData = new CallbackInfo()
                    {
                        Step = 2,
                        Status = false,
                        Data = mTarget
                    };
                    string port = tportList.SelectedValue.ToString();
                    ThreadPool.QueueUserWorkItem(o => CheckBattery(sender, port, callbackData));
                }
                else
                {
                    MessageBox.Show("Barcode format error", "Alert");
                    return;
                }
            }
        }
        /// <summary>
        /// 與電池連線電池
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="port"></param>
        /// <param name="baudRate"></param>
        private void CheckBattery(object sender, string port, CallbackInfo callbackData)
        {
            List<int> baudRateList = new List<int>();
            baudRateList.Add(38400);
            baudRateList.Add(115200);

            BatteryAtter data = (BatteryAtter)callbackData.Data;
            foreach (int baudRate in baudRateList)
            {
                bool isSuccess = mBattery.Connect(port, baudRate);
                if (isSuccess)
                {
                    isSuccess = mBattery.RefreshMCUEEP();
                    isSuccess = isSuccess && mBattery.RefreshPBEEP();
                    if (isSuccess)
                    {
                        object value = mBattery.SendBatteryCmd(BatteryCmd.CMD_DEVICE_NAME_GET);
                        if (value != null)
                        {
                            data.DeviceName = value.ToString();
                            value = mBattery.SendBatteryCmd(BatteryCmd.CMD_MCU_VERSION_GET);
                            if (value != null)
                            {
                                data.MCUVer = value.ToString();

                                value = mBattery.SendBatteryCmd(BatteryCmd.CMD_BARCODE_DATA_GET);
                                if (value != null)
                                {
                                    data.SN = value.ToString();
                                }
                                else
                                {
                                    value = mBattery.SendBatteryCmd(BatteryCmd.CMD_BATTERY_NO_GET);
                                    if (value != null)
                                    {
                                        data.SN = value.ToString();
                                    }
                                }
                            }
                        }
                        value = mBattery.GetPBEEPData(BatteryEEPAttrCmd.ProjectName, 1);
                        if (value != null)
                        {
                            data.OZ2PN = value.ToString();
                        }

                        value = mBattery.SendBatteryCmd(BatteryCmd.CMD_CYCLE_COUNT_GET);
                        if (value != null)
                        {
                            ulong numberValue = 0;
                            ulong.TryParse(value.ToString(), out numberValue);
                            data.CycleCount = numberValue;
                        }

                        if (data.SN != null)
                        {
                            callbackData.Status = true;
                        }
                    }
                    mBattery.Disconnect();
                    break;
                }
            }

            Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { sender, callbackData });
        }

        private void SetDeviceRTCTime(object sender, string port, CallbackInfo callbackData)
        {
            List<int> baudRateList = new List<int>();
            baudRateList.Add(38400);
            baudRateList.Add(115200);

            BatteryAtter data = (BatteryAtter)callbackData.Data;
            foreach (int baudRate in baudRateList)
            {
                bool isSuccess = mBattery.Connect(port, baudRate);
                if (isSuccess)
                {
                    DateTime dt = DateTime.UtcNow;
                    mBattery.SendBatteryCmd(BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET, Convert.ToByte(0x83));
                    mBattery.SendBatteryCmd(BatteryCmd.CMD_SET_SEC, Convert.ToByte(dt.Second));
                    mBattery.SendBatteryCmd(BatteryCmd.CMD_SET_MIN, Convert.ToByte(dt.Minute));
                    mBattery.SendBatteryCmd(BatteryCmd.CMD_SET_HOUR, Convert.ToByte(dt.Hour));
                    mBattery.SendBatteryCmd(BatteryCmd.CMD_SET_DAY, Convert.ToByte(dt.Day));
                    mBattery.SendBatteryCmd(BatteryCmd.CMD_SET_MONTH, Convert.ToByte(dt.Month));
                    mBattery.SendBatteryCmd(BatteryCmd.CMD_SET_YEAR, Convert.ToByte(dt.Year - 2000));
                    mBattery.SendBatteryCmd(BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET, Convert.ToByte(0x03));

                    System.Threading.Thread.Sleep(500);
                    int[] dt2 = new int[6];
                    object value = mBattery.SendBatteryCmd(BatteryCmd.CMD_RTC_REG_DUMP);
                    if (value != null)
                    {
                        dt2 = (int[])value;
                        try
                        {
                            DateTime getCurrentTime = DateTime.Parse(string.Format("{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}", dt2[0], dt2[1], dt2[2], dt2[3], dt2[4], dt2[5]));

                            long btDT = getCurrentTime.Ticks;
                            long gmtMaxDT = dt.AddMinutes(1).Ticks;
                            long gmtMinDT = dt.AddMinutes(-1).Ticks;

                            if (btDT < gmtMaxDT && btDT > gmtMinDT)
                            {
                                callbackData.Status = true;
                            }
                        }
                        catch
                        {
                        }
                    }

                    mBattery.Disconnect();
                    break;
                }
            }

            Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { sender, callbackData });
        }

        private void SetDeviceConfig(object sender, string port, string barcodeText, CallbackInfo callbackData)
        {
            if (barcodeText != null && barcodeText.Length == 10)
            {
                List<int> baudRateList = new List<int>();
                baudRateList.Add(38400);
                baudRateList.Add(115200);

                BatteryAtter data = (BatteryAtter)callbackData.Data;
                foreach (int baudRate in baudRateList)
                {
                    bool isSuccess = mBattery.Connect(port, baudRate);
                    if (isSuccess)
                    {
                        mBattery.SendBatteryCmd(BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET, Convert.ToByte(0x83));

                        int[] barCodeItem = new int[5];
                        barCodeItem[0] = int.Parse(barcodeText.Substring(0, 1)); // 生產商代碼
                        barCodeItem[1] = int.Parse(barcodeText.Substring(1, 1)); // 產品商代碼
                        barCodeItem[2] = int.Parse(barcodeText.Substring(2, 2)); // 製造年份
                        barCodeItem[3] = int.Parse(barcodeText.Substring(4, 2)); // 製造年週別
                        barCodeItem[4] = int.Parse(barcodeText.Substring(6, 4)); // 序號

                        isSuccess = mBattery.SetMCUEEPCmd(BatteryEEPAttrCmd.ManufactureInfo, Convert.ToByte((barCodeItem[0] << 4) | barCodeItem[1])); // Set Manufacturer & ProductClass

                        int year = 2000 + barCodeItem[2]; //barcode只有兩千年後兩碼
                        isSuccess = mBattery.SetMCUEEPCmd(BatteryEEPAttrCmd.ManufactureDate, ISO8601WeekNumToThursday(year, barCodeItem[3])) && isSuccess;// Set Manufacture Date

                        isSuccess = mBattery.SetMCUEEPCmd(BatteryEEPAttrCmd.ManufactureWeekOfYear, Convert.ToByte((barCodeItem[3]))) && isSuccess; // Set Manufacture Week Of Year

                        isSuccess = mBattery.SetMCUEEPCmd(BatteryEEPAttrCmd.SerialNumber, barCodeItem[4]) && isSuccess; // Set Serial Number

                        mBattery.SendBatteryCmd(BatteryCmd.CMD_MCUEEP_SET, Convert.ToByte(Convert.ToInt32("C", 16)), Convert.ToByte(Convert.ToInt32("27", 16)), 1);
                        mBattery.SendBatteryCmd(BatteryCmd.CMD_MCUEEP_SET, Convert.ToByte(Convert.ToInt32("D", 16)), Convert.ToByte(Convert.ToInt32("10", 16)), 1);

                        mBattery.SendBatteryCmd(BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET, Convert.ToByte(0x03));

                        mBattery.Disconnect();
                        System.Threading.Thread.Sleep(500);
                        callbackData.Status = true;
                        string an1310 = System.AppDomain.CurrentDomain.BaseDirectory + "Tools\\cl.exe";
                        System.Diagnostics.Process process = new System.Diagnostics.Process();
                        process.StartInfo.FileName = an1310;
                        process.StartInfo.Arguments = string.Format(" -d {0} -r", port);
                        process.StartInfo.UseShellExecute = false;
                        process.StartInfo.CreateNoWindow = true;
                        process.EnableRaisingEvents = true;
                        process.Exited += process_Exited;
                        process.Start();
                        break;
                    }
                }
            }
            if (!callbackData.Status)
            {
                Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { sender, callbackData });
            }
        }
        /// <summary>
        /// 轉換至ISO8601標準的週別日期時間
        /// </summary>
        /// <param name="year"></param>
        /// <param name="weekOfYear"></param>
        /// <returns></returns>
        public int[] ISO8601WeekNumToThursday(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1); //當年分第一天
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;//移動至星期三

            DateTime firstThursday = jan1.AddDays(daysOffset);//當周星期四時間
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            DateTime result = firstThursday.AddDays(weekNum * 7);
            int[] date = new int[3];
            date[0] = result.Year;
            date[1] = result.Month;
            date[2] = result.Day;
            return date;
        }

         /// <summary>
        /// 使用Thread回傳訊息之事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MessageEvent(object sender, CallbackInfo e)
        {
            switch(e.Step){
                case 1:
                    {
                        if (e.Status)
                        {
                            transferStateTitle.Content = "Check Target Battery";
                            transferStateValue.Content = "---";
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                            e.Step = 2;
                            e.Status = false;
                            e.Data = mTarget;
                            string port = tportList.SelectedValue.ToString();
                            ThreadPool.QueueUserWorkItem(o => CheckBattery(sender, port, e));
                        }
                        else
                        {
                            transferStateValue.Content = "Load Fail";
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                            rportList.IsEnabled = true;
                            tportList.IsEnabled = true;
                            transferButton.IsEnabled = true;
                            closeButton.IsEnabled = true;
                            barcodeText.IsEnabled = true;
                            codeTextBox.IsEnabled = true;
                        }
                        break;
                    }
                case 2:
                    {
                        if (e.Status)
                        {
                            int ret = 2;
                            bool isMatch = false;
                            Regex regex = new Regex("BZRMA");
                            isMatch = regex.IsMatch(mTarget.OZ2PN);
                            if (isMatch)
                            {
                                ret = 1;
                                for (int i = 0; i < mAppSetting.BatteryTypeList.Count(); i++)
                                {
                                    regex = new Regex(mAppSetting.BatteryTypeList[i]);
                                    if (mode != 2)
                                    {
                                        isMatch = regex.IsMatch(mResource.DeviceName) && regex.IsMatch(mTarget.DeviceName);
                                        if (isMatch)
                                        {
                                            ret = 0;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        isMatch = regex.IsMatch(mTarget.DeviceName);
                                        if (isMatch)
                                        {
                                            ret = 0;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (ret == 0)
                            {
                                // set Target rtc
                                transferStateTitle.Content = "Set Target RTC";
                                transferStateValue.Content = "---";
                                transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                                e.Step = 3;
                                e.Status = false;
                                e.Data = mTarget;
                                string port = tportList.SelectedValue.ToString();
                                ThreadPool.QueueUserWorkItem(o => SetDeviceRTCTime(sender, port, e));
                            }
                            else if (ret == 1)
                            {
                                transferStateTitle.Content = "Check Battery";
                                transferStateValue.Content = "Verify Fail";
                                transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                                Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                                rportList.IsEnabled = true;
                                tportList.IsEnabled = true;
                                transferButton.IsEnabled = true;
                                closeButton.IsEnabled = true;
                                barcodeText.IsEnabled = true;
                                codeTextBox.IsEnabled = true;
                            }
                            else if (ret == 2)
                            {
                                transferStateTitle.Content = "Target Board";
                                transferStateValue.Content = "Verify Error";
                                transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                                Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                                rportList.IsEnabled = true;
                                tportList.IsEnabled = true;
                                transferButton.IsEnabled = true;
                                closeButton.IsEnabled = true;
                                barcodeText.IsEnabled = true;
                                codeTextBox.IsEnabled = true;
                            }
                        }
                        else
                        {
                            transferStateValue.Content = "Load Fail";
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                            rportList.IsEnabled = true;
                            tportList.IsEnabled = true;
                            transferButton.IsEnabled = true;
                            closeButton.IsEnabled = true;
                            barcodeText.IsEnabled = true;
                            codeTextBox.IsEnabled = true;
                        }
                        break;
                    }
                case 3:
                    {          
                        if (e.Status)
                        {
                            if (mode != 2)
                            {
                                transferStateTitle.Content = "Transfer Battery";
                                transferStateValue.Content = "Please Wait";
                                transferStateValue.Foreground = new SolidColorBrush(Colors.Black);

                                string an1310 = System.AppDomain.CurrentDomain.BaseDirectory + "Tools\\cl.exe";
                                System.Diagnostics.Process process = new System.Diagnostics.Process();
                                process.StartInfo.FileName = an1310;
                                process.StartInfo.Arguments = string.Format(" -i {0} -d {1} -p -m -r", rportList.SelectedValue.ToString(), tportList.SelectedValue.ToString());
                                process.StartInfo.UseShellExecute = false;
                                process.StartInfo.CreateNoWindow = true;
                                process.EnableRaisingEvents = true;
                                process.Exited += process_Exited;
                                process.Start();
                            }
                            else
                            {
                                transferStateTitle.Content = "Set Target Config";
                                transferStateValue.Content = "---";
                                transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                                e.Step = 4;
                                e.Status = false;
                                e.Data = mTarget;
                                string port = tportList.SelectedValue.ToString();
                                string barcode = barcodeText.Text;
                                ThreadPool.QueueUserWorkItem(o => SetDeviceConfig(sender, port, barcode, e));
                            }
                        }
                        else
                        {
                            transferStateValue.Content = "RTC Setting Fail";
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                            rportList.IsEnabled = true;
                            tportList.IsEnabled = true;
                            transferButton.IsEnabled = true;
                            closeButton.IsEnabled = true;
                            barcodeText.IsEnabled = true;
                            codeTextBox.IsEnabled = true;
                        }
                        break;
                    }
                case 4:
                    {
                        transferStateTitle.Content = "Reload Target Battery";
                        mTarget = new BatteryAtter();
                        e.Step = 5;
                        e.Status = false;
                        e.Data = mTarget;
                        string port = tportList.SelectedValue.ToString();
                        ThreadPool.QueueUserWorkItem(o => CheckBattery(null, port, e));

                        break;
                    }
                case 5:
                    {                
                        if (e.Status)
                        {
                            transferStateTitle.Content = "Transfer Battery";
                            if (mode != 2)
                            {
                                if (mResource.DeviceName == mTarget.DeviceName && mResource.SN == mTarget.SN && mResource.MCUVer == mTarget.MCUVer)
                                {
                                    transferStateValue.Content = "Success";
                                    transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                                }
                                else
                                {
                                    transferStateValue.Content = "Fail";
                                    transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                                }
                            }
                            else
                            {
                                string barcode = barcodeText.Text;
                                if (mTarget.SN == barcode && mTarget.CycleCount == 10000)
                                {
                                    transferStateValue.Content = "Success";
                                    transferStateValue.Foreground = new SolidColorBrush(Colors.Black);
                                }
                                else
                                {
                                    transferStateValue.Content = "Fail";
                                    transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                                }
                            }


                            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                            rportList.IsEnabled = true;
                            tportList.IsEnabled = true;
                            transferButton.IsEnabled = true;
                            closeButton.IsEnabled = true;
                            barcodeText.IsEnabled = true;
                            codeTextBox.IsEnabled = true;
                        }
                        else
                        {
                            transferStateValue.Content = "Load Fail";
                            transferStateValue.Foreground = new SolidColorBrush(Colors.Red);
                            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
                            rportList.IsEnabled = true;
                            tportList.IsEnabled = true;
                            transferButton.IsEnabled = true;
                            closeButton.IsEnabled = true;
                            barcodeText.IsEnabled = true;
                            codeTextBox.IsEnabled = true;
                        }
                        break;
                    }
            }
        }

        void process_Exited(object sender, EventArgs e)
        {
            Thread.Sleep(3000);
            CallbackInfo callbackData = new CallbackInfo()
            {
                Step = 4,
                Status = true,
                Data = null
            };
            Application.Current.Dispatcher.Invoke(new MessageHandler(MessageEvent), new object[] { sender, callbackData });
        }

        private void CloseAP_Click(object sender, RoutedEventArgs e)
        {
            mEnableAppClose = true;
            this.Close();
        }
        /// <summary>
        /// 視窗準備關閉之事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (!mEnableAppClose) // 決定可否關閉視窗之條件
            {
                e.Cancel = true;
            }
        }

        private void StatusBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            mClickBarTime++;
            if (mClickBarTime == 10)
            {
                mClickBarTime = 0;
                codeTextBox.Visibility = System.Windows.Visibility.Visible;
            }
        }

        Regex reg = new Regex("gte.QA");
        Regex reg2 = new Regex("bzrma");
        int mode = 0;
        bool isBardcodeSuccess = false;
        private void codeTextBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
           
            if (mode == 1)
            {
                if (isBardcodeSuccess)
                {
                    bool isMatch = reg2.IsMatch(codeTextBox.Password);
                    if (isMatch)
                    {
                        mode = 2;
                        modeValue.Content = "QA";
                    }
                }
                else
                {
                    mode = 0;
                    mClickBarTime = 0;
                    barcodeText.Visibility = System.Windows.Visibility.Collapsed;
                    codeTextBox.Visibility = System.Windows.Visibility.Collapsed;
                    modeValue.Content = "Transfer";
                }
            }
            else if (mode == 2)
            {
                mode = 0;
                barcodeText.Visibility = System.Windows.Visibility.Collapsed;
                barcodeText.Text = "";
                codeTextBox.Password = "";
                modeValue.Content = "Transfer";
            }
            else
            {
                bool isMatch = reg.IsMatch(codeTextBox.Password);
                if (isMatch)
                {
                    barcodeText.Visibility = System.Windows.Visibility.Visible;
                    mode = 1;
                }
                else
                {
                    barcodeText.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        private void BarcodeTextChanged(object sender, TextChangedEventArgs e)
        {
            isBardcodeSuccess = false;
            TextBox textBox = (TextBox)sender;
            if (textBox.Text.Length > 10)
            {
                textBox.Text = textBox.Text.Substring(0, 10);
                textBox.SelectionStart = textBox.Text.Length;
            }
            else
            {
                Regex regex = new Regex(@"^\d{10}$");
                isBardcodeSuccess = regex.IsMatch(textBox.Text);
            }
        }
    }
}
