﻿using GTAUOUtility.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using static GTAUOCore.AUOBatCore;

namespace GTAUOUtility.ViewModel
{
    public class UtilityRunDialogViewModel : INotifyPropertyChanged
    {
        public ICommand ConfirmCommand { get { return new MainWindowCommand(Confirm); } }
        public ICommand CancelCommand { get { return new MainWindowCommand(Canecl); } }

        private string[] mPortList = System.IO.Ports.SerialPort.GetPortNames();
        private BatConfig mBatConfig { get; set; }
        private ObservableCollection<string> mCOMPortList = new ObservableCollection<string>();
        private ObservableCollection<int> mAppBaudRateList = new ObservableCollection<int>();
        private string mPath = "";
        private int mPort = 115200;
        private string mErrorMessage = "";

        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<string> COMPortList
        {
            get { return mCOMPortList; }
            set { mCOMPortList = value; NotifyPropertyChanged("COMPortList"); }
        }
        public ObservableCollection<int> AppBaudRateList
        {
            get { return mAppBaudRateList; }
            set { mAppBaudRateList = value; NotifyPropertyChanged("AppBaudRateList"); }
        }
        public string Path
        {
            get { return mPath; }
            set { mPath = value; NotifyPropertyChanged("Path"); }
        }
        public int Port
        {
            get { return mPort; }
            set { mPort = value; NotifyPropertyChanged("Port"); }
        }
        public string ErrorMessage
        {
            get { return mErrorMessage; }
            set { mErrorMessage = value; NotifyPropertyChanged("ErrorMessage"); }
        }
        public System.Windows.Visibility EnableSerialPortVisibility
        {
            get { return (mBatConfig != null && mBatConfig.Comm == CommMode.SerialPort) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableSerialPortVisibility"); }
        }
        public System.Windows.Visibility EnableSNMPVisibility
        {
            get { return (mBatConfig != null && mBatConfig.Comm != CommMode.SerialPort) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set { NotifyPropertyChanged("EnableSNMPVisibility"); }
        }

        public UtilityRunDialogViewModel(BatConfig config)
        {
            mBatConfig = config;
            if (mBatConfig.Comm == CommMode.SerialPort)
            {
                SetCOMPortList();

                mAppBaudRateList.Add(115200);
                AppBaudRateList = mAppBaudRateList;
            }
            else
            {
                Path = "";
                Port = 0;
            }

            NotifyPropertyChanged("EnableSerialPortVisibility");
            NotifyPropertyChanged("EnableIPAddressVisibility");
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void NotifyPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged != null) foreach (string propertyName in propertyNames) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SetCOMPortList()
        {
            mCOMPortList.Clear();

            string matchTargetStr = "";
            foreach (string str in mPortList)
            {
                mCOMPortList.Add(str);
                Path = str;
                if (mBatConfig.Path == str) matchTargetStr = str;
            }

            if (matchTargetStr != "") Path = matchTargetStr;
            COMPortList = mCOMPortList;
        }

        private void Confirm(object obj)
        {
            if (Path == "" || Port == 0)
            {
                ErrorMessage = "Target cannot empty";
                return;
            }

            if (mBatConfig.Comm != CommMode.SerialPort)
            {
                IPAddress address;
                if (!IPAddress.TryParse(Path, out address))
                {
                    ErrorMessage = "IP Format Error";
                    return;
                    //Valid IP, with address containing the IP
                }
            }

            mBatConfig.Path = Path;
            mBatConfig.Port = Port;
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(mBatConfig, null);
        }

        private void Canecl(object obj)
        {
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(null, null);
        }
    }
}
