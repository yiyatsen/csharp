﻿using GTAUOCore;
using GTAUOUtility.Model;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using static GTAUOCore.AUOBatCore;
using static GTAUOCore.AUOBatFormat;

namespace GTAUOUtility.ViewModel
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        public enum PCCmdStep : byte
        {
            CheckParam = 0x00,
            CheckInfo = 0x01,
            CheckAlarm = 0x02
        }

        public ICommand ExcuteUtilityDialogCommand { get { return new MainWindowCommand(ExcuteUtilityDialog); } }
        public ICommand StopUtilityDialogCommand { get { return new MainWindowCommand(StopUtilityDialog); } }
        public ICommand ExcuteUtilitySettingDialogCommand { get { return new MainWindowCommand(ExcuteUtilitySettingDialog); } }
        public ICommand ExitAppCommand { get { return new MainWindowCommand(ExitApp); } }

        public ICommand BATIDSettingCommand { get { return new MainWindowCommand(BATIDSetting); } }
        public ICommand ParamSettingCommand { get { return new MainWindowCommand(ParamSetting); } }
        public ICommand Serial1SettingCommand { get { return new MainWindowCommand(Serial1Setting); } }
        public ICommand Serial2SettingCommand { get { return new MainWindowCommand(Serial2Setting); } }

        public event PropertyChangedEventHandler PropertyChanged;

        private PCCmdStep mPCCmdStep { get; set; }
        private CancellationTokenSource mCTS { get; set; }
        private CancellationTokenSource mCTSLog { get; set; }
        private bool ISBETA = true;
        private string mAppVersion { get; set; }
        private AUOBatCore mAUOBatCore = new AUOBatCore();
        private BatConfig mBatConfig = new BatConfig();

        private ObservableCollection<ParamConfig> mSettingList = new ObservableCollection<ParamConfig>();
        private ObservableCollection<ParamConfig> mParamList = new ObservableCollection<ParamConfig>();
        private ObservableCollection<ParamConfig> mInfo1List = new ObservableCollection<ParamConfig>();
        private ObservableCollection<ParamConfig> mInfo2List = new ObservableCollection<ParamConfig>();

        public string AppVersion
        {
            get { return mAppVersion; }
            set
            {
                mAppVersion = value;
                NotifyPropertyChanged("AppVersion");
            }
        }

        public System.Windows.Visibility ConnectionVisibility
        {
            get { return (mAUOBatCore != null && mAUOBatCore.IsOpen) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set
            {
                NotifyPropertyChanged("ConnectionVisibility");
            }
        }

        public System.Windows.Visibility DisconnectionVisibility
        {
            get { return (mAUOBatCore != null &&  !mAUOBatCore.IsOpen) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set
            {
                NotifyPropertyChanged("DisconnectionVisibility");
            }
        }

        public string PathAndPort
        {
            get { return string.Format("Path({0})", mAUOBatCore.PathAndPort); }
            set
            {
                NotifyPropertyChanged("PathAndPort");
                NotifyPropertyChanged("ConnectionVisibility");
                NotifyPropertyChanged("DisconnectionVisibility");
            }
        }

        public string EmulatorMode
        {
            get { return mBatConfig.Role.ToString(); }
            set
            {
                NotifyPropertyChanged("EmulatorMode");
            }
        }

        public string DataMode
        {
            get { return string.Format("Data Mode: {0}", mBatConfig.Display.ToString()); }
            set
            {
                NotifyPropertyChanged("DataMode");
            }
        }

        public bool IsPCMode
        {
            get { return (mBatConfig.Role == AUORole.PCEmulator); }
            set
            {
                NotifyPropertyChanged("IsPCMode");
            }
        }

        public System.Windows.Visibility BATEmulatorVisibility
        {
            get { return (mBatConfig.Role == AUORole.BATEmulator) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
            set
            {
                NotifyPropertyChanged("BATEmulatorVisibility");
            }
        }

        private string mBATID = "";
        
        public string BATID
        {
            get { return string.Format("{0}", mAUOBatCore.BatID); }
            set
            {
                mBATID = value;
                NotifyPropertyChanged("BATID");
            }
        }
        public ObservableCollection<ParamConfig> SettingList
        {
            get { return mSettingList; }
            set { NotifyPropertyChanged("SettingList"); }
        }
        public ObservableCollection<ParamConfig> ParamList
        {
            get { return mParamList; }
            set { NotifyPropertyChanged("ParamList"); }
        }
        public ObservableCollection<ParamConfig> BaInfo1List
        {
            get { return mInfo1List; }
            set { NotifyPropertyChanged("BaInfo1List"); }
        }
        public ObservableCollection<ParamConfig> BaInfo2List
        {
            get { return mInfo2List; }
            set { NotifyPropertyChanged("BaInfo2List"); }
        }
 
        public MainWindowViewModel()
        {
            mBatConfig.Role = AUORole.PCEmulator;

            RefreshConfig();
            AppVersion = string.Format("Ver: {0}{1}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(), (ISBETA) ? "(BETA)" : "");
            PathAndPort = "";
            if (!Directory.Exists(AUOBatCore.SYS_LOG_PATH)) Directory.CreateDirectory(AUOBatCore.SYS_LOG_PATH);
            if (!Directory.Exists(AUOBatCore.LOG_PATH)) Directory.CreateDirectory(AUOBatCore.LOG_PATH);

            mAUOBatCore.Init(0x00);
            RefreshData();


            mAUOBatCore.RefreshUIEvent += MAUOBatCore_RefreshUIEvent;

            // 測試

            //string logFile = System.AppDomain.CurrentDomain.BaseDirectory + @".\20180508log.txt";
            //string parser = System.AppDomain.CurrentDomain.BaseDirectory + @".\parser.txt";
            //if (File.Exists(logFile))
            //{
            //    Regex regT = new Regex(@"^\d{4}/\d{2}/\d{2}\s\d{2}:\d{2}:\d{2}\[T\]");
            //    Regex regR = new Regex(@"^\d{4}/\d{2}/\d{2}\s\d{2}:\d{2}:\d{2}\[R\]");
            //    string tTimeStr = "";
            //    string tStr = "";
            //    string rTimeStr = "";
            //    string rStr = "";
            //    string parserData = "";
            //    List<byte> data = new List<byte>();
            //    using (StreamReader sr = File.OpenText(logFile))
            //    {
            //        string s = String.Empty;
            //        while ((s = sr.ReadLine()) != null)
            //        {
            //            if (regT.IsMatch(s))
            //            {
            //                if (rStr != "")
            //                {
            //                    string[] rSplit = rStr.Trim().Split(' ');
            //                    for (int i = 0; i < rSplit.Length; i++)
            //                    {
            //                        data.Add((byte)Convert.ToInt32(rSplit[i], 16));
            //                    }
            //                    // 資料轉換
            //                    Dictionary<string, object> raw = new Dictionary<string, object>();
            //                    Dictionary<string, object> real = new Dictionary<string, object>();
            //                    try
            //                    {
            //                        List<ParserFormat> ddd = ParserSerialData(data);
            //                        if (ddd.Count > 1)
            //                        {

            //                        }
            //                        AUOBatCore.UnpackBatInfoRaw(ddd[0], raw);
            //                        AUOBatCore.UnpackBatInfo(raw, real);
            //                        foreach (string key in real.Keys)
            //                        {
            //                            parserData += real[key].ToString() + "\t";
            //                        }
            //                    }
            //                    catch (Exception err)
            //                    {
            //                        parserData = err.Message;
            //                    }
            //                    data.Clear();
            //                }
            //                if (tStr != "")
            //                {
            //                    string txt = string.Format("{0}\t{1}\r\n", tTimeStr, tStr);
            //                    if (rStr != "")
            //                    {
            //                        txt += string.Format("{0}\t{1}\r\n", rTimeStr, rStr);
            //                        if (parserData != "")
            //                        {
            //                            txt += string.Format("{0}_Parser\t{1}\r\n", rTimeStr, parserData);
            //                        }
            //                    }
            //                    File.AppendAllText(parser, txt);
            //                    txt = "";
            //                }
            //                rStr = "";
            //                rTimeStr = "";
            //                parserData = "";
            //                Match match = regT.Match(s);
            //                tTimeStr = match.Value;
            //                tStr = Regex.Replace(s, @"^\d{4}/\d{2}/\d{2}\s\d{2}:\d{2}:\d{2}\[T\]", "").Trim();
            //            }
            //            else if (regR.IsMatch(s))
            //            {
            //                Match match = regR.Match(s);
            //                rTimeStr = match.Value;
            //                rStr += Regex.Replace(s, @"^\d{4}/\d{2}/\d{2}\s\d{2}:\d{2}:\d{2}\[R\]", "").Trim() + " ";
            //            }
            //        }
            //    }
            //}
        }

        private void MAUOBatCore_RefreshUIEvent()
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(
                () =>
                {
                    RefreshData();
                }));
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void RefreshConfig()
        {
            NotifyPropertyChanged("IsPCMode");
            NotifyPropertyChanged("EmulatorMode");
            NotifyPropertyChanged("DataMode");
            NotifyPropertyChanged("BATEmulatorVisibility");
        }

        private void RefreshSetting()
        {
            RefreshData("SettingList", mAUOBatCore.ParamRaw, mSettingList, DisplayMode.Normal);
        }

        private void RefreshData()
        {
            NotifyPropertyChanged("BATID");
            if (mBatConfig.Display == DisplayMode.Raw)
            {
                RefreshData("ParamList", mAUOBatCore.ParamRaw, mParamList, mBatConfig.Display);
                RefreshData("BaInfo1List", mAUOBatCore.Info1Raw, mInfo1List, mBatConfig.Display);
                RefreshData("BaInfo2List", mAUOBatCore.Info2Raw, mInfo2List, mBatConfig.Display);
            }
            else
            {
                RefreshData("ParamList", mAUOBatCore.Param, mParamList, mBatConfig.Display);
                RefreshData("BaInfo1List", mAUOBatCore.Info1, mInfo1List, mBatConfig.Display);
                RefreshData("BaInfo2List", mAUOBatCore.Info2, mInfo2List, mBatConfig.Display);
            }
        }

        private void ResetData(Dictionary<string, object> data, ObservableCollection<ParamConfig> config, DisplayMode display)
        {
            foreach (var value in config)
            {
                object tmp = Convert.ToInt32(value.Value, 16);
                if (display == DisplayMode.Raw)
                {
                    switch (value.Type)
                    {
                        case "System.Byte":
                            {
                                tmp = Convert.ToByte(value.Value, 16);
                                break;
                            }
                        case "System.UInt16":
                            {
                                tmp = Convert.ToUInt16(value.Value, 16);
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
                else
                {
                }
                AUOBatCore.SetDataTable(data, value.Name, tmp);
            }
        }

        private void RefreshData(string attr, Dictionary<string, object> data, ObservableCollection<ParamConfig> config, DisplayMode display)
        {
            config.Clear();

            foreach (var value in data)
            {
                string tmpValueStr = value.Value.ToString();
                string type = value.Value.GetType().FullName;
                if (display == DisplayMode.Raw)
                {
                    switch (type)
                    {
                        case "System.Byte":
                            {
                                tmpValueStr = ((byte)value.Value).ToString("X2");
                                break;
                            }
                        case "System.UInt16":
                            {
                                tmpValueStr = string.Format("{0:0000}", ((ushort)value.Value).ToString("X4"));
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
                config.Add(new ParamConfig() { Name = value.Key, Type = type, Value = tmpValueStr });
            }

            NotifyPropertyChanged(attr);
        }

        private void StopUtility()
        {
            if (mCTS != null) mCTS.Cancel();
            if (mCTSLog != null) mCTSLog.Cancel();
            if (mAUOBatCore != null) mAUOBatCore.Disconnect();
            PathAndPort = "";
        }

        public async Task RecordData(CancellationToken cancellationToken)
        {
            DateTime RecordDT = DateTime.UtcNow;
            string file = string.Format("{0}{1}_{2}.txt", AUOBatCore.LOG_PATH, mAUOBatCore.mAUORole.ToString(), RecordDT.ToString("yyyyMMddHH"));
            List<byte> dataList = new List<byte>();
            while (true)
            {
                DateTime dtStart = DateTime.UtcNow;
                try
                {
                    int count = AUOBatCore.LogData.Count;
                    if (AUOBatCore.LogData.Count > 0)
                    {
                        string txt = "";
                        for (int i = 0; i < count; i++)
                        {
                            txt += AUOBatCore.LogData[i];
                        }
                        AUOBatCore.LogData.RemoveRange(0, count);
                        File.AppendAllText(file, txt);
                    }
                }
                catch (Exception err)
                {
                    string sysfile = string.Format("{0}{1}.txt", AUOBatCore.SYS_LOG_PATH, dtStart.ToString("yyyyMMdd"));
                    File.AppendAllText(sysfile, string.Format("{0}\t{1}\t{2}\r\n", dtStart.ToString("yyyyMMddHHmmss"), "Parser Error", err.Message));
                }

                try
                {
                    if ((dtStart.Ticks - RecordDT.Ticks) > 6000000000)  // 超過一個小時
                    {
                        RecordDT = DateTime.UtcNow;
                        file = string.Format("{0}{1}_{2}.txt", AUOBatCore.LOG_PATH, mAUOBatCore.mAUORole.ToString(), RecordDT.ToString("yyyyMMddHH"));
                    }
                }
                catch (Exception err)
                {
                    string sysfile = string.Format("{0}{1}.txt", AUOBatCore.SYS_LOG_PATH, dtStart.ToString("yyyyMMdd"));
                    File.AppendAllText(sysfile, string.Format("{0}\t{1}\t{2}\r\n", dtStart.ToString("yyyyMMddHHmmss"), "Task Error", err.Message));
                }
                await Task.Delay(3000, cancellationToken);
            }
        }

        public async Task PCEmulatorFlow(CancellationToken cancellationToken)
        {
            mPCCmdStep = PCCmdStep.CheckParam;
            List<byte> param = new List<byte>();

            while (true)
            {
                switch (mPCCmdStep)
                {
                    case PCCmdStep.CheckInfo:
                        {
                            bool ret = true;
                            for (int i = 0; i < (byte)mAUOBatCore.ParamRaw["GaugeCount"]; i++)
                            {
                                for (int j = 0; j < (byte)mAUOBatCore.ParamRaw["SeriesCount"]; j++)
                                {
                                    param.Clear();
                                    param.Add((byte)(i + 1));
                                    param.Add((byte)(j + 1));
                                    bool tmp = await mAUOBatCore.PCEmulatorFlow(BatCmd.GetBatInfo, param);
                                    ret = ret && tmp;
                                    if (!ret) break;
                                }
                                if (!ret) break;
                            }
                            if (ret)
                            {
                                mPCCmdStep = PCCmdStep.CheckAlarm;
                            }
                            break;
                        }
                    case PCCmdStep.CheckAlarm:
                        {
                            bool ret = await mAUOBatCore.PCEmulatorFlow(BatCmd.GetBatAlarmStatus, param);
                            if (ret)
                            {
                                RefreshData();
                                mPCCmdStep = PCCmdStep.CheckInfo;
                            }
                            break;
                        }
                    default:
                        {
                            bool ret = await mAUOBatCore.PCEmulatorFlow(BatCmd.GetBatParam, param);
                            if (ret)
                            {
                                mPCCmdStep = PCCmdStep.CheckInfo;
                            }
                            break;
                        }
                }

                await Task.Delay(500, cancellationToken);
            }
        }

        private void BATIDSetting(object obj)
        {
            try
            {
                mAUOBatCore.BatID = Convert.ToByte(mBATID, 10);
            }
            catch (Exception err)
            {

            }
            NotifyPropertyChanged("BATID");
        }
 
        private void ParamSetting(object obj)
        {
            ResetData(mAUOBatCore.ParamRaw, mParamList, mBatConfig.Display);
        }
 
        private void Serial1Setting(object obj)
        {
            ResetData(mAUOBatCore.Info1Raw, mInfo1List, mBatConfig.Display);
        }
 
        private void Serial2Setting(object obj)
        {
            ResetData(mAUOBatCore.Info2Raw, mInfo2List, mBatConfig.Display);
        }

        private async void ExcuteUtilityDialog(object obj)
        {
            await DialogHost.Show(new UtilityRunDialog()
            {
                DataContext = new UtilityRunDialogViewModel(mBatConfig)
            }, "RootDialog", async (object sender, DialogClosingEventArgs eventArgs) =>
            {
                if (eventArgs.Parameter == null) return;

                mBatConfig = (BatConfig)eventArgs.Parameter;

                switch (mBatConfig.Comm)
                {
                    case CommMode.SerialPort:
                        {
                            mAUOBatCore.SetCommInterface(new CommSerialPort());
                            break;
                        }
                    case CommMode.UDP:
                        {
                            mAUOBatCore.SetCommInterface(new CommUDP());
                            break;
                        }
                    //case CommMode.TCP:
                    //    {
                    //        mAUOBatCore.SetCommInterface(new CommTCP());
                    //        break;
                    //    }
                    default:
                        {
                            break;
                        }
                }

                bool isConnect = mAUOBatCore.Connect(mBatConfig);
                PathAndPort = "";
                if (isConnect)
                {
                    mCTS = new CancellationTokenSource();
                    mCTSLog = new CancellationTokenSource();
                    if (mBatConfig.Role == AUORole.PCEmulator)
                    {
                        PCEmulatorFlow(mCTS.Token);
                    }
                    else if (mBatConfig.Role == AUORole.Unknow)
                    {
                        mAUOBatCore.Init(0x00);
                        mAUOBatCore.BatGaugeNum = AUOBatCore.GaugeNum;
                        mAUOBatCore.BatSerialNum = AUOBatCore.SerialNum;
                        mAUOBatCore.BatGauge = 1;
                        mAUOBatCore.BatSerial = 1;

                        RefreshData();
                    }
                    RecordData(mCTSLog.Token);
                }
                else
                {
                    await DialogHost.Show(new NotifyDialog()
                    {
                        DataContext = new NotifyDialogViewModel
                        {
                            EnableYesNoQuestion = false,
                            NotifyTitle = "Warning",
                            NotifyMessage = "Please check Path and Port!"
                        }
                    }, "NotifyDialog");
                }
            });
        }

        private async void StopUtilityDialog(object obj)
        {
            await DialogHost.Show(new NotifyDialog()
            {
                DataContext = new NotifyDialogViewModel()
                {
                    EnableYesNoQuestion = true,
                    NotifyTitle = "Warning",
                    NotifyMessage = "Do you want to cancel the task?"
                }
            }, "RootDialog", (object sender, DialogClosingEventArgs eventArgs) =>
            {
                if ((bool)eventArgs.Parameter == false) return;

                StopUtility();
            });
        }

        private async void ExcuteUtilitySettingDialog(object obj)
        {
            await DialogHost.Show(new UtilitySettingDialog()
            {
                DataContext = new UtilitySettingDialogViewModel(mBatConfig)
            }, "RootDialog", (object sender, DialogClosingEventArgs eventArgs) =>
            {
                if (eventArgs.Parameter == null) return;

                mBatConfig = (BatConfig)eventArgs.Parameter;
                RefreshConfig();
                RefreshData();
            });
        }

        private async void ExitApp(object obj)
        {
            await DialogHost.Show(new NotifyDialog()
            {
                DataContext = new NotifyDialogViewModel()
                {
                    EnableYesNoQuestion = true,
                    NotifyTitle = "Warning",
                    NotifyMessage = "Do you want to leave now?"
                }
            }, "RootDialog", (object sender, DialogClosingEventArgs eventArgs) =>
            {
                if ((bool)eventArgs.Parameter == false) return;

                StopUtility();
                MainWindow.ExitAPP();
            });
        }
    }
}
