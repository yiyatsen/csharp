﻿using GTAUOUtility.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using static GTAUOCore.AUOBatCore;

namespace GTAUOUtility.ViewModel
{
    public class UtilitySettingDialogViewModel : INotifyPropertyChanged
    {
        public ICommand ConfirmCommand { get { return new MainWindowCommand(Confirm); } }
        public ICommand CancelCommand { get { return new MainWindowCommand(Canecl); } }

        private BatConfig mBatConfig { get; set; }
        private ObservableCollection<string> mRoleList = new ObservableCollection<string>();
        private ObservableCollection<string> mCommList = new ObservableCollection<string>();
        private ObservableCollection<string> mDisplayList = new ObservableCollection<string>();
        private string mRole = "";
        private string mComm = "";
        private string mDisplay = "";
        private string mErrorMessage = "";

        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<string> RoleList
        {
            get { return mRoleList; }
            set { mRoleList = value; NotifyPropertyChanged("RoleList"); }
        }
        public ObservableCollection<string> CommList
        {
            get { return mCommList; }
            set { mCommList = value; NotifyPropertyChanged("CommList"); }
        }
        public ObservableCollection<string> DisplayList
        {
            get { return mDisplayList; }
            set { mDisplayList = value; NotifyPropertyChanged("DisplayList"); }
        }
        public string Role
        {
            get { return mRole; }
            set { mRole = value; NotifyPropertyChanged("Role"); }
        }
        public string Comm
        {
            get { return mComm; }
            set { mComm = value; NotifyPropertyChanged("Comm"); }
        }
        public string Display
        {
            get { return mDisplay; }
            set { mDisplay = value; NotifyPropertyChanged("Display"); }
        }
        public string ErrorMessage
        {
            get { return mErrorMessage; }
            set { mErrorMessage = value; NotifyPropertyChanged("ErrorMessage"); }
        }

        public UtilitySettingDialogViewModel(BatConfig config)
        {
            mBatConfig = config;

            SetRole();
            SetComm();
            SetDisplay();
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        private void NotifyPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged != null) foreach (string propertyName in propertyNames) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SetRole()
        {
            mRoleList.Clear();

            foreach (var item in Enum.GetValues(typeof(AUORole)).Cast<AUORole>())
            {
                string str = item.ToString();
                mRoleList.Add(str);
                if (mBatConfig.Role == item)
                {
                    Role = str;
                }
            }
            RoleList = mRoleList;
        }
        private void SetComm()
        {
            mCommList.Clear();

            foreach (var item in Enum.GetValues(typeof(CommMode)).Cast<CommMode>())
            {
                string str = item.ToString();
                mCommList.Add(str);
                if (mBatConfig.Comm == item)
                {
                    Comm = str;
                }
            }
            CommList = mCommList;
        }
        private void SetDisplay()
        {
            mDisplayList.Clear();

            foreach (var item in Enum.GetValues(typeof(DisplayMode)).Cast<DisplayMode>())
            {
                string str = item.ToString();
                mDisplayList.Add(str);
                if (mBatConfig.Display == item)
                {
                    Display = str;
                }
            }
            DisplayList = mDisplayList;
        }

        private void Confirm(object obj)
        {
            mBatConfig.Role = (AUORole)System.Enum.Parse(typeof(AUORole), Role);
            mBatConfig.Comm = (CommMode)System.Enum.Parse(typeof(CommMode), Comm);
            mBatConfig.Display = (DisplayMode)System.Enum.Parse(typeof(DisplayMode), Display);
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(mBatConfig, null);
        }

        private void Canecl(object obj)
        {
            MaterialDesignThemes.Wpf.DialogHost.CloseDialogCommand.Execute(null, null);
        }
    }
}
