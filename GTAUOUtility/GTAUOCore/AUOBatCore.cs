﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static GTAUOCore.AUOBatFormat;

namespace GTAUOCore
{
    public class AUOBatCore
    {
        public enum DisplayMode
        {
            Raw,
            Normal
        }

        public enum CommMode
        {
            SerialPort,
            UDP,
            //TCP
            //SNMP,
            //Unknow
        }

        public enum AUORole
        {
            PCEmulator = 0xA5,
            BATEmulator = 0x5A,
            Unknow
        }

        public enum BatCmd : byte
        {
            None = 0x00,
            GetBatInfo = 0x01,
            SetBatParam = 0x03,
            GetBatParam = 0x04,
            GetBatAlarmStatus = 0x05
        }

        public enum BatCmdStep : byte
        {
            CheckSTX = 0x00,
            CheckBATDirection = 0x01,
            CheckCmd = 0x02,
            CheckCmdLen = 0x03,
            ReceiveData = 0x04,
            CheckSum = 0x05,
            CheckETX = 0x06
        }

        public enum BatCmdErrorType : byte
        {
            None = 0x00,
            ErrorBATDirectionCmd = 0x01,
            ErrorCmd = 0x02,
            ErrorReceiveData = 0x04,
            ErrorCheckSum = 0x05,
            ErrorETX = 0x06,
            Unknow = 0x07,
        }

        public class BatConfig
        {
            public DisplayMode Display { get; set; }
            public CommMode Comm { get; set; }
            public AUORole Role { get; set; }
            public string Path { get; set; }
            public int Port { get; set; }
        }

        public class BatParamTable
        {
            public double VoltBase { get; set; }
        }

        public class ParserFormat
        {
            public DateTime Timestatmp { get; set; }
            public List<byte> DataList { get; set; }
            public BatCmd Cmd { get; set; }
            public AUORole Role { get; set; }
            public BatCmdErrorType ErrorType { get; set; }
            public Dictionary<string, object> RawData = new Dictionary<string, object>();
            public Dictionary<string, object> RealData = new Dictionary<string, object>();

            public ParserFormat()
            {
                Timestatmp = DateTime.UtcNow;
            }
        }

        public const byte GaugeNum = 1;
        public const byte SerialNum = 2;
        public static string SYS_LOG_PATH = System.AppDomain.CurrentDomain.BaseDirectory + @".\SysLog\";
        public static string LOG_PATH = System.AppDomain.CurrentDomain.BaseDirectory + @".\Log\";

        public static byte STX = 0x02;
        public static byte ETX = 0x03;
        public const byte ToBAT = 0x5A;
        public const byte FromBAT = 0xA5;

        public static double Volt = 1.5;
        public static double Current = 0.78125;
        public static double OT = 0.49;

        public static double ConvertOVUV(object OVUVRaw)
        {
            return ((double)((byte)OVUVRaw * 24) * 0.001);
        }

        public static double ConvertVolt(object VoltRaw)
        {
            ushort raw = (ushort)VoltRaw;

            return ((double)(raw * 15) * 0.0001);
        }

        public static double ConvertOC(object OCRaw)
        {
            ushort raw = (ushort)OCRaw;
            int OCBase = 78;
            // 0x00(+0) => 0x05(+1) => 0x0C(+2) => 0x15(+3) => 0x1C(+4) => ...
            int OCOffset = (raw >> 4) * 2;
            byte OCOffsetBase = (byte)(raw & 0x0F);
            if (OCOffsetBase >= 0x0C)
            {
                OCOffset += 2;
            }
            else if (OCOffsetBase >= 0x05)
            {
                OCOffset += 1;
            }
            // //
            return  ((double)((raw * OCBase) + OCOffset) * 0.01);
        }

        public static double ConvertOT(object OTRaw)
        {
            ushort raw = (ushort)OTRaw;
            int OTBase = 5;
            // 0x00(+0) => 0x05(-1) => 0x0D(-2) => 0x16(-3) => 0x1E(-4) => ...
            int OCOffset = (raw /17) * 2;
            int OCOffsetBase = (raw > 0x400) ? ((raw & 0x400) % 17) : (raw % 17);
            if (OCOffsetBase >= 0x0D)
            {
                OCOffset += 2;
            }
            else if (OCOffsetBase >= 0x05)
            {
                OCOffset += 1;
            }
            OCOffset = OCOffset * -1;
            // //
            return ((double)((raw * OTBase) + OCOffset) * 0.01);
        }

        public static double ConvertCurrent(object CurrentRaw)
        {
            ushort raw = (ushort)CurrentRaw;
            int CurrentBase = 78;
            // 0x00(+0) => 0x04(+1) => 0x0B(+2) => 0x14(+3) => 0x1B(+4) => ...
            int CurrentOffset = (raw >> 4) * 2;
            byte CurrentOffsetBase = (byte)(raw & 0x0F);
            if (CurrentOffsetBase >= 0x0B)
            {
                CurrentOffset += 2;
            }
            else if (CurrentOffsetBase >= 0x04)
            {
                CurrentOffset += 1;
            }
            // //
            return ((double)(((raw * CurrentBase) + CurrentOffset) - 39922) * 0.01);
        }

        public static double ConvertTemperatureExt(object TempRaw)
        {
            ushort raw = (ushort)TempRaw;
            int TempParam = 0;
            double TempOpt = -1.0;

            if (raw > 0x3FF)
            {
                TempOpt = 1.0;
                TempParam = (raw & 0x3FF);
            }
            else
            {
                TempParam = (0x3FF - raw);
            }
            // 24度(0x3FF)為基底 曲線
            double TempOffset = ((TempParam * TempParam * 0.00001) - (TempParam * 0.0475)) * TempOpt;
            // //
            return (TempOffset + (double)24.00);
        }

        public static double ConvertTemperatureInt(object TempRaw)
        {
            ushort raw = (ushort)TempRaw;
            int TempBase = 19;
            // 每四位(0.1875*4) 餘數為2(+18), 其餘(+19) 
            int TempOffset = ((raw + 2) >> 2);
            // //
            return ((double)(((raw * TempBase) - TempOffset) - 27316) * 0.01);
        }

        public static List<string> LogData = new List<string>();

        public static bool IsGetBatParamFromBATCmd(BatCmd batCmd, byte batDirection)
        {
            return (BatCmd.GetBatParam == batCmd && (FromBAT == batDirection));
        }

        public static byte SetDataToList(List<byte> dataList, byte data)
        {
            dataList.Add(data);
            return data;
        }

        public static byte PackBat(List<ParamConfig> configList, Dictionary<string, object> raw, List<byte> dataList)
        {
            byte dataLen = 0x00;
            byte checkSum = SetDataToList(dataList, dataLen);

            for(int i =0;i< configList.Count; i++)
            {
                object value = raw[configList[i].Name];
                string type = value.GetType().FullName;

                switch (type)
                {
                    case "System.Byte":
                        {
                            checkSum += SetDataToList(dataList, (byte)((byte)value & 0xFF));
                            dataLen += 1;
                            break;
                        }
                    case "System.UInt16":
                        {
                            checkSum += SetDataToList(dataList, (byte)((((ushort)value) >> 0x08) & 0xFF));
                            checkSum += SetDataToList(dataList, (byte)(((ushort)value) & 0xFF));
                            dataLen += 2;
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }

            dataList[4] = dataLen;
            checkSum += dataLen;

            return checkSum;
        }

        public static List<byte> PackData(BatCmd batCmd, AUOBatCore auoBatCore, List<byte> param)
        {
            List<byte> dataList = new List<byte>();

            byte checkSum = SetDataToList(dataList, STX);
            checkSum += SetDataToList(dataList, auoBatCore.BatID);
            checkSum += SetDataToList(dataList, 0x00);
            checkSum += SetDataToList(dataList, (byte)batCmd);

            switch (auoBatCore.mAUORole)
            {
                case AUORole.PCEmulator:
                    {
                        dataList[2] = (byte)AUORole.BATEmulator;
                        checkSum += dataList[2];
                        switch (batCmd)
                        {
                            case BatCmd.GetBatInfo:
                                {
                                    checkSum += SetDataToList(dataList, 0x03);
                                    checkSum += SetDataToList(dataList, param[0]);
                                    checkSum += SetDataToList(dataList, param[1]);
                                    checkSum += SetDataToList(dataList, 0x00);

                                    break;
                                }
                            case BatCmd.SetBatParam:
                                {
                                    checkSum += AUOBatCore.PackBat(Bat_Param_Config, auoBatCore.ParamRaw, dataList);
                                    break;
                                }
                            case BatCmd.GetBatParam:
                            case BatCmd.GetBatAlarmStatus:
                                {
                                    checkSum += SetDataToList(dataList, 0x02);
                                    checkSum += SetDataToList(dataList, 0x00);
                                    checkSum += SetDataToList(dataList, 0x00);

                                    break;
                                }
                            default:
                                {
                                    dataList.Clear();
                                    break;
                                }
                        }
                        break;
                    }
                case AUORole.BATEmulator:
                    {
                        dataList[2] = (byte)AUORole.PCEmulator;
                        checkSum += dataList[2];
                        switch (batCmd)
                        {
                            case BatCmd.GetBatInfo:
                                {
                                    if (param != null)
                                    {
                                        if (param.Count == 1)
                                        {
                                            checkSum += SetDataToList(dataList, 0x01);
                                            checkSum += SetDataToList(dataList, param[0]);
                                        }
                                        else
                                        {
                                            Dictionary<string, object> raw = (param[1] == 1) ? auoBatCore.Info1Raw : auoBatCore.Info2Raw;

                                            checkSum += AUOBatCore.PackBat(Bat_Info_Config, raw, dataList);
                                            
                                            Console.WriteLine(string.Format("TemperatureExt({0})", AUOBatCore.ConvertTemperatureExt(raw["TemperatureExt1"])));
                                        }
                                    }
                                    else
                                    {
                                        dataList.Clear();
                                    }

                                    break;
                                }
                            case BatCmd.SetBatParam:
                                {
                                    checkSum += SetDataToList(dataList, 0x01);
                                    checkSum += SetDataToList(dataList, param[0]);

                                    break;
                                }
                            case BatCmd.GetBatParam:
                                {
                                    //if (param != null && param.Count == 1)
                                    //{
                                    //    checkSum += SetDataToList(dataList, 0x01);
                                    //    checkSum += SetDataToList(dataList, param[0]);
                                    //}
                                    //else
                                    //{
                                    //    checkSum += AUOBatCore.PackBat(Bat_Param_Config, auoBatCore.ParamRaw, dataList);
                                    //}
                                    checkSum += AUOBatCore.PackBat(Bat_Param_Config, auoBatCore.ParamRaw, dataList);

                                    break;
                                }
                            case BatCmd.GetBatAlarmStatus:
                                {
                                    checkSum += SetDataToList(dataList, 0x01);
                                    checkSum += SetDataToList(dataList, param[0]);

                                    break;
                                }
                            default:
                                {
                                    dataList.Clear();
                                    break;
                                }
                        }
                        break;
                    }
                default:
                    {
                        dataList.Clear();
                        break;
                    }
            }

            if (dataList.Count > 0)
            {
                dataList.Add(checkSum);
                dataList.Add(ETX);
            }

            return dataList;
        }

        public static void SetDataTable(Dictionary<string, object> data, string key, object value)
        {
            if (data.ContainsKey(key))
            {
                data[key] = value;
            }
            else
            {
                data.Add(key, value);
            }
        }

        public static int UnpackBatInfoRaw(ParserFormat parser, Dictionary<string, object> data)
        {
            int index = 0x05;
            for (int i = 0; i < 12; i++)
            {
                AUOBatCore.SetDataTable(data, string.Format("Cell{0}", i + 1), (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));

                index += 2;
            }

            AUOBatCore.SetDataTable(data, "UVStatus", (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));
            index += 2;
            AUOBatCore.SetDataTable(data, "OVStatus", (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));
            index += 2;
            
            for (int i = 0; i < 2; i++)
            {
                AUOBatCore.SetDataTable(data, string.Format("TemperatureExt{0}", i + 1), (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));
                index += 2;
            }

            AUOBatCore.SetDataTable(data, "TemperatureInt", (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));
            index += 2;

            AUOBatCore.SetDataTable(data, "BalanceStatus", (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));
            index += 2;
            AUOBatCore.SetDataTable(data, "Current", (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));
            index += 2;
            AUOBatCore.SetDataTable(data, "SOC", (byte)(parser.DataList[index]));
            index += 1;
            AUOBatCore.SetDataTable(data, "UVCount", (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));
            index += 2;
            AUOBatCore.SetDataTable(data, "OVCount", (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));
            index += 2;
            AUOBatCore.SetDataTable(data, "OTCount", (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));
            index += 2;
            AUOBatCore.SetDataTable(data, "OCCount", (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));
            index += 2;

            return index;
        }

        public static int UnpackBatParamRaw(ParserFormat parser, Dictionary<string, object> data)
        {
            int index = 0x05;

            AUOBatCore.SetDataTable(data, "UV", (byte)(parser.DataList[index]));
            index += 1;
            AUOBatCore.SetDataTable(data, "OV", (byte)(parser.DataList[index]));
            index += 1;

            AUOBatCore.SetDataTable(data, "VoltDiff", (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));
            index += 2;
            AUOBatCore.SetDataTable(data, "VoltMax", (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));
            index += 2;

            AUOBatCore.SetDataTable(data, "OC", (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));
            index += 2;
            AUOBatCore.SetDataTable(data, "OT", (ushort)((parser.DataList[index] << 0x08) | parser.DataList[index + 1]));
            index += 2;

            AUOBatCore.SetDataTable(data, "GaugeCount", (byte)(parser.DataList[index]));
            index += 1;
            AUOBatCore.SetDataTable(data, "SeriesCount", (byte)(parser.DataList[index]));
            index += 1;
            AUOBatCore.SetDataTable(data, "CellCount", (byte)(parser.DataList[index]));
            index += 1;
            AUOBatCore.SetDataTable(data, "EnableDischarge", (byte)(parser.DataList[index]));
            index += 1;
            AUOBatCore.SetDataTable(data, "LastCellCount", (byte)(parser.DataList[index]));
            index += 1;
            AUOBatCore.SetDataTable(data, "FanOnTemperature", (byte)(parser.DataList[index]));
            index += 1;
            AUOBatCore.SetDataTable(data, "FanOffTemperature", (byte)(parser.DataList[index]));
            index += 1;

            return index;
        }

        public static void UnpackBatInfo(Dictionary<string, object> raw, Dictionary<string, object> data)
        {
            string key = "";
            for (int i = 0; i < 12; i++)
            {
                key = string.Format("Cell{0}", i + 1);
                if (raw.ContainsKey(key))
                {
                    AUOBatCore.SetDataTable(data, key, AUOBatCore.ConvertVolt(raw[key]));
                }
            }
            key = "UVStatus";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, raw[key]);
            }
            key = "OVStatus";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, raw[key]);
            }
            for (int i = 0; i < 12; i++)
            {
                key = string.Format("TemperatureExt{0}", i + 1);
                if (raw.ContainsKey(key))
                {
                    AUOBatCore.SetDataTable(data, key, raw[key]);
                }
            }
            key = "TemperatureInt";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, AUOBatCore.ConvertTemperatureInt(raw[key]));
            }
            key = "BalanceStatus";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, raw[key]);
            }
            key = "Current";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, AUOBatCore.ConvertCurrent(raw[key]));
            }
            key = "SOC";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, raw[key]);
            }
            key = "UVCount";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, raw[key]);
            }
            key = "OVCount";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, raw[key]);
            }
            key = "OTCount";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, raw[key]);
            }
            key = "OCCount";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, raw[key]);
            }
        }
 
        public static void UnpackBatParam(Dictionary<string, object> raw, Dictionary<string, object> data)
        {
            string key = "UV";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, AUOBatCore.ConvertOVUV(raw[key]));
            }
            key = "OV";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, AUOBatCore.ConvertOVUV(raw[key]));
            }
            key = "VoltDiff";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, AUOBatCore.ConvertVolt(raw[key]));
            }
            key = "VoltMax";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, AUOBatCore.ConvertVolt(raw[key]));
            }
            key = "OC";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, AUOBatCore.ConvertOC(raw[key]));
            }
            key = "OT";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, AUOBatCore.ConvertOT(raw[key]));
            }
            key = "GaugeCount";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, raw[key]);
            }
            key = "SeriesCount";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, raw[key]);
            }
            key = "CellCount";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, raw[key]);
            }
            key = "EnableDischarge";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, Convert.ToBoolean(raw[key]));
            }
            key = "LastCellCount";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, raw[key]);
            }
            key = "FanOnTemperature";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, raw[key]);
            }
            key = "FanOffTemperature";
            if (raw.ContainsKey(key))
            {
                AUOBatCore.SetDataTable(data, key, raw[key]);
            }
        }

        public static string GetRealDataStr(List<ParamConfig> configList, Dictionary<string, object> data)
        {
            string str = "";
            for (int i = 0; i < configList.Count; i++)
            {
                if (data.ContainsKey(configList[i].Name))
                {
                    object value = data[configList[i].Name];
                    string type = value.GetType().FullName;

                    switch (type)
                    {
                        case "System.Byte":
                            {
                                str += string.Format("{0}\t", (byte)value);
                                break;
                            }
                        case "System.UInt16":
                            {
                                str += string.Format("{0}\t", (ushort)value);
                                break;
                            }
                        case "System.Double":
                            {
                                str += string.Format("{0}\t", (double)value);
                                break;
                            }
                        case "System.Boolean":
                            {
                                str += string.Format("{0}\t", (bool)value);
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
            }
            return str;
        }

        public static bool UnpackData(AUOBatCore auoBatCore, ParserFormat parser, List<byte> param)
        {
            if (parser.DataList.Count < 3)
            {
                return false;
            }
            if (auoBatCore.mAUORole != AUORole.Unknow && parser.DataList[2] != (byte)auoBatCore.mAUORole)
            {
                return false;
            }
            BatCmd batCmd = (BatCmd)(parser.DataList[3]);

            bool isSuccess = false;

            string tmpLog = string.Format("{0}\t{1}\t", parser.Timestatmp.ToString("yyyy/MM/dd HH:mm:ss"), auoBatCore.mAUORole.ToString()) ;

            switch (auoBatCore.mAUORole)
            {
                case AUORole.BATEmulator:
                    {
                        switch (batCmd)
                        {
                            case BatCmd.GetBatInfo:
                                {
                                    tmpLog += string.Format("{0}P({1})S({2})\t", batCmd.ToString(), parser.DataList[5], parser.DataList[6]);
                                    param.Add(parser.DataList[5]);
                                    param.Add(parser.DataList[6]);
                                    isSuccess =  true;
                                    break;
                                }
                            case BatCmd.GetBatParam:
                                {
                                    tmpLog += string.Format("{0}\t", batCmd.ToString());
                                    param.Add(0x00);
                                    isSuccess = true;
                                    break;
                                }
                            case BatCmd.GetBatAlarmStatus:
                                {
                                    tmpLog += string.Format("{0}\t", batCmd.ToString());
                                    if (auoBatCore.BatID == parser.DataList[1])
                                    {
                                        param.Add(0x00);
                                        isSuccess = true;
                                    }
                                    else
                                    {
                                        param.Add(0xFF);

                                    }
                                    break;
                                }
                            case BatCmd.SetBatParam:
                                {
                                    tmpLog += string.Format("{0}\t", batCmd.ToString());
                                    param.Add(0x00);
                                    AUOBatCore.UnpackBatParamRaw(parser, parser.RawData);
                                    AUOBatCore.UnpackBatParam(parser.RawData, parser.RealData);

                                    auoBatCore.BatID = parser.DataList[1];
                                    auoBatCore.ParamRaw = parser.RawData;
                                    auoBatCore.Param = parser.RealData;
                                    isSuccess = true;

                                    break;
                                }
                            default:
                                {
                                    break;
                                }
                        }
                        break;
                    }
                default:
                    {
                        switch (batCmd)
                        {
                            case BatCmd.GetBatInfo:
                                {
                                    // param[0] Gauge ID
                                    // param[1] Serial ID
                                    if (auoBatCore.mAUORole == AUORole.Unknow)
                                    {
                                        auoBatCore.BatID = parser.DataList[1];
                                    }
                                    tmpLog += string.Format("{0}P({1})S({2})\t", batCmd.ToString(), param[0], param[1]);
                                    AUOBatCore.UnpackBatInfoRaw(parser, parser.RawData);
                                    AUOBatCore.UnpackBatInfo(parser.RawData, parser.RealData);

                                    tmpLog += AUOBatCore.GetRealDataStr(Bat_Info_Config, parser.RealData);
                                    if (param[1] == 1)
                                    {
                                        auoBatCore.Info1Raw = parser.RawData;
                                        auoBatCore.Info1 = parser.RealData;
                                    }
                                    else
                                    {
                                        auoBatCore.Info2Raw = parser.RawData;
                                        auoBatCore.Info2 = parser.RealData;
                                    }
                                    isSuccess = true;
                                    break;
                                }
                            case BatCmd.GetBatParam:
                                {
                                    tmpLog += string.Format("{0}\t", batCmd.ToString());
                                    auoBatCore.BatID = parser.DataList[1];
                                    // 轉換實際數值
                                    AUOBatCore.UnpackBatParamRaw(parser, parser.RawData);
                                    AUOBatCore.UnpackBatParam(parser.RawData, parser.RealData);

                                    tmpLog += AUOBatCore.GetRealDataStr(Bat_Param_Config, parser.RealData);
                                    auoBatCore.ParamRaw = parser.RawData;
                                    auoBatCore.Param = parser.RealData;
                                    isSuccess = true;
                                    break;
                                }
                            case BatCmd.SetBatParam:
                            case BatCmd.GetBatAlarmStatus:
                                {
                                    tmpLog += string.Format("{0}\t", batCmd.ToString());
                                    // 轉換實際數值 GetBatAlarmStatus ?
                                    isSuccess = (parser.DataList[0x05] != 0xFF);
                                    break;
                                }
                            default:
                                {
                                    break;
                                }
                        }
                        break;
                    }
            }
            foreach (byte data in parser.DataList)
            {
                tmpLog += string.Format("{0} ", data.ToString("X2"));
            }
            tmpLog += "\r\n";
            LogData.Add(tmpLog);

            return isSuccess;
        }
        
        public static List<ParserFormat> ParserSerialData(List<byte> dataList)
        {
            List<ParserFormat> parserFormatList = new List<ParserFormat>();

            int dataLen = dataList.Count;
            BatCmdStep batCmdStep = BatCmdStep.CheckSTX;
            BatCmdErrorType batCmdErrorType = BatCmdErrorType.Unknow;
            int removeLen = 0;
            byte data = 0x00;
            int cmdInfoLen = 0;
            byte checkSum = 0x00;
            BatCmd batCmd = BatCmd.None;
            List<byte> cmdDataList = new List<byte>();
            for (int i = 0; i < dataLen; i++)
            {
                data = dataList[i];
                cmdDataList.Add(data);
                int cmdLen = cmdDataList.Count;
                switch (batCmdStep)
                {
                    case BatCmdStep.CheckBATDirection:
                        {
                            checkSum += data;
                            if (cmdLen == 3 && (data == ToBAT || data == FromBAT))
                            {
                                batCmdStep = BatCmdStep.CheckCmd;
                            }
                            else if (cmdLen > 3)
                            {
                                parserFormatList.Add(new ParserFormat
                                {
                                    ErrorType = BatCmdErrorType.ErrorBATDirectionCmd,
                                    Cmd = batCmd,
                                    DataList = cmdDataList.ToList()
                                });
                                cmdDataList.Clear();

                                batCmdStep = BatCmdStep.CheckSTX;
                                removeLen += cmdLen;
                            }
                            break;
                        }
                    case BatCmdStep.CheckCmd:
                        {
                            checkSum += data;
                            switch ((BatCmd)data)
                            {
                                case BatCmd.GetBatInfo:
                                case BatCmd.SetBatParam:
                                case BatCmd.GetBatParam:
                                case BatCmd.GetBatAlarmStatus:
                                    {
                                        batCmdStep = BatCmdStep.CheckCmdLen;
                                        batCmd = (BatCmd)data;
                                        break;
                                    }
                                default:
                                    {
                                        parserFormatList.Add(new ParserFormat
                                        {
                                            ErrorType = BatCmdErrorType.ErrorCmd,
                                            Cmd = batCmd,
                                            DataList = cmdDataList.ToList()
                                        });
                                        cmdDataList.Clear();

                                        batCmdStep = BatCmdStep.CheckSTX;
                                        removeLen += cmdLen;
                                        break;
                                    }
                            }
                            break;
                        }
                    case BatCmdStep.CheckCmdLen:
                        {
                            if (IsGetBatParamFromBATCmd((BatCmd)cmdDataList[3], cmdDataList[2]))
                            {
                                cmdInfoLen = 0x11; // 從電池端回覆的長度及CheckSum是不正確的
                            }
                            else
                            {
                                cmdInfoLen = data;
                            }
                            batCmdStep = BatCmdStep.ReceiveData;
                            break;
                        }
                    case BatCmdStep.ReceiveData:
                        {
                            int cmdTotalLen = cmdInfoLen + 5;

                            if (cmdLen == cmdTotalLen)
                            {
                                batCmdStep = BatCmdStep.CheckSum;
                            }
                            else if (cmdLen > cmdTotalLen)
                            {
                                parserFormatList.Add(new ParserFormat {
                                    ErrorType = BatCmdErrorType.ErrorReceiveData,
                                    Cmd = batCmd,
                                    DataList = cmdDataList.ToList()
                                });
                                cmdDataList.Clear();

                                batCmdStep = BatCmdStep.CheckSTX;
                                removeLen += cmdLen;
                            }
                            break;
                        }
                    case BatCmdStep.CheckSum:
                        {
                            if (checkSum == data || IsGetBatParamFromBATCmd((BatCmd)cmdDataList[3], cmdDataList[2]))
                            {
                                batCmdStep = BatCmdStep.CheckETX;
                            }
                            else
                            {
                                // CheckSum錯誤
                                parserFormatList.Add(new ParserFormat
                                {
                                    ErrorType = BatCmdErrorType.ErrorCheckSum,
                                    Cmd = batCmd,
                                    DataList = cmdDataList.ToList()
                                });
                                cmdDataList.Clear();

                                batCmdStep = BatCmdStep.CheckSTX;
                                removeLen += cmdLen;
                            }
                            break;
                        }
                    case BatCmdStep.CheckETX:
                        {
                            parserFormatList.Add(new ParserFormat
                            {
                                ErrorType = (ETX == data) ? BatCmdErrorType.None : BatCmdErrorType.ErrorETX,
                                Cmd = batCmd,
                                DataList = cmdDataList.ToList()
                            });
                            cmdDataList.Clear();

                            batCmdStep = BatCmdStep.CheckSTX;
                            removeLen += cmdLen;
                            break;
                        }
                    default:
                        {
                            if (data == STX)
                            {
                                batCmd = BatCmd.None;
                                cmdDataList.RemoveAt(cmdLen - 1);
                                if (cmdDataList.Count > 0)
                                {
                                    // 回傳未知格式
                                    parserFormatList.Add(new ParserFormat
                                    {
                                        ErrorType = batCmdErrorType,
                                        Cmd = batCmd,
                                        DataList = cmdDataList.ToList()
                                    });
                                    cmdDataList.Clear();
                                }

                                batCmdStep = BatCmdStep.CheckBATDirection;
                                checkSum = data;
                                cmdInfoLen = 0x00;
                                cmdDataList.Add(data);
                            }
                            else
                            {
                                removeLen += 1;
                            }
                            batCmdErrorType = BatCmdErrorType.Unknow;
                            break;
                        }
                }
            }

            if (removeLen > 0)
            {
                dataList.RemoveRange(0, removeLen);
            }

            return parserFormatList;
        }
        
        public AUORole mAUORole = AUORole.PCEmulator;
        public bool IsOpen { get { return (mCommInterface != null) ? mCommInterface.IsOpen : false; } }
        private CommInterface mCommInterface { get; set; }

        public byte BatID { get; set; }

        public byte BatGaugeNum { get; set; }
        public byte BatSerialNum { get; set; }
        public byte BatGauge { get; set; }
        public byte BatSerial { get; set; }
        public delegate void RefreshUIHandler();
        public event RefreshUIHandler RefreshUIEvent;

        public Dictionary<string, object> ParamRaw = new Dictionary<string, object>();
        public Dictionary<string, object> Param = new Dictionary<string, object>();
        public Dictionary<string, object> Info1Raw = new Dictionary<string, object>();
        public Dictionary<string, object> Info1 = new Dictionary<string, object>();
        public Dictionary<string, object> Info2Raw = new Dictionary<string, object>();
        public Dictionary<string, object> Info2 = new Dictionary<string, object>();

        public string PathAndPort
        {
            get { return (mCommInterface == null) ? "" : string.Format("{0}:{1}", mCommInterface.Path, mCommInterface.Port); }
        }

        public void SetCommInterface(CommInterface commInterface)
        {
            mCommInterface = commInterface;
        }

        public bool Disconnect()
        {
            if (mCommInterface != null)
            {
                mCommInterface.Disconnect();
                mCommInterface.DataReceivedEvent -= DataReceivedEvent;
                return true;
            }
            return false;
        }

        public bool Connect(BatConfig batConfig)
        {
            if (mCommInterface != null)
            {
                Reset();
                this.mAUORole = batConfig.Role;
                mCommInterface.DataReceivedEvent += DataReceivedEvent;
                return (mCommInterface.Connect(batConfig.Path, batConfig.Port) == 0);
            }
            return false;
        }

        public void Reset()
        {
            if (mAUORole == AUORole.PCEmulator)
            {
                Init(0x00);
            }
        }

        public void Init(byte id)
        {
            BatID = id;
            byte checkSum = (byte)(0x02 + BatID + 0xA5 + 0x04 + 0x0C);

            ParserFormat parser = new ParserFormat();
            parser.Cmd = BatCmd.GetBatParam;
            parser.DataList = new List<byte>() { 0x02, BatID, 0xA5, 0x04, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, checkSum, 0x03 };
            if (mAUORole == AUORole.Unknow)
            {
                parser.DataList = new List<byte>() { 0x02, BatID, 0xA5, 0x04, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, checkSum, 0x03 };
            }
            else
            {
                parser.DataList = new List<byte>() { 0x02, BatID, 0xA5, 0x04, 0x0C, 0x6C, 0x98, 0x01, 0xD2, 0x09, 0x81, 0x01, 0x5A, 0x04, 0x6A, 0x01, 0x02, 0x08, 0x00, 0x08, 0x1B, 0x19, checkSum, 0x03 };
            }

            AUOBatCore.UnpackBatParamRaw(parser, ParamRaw);
            AUOBatCore.UnpackBatParam(ParamRaw, Param);

            checkSum = (byte)(0x02 + BatID + 0xA5 + 0x01 + 0x2F);
            parser.Cmd = BatCmd.GetBatInfo;
            parser.DataList = new List<byte>() { 0x02, BatID, 0xA5, 0x01, 0x2F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, checkSum, 0x03 };
            if (mAUORole == AUORole.Unknow)
            {
                parser.DataList = new List<byte>() { 0x02, BatID, 0xA5, 0x01, 0x2F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, checkSum, 0x03 };
            }
            else
            {
                parser.DataList = new List<byte>() { 0x02, BatID, 0xA5, 0x01, 0x2F, 0x08, 0x92, 0x08, 0x93, 0x08, 0x92, 0x0A, 0x39, 0x0A, 0x39, 0x0A, 0x39, 0x0A, 0x39, 0x0A, 0x39, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x32, 0x04, 0x38, 0x06, 0x23, 0x00, 0x00, 0x01, 0xFF, 0x31, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0, 0x00, 0x00, checkSum, 0x03 };
            }
            AUOBatCore.UnpackBatInfoRaw(parser, Info1Raw);
            AUOBatCore.UnpackBatInfo(Info1Raw, Info1);

            checkSum = (byte)(0x02 + BatID + 0xA5 + 0x01 + 0x2F);
            parser.Cmd = BatCmd.GetBatInfo;
            parser.DataList = new List<byte>() { 0x02, BatID, 0xA5, 0x01, 0x2F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, checkSum, 0x03 };
            if (mAUORole == AUORole.Unknow)
            {
                parser.DataList = new List<byte>() { 0x02, BatID, 0xA5, 0x01, 0x2F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, checkSum, 0x03 };
            }
            else
            {
                parser.DataList = new List<byte>() { 0x02, BatID, 0xA5, 0x01, 0x2F, 0x08, 0x92, 0x08, 0x94, 0x08, 0x91, 0x08, 0x92, 0x08, 0x91, 0x08, 0x8F, 0x08, 0x90, 0x08, 0x8E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x32, 0x04, 0x38, 0x06, 0x23, 0x00, 0x00, 0x01, 0xFF, 0x31, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, checkSum, 0x03 };
            }
            AUOBatCore.UnpackBatInfoRaw(parser, Info2Raw);
            AUOBatCore.UnpackBatInfo(Info2Raw, Info2);
        }

        public async Task<bool> PCEmulatorFlow(BatCmd batCmd, List<byte> param)
        {
            // 1. 封裝傳送的PC端指令
            List<byte> sendList = AUOBatCore.PackData(batCmd, this, param);
            if (sendList.Count == 0) return false;

            // 2. 傳送PC端指令
            mCommInterface.SendData(sendList.ToArray());
            // 3. 接收BAT端指令
            List<ParserFormat> parserFormatList = null;

            bool isContinue = true;
            long startTime = DateTime.Now.Ticks + 10000000;
            while (isContinue)
            {
                await Task.Delay(200);
                parserFormatList = ParserSerialData(mCommInterface.RecvData);
                isContinue = (parserFormatList.Count == 0);
                if (isContinue)
                {
                    isContinue = DateTime.Now.Ticks < startTime;
                }
            }

            bool isSuccess = false;
            for (int i = 0; i < parserFormatList.Count; i++)
            {
                // 4. 解封裝接收的BAT端指令
                isSuccess = AUOBatCore.UnpackData(this, parserFormatList[i], param);
            }

            return isSuccess;
        }
        
        public bool BATEmulatorFlow()
        {
            List<ParserFormat> parserFormatList = ParserSerialData(mCommInterface.RecvData);
            if (parserFormatList.Count == 0) return false;

            List<byte> param = new List<byte>();
            // 1. 解封裝接收的PC端指令
            bool isSuccess = false;
            BatCmd batCmd = BatCmd.None;
            for (int i = 0; i < parserFormatList.Count; i++)
            {
                // 4. 解封裝接收的BAT端指令
                if (this.mAUORole == AUORole.Unknow)
                {
                    param.Clear();
                    param.Add(BatGauge);
                    param.Add(BatSerial);
                }

                isSuccess = AUOBatCore.UnpackData(this, parserFormatList[i], param);
                isSuccess = (isSuccess && parserFormatList[i].DataList.Count > 3);
                if (isSuccess)
                {
                    batCmd = (BatCmd)(parserFormatList[i].DataList[3]);
                    if (this.mAUORole == AUORole.Unknow)
                    {
                        if (batCmd == BatCmd.GetBatParam)
                        {
                            BatGaugeNum = (byte)Param["GaugeCount"];
                            BatSerialNum = (byte)Param["SeriesCount"];
                            RefreshUIEvent();
                        }
                        else if (batCmd == BatCmd.GetBatAlarmStatus)
                        {
                            BatGauge = 1;
                            BatSerial = 1;
                        }
                        else if (batCmd == BatCmd.GetBatInfo)
                        {
                            // 錯誤訊息
                            bool isFail = false;
                            Dictionary<string, object> checkData = (BatSerial == 1) ? Info1Raw : Info2Raw;

                            if ((ushort)checkData["TemperatureExt1"] == 0x00)
                            {
                                isFail = true;
                            }
                            if ((ushort)checkData["TemperatureExt2"] == 0x00)
                            {
                                isFail = true;
                            }
                            if ((ushort)checkData["TemperatureInt"] == 0x00)
                            {
                                isFail = true;
                            }
                            if (isFail)
                            {
                                string sysfile = string.Format("{0}{1}.txt", AUOBatCore.SYS_LOG_PATH, parserFormatList[i].Timestatmp.ToString("yyyyMMdd"));
                                File.AppendAllText(sysfile, string.Format("{0}\t{1}\r\n", parserFormatList[i].Timestatmp.ToString("yyyyMMddHHmmss"), "Bat Info Invaild"));
                            }

                            BatGauge = (byte)(((BatGauge) % BatGaugeNum) + 1);
                            BatSerial = (byte)(((BatSerial) % BatSerialNum) + 1);
                            RefreshUIEvent();
                        }
                    }
                }
            }
            if (!isSuccess)
            {
                return false;
            }
            // 2. 封裝傳送的BAT端指令
            List<byte> sendList = AUOBatCore.PackData(batCmd, this, param);
            if (sendList.Count == 0)
            {
                return false;
            }

            if (mAUORole == AUORole.BATEmulator)
            {
                // 3. 傳送BAT端指令
                string tmpLog = string.Format("{0}\t{1}(Send)\t", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), mAUORole.ToString());
                tmpLog += string.Format("{0}\t", batCmd.ToString());
                foreach (byte data in sendList)
                {
                    tmpLog += string.Format("{0} ", data);
                }
                tmpLog += "\r\n";
                //Console.WriteLine(txt);

                LogData.Add(tmpLog);
                // 錯誤測試
                if (batCmd == BatCmd.GetBatInfo)
                {
                    if (param[1] == 1)
                    {
                        if ((info1Count % 3) == 0)
                        {
                            info1Count = 0;
                            mCommInterface.SendData(sendList.ToArray());
                        } else
                        {
                            int count = 24;
                            mCommInterface.SendData(sendList.GetRange(0, 24).ToArray());
                            Thread.Sleep(50);
                            count = 14;
                            mCommInterface.SendData(sendList.GetRange(24, count).ToArray());
                            Thread.Sleep(50);
                            count = sendList.Count - 38;
                            mCommInterface.SendData(sendList.GetRange(38, count).ToArray());
                        }
                        info1Count++;
                    }
                    else
                    {
                        if ((info2Count % 2) == 0)
                        {
                            mCommInterface.SendData(sendList.ToArray());
                            info2Count = 0;
                        }
                        else
                        {
                            int count = 23;
                            mCommInterface.SendData(sendList.GetRange(0, count).ToArray());
                            Thread.Sleep(50);
                            count = sendList.Count - 23;
                            mCommInterface.SendData(sendList.GetRange(23, count).ToArray());
                        }
                        info2Count++;
                    }
                }
                else
                {
                    mCommInterface.SendData(sendList.ToArray());
                }
                // //

                if (batCmd == BatCmd.SetBatParam)
                {
                    // 需要回報界面修改
                    RefreshUIEvent();
                }
            }

            return true;
        }

        int info1Count = 0;
        int info2Count = 0;

        private void DataReceivedEvent(List<byte> data)
        {
            if (mAUORole != AUORole.PCEmulator)
            {
                BATEmulatorFlow();
            }
        }
    }
}
