﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTAUOCore
{
    public class AUOBatFormat
    {
        public class ParamConfig
        {
            public string Name { get; set; }
            public string Type { get; set; }
            public string Value { get; set; }
        }

        public static List<ParamConfig> Bat_Param_Value_Config = new List<ParamConfig>()
        {
            new ParamConfig() { Name = "Volt", Type = "System.Double" },
            new ParamConfig() { Name = "Current", Type = "System.Double" },
            new ParamConfig() { Name = "OC", Type = "System.Double" },
            new ParamConfig() { Name = "OT", Type = "System.Double" },
            new ParamConfig() { Name = "OVUV", Type = "System.Double" }
        };

        public static List<ParamConfig> Bat_Param_Config = new List<ParamConfig>()
        {
            new ParamConfig() { Name = "UV", Type = "System.Byte" },
            new ParamConfig() { Name = "OV", Type = "System.Byte" },
            new ParamConfig() { Name = "VoltDiff", Type = "System.UInt16" },
            new ParamConfig() { Name = "VoltMax", Type = "System.UInt16" },
            new ParamConfig() { Name = "OC", Type = "System.UInt16" },
            new ParamConfig() { Name = "OT", Type = "System.Double" },
            new ParamConfig() { Name = "GaugeCount", Type = "System.Byte" },
            new ParamConfig() { Name = "SeriesCount", Type = "System.Byte" },
            new ParamConfig() { Name = "CellCount", Type = "System.Byte" },
            new ParamConfig() { Name = "EnableDischarge", Type = "System.Boolean" },
            new ParamConfig() { Name = "LastCellCount", Type = "System.Byte" },
            new ParamConfig() { Name = "FanOnTemperature", Type = "System.Byte" },
            new ParamConfig() { Name = "FanOffTemperature", Type = "System.Byte" }
        };

        public static List<ParamConfig> Bat_Info_Config = new List<ParamConfig>()
        {
            new ParamConfig() { Name = "Cell1", Type = "System.UInt16" },
            new ParamConfig() { Name = "Cell2", Type = "System.UInt16" },
            new ParamConfig() { Name = "Cell3", Type = "System.UInt16" },
            new ParamConfig() { Name = "Cell4", Type = "System.UInt16" },
            new ParamConfig() { Name = "Cell5", Type = "System.UInt16" },
            new ParamConfig() { Name = "Cell6", Type = "System.UInt16" },
            new ParamConfig() { Name = "Cell7", Type = "System.UInt16" },
            new ParamConfig() { Name = "Cell8", Type = "System.UInt16" },
            new ParamConfig() { Name = "Cell9", Type = "System.UInt16" },
            new ParamConfig() { Name = "Cell10", Type = "System.UInt16" },
            new ParamConfig() { Name = "Cell11", Type = "System.UInt16" },
            new ParamConfig() { Name = "Cell12", Type = "System.UInt16" },
            new ParamConfig() { Name = "UVStatus", Type = "System.UInt16" },
            new ParamConfig() { Name = "OVStatus", Type = "System.UInt16" },
            new ParamConfig() { Name = "TemperatureExt1", Type = "System.Double" },
            new ParamConfig() { Name = "TemperatureExt2", Type = "System.Double" },
            new ParamConfig() { Name = "TemperatureInt", Type = "System.Double" },
            new ParamConfig() { Name = "BalanceStatus", Type = "System.UInt16" },
            new ParamConfig() { Name = "Current", Type = "System.UInt16" },
            new ParamConfig() { Name = "SOC", Type = "System.Byte" },
            new ParamConfig() { Name = "UVCount", Type = "System.UInt16" },
            new ParamConfig() { Name = "OVCount", Type = "System.UInt16" },
            new ParamConfig() { Name = "OTCount", Type = "System.UInt16" },
            new ParamConfig() { Name = "OCCount", Type = "System.UInt16" }
        };
    }
}
