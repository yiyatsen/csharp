﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GTAUOCore
{
    public class CommUDP : CommInterface
    {
        private bool IsOpen { get; set; }
        private string Path { get; set; }
        private int Port { get; set; }
        private IPEndPoint mIPEndPoint { get; set; }
        private UdpClient mUdpClient { get; set; }
        private CancellationTokenSource mCTS { get; set; }

        protected override string GetPath()
        {
            return "";
        }

        protected override int GetPort()
        {
            return (IsOpen) ? Port : 0;
        }
        protected override bool GetOpen()
        {
            return IsOpen;
        }

        public override void Disconnect()
        {
            if (IsOpen)
            {
                Port = 0;
                mCTS.Cancel();

            }
            if (mUdpClient != null)
            {
                mUdpClient.Close();
            }
            RecvData.Clear();
            IsOpen = false;
        }

        public override int Connect(string path, int port)
        {
            if (IsOpen)
            {
                return 0;
            }
            try
            {
                mIPEndPoint = new IPEndPoint(IPAddress.Any, port);
                Port = port;
                Path = path;
                mCTS = new CancellationTokenSource();
                DataReceived(mCTS.Token);
                IsOpen = true;
            }
            catch (Exception err)
            {
                return -1;
            }
            return 0;
        }

        public override int SendData(byte[] sendStream)
        {
            try
            {
                IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(Path), Port);
                UdpClient uc = new UdpClient();
                uc.SendAsync(sendStream, sendStream.Length, ipep);
            }
            catch
            {
                return -1;
            }
            return 0;
        }

        public int SendData(byte[] sendStream, string path)
        {
            try
            {
                IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(path), Port);
                UdpClient uc = new UdpClient();
                uc.SendAsync(sendStream, sendStream.Length, ipep);
            }
            catch
            {
                return -1;
            }
            return 0;
        }

        private async Task<int> DataReceived(CancellationToken token)
        {
            mUdpClient = new UdpClient(mIPEndPoint);
            while (!token.IsCancellationRequested)
            {
                UdpReceiveResult data = await mUdpClient.ReceiveAsync();

                RecvData.AddRange(data.Buffer);

                DataReceived();
            }

            mUdpClient.Close();
            return 0;
        }
    }
}
