﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTAUOCore
{
    public abstract class CommInterface
    {
        public delegate void DataReceivedHandler(List<byte> data);

        public string Path { get { return GetPath(); } }
        public int Port { get { return GetPort(); } }
        public bool IsOpen { get { return GetOpen(); } }
        public List<byte> RecvData = new List<byte>();
        public event DataReceivedHandler DataReceivedEvent;

        protected abstract string GetPath();
        protected abstract int GetPort();
        protected abstract bool GetOpen();

        public abstract void Disconnect();
        public abstract int Connect(string path, int port);
        public abstract int SendData(byte[] sendStream);
        protected void DataReceived()
        {
            if (RecvData.Count > 0)
            {
                DataReceivedEvent(RecvData);
            }
        }
    }
}
