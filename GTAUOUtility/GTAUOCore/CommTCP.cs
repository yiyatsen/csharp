﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GTAUOCore
{
    public class CommTCP : CommInterface
    {
        private bool IsOpen { get; set; }
        private string Path { get; set; }
        private int Port { get; set; }
        private IPEndPoint mIPEndPoint { get; set; }
        private TcpClient mTcpClient { get; set; }
        private CancellationTokenSource mCTS { get; set; }

        protected override string GetPath()
        {
            return "";
        }

        protected override int GetPort()
        {
            return (IsOpen) ? Port : 0;
        }
        protected override bool GetOpen()
        {
            return IsOpen;
        }

        public override void Disconnect()
        {
            if (IsOpen)
            {
                Port = 0;
                mCTS.Cancel();
            }
            if (mTcpClient != null)
            {
                mTcpClient.Close();
            }
            RecvData.Clear();
            IsOpen = false;
        }

        public override int Connect(string path, int port)
        {
            if (IsOpen)
            {
                return 0;
            }
            try
            {
                mIPEndPoint = new IPEndPoint(IPAddress.Any, port);
                Port = port;
                Path = path;
                mCTS = new CancellationTokenSource();
                DataReceived(mCTS.Token);
                IsOpen = true;
            }
            catch (Exception err)
            {
                return -1;
            }
            return 0;
        }

        public override int SendData(byte[] sendStream)
        {
            try
            {
                IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(Path), Port);
                TcpClient tcp = new TcpClient(ipep);
                NetworkStream stream = tcp.GetStream();
                stream.Write(sendStream, 0, sendStream.Length);
            }
            catch
            {
                return -1;
            }
            return 0;
        }

        public int SendData(byte[] sendStream, string path)
        {
            try
            {
                IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(path), Port);
                TcpClient tcp = new TcpClient(ipep);
                NetworkStream stream = tcp.GetStream();
                stream.Write(sendStream, 0, sendStream.Length);
            }
            catch
            {
                return -1;
            }
            return 0;
        }

        private async Task<int> DataReceived(CancellationToken token)
        {
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(Path), Port);
            mTcpClient = new TcpClient(ipep);
            byte[] bytes = new byte[1024];
            while (!token.IsCancellationRequested)
            {
                try
                {
                    NetworkStream stream = mTcpClient.GetStream();
                    int length = stream.Read(bytes, 0, bytes.Length);
                    if (length > 0)
                    {
                        RecvData.AddRange(bytes.Take(length));

                        DataReceived();
                    }
                } catch (Exception ex)
                {

                }
            }
            mTcpClient.Close();

            return 0;
        }
    }
}
