﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTAUOCore
{
    public class CommSerialPort : CommInterface
    {
        private SerialPort SerialPortObj = new SerialPort();

        protected override string GetPath()
        {
            return (SerialPortObj.IsOpen) ? SerialPortObj.PortName : "";
        }

        protected override int GetPort()
        {
            return (SerialPortObj.IsOpen) ? SerialPortObj.BaudRate : 0;
        }
        protected override bool GetOpen()
        {
            return SerialPortObj.IsOpen;
        }

        public override void Disconnect()
        {
            if (SerialPortObj != null)
            {
                SerialPortObj.DataReceived -= SerialPortObj_DataReceived;
                SerialPortObj.Close();
                RecvData.Clear();
            }
        }

        public override int Connect(string path, int port)
        {
            try
            {
                this.Disconnect();
                SerialPortObj.PortName = path;
                SerialPortObj.BaudRate = port;
                RecvData.Clear();
                SerialPortObj.DataReceived += SerialPortObj_DataReceived;
                SerialPortObj.Open();
            }
            catch (Exception err)
            {
                return -1;
            }
            return 0;
        }

        public override int SendData(byte[] sendStream)
        {
            try
            {
                SerialPortObj.DiscardOutBuffer();
                SerialPortObj.Write(sendStream, 0, sendStream.Length);
            }
            catch
            {
                return -1;
            }
            return 0;
        }

        private void SerialPortObj_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            byte[] buffer = new byte[256];
            while (sp.BytesToRead > 0)
            {
                Int32 receivedLen = sp.Read(buffer, 0, buffer.Length);
                Array.Resize(ref buffer, receivedLen);
                RecvData.AddRange(buffer);
            }

            DataReceived();
        }
    }
}
