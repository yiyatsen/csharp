﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskLib.Command
{
    public enum KioskGateCmd : byte
    {
        BlueLight = 0x01,
        GreenLight = 0x02,
        FrontGateLock = 0x04,
        BackGateLock = 0x08,
        OnChage = 0x10,
    }
}
