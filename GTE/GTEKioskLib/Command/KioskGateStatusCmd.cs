﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskLib.Command
{
    public enum KioskGateStatusCmd
    {
        CurrentStatus,
        TurnOffAllStatus,
        TurnOnAllStatus,
        BlueLightOff,
        BlueLightOn,
        GreenLightOff,
        GreenLightOn,
        FrontUnLockOff,
        FrontUnLockOn,
        BackUnLockOff,
        BackUnLockOn,
        OffCharge,
        OnCharge
    }
}
