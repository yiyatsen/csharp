﻿using GTE.InfoModel;
using GTEKioskLib.Command;
using GTEKioskLib.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskLib.DeivceConfiguration
{
    public class GateConfigAPI
    {
        public SendInfo ResetCmdInfo(KioskGateStatusCmd cmd, int address, GateStatusInfo para)
        {
            SendInfo result = new SendInfo();
            result.DataFooter = 0x0D;
            switch (cmd)
            {
                case KioskGateStatusCmd.CurrentStatus:
                    {
                        result.DataStream = new byte[0];

                        break;
                    }
                case KioskGateStatusCmd.TurnOnAllStatus:
                    {
                        int data = (byte)KioskGateCmd.BlueLight + (byte)KioskGateCmd.GreenLight + (byte)KioskGateCmd.FrontGateLock
                            + (byte)KioskGateCmd.BackGateLock + (byte)KioskGateCmd.OnChage;
                        result.DataStream = ASCIIEncoding.ASCII.GetBytes(string.Format("{0:X2}", data));
                        break;
                    }
                case KioskGateStatusCmd.TurnOffAllStatus:
                    {
                        int data = 0;
                        result.DataStream = ASCIIEncoding.ASCII.GetBytes(string.Format("{0:X2}", data));

                        break;
                    }
                case KioskGateStatusCmd.BlueLightOn:
                    {
                        int data = ConvertGateStatusToByte(para) | (byte)KioskGateCmd.BlueLight;
                        result.DataStream = ASCIIEncoding.ASCII.GetBytes(string.Format("{0:X2}", data));
                        break;
                    }
                case KioskGateStatusCmd.GreenLightOn:
                    {
                        int data = ConvertGateStatusToByte(para) | (byte)KioskGateCmd.GreenLight;
                        result.DataStream = ASCIIEncoding.ASCII.GetBytes(string.Format("{0:X2}", data));
                        break;
                    }
                case KioskGateStatusCmd.FrontUnLockOn:
                    {
                        int data = ConvertGateStatusToByte(para) | (byte)KioskGateCmd.FrontGateLock;
                        result.DataStream = ASCIIEncoding.ASCII.GetBytes(string.Format("{0:X2}", data));
                        break;
                    }
                case KioskGateStatusCmd.BackUnLockOn:
                    {
                        int data = ConvertGateStatusToByte(para) | (byte)KioskGateCmd.BackGateLock;
                        result.DataStream = ASCIIEncoding.ASCII.GetBytes(string.Format("{0:X2}", data));
                        break;
                    }
                case KioskGateStatusCmd.OnCharge:
                    {
                        int data = ConvertGateStatusToByte(para) | (byte)KioskGateCmd.OnChage;
                        result.DataStream = ASCIIEncoding.ASCII.GetBytes(string.Format("{0:X2}", data));
                        break;
                    }
                case KioskGateStatusCmd.BlueLightOff:
                    {
                        int data = ConvertGateStatusToByte(para) & (0xFF - (byte)KioskGateCmd.BlueLight);
                        result.DataStream = ASCIIEncoding.ASCII.GetBytes(string.Format("{0:X2}", data));
                        break;
                    }
                case KioskGateStatusCmd.GreenLightOff:
                    {
                        int data = ConvertGateStatusToByte(para) & (0xFF - (byte)KioskGateCmd.GreenLight);
                        result.DataStream = ASCIIEncoding.ASCII.GetBytes(string.Format("{0:X2}", data));
                        break;
                    }
                case KioskGateStatusCmd.FrontUnLockOff:
                    {
                        int data = ConvertGateStatusToByte(para) & (0xFF - (byte)KioskGateCmd.FrontGateLock);
                        result.DataStream = ASCIIEncoding.ASCII.GetBytes(string.Format("{0:X2}", data));
                        break;
                    }
                case KioskGateStatusCmd.BackUnLockOff:
                    {
                        int data = ConvertGateStatusToByte(para) & (0xFF - (byte)KioskGateCmd.BackGateLock);
                        result.DataStream = ASCIIEncoding.ASCII.GetBytes(string.Format("{0:X2}", data));
                        break;
                    }
                case KioskGateStatusCmd.OffCharge:
                    {
                        int data = ConvertGateStatusToByte(para) & (0xFF - (byte)KioskGateCmd.OnChage);
                        result.DataStream = ASCIIEncoding.ASCII.GetBytes(string.Format("{0:X2}", data));
                        break;
                    }
                default:
                    {
                        result = null;
                        break;
                    }
            }

            return result;
        }

        public object ParserData(byte[] rawData)
        {
            object data = null;
            if (rawData != null)
            {
                if (rawData.Length == 0)
                {
                    data = true;
                }
                else if (rawData.Length == 4)
                {
                    string dataACSII = ASCIIEncoding.ASCII.GetString(rawData);

                    byte DO = Convert.ToByte(dataACSII.Substring(0, 2), 16);
                    byte DI = Convert.ToByte(dataACSII.Substring(2, 2), 16);

                    data = ConvertByteToGateStatus(DO, DI);
                }
            }

            return data;
        }

        private int ConvertGateStatusToByte(GateStatusInfo para)
        {
            return (int)(ConvertBooleanToByte(para.IsBlueLightOn) | ConvertBooleanToByte(para.IsGreenLightOn) << 1
                | ConvertBooleanToByte(para.IsFrontDoorUnLock) << 2 | ConvertBooleanToByte(para.IsBackDoorUnLock) << 3
                | ConvertBooleanToByte(para.IsOnCharge) << 4);
        }

        private byte ConvertBooleanToByte(bool value)
        {
            return value ? (byte)1 : (byte)0;
        }

        private GateStatusInfo ConvertByteToGateStatus(byte DO, byte DI)
        {
            GateStatusInfo status = new GateStatusInfo();

            status.IsBlueLightOn = ((DO & (byte)KioskGateCmd.BlueLight) > 0);
            status.IsGreenLightOn = ((DO & (byte)KioskGateCmd.GreenLight) > 0);
            status.IsFrontDoorUnLock = ((DO & (byte)KioskGateCmd.FrontGateLock) > 0);
            status.IsBackDoorUnLock = ((DO & (byte)KioskGateCmd.BackGateLock) > 0);
            status.IsOnCharge = ((DO & (byte)KioskGateCmd.OnChage) > 0);

            status.IsFrontDoorClose = ((DI & (byte)0x01) > 0);
            status.IsBatterySensoredLock = ((DI & (byte)0x02) > 0);
            status.IsBatteryInserted = ((DI & (byte)0x04) > 0);
            status.IsFanRun = ((DI & (byte)0x08) > 0); 

            return status;
        }
    }
}
