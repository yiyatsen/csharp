﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskLib.InfoModel
{
    public class GateStatusInfo
    {
        public bool IsBlueLightOn { get; set; }
        public bool IsGreenLightOn { get; set; }
        public bool IsFrontDoorUnLock { get; set; }
        public bool IsBackDoorUnLock { get; set; }
        public bool IsOnCharge { get; set; }

        public bool IsFrontDoorClose { get; set; }
        public bool IsBatterySensoredLock { get; set; }
        public bool IsBatteryInserted { get; set; }
        public bool IsFanRun { get; set; }
    }
}
