﻿using GTE.Command;
using GTE.DeivceConfiguration;
using GTE.Device;
using GTE.Encapsulation;
using GTE.InfoModel;
using GTE.Module;
using GTE.Protocol;
using GTEKioskLib.Command;
using GTEKioskLib.DeivceConfiguration;
using GTEKioskLib.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEKioskLib
{
    public class KioskAPI : DeviceAPI
    {
        private static KioskAPI mKiosk { get; set; }
        public static KioskAPI Kiosk { get { if (mKiosk == null) { mKiosk = new KioskAPI(); } return mKiosk; } }

        #region Property
        protected ProtocolAPI mProtocol { get; set; }


        protected BatteryConfigAPI mBatteryConfigInfo { get; set; }

        protected EncapsulationAPI mBatteryEncapsulation { get; set; }

        protected EncapsulationAPI mDCONEncapsulation = new EncapsulationDCON();

        protected GateConfigAPI mGateConfigInfo = new GateConfigAPI();
        protected DCONConfigAPI mModule7065 = new DCONConfig7065();
        protected DCONConfigAPI mModule7521 = new DCONConfig7521();
        #endregion

        private KioskAPI()
        {
            mProtocol = new ProtocolCOM(); //Default
            mBatteryEncapsulation = new EncapsulationMP();
        }

        #region Protocol Method
        public override bool Connect(string port, int baudRate)
        {
            bool isSuccess = false;
            if (mProtocol != null)
            {
                Disconnect();
                isSuccess = mProtocol.Connect(port, baudRate);
            }
            return isSuccess;
        }

        public override bool Disconnect()
        {
            if (mProtocol != null)
            {
                return mProtocol.Disconnect();
            }
            return true;
        }

        public override bool IsConnect()
        {
            if (mProtocol != null)
            {
                return mProtocol.IsConnect();
            }
            return false;
        }

        public override string GetPort()
        {
            if (mProtocol != null)
            {
                return mProtocol.GetPort();
            }
            return null;
        }

        public override int GetBaudRate()
        {
            if (mProtocol != null)
            {
                return mProtocol.GetBaudRate();
            }

            return 0;
        }

        public int SetBaudRate(int baudRate)
        {
            if (mProtocol != null)
            {
                return mProtocol.SetBaudRate(baudRate);
            }

            return 0;
        }
        #endregion
        #region Encapsulation Method
        public void SetBatteryConfig(KioskBatteryType batteryType)
        {
            switch (batteryType)
            {
                case KioskBatteryType.MPM:
                    {
                        mBatteryConfigInfo = new BatteryConfigMPM(batteryType.ToString());
                        break;
                    }
                case KioskBatteryType.MPS:
                    {
                        mBatteryConfigInfo = new BatteryConfigMPS(batteryType.ToString());
                        break;
                    }
            }

            if (mBatteryConfigInfo != null)
            {
                mBatteryConfigInfo.InitialzeOtherInfo();
            }
        }


        private DCONConfigAPI SetDCONConfig(KioskModuleType type)
        {
            DCONConfigAPI moduleConfig = null;

            switch (type)
            {
                case KioskModuleType.Battery:
                    {
                        moduleConfig = mModule7521;
                        break;
                    }
                case KioskModuleType.Gate:
                    {
                        moduleConfig = mModule7065;
                        break;
                    }
            }

            return moduleConfig;
        }
        #endregion
        #region Message Method
        public object SendModuleCmdByAddress(KioskModuleType type, DCONCmd cmd, int address, SendInfo para)
        {
            object result = null;

            DCONConfigAPI moduleConfig = SetDCONConfig(type);

            DCONCmdInfo cmdInfo = moduleConfig.ResetCmdInfo(cmd, address, para);

            SendInfo sendInfo = mDCONEncapsulation.DataEncapsulation(cmdInfo);
            if (sendInfo != null)
            {
                byte[] receivedData = mProtocol.SendData(sendInfo);
                if (receivedData != null)
                {
                    byte[] parserData = mDCONEncapsulation.DataDecapsulation(receivedData);

                    result = moduleConfig.ParserData(cmd, parserData);
                }
            }
            return result;
        }

        public object SendGateCmdByAddress(KioskGateStatusCmd cmd, int address, GateStatusInfo status)
        {
            object result = null;

            SendInfo cmdInfo = mGateConfigInfo.ResetCmdInfo(cmd, address, status);
            if (cmdInfo != null)
            {
                byte[] data = (byte[])SendModuleCmdByAddress(KioskModuleType.Gate, DCONCmd.ReadDIDOStatus, address, cmdInfo);
                if (data != null)
                {
                    result = mGateConfigInfo.ParserData(data);
                }
            }
            return result;
        }

        public object SendBatteryCmdByAddress(BatteryCmd api, int address)
        {
            return SendBatteryCmdByAddress(api, 0x00, null, address);
        }
        public object SendBatteryCmdByAddress(BatteryCmd api, byte param, int address)
        {
            return SendBatteryCmdByAddress(api, param, null, address);
        }
        public object SendBatteryCmdByAddress(BatteryCmd api, byte param, object value, int address)
        {
            object result = null;

            BatteryCmdInfo cmdInfo = mBatteryConfigInfo.GetGTECmdInfo(api);
            // Setting Data
            cmdInfo = mBatteryConfigInfo.ResetCmdInfo(cmdInfo, param, value);
            SendInfo sendData = mBatteryEncapsulation.DataEncapsulation(cmdInfo);
            if (sendData != null)
            {
                //byte[] receivedData = mProtocol.SendData(sendData);
                byte[] receivedData = (byte[])SendModuleCmdByAddress(KioskModuleType.Battery, DCONCmd.ReadBattery, address, sendData);
                if (receivedData != null)
                {
                    byte[] parserData = mBatteryEncapsulation.DataDecapsulation(receivedData);

                    result = mBatteryConfigInfo.ParserData(parserData);
                }
            }
            return result;
        }
        #endregion

        #region Battery
        public EncapsulationAPI CheckBatteryVersion(int address)
        {
            EncapsulationAPI result = null;
            if (IsConnect())
            {
                //var values = Enum.GetValues(typeof(EncapsulationType));// 需要加入可以設定判斷的順序
                //foreach (EncapsulationType type in values) //針對不同類型連線
                //{
                    //SetEncapsulationType(type);

                    object value = SendBatteryCmdByAddress(BatteryCmd.CMD_DEVICE_NAME_GET, address);
                    if (value != null)
                    {
                        string deviceType = value.ToString();
                        value = SendBatteryCmdByAddress(BatteryCmd.CMD_MCU_VERSION_GET, address);
                        if (value != null)
                        {
                            mBatteryEncapsulation.SetEncryptionAlgorithm(value.ToString());

                            BatteryCmdInfo cmdInfo = mBatteryConfigInfo.GetGTECmdInfo(BatteryCmd.CMD_CIPHER_CARRIER);
                            if (cmdInfo != null)
                            {
                                mBatteryEncapsulation.SetEncryptionCode(cmdInfo.CmdCode);
                            }
                            result = mBatteryEncapsulation;
                            //break;
                        }
                    }
                    //else
                    //{
                    //    switch (type)
                    //    {
                    //        case EncapsulationType.VVersion:
                    //            {
                    //                RestMPCmd();
                    //                break;
                    //            }
                    //    }
                    //}
                //}
            }

            return result;
        }

        ///// <summary>
        ///// 用於偵測非加密版後Rest加密版通訊的問題
        ///// </summary>
        //private void RestMPCmd()
        //{
        //    SendInfo sendData = new SendInfo();
        //    sendData.DataStream = new byte[1];
        //    sendData.DataStream[0] = 0x03;
        //    byte[] receivedData = mProtocol.SendData(sendData);
        //}

        /// <summary>
        /// 設定資料封裝類型
        /// </summary>
        /// <param name="encapsulationType">資料封裝類型</param>
        public void SetEncapsulationType(EncapsulationType encapsulationType)
        {
            switch (encapsulationType)
            {
                case EncapsulationType.MPVersion:
                    {
                        mBatteryEncapsulation = new EncapsulationMP();
                        break;
                    }
                case EncapsulationType.VVersion:
                    {
                        mBatteryEncapsulation = new EncapsulationV();
                        break;
                    }
            }
        }

         /// <summary>
        /// 設定資料封裝類型
        /// </summary>
        /// <param name="encapsulationAPI">資料封裝類型</param>
        public void SetEncapsulationType(EncapsulationAPI encapsulationAPI)
        {
            mBatteryEncapsulation = encapsulationAPI;
        }
        #endregion
    }
}
