﻿using GTE.Command;
using GTE.DeivceConfiguration;
using GTE.Device;
using GTE.Encapsulation;
using GTE.InfoModel;
using GTE.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GTEBatteryLib
{
    public class BatteryAPI : DeviceAPI
    {
        #region Property
        private bool mIsSimulator { get; set; }
        /// <summary>
        /// 判斷是否能與電池通訊
        /// </summary>
        private bool mIsConnectToBattery { get; set; }

        private ProtocolType mConnectProtocolType { get; set; }
        private ProtocolAPI mProtocol { get; set; }

        private EncapsulationType mEncapsulationType { get; set; }
        private EncapsulationAPI mEncapsulation { get; set; }

        private BatteryConfigAPI mConfigInfo { get; set; }

        private string mMCUVer { get; set; }
        private string mEncryptionKey { get; set; }

        private int mDelayTime { get; set; }
        #endregion

        public BatteryAPI()
        {
            SetProtocolType(ProtocolType.COM);
        }

        #region Protocol Method
        /// <summary>
        /// 設定通訊類型
        /// </summary>
        /// <param name="connectType">通訊類型</param>
        public void SetProtocolType(ProtocolType connectType)
        {
            mConnectProtocolType = connectType;

            // If Protocol not empty, Release it
            // 1. Disconnect Protocol
            // 2. Release Protocol Rescoure
            DisposeProtocol();

            switch (connectType)
            {
                case ProtocolType.COM:
                    {
                        mProtocol = new ProtocolCOM();

                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }
        /// <summary>
        /// 釋放通訊
        /// </summary>
        private void DisposeProtocol()
        {
            if (mProtocol != null)
            {
                Disconnect(); // 關閉連線
                mProtocol.Dispose(); // 釋放連線物件
                mProtocol = null;
            }
        }

        public override bool Connect(string port, int baudRate)
        {
            bool isSuccess = false;
            if (mProtocol != null)
            {
                Disconnect();
                isSuccess = mProtocol.Connect(port, baudRate);
                if (isSuccess)
                {
                    var values = Enum.GetValues(typeof(EncapsulationType));// 需要加入可以設定判斷的順序

                    foreach (EncapsulationType type in values) //針對不同類型連線
                    {
                        SetConnectDelayTime();

                        SetEncapsulationType(type);

                        isSuccess = InitBatteryVersion();

                        OnConnectingDeviceEvent(type, GetPort(), GetBaudRate(), isSuccess, GetVersion());

                        if (isSuccess)
                        {
                            break;
                        }
                    }
                }
            }
            mIsConnectToBattery = isSuccess;
            return isSuccess;
        }

        public override bool Connect(string port, int baudRate, EncapsulationType[] encapsulationType)
        {
            bool isSuccess = false;
            if (mProtocol != null)
            {
                Disconnect();
                isSuccess = mProtocol.Connect(port, baudRate);
                if (isSuccess)
                {
                    var values = Enum.GetValues(typeof(EncapsulationType));// 需要加入可以設定判斷的順序
                    if (encapsulationType != null)
                    {
                        values = encapsulationType;
                    }

                    foreach (EncapsulationType type in values) //針對不同類型連線
                    {
                        SetConnectDelayTime();

                        SetEncapsulationType(type);

                        isSuccess = InitBatteryVersion();

                        OnConnectingDeviceEvent(type, GetPort(), GetBaudRate(), isSuccess, GetVersion());

                        if (isSuccess)
                        {
                            break;
                        }
                    }
                }
            }
            mIsConnectToBattery = isSuccess;
            return isSuccess;
        }

        public override bool Disconnect()
        {
            mIsConnectToBattery = false;
            mMCUVer = null;
            if (mProtocol != null)
            {
                return mProtocol.Disconnect();
            }
            return true;
        }

        public override bool IsConnect()
        {
            if (mProtocol != null)
            {
                return (mProtocol.IsConnect() && mIsConnectToBattery);
            }
            return false;
        }

        public override string GetPort()
        {
            if (mProtocol != null)
            {
                return mProtocol.GetPort();
            }
            return null;
        }

        public override int GetBaudRate()
        {
            if (mProtocol != null)
            {
                return mProtocol.GetBaudRate();
            }

            return 0;
        }

        public void SetConnectDelayTime()
        {
            if (GetBaudRate() > 38400)
            {
                mDelayTime = 1;
            }
            else
            {
                mDelayTime = 2;
            }
        }
        #endregion

        #region Encapsulation Method
        /// <summary>
        /// 設定通訊類型
        /// </summary>
        /// <param name="connectType">通訊類型</param>
        /// <param name="isSimulator">是否為模擬裝置</param>
        public void SetEncapsulationType(EncapsulationType encapsulationType)
        {
            mEncapsulationType = encapsulationType;

            switch (mEncapsulationType)
            {
                case EncapsulationType.MPVersion:
                    {
                        mEncapsulation = new EncapsulationMP();
                        SetBatteryConfigInfo("MPM"); //預設連結方式
                        break;
                    }
                case EncapsulationType.VVersion:
                    {
                        mEncapsulation = new EncapsulationV();
                        SetBatteryConfigInfo("V");//預設連結方式
                        break;
                    }
                default:
                    {
                        mEncapsulation = null;
                        break;
                    }
            }
        }
        /// <summary>
        /// 設定電池設定資訊
        /// </summary>
        /// <param name="deviceVer">裝置版本</param>
        public void SetBatteryConfigInfo(string deviceVer)
        {
            if (deviceVer != null && !deviceVer.Equals(""))
            {
                switch (mEncapsulationType)
                {
                    case EncapsulationType.MPVersion:
                        {
                            Regex regex = new Regex(@"^MPS");
                            if (regex.IsMatch(deviceVer))
                            {
                                mConfigInfo = new BatteryConfigMPS(deviceVer); // 4816                    
                            }
                            else
                            {
                                mConfigInfo = new BatteryConfigMPM(deviceVer); // 4840
                            }
                            break;
                        }
                    case EncapsulationType.VVersion:
                        {
                            mConfigInfo = new BatteryConfigMPM(deviceVer); // MPM

                            break;
                        }
                    default:
                        {
                            mConfigInfo = null;
                            break;
                        }
                }
            }
            else
            {
                mConfigInfo = null;
            }
        }
        /// <summary>
        /// 連線後初始化電池資訊
        /// </summary>
        /// <returns></returns>
        private bool InitBatteryVersion()
        {
            bool isSuccess = false;

            object value = SendBatteryCmd(BatteryCmd.CMD_CIPHER_UUID_GET);
            value = SendBatteryCmd(BatteryCmd.CMD_DEVICE_NAME_GET);
            if (value != null)
            {
                string deviceType = value.ToString();
                value = SendBatteryCmd(BatteryCmd.CMD_MCU_VERSION_GET);
                if (value != null)
                {
                    mMCUVer = value.ToString();

                    switch (mEncapsulationType)
                    {
                        case EncapsulationType.MPVersion:
                            {
                                //針對元智版本特製//
                                Regex regex = new Regex(@"^V");
                                if (regex.IsMatch(mMCUVer))
                                {
                                    mMCUVer = "MPM" + mMCUVer;
                                }
                                ///////////////////////
                                SetBatteryConfigInfo(mMCUVer);
                                break;
                            }
                        case EncapsulationType.VVersion:
                            {
                                Regex regex = new Regex(@"^V");
                                if (!regex.IsMatch(mMCUVer))
                                {
                                    mMCUVer = "V" + mMCUVer;
                                }
                                SetBatteryConfigInfo(mMCUVer);
                                break;
                            }
                    }

                    mEncapsulation.SetEncryptionAlgorithm(mMCUVer);

                    if (mConfigInfo != null)
                    {
                        mConfigInfo.InitialzeOtherInfo();

                        BatteryCmdInfo cmdInfo = mConfigInfo.GetGTECmdInfo(BatteryCmd.CMD_CIPHER_CARRIER);
                        if (cmdInfo != null)
                        {
                            mEncapsulation.SetEncryptionCode(cmdInfo.CmdCode);
                        }
                        isSuccess = true;
                    }
                }
            }
            else
            {
                switch (mEncapsulationType)
                {
                    case EncapsulationType.VVersion:
                        {
                            RestMPCmd();
                            break;
                        }
                }
            }
            return isSuccess;
        }
        #endregion

        #region Message Method
        /// <summary>
        /// 更新MCU EEP資料
        /// </summary>
        /// <returns></returns>
        public bool RefreshMCUEEP()
        {
            bool isSuccess = false;
            List<byte> tempData = new List<byte>();

            object result = SendBatteryCmd(BatteryCmd.CMD_MCU_EEPROM_H_GET);
            isSuccess = (result != null);
            if (result != null)
            {
                tempData.AddRange((byte[])result);
            }

            result = SendBatteryCmd(BatteryCmd.CMD_MCU_EEPROM_L_GET);
            isSuccess = (result != null);
            if (result != null)
            {
                tempData.AddRange((byte[])result);
            }

            if (mConfigInfo != null && tempData.Count > 0)
            {
                mConfigInfo.SetMCUEEP(tempData.ToArray());
                isSuccess = true;
            }
            else
            {
                isSuccess = false;
            }

            return isSuccess;
        }
        /// <summary>
        /// 更新Protect Board EEP資料
        /// </summary>
        /// <returns></returns>
        public bool RefreshPBEEP()
        {
            bool isSuccess = false;
            byte[][] PBEEP = new byte[2][];

            byte[] tempData = null;

            object result = SendBatteryCmd(BatteryCmd.CMD_OZ1_EEPROM_GET);
            isSuccess = (result != null);
            if (result != null)
            {
                tempData = (byte[])result;
                PBEEP[0] = new byte[tempData.Length];
                Array.Copy(tempData, 0, PBEEP[0], 0, tempData.Length);
            }
            result = SendBatteryCmd(BatteryCmd.CMD_OZ2_EEPROM_GET);
            isSuccess = isSuccess && (result != null);
            if (result != null)
            {
                tempData = (byte[])result;
                PBEEP[1] = new byte[tempData.Length];
                Array.Copy(tempData, 0, PBEEP[1], 0, tempData.Length);
            }

            if (mConfigInfo != null)
            {
                mConfigInfo.SetPBEEP((byte[][])PBEEP.Clone());
                isSuccess = true;
            }
            else
            {
                isSuccess = false;
            }
            return isSuccess;
        }
        /// <summary>
        /// 設定MCU EEP位置的值
        /// </summary>
        /// <param name="mcuAttr"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool SetMCUEEPCmd(BatteryEEPAttrCmd mcuAttr, object value)
        {
            bool isSuccess = false;
            if (mConfigInfo != null)
            {
                BatteryAttrAddressInfo attrAddressInfo = mConfigInfo.SetMCUAttrInfo(mcuAttr, value);
                if (attrAddressInfo != null)
                {
                    isSuccess = true;
                    for (int i = 0; i < attrAddressInfo.Size; i++)
                    {
                        object result =
                            SendBatteryCmd(BatteryCmd.CMD_MCUEEP_SET, (byte)(attrAddressInfo.Location + i), attrAddressInfo.OutputData[i], 2);

                        isSuccess = (isSuccess && (result != null));
                    }
                }
            }
            return isSuccess;
        }
        /// <summary>
        /// 取得MCUEEP資料
        /// </summary>
        /// <param name="mcuAttr"></param>
        /// <returns></returns>
        public object GETMCUEEPData(BatteryEEPAttrCmd mcuAttr)
        {
            object result = null;

            if (mConfigInfo != null)
            {
                return mConfigInfo.GetMCUAttrInfo(mcuAttr);
            }

            return result;
        }
        /// <summary>
        /// 設定Protect Board EEP資料
        /// </summary>
        /// <param name="mcuAttr"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool SetPBEEPData(BatteryEEPAttrCmd pbeepAttr, int pbIndex, object data)
        {
            bool isSuccess = false;
            if (mConfigInfo != null)
            {
                BatteryAttrAddressInfo attrAddressInfo = mConfigInfo.SetPBAttrInfo(pbeepAttr, data);
                if (attrAddressInfo != null)
                {
                    isSuccess = true;
                    BatteryCmd cmd = BatteryCmd.CMD_OZ1_BYTE_EEPROM_SET;
                    if (pbIndex == 1)
                    {
                        cmd = BatteryCmd.CMD_OZ2_BYTE_EEPROM_SET;
                    }
                    for (int i = 0; i < attrAddressInfo.Size; i++)
                    {
                        object result =
                            SendBatteryCmd(cmd, (byte)(attrAddressInfo.Location + i), attrAddressInfo.OutputData[i], 2);

                        isSuccess = (isSuccess && (result != null));
                    }
                }
            }
            return isSuccess;
        }
        /// <summary>
        /// 取得Protect Board EEP資料
        /// </summary>
        /// <param name="pbAttr"></param>
        /// <param name="pbIndex"></param>
        /// <returns></returns>
        public object GetPBEEPData(BatteryEEPAttrCmd pbAttr, int pbIndex)
        {
            object result = null;

            if (mConfigInfo != null)
            {
                return mConfigInfo.GetPBAttrInfo(pbAttr, pbIndex);
            }

            return result;
        }

        public object GetPBOPEEPData(BatteryEEPAttrCmd pbAttr, int pbIndex)
        {
            object result = null;
            BatteryEEPAttrInfo attrAddressInfo = mConfigInfo.GetPBEEPAttrInfo(pbAttr);
            if (attrAddressInfo != null)
            {
                BatteryCmd cmd = BatteryCmd.CMD_OZ1_BYTE_OP_GET;
                if(pbIndex == 1){
                    cmd = BatteryCmd.CMD_OZ2_BYTE_OP_GET;
                }

                switch(pbAttr){
                    case BatteryEEPAttrCmd.OVTH:
                    case BatteryEEPAttrCmd.OVRelease:{
                        object tempValue1 =
                            SendBatteryCmd(cmd, (byte)(attrAddressInfo.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME].Location ));
                        object tempValue2 =
                            SendBatteryCmd(cmd, (byte)(attrAddressInfo.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME].Location + 1));
                        if (tempValue1 != null && tempValue2 != null)
                        {
                            result = ((((byte)tempValue2 << 5) | ((byte)tempValue1 >> 3)) * 5000 / 4096);
                        }
                        break;
                    }
                }

            }

            return result;
        }

        public int GetPBNumber()
        {
            return mConfigInfo.GetPBNumber();
        }

        public object SendBatteryCmd(BatteryCmd api)
        {
            return SendBatteryCmd(api, 0x00, null, 3);
        }
        public object SendBatteryCmd(BatteryCmd api, int retryTime)
        {
            return SendBatteryCmd(api, 0x00, null, retryTime);
        }
        public object SendBatteryCmd(BatteryCmd api, byte param)
        {
            return SendBatteryCmd(api, param, null, 3);
        }
        public object SendBatteryCmd(BatteryCmd api, byte param, object value, int retryTime)
        {
            object result = null;

            if (mProtocol != null && mProtocol.IsConnect() && mConfigInfo != null && mEncapsulation != null) //判斷是否開啟通訊連線
            {
                BatteryCmdInfo cmdInfo = mConfigInfo.GetGTECmdInfo(api);
                if (cmdInfo != null)
                {
                    // Setting Data
                    cmdInfo = mConfigInfo.ResetCmdInfo(cmdInfo, param, value);
                    SendInfo sendData = mEncapsulation.DataEncapsulation(cmdInfo);
                    sendData.DelayTime = sendData.DelayTime * mDelayTime; // 如果BaudRate比較慢延長等待時間
                    if (sendData != null)
                    {
                        OnSendDataEvent(GetPort(), api, sendData.DataStream); // 送出前的資料的觸發事件
                        try
                        {
                            for (int i = 0; i < retryTime; i++)
                            { // 針對指令傳送嘗試三次
                                byte[] receivedData = mProtocol.SendData(sendData);
                                if (receivedData != null)
                                {
                                    OnReceivedDataEvent(GetPort(), api, receivedData);// 收到後的資料的觸發事件

                                    byte[] parserData = mEncapsulation.DataDecapsulation(receivedData);

                                    result = mConfigInfo.ParserData(parserData);

                                    break;
                                }
                            }
                        }
                        catch
                        {
                        }
                    }
                }
                else
                {
                    // No Implment
                }
            }

            return result;

        }

        public object SendCmd(BatteryCmdInfo cmdInfo)
        {
            object result = null;

            if (cmdInfo != null)
            {
                SendInfo sendData = mEncapsulation.DataEncapsulation(cmdInfo);
                sendData.DelayTime = sendData.DelayTime * mDelayTime; // 如果BaudRate比較慢延長等待時間
                if (sendData != null)
                {
                    OnSendDataEvent(GetPort(), cmdInfo.Cmd, sendData.DataStream); // 送出前的資料的觸發事件
                    try
                    {
                        byte[] receivedData = null;

                        if (cmdInfo.Cmd == BatteryCmd.CMD_GTE_DEBUGMSG)
                        {
                            receivedData = mProtocol.SendDebugData(sendData);
                            if (receivedData != null)
                            {
                                OnReceivedDataEvent(GetPort(), cmdInfo.Cmd, receivedData);// 收到後的資料的觸發事件
                                result = receivedData;
                            }
                        }
                        else
                        {
                            receivedData = mProtocol.SendData(sendData);
                            if (receivedData != null)
                            {
                                OnReceivedDataEvent(GetPort(), cmdInfo.Cmd, receivedData);// 收到後的資料的觸發事件

                                byte[] parserData = mEncapsulation.DataDecapsulation(receivedData);

                                result = mConfigInfo.ParserData(parserData);
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }

            return result;
        }

        public object ParserData(byte[] data)
        {
            object result = null;
            if (data != null)
            {
                try
                {
                    byte[] parserData = mEncapsulation.DataDecapsulation(data);

                    result = mConfigInfo.ParserData(parserData);
                }
                catch
                {

                }
            }
            return result;
        }

        /// <summary>
        /// 用於偵測非加密版後Rest加密版通訊的問題
        /// </summary>
        private void RestMPCmd()
        {
            SendInfo sendData = new SendInfo();
            sendData.DataStream = new byte[1];
            sendData.DataStream[0] = 0x03;
            byte[] receivedData = mProtocol.SendData(sendData);
        }

        public string GetVersion()
        {
            return mMCUVer;
        }

        public List<BatteryCmdInfo> GetAllBatteryCmdInfo()
        {
            if (mConfigInfo != null)
            {
                return mConfigInfo.GetAllBatteryCmdInfo();
            }
            return null;
        }

        public void EnableEncryption(bool enableEncryption)
        {
            if (mEncapsulation != null)
            {
                mEncapsulation.EnableEncryption(enableEncryption);
            }
        }
        #endregion

        #region Event
        public delegate void GTEConnectDeviceHandler(EncapsulationType type, string port, int baudrate, bool isSuccess, string version);
        /// <summary>
        /// 連接電池資訊之事件
        /// </summary>
        public event GTEConnectDeviceHandler ConnectingDeviceEvent;
        /// <summary>
        /// 觸發連接電池資訊之事件
        /// </summary>
        /// <param name="type"></param>
        /// <param name="port"></param>
        /// <param name="baudrate"></param>
        /// <param name="isSuccess"></param>
        /// <param name="version"></param>
        protected void OnConnectingDeviceEvent(EncapsulationType type, string port, int baudrate, bool isSuccess, string version)
        {
            if (ConnectingDeviceEvent != null)
            {
                ConnectingDeviceEvent(type, port, baudrate, isSuccess, version);
            }
        }

        public delegate void GTEDataHandler(string port, BatteryCmd cmdType, object data);
        /// <summary>
        /// 裝置傳送封裝資料前之事件
        /// </summary>
        public event GTEDataHandler SendDataEvent;
        /// <summary>
        /// 裝置接收封裝資料後之事件
        /// </summary>
        public event GTEDataHandler ReceivedDataEvent;
        /// <summary>
        /// 裝置剖析封裝資料後之事件
        /// </summary>
        public event GTEDataHandler ReceivedMessageEvent;
        /// <summary>
        /// 觸發裝置傳送封裝資料前之事件
        /// </summary>
        /// <param name="port"></param>
        /// <param name="cmdType"></param>
        /// <param name="data"></param>
        protected void OnSendDataEvent(string port, BatteryCmd cmdType, byte[] data)
        {
            if (SendDataEvent != null)
            {
                SendDataEvent(port, cmdType, data);
            }
        }
        /// <summary>
        /// 觸發裝置接收封裝資料後之事件
        /// </summary>
        /// <param name="port"></param>
        /// <param name="cmdType"></param>
        /// <param name="data"></param>
        protected void OnReceivedDataEvent(string port, BatteryCmd cmdType, byte[] data)
        {
            if (ReceivedDataEvent != null)
            {
                ReceivedDataEvent(port, cmdType, data);
            }
        }
        /// <summary>
        /// 觸發裝置剖析封裝資料後之事件
        /// </summary>
        /// <param name="port"></param>
        /// <param name="cmdType"></param>
        /// <param name="data"></param>
        protected void OnReceivedMessageEvent(string port, BatteryCmd cmdType, object data)
        {
            if (ReceivedMessageEvent != null)
            {
                ReceivedMessageEvent(port, cmdType, data);
            }
        }
        #endregion
    }
}
