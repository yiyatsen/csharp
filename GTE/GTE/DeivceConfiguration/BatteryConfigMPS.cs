﻿using GTE.Command;
using GTE.Encapsulation;
using GTE.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.DeivceConfiguration
{
    public class BatteryConfigMPS : BatteryConfigAPI
    {
        public BatteryConfigMPS(string deviceVer)
            : base(deviceVer)
        {
        }

        protected override void InitializeCmdInfo()
        {
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCU_EEPROM_H_GET, 500));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCU_EEPROM_L_GET, 500));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_VOLTAGE_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CURRENT_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_REMAIN_CAPACITY_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_RSOC_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CYCLE_COUNT_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_TEMP_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_PROTECTION_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_DESIGN_CAPACITY_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_DESIGN_VOLTAGE_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_FULL_CC_GET));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MANUFACTURE_DATA_GET, 200));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_EEP_VERSION_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_BATTERY_NO_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MANUFACTURE_DATE_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_RESET_COUNT_GET));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CELL_NUMBER_GET));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MANUFACTURE_NAME_GET));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_BARCODE_DATA_GET));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_TIMESET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_LAST_CHARGE_END));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_LAST_DISCHARGE_END));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_LAST_CHARGE_START));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_LAST_DISCHARGE_START));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_PID_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_PID_SET, 0x06, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CID_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CID_SET, 0x06, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OID_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OID_SET, 0x06, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_UID_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_UID_SET, 0x06, false));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_LOCK_STATUS_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_LOCK_STATUS_SET, 0x02, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CAPACITY_REC_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CAPACITY_REC_CLR, 0x02, false));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CAPACITY_CHARGING_GET));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCU_OPTION_BYTE_SET, 0x12, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCU_OPTION_BYTE_GET));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCUEEP_SET, 0x04, false)); // Fixed by Neil 2016.01.06

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCUEEP_GET, 0x03, true));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SET_YEAR, 0x02, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SET_MONTH, 0x02, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SET_DAY, 0x02, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SET_HOUR, 0x02, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SET_MIN, 0x02, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SET_SEC, 0x02, false));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_RESET_BATTERY_MCU));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_RESET_PBPOWER));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CIPHER_CARRIER));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CIPHER_UUID_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CIPHER_ACCEPT));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CIPHER_KEY_RENEW, 0x02, false));

            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ1_EEPROM_GET));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ2_EEPROM_GET));

            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CURRENT_GAIN));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CURRENT_OFFSET));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_EDV2_TIME));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_TAPER_CURRENT_GET));

            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CHEMISTRY_GET));

            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SIMULATION_FCC_GET));

            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_YEAR));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_MONTH));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_DAY));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_HOURS));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_MIN));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_SEC));

            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_RTC_REG_DUMP));

            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_CHARGE_LOG_00_22));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_CHARGE_LOG_23_45));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_CHARGE_LOG_46_49));

            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_FUNC_STATUS_GET));

            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCU_FLAG_GET));

            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCU_FLASH_PROGRAM));

            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_ALL_OZ_RESET_SET));

            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ1_BYTE_EEPROM_GET));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ2_BYTE_EEPROM_GET));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ1_BYTE_OP_GET));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ2_BYTE_OP_GET, 0x03, true));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ1_BYTE_EEPROM_SET, 0x03, false));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ1_BYTE_EEPROM_SET, 0x03, false));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ1_BYTE_OP_SET, 0x03, false));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ2_BYTE_OP_SET, 0x03, false));

            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_RESET_CYCLE));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_NRCYCLE));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SHUTDOWN_MCUOZ));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_OV_HAPPENTIME));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_UV_HAPPENTIME));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_COC_HAPPENTIME));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_SC_HAPPENTIME));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_OT_HAPPENTIME));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_UT_HAPPENTIME));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_DOC_HAPPENTIME));
        }

        protected override void InitializeMCUEEPAttrInfo()
        {
            Dictionary<string, BatteryAttrAddressInfo> attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo((0x3F << 1), 2));
            BatteryEEPAttrInfo attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.EDV2Value, attrAddInfoList);
            AddMCUEEPAttrInfo(attrInfo);

            attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(56, 1));
            attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.ManufactureInfo, attrAddInfoList);
            AddMCUEEPAttrInfo(attrInfo);

            //attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            //attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(18, 2));
            //attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.ManufactureDate, attrAddInfoList);
            //AddMCUEEPAttrInfo(attrInfo);

            //attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            //attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(57, 1));
            //attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.ManufactureWeekOfYear, attrAddInfoList);
            //AddMCUEEPAttrInfo(attrInfo);

            attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(0x03 << 1, (0x04 << 1 - 0x03 << 1)));
            attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.SerialNumber, attrAddInfoList);
            AddMCUEEPAttrInfo(attrInfo);
        }

        public override object GetMCUAttrInfo(BatteryEEPAttrCmd mcuAttr)
        {
            object data = null;
            BatteryEEPAttrInfo value = GetMCUEEPAttrInfo(mcuAttr);
            if (value != null)
            {
                switch (mcuAttr)
                {
                    case BatteryEEPAttrCmd.EDV2Value:
                        {
                            BatteryAttrAddressInfo mainInfo = value.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME];
                            byte[] tempData = new byte[mainInfo.Size];
                            for (int i = 0; i < mainInfo.Size; i++)
                            {
                                tempData[i] = mMCUEEP[mainInfo.Location + i];
                            }
                            data = (UInt16)((tempData[0] << 8) | tempData[1]);

                            break;
                        }
                }
            }

            return data;
        }

        public override BatteryAttrAddressInfo SetMCUAttrInfo(BatteryEEPAttrCmd mcuAttr, object value)
        {
            BatteryAttrAddressInfo result = null;
            try
            {
                BatteryEEPAttrInfo eepAttrInfo = GetMCUEEPAttrInfo(mcuAttr);

                if (eepAttrInfo != null && eepAttrInfo.AttrAddressInfoList.ContainsKey(BatteryAttrAddressInfo.MAINATTRIBUTENAME))
                {
                    result = eepAttrInfo.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME];
                    result.OutputData = new byte[result.Size];

                    switch (mcuAttr)
                    {
                        case BatteryEEPAttrCmd.ManufactureInfo:
                        case BatteryEEPAttrCmd.ManufactureWeekOfYear:
                            {
                                result.OutputData[0] = (byte)value;

                                break;
                            }
                        case BatteryEEPAttrCmd.ManufactureDate:
                            {
                                int[] date = (int[])value;
                                int year = (date[0] - DEFAULTYEAR);
                                UInt16 datetimeByte = (UInt16)(((year & 0x7F) << 9) | ((date[1] & 0x0F) << 5) | (date[2] & 0x1F));
                                result.OutputData[0] = (byte)(datetimeByte >> 8);
                                result.OutputData[1] = (byte)((datetimeByte & 0xFF));

                                break;
                            }
                        case BatteryEEPAttrCmd.SerialNumber:
                            {
                                UInt16 snValue = Convert.ToUInt16(value.ToString());
                                result.OutputData[0] = (byte)(snValue >> 8);
                                result.OutputData[1] = (byte)((snValue & 0xFF));

                                break;
                            }
                    }
                }
            }
            catch
            {
                result = null;
            }
            return result;
        }

        protected override void InitializePBEEPAttrInfo()
        {

        }

        public override int GetPBNumber()
        {
            int number = 0;

            if (mPBEEP != null)
            {
                foreach (byte[] eep in mPBEEP)
                {
                    if (eep != null)
                    {
                        number++;
                    }
                }
            }

            return number;
        }

        public override BatteryAttrAddressInfo SetPBAttrInfo(BatteryEEPAttrCmd mcuAttr, object value)
        {
            return null;
        }

        public override object GetPBAttrInfo(BatteryEEPAttrCmd pbAttr, int pbIndex)
        {
            object data = null;
            BatteryEEPAttrInfo value = GetPBEEPAttrInfo(pbAttr);
            if (value != null && pbIndex > -1 && pbIndex < mPBEEP.Length)
            {

            }
            return data;
        }

        public override BatteryCmdInfo ResetCmdInfo(BatteryCmdInfo cmdInfo, byte param, object value)
        {
            switch (cmdInfo.CmdCode)
            {
                case (byte)BatteryCmd.CMD_PID_SET:
                case (byte)BatteryCmd.CMD_CID_SET:
                case (byte)BatteryCmd.CMD_OID_SET:
                case (byte)BatteryCmd.CMD_UID_SET:
                    {
                        byte[] idList = BitConverter.GetBytes(Convert.ToUInt32(param));

                        cmdInfo.Info = idList;

                        break;

                    }
                case (byte)BatteryCmd.CMD_LOCK_STATUS_SET:
                    {
                        //byte lockState = 0x00;

                        //GTECmdManagement.ChargingStatus chargingStatus =
                        //    (GTECmdManagement.ChargingStatus)Enum.ToObject(typeof(GTECmdManagement.ChargingStatus), param);
                        //switch (chargingStatus)
                        //{
                        //    case GTECmdManagement.ChargingStatus.DischargingLock:
                        //    case GTECmdManagement.ChargingStatus.ChargingLock:
                        //        {
                        //            lockState |= (byte)chargingStatus;
                        //            break;
                        //        }
                        //    case GTECmdManagement.ChargingStatus.ChargingUnLock:
                        //    case GTECmdManagement.ChargingStatus.DischargingUnLock:
                        //        {
                        //            lockState &= (byte)chargingStatus;
                        //            break;
                        //        }
                        //}

                        //cmdInfo.Para = lockState;

                        break;

                    }
                case (byte)BatteryCmd.CMD_MCU_FLASH_PROGRAM:
                    {
                        //GTECmdManagement.FlashBurnType flashBurnType =
                        //    (GTECmdManagement.FlashBurnType)Enum.ToObject(typeof(GTECmdManagement.FlashBurnType), param);

                        //cmdInfo.Para = (byte)flashBurnType;
                        //switch (flashBurnType)
                        //{
                        //    // not implement flash burn
                        //    case GTECmdManagement.FlashBurnType.CMD_GO:
                        //    case GTECmdManagement.FlashBurnType.CMD_ER:
                        //        {
                        //            uint addr = (uint)value;
                        //            cmdInfo.Info = new byte[4];
                        //            cmdInfo.Info[0] = Convert.ToByte((addr >> 24) & 0x000000FF);
                        //            cmdInfo.Info[1] = Convert.ToByte((addr >> 16) & 0x000000FF);
                        //            cmdInfo.Info[2] = Convert.ToByte((addr >> 8) & 0x000000FF);
                        //            cmdInfo.Info[3] = Convert.ToByte((addr) & 0x000000FF);

                        //            break;
                        //        }
                        //    case GTECmdManagement.FlashBurnType.CMD_RM:
                        //        {
                        //            uint addr = (uint)value;
                        //            cmdInfo.Info = new byte[8];
                        //            cmdInfo.Info[0] = Convert.ToByte((addr >> 24) & 0x000000FF);
                        //            cmdInfo.Info[1] = Convert.ToByte((addr >> 16) & 0x000000FF);
                        //            cmdInfo.Info[2] = Convert.ToByte((addr >> 8) & 0x000000FF);
                        //            cmdInfo.Info[3] = Convert.ToByte((addr) & 0x000000FF);
                        //            cmdInfo.Info[4] = 0;
                        //            cmdInfo.Info[5] = 0;
                        //            cmdInfo.Info[6] = 4;
                        //            cmdInfo.Info[7] = 0;

                        //            break;
                        //        }
                        //    case GTECmdManagement.FlashBurnType.CMD_WM:
                        //        {
                        //            // need to fix
                        //            cmdInfo.Info = (byte[])value;

                        //            break;
                        //        }
                        //    case GTECmdManagement.FlashBurnType.CMD_BM:
                        //    case GTECmdManagement.FlashBurnType.CMD_BMUN:
                        //        {
                        //            // need to fix

                        //            break;
                        //        }
                        //}
                        break;
                    }
                case (byte)BatteryCmd.CMD_MCUEEP_SET:
                    {
                        cmdInfo.Para = (byte)param; // param must be a int

                        cmdInfo.Info = new byte[2];
                        cmdInfo.Info[0] = 0x01;
                        cmdInfo.Info[1] = (byte)value;
                        //cmdInfo.BaseLength = 4; // Fixed by Neil 2016.01.06

                        break;
                    }
                case (byte)BatteryCmd.CMD_MCUEEP_GET:
                    {
                        cmdInfo.Para = (byte)param; // param must be a int

                        cmdInfo.Info = new byte[1];
                        cmdInfo.Info[0] = 1;

                        break;
                    }
                case (byte)BatteryCmd.CMD_OZ1_BYTE_EEPROM_GET:
                case (byte)BatteryCmd.CMD_OZ2_BYTE_EEPROM_GET:
                case (byte)BatteryCmd.CMD_OZ1_BYTE_OP_GET:
                case (byte)BatteryCmd.CMD_OZ2_BYTE_OP_GET:
                    {
                        cmdInfo.Para = (byte)param; // param must be a int

                        break;
                    }
                case (byte)BatteryCmd.CMD_OZ1_BYTE_EEPROM_SET:
                case (byte)BatteryCmd.CMD_OZ2_BYTE_EEPROM_SET:
                case (byte)BatteryCmd.CMD_OZ1_BYTE_OP_SET:
                case (byte)BatteryCmd.CMD_OZ2_BYTE_OP_SET:
                    {
                        cmdInfo.Para = (byte)param; // param must be a int

                        cmdInfo.Info = new byte[2];
                        cmdInfo.Info[0] = (byte)value;
                        cmdInfo.Info[1] = 0xFF;

                        break;
                    }
                case (byte)BatteryCmd.CMD_SET_YEAR:
                case (byte)BatteryCmd.CMD_SET_MONTH:
                case (byte)BatteryCmd.CMD_SET_DAY:
                case (byte)BatteryCmd.CMD_SET_HOUR:
                case (byte)BatteryCmd.CMD_SET_MIN:
                case (byte)BatteryCmd.CMD_SET_SEC:
                    {
                        cmdInfo.Para = Convert.ToByte(param); // param must be a int

                        break;
                    }
                case (byte)BatteryCmd.CMD_GET_LAST_CHARGE_END:
                case (byte)BatteryCmd.CMD_GET_LAST_DISCHARGE_END:
                case (byte)BatteryCmd.CMD_GET_LAST_CHARGE_START:
                case (byte)BatteryCmd.CMD_GET_LAST_DISCHARGE_START:
                case (byte)BatteryCmd.CMD_GET_OV_HAPPENTIME:
                case (byte)BatteryCmd.CMD_GET_UV_HAPPENTIME:
                case (byte)BatteryCmd.CMD_GET_COC_HAPPENTIME:
                case (byte)BatteryCmd.CMD_GET_SC_HAPPENTIME:
                case (byte)BatteryCmd.CMD_GET_OT_HAPPENTIME:
                case (byte)BatteryCmd.CMD_GET_UT_HAPPENTIME:
                case (byte)BatteryCmd.CMD_GET_DOC_HAPPENTIME:
                    {
                        cmdInfo.Para = Convert.ToByte(param); // param must be a int

                        break;
                    }
            }
            return cmdInfo;
        }

        public override object ParserData(byte[] rawData)
        {
            object value = null;

            if (rawData != null && rawData.Length >= 3)
            {
                byte cmdLen = rawData[0];
                byte cmdPara = rawData[1];
                byte cmdCode = rawData[2];

                int infoLen = (int)(cmdLen - 2);
                byte[] info = null;
                if (infoLen > 0 && ((rawData.Length - 3) >= infoLen))
                {
                    Array.Resize(ref info, infoLen);
                    Array.Copy(rawData, 3, info, 0, infoLen);
                }
                BatteryErrorCmd errorCmd = ConvertToErrorCmd(cmdPara);

                if (errorCmd == BatteryErrorCmd.NOERROR)
                {
                    switch (cmdCode)
                    {
                        case (byte)BatteryCmd.CMD_OZ1_EEPROM_GET:
                        case (byte)BatteryCmd.CMD_OZ2_EEPROM_GET:
                        case (byte)BatteryCmd.CMD_MCU_EEPROM_H_GET:
                        case (byte)BatteryCmd.CMD_MCU_EEPROM_L_GET:
                            {
                                value = info;
                                break;
                            }
                        case (byte)BatteryCmd.CMD_MCU_OPTION_BYTE_GET:
                        case (byte)BatteryCmd.CMD_MCU_FLAG_GET:
                        case (byte)BatteryCmd.CMD_CIPHER_UUID_GET:
                        case (byte)BatteryCmd.CMD_CIPHER_ACCEPT:
                            {
                                byte[] valueList = null;
                                Array.Resize(ref valueList, info.Length);
                                Array.Copy(info, 0, valueList, 0, info.Length);
                                value = valueList;

                                break;
                            }
                        case (byte)BatteryCmd.CMD_VOLTAGE_GET:
                            {
                                int[] cellVolt = null;

                                int cellCount = Convert.ToInt16(info[4]) + Convert.ToInt16(info[5]); // total base dwon number (n66 + n88)

                                int sum = 0;
                                Array.Resize(ref cellVolt, cellCount);
                                for (int i = 0; i < cellCount; i++)
                                {
                                    cellVolt[i] = ((info[i * 2 + 6] << 8) + info[i * 2 + 7]);

                                    sum = sum + cellVolt[i];
                                }

                                value = cellVolt;
                                break;
                            }
                        case (byte)BatteryCmd.CMD_RSOC_GET:
                            {
                                value = (int)info[0];
                                break;
                            }
                        case (byte)BatteryCmd.CMD_CELL_NUMBER_GET:
                        case (byte)BatteryCmd.CMD_LOCK_STATUS_GET:
                        case (byte)BatteryCmd.CMD_GET_MONTH:
                        case (byte)BatteryCmd.CMD_GET_DAY:
                        case (byte)BatteryCmd.CMD_GET_HOURS:
                        case (byte)BatteryCmd.CMD_GET_MIN:
                        case (byte)BatteryCmd.CMD_GET_SEC:
                        case (byte)BatteryCmd.CMD_GET_1224:
                        case (byte)BatteryCmd.CMD_OZ1_BYTE_EEPROM_GET:
                        case (byte)BatteryCmd.CMD_OZ2_BYTE_EEPROM_GET:
                        case (byte)BatteryCmd.CMD_OZ1_BYTE_OP_GET:
                        case (byte)BatteryCmd.CMD_OZ2_BYTE_OP_GET:
                            {
                                value = info[0];
                                break;
                            }
                        case (byte)BatteryCmd.CMD_GET_YEAR:
                            {
                                value = info[0] + DEFAULTYEAR;
                                break;
                            }
                        case (byte)BatteryCmd.CMD_TEMP_GET:
                            {
                                double tempZero = 2731.2;
                                double tempTime = 10.0;

                                int n66 = Convert.ToInt16(info[0]);
                                int n68 = Convert.ToInt16(info[1]);
                                int cellCount = n66 + n68; // total base dwon number (n66 + n88)

                                double[] tempList = null;
                                Array.Resize(ref tempList, cellCount);

                                for (int i = 0; i < n66; i++)
                                {
                                    double currentTemp = ((info[2 + i * 2] << 8 | info[3 + i * 2]) - tempZero) / tempTime;
                                    tempList[i] = currentTemp;
                                }
                                for (int i = 0; i < n68; i++)
                                {
                                    double currentTemp = (((info[2 + (n66 + i) * 2] << 8) | (info[3 + (n66 + i) * 2])) - tempZero) / tempTime;
                                    tempList[n66 + i] = currentTemp;
                                }

                                value = tempList;
                                break;
                            }
                        case (byte)BatteryCmd.CMD_PROTECTION_GET:
                            {
                                byte[] statusList = new byte[12]; // 13 data

                                statusList[0] = (byte)((info[0] >> 0) & 0x01);//Over Voltage Protection
                                statusList[1] = (byte)(((info[0] >> 1) & 0x01) | ((info[0] >> 7) & 0x01));//Under voltage protection(cell uv & all uv)
                                statusList[2] = (byte)((info[0] >> 3) & 0x01);// Over Current Charge
                                statusList[3] = (byte)((info[0] >> 4) & 0x01);// Over Current Discharge
                                statusList[4] = (byte)(((info[0] >> 5) & 0x01) | ((info[0] >> 6) & 0x01));// Temp Discharge (OT)(dot & cot)
                                statusList[5] = (byte)0; // Temp charge (UT)
                                statusList[6] = (byte)0; // Short Circuit
                                statusList[7] = (byte)((info[0] >> 2) & 0x01); // CDS
                                statusList[8] = (byte)((info[1] >> 0) & 0x01);// Enable Charge
                                statusList[9] = (byte)((info[1] >> 1) & 0x01); // Enable Discharge
                                statusList[10] = (byte)((info[1] >> 2) & 0x01); // Discharging
                                statusList[11] = (byte)((info[1] >> 3) & 0x01);// Charging

                                value = statusList;
                                break;
                            }
                        case (byte)BatteryCmd.CMD_CURRENT_GET:
                            {
                                //有負值
                                value = (Int32)((ulong)info[0] << 24 | (ulong)info[1] << 16 | (ulong)info[2] << 8 | (ulong)info[3]);
                                break;
                            }
                        case (byte)BatteryCmd.CMD_REMAIN_CAPACITY_GET:
                        case (byte)BatteryCmd.CMD_DESIGN_CAPACITY_GET:
                        case (byte)BatteryCmd.CMD_DESIGN_VOLTAGE_GET:
                        case (byte)BatteryCmd.CMD_FULL_CC_GET:
                        case (byte)BatteryCmd.CMD_TAPER_CURRENT_GET:
                            {
                                value = ((ulong)info[0] << 24) | ((ulong)info[1] << 16) | ((ulong)info[2] << 8) | ((ulong)info[3]);
                                break;
                            }
                        case (byte)BatteryCmd.CMD_PID_GET:
                        case (byte)BatteryCmd.CMD_CID_GET:
                        case (byte)BatteryCmd.CMD_OID_GET:
                        case (byte)BatteryCmd.CMD_UID_GET:
                        case (byte)BatteryCmd.CMD_CAPACITY_REC_GET:
                        case (byte)BatteryCmd.CMD_CAPACITY_CHARGING_GET:
                        case (byte)BatteryCmd.CMD_EDV2_TIME:
                            {
                                value = ((ulong)info[0] << 24 | (ulong)info[1] << 16 | (ulong)info[2] << 8 | (ulong)info[3]);
                                break;
                            }
                        case (byte)BatteryCmd.CMD_DEVICE_NAME_GET:
                        case (byte)BatteryCmd.CMD_MANUFACTURE_DATA_GET:
                        case (byte)BatteryCmd.CMD_EEP_VERSION_GET:
                        case (byte)BatteryCmd.CMD_MCU_VERSION_GET:
                        case (byte)BatteryCmd.CMD_CHEMISTRY_GET:
                        case (byte)BatteryCmd.CMD_MANUFACTURE_NAME_GET:
                            {
                                string data = "";
                                for (int i = 0; i < (infoLen); i++)
                                    data += String.Format("{0}", (char)info[i]).ToString().Trim('\0');

                                value = data.Trim();
                                break;
                            }
                        case (byte)BatteryCmd.CMD_CYCLE_COUNT_GET:
                            {
                                value = (((ulong)info[0] << 8) + (ulong)info[1]);
                                break;
                            }
                        case (byte)BatteryCmd.CMD_BATTERY_NO_GET:
                            {
                                value = (((ulong)info[0] << 8) + (ulong)info[1]).ToString();
                                break;
                            }
                        case (byte)BatteryCmd.CMD_RESET_COUNT_GET:
                            {
                                value = (Int32)((ulong)info[0] << 8 | (ulong)info[1]);
                                break;
                            }
                        case (byte)BatteryCmd.CMD_RESET_CYCLE:
                        case (byte)BatteryCmd.CMD_GET_NRCYCLE:
                            {
                                value = Convert.ToUInt16((info[0] << 8) | info[1]);
                                break;
                            }
                        case (byte)BatteryCmd.CMD_MANUFACTURE_DATE_GET:
                            {
                                int TempDate = (ushort)info[0] << 8 | info[1];
                                int[] dataList = new int[3];

                                dataList[0] = ((TempDate >> 9) & 0x0000007F) + DEFAULTYEAR;
                                int year = DateTime.Now.Year;
                                if (year < dataList[0])
                                {//1980 Start year
                                    dataList[0] = dataList[0] - 20;
                                }
                                dataList[1] = ((TempDate >> 5) & 0x0000000F);
                                dataList[2] = TempDate & 0x0000001F;

                                value = dataList;
                                break;
                            }
                        case (byte)BatteryCmd.CMD_BARCODE_DATA_GET:
                            {
                                // not implement
                                int manufacturer = (info[0] >> 4);
                                int productClass = (info[0] & 0x0F);
                                int year = info[1];
                                int week = info[2];
                                ulong serial = (ulong)info[3] << 8 | (ulong)info[4];


                                value = string.Format("{0:0}{1:0}{2:00}{3:00}{4:0000}", manufacturer, productClass, year, week, serial);
                                break;
                            }
                        case (byte)BatteryCmd.CMD_GET_OV_HAPPENTIME:
                        case (byte)BatteryCmd.CMD_GET_UV_HAPPENTIME:
                        case (byte)BatteryCmd.CMD_GET_COC_HAPPENTIME:
                        case (byte)BatteryCmd.CMD_GET_SC_HAPPENTIME:
                        case (byte)BatteryCmd.CMD_GET_OT_HAPPENTIME:
                        case (byte)BatteryCmd.CMD_GET_UT_HAPPENTIME:
                        case (byte)BatteryCmd.CMD_GET_DOC_HAPPENTIME:
                            {
                                int[] time = new int[6];
                                time[0] = info[0] + DEFAULTYEAR; // year
                                time[1] = info[1]; // month
                                time[2] = info[2]; // day
                                time[3] = info[3]; // hour
                                time[4] = info[4]; // min
                                time[5] = info[5]; // sec
                                //time[6] = (info[6] << 8) + (info[7]); // vmin
                                //time[7] = (info[8] << 8) + (info[9]); // vmax
                                //time[8] = (info[10] << 8) + (info[11]); // error count
                                //time[9] = info[12]; // soc
                                value = time;

                                break;
                            }
                        case (byte)BatteryCmd.CMD_GET_TIMESET:
                            {
                                int[] time = new int[6];
                                time[0] = info[0] + DEFAULTYEAR; // year
                                time[1] = info[1]; // month
                                time[2] = info[2]; // day
                                time[3] = info[3]; // hour
                                time[4] = info[4]; // min
                                time[5] = info[5]; // sec
                                value = time;

                                break;
                            }
                        case (byte)BatteryCmd.CMD_GET_LAST_CHARGE_END:
                        case (byte)BatteryCmd.CMD_GET_LAST_DISCHARGE_END:
                        case (byte)BatteryCmd.CMD_GET_LAST_CHARGE_START: //4840沒有值
                        case (byte)BatteryCmd.CMD_GET_LAST_DISCHARGE_START:   //4840沒有值
                            {
                                //4816沒有當時的SOC
                                int[] time = new int[6];
                                time[0] = info[0] + DEFAULTYEAR; // year
                                time[1] = info[1]; // month
                                time[2] = info[2]; // day
                                time[3] = info[3]; // hour
                                time[4] = info[4]; // min
                                time[5] = info[5]; // sec
                                //time[6] = info[6]; // ****SOC****
                                value = time;
                                break;
                            }
                        case (byte)BatteryCmd.CMD_GET_CHARGE_LOG_00_22:
                        case (byte)BatteryCmd.CMD_GET_CHARGE_LOG_23_45:
                        case (byte)BatteryCmd.CMD_GET_CHARGE_LOG_46_49:
                            {
                                List<int[]> tempData = new List<int[]>();

                                int size = 0;
                                bool checkLastChargeTimeData = false;

                                if (cmdCode == (byte)BatteryCmd.CMD_GET_CHARGE_LOG_00_22)
                                {
                                    size = 23;
                                    checkLastChargeTimeData = true;
                                }
                                else if (cmdCode == (byte)BatteryCmd.CMD_GET_CHARGE_LOG_23_45)
                                {
                                    size = 23;
                                }
                                else if (cmdCode == (byte)BatteryCmd.CMD_GET_CHARGE_LOG_46_49)
                                {
                                    size = 4;
                                }

                                for (int i = 0; i < size; i++)
                                {
                                    int[] temp = new int[8];
                                    int date = info[i * 10] << 8 | info[i * 10 + 1];

                                    temp[0] = ((date >> 9) & 0x0000007F) + DEFAULTYEAR;//year
                                    temp[1] = ((date >> 5) & 0x0000000F); //month
                                    temp[2] = date & 0x0000001F; // day
                                    temp[3] = info[i * 10 + 2]; // hour
                                    temp[4] = info[i * 10 + 3]; // minute
                                    temp[5] = info[i * 10 + 4] << 8 | info[i * 10 + 5]; //voltstart
                                    temp[6] = info[i * 10 + 6] << 8 | info[i * 10 + 7]; //voltstop
                                    if (i == 1 && checkLastChargeTimeData)
                                    {
                                        temp[7] = (info[i * 10 + 8] << 8 | info[i * 10 + 9]) * 60; //duration
                                    }
                                    else
                                    {
                                        temp[7] = info[i * 10 + 8] << 8 | info[i * 10 + 9]; //duration
                                    }

                                    tempData.Add(temp);
                                }

                                value = tempData.ToArray();
                                break;
                            }
                        case (byte)BatteryCmd.CMD_RTC_REG_DUMP:
                            {
                                byte[] tempData = new byte[8];

                                tempData[0] = info[0];//year 
                                tempData[1] = info[1];//month
                                tempData[2] = info[2];//Date
                                tempData[3] = info[3];//day 
                                tempData[4] = info[4];//Hour
                                tempData[5] = info[5];//min
                                tempData[6] = info[6];//sec
                                tempData[7] = info[7]; //Config

                                value = tempData;

                                break;
                            }
                        case (byte)BatteryCmd.CMD_PID_SET:
                        case (byte)BatteryCmd.CMD_CID_SET:
                        case (byte)BatteryCmd.CMD_OID_SET:
                        case (byte)BatteryCmd.CMD_UID_SET:
                        case (byte)BatteryCmd.CMD_LOCK_STATUS_SET:
                        case (byte)BatteryCmd.CMD_MCUEEP_SET:
                        case (byte)BatteryCmd.CMD_SET_YEAR:
                        case (byte)BatteryCmd.CMD_SET_MONTH:
                        case (byte)BatteryCmd.CMD_SET_DAY:
                        case (byte)BatteryCmd.CMD_SET_HOUR:
                        case (byte)BatteryCmd.CMD_SET_MIN:
                        case (byte)BatteryCmd.CMD_SET_SEC:
                            {
                                value = true; // 成功回傳
                                break;
                            }
                        case (byte)BatteryCmd.CMD_CAPACITY_REC_CLR:
                        case (byte)BatteryCmd.CMD_MCU_OPTION_BYTE_SET:
                        case (byte)BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET:
                        case (byte)BatteryCmd.CMD_FUNC_STATUS_GET:
                        case (byte)BatteryCmd.CMD_ALL_OZ_RESET_SET:
                        case (byte)BatteryCmd.CMD_OZ1_BYTE_EEPROM_SET:
                        case (byte)BatteryCmd.CMD_OZ2_BYTE_EEPROM_SET:
                        case (byte)BatteryCmd.CMD_OZ1_BYTE_OP_SET:
                        case (byte)BatteryCmd.CMD_OZ2_BYTE_OP_SET:
                        case (byte)BatteryCmd.CMD_RESET_BATTERY_MCU:
                        case (byte)BatteryCmd.CMD_RESET_PBPOWER:
                        case (byte)BatteryCmd.CMD_SHUTDOWN_MCUOZ:
                            {
                                //No Use
                                break;
                            }
                        case (byte)BatteryCmd.CMD_MCUEEP_GET:
                            {
                                //int EEP1_Len, EEP2_Len;
                                //if ((Len - 2 + m_CmdParam) > 128)
                                //{//-2: para & cmd_code
                                //    EEP1_Len = 128 - Para;
                                //    EEP2_Len = Len - 2 - EEP1_Len;
                                //}
                                //else
                                //{
                                //    EEP1_Len = Len - 2;
                                //    EEP2_Len = 0;
                                //}
                                //for (int i = 0; i < EEP1_Len; i++)
                                //    m_baryMCUEE1[m_CmdParam + i] = Info[i];
                                //for (int i = 0; i < EEP2_Len; i++)
                                //    m_baryMCUEE2[i] = Info[EEP1_Len + i];
                                break;
                            }
                        case (byte)BatteryCmd.CMD_CIPHER_CARRIER:
                            {
                                break;
                            }
                        case (byte)BatteryCmd.CMD_CIPHER_KEY_RENEW:
                            {
                                // Not Implement

                                //KeyRenew();
                                //{//For Debug
                                //    string strDisp = Encoding.ASCII.GetString(LcmBattInfo[MASTER_PACK_ARRARY_ID].CipherKey);
                                //    Debug.Print("New Session Key: " + strDisp);
                                //    strDisp = null;
                                //}
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
                else
                {
                    // Error
                }
            }

            return value;
        }
    }
}
