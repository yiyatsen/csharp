﻿using GTE.Device;
using GTE.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.DeivceConfiguration
{
    public abstract class AppDeviceAdapter
    {
        #region Property
        protected int mErrorDataTime { get; set; }
        protected int mMaxErrorConnectTime { get; set; }
        #endregion
        public AppDeviceAdapter()
        {
            mMaxErrorConnectTime = 2;
        }

        #region Method
        public void SetMaxErrorConnectTime(int time)
        {
            mMaxErrorConnectTime = time;
        }
        protected object IsLegalData(object data)
        {
            if (data == null)
            {
                mErrorDataTime++;
                if (mMaxErrorConnectTime < mErrorDataTime)
                {
                    ReportMessage(ErrorType.MaxErrorConnection, ErrorLevelType.Normal, "Max Error Connection", null);
                }
            }
            return data;
        }
        #endregion

        #region Event
        public event GTE.Message.MessageEvent.ErrorMessageHandler ErrorMessageEvent;
        /// <summary>
        /// 訊息之回報
        /// </summary>
        /// <param name="messageType"></param>
        /// <param name="message"></param>
        protected void ReportMessage(ErrorType type, ErrorLevelType level, string message, string stack)
        {
            if (ErrorMessageEvent != null)
            {
                ErrorMessageEvent(type, level, message, stack);
            }
        }
        #endregion
    }
}
