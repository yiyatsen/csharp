﻿using GTE.Command;
using GTE.Encapsulation;
using GTE.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GTE.DeivceConfiguration
{
    public abstract class BatteryConfigAPI
{
        protected const int DEFAULTYEAR = 2000;

        #region Property
        protected string mDeviceVer { get; set; }
        private EncapsulationType mVersionType { get; set; }
        protected EncapsulationType VersionType { get { return mVersionType; } }
        private List<BatteryCmdInfo> mGTECmdInfoList = new List<BatteryCmdInfo>();
        private List<BatteryEEPAttrInfo> mMCUEEPInfoList = new List<BatteryEEPAttrInfo>();
        private List<BatteryEEPAttrInfo> mPBEEPInfoList = new List<BatteryEEPAttrInfo>();

        protected byte[] mMCUEEP { get; set; }
        protected byte[][] mPBEEP { get; set; }

        #endregion
        public BatteryConfigAPI(string deviceVer)
        {
            mDeviceVer = deviceVer; //針對Version設定不同的定義內容
            Regex regex = new Regex(@"^V");
            if (regex.IsMatch(deviceVer))
            {
                mVersionType = EncapsulationType.VVersion;
            }
            else
            {
                mVersionType = EncapsulationType.MPVersion;
            }

            InitializeCmdList();
        }

        /// <summary>
        /// 設定MCUEEP資料
        /// </summary>
        /// <param name="mcuEEP"></param>
        public void SetMCUEEP(byte[] mcuEEP)
        {
            mMCUEEP = mcuEEP;
        }
        /// <summary>
        /// 設定Protect Board EEP資料
        /// </summary>
        /// <param name="mcuEEP"></param>
        public void SetPBEEP(byte[][] pbEEP)
        {
            mPBEEP = pbEEP;
        }

        #region Initialze Method
        private void InitializeCmdList()
        {
            mGTECmdInfoList.Add(new BatteryCmdInfo(BatteryCmd.CMD_DEVICE_NAME_GET, false, 200));
            mGTECmdInfoList.Add(new BatteryCmdInfo(BatteryCmd.CMD_MCU_VERSION_GET, false));
        }
        public void InitialzeOtherInfo()
        {
            InitializeCmdInfo();
            InitializeMCUEEPAttrInfo();
            InitializePBEEPAttrInfo();
        }
        #endregion
        #region CmdInfo Method
        protected abstract void InitializeCmdInfo();
        protected bool AddCmdInfo(BatteryCmdInfo cmdInfo)
        {
            bool isSuccess = (GetGTECmdInfo(cmdInfo.Cmd) == null);
            if (isSuccess)
            {
                mGTECmdInfoList.Add(cmdInfo);
            }
            return isSuccess;
        }
        protected bool RemoveCmdInfo(BatteryCmdInfo cmdInfo)
        {
            return mGTECmdInfoList.Remove(cmdInfo);
        }
        public BatteryCmdInfo GetGTECmdInfo(BatteryCmd cmd)
        {
            try
            {
                return (from list in mGTECmdInfoList
                        where list.Cmd == cmd
                        select list).FirstOrDefault();
            }
            catch{}

            return null;
        }

        public List<BatteryCmdInfo> GetAllBatteryCmdInfo()
        {
            return mGTECmdInfoList;
        }
        #endregion
        #region MCUEEP Method
        protected abstract void InitializeMCUEEPAttrInfo();
        protected bool AddMCUEEPAttrInfo(BatteryEEPAttrInfo attrInfo)
        {
            bool isSuccess = (GetMCUEEPAttrInfo(attrInfo.EEPAttrCmd) == null);
            if (isSuccess)
            {
                mMCUEEPInfoList.Add(attrInfo);
            }
            return isSuccess;
        }
        protected bool RemoveMCUEEPAttrInfo(BatteryEEPAttrInfo attrInfo)
        {
            return mMCUEEPInfoList.Remove(attrInfo);
        }
        public BatteryEEPAttrInfo GetMCUEEPAttrInfo(BatteryEEPAttrCmd cmd)
        {
            try
            {
                return (from list in mMCUEEPInfoList
                        where list.EEPAttrCmd == cmd
                        select list).FirstOrDefault();
            }
            catch
            {
            }

            return null;
        }
        /// <summary>
        /// 取得MCU屬性之資訊
        /// </summary>
        /// <param name="mcuAttr"></param>
        /// <returns></returns>
        public abstract object GetMCUAttrInfo(BatteryEEPAttrCmd mcuAttr);

        public abstract BatteryAttrAddressInfo SetMCUAttrInfo(BatteryEEPAttrCmd mcuAttr, object value);
        #endregion
        #region PBEEP Method
        protected abstract void InitializePBEEPAttrInfo();
        protected bool AddPBEEPAttrInfo(BatteryEEPAttrInfo attrInfo)
        {
            bool isSuccess = (GetPBEEPAttrInfo(attrInfo.EEPAttrCmd) == null);
            if (isSuccess)
            {
                mPBEEPInfoList.Add(attrInfo);
            }
            return isSuccess;
        }
        protected bool RemovePBEEPAttrInfo(BatteryEEPAttrInfo attrInfo)
        {
            return mPBEEPInfoList.Remove(attrInfo);
        }
        public BatteryEEPAttrInfo GetPBEEPAttrInfo(BatteryEEPAttrCmd cmd)
        {
            try
            {
                return (from list in mPBEEPInfoList
                        where list.EEPAttrCmd == cmd
                        select list).FirstOrDefault();
            }
            catch
            {
            }

            return null;
        }

        public abstract int GetPBNumber();
        public abstract BatteryAttrAddressInfo SetPBAttrInfo(BatteryEEPAttrCmd mcuAttr, object value);
        /// <summary>
        /// 取得Protect Board EEP上之屬性資訊
        /// </summary>
        /// <param name="pbAttr"></param>
        /// <param name="pbIndex"></param>
        /// <returns></returns>
        public abstract object GetPBAttrInfo(BatteryEEPAttrCmd pbAttr, int pbIndex);
        #endregion
        #region Parser Data Method
        public abstract BatteryCmdInfo ResetCmdInfo(BatteryCmdInfo cmdInfo, byte param, object value);
        public abstract object ParserData(byte[] rawData);

        protected BatteryErrorCmd ConvertToErrorCmd(byte para)
        {
            BatteryErrorCmd errorCmd = BatteryErrorCmd.UNKNOW;
            switch (para)
            {
                case (byte)BatteryErrorCmd.NOERROR:
                    {
                        errorCmd = BatteryErrorCmd.NOERROR;
                        break;
                    }
                case (byte)BatteryErrorCmd.CHECKSUMERROR:
                    {
                        errorCmd = BatteryErrorCmd.CHECKSUMERROR;
                        break;
                    }
                case (byte)BatteryErrorCmd.INVALIDCMDERROR:
                    {
                        errorCmd = BatteryErrorCmd.INVALIDCMDERROR;
                        break;
                    }
                case (byte)BatteryErrorCmd.FORMATERROR:
                    {
                        errorCmd = BatteryErrorCmd.FORMATERROR;
                        break;
                    }
                case (byte)BatteryErrorCmd.DATAERROR:
                    {
                        errorCmd = BatteryErrorCmd.DATAERROR;
                        break;
                    }
                case (byte)BatteryErrorCmd.FLASHWRITIN:
                    {
                        errorCmd = BatteryErrorCmd.FLASHWRITIN;
                        break;
                    }
                case (byte)BatteryErrorCmd.INVALIDADDRESS:
                    {
                        errorCmd = BatteryErrorCmd.INVALIDADDRESS;
                        break;
                    }
                case (byte)BatteryErrorCmd.COMMUNICATEERROR:
                    {
                        errorCmd = BatteryErrorCmd.COMMUNICATEERROR;
                        break;
                    }
                default:
                    {
                        errorCmd = BatteryErrorCmd.UNKNOW;
                        break;
                    }
            }

            return errorCmd;
        }
        #endregion
    }
}
