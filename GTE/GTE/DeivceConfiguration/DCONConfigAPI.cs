﻿using GTE.Command;
using GTE.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.DeivceConfiguration
{
    public abstract class DCONConfigAPI
    {
        public abstract DCONCmdInfo ResetCmdInfo(DCONCmd cmd, int address, SendInfo para);

        public abstract object ParserData(DCONCmd cmd, byte[] rawData);
    }
}
