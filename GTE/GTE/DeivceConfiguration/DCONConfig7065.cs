﻿using GTE.Command;
using GTE.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.DeivceConfiguration
{
    public class DCONConfig7065 : DCONConfigAPI
    {
        public override DCONCmdInfo ResetCmdInfo(DCONCmd cmd, int address, SendInfo para)
        {
            DCONCmdInfo result = new DCONCmdInfo();

            switch (cmd)
            {
                case DCONCmd.ReadDIDOStatus:
                    {
                        byte[] header = ASCIIEncoding.ASCII.GetBytes(string.Format("@{0:X2}\r", address));
                        if (para != null && para.DataStream != null)
                        {
                            result.DataHeader = para.DataHeader;
                            result.DataFooter = para.DataFooter;
                            result.DelayTime = para.DelayTime;
                            result.Timeout = para.Timeout;

                            int length = para.DataStream.Length + 4;
                            result.DataStream = new byte[length];

                            result.DataStream[0] = header[0];
                            result.DataStream[1] = header[1];
                            result.DataStream[2] = header[2];
                            result.DataStream[length - 1] = header[3];

                            Array.Copy(para.DataStream, 0, result.DataStream, 3, para.DataStream.Length);
                        }
                        else
                        {
                            result.DataStream = header;
                        }

                        break;
                    }
                case DCONCmd.ReadModuleName:
                    {
                        result.DataStream = ASCIIEncoding.ASCII.GetBytes(string.Format("${0:X2}M\r", address));

                        break;
                    }
                case DCONCmd.ReadModuleStatus:
                    {
                        result.DataStream = ASCIIEncoding.ASCII.GetBytes(string.Format("~{0:X2}0\r", address));

                        break;
                    }
                default:
                    {
                        // not support
                        result = null;
                        break;
                    }
            }

            return result;
        }

        public override object ParserData(DCONCmd cmd, byte[] rawData)
        {
            object result = null;
            if (rawData != null)
            {
                switch (cmd)
                {
                    case DCONCmd.ReadDIDOStatus:
                        {
                            result = rawData;

                            break;
                        }
                    case DCONCmd.ReadModuleConfig:
                        {
                            break;
                        }
                    case DCONCmd.ReadModuleName:
                        {
                            if (rawData.Length >= 6)
                            {
                                string s = ASCIIEncoding.ASCII.GetString(rawData);
                                result = s.Substring(2, 4);
                            }

                            break;
                        }
                    case DCONCmd.ReadModuleStatus:
                        {
                            break;
                        }
                    case DCONCmd.SetModuleAddress:
                        {
                            break;
                        }
                    case DCONCmd.SetModuleBaudRate:
                        {
                            break;
                        }
                    default:
                        {
                            // not support
                            result = null;
                            break;
                        }
                }
            }

            return result;
        }
    }
}
