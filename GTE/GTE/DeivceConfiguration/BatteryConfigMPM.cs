﻿using GTE.Command;
using GTE.Encapsulation;
using GTE.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GTE.DeivceConfiguration
{
    public class BatteryConfigMPM : BatteryConfigAPI
    {
        private bool mIsVVersion = false;

        private double[] dbaryADCTable ={
			              1763,1477,1231,1019,843,
			              695, 573,473,392,325,
			              270, 225,189,158,134,
			              113,  96, 82, 70, 60,
			              52,  45, 39, 34, 30};
        private double[] dbaryDegCTable = { -30, -25, -20, -15, -10,
			                     -5,   0,   5,  10,  15,
			                    20,  25,  30,  35,  40,
			                     45,  50,  55,  60,  65,
			                     70,  75,  80,  85,  90};

        public BatteryConfigMPM(string deviceVer)
            : base(deviceVer)
        {
        }

        protected override void InitializeCmdInfo()
        {
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ1_EEPROM_GET, 500));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ2_EEPROM_GET, 500));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCU_EEPROM_H_GET, 500));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCU_EEPROM_L_GET, 500));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_VOLTAGE_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CURRENT_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_REMAIN_CAPACITY_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_RSOC_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CYCLE_COUNT_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_TEMP_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_PROTECTION_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_DESIGN_CAPACITY_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_DESIGN_VOLTAGE_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_FULL_CC_GET));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MANUFACTURE_DATA_GET));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_EEP_VERSION_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_BATTERY_NO_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MANUFACTURE_DATE_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_RESET_COUNT_GET));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CURRENT_GAIN));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CURRENT_OFFSET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_EDV2_TIME));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_TAPER_CURRENT_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CELL_NUMBER_GET));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_YEAR));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_MONTH));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_DAY));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_HOURS));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_MIN));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_SEC));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_1224));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_TIMESET)); //MPM為取得設定RTC之時間點
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_LAST_CHARGE_END));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_LAST_DISCHARGE_END));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_RTC_REG_DUMP)); //MPM為取得RTC時間點
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_LAST_CHARGE_START));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_LAST_DISCHARGE_START));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_PID_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_PID_SET, 0x06, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CID_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CID_SET, 0x06, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OID_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OID_SET, 0x06, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_UID_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_UID_SET, 0x06, false));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_LOCK_STATUS_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_LOCK_STATUS_SET, 0x02, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CAPACITY_REC_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CAPACITY_REC_CLR, 0x02, false));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CAPACITY_CHARGING_GET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_FUNC_STATUS_GET));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CAPACITY_REC_GET)); // Fixed by Neil 2016.01.24
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CAPACITY_REC_CLR)); // Fixed by Neil 2016.01.24

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCU_FLAG_GET));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCUEEP_SET, 0x04, false)); // Fixed by Neil 2016.01.06
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_ALL_OZ_RESET_SET));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCUEEP_GET, 0x02, false)); // Fixed by Neil 2016.01.06
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ1_BYTE_EEPROM_GET, false, 0x02, false, 80));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ2_BYTE_EEPROM_GET, false, 0x02, false, 80));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ1_BYTE_OP_GET, false, 0x02, false, 80));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ2_BYTE_OP_GET, false, 0x02, false, 80));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ1_BYTE_EEPROM_SET, false, 0x03, false, 80));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ2_BYTE_EEPROM_SET, false, 0x03, false, 80));// Fixed by Neil 2016.01.24
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ1_BYTE_OP_SET, 0x03, false)); 
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_OZ2_BYTE_OP_SET, 0x03, false));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SET_YEAR, 0x02, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SET_MONTH, 0x02, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SET_DAY, 0x02, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SET_HOUR, 0x02, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SET_MIN, 0x02, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SET_SEC, 0x02, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SET_1224, 0x02, false));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_LAST_START_CHG_TIME));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_LAST_STOP_CHG_TIME));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_RESET_BATTERY_MCU)); // Fixed by Neil 2017.03.10
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_RESET_CYCLE));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_NRCYCLE));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_RESET_PBPOWER));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SHUTDOWN_MCUOZ));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_OV_HAPPENTIME));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_UV_HAPPENTIME));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_COC_HAPPENTIME));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_SC_HAPPENTIME));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_OT_HAPPENTIME));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_UT_HAPPENTIME));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_DOC_HAPPENTIME));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_LAST_FCC_INFO));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_DISCHARGE_LOG, 300));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_CRATE_INFO, 0x02, false, 50));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_TEMPERATURE_INTERVAL, 0x02, false, 50));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_LAST_RESET_RTC_INFO));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCU_EEPROM_GET, 0x02, false, 200));

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_BARCODE_DATA_GET));

            Regex reg = new Regex(@"^MPMV");
            if (!reg.IsMatch(mDeviceVer))
            {
                reg = new Regex(@"^V");

                mIsVVersion = reg.IsMatch(mDeviceVer);

                if (mIsVVersion)
                {
                    //// Only 未加密版本
                    AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET, 0x02, false));

                    AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_CHARGE_LOG_00_22, 300));
                    AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_CHARGE_LOG_23_45, 300));
                    AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_CHARGE_LOG_46_49, 300));

                    //string[] verSplit = mDeviceVer.Split('.');
                    //if (verSplit != null && verSplit.Length == 4)
                    //{
                    //    int verNumber = 0;
                    //    bool isSuccess = int.TryParse(verSplit[3], out verNumber);
                    //    if (isSuccess && verNumber > 42)
                    //    {
                    //        AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_CHARGE_LOG_00_22, 300));
                    //        AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_CHARGE_LOG_23_45, 300));
                    //        AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_CHARGE_LOG_46_49, 300));
                    //    }
                    //}
                }
                else
                {
                    //// Only MPM(元智版本未加入)
                    AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MANUFACTURE_NAME_GET));


                    AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CIPHER_CARRIER));
                    AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CIPHER_UUID_GET));
                    AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CIPHER_ACCEPT));
                    AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CIPHER_KEY_RENEW, 0x02, false));

                    AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_CHARGE_LOG_00_22, 300));
                    AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_CHARGE_LOG_23_45, 300));
                    AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_CHARGE_LOG_46_49, 300));
                }
            }

            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_GET_CAN_MODE_STATUS, 0x02, false));
            AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SET_CAN_MODE_STATUS, 0x03, false));
            ////
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_CHEMISTRY_GET));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_SIMULATION_FCC_GET));

            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCU_OPTION_BYTE_SET, 0x12, false));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCU_OPTION_BYTE_GET));
            //AddCmdInfo(new BatteryCmdInfo(BatteryCmd.CMD_MCU_FLASH_PROGRAM));
        }

        protected override void InitializeMCUEEPAttrInfo()
        {
            Dictionary<string, BatteryAttrAddressInfo> attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(0, 2));
            BatteryEEPAttrInfo attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.EDV2Value, attrAddInfoList);
            AddMCUEEPAttrInfo(attrInfo);

            attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(56, 1));
            attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.ManufactureInfo, attrAddInfoList);
            AddMCUEEPAttrInfo(attrInfo);

            attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(18, 2));
            attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.ManufactureDate, attrAddInfoList);
            AddMCUEEPAttrInfo(attrInfo);

            attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(57, 1));
            attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.ManufactureWeekOfYear, attrAddInfoList);
            AddMCUEEPAttrInfo(attrInfo);

            attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(20, 2));
            attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.SerialNumber, attrAddInfoList);
            AddMCUEEPAttrInfo(attrInfo);
        }

        public override object GetMCUAttrInfo(BatteryEEPAttrCmd mcuAttr)
        {
            object data = null;
            BatteryEEPAttrInfo value = GetMCUEEPAttrInfo(mcuAttr);
            if (value != null)
            {
                switch (mcuAttr)
                {
                    case BatteryEEPAttrCmd.EDV2Value:
                        {
                            BatteryAttrAddressInfo mainInfo = value.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME];
                            byte[] tempData = new byte[mainInfo.Size];
                            for (int i = 0; i < mainInfo.Size; i++)
                            {
                                tempData[i] = mMCUEEP[mainInfo.Location + i];
                            }
                            data = (UInt16)((tempData[0] << 8) | tempData[1]);

                            break;
                        }
                }
            }

            return data;
        }

        public override BatteryAttrAddressInfo SetMCUAttrInfo(BatteryEEPAttrCmd mcuAttr, object value)
        {
            BatteryAttrAddressInfo result = null;
            try
            {
                BatteryEEPAttrInfo eepAttrInfo = GetMCUEEPAttrInfo(mcuAttr);

                if (eepAttrInfo != null && eepAttrInfo.AttrAddressInfoList.ContainsKey(BatteryAttrAddressInfo.MAINATTRIBUTENAME))
                {
                    result = eepAttrInfo.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME];
                    result.OutputData = new byte[result.Size];

                    switch (mcuAttr)
                    {
                        case BatteryEEPAttrCmd.ManufactureInfo:
                        case BatteryEEPAttrCmd.ManufactureWeekOfYear:
                            {
                                result.OutputData[0] = (byte)value;

                                break;
                            }
                        case BatteryEEPAttrCmd.ManufactureDate:
                            {
                                int[] date = (int[])value;
                                int year = (date[0] - DEFAULTYEAR);
                                UInt16 datetimeByte = (UInt16)(((year & 0x7F) << 9) | ((date[1] & 0x0F) << 5) | (date[2] & 0x1F));
                                result.OutputData[0] = (byte)(datetimeByte >> 8);
                                result.OutputData[1] = (byte)((datetimeByte & 0xFF));

                                break;
                            }
                        case BatteryEEPAttrCmd.SerialNumber:
                            {
                                UInt16 snValue = Convert.ToUInt16(value.ToString());
                                result.OutputData[0] = (byte)(snValue >> 8);
                                result.OutputData[1] = (byte)((snValue & 0xFF));

                                break;
                            }
                        case BatteryEEPAttrCmd.OVTH:
                        case BatteryEEPAttrCmd.OVRelease:
                            {
                                int tmpData = ((int)value) * 4096 / 5000;
                                result.OutputData[0] = (byte)((tmpData << 3) & 0xF8);
                                result.OutputData[1] = (byte)((tmpData >> 5) & 0xFF);

                                break;
                            }
                    }
                }
            }
            catch
            {
                result = null;
            }
            return result;
        }

        protected override void InitializePBEEPAttrInfo()
        {
            Dictionary<string, BatteryAttrAddressInfo> attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(0x45, 1));
            BatteryEEPAttrInfo attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.Ver, attrAddInfoList);
            AddPBEEPAttrInfo(attrInfo);

            attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(0x2B, 1));
            attrAddInfoList.Add("Offset", new BatteryAttrAddressInfo(0x03, 1));
            attrAddInfoList.Add("Data_H", new BatteryAttrAddressInfo(0x35, 1));
            attrAddInfoList.Add("Data_L", new BatteryAttrAddressInfo(0x34, 1));
            attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.SCTH, attrAddInfoList);
            AddPBEEPAttrInfo(attrInfo);

            attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(0x2C, 1));
            attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.SCDelay, attrAddInfoList);
            AddPBEEPAttrInfo(attrInfo);

            attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(0x29, 1));
            attrAddInfoList.Add("Offset", new BatteryAttrAddressInfo(0x04, 1));
            attrAddInfoList.Add("Data_H", new BatteryAttrAddressInfo(0x35, 1));
            attrAddInfoList.Add("Data_L", new BatteryAttrAddressInfo(0x34, 1));
            attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.OCTH, attrAddInfoList);
            AddPBEEPAttrInfo(attrInfo);

            attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(0x2A, 1));
            attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.OCDelayTime, attrAddInfoList);
            AddPBEEPAttrInfo(attrInfo);


            attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(0x36, 10));
            attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.FactoryName, attrAddInfoList);
            AddPBEEPAttrInfo(attrInfo);

            attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(0x40, 5));
            attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.ProjectName, attrAddInfoList);
            AddPBEEPAttrInfo(attrInfo);

            attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(0x4a, 2));
            attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.OVTH, attrAddInfoList);
            AddPBEEPAttrInfo(attrInfo);

            attrAddInfoList = new Dictionary<string, BatteryAttrAddressInfo>();
            attrAddInfoList.Add(BatteryAttrAddressInfo.MAINATTRIBUTENAME, new BatteryAttrAddressInfo(0x4c, 2));
            attrInfo = new BatteryEEPAttrInfo(BatteryEEPAttrCmd.OVRelease, attrAddInfoList);
            AddPBEEPAttrInfo(attrInfo);
        }

        public override int GetPBNumber()
        {
            int number = 0;

            if (mPBEEP != null)
            {
                foreach (byte[] eep in mPBEEP)
                {
                    if (eep != null)
                    {
                        number++;
                    }
                }
            }

            return number;
        }

        public override BatteryAttrAddressInfo SetPBAttrInfo(BatteryEEPAttrCmd mcuAttr, object value)
        {
            BatteryAttrAddressInfo result = null;
            try
            {
                BatteryEEPAttrInfo eepAttrInfo = GetPBEEPAttrInfo(mcuAttr);

                if (eepAttrInfo != null && eepAttrInfo.AttrAddressInfoList.ContainsKey(BatteryAttrAddressInfo.MAINATTRIBUTENAME))
                {
                    result = eepAttrInfo.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME];
                    result.OutputData = new byte[result.Size];

                    switch (mcuAttr)
                    {
                        case BatteryEEPAttrCmd.OVTH:
                        case BatteryEEPAttrCmd.OVRelease:
                            {
                                int tmpData =Convert.ToInt32( ((long)value) * 4096.0 / 5000.0);
                                result.OutputData[0] = (byte)((tmpData << 3) & 0xF8);
                                result.OutputData[1] = (byte)((tmpData >> 5) & 0xFF);

                                break;
                            }
                    }
                }
            }
            catch
            {
                result = null;
            }
            return result;
        }

        public override object GetPBAttrInfo(BatteryEEPAttrCmd pbAttr, int pbIndex)
        {
            object data = null;
            BatteryEEPAttrInfo value = GetPBEEPAttrInfo(pbAttr);
            if (value != null && pbIndex > -1 && pbIndex < mPBEEP.Length)
            {
                byte[] pbEEP = mPBEEP[pbIndex];
                if (pbEEP != null)
                {
                    switch (pbAttr)
                    {
                        case BatteryEEPAttrCmd.Ver:
                            {
                                BatteryAttrAddressInfo mainInfo = value.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME];

                                byte pbVerAll = pbEEP[mainInfo.Location];
                                int ipbAph = Convert.ToInt32((pbVerAll & 0x0F0) >> 4);
                                int iNumber = Convert.ToInt32(pbVerAll & 0x0F);

                                char chrAph = Convert.ToChar(65 + ipbAph);

                                data = string.Format("{0}{1:D2}", chrAph, iNumber);

                                break;
                            }
                        case BatteryEEPAttrCmd.FactoryName:
                        case BatteryEEPAttrCmd.ProjectName:
                            {
                                BatteryAttrAddressInfo mainInfo = value.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME];

                                //byte pbVerAll = pbEEP[mainInfo.Location];
                                //int ipbAph = Convert.ToInt32((pbVerAll & 0x0F0) >> 4);
                                //int iNumber = Convert.ToInt32(pbVerAll & 0x0F);

                                //char chrAph = Convert.ToChar(65 + ipbAph);

                                //data = string.Format("{0}{1:D2}", chrAph, iNumber);
                                List<byte> temp = new List<byte>();
                                for (int i = 0; i < mainInfo.Size; i++)
                                {
                                    if (pbEEP[mainInfo.Location + i] == 0)
                                    {
                                        break;
                                    }
                                    temp.Add(pbEEP[mainInfo.Location + i]);
                                }

                                data = System.Text.Encoding.ASCII.GetString(temp.ToArray());
                                break;
                            }
                        case BatteryEEPAttrCmd.OVTH:
                        case BatteryEEPAttrCmd.OVRelease:
                            {
                                byte nValue1 = pbEEP[value.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME].Location];
                                byte nValue2 = pbEEP[value.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME].Location + 1];

                                data = (((nValue2 << 5) | (nValue1 >> 3)) * 5000 / 4096);

                                break;
                            }
                        case BatteryEEPAttrCmd.SCTH:
                            {
                                byte nValue = pbEEP[value.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME].Location];
                                byte mValue = pbEEP[value.AttrAddressInfoList["Offset"].Location];
                                byte data_LValue = pbEEP[value.AttrAddressInfoList["Data_L"].Location];
                                byte data_HValue = pbEEP[value.AttrAddressInfoList["Data_H"].Location];

                                int N = Convert.ToInt32(nValue & 0x3F);

                                //int M = mValue & 0x0F;  // M is Bit0~3 of OzEEP addr 0x03 value
                                int M = mValue & 0x0F; // 0x07?
                                if ((M & 0x08) != 0)
                                {
                                    M = ~M;
                                    M = M + 1;
                                    M = M & 0x0F;
                                    M = 0 - M;
                                }

                                double Rs = Convert.ToDouble((((data_HValue << 8) | data_LValue) * 0.001));
                                double resultValue = (((double)N + (double)M + 2) * 10.0) / Rs;   // TH=(N + M + 2)*(-10)/Rs

                                data = Math.Floor(resultValue * 10.0) / 10.0;//取小數點1位

                                break;
                            }
                        case BatteryEEPAttrCmd.SCDelay:
                            {
                                BatteryAttrAddressInfo mainInfo = value.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME];

                                byte item = pbEEP[mainInfo.Location];

                                int valueUnit = Convert.ToInt32(item & 0x7);

                                switch (valueUnit)
                                {
                                    case 0:
                                        valueUnit = 8;
                                        break;
                                    case 1:
                                        valueUnit = 16;
                                        break;
                                    case 2:
                                        valueUnit = 32;
                                        break;
                                    case 3:
                                        valueUnit = 64;
                                        break;
                                    case 4:
                                        valueUnit = 128;
                                        break;
                                    case 5:
                                        valueUnit = 256;
                                        break;
                                    case 6:
                                        valueUnit = 512;
                                        break;
                                    case 7:
                                        valueUnit = 1024;
                                        break;
                                    default:
                                        {
                                            valueUnit = 0;
                                            break;
                                        }
                                }

                                int iNumber = Convert.ToInt32((item & 0xF8) >> 3); //算法不同?
                                data = ((iNumber + 1) * valueUnit);
                                break;
                            }
                        case BatteryEEPAttrCmd.OCTH:
                            {
                                byte nvalue = pbEEP[value.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME].Location];
                                byte mValue = pbEEP[value.AttrAddressInfoList["Offset"].Location];
                                byte data_LValue = pbEEP[value.AttrAddressInfoList["Data_L"].Location];
                                byte data_HValue = pbEEP[value.AttrAddressInfoList["Data_H"].Location];

                                int N = Convert.ToInt32(nvalue & 0x3F);

                                int M = Convert.ToInt16((mValue >> 4) & 0x0F);
                                //int M = mValue & 0x0F;
                                if ((M & 0x08) != 0)
                                {
                                    M = ~M;
                                    M = M + 1;
                                    M = M & 0x0F;
                                    M = 0 - M;
                                }

                                double Rs = Convert.ToDouble((((data_HValue << 8) | data_LValue) * 0.001));
                                double resultValue = (((double)N + (double)M) * (-5.0)) / Rs;   // TH=(N + M + 2)*(-10)/Rs

                                data = Math.Floor(resultValue * 10.0) / 10.0;//取小數點兩位
                                break;
                            }
                        case BatteryEEPAttrCmd.OCDelayTime:
                            {
                                BatteryAttrAddressInfo mainInfo = value.AttrAddressInfoList[BatteryAttrAddressInfo.MAINATTRIBUTENAME];

                                byte item = pbEEP[mainInfo.Location];

                                int valueUnit = Convert.ToInt32(item & 0x07);
                                switch (valueUnit)
                                {
                                    case 0:
                                        valueUnit = 2;
                                        break;
                                    case 1:
                                        valueUnit = 6;
                                        break;
                                    case 2:
                                        valueUnit = 14;
                                        break;
                                    case 3:
                                        valueUnit = 30;
                                        break;
                                    case 4:
                                        valueUnit = 62;
                                        break;
                                    case 5:
                                        valueUnit = 126;
                                        break;
                                    case 6:
                                        valueUnit = 254;
                                        break;
                                    case 7:
                                        valueUnit = 510;
                                        break;
                                    default:
                                        {
                                            valueUnit = 0; //error!!
                                            break;
                                        }
                                }

                                int iNumber = Convert.ToInt32((item >> 3) & 0x1F);//算法不同?
                                data = (iNumber + 1) * valueUnit;
                                break;
                            }
                    }
                }
            }

            return data;
        }

        public override BatteryCmdInfo ResetCmdInfo(BatteryCmdInfo cmdInfo, byte param, object value)
        {
            switch (cmdInfo.CmdCode)
            {
                case (byte)BatteryCmd.CMD_PID_SET:
                case (byte)BatteryCmd.CMD_CID_SET:
                case (byte)BatteryCmd.CMD_OID_SET:
                case (byte)BatteryCmd.CMD_UID_SET:
                    {
                        byte[] idList = BitConverter.GetBytes(Convert.ToUInt32(param));

                        cmdInfo.Info = idList;

                        break;
                    }
                case (byte)BatteryCmd.CMD_LOCK_STATUS_SET:
                    {
                        //byte lockState = 0x00;

                        //GTECmdManagement.ChargingStatus chargingStatus =
                        //    (GTECmdManagement.ChargingStatus)Enum.ToObject(typeof(GTECmdManagement.ChargingStatus), param);
                        //switch (chargingStatus)
                        //{
                        //    case GTECmdManagement.ChargingStatus.DischargingLock:
                        //    case GTECmdManagement.ChargingStatus.ChargingLock:
                        //        {
                        //            lockState |= (byte)chargingStatus;
                        //            break;
                        //        }
                        //    case GTECmdManagement.ChargingStatus.ChargingUnLock:
                        //    case GTECmdManagement.ChargingStatus.DischargingUnLock:
                        //        {
                        //            lockState &= (byte)chargingStatus;
                        //            break;
                        //        }
                        //}

                        //cmdInfo.Para = lockState;

                        break;

                    }
                case (byte)BatteryCmd.CMD_MCU_FLASH_PROGRAM:
                    {
                        //GTECmdManagement.FlashBurnType flashBurnType =
                        //    (GTECmdManagement.FlashBurnType)Enum.ToObject(typeof(GTECmdManagement.FlashBurnType), param);

                        //cmdInfo.Para = (byte)flashBurnType;
                        //switch (flashBurnType)
                        //{
                        //    // not implement flash burn
                        //    case GTECmdManagement.FlashBurnType.CMD_GO:
                        //    case GTECmdManagement.FlashBurnType.CMD_ER:
                        //        {
                        //            uint addr = (uint)value;
                        //            cmdInfo.Info = new byte[4];
                        //            cmdInfo.Info[0] = Convert.ToByte((addr >> 24) & 0x000000FF);
                        //            cmdInfo.Info[1] = Convert.ToByte((addr >> 16) & 0x000000FF);
                        //            cmdInfo.Info[2] = Convert.ToByte((addr >> 8) & 0x000000FF);
                        //            cmdInfo.Info[3] = Convert.ToByte((addr) & 0x000000FF);

                        //            break;
                        //        }
                        //    case GTECmdManagement.FlashBurnType.CMD_RM:
                        //        {
                        //            uint addr = (uint)value;
                        //            cmdInfo.Info = new byte[8];
                        //            cmdInfo.Info[0] = Convert.ToByte((addr >> 24) & 0x000000FF);
                        //            cmdInfo.Info[1] = Convert.ToByte((addr >> 16) & 0x000000FF);
                        //            cmdInfo.Info[2] = Convert.ToByte((addr >> 8) & 0x000000FF);
                        //            cmdInfo.Info[3] = Convert.ToByte((addr) & 0x000000FF);
                        //            cmdInfo.Info[4] = 0;
                        //            cmdInfo.Info[5] = 0;
                        //            cmdInfo.Info[6] = 4;
                        //            cmdInfo.Info[7] = 0;

                        //            break;
                        //        }
                        //    case GTECmdManagement.FlashBurnType.CMD_WM:
                        //        {
                        //            // need to fix
                        //            cmdInfo.Info = (byte[])value;

                        //            break;
                        //        }
                        //    case GTECmdManagement.FlashBurnType.CMD_BM:
                        //    case GTECmdManagement.FlashBurnType.CMD_BMUN:
                        //        {
                        //            // need to fix

                        //            break;
                        //        }
                        //}
                        break;
                    }
                case (byte)BatteryCmd.CMD_MCUEEP_SET:
                    {
                        cmdInfo.Para = (byte)param; // param must be a int

                        cmdInfo.Info = new byte[2];
                        // Fixed by Neil 2016.01.06
                        if (mIsVVersion)
                        {
                            //舊版設定MCU沒有數量設定
                            cmdInfo.Info[0] = (byte)value;
                            cmdInfo.Info[1] = 0xFF;
                        }
                        else
                        {
                            cmdInfo.Info[0] = 0x01;
                            cmdInfo.Info[1] = (byte)value;
                        }
                        //

                        break;
                    }
                case (byte)BatteryCmd.CMD_SET_CAN_MODE_STATUS: // Add by Neil 2017.03.23
                    {
                        cmdInfo.Para = (byte)param; // param must be a int
                        cmdInfo.Info = new byte[1];
                        cmdInfo.Info[0] = (byte)value;

                        break;
                    }
                case (byte)BatteryCmd.CMD_GET_CAN_MODE_STATUS: // Add by Neil 2017.03.23
                case (byte)BatteryCmd.CMD_MCUEEP_GET:
                    {
                        cmdInfo.Para = (byte)param; // param must be a int

                        // Fixed by Neil 2016.01.06
                        //cmdInfo.Info = new byte[1];
                        //cmdInfo.Info[0] = 1;
                        //
                        break;
                    }
                case (byte)BatteryCmd.CMD_OZ1_BYTE_EEPROM_GET:
                case (byte)BatteryCmd.CMD_OZ2_BYTE_EEPROM_GET:
                case (byte)BatteryCmd.CMD_OZ1_BYTE_OP_GET:
                case (byte)BatteryCmd.CMD_OZ2_BYTE_OP_GET:
                case (byte)BatteryCmd.CMD_GET_CRATE_INFO:
                case (byte)BatteryCmd.CMD_GET_TEMPERATURE_INTERVAL:
                case (byte)BatteryCmd.CMD_MCU_EEPROM_GET:
                    {
                        cmdInfo.Para = (byte)param; // param must be a int

                        break;
                    }
                case (byte)BatteryCmd.CMD_OZ1_BYTE_EEPROM_SET:
                case (byte)BatteryCmd.CMD_OZ2_BYTE_EEPROM_SET:
                case (byte)BatteryCmd.CMD_OZ1_BYTE_OP_SET:
                case (byte)BatteryCmd.CMD_OZ2_BYTE_OP_SET:
                    {
                        cmdInfo.Para = (byte)param; // param must be a int

                        cmdInfo.Info = new byte[1];
                        cmdInfo.Info[0] = (byte)value;

                        break;
                    }
                case (byte)BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET:
                case (byte)BatteryCmd.CMD_SET_YEAR:
                case (byte)BatteryCmd.CMD_SET_MONTH:
                case (byte)BatteryCmd.CMD_SET_DAY:
                case (byte)BatteryCmd.CMD_SET_HOUR:
                case (byte)BatteryCmd.CMD_SET_MIN:
                case (byte)BatteryCmd.CMD_SET_SEC:
                    {
                        cmdInfo.Para = Convert.ToByte(param); // param must be a int

                        break;
                    }
                case (byte)BatteryCmd.CMD_GET_LAST_CHARGE_END:
                case (byte)BatteryCmd.CMD_GET_LAST_DISCHARGE_END:
                case (byte)BatteryCmd.CMD_GET_LAST_CHARGE_START:
                case (byte)BatteryCmd.CMD_GET_LAST_DISCHARGE_START:
                case (byte)BatteryCmd.CMD_GET_OV_HAPPENTIME:
                case (byte)BatteryCmd.CMD_GET_UV_HAPPENTIME:
                case (byte)BatteryCmd.CMD_GET_COC_HAPPENTIME:
                case (byte)BatteryCmd.CMD_GET_SC_HAPPENTIME:
                case (byte)BatteryCmd.CMD_GET_OT_HAPPENTIME:
                case (byte)BatteryCmd.CMD_GET_UT_HAPPENTIME:
                case (byte)BatteryCmd.CMD_GET_DOC_HAPPENTIME:
                    {
                        cmdInfo.Para = Convert.ToByte(param); // param must be a int

                        break;
                    }
            }
            return cmdInfo;
        }

        public override object ParserData(byte[] rawData)
        {
            object value = null;

            if (rawData != null && rawData.Length >= 3)
            {
                byte cmdLen = rawData[0];
                byte cmdPara = rawData[1];
                byte cmdCode = rawData[2];

                int infoLen = (int)(cmdLen - 2);
                if (VersionType == EncapsulationType.VVersion)
                {
                    infoLen = (int)(cmdLen - 3);
                }

                byte[] info = null;
                if (infoLen > 0 && ((rawData.Length - 3) >= infoLen))
                {
                    Array.Resize(ref info, infoLen);
                    Array.Copy(rawData, 3, info, 0, infoLen);
                }
                //BatteryErrorCmd errorCmd = ConvertToErrorCmd(cmdPara);

                //if (errorCmd == BatteryErrorCmd.NOERROR)
                //{
                    switch (cmdCode)
                    {
                        case (byte)BatteryCmd.CMD_MCU_EEPROM_GET:
                            {
                                if (cmdPara == 0)
                                {
                                    value = (int)info[0];
                                }
                                else
                                {
                                    value = info;
                                }

                                break;
                            }
                        case (byte)BatteryCmd.CMD_OZ1_EEPROM_GET:
                        case (byte)BatteryCmd.CMD_OZ2_EEPROM_GET:
                        case (byte)BatteryCmd.CMD_MCU_EEPROM_H_GET:
                        case (byte)BatteryCmd.CMD_MCU_EEPROM_L_GET:
                            {
                                value = info;
                                break;
                            }
                        case (byte)BatteryCmd.CMD_MCU_OPTION_BYTE_GET:
                        case (byte)BatteryCmd.CMD_MCU_FLAG_GET:
                        case (byte)BatteryCmd.CMD_CIPHER_UUID_GET:
                        case (byte)BatteryCmd.CMD_CIPHER_ACCEPT:
                            {
                                byte[] valueList = null;
                                Array.Resize(ref valueList, info.Length);
                                Array.Copy(info, 0, valueList, 0, info.Length);
                                value = valueList;

                                break;
                            }
                        case (byte)BatteryCmd.CMD_VOLTAGE_GET:
                            {
                                List<int> cellVoltList = new List<int>();

                                switch (VersionType)
                                {
                                    case EncapsulationType.MPVersion:
                                        {
                                            int cellCount = Convert.ToInt16(info[4]) + Convert.ToInt16(info[5]); // total base dwon number (n66 + n88)

                                            for (int i = 0; i < cellCount; i++)
                                            {
                                                cellVoltList.Add((info[i * 2 + 6] << 8) + info[i * 2 + 7]);
                                            }
                                            break;
                                        }
                                    case EncapsulationType.VVersion:
                                        {
                                            for (int i = 0; i < info.Length; i = i + 2)
                                            {
                                                cellVoltList.Add((((int)info[i] | ((int)info[i + 1] << 8)) >> 3) * 5000 / 4096);
                                            }

                                            break;
                                        }
                                }

                                if (cellVoltList.Count > 20 && cellVoltList.Count % 2 == 1) // 7244舊版過濾有假電壓的版本
                                {
                                    cellVoltList.RemoveAt(cellVoltList.Count - 1);
                                }
                                else if (cellVoltList.Count > 14 && cellVoltList.Count % 2 == 1) // 過濾有假電壓的版本
                                {
                                    cellVoltList.RemoveAt((cellVoltList.Count / 2) + 1);
                                }

                                value = cellVoltList.ToArray();
                                break;
                            }
                        case (byte)BatteryCmd.CMD_RSOC_GET:
                            {
                                switch (VersionType)
                                {
                                    case EncapsulationType.MPVersion:
                                        {
                                            value = (int)info[0];
                                            break;
                                        }
                                    case EncapsulationType.VVersion:
                                        {
                                            value = (int)(info[0] << 8) + (int)(info[1]);
                                            break;
                                        }
                                }
                                break;
                            }
                        case (byte)BatteryCmd.CMD_CELL_NUMBER_GET:
                        case (byte)BatteryCmd.CMD_LOCK_STATUS_GET:
                        case (byte)BatteryCmd.CMD_GET_MONTH:
                        case (byte)BatteryCmd.CMD_GET_DAY:
                        case (byte)BatteryCmd.CMD_GET_HOURS:
                        case (byte)BatteryCmd.CMD_GET_MIN:
                        case (byte)BatteryCmd.CMD_GET_SEC:
                        case (byte)BatteryCmd.CMD_GET_1224:
                        case (byte)BatteryCmd.CMD_OZ1_BYTE_EEPROM_GET:
                        case (byte)BatteryCmd.CMD_OZ2_BYTE_EEPROM_GET:
                        case (byte)BatteryCmd.CMD_OZ1_BYTE_OP_GET:
                        case (byte)BatteryCmd.CMD_OZ2_BYTE_OP_GET:
                            {
                                value = info[0];
                                break;
                            }
                        case (byte)BatteryCmd.CMD_GET_YEAR:
                            {
                                value = info[0] + DEFAULTYEAR;
                                break;
                            }
                        case (byte)BatteryCmd.CMD_TEMP_GET:
                            {
                                switch (VersionType)
                                {
                                    case EncapsulationType.MPVersion:
                                        {
                                            double tempZero = 2731.2;
                                            double tempTime = 10.0;

                                            int n66 = Convert.ToInt16(info[0]);
                                            int n68 = Convert.ToInt16(info[1]);
                                            int cellCount = n66 + n68; // total base dwon number (n66 + n88)

                                            double[] tempList = null;
                                            Array.Resize(ref tempList, cellCount);

                                            for (int i = 0; i < n66; i++)
                                            {
                                                double currentTemp = ((info[2 + i * 2] << 8 | info[3 + i * 2]) - tempZero) / tempTime;
                                                tempList[i] = currentTemp;
                                            }
                                            for (int i = 0; i < n68; i++)
                                            {
                                                double currentTemp = (((info[2 + (n66 + i) * 2] << 8) | (info[3 + (n66 + i) * 2])) - tempZero) / tempTime;
                                                tempList[n66 + i] = currentTemp;
                                            }

                                            value = tempList;
                                            break;
                                        }
                                    case EncapsulationType.VVersion:
                                        {
                                            int n66 = 0;
                                            int n68 = 0;
                                            if ((cmdLen - 2) >> 1 == 3)
                                            {
                                                n66 = 3;
                                                n68 = 0;
                                            }
                                            else if ((cmdLen - 2) >> 1 == 6)
                                            {
                                                n66 = 3;
                                                n68 = 3;
                                            }
                                            int cellCount = n66 + n68; // total base dwon number (n66 + n88)
                                            if (cellCount != 0)
                                            {
                                                double[] tempList = null;
                                                Array.Resize(ref tempList, cellCount);

                                                for (int i = 0; i < n66; i++)
                                                {
                                                    double currentTemp = ((int)(info[i * 2 + 1]) << 5 | (int)(info[i * 2]) >> 3);
                                                    if (i == 0)
                                                    {
                                                        // OZ_CountInternalTemp
                                                        currentTemp = OZ_CountInternalTemp(currentTemp, 0);
                                                    }
                                                    else
                                                    {
                                                        // OZ_CheckExternalTempTabel
                                                        currentTemp = OZ_CheckExternalTempTabel(currentTemp);
                                                    }
                                                    tempList[i] = currentTemp;
                                                }
                                                for (int i = 0; i < n68; i++)
                                                {
                                                    double currentTemp = ((int)(info[(i + n66) * 2 + 1]) << 5 | (int)(info[(i + n66) * 2]) >> 3);
                                                    if (i == 0)
                                                    {
                                                        // OZ_CountInternalTemp
                                                        currentTemp = OZ_CountInternalTemp(currentTemp, 1);
                                                    }
                                                    else
                                                    {
                                                        // OZ_CheckExternalTempTabel
                                                        currentTemp = OZ_CheckExternalTempTabel(currentTemp);
                                                    }
                                                    tempList[n66 + i] = currentTemp;
                                                }

                                                value = tempList;
                                            }

                                            break;
                                        }
                                }
                                break;
                            }
                        case (byte)BatteryCmd.CMD_PROTECTION_GET:
                            {
                                byte[] statusList = new byte[7]; // 13 data

                                if (info != null && (info.Length != 0) && (info.Length % 2 == 0))
                                {
                                    int ozNumber = info.Length / 2;
                                    int index = 0;
                                    for (int i = 0; i < ozNumber; i ++)
                                    {
                                        statusList[0] = (byte)(statusList[0] | ((info[index] >> 0) & 0x01));//Over Voltage Protection
                                        statusList[1] = (byte)(statusList[1] | ((info[index] >> 1) & 0x01));//Under voltage protection
                                        statusList[2] = (byte)(statusList[2] | (((info[index] >> 2) & 0x01) & ((info[index + 1] >> 3) & 0x01)));// Over Current Charge
                                        statusList[3] = (byte)(statusList[3] | (((info[index] >> 2) & 0x01) & ((info[index+ 1] >> 2) & 0x01)));// Over Current Discharge
                                        statusList[4] = (byte)(statusList[4] | (((info[index] >> 4) & 0x01)));// Temp Discharge (OT)
                                        statusList[5] = (byte)(statusList[5] | (((info[index] >> 5) & 0x01)));// Temp charge (UT)
                                        statusList[6] = (byte)(statusList[6] | (((info[index] >> 3) & 0x01)));// Short Circuit

                                        index = index + 2;
                                    }
                                }

                                value = statusList;
                                break;
                            }
                        case (byte)BatteryCmd.CMD_CURRENT_GET:
                            {
                                //有負值
                                value = (Int32)((ulong)info[0] << 24 | (ulong)info[1] << 16 | (ulong)info[2] << 8 | (ulong)info[3]);
                                break;
                            }
                        case (byte)BatteryCmd.CMD_DESIGN_VOLTAGE_GET:
                            {
                                switch (VersionType)
                                {
                                    case EncapsulationType.MPVersion:
                                        {
                                            value = ((ulong)info[0] << 24) | ((ulong)info[1] << 16) | ((ulong)info[2] << 8) | ((ulong)info[3]);
                                            break;
                                        }
                                    case EncapsulationType.VVersion:
                                        {
                                            value = ((ulong)info[0] << 8 | (ulong)info[1]) * 1000;
                                            break;
                                        }
                                }
                                break;
                            }
                        case (byte)BatteryCmd.CMD_REMAIN_CAPACITY_GET:
                        case (byte)BatteryCmd.CMD_DESIGN_CAPACITY_GET:
                        case (byte)BatteryCmd.CMD_FULL_CC_GET:
                        case (byte)BatteryCmd.CMD_TAPER_CURRENT_GET:
                            {
                                switch (VersionType)
                                {
                                    case EncapsulationType.MPVersion:
                                        {
                                            value = ((ulong)info[0] << 24) | ((ulong)info[1] << 16) | ((ulong)info[2] << 8) | ((ulong)info[3]);
                                            break;
                                        }
                                    case EncapsulationType.VVersion:
                                        {
                                            value = (ulong)(((ulong)info[0] << 24 | (ulong)info[1] << 16 | (ulong)info[2] << 8 | (ulong)info[3]) / 3600);
                                            break;
                                        }
                                }
                                break;
                            }
                        case (byte)BatteryCmd.CMD_CURRENT_OFFSET:
                        case (byte)BatteryCmd.CMD_PID_GET:
                        case (byte)BatteryCmd.CMD_CID_GET:
                        case (byte)BatteryCmd.CMD_OID_GET:
                        case (byte)BatteryCmd.CMD_UID_GET:
                        case (byte)BatteryCmd.CMD_CAPACITY_REC_GET:
                        case (byte)BatteryCmd.CMD_CAPACITY_CHARGING_GET:
                        case (byte)BatteryCmd.CMD_EDV2_TIME:
                            {
                                value = ((ulong)info[0] << 24 | (ulong)info[1] << 16 | (ulong)info[2] << 8 | (ulong)info[3]);
                                break;
                            }
                        case (byte)BatteryCmd.CMD_DEVICE_NAME_GET:
                        case (byte)BatteryCmd.CMD_MANUFACTURE_DATA_GET:
                        case (byte)BatteryCmd.CMD_EEP_VERSION_GET:
                        case (byte)BatteryCmd.CMD_MCU_VERSION_GET:
                        case (byte)BatteryCmd.CMD_CHEMISTRY_GET:
                        case (byte)BatteryCmd.CMD_MANUFACTURE_NAME_GET:
                            {
                                string data = "";
                                for (int i = 0; i < (infoLen); i++)
                                    data += String.Format("{0}", (char)info[i]).ToString().Trim('\0');

                                value = data.Trim();
                                break;
                            }
                        case (byte)BatteryCmd.CMD_CYCLE_COUNT_GET:
                            {
                                value = (((ulong)info[0] << 8) + (ulong)info[1]);
                                break;
                            }
                        case (byte)BatteryCmd.CMD_BATTERY_NO_GET:
                            {
                                value = (((ulong)info[0] << 8) + (ulong)info[1]).ToString();
                                break;
                            }
                        case (byte)BatteryCmd.CMD_RESET_COUNT_GET:
                            {
                                value = (Int32)((ulong)info[0] << 8 | (ulong)info[1]);
                                break;
                            }
                        case (byte)BatteryCmd.CMD_CURRENT_GAIN:
                            {
                                value = Convert.ToUInt16(((ushort)info[0] << 8) + ((ushort)info[1]));
                                break;
                            }
                        case (byte)BatteryCmd.CMD_RESET_CYCLE:
                        case (byte)BatteryCmd.CMD_GET_NRCYCLE:
                            {
                                value = Convert.ToUInt16((info[0] << 8) | info[1]);
                                break;
                            }
                        case (byte)BatteryCmd.CMD_MANUFACTURE_DATE_GET:
                            {
                                int TempDate = (ushort)info[0] << 8 | info[1];
                                int[] dataList = new int[3];

                                dataList[0] = ((TempDate >> 9) & 0x0000007F) + DEFAULTYEAR;
                                int year = DateTime.Now.Year;
                                if (year < dataList[0])
                                {//1980 Start year
                                    dataList[0] = dataList[0] - 20;
                                }

                                dataList[1] = ((TempDate >> 5) & 0x0000000F);
                                dataList[2] = TempDate & 0x0000001F;

                                value = dataList;
                                break;
                            }
                        case (byte)BatteryCmd.CMD_BARCODE_DATA_GET:
                            {
                                // Fixed by Neil 2016.01.06 skip
                                //switch (VersionType)
                                //{
                                //    case EncapsulationType.MPVersion:
                                //        {
                                //            int manufacturer = (info[0] >> 4);
                                //            int productClass = (info[0] & 0x0F);
                                //            int year = info[1];
                                //            int week = info[2];
                                //            ulong serial = (ulong)info[3] << 8 | (ulong)info[4];

                                //            value = string.Format("{0:0}{1:0}{2:00}{3:00}{4:0000}", manufacturer, productClass, year, week, serial);
                                //            break;
                                //        }
                                //}
                                // Fixed by Neil 2016.01.06 Add
                                int manufacturer = (info[0] >> 4);
                                int productClass = (info[0] & 0x0F);
                                int year = info[1];
                                int week = info[2];
                                ulong serial = (ulong)info[3] << 8 | (ulong)info[4];

                                value = string.Format("{0:0}{1:0}{2:00}{3:00}{4:0000}", manufacturer, productClass, year, week, serial);
                                break;
                                ///
                            }
                        case (byte)BatteryCmd.CMD_GET_OV_HAPPENTIME:
                        case (byte)BatteryCmd.CMD_GET_UV_HAPPENTIME:
                        case (byte)BatteryCmd.CMD_GET_COC_HAPPENTIME:
                        case (byte)BatteryCmd.CMD_GET_SC_HAPPENTIME:
                        case (byte)BatteryCmd.CMD_GET_OT_HAPPENTIME:
                        case (byte)BatteryCmd.CMD_GET_UT_HAPPENTIME:
                        case (byte)BatteryCmd.CMD_GET_DOC_HAPPENTIME:
                            {
                                switch (VersionType)
                                {
                                    case EncapsulationType.MPVersion:
                                        {
                                            int[] time = new int[10];
                                            time[0] = info[0] + DEFAULTYEAR; // year
                                            time[1] = info[1]; // month
                                            time[2] = info[2]; // day
                                            time[3] = info[3]; // hour
                                            time[4] = info[4]; // min
                                            time[5] = info[5]; // sec
                                            time[6] = (info[6] << 8) + (info[7]); // vmin
                                            time[7] = (info[8] << 8) + (info[9]); // vmax
                                            time[8] = (info[10] << 8) + (info[11]); // error count
                                            time[9] = info[12]; // soc
                                            value = time;

                                            break;
                                        }
                                    case EncapsulationType.VVersion:
                                        {
                                            if (info != null && info.Length == 13)
                                            {
                                                int[] time = new int[10];
                                                time[0] = info[0] + DEFAULTYEAR; // year
                                                time[1] = info[1]; // month
                                                time[2] = info[2]; // day
                                                time[3] = info[3]; // hour
                                                time[4] = info[4]; // min
                                                time[5] = info[5]; // sec

                                                time[6] = ((info[6] << 8) + (info[7])) * 5000 / 4096; // vmin
                                                if (time[6] > 0xFFFF)
                                                {
                                                    time[6] = 0;
                                                }
                                                time[7] = ((info[8] << 8) + (info[9])) * 5000 / 4096; // vmax
                                                if (time[7] > 0xFFFF)
                                                {
                                                    time[7] = 0;
                                                }

                                                time[8] = (info[10] << 8) + (info[11]); // error count
                                                time[9] = info[12]; // soc
                                                value = time;
                                            }
                                            else
                                            {
                                                value = null;
                                            }
                                            break;
                                        }
                                }


                                break;
                            }
                        case (byte)BatteryCmd.CMD_GET_TIMESET:
                            {
                                int[] time = new int[6];
                                time[0] = info[0] + DEFAULTYEAR; // year
                                time[1] = info[1]; // month
                                time[2] = info[2]; // day
                                time[3] = info[3]; // hour
                                time[4] = info[4]; // min
                                time[5] = info[5]; // sec
                                value = time;

                                break;
                            }
                        case (byte)BatteryCmd.CMD_GET_LAST_FCC_INFO:
                        case (byte)BatteryCmd.CMD_GET_LAST_RESET_RTC_INFO:
                            {
                                int[] time = new int[6];
                                time[0] = info[0] + DEFAULTYEAR; // year
                                time[1] = info[1]; // month
                                time[2] = info[2]; // day
                                time[3] = info[3]; // hour
                                time[4] = info[4]; // min
                                time[5] = ((info[5] << 8) + (info[6])); // Count
                                value = time;

                                break;
                            }
                        case (byte)BatteryCmd.CMD_GET_CRATE_INFO:
                        case (byte)BatteryCmd.CMD_GET_TEMPERATURE_INTERVAL:
                            {
                                if (cmdPara == 0)
                                {
                                    int[] time = new int[4];
                                    time[0] = info[0]; // 1C
                                    time[1] = info[1]; // 2C
                                    time[2] = info[2]; // 3C
                                    time[3] = info[3]; // 4C
                                    value = time;
                                }
                                else if (cmdPara == 1)
                                {
                                    if (infoLen % 4 == 0)
                                    {
                                        int number = infoLen / 4;
                                        long[] count = new long[number];
                                        for (int i = 0; i < number; i++)
                                        {
                                            count[i] = (info[i * 4] << 24) + (info[i * 4 + 1] << 16) + (info[i * 4 + 2] << 8) + info[i * 4 + 3];
                                        }
                                        value = count;
                                    }
                                }

                                break;
                            }
                        case (byte)BatteryCmd.CMD_GET_LAST_CHARGE_END:
                        case (byte)BatteryCmd.CMD_GET_LAST_DISCHARGE_END:
                        case (byte)BatteryCmd.CMD_GET_LAST_CHARGE_START: //4840沒有值
                        case (byte)BatteryCmd.CMD_GET_LAST_DISCHARGE_START:   //4840沒有值
                            {
                                int[] time = new int[7];
                                time[0] = info[0] + DEFAULTYEAR; // year
                                time[1] = info[1]; // month
                                time[2] = info[2]; // day
                                time[3] = info[3]; // hour
                                time[4] = info[4]; // min
                                time[5] = info[5]; // sec

                                switch (VersionType)
                                {
                                    case EncapsulationType.MPVersion:
                                        {
                                            time[6] = info[12]; // ****SOC****
                                            break;
                                        }
                                    case EncapsulationType.VVersion:
                                        {
                                            time[6] = info[6]; // ****SOC****
                                            break;
                                        }

                                }
                                value = time;
                                break;
                            }
                        case (byte)BatteryCmd.CMD_GET_CHARGE_LOG_00_22:
                        case (byte)BatteryCmd.CMD_GET_CHARGE_LOG_23_45:
                        case (byte)BatteryCmd.CMD_GET_CHARGE_LOG_46_49:
                            {
                                List<int[]> tempData = new List<int[]>();

                                int size = 0;
                                bool checkLastChargeTimeData = false;

                                if (cmdCode == (byte)BatteryCmd.CMD_GET_CHARGE_LOG_00_22)
                                {
                                    size = 23;
                                    checkLastChargeTimeData = true;
                                }
                                else if (cmdCode == (byte)BatteryCmd.CMD_GET_CHARGE_LOG_23_45)
                                {
                                    size = 23;
                                }
                                else if (cmdCode == (byte)BatteryCmd.CMD_GET_CHARGE_LOG_46_49)
                                {
                                    size = 4;
                                }


                                for (int i = 0; i < size; i++)
                                {
                                    int[] temp = new int[8];
                                    int date = info[i * 10] << 8 | info[i * 10 + 1];

                                    temp[0] = ((date >> 9) & 0x0000007F) + DEFAULTYEAR;//year
                                    temp[1] = ((date >> 5) & 0x0000000F); //month
                                    temp[2] = date & 0x0000001F; // day
                                    temp[3] = info[i * 10 + 2] & 0x0000001F; // hour
                                    temp[4] = info[i * 10 + 3] & 0x0000003F; // minute
                                    temp[5] = (info[i * 10 + 2] & 0x80) << 9 | info[i * 10 + 4] << 8 | info[i * 10 + 5]; //voltstart
                                    temp[6] = (info[i * 10 + 8] & 0x80) << 9 | info[i * 10 + 6] << 8 | info[i * 10 + 7]; //voltstop
                                    if (i == 1 && checkLastChargeTimeData)
                                    {
                                        temp[7] = ((0x7F & info[i * 10 + 8]) << 8 | info[i * 10 + 9]) * 60; //duration
                                    }
                                    else
                                    {
                                        temp[7] = (0x7F & info[i * 10 + 8]) << 8 | info[i * 10 + 9]; //duration
                                    }

                                    tempData.Add(temp);
                                }

                                value = tempData.ToArray();
                                break;
                            }
                        case (byte)BatteryCmd.CMD_GET_DISCHARGE_LOG:
                            {
                                List<int[]> tempData = new List<int[]>();
                                for (int i = 0; i < 25; i++)
                                {
                                    int[] temp = new int[6];
                                    int date = info[i * 5] << 8 | info[i * 5 + 1];

                                    temp[0] = ((date >> 9) & 0x0000007F) + DEFAULTYEAR;//year
                                    temp[1] = ((date >> 5) & 0x0000000F); //month
                                    temp[2] = date & 0x0000001F; // day
                                    temp[3] = info[i * 5 + 2] & 0x0000001F; // hour
                                    temp[4] = info[i * 5 + 3] & 0x0000003F; // minute
                                    temp[5] = (info[i * 5 + 2] & 0x80) >> 5 | info[i * 5 + 4]; //current

                                    tempData.Add(temp);
                                }

                                value = tempData.ToArray();

                                break;
                            }
                        case (byte)BatteryCmd.CMD_RTC_REG_DUMP:
                            {
                                int[] tempData = new int[7];

                                //tempData[6] = info[7]; // Control Register
                                //tempData[5] = ((info[0] >> 4) & 0x07) * 10 | (info[0] & 0x0F);//sec
                                //tempData[4] = ((info[1] >> 4) & 0x07) * 10 | (info[1] & 0x0F);//min
                                //tempData[3] = ((info[2] >> 4) & 0x03) * 10 | (info[2] & 0x0F);//Hour
                                //tempData[2] = ((info[4] >> 4) & 0x03) * 10 | (info[4] & 0x0F);//day 
                                //tempData[1] = ((info[5] >> 4) & 0x01) * 10 | (info[5] & 0x0F);//month
                                //tempData[0] = ((info[6] >> 4)) * 10 | (info[6] & 0x0F) + DEFAULTYEAR;//year

                                tempData[6] = info[7]; // Control Register
                                tempData[5] = int.Parse((info[0] & 0x7F).ToString("X"));//sec
                                tempData[4] = int.Parse((info[1] & 0x7F).ToString("X"));//min
                                tempData[3] = int.Parse((info[2] & 0x3F).ToString("X"));//Hour
                                tempData[2] = int.Parse((info[4] & 0x3F).ToString("X"));//day 
                                tempData[1] = int.Parse((info[5] & 0x1F).ToString("X"));//month
                                tempData[0] = int.Parse((info[6]).ToString("X")) + DEFAULTYEAR;//year

                                value = tempData;

                                break;
                            }
                        case (byte)BatteryCmd.CMD_PID_SET:
                        case (byte)BatteryCmd.CMD_CID_SET:
                        case (byte)BatteryCmd.CMD_OID_SET:
                        case (byte)BatteryCmd.CMD_UID_SET:
                        case (byte)BatteryCmd.CMD_LOCK_STATUS_SET:
                        case (byte)BatteryCmd.CMD_MCUEEP_SET:
                        case (byte)BatteryCmd.CMD_SET_YEAR:
                        case (byte)BatteryCmd.CMD_SET_MONTH:
                        case (byte)BatteryCmd.CMD_SET_DAY:
                        case (byte)BatteryCmd.CMD_SET_HOUR:
                        case (byte)BatteryCmd.CMD_SET_MIN:
                        case (byte)BatteryCmd.CMD_SET_SEC:
                            {
                                value = true; // 成功回傳
                                break;
                            }
                        case (byte)BatteryCmd.CMD_APP_SWITCH_TO_EEP_SET:
                            {
                                switch (VersionType)
                                {
                                    case EncapsulationType.VVersion:
                                        {
                                            value = true; // 成功回傳
                                            break;
                                        }
                                }
                                break;
                            }
                        case (byte)BatteryCmd.CMD_SET_CAN_MODE_STATUS:
                        case (byte)BatteryCmd.CMD_GET_CAN_MODE_STATUS:
                            {
                                if (info == null)
                                {
                                    value = "No Support";
                                    break;
                                }
                                else
                                {
                                    value = info[0];
                                }
                                break;
                            }
                        case (byte)BatteryCmd.CMD_CAPACITY_REC_CLR:
                        case (byte)BatteryCmd.CMD_MCU_OPTION_BYTE_SET:
                        case (byte)BatteryCmd.CMD_FUNC_STATUS_GET:
                        case (byte)BatteryCmd.CMD_ALL_OZ_RESET_SET:
                        case (byte)BatteryCmd.CMD_OZ1_BYTE_EEPROM_SET:
                        case (byte)BatteryCmd.CMD_OZ2_BYTE_EEPROM_SET:
                        case (byte)BatteryCmd.CMD_OZ1_BYTE_OP_SET:
                        case (byte)BatteryCmd.CMD_OZ2_BYTE_OP_SET:
                        case (byte)BatteryCmd.CMD_RESET_BATTERY_MCU:
                        case (byte)BatteryCmd.CMD_RESET_PBPOWER:
                        case (byte)BatteryCmd.CMD_SHUTDOWN_MCUOZ:
                            {
                                //No Use
                                value = true;
                                break;
                            }
                        case (byte)BatteryCmd.CMD_MCUEEP_GET:
                            {
                                value = info;
                                //int EEP1_Len, EEP2_Len;
                                //if ((Len - 2 + m_CmdParam) > 128)
                                //{//-2: para & cmd_code
                                //    EEP1_Len = 128 - Para;
                                //    EEP2_Len = Len - 2 - EEP1_Len;
                                //}
                                //else
                                //{
                                //    EEP1_Len = Len - 2;
                                //    EEP2_Len = 0;
                                //}
                                //for (int i = 0; i < EEP1_Len; i++)
                                //    m_baryMCUEE1[m_CmdParam + i] = Info[i];
                                //for (int i = 0; i < EEP2_Len; i++)
                                //    m_baryMCUEE2[i] = Info[EEP1_Len + i];
                                break;
                            }
                        case (byte)BatteryCmd.CMD_CIPHER_CARRIER:
                            {
                                break;
                            }
                        case (byte)BatteryCmd.CMD_CIPHER_KEY_RENEW:
                            {
                                // Not Implement

                                //KeyRenew();
                                //{//For Debug
                                //    string strDisp = Encoding.ASCII.GetString(LcmBattInfo[MASTER_PACK_ARRARY_ID].CipherKey);
                                //    Debug.Print("New Session Key: " + strDisp);
                                //    strDisp = null;
                                //}
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                //}
                //else
                //{
                //    // Error
                //}
            }

            return value;
        }

        private double OZ_CheckExternalTempTabel(double dbExternADC)
        {
            double dbTemp = 0;
            int iLowBound = -1;

            if (dbExternADC == 0) dbExternADC = 1231;

            for (int i = 0; i < 25; i++)
            {
                if (dbaryADCTable[i] <= dbExternADC) //find out the interval of ADC
                {
                    iLowBound = i;
                    break;
                }
            }
            if (iLowBound == -1)
            {
                iLowBound = 24;
                dbTemp = dbaryDegCTable[iLowBound];
            }
            else if (iLowBound == 0)
            {
                dbTemp = dbaryDegCTable[iLowBound];
            }
            else
            {
                double dbUpper = dbaryADCTable[iLowBound - 1] - (double)dbExternADC;
                double dbLower = dbaryADCTable[iLowBound] - (double)dbExternADC;

                dbTemp = (dbLower * dbaryDegCTable[iLowBound - 1] - dbUpper * dbaryDegCTable[iLowBound]) / (dbLower - dbUpper);
            }

            return dbTemp;
        }

        private double OZ_CountInternalTemp(double dbInternalADC, int iOzNumb)
        {
            dbInternalADC = (dbInternalADC * 2500) / 4096;

            double dbITv = 150;
            if (mPBEEP != null && mPBEEP[iOzNumb] != null)
            {
                dbITv = (double)mPBEEP[iOzNumb][2];
            }
            else if (mPBEEP != null && mPBEEP[0] != null)
            {
                dbITv = (double)mPBEEP[0][2];
            }
            dbITv += 400;

            return (((dbInternalADC - dbITv) / 2.0976) + 30);
        }
    }
}
