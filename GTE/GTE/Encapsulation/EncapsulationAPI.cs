﻿using GTE.Encryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.Encapsulation
{
    public abstract class EncapsulationAPI
    {
        #region Property
        protected byte mDataHeader { get; set; }
        protected byte mDataFooter { get; set; }
        protected byte mEncryptCode { get; set; }
        protected EncryptionAPI mEncryptionAlgorithm { get; set; }
        protected bool mEnableEncryption { get; set; }
        #endregion
        #region Encryption Method
        /// <summary>
        /// Set Encryption Key
        /// </summary>
        /// <param name="encryptionKey"></param>
        public void SetEncryptionKey(string encryptionKey)
        {
            if (mEncryptionAlgorithm != null)
            {
                mEncryptionAlgorithm.SetEncryptionKey(encryptionKey);
            }
        }
        /// <summary>
        /// Set Encryption Code Number
        /// </summary>
        /// <param name="encryptCode"></param>
        public void SetEncryptionCode(byte encryptCode)
        {
            mEncryptCode = encryptCode;
        }
        /// <summary>
        /// Enable Encryption
        /// </summary>
        /// <param name="enableEncryption"></param>
        public void EnableEncryption(bool enableEncryption)
        {
            mEnableEncryption = enableEncryption;
        }
        /// <summary>
        /// Set Encryption Algorithm By Device Version
        /// </summary>
        /// <param name="deviceVer">Device Version</param>  
        public abstract void SetEncryptionAlgorithm(string deviceVer);
        #endregion
        #region Encapsulation/DataDecapsulation
        /// <summary>
        /// Data Encapsulation
        /// </summary>
        /// <param name="cmdInfo"></param>
        /// <returns></returns>
        public abstract GTE.InfoModel.SendInfo DataEncapsulation(object cmdInfo);
        /// <summary>
        /// Data Decapsulation
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public abstract byte[] DataDecapsulation(byte[] data);
        #endregion
    }
}
