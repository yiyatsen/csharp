﻿using GTE.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.Encapsulation
{
    public class EncapsulationV : EncapsulationAPI
    {
        public EncapsulationV()
        {
            mDataHeader = 0x00;
            mDataFooter = 0xFF;
        }

        public override void SetEncryptionAlgorithm(string deviceVer)
        {
            mEncryptionAlgorithm = null;
        }

        public override SendInfo DataEncapsulation(object cmdInfo)
        {
            BatteryCmdInfo batteryCmdInfo = (BatteryCmdInfo)cmdInfo;

            SendInfo value = null;
            if (batteryCmdInfo != null)
            {
                value = new SendInfo();
                value.DataHeader = mDataHeader;
                value.DataFooter = mDataFooter;
                value.DelayTime = batteryCmdInfo.MinWaitingTime;

                List<byte> cmdPacket = new List<byte>();
                byte cmdCode = batteryCmdInfo.CmdCode;
                uint cmdLen = batteryCmdInfo.BaseLength;
                byte para = batteryCmdInfo.Para;
                byte[] info = batteryCmdInfo.Info;

                cmdPacket.Add(Convert.ToByte(cmdLen));
                cmdPacket.Add(para);
                cmdPacket.Add(cmdCode);

                if (info != null)
                {
                    for (int i = 0; i < info.Length; i++)
                    {
                        cmdPacket.Add(info[i]);
                    }
                }

                //cmdPacket.Add(mDataFooter); // Add Footer

                value.DataStream = cmdPacket.ToArray();
            }
            return value;
        }

        public override byte[] DataDecapsulation(byte[] data)
        {
            byte[] result = null;

            if (data != null && data.Length > 0)
            {
                List<byte> outputData = new List<byte>();

                for (int i = 0; i < data.Length; i++)
                {
                    if (data[i].Equals(mDataFooter) && (data.Length - 1) == i)
                    {
                        break;
                    }
                    outputData.Add(data[i]);
                }
                //len + para + cmd + leninfo
                result = outputData.ToArray(); ;
            }
            else
            {
                // Null Data
            }

            return result;
        }
    }
}
