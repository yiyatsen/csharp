﻿using GTE.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.Encapsulation
{
    public class EncapsulationDCON : EncapsulationAPI
    {
        private byte mDataHeader2;
        public EncapsulationDCON()
        {
            mDataHeader2 = 0x3E;
            mDataHeader = 0x21; // Dynamic
            mDataFooter = 0x0D;
        }

        public override void SetEncryptionAlgorithm(string deviceVer)
        {
            mEncryptionAlgorithm = null;
        }

        public override SendInfo DataEncapsulation(object cmdInfo)
        {
            SendInfo result = null;
            DCONCmdInfo dconCmdInfo = (DCONCmdInfo)cmdInfo;
            if (dconCmdInfo != null)
            {
                result = new SendInfo();
                result.DataHeader = dconCmdInfo.DataHeader; //需要少兩個Byte
                result.DataFooter = dconCmdInfo.DataFooter;
                result.Timeout = dconCmdInfo.Timeout;
                result.DelayTime = dconCmdInfo.DelayTime;

                if (dconCmdInfo.DataFooter == 0)
                {
                    result.DataFooter = mDataFooter;
                }
                result.DataStream = dconCmdInfo.DataStream;
            }

            return result;
        }

        public override byte[] DataDecapsulation(byte[] data)
        {
            byte[] result = data;
            if (data != null && data.Length > 0)
            {
                if (data[0] == mDataHeader || data[0] == mDataHeader2)
                {
                    if (data[data.Length - 1] == mDataFooter)
                    {
                        result = new byte[data.Length - 2];
                        Array.Copy(data, 1, result, 0, data.Length - 2);
                    }
                }
            }
            return result;
        }
    }
}
