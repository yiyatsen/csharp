﻿using GTE.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GTE.Encapsulation
{
    public class EncapsulationMP : EncapsulationAPI
    {
        public EncapsulationMP()
        {
            mDataHeader = 0x02;
            mDataFooter = 0x03;
        }

        public override void SetEncryptionAlgorithm(string deviceVer)
        {
            if (deviceVer != null && !deviceVer.Equals(""))
            {
                Regex regex = new Regex(@"^V");
                if (regex.IsMatch(deviceVer)) // V版為非加密
                {
                    mEncryptionAlgorithm = null;
                }
                else
                {
                    regex = new Regex(@"^MPS");
                    if (regex.IsMatch(deviceVer))
                    {
                        mEncryptionAlgorithm = new GTE.Encryption.EncryptionAES();
                        mEnableEncryption = true;
                        mEncryptionAlgorithm.SetEncryptionKey("GTE Default Key!"); //MPS預設加密密鑰
                    }
                    else
                    {
                        regex = new Regex(@"^MPM");
                        if (regex.IsMatch(deviceVer))
                        {
                            mEncryptionAlgorithm = new GTE.Encryption.EncryptionAES();
                            mEnableEncryption = true;
                            mEncryptionAlgorithm.SetEncryptionKey("qswdefrgthyjukil"); //MPM預設加密密鑰
                        }
                        else
                        {
                            mEncryptionAlgorithm = null;
                        }
                    }
                }
            }
            else
            {
                mEncryptionAlgorithm = null;
            }
        }

        public override SendInfo DataEncapsulation(object cmdInfo)
        {
            BatteryCmdInfo batteryCmdInfo = (BatteryCmdInfo)cmdInfo;

            SendInfo value = null;
            if (batteryCmdInfo != null)
            {
                value = new SendInfo();
                value.DataHeader = mDataHeader;
                value.DataFooter = mDataFooter;
                value.DelayTime = batteryCmdInfo.MinWaitingTime;

                uint checksum = 0;
                List<byte> cmdPacket = new List<byte>();
                byte cmdCode = batteryCmdInfo.CmdCode;
                uint cmdLen = batteryCmdInfo.BaseLength;
                byte para = batteryCmdInfo.Para;
                byte[] info = batteryCmdInfo.Info;

                cmdPacket.Add(mDataHeader); // Add Header

                if (cmdLen != 0) // Original Protocol
                {
                    if (mEnableEncryption && mEncryptionAlgorithm != null && batteryCmdInfo.IsEncryption)//需要加密
                    {
                        // Encrypt step
                        //  +2 ==  Header + Footer
                        uint cmdLengthForEncrypt = (((cmdLen + 16) >> 4) << 4);
                        if (cmdLen > 16)
                        {
                            cmdLengthForEncrypt = ((cmdLen >> 4) << 4);
                        }

                        byte[] encryptInputData = new byte[cmdLengthForEncrypt];
                        //Encrypt Info
                        encryptInputData[0] = (byte)cmdLen;
                        encryptInputData[1] = para;
                        encryptInputData[2] = cmdCode;

                        // Original data not empty
                        if (info != null)
                        {
                            Array.Resize(ref info, (int)cmdLengthForEncrypt);
                            Array.Copy(info, 0, encryptInputData, 3, encryptInputData.Length - 3); // reduce len (cmdlen + para + cmdCode)
                        }

                        if (mEnableEncryption && mEncryptionAlgorithm != null)
                        {
                            info = mEncryptionAlgorithm.EncryptionData(encryptInputData);
                        }

                        cmdCode = mEncryptCode; // Encrypt CMD
                        para = 0x00;

                        cmdLen = cmdLengthForEncrypt + 2;
                    }

                    // Add Cmd Length to buffer
                    cmdPacket.Add(ConvertByteToASCIICode((byte)(cmdLen >> 4)));
                    cmdPacket.Add(ConvertByteToASCIICode((byte)(cmdLen & 0x0F)));

                    checksum = cmdLen;
                }
                else
                {
                    if (mEnableEncryption && mEncryptionAlgorithm != null && batteryCmdInfo.IsEncryption) // 需要加密
                    {

                    }
                    else
                    {

                    }
                }

                // Add Parameter to buffer
                cmdPacket.Add(ConvertByteToASCIICode((byte)(para >> 4)));
                cmdPacket.Add(ConvertByteToASCIICode((byte)(para & 0x0F)));
                // Add Cmd Code to buffer
                cmdPacket.Add(ConvertByteToASCIICode((byte)(cmdCode >> 4)));
                cmdPacket.Add(ConvertByteToASCIICode((byte)(cmdCode & 0x0F)));

                // Add param Data to buffer
                if ((info != null) && (2076 >= ((info.Length << 1) + 12)))
                {
                    for (int i = 0; i < info.Length; i++)
                    {
                        cmdPacket.Add(ConvertByteToASCIICode((byte)(info[i] >> 4)));
                        cmdPacket.Add(ConvertByteToASCIICode((byte)(info[i] & 0x0F)));
                        checksum += (uint)info[i];
                    }
                }

                checksum += Convert.ToUInt16(para + cmdCode);
                // Add checksum to buffer
                cmdPacket.Add(ConvertByteToASCIICode((byte)((checksum >> 12) & 0x0F)));
                cmdPacket.Add(ConvertByteToASCIICode((byte)((checksum >> 8) & 0x0F)));
                cmdPacket.Add(ConvertByteToASCIICode((byte)((checksum >> 4) & 0x0F)));
                cmdPacket.Add(ConvertByteToASCIICode((byte)((checksum >> 0) & 0x0F)));

                cmdPacket.Add(mDataFooter); // Add Footer

                value.DataStream = cmdPacket.ToArray();
            }
            return value;
        }

        public override byte[] DataDecapsulation(byte[] data)
        {
            byte[] result = null;

            if (data != null && data.Length > 0)
            {
                List<byte> outputData = new List<byte>();

                byte encryptCmdCode = mEncryptCode;

                byte[] encryptCaptchaByte = new byte[6];
                byte stx = data[0];
                byte etx = data[data.Length - 1];
                int checkSum = 0;
                int checkSumCheck = 0;

                for (int i = 1; i < data.Length; i += 2)
                {
                    int footer = i + 5;
                    if (footer >= data.Length)
                    {
                        // Check sum
                        byte inThousands = ConvertASCIICodeToByte(data[i]);
                        byte inHundreds = ConvertASCIICodeToByte(data[i + 1]);
                        byte inTens = ConvertASCIICodeToByte(data[i + 2]);
                        byte inOne = ConvertASCIICodeToByte(data[i + 3]);

                        checkSumCheck = ((int)inThousands) * 16 * 16 * 16 + ((int)inHundreds) * 16 * 16 + inTens * 16 + inOne;

                        break;
                    }
                    else
                    {
                        // len, para, cmdcode
                        byte inTens = ConvertASCIICodeToByte(data[i]);
                        byte inOne = ConvertASCIICodeToByte(data[i + 1]);

                        checkSum += (int)(inTens * 16 + inOne);

                        outputData.Add((byte)(inTens * 16 + inOne));
                    }
                }
                //len + para + cmd + leninfo +checksum 
                result = outputData.ToArray();

                if (checkSum == checkSumCheck && outputData.Count >= 3)
                {
                    // Is Encrypt
                    if (outputData[2].Equals(encryptCmdCode))
                    {
                        int encryptedlen = outputData[0] - 2;
                        byte[] tempData = null;
                        Array.Resize(ref tempData, encryptedlen);

                        Array.Copy(outputData.ToArray(), 3, tempData, 0, encryptedlen);
                        if (mEnableEncryption && mEncryptionAlgorithm != null)
                        {
                            result = mEncryptionAlgorithm.DecryptionData(tempData);
                        }
                    }
                }
                else
                {
                    // Chech sum error
                }
            }
            else
            {
                // null Data
            }

            return result;
        }

        private byte ConvertByteToASCIICode(byte inputData)
        {
            if (inputData >= 10)
            {
                return (byte)(inputData - 10 + 'A');
            }
            else
            {
                return (byte)(inputData + '0');
            }
        }

        private byte ConvertASCIICodeToByte(byte inputData)
        {
            if (inputData >= 65)
            {
                return (byte)(inputData + 10 - 'A');
            }
            else if (inputData >= 48)
            {
                return (byte)(inputData - '0');
            }
            else
            {
                return inputData;
            }
        }
    }
}
