﻿using GTE.Encapsulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.Device
{
    public abstract class DeviceAPI
    {
        #region Protocol Method
        /// <summary>
        /// 開始進行連線通訊
        /// </summary>
        /// <param name="port"></param>
        /// <param name="baudRate"></param>
        /// <returns>成功/失敗</returns>
        public abstract bool Connect(string port, int baudRate);
        /// <summary>
        /// 開始進行連線通訊
        /// </summary>
        /// <param name="port"></param>
        /// <param name="baudRate"></param>
        /// <returns>成功/失敗</returns>
        public abstract bool Connect(string port, int baudRate, EncapsulationType[] encapsulationType);
        /// <summary>
        /// 關閉通訊連線
        /// </summary>
        /// <returns></returns>
        public abstract bool Disconnect();
        /// <summary>
        /// 是否連線至裝置
        /// </summary>
        /// <returns>成功/失敗</returns>
        public abstract bool IsConnect();
        /// <summary>
        /// 連線使用的Port
        /// </summary>
        /// <returns></returns>
        public abstract string GetPort();
        /// <summary>
        /// 連線使用的Baud rate
        /// </summary>
        /// <returns></returns>
        public abstract int GetBaudRate();
        #endregion
    }
}
