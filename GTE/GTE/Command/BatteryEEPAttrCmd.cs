﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.Command
{
    /// <summary>
    /// Battery EEP屬性指令列表
    /// </summary>
    public enum BatteryEEPAttrCmd
    {
        Ver,
        SCTH,
        SCDelay,
        OCTH,
        OCDelayTime,

        OVTH,
        OVRelease,


        EDV2Value,
        ManufactureInfo,
        ManufactureDate,
        ManufactureWeekOfYear,
        SerialNumber,
        ProjectName,
        FactoryName
    }
}
