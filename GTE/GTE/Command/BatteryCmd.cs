﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.Command
{
    /// <summary>
    /// GTE指令列表
    /// </summary>
    public enum BatteryCmd : byte
    {
        CMD_OZ1_EEPROM_GET = 0x00,
        CMD_OZ2_EEPROM_GET = 0x01,
        CMD_MCU_EEPROM_H_GET = 0x02,
        CMD_MCU_EEPROM_L_GET = 0x03,
        CMD_VOLTAGE_GET = 0x04,
        CMD_CURRENT_GET = 0x05,
        CMD_REMAIN_CAPACITY_GET = 0x06,
        CMD_RSOC_GET = 0x07,
        CMD_CYCLE_COUNT_GET = 0x08,
        CMD_TEMP_GET = 0x09,
        CMD_PROTECTION_GET = 0x0A,
        CMD_DESIGN_CAPACITY_GET = 0x0B,
        CMD_DESIGN_VOLTAGE_GET = 0x0C,
        CMD_FULL_CC_GET = 0x0D,
        CMD_DEVICE_NAME_GET = 0x0E,
        CMD_MANUFACTURE_DATA_GET = 0x0F,
        CMD_EEP_VERSION_GET = 0x10,
        CMD_BATTERY_NO_GET = 0x11,
        CMD_MANUFACTURE_DATE_GET = 0x12,
        CMD_RESET_COUNT_GET = 0x13,
        CMD_MCU_VERSION_GET = 0x14,
        CMD_CURRENT_GAIN = 0x15,
        CMD_CURRENT_OFFSET = 0x16,
        CMD_EDV2_TIME = 0x17,
        CMD_TAPER_CURRENT_GET = 0x18,
        CMD_CELL_NUMBER_GET = 0x19,
        CMD_CHEMISTRY_GET = 0x1A,
        CMD_MANUFACTURE_NAME_GET = 0x1B,
        CMD_SIMULATION_FCC_GET = 0x1C,            //CMD_EDV2_VOL_GET = 0x1C,
        CMD_BARCODE_DATA_GET = 0x1D,
        CMD_GET_YEAR = 0x20,
        CMD_GET_MONTH = 0x21,
        CMD_GET_DAY = 0x22,
        CMD_GET_HOURS = 0x23,
        CMD_GET_MIN = 0x24,
        CMD_GET_SEC = 0x25,
        CMD_GET_1224 = 0x26,
        CMD_GET_TIMESET = 0x27,
        CMD_GET_LAST_CHARGE_END = 0x28,
        CMD_GET_LAST_DISCHARGE_END = 0x29,
        CMD_RTC_REG_DUMP = 0x2A,
        CMD_GET_LAST_CHARGE_START = 0x2B,
        CMD_GET_LAST_DISCHARGE_START = 0x2C,
        CMD_GET_CHARGE_LOG_00_22 = 0x2D,
        CMD_GET_CHARGE_LOG_23_45 = 0x2E,
        CMD_GET_CHARGE_LOG_46_49 = 0x2F,
        CMD_PID_GET = 0x30,
        CMD_PID_SET = 0x31,
        CMD_CID_GET = 0x32,
        CMD_CID_SET = 0x33,
        CMD_OID_GET = 0x34,
        CMD_OID_SET = 0x35,
        CMD_UID_GET = 0x36,
        CMD_UID_SET = 0x37,
        CMD_LOCK_STATUS_GET = 0x38,
        CMD_LOCK_STATUS_SET = 0x39,
        CMD_CAPACITY_REC_GET = 0x3A,
        CMD_CAPACITY_REC_CLR = 0x3B,
        CMD_CAPACITY_CHARGING_GET = 0x3C,
        CMD_APP_SWITCH_TO_EEP_SET = 0x3E, //only for 26K22
        CMD_FUNC_STATUS_GET = 0x3F,
        CMD_MCU_FLAG_GET = 0x50,
        CMD_MCU_OPTION_BYTE_SET = 0x51,
        CMD_MCU_OPTION_BYTE_GET = 0x52,
        CMD_MCU_FLASH_PROGRAM = 0x53,
        CMD_GET_LAST_FCC_INFO = 0x61,
        CMD_GET_DISCHARGE_LOG = 0x62,
        CMD_GET_CRATE_INFO = 0x63,
        CMD_GET_LAST_RESET_RTC_INFO = 0x64,
        CMD_MCU_EEPROM_GET = 0x65,
        CMD_GET_TEMPERATURE_INTERVAL = 0x66,
        CMD_MCUEEP_SET = 0x91,
        CMD_ALL_OZ_RESET_SET = 0x92,
        CMD_MCUEEP_GET = 0x93,
        CMD_OZ1_BYTE_EEPROM_GET = 0x97,
        CMD_OZ2_BYTE_EEPROM_GET = 0x98,
        CMD_OZ1_BYTE_OP_GET = 0x99,
        CMD_OZ2_BYTE_OP_GET = 0x9A,
        CMD_OZ1_BYTE_EEPROM_SET = 0x9C,
        CMD_OZ2_BYTE_EEPROM_SET = 0x9D,
        CMD_OZ1_BYTE_OP_SET = 0x9E,
        CMD_OZ2_BYTE_OP_SET = 0x9F,
        CMD_SET_YEAR = 0xA0,
        CMD_SET_MONTH = 0xA1,
        CMD_SET_DAY = 0xA2,
        CMD_SET_HOUR = 0xA3,
        CMD_SET_MIN = 0xA4,
        CMD_SET_SEC = 0xA5,
        CMD_SET_1224 = 0xA6,

        CMD_GET_LAST_START_CHG_TIME = 0xA7, //duplicated
        CMD_GET_LAST_STOP_CHG_TIME = 0xA8, //duplicated 

        CMD_RESET_BATTERY_MCU = 0xAA,
        CMD_RESET_CYCLE = 0xAB,
        CMD_GET_NRCYCLE = 0xAC,
        CMD_RESET_PBPOWER = 0xAD,
        CMD_SHUTDOWN_MCUOZ = 0xAE,
        CMD_GET_OV_HAPPENTIME = 0xAF,
        CMD_GET_UV_HAPPENTIME = 0xB0,
        CMD_GET_COC_HAPPENTIME = 0xB1,
        CMD_GET_SC_HAPPENTIME = 0xB2,
        CMD_GET_OT_HAPPENTIME = 0xB3,
        CMD_GET_UT_HAPPENTIME = 0xB4,
        CMD_GET_DOC_HAPPENTIME = 0xB5,

        /* YuKC, 2014/2/5, Implement secure shell, >>> */
        CMD_CIPHER_CARRIER = 0xC0,
        CMD_CIPHER_UUID_GET = 0xC1,
        CMD_CIPHER_ACCEPT = 0xC2,
        CMD_CIPHER_KEY_RENEW = 0xC3,
        /* YuKC, 2014/2/5, Implement secure shell, <<< */

        // Add by Neil 2017.03.29
        CMD_GET_CAN_MODE_STATUS = 0xCA,
        CMD_SET_CAN_MODE_STATUS = 0xCB,
        
        CMD_GTE_DEBUGMSG = 0xF1,
        CMD_GTE_INVALID = 0xFF
    }
}
