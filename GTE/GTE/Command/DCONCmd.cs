﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.Command
{
    public enum DCONCmd
    {
        ReadBattery,
        ReadDIDOStatus,
        ReadModuleName,
        ReadModuleConfig,
        ReadModuleStatus,
        SetModuleConfig, // Only Support 7065
        SetModuleAddress, // Only Support 7521
        SetModuleBaudRate, // Only Support 7521
    }
}
