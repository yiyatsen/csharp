﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.Command
{
    public enum BatteryErrorCmd : byte
    {
        NOERROR = 0,
        CHECKSUMERROR = 2,
        INVALIDCMDERROR = 4,
        FORMATERROR = 5,
        DATAERROR = 6,
        FLASHWRITIN = 7,
        INVALIDADDRESS = 0x90,
        COMMUNICATEERROR = 0x91,
        UNKNOW
    }
}
