﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.InfoModel
{
    public class SendInfo
    {
        private const int DEFAULTTIME = 100;

        public byte DataHeader { get; set; }
        public byte DataFooter { get; set; }
        public int DelayTime { get; set; }
        public int Timeout { get; set; }
        public byte[] DataStream { get; set; }

        public SendInfo()
        {
            DelayTime = DEFAULTTIME;
            Timeout = DEFAULTTIME;
        }
    }
}
