﻿using GTE.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.InfoModel
{
    public class BatteryEEPAttrInfo
    {
        private BatteryEEPAttrCmd mEEPAttrCmd { get; set; }
        public BatteryEEPAttrCmd EEPAttrCmd { get { return mEEPAttrCmd; } }

        private Dictionary<string, BatteryAttrAddressInfo> mAttrAddressInfoList { get; set; }
        public Dictionary<string, BatteryAttrAddressInfo> AttrAddressInfoList { get { return mAttrAddressInfoList; } }

        public BatteryEEPAttrInfo(BatteryEEPAttrCmd eepAttrCmd, Dictionary<string, BatteryAttrAddressInfo> attrAddressInfoList)
        {
            mEEPAttrCmd = eepAttrCmd;
            mAttrAddressInfoList = attrAddressInfoList;
        }
    }
}
