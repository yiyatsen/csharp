﻿using GTE.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.InfoModel
{
    public class DCONCmdInfo : SendInfo
    {
        public DCONCmd Cmd { get; set; }
    }
}
