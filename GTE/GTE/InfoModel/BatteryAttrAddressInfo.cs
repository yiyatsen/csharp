﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.InfoModel
{
    public class BatteryAttrAddressInfo
    {
        public const string MAINATTRIBUTENAME = "MainAttr";

        private int mLocation { get; set; }
        public int Location { get { return mLocation; } }
        private int mSize { get; set; }
        public int Size { get { return mSize; } }

        public byte[] OutputData { get; set; }

        public BatteryAttrAddressInfo(int location, int size)
        {
            mLocation = location;
            mSize = size;
        }
    }
}
