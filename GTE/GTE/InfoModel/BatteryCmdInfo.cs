﻿using GTE.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.InfoModel
{
    public class BatteryCmdInfo
    {
        private const int DEFAULTWAITINGTIME = 80;
        private const uint DEFAULTLENGTH = 0x02;

        private BatteryCmd mCmd { get; set; }
        public BatteryCmd Cmd { get { return mCmd; } }
        private byte mCmdCode { get; set; }
        public byte CmdCode { get { return mCmdCode; } }
        private uint mBaseLength = DEFAULTLENGTH;
        public uint BaseLength { get { return mBaseLength; } set { if (!mIsParaReadOnly) { mBaseLength = value; } } }

        private bool mIsEncryption = true;
        public bool IsEncryption { get { return mIsEncryption; } }

        private int mMinWaitingTime = DEFAULTWAITINGTIME;
        public int MinWaitingTime { get { return mMinWaitingTime; } }

        private bool mIsParaReadOnly = true;
        public bool IsParaReadOnly { get { return mIsParaReadOnly; } }

        private byte mPara { get; set; }
        public byte Para { get { return mPara; } set { if (!mIsParaReadOnly) { mPara = value; } } }
        private byte[] mInfo { get; set; }
        public byte[] Info { get { return mInfo; } set { if (!mIsParaReadOnly) { mInfo = value; } } }

        public BatteryCmdInfo(byte cmd, uint cmdLength, bool isParaReadOnly, int waitingTime)
        {
            mCmd = BatteryCmd.CMD_GTE_INVALID;
            mCmdCode = cmd;
            mBaseLength = cmdLength;
            mIsParaReadOnly = isParaReadOnly;
            mMinWaitingTime = waitingTime;
        }

        public BatteryCmdInfo(BatteryCmd cmd)
        {
            mCmd = cmd;
            mCmdCode = (byte)cmd;
        }

        public BatteryCmdInfo(BatteryCmd cmd, bool isEncryption)
        {
            mCmd = cmd;
            mCmdCode = (byte)cmd;
            mIsEncryption = isEncryption;
        }

        public BatteryCmdInfo(BatteryCmd cmd, int waitingTime)
        {
            mCmd = cmd;
            mCmdCode = (byte)cmd;
            mMinWaitingTime = waitingTime;
        }

        public BatteryCmdInfo(BatteryCmd cmd, bool isEncryption, int waitingTime)
        {
            mCmd = cmd;
            mCmdCode = (byte)cmd;
            mIsEncryption = isEncryption;
            mMinWaitingTime = waitingTime;
        }

        public BatteryCmdInfo(BatteryCmd cmd, uint cmdLength, bool isParaReadOnly)
        {
            mCmd = cmd;
            mCmdCode = (byte)cmd;
            mBaseLength = cmdLength;
            mIsParaReadOnly = isParaReadOnly;
        }

        public BatteryCmdInfo(BatteryCmd cmd, uint cmdLength, bool isParaReadOnly, int waitingTime)
        {
            mCmd = cmd;
            mCmdCode = (byte)cmd;
            mBaseLength = cmdLength;
            mIsParaReadOnly = isParaReadOnly;
            mMinWaitingTime = waitingTime;
        }

        public BatteryCmdInfo(BatteryCmd cmd, bool isEncryption, uint cmdLength, bool isParaReadOnly, int waitingTime)
        {
            mCmd = cmd;
            mCmdCode = (byte)cmd;
            mIsEncryption = isEncryption;
            mBaseLength = cmdLength;
            mIsParaReadOnly = isParaReadOnly;
            mMinWaitingTime = waitingTime;
        }
    }
}
