﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace GTE.Encryption
{
    public class EncryptionAES : EncryptionAPI
    {
        private AesManaged aesAlg = new AesManaged
        {
            KeySize = 128,
            BlockSize = 128,
            Mode = CipherMode.ECB,
            Padding = PaddingMode.Zeros,
            IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
        };

        public override void SetEncryptionKey(string encryptionKey)
        {
            aesAlg.Key = System.Text.Encoding.ASCII.GetBytes(encryptionKey);
        }

        public override byte[] EncryptionData(byte[] inputData)
        {
            int inputDataLength = inputData.Length;
            byte[] outputData = new byte[inputData.Length];

            try
            {
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                outputData = encryptor.TransformFinalBlock(inputData, 0, inputData.Length);
            }
            catch (Exception ex)
            {
                //GTECmd.Message.ErrorLog.WriteErrorLog(Message.MessageType.Error_EncryptionData, ex.Message);
            }
            return outputData;
        }

        public override byte[] DecryptionData(byte[] inputData)
        {
            byte[] outputData = new byte[inputData.Length];
            try
            {
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                outputData = decryptor.TransformFinalBlock(inputData, 0, inputData.Length);
            }
            catch (Exception ex)
            {
                //GTECmd.Message.ErrorLog.WriteErrorLog(Message.MessageType.Error_EncryptionData, ex.Message);
            }
            return outputData;
        }
    }
}
