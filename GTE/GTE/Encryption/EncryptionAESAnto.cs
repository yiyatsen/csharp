﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace GTE.Encryption
{
    public class EncryptionAESAnto : EncryptionAPI
    {
        private Aes encryptor = Aes.Create();

        public EncryptionAESAnto()
            : base()
        {
            Rfc2898DeriveBytes pdb = 
                new Rfc2898DeriveBytes("PKCDEGAWQXVBWVB"
                    , new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
        }

        public override void SetEncryptionKey(string encryptionKey)
        {
            throw new NotImplementedException();
        }

        public override byte[] EncryptionData(byte[] inputData)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(inputData, 0, inputData.Length);
                    cs.Close();
                }
                inputData = ms.ToArray();
            }
            return inputData;
        }

        public override byte[] DecryptionData(byte[] inputData)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(inputData, 0, inputData.Length);
                    cs.Close();
                }

                inputData = ms.ToArray();
            }
            return inputData;
        }
    }
}
