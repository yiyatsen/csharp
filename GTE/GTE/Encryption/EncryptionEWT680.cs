﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace GTE.Encryption
{
    public class EncryptionEWT680 : EncryptionAPI
    {
        private RijndaelManaged mRij { get; set; }
        private byte[] mIV = new byte[] { 219, 16, 241, 218, 71, 10, 119, 104, 129, 247, 34, 38, 36, 76, 165, 0 };
        private byte[] mKey = new byte[] { 193, 108, 8, 218, 81, 158, 131, 255, 173, 153, 36, 216, 132, 252, 20, 201,
                                                                        122, 108, 240, 114, 41, 130, 194, 20, 1 ,55, 148, 124, 251, 208, 152, 0};

        public EncryptionEWT680()
            : base()
        {
            mRij = new RijndaelManaged();
            mRij.Key = mKey;
            mRij.IV = mIV;
        }

        public override void SetEncryptionKey(string encryptionKey)
        {
            throw new NotImplementedException();
        }

        public override byte[] EncryptionData(byte[] inputData)
        {
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, mRij.CreateEncryptor(this.mRij.Key, this.mRij.IV), CryptoStreamMode.Write);

            cryptoStream.Write(inputData, 0, inputData.Length);
            cryptoStream.Close();
            inputData = memoryStream.ToArray();
           memoryStream.Close();

            return inputData;
        }

        public override byte[] DecryptionData(byte[] inputData)
        {
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, mRij.CreateDecryptor(this.mRij.Key, this.mRij.IV), CryptoStreamMode.Write);
            cryptoStream.Write(inputData, 0, inputData.Length);
            cryptoStream.Close();
            inputData = memoryStream.ToArray();
            memoryStream.Close();

            return inputData;
        }

        public byte[] EncryptionStringDataToBytes(string data)
        {
            return Encoding.Unicode.GetBytes(data);
        }

        public byte[] DecryptionStringDataToBytes(string data)
        {
            int num = data.Length / 2;
            byte[] array = new byte[num];
            for (int i = 0; i < num; i++)
            {
                array[i] = Convert.ToByte(data.Substring(2 * i, 2), 16);
            }
            return array;
        }

        public string EncryptionBytesDataToString(byte[] data)
        {
            int num = data.Length;
            string text = "";
            for (int i = 0; i < num; i++)
            {
                text += data[i].ToString("X2");
            }
            return text;
        }

        public string DecryptionBytesDataToString(byte[] data)
        {
            return Encoding.Unicode.GetString(data);
        }
    }
}
