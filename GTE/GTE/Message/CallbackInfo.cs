﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.Message
{
    public class CallbackInfo
    {
        public CallbackType Type { get; set; }
        public CallbackStatus Status { get; set; }
        public object Data { get; set; }
    }
}
