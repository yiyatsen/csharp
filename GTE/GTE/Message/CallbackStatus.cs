﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.Message
{
    public enum CallbackStatus
    {
        Success,
        Fail,
        WriteToDeviceError,
        DataFormatError
    }
}
