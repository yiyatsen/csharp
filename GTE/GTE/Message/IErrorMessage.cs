﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.Message
{
    public interface IErrorMessage
    {              
        event GTE.Message.MessageEvent.ErrorMessageHandler ErrorMessageEvent;
    }
}
