﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.Message
{
    public enum CallbackType
    {
        ConnectDevice,
        DisconnectDevice,
        DisconnectDeviceFromError,
        GetDeviceInformation,
        GetDeviceBarcode,
        SetDeviceBarcode,
        GetDeviceRTCTime,
        SetDeviceRTCTime
    }
}
