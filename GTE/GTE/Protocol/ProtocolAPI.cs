﻿using GTE.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTE.Protocol
{
    public abstract class ProtocolAPI : IDisposable, IErrorMessage
    {
        #region Method
        /// <summary>
        /// Get Port Name
        /// </summary>
        /// <returns>Port Name</returns>
        public abstract string GetPort();
        /// <summary>
        /// Get Baud Rate
        /// </summary>
        /// <returns>Baud Rate</returns>
        public abstract int GetBaudRate();
        /// <summary>
        /// Set Baud Rate
        /// </summary>
        /// <param name="baudRate">Baud Rate</param>
        /// <returns></returns>
        public abstract int SetBaudRate(int baudRate);
        /// <summary>
        /// Connect to Device
        /// </summary>
        /// <param name="port">Connected Port</param>
        /// <param name="baudRate">Connected Baud Rate</param>
        /// <returns>Success/Fail</returns>
        public abstract bool Connect(string port, int baudRate);
        /// <summary>
        /// Disconnect to Device
        /// </summary>
        /// <returns>Success/Fail</returns>
        public abstract bool Disconnect();
        /// <summary>
        /// Is Connect to Device
        /// </summary>
        /// <returns></returns>
        public abstract bool IsConnect();
        /// <summary>
        /// Set Received Data Timeout
        /// </summary>
        /// <param name="timeout"></param>
        public abstract void SetReadTimeout(int timeout);
        /// <summary>
        /// Set Send Data Timeout
        /// </summary>
        /// <param name="timeout"></param>
        public abstract void SetWriteTimeout(int timeout);
        /// <summary>
        /// Send Data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public abstract byte[] SendData(GTE.InfoModel.SendInfo data);
        /// <summary>
        /// Send Debug Data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public abstract byte[] SendDebugData(GTE.InfoModel.SendInfo data);
        /// <summary>
        /// Received Data
        /// </summary>
        public abstract void ReceiveData();
        /// <summary>
        /// Release Object
        /// </summary>
        public abstract void Dispose();
        #endregion

        event GTE.Message.MessageEvent.ErrorMessageHandler IErrorMessage.ErrorMessageEvent
        {
            add { throw new NotImplementedException(); }
            remove { throw new NotImplementedException(); }
        }
    }
}
