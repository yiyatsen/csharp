﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;

namespace GTE.Protocol
{
    public class ProtocolCOM : ProtocolAPI
    {
        #region Property
        protected bool mPrepareClose { get; set; }
        protected bool mIsSending { get; set; }
        protected bool mIsReceiving { get; set; }
        protected SerialPort mSerialPort { get; set; }
        #endregion

        public override string GetPort()
        {
            if (mSerialPort != null)
            {
                return mSerialPort.PortName;
            }

            return null;
        }

        public override int GetBaudRate()
        {
            if (mSerialPort != null)
            {
                return mSerialPort.BaudRate;
            }

            return 0;
        }

        public override int SetBaudRate(int baudRate)
        {
            if (mSerialPort != null)
            {
                return mSerialPort.BaudRate = baudRate;
            }

            return 0;
        }

        public override bool Connect(string port, int baudRate)
        {
            Disconnect();

            try
            {
                mPrepareClose = false;
                mSerialPort = new SerialPort(port, baudRate);
                mSerialPort.DtrEnable = true;
                mSerialPort.Open();
            }
            catch { }

            return IsConnect();
        }

        public override bool Disconnect()
        {
            if (mSerialPort != null && mSerialPort.IsOpen)
            {
                mPrepareClose = true;
                while (mIsSending)
                {
                    System.Threading.Thread.Sleep(50);
                }
                try
                {
                    mSerialPort.Close();
                    System.Threading.Thread.Sleep(100); // Let hardware shutdown
                }
                catch { }
            }

            mPrepareClose = false;
            mIsSending = false;
            mIsReceiving = false;

            return IsConnect();
        }

        public override bool IsConnect()
        {
            if (mSerialPort != null)
            {
                return mSerialPort.IsOpen;
            }

            return false;
        }

        public override void SetReadTimeout(int timeout)
        {
            if (mSerialPort != null)
            {
                mSerialPort.ReadTimeout = timeout;
            }
        }

        public override void SetWriteTimeout(int timeout)
        {
            if (mSerialPort != null)
            {
                mSerialPort.WriteTimeout = timeout;
            }
        }

        public override byte[] SendData(GTE.InfoModel.SendInfo data)
        {
            byte[] result = null;
            while (mIsSending) //有人正在傳輸
            {
                Thread.Sleep(50);
            }

            if (!mPrepareClose)
            {
                mIsSending = true;
                try
                {
                    mSerialPort.DiscardInBuffer();
                    mSerialPort.DiscardOutBuffer();

                    mSerialPort.Write(data.DataStream, 0, data.DataStream.Length);

                    result = DoReceive(data);
                }
                catch
                {
                    mIsSending = false; // 不可省略
                    //Disconnect();
                }
                finally
                {
                    mIsSending = false;
                }
            }

            return result;
        }

        public override byte[] SendDebugData(GTE.InfoModel.SendInfo data)
        {
            byte[] result = null;
            while (mIsSending) //有人正在傳輸
            {
                Thread.Sleep(50);
            }

            if (!mPrepareClose)
            {
                mIsSending = true;
                try
                {
                    mSerialPort.DiscardInBuffer();
                    mSerialPort.DiscardOutBuffer();

                    mSerialPort.Write(data.DataStream, 0, data.DataStream.Length);

                    result = DoDebugReceive(data);
                }
                catch
                {
                    mIsSending = false; // 不可省略
                    //Disconnect();
                }
                finally
                {
                    mIsSending = false;
                }
            }

            return result;
        }

        private byte[] DoReceive(GTE.InfoModel.SendInfo data)
        {
            mIsReceiving = true;

            byte[] value = null;
            List<byte> tempList = new List<byte>();
            byte[] buffer = new byte[1024];
            Thread.Sleep(data.DelayTime); // 第一次等待時間

            DateTime dt = DateTime.Now;
            while (mIsReceiving)
            {
                if (mSerialPort.BytesToRead > 0)
                {
                    try
                    {
                        Int32 receivedLength = mSerialPort.Read(buffer, 0, buffer.Length);
                        Array.Resize(ref buffer, receivedLength);
                        tempList.AddRange(buffer);
                    }
                    catch
                    {
                        //// 錯誤狀況發生關閉通訊連線
                        //Disconnect();
                        mIsReceiving = false;
                    }
                }

                if (buffer != null && buffer.Last().Equals(data.DataFooter))
                {
                    value = tempList.ToArray();
                    tempList.Clear();
                    mIsReceiving = false;
                }
                else
                {
                    if (DateTime.Now > dt.AddMilliseconds(data.Timeout))
                    {
                        mIsReceiving = false;
                    }
                    else
                    {
                        Array.Resize(ref buffer, 1024);
                        Thread.Sleep(1);
                    }
                }
            }

            return value;
        }

        private byte[] DoDebugReceive(GTE.InfoModel.SendInfo data)
        {
            mIsReceiving = true;

            List<byte> tempList = new List<byte>();
            byte[] buffer = new byte[1024];
            Thread.Sleep(data.DelayTime); // 第一次等待時間

            DateTime dt = DateTime.Now;
            while (mIsReceiving)
            {
                if (mSerialPort.BytesToRead > 0)
                {
                    try
                    {
                        Int32 receivedLength = mSerialPort.Read(buffer, 0, buffer.Length);
                        Array.Resize(ref buffer, receivedLength);
                        tempList.AddRange(buffer);
                    }
                    catch
                    {
                        //// 錯誤狀況發生關閉通訊連線
                        //Disconnect();
                        mIsReceiving = false;
                    }
                }

                if (DateTime.Now > dt.AddMilliseconds(data.Timeout))
                {
                    mIsReceiving = false;
                }

                Array.Resize(ref buffer, 1024);
                Thread.Sleep(1);
            }

            return tempList.ToArray();
        }

        public override void ReceiveData()
        {
            throw new NotImplementedException();
        }

        public override void Dispose()
        {
            Disconnect();
            mSerialPort.Dispose();
        }
    }
}
