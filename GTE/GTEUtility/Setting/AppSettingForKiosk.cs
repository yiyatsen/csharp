﻿using GTEUtility.Kiosk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEUtility.Setting
{
    public class AppSettingForKiosk : AppSetting
    {
        public string BatteryType { get; set; }
        public string[] BatteryTypeList { get; set; }

        public int StartAddress { get; set; }
        public int EndAddress { get; set; }

        public List<ChargeSlotItem> ChargeSlotList { get; set; }

        public AppSettingForKiosk()
            : base()
        {
            BatteryTypeList = new string[] { "4840", "4816" };
            ChargeSlotList = new List<ChargeSlotItem>();
        }


        //public int WaitingTimeForAd { get; set; }
        //// TODO

        //public string WebServer { get; set; }
        //public int UpdateTimeForWebServer { get; set; }


        // TODO
    }
}
