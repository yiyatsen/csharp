﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEUtility.Kiosk
{
    public class ChargeSlotItem
    {
        public int SlotId { get; set; }
        public int GateAddress { get; set; }
        public bool GateStatus { get; set; }
        public int BatteryAddress { get; set; }
        public bool BatteryStatus { get; set; }
    }
}
