﻿using GTE.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEUtility.Kiosk
{
    public class ModuleItem
    {
        public KioskModuleType Type { get; set; }
        /// <summary>
        /// 是否被選擇
        /// </summary>
        public bool IsSelected { get; set; }
        public int Address { get; set; }
        /// <summary>
        /// 是否被模組使用中
        /// </summary>
        public bool IsUsed { get; set; }
    }
}
