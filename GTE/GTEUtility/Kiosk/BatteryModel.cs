﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTEUtility.Kiosk
{
    public class BatteryModel
    {
        public string MCUVer { get; set; }
        public string BatteryNo { get; set; }
        public int SOC { get; set; }
        public int[] CellVoltage { get; set; }
    }
}
