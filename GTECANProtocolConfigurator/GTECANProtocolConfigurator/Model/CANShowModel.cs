﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GTECANProtocolConfigurator.Model
{
    public class CANShowModel : INotifyPropertyChanged
    {
        private bool _enable;
        private uint _id;
        private string _name;
        private int _sum;
        private string _timeStamp;
        private string _attribute;
        private string _rawData;

        public bool Enable
        {
            get { return _enable; }
            set
            {
                if (_enable == value) return;
                _enable = value;
                OnPropertyChanged("Enable");
            }
        }
        
        public string ID
        {
            get { return string.Format("0x{0:X03} ({1})", _id, _name); }
        }

        public int Sum
        {
            get { return _sum; }
            set
            {
                if (_sum == value) return;
                _sum = value;
                OnPropertyChanged("Sum");
            }
        }

        public string TimeStamp
        {
            get { return _timeStamp; }
            set
            {
                if (_timeStamp == value) return;
                _timeStamp = value;
                OnPropertyChanged("TimeStamp");
            }
        }

        public string Attribute
        {
            get { return _attribute; }
            set
            {
                if (_attribute == value) return;
                _attribute = value;
                OnPropertyChanged("Attribute");
            }
        }

        public string RawData
        {
            get { return _rawData; }
            set
            {
                if (_rawData == value) return;
                _rawData = value;
                OnPropertyChanged("RawData");
            }
        }


        public CANShowModel(uint id, string name)
        {
            _id = id;
            _name = name;
        }

        public bool IsMatchCANID(uint id)
        {
            return (_id == id);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
