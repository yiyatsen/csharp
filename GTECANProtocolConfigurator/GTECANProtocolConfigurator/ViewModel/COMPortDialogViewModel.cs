﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTECANProtocolConfigurator.ViewModel
{
    public class COMPortDialogViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<string> comPortList = new ObservableCollection<string>();
        public ObservableCollection<string> COMPortList
        {
            get { return comPortList; }
            set { comPortList = value; NotifyPropertyChanged("COMPortList"); }
        }
        private string selectedItem = "";
        public string SelectedItem
        {
            get { return selectedItem; }
            set { selectedItem = value; NotifyPropertyChanged("SelectedItem"); }
        }
 
        public event PropertyChangedEventHandler PropertyChanged;

        public COMPortDialogViewModel(string defultPort)
        {
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();

            bool isMatch = false;
            foreach(string port in ports)
            {
                comPortList.Add(port);
                if (!isMatch)
                {
                    if (defultPort == port)
                    {
                        selectedItem = port;
                        isMatch = true;
                    }
                    else
                    {
                        selectedItem = port;
                    }
                }
            }
            COMPortList = comPortList;
            SelectedItem = selectedItem;
        }

        protected virtual void NotifyPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged != null)
            {
                foreach (string propertyName in propertyNames) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                PropertyChanged(this, new PropertyChangedEventArgs("HasError"));
            }
        }
    }
}
