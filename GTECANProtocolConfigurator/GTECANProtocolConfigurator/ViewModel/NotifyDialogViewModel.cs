﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTECANProtocolConfigurator.ViewModel
{
    public class NotifyDialogViewModel : INotifyPropertyChanged
    {
        private string mTitle { get; set; }
        private string mMessage { get; set; }
        
        public string Title
        {
            get { return mTitle; }
            set { NotifyPropertyChanged("Title"); }
        }

        public string Message
        {
            get { return mMessage; }
            set { NotifyPropertyChanged("Message"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public NotifyDialogViewModel(string title, string message)
        {
            mTitle = title;
            mMessage = message;
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
