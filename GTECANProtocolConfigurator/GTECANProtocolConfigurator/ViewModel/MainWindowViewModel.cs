﻿using GTCore;
using GTECANProtocolConfigurator.Model;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace GTECANProtocolConfigurator.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private static string APPLOGPATH = System.AppDomain.CurrentDomain.BaseDirectory + @"Log\";
        /// <summary>
        /// 建立Command物件，可設定於Button Command屬性中
        /// </summary>
        public ICommand ExitToAppCommand { get { return new MainWindowCommand(ExitToApp); } }
        public ICommand SaveLogCommand { get { return new MainWindowCommand(SaveLog); } }
        public ICommand ConfigCANCommand { get { return new MainWindowCommand(ConfigCAN); } }
        public ICommand RunCOMPortDialogCommand { get { return new MainWindowCommand(ExecuteCOMPortDialog); } }
        public string GTCoreVersion
        {
            get { return string.Format("GTCore Ver.: {0}", Slot.GTCoreVersion); }
        }
        public string AppVersion
        {
            get { return string.Format("Ver.: {0}{1}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(), (MainWindow.ISBETA) ? "(BETA)" : ""); }
        }
        public string COMPortStatus
        {
            get { return (mSlot.isOpen) ? "Disconnect" : "Connect"; }
            set
            {
                NotifyPropertyChanged("COMPort");
                NotifyPropertyChanged("COMPortStatus");
                NotifyPropertyChanged("COMPortStatusICON");
                NotifyPropertyChanged("ConnectionStatus");
                NotifyPropertyChanged("DisconnectionStatus");
            }
        }
        public MaterialDesignThemes.Wpf.PackIconKind COMPortStatusICON
        {
            get { return (mSlot.isOpen) ? MaterialDesignThemes.Wpf.PackIconKind.MouseVariantOff : MaterialDesignThemes.Wpf.PackIconKind.MouseVariant; }
        }
        public string COMPort
        {
            get { return string.Format("Port: {0}", (mSlot.path != "") ? mSlot.path : "None"); }
        }
        public System.Windows.Visibility ConnectionStatus
        {
            get { return (mSlot.isOpen) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
        }
        public System.Windows.Visibility DisconnectionStatus
        {
            get { return (!mSlot.isOpen) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
        }
        
        private readonly ObservableCollection<CANShowModel> dataList;
        public ObservableCollection<CANShowModel> Items => dataList;

        public event PropertyChangedEventHandler PropertyChanged;
        private MainWindow mMainWindow { get; set; }
        private Slot mSlot { get; set; }
        private CancellationTokenSource mCTS { get; set; }
        public string mDefaultPort = "";
        private List<string> mRawList = new List<string>();
        private List<string> mBatInoList = new List<string>();
        private bool mIsGetCANConfig { get; set; }

        public MainWindowViewModel(MainWindow mainWindow)
        {
            mMainWindow = mainWindow;
            mSlot = new Slot();
            mSlot.autoSetting.enableCANCmd = true;

            dataList = new ObservableCollection<CANShowModel>();

            foreach (GTCore.GTCAN.GTCANATTR attr in GTCore.GTCAN.CANATTRLIST)
            {
                dataList.Add(new CANShowModel(attr.id, attr.name));
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void ExitToApp(object obj)
        {
            if (mSlot.isOpen)
            {
                this.StopRun();
            }
            mMainWindow.ExitAPP();
        }

        private async void SaveLog(object obj)
        {
            if (mSlot.isOpen)
            {
                DateTime dt = DateTime.Now;
                try
                {
                    if (!Directory.Exists(APPLOGPATH))
                    {
                        Directory.CreateDirectory(APPLOGPATH);
                    }
                    string txt = "TimeStamp\tID\tAttribute\tRawData\r\n";
                    foreach (string str in mRawList)
                    {
                        txt += str;
                    }
                    File.AppendAllText(APPLOGPATH + dt.ToString("yyyyMMddHHmmss") + "_raw.txt", txt);
                    txt = "[";
                    foreach (string str in mBatInoList)
                    {
                        txt += str;
                    }

                    txt = txt.TrimEnd('\n').TrimEnd('\r').TrimEnd(',') + "]";
                    File.AppendAllText(APPLOGPATH + dt.ToString("yyyyMMddHHmmss") + ".txt", txt);

                    ExecuteNotifyDialog("Success", "Save Log Success!");
                }
                catch (Exception ex)
                {
                    ExecuteNotifyDialog("Alert", ex.Message);
                }
            }
        }

        private async void ConfigCAN(object obj)
        {
            if (mSlot.isOpen)
            {
                var result = await DialogHost.Show(new ProgressBarDialog(), "RootDialog", ConfigCANOpenedEventHandler, ConfigCANClosingEventHandler);
            }
        }

        private async void ConfigCANOpenedEventHandler(object sender, DialogOpenedEventArgs eventargs)
        {
            await Task.Delay(500);
            // DataGrid 的 Checkbox 有問題, 如果沒更新UI會造成最後一筆資料不會更新
            int value = 0;

            for (int i = 0; i < Items.Count; i++)
            {
                if (Items[i].Enable)
                {
                    value |= (0x01 << i);
                }
            }
            mIsGetCANConfig = false;

            BatOptions batOpts = new BatOptions();
            batOpts.EnableRecv = false;
            batOpts.STX = BatCore.CANSTX;
            batOpts.CANID = 0x11000001;
            List<byte> dataPacket = new List<byte>();
            dataPacket.Add(0x02);
            dataPacket.Add((byte)value);
            batOpts.CANData = dataPacket.ToArray();
            batOpts.CANType = 0x01;

            batOpts = await mSlot.SendMsg(batOpts);
            eventargs.Session.Close(false);
        }

        private void ConfigCANClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {

        }
            private async void ExecuteNotifyDialog(string title, string message)
        {
            var result = await DialogHost.Show(new NotifyDialog
            {
                DataContext = new NotifyDialogViewModel(title, message)
            }, "RootDialog");
        }

        private async void ExecuteCOMPortDialog(object o)
        {
            if (!mSlot.isOpen)
            {
                //  let's set up a little MVVM, cos that's what the cool kids are doing:
                //  show the dialog
                var result = await DialogHost.Show(new COMPortDialog
                {
                    DataContext = new COMPortDialogViewModel(mDefaultPort)
                }, "SecondDialog", COMPortDialogClosingEventHandler);
            }
            else
            {
                this.StopRun();
            }
        }
        private void StopRun()
        {
            if (mCTS != null)
            {
                mCTS.Cancel();
            }

            mSlot.DisonnectCOMPort();

            mRawList.Clear();
            mBatInoList.Clear();

            mIsGetCANConfig = false;

            foreach (var result in Items)
            {
                result.Enable = false;
                result.TimeStamp = "";
                result.RawData = "";
                result.Attribute = "";
                result.Sum = 0;
            }
            
            COMPortStatus = "";
        }

        private void COMPortDialogClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if ((bool)eventArgs.Parameter == false) return;
            
            if (((COMPortDialog)eventArgs.Content).COMPortComboBox.SelectedItem != "")
            {
                mDefaultPort = ((COMPortDialog)eventArgs.Content).COMPortComboBox.SelectedItem.ToString();
                int result = mSlot.ConnectCOMPort(mDefaultPort, Slot.DEFAULTBAUDRATE.BaudRate);
                if (result == 0)
                {
                    COMPortStatus = "";

                    mCTS = new CancellationTokenSource();
                    PeriodicAsync(mCTS.Token);
                }
                else
                {
                    this.StopRun();

                    eventArgs.Cancel();
                    eventArgs.Session.UpdateContent(new NotifyDialog()
                    {
                        DataContext = new NotifyDialogViewModel("Warning", "Open COM Port Fail!" )
                    });
                }
            }
            else
            {
                ExecuteNotifyDialog("Warning", "Please check COM Port!");
                //eventArgs.Cancel();
                //eventArgs.Session.UpdateContent(new NotifyDialog()
                //{
                //    DataContext = new NotifyDialogViewModel("Warning", "Please check COM Port!")
                //});
            }
        }

        public async Task PeriodicAsync(CancellationToken cancellationToken)
        {
            while (true)
            {
                if (!mIsGetCANConfig)
                {
                    BatOptions batOpts = new BatOptions();
                    batOpts.EnableRecv = false;
                    batOpts.STX = BatCore.CANSTX;
                    batOpts.CANID = 0x11000001;
                    List<byte> dataPacket = new List<byte>();
                    dataPacket.Add(0x01);
                    batOpts.CANData = dataPacket.ToArray();
                    batOpts.CANType = 0x01;

                    batOpts = await mSlot.SendMsg(batOpts);
                }
                await RecvMsg();
                await Task.Delay(1000, cancellationToken);
            }
        }

        private async Task<BatOptions> RecvMsg()
        {
            BatOptions batOpts = new BatOptions();
            batOpts.EnableRecv = true;
            batOpts.FirstDelayTime = 0;

            batOpts = await mSlot.RecvMsg(batOpts);
            string dtStr = DateTime.Now.ToString("HH':'mm':'ss");
            foreach (var info in batOpts.RecvCANInfo)
            {
                if (0x12000001 == info.canID)
                {
                    if (info.result.ContainsKey("PACK_CONFIG"))
                    {
                        for (int i = 0; i < Items.Count; i++)
                        {
                            Items[i].Enable = (((byte)(info.result["PACK_CONFIG"]) >> i) & 0x01) == 0x01;
                        }
                        mIsGetCANConfig = true;
                    }
                }
                else
                {
                    CANShowModel result = Items.Where(x => x.IsMatchCANID(info.canID)).First();
                    result.TimeStamp = dtStr;
                    result.Sum += 1;
                    result.RawData = "[" + string.Join(",", info.data) + "]";
                    string str = "";
                    foreach(var item in info.result)
                    {
                        str += string.Format("[{0}: {1}]", item.Key, BatCore.ConvertAttrToString(item.Key, item.Value));
                    }
                    result.Attribute = str;

                    mRawList.Add(string.Format("{0}\t{1}\t{2}\t{3}\r\n", result.TimeStamp, result.ID, result.Attribute, result.RawData));
                    if (mRawList.Count > 5000)
                    {
                        mRawList.RemoveAt(0);
                    }
                }
            }

            string strAttr = "{\r\n\timestamp: " + dtStr + ",\r\n";
            foreach (var item in batOpts.Result)
            {
                if (item.Key == "PROTECTION")
                {
                    strAttr += string.Format("\t{0}", BatCore.ConvertAttrToString(item.Key, item.Value));
                }
                else
                {
                    strAttr += string.Format("\t{0}: {1}", item.Key, BatCore.ConvertAttrToString(item.Key, item.Value));
                }
                strAttr += ",\r\n";
            }
            strAttr += "},\r\n";
            mBatInoList.Add(strAttr);

            if (mBatInoList.Count > 500)
            {
                mBatInoList.RemoveAt(0);
            }

            return batOpts;
        }
    }
}
